<?php

Route::group(array('before'=> 'customer.account'), function () {
    Route::get('/account/listing/event/{id}', array('as'=>'account.listing.event','uses'=>'AccountController@getEventList'))->where('id', '[0-9]+');
    Route::post('/account/listing/event/update/{id}', array('as'=>'account.listing.event.update','uses'=>'AccountController@updateEvent'))->where('id', '[0-9]+');

    Route::post('/account/orders/update-delivery-address/{orderId}', array('before' => 'csrf', 'uses'=>'AccountController@updateShipping'))->where('orderId', '[0-9]+');
    Route::get('/account/orders/customer-address/{orderId}', array('as'=>'account.orders.customer.address','uses'=>'AccountController@getSetCustomerAddressByOrderId'))->where('orderId', '[0-9]+');
    //get customer address
    Route::get('/account/sale/customer-address/{orderId}', array('as'=>'account.sales.customer.address','uses'=>'AccountController@getCustomerAddressByOrderId'))->where('orderId', '[0-9]+');

    //save royal mail tracking code
    Route::post('/account/sale/tracking-code/update', array('as'=>'account.sales.tracking-code.update','uses'=>'AccountController@saveTrackingCode'));

    //contact by seller
    Route::post('/account/sale/contact-by-seller', array('as'=>'account.sales.contact-by-seller','uses'=>'ContactController@contactFromSeller'));


    //update ticket listing
    Route::post('/ticket/update/{id}', array('as'=>'account.listing.ticket.update', 'uses'=>'TicketListController@updateTicket'))->where('id', '[0-9]+');

    //clone ticket
    Route::get('/ticket/clone/{id}', array('as'=>'account.listing.ticket.clone', 'uses'=>'TicketListController@cloneTicket'))->where('id', '[0-9]+');
    Route::post('/account/listed/get-similar-listings/{id}', array('before'=>'csrf', 'as'=>'account.listed.similar-listings', 'uses'=>'AccountController@getSimilarListingsByListingID'))->where('id', '[0-9]+');
    Route::post('/account/listed/get-all-listings/{id}', array('before'=>'csrf', 'as'=>'account.listed.all-listings', 'uses'=>'AccountController@getAllListingsByListingID'))->where('id', '[0-9]+');

    Route::post('/account/sales/get-sales-for-event/{id}', array('before'=>'csrf','uses'=>'AccountController@getCustomerSalesByEventId'))->where('id', '[0-9]+');

    Route::post('/account/sales/get-dispatch-view-for-order/{id}', array('before'=>'csrf', 'uses'=>'AccountController@getDispatchViewByOrderID'))->where('id', '[0-9]+');
    Route::post('/account/sales/upload/{id}', array('before'=>'csrf', 'uses'=>'ETicketController@saveTicketsForOrderId'))->where('id', '[0-9]+');
    Route::get('/account/download-ticket/{id}', 'ETicketController@viewETicketById')->where('id', '[0-9]+');
    Route::post('/account/delete-ticket/{id}', array('uses'=>'ETicketController@deleteETicketById', 'before'=>'csrf'))->where('id', '[0-9]+');
    Route::post('/account/sale/update-collection/{id}', 'AccountController@saveCollectionPointForId')->where('id', '[0-9]+');

    Route::post('/account/purchases/get-delivery-action/{id}',array('uses'=>'AccountController@getDeliveryAction', 'before'=>'csrf'))->where('id', '[0-9]+');
});

Route::any('/shipping/details', 'CheckoutController@getShipping');

Route::post('/checkout/logprog', 'AbandonController@processInput');
Route::post('/checkout/logproglogin/{pid}', 'AbandonController@processLogin');

Route::post('/group-purchase-request', array('before'=>'csrf','as'=>'customer.group-purchase-request', 'uses'=>'ContactController@groupPurchased'));

//event auto complete
Route::get('/events/auto-complete', array('as'=>'ticket.events.autocomplete', 'uses'=>'TicketListController@eventAutoComplete'));
/*
|--------------------------------------------------------------------------
| Football tickets
|--------------------------------------------------------------------------
*/
Route::post('/ticket/registration', array('before' => 'csrf', 'as' => 'ticket.registrations', 'uses' => 'CustomerController@registrationAction'));

Route::group(array('prefix' => Config::get('bondcms.admin_prefix'), 'before' => array('auth.admin', 'assets_admin')), function () {
    Route::post('/ticket/group/list.json', array('as'=>'admin.event.ticket.group.list', 'uses'=>'FootballTicketController@listAction'));

    Route::get('/stadiums', array('as'=>'admin.stadiums.index', 'uses'=>'StadiumController@adminIndex'));
    Route::post('/stadiums/list.json', array('as'=>'admin.stadiums.index.list', 'uses'=>'StadiumController@adminGetList'));
    Route::get('/stadiums/edit/{id}', array('uses'=>'StadiumController@adminEdit'))->where('id', '[0-9]+');
    Route::post('/stadiums/update/{id}', array('uses'=>'StadiumController@adminUpdate', 'before'=>'csrf'))->where('id', '[0-9]+');
    Route::get('/stadiums/create', array('uses'=>'StadiumController@adminCreate'));
    Route::post('/stadiums/save', array('uses'=>'StadiumController@adminSave', 'before'=>'csrf'));

    Route::post('/stadiums/delete/{id}', array('uses'=>'StadiumController@adminDelete', 'before'=>'csrf'))->where('id', '[0-9]+');



    Route::get('/agents', 'AgentController@index');
    Route::POST('/agents/agentDT', 'AgentController@agentDT');
    Route::POST('/agents/add-agent', array('uses'=>'AgentController@addAgent','before'=>'csrf'));
    Route::POST('/agents/update-agent/{id}', array('uses'=>'AgentController@updateAgent','before'=>'csrf'))->where('id', '[0-9]+');

    Route::get('/agents/sales', 'AgentController@sales');
    Route::post('/agents/sales/totalDT', 'AgentController@salesTotalDT');
});

Route::get('/checkout/payment/{tid}', array('as'=>'checkout.payment', 'uses'=>function ($tid) {
        View::share('body_class', 'checkout-payment pages checkout checkout');

        $shoppingCartInfo = json_decode(Session::get('mid_'.$tid), true);
        View::share('redirect_link', $shoppingCartInfo['redirect_link']);
        View::share('product_id', $shoppingCartInfo['product_id']);
        View::share('input', Input::all());
        View::share('uip', Input::get('input'));
        View::share('id', Input::get('id'));

        $rt = RelatedTicket::where('product_id', Input::get('id'))->first();
        if($rt)
        {
            $event = FootBallEvent::where('id', $rt->event_id)->first();
        }

        View::share('ticket', RelatedTicket::getTicketByTicketId($rt->product_id));
        View::share('event', $event);

        return View::make(Template::name('frontend.%s.SlidingCheckout.pages.payment.page'))->with('tid', $tid);
}));


Route::get('/tracking/code/test', function () {
    try {
        echo 'should save';
        $tracking = new RoyalMailTracking();
        var_dump($tracking);
        $tracking->event_id = 1;
        $tracking->product_id = 1;
        $tracking->order_id = 1;
        $tracking->tracking_id_number = 1;
        $tracking->tracking_code = 1;
        echo $tracking->save();
    } catch (Exception $e) {
        echo $e->getMessage();
    }

});

Route::get('reset-mark', function(){
    // So need to get the events_ticket_buy
    // I need to get it by events_related_tickets
    // I need to get these by events date
    // I need to get these by user id

    // I need to
/*
    $theIds = DB::table('events_ticket_buy')
                ->leftJoin('events_related_tickets', 'events_ticket_buy.product_id', '=','events_related_tickets.product_id')
                ->leftJoin('web_user', 'events_related_tickets.user_id', '=', 'web_user.user_id')
                ->leftJoin('events', 'events_ticket_buy.event_id', '=' ,'events.id')
                ->where('web_user.user_id', 39)
                ->where('events.datetime', '>', '2016/02/01', 'AND', '2016/02/01')
                ->where('events.datetime', '<', '2016/02/01')
                ->select('events_ticket_buy.*')
                ->get();
*/
});

