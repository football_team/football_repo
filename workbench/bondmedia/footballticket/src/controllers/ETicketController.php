<?php

class ETicketController extends BaseController
{
    public function __construct()
    {

    }

    public function saveTicketsForOrderId($id)
    {
        $customer = Session::get('customer');
        $valid_access = FootballTicketBuy::getBuyerSellerIdByOrderId($id);
        $webUser = WebUser::where('user_id', '=', $customer['entity_id'])->first();
        if($valid_access[0]->user_id == $customer['entity_id'])
        {
            //User has valid access to upload tickets for this order.
            $files = Input::file('files');
            foreach($files as $file) 
            {
                $validator = Validator::make(
                    // Value for reqfile;
                    array('reqfile' => $file),
                    // Validator rule for reqfile
                    array('reqfile' => 'mimes:pdf|max:1024')
                );

                if(!$validator->fails())
                {
                    //The file is validated, it is a pdf and it is <=50MB in size
                    if($file->guessClientExtension() == "pdf")
                    {

                        $dbTicket = new ETicket();
                        $dbTicket->order_id = $id;
                        $name = microtime();
                        $name = $name.".pdf";
                        $path = storage_path()."/e-tickets/".$name;
                        $file->move(storage_path()."/e-tickets/",$name);
                        $dbTicket->pdf_path = $path;
                        $dbTicket->available = 0;
                        $dbTicket->save();

                        $fbt = FootballTicketBuy::where('order_id', '=', $id)->first();
                        $fbt->delivery_status = "in-transit";
                        $fbt->save();
                    }
                    else{
                        return Response::json(["0"=>"File must be a pdf, file size must be less than 50MB"], 404);
                    }
                    
                }
                else
                {
                    return Response::json(["0"=>"File must be a pdf, file size must be less than 50MB"], 404);
                }
            }

            //PUT EMAIL HERE

            $order = FootballTicketBuy::where('order_id', '=', $id)->first();
            $relatedTicket = RelatedTicket::where('product_id', '=', $order->product_id)->first();
            $buyer = WebUser::where('user_id', '=', $order->buyer_id)->first();
            $seller = WebUser::where('user_id', '=', $relatedTicket->user_id)->first();
            $event = FootBallEvent::where('id', '=', $relatedTicket->event_id)->first();

            $jT = json_decode($relatedTicket->ticket);
            $event->block = $jT->ticketInformation->loc_block;
            $event->row = $jT->ticketInformation->loc_row;

            $typestr = DB::table('events_ticket_type')
                        ->where('id','=',$jT->ticketInformation->ticket_type)
                        ->first();

            $event->ticket_type = $typestr->title;
            $event->qty = $order->qty;

            $toEmail = array();
            $toEmail['event']=$event;
            $toEmail['buyer']=$buyer;
            $toEmail['seller']=$seller;

            Mail::send('ticketAdmin::emails.eticket-approval', $toEmail, function ($message) use($id) {
                $from = Config::get('mail.from');
                $from['address'] = trans('homepage.contact-email');
                $message->from($from['address'], $from['name']);

                $message->to('info@footballticketpad.com', 'Football Ticket Pad')
                    ->subject("E-Ticket Require Approval: ".$id);

            });


            return Redirect::to('/account/sales');
        }
        else
        {
            return Response::json(["0"=>"You do not have access to this resource"], 404);
        }
    }

    public function viewETicketById($id)
    {
        $customer = Session::get('customer');
        $cId = $customer['entity_id'];

        $pdf = ETicket::where('id', '=', $id)->first();
        $valid_access = FootballTicketBuy::getBuyerSellerIdByOrderId($pdf->order_id);
        if(($valid_access[0]->user_id == $customer['entity_id'])||($valid_access[0]->buyer_id == $customer['entity_id']))
        {
            if($valid_access[0]->buyer_id == $customer['entity_id'])
            {
                if(!$pdf->available)
                {
                    App::abort(403, "Unauthorized Action.");
                }
            }
            if($pdf)
            {
                return Response::make(file_get_contents($pdf->pdf_path), 200, [
                    'Content-Type' => 'application/pdf',
                    'Content-Disposition' => 'inline; '.$pdf->id.".pdf",
                ]);
            }
            else
            {
                App::abort(404);
            }
        }
        else
        {
            App::abort(403, "Unauthorized Action.");
        }
    }

    public function deleteETicketByID($id)
    {
        $customer = Session::get('customer');
        $cId = $customer['entity_id'];

        $pdf = ETicket::where('id', '=', $id)->first();
        if(!$pdf)
        {
            App::abort(404);
        }
        
        $valid_access = FootballTicketBuy::getBuyerSellerIdByOrderId($pdf->order_id);
            
        if($valid_access[0]->user_id == $customer['entity_id'])
        {
            if(!$pdf->available)
            {
                $dir = $pdf->pdf_path;
                unlink($dir);
                $pdf->delete();
                return Response::json([0=>'Success'],200);
            }
            else
            {
                return Response::json([0=>"Cannot delete a ticket sent to a customer."], 404);
            }
        }
        else
        {
            App::abort(403, "Unauthorized Action.");
        }
    }


    public function adminViewETicket($id)
    {
        $pdf = ETicket::where('id', '=', $id)->first();
        if($pdf)
        {
            return Response::make(file_get_contents($pdf->pdf_path), 200, [
                'Content-Type' => 'application/pdf',
                'Content-Disposition' => 'inline; '.$pdf->id.".pdf",
            ]);
        }
        else
        {
            return Response::json([0=>"There was an error"], 404);
        }
    }

    private function adminDeleteETicketByID($id)
    {
        $pdf = ETicket::where('id', '=', $id)->first();
        if(!$pdf)
        {
            return false;
        }
        
 
        $dir = $pdf->pdf_path;
        unlink($dir);
        $pdf->delete();
        return true;
        
    }

    private function adminApproveETicketByID($id)
    {
        $pdf = ETicket::where('id', '=', $id)->first();
        if(!$pdf)
        {
            return false;
        }

        $pdf->available = 1;
        $pdf->save();

        return true;
    }

    public function adminSaveTicketsForOrderId($id)
    {
        //User has valid access to upload tickets for this order.
        $files = Input::file('files');
        foreach($files as $file) 
        {
            $validator = Validator::make(
                // Value for reqfile;
                array('reqfile' => $file),
                // Validator rule for reqfile
                array('reqfile' => 'mimes:pdf|max:1024')
            );

            if(!$validator->fails())
            {
                //The file is validated, it is a pdf and it is <=50MB in size
                if($file->guessClientExtension() == "pdf")
                {

                    $dbTicket = new ETicket();
                    $dbTicket->order_id = $id;
                    $name = microtime();
                    $name = $name.".pdf";
                    $path = storage_path()."/e-tickets/".$name;
                    $file->move(storage_path()."/e-tickets/",$name);
                    $dbTicket->pdf_path = $path;
                    $dbTicket->available = 0;
                    $dbTicket->save();

                    $fbt = FootballTicketBuy::where('order_id', '=', $id)->first();
                    $fbt->delivery_status = "in-transit";
                    $fbt->save();
                }
                else{
                    return Response::json(["0"=>"File must be a pdf, file size must be less than 50MB"], 404);
                }
            }
            else
            {
                return Response::json(["0"=>"File must be a pdf, file size must be less than 50MB"], 404);
            }
        }

        //Send email to us saying that etickets have been added to the order.


        return Redirect::to('/admin/sold-tickets/etickets');
    }

    public function adminTicketActions()
    {
        $inputs = Input::all();
        $oId = "";
        $tickets = [];
        $delTics = array();
        foreach($inputs as $key=>$val)
        {
            if(($key != "_token")&&($key != "_url"))
            {
                if($oId == "")
                {
                    $ticket = ETicket::where('id', '=', $key)->first();
                    $oId = $ticket->order_id;
                }
                switch($val)
                {
                    case "D":
                        ///This is a delete thing. I need to send emails for the deletes.
                        $delTic = ETicket::where('id', '=', $key)->first();
                        if(!$this->adminDeleteETicketByID($key))
                        {
                            return Response::json("Error", 404);
                        }
                        else
                        {
                            $delTics[] = $delTic;
                        }
                        break;
                    case "A":
                        if(!$this->adminApproveETicketByID($key))
                        {
                            return Response::json("Error", 404);
                        }
                        break;
                    case "N":
                        break;
                    default:
                        return Response::json("Error", 404);
                }
            }
        }
        if(sizeof($delTics))
        {
            //Tickets deleted need to email broker.
            $order = FootballTicketBuy::where('order_id', '=', $delTics[0]->order_id)->first();
            $relatedTicket = RelatedTicket::where('product_id', '=', $order->product_id)->first();
            $seller = WebUser::where('user_id', '=', $relatedTicket->user_id)->first();
            $buyer = WebUser::where('user_id', '=', $order->buyer_id)->first();
            $event = FootBallEvent::where('id', '=', $relatedTicket->event_id)->first();

            $jT = json_decode($relatedTicket->ticket);
            $event->block = $jT->ticketInformation->loc_block;
            $event->row = $jT->ticketInformation->loc_row;

            $typestr = DB::table('events_ticket_type')
                        ->where('id','=',$jT->ticketInformation->ticket_type)
                        ->first();

            $event->ticket_type = $typestr->title;

            $toEmail = array();
            $toEmail['event']=$event;
            $toEmail['buyer']=$buyer;
            $toEmail['tickets']=$delTics;
            $toEmail['seller']=$seller;
            $toEmail['num_deleted'] = sizeof($delTics);


            Mail::send('ticketAdmin::emails.deleted-tickets', $toEmail, function ($message) use($seller) {
                $from = Config::get('mail.from');
                $from['address'] = trans('homepage.contact-email');
                $message->from($from['address'], $from['name']);
                $email = 'info@footballticketpad.com';

                $message->to($seller->email, $seller->firstname." ".$seller->lastname)
                    ->bcc($email, 'Football Ticket Pad')
                    ->subject("E-Tickets Deleted.");

                if(trim($email) != 'info@footballticketpad.com' ) {
                    $message->bcc('info@footballticketpad.com', 'Football Ticket Pads');
                }
            });
        }

        $tickets = ETicket::where('order_id', '=', $oId)->get();
        $remaining = ETicket::where('order_id', '=', $oId)->where('available', '=',0)->get();

        if(!count($remaining))
        {
            //All approved, I need to send an email to customer

            //So create email blade with links to the pdf's (this is view/id)
            //
            $order = FootballTicketBuy::where('order_id', '=', $tickets->first()->order_id)->first();
            $order->delivery_status = 'delivered';
            $order->save();

            $relatedTicket = RelatedTicket::where('product_id', '=', $order->product_id)->first();
            $buyer = WebUser::where('user_id', '=', $order->buyer_id)->first();
            $seller = WebUser::where('user_id', '=', $relatedTicket->user_id)->first();
            $event = FootBallEvent::where('id', '=', $relatedTicket->event_id)->first();

            $buyer->brokerEmail = $seller->email;
            $jT = json_decode($relatedTicket->ticket);
            $event->block = $jT->ticketInformation->loc_block;
            $event->row = $jT->ticketInformation->loc_row;

            $typestr = DB::table('events_ticket_type')
                        ->where('id','=',$jT->ticketInformation->ticket_type)
                        ->first();

            $event->ticket_type = $typestr->title;

            $toEmail = array();
            $toEmail['event']=$event;
            $toEmail['buyer']=$buyer;
            $toEmail['tickets']=$tickets;

            Mail::send('ticketAdmin::emails.etickets-received', $toEmail, function ($message) use($buyer) {
                $from = Config::get('mail.from');
                $from['address'] = trans('homepage.contact-email');
                $message->from($from['address'], $from['name']);
                $email = 'info@footballticketpad.com';

                $message->to($buyer->email, $buyer->firstname." ".$buyer->lastname)
                    ->bcc($email, 'Football Ticket Pad')
                    ->bcc($buyer->brokerEmail, 'Broker')
                    ->subject("E-Tickets Delivered.");

                if(trim($email) != 'info@footballticketpad.com' ) {
                    $message->bcc('info@footballticketpad.com', 'Football Ticket Pads');
                }
            });
        }
        return View::make('ticketAdmin::sold-tickets.e-ticket-admin', ['order_id'=>$oId, 'tickets'=>$tickets]);
    }
}
