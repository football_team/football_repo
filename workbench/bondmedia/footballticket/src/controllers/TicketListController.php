<?php

class TicketListController extends \BaseController {

    private $data = '';
    private $ticketObject = null;

    public function eventAutoComplete() {
        $output = array();
        $q = Input::get('term');
        if(empty($q)) {
            return Response::json([], 400);
        }
        $events = FootBallEvent::whereRaw("title like  '%{$q}%' AND datetime >'".date('Y-m-d H:i:s')."'")
            ->where('id','!=',1465)
            ->skip(0)
            ->take(20)
            ->get();

        foreach($events as $e) {
            $output[] = array(
                'value' => strtolower($e->title),
                'label' => $e->title,
                'desc'  => date('dS F Y', strtotime($e->datetime)),
                'link'  => URL::to('/ticket/sell/'.$e->id),
                'eventLink' => FootBallEvent::getUrl($e)
            );
        }

        return Response::json($output, 200);
    }

    public function cloneTicket($id) {
        try {
            $customer = Session::get('customer');
            $this->ticketObject = RelatedTicket::find($id);
            if(!$this->ticketObject) {
                throw new Exception('ticket not found.');
            }
            $node = FootBallEvent::find($this->ticketObject->event_id);
            $ticketId = $this->ticketObject->event_id;
            if(!$node) {
                throw new Exception('event not found.');
            }
            $info = $node->toArray();

            $ticket =  new RelatedTicket;
            $ticket->fill($this->ticketObject->toArray());

            $data = json_decode($this->ticketObject->ticket, true);
            $ticket->available_qty = $data['ticketInformation']['number_of_ticket'];
            $attributeSet = TicketSoap::process('product_attribute_set.list');

            $product = array(
                'simple',
                $attributeSet[0]['set_id'],
                "-{$ticketId}-{$customer['entity_id']}-{$data['ticketInformation']['ticket_type']}-{$data['ticketInformation']['loc_block']}-{$data['ticketInformation']['loc_row']}-".time(),
                array(
                    'categories' => array(2),
                    'websites' => array(1),
                    'name' => $info['title'],
                    'description' => $info['content'],
                    'short_description' => $info['content'],
                    'weight' => '1',
                    'status' => '1',
                    'url_key' => $info['slug'],
                    'url_path' => $info['slug'],
                    'visibility' => '4',
                    'price' => $data['ticketInformation']['price'],
                    'tax_class_id' => 0,
                    'meta_title' => $info['title'],
                    'meta_keyword' => strip_tags($info['content']),
                    'meta_description' => strip_tags($info['content']),
                    'stock_data' => array(
                        'qty' => $data['ticketInformation']['number_of_ticket'],
                        'is_in_stock'=>1,
                        'min_sale_qty'=> 1

                    ),

                )
            );
            $ticket->product_id = TicketSoap::process('catalog_product.create', $product);
            $ticket->save();
            
            $locale = App::getLocale();


            $conv = DB::table('locales')
                        ->where('lang', $locale)
                        ->first();
            $conv=$conv->conversion;

            $output = array();
            $customer = Session::get('customer');

            $eventTickets = RelatedTicket::where('event_id', '=',$this->ticketObject->event_id)
                            ->where('user_id', '=',$customer['entity_id'])
                            ->where('ticket_status', '<>', 'deleted')
                            ->get();

            $node = FootBallEvent::find($this->ticketObject->event_id);
            $defaultRestriction = [];
            foreach($node->getSelectedRestrictions()  as $val) {

                $defaultRestriction[] = array(
                    'id'    => $val->id,
                    'title' => $val->title
                );
            }
            foreach($eventTickets as $t) {
                $temp = array();
                $temp['id'] = $t['id'];
                $ti = json_decode($t['ticket'], true);
                $temp['price'] = $t['price'] * $conv;
                $temp['earning'] = ($t['price'] - (($t['price'] * $t['selling_commission_percentage']) / 100))*$conv;
                $temp['listing_id'] = $t['product_id'];
                $temp['block'] = $ti['ticketInformation']['loc_block'];
                $temp['row'] = $ti['ticketInformation']['loc_row'];
                $temp['number_of_ticket'] = $t['available_qty'];
                $temp['form_of_ticket'] = $ti['ticketInformation']['form_of_ticket'];
                $temp['ticket_type'] = $ti['ticketInformation']['ticket_type'];
                $temp['status'] = $t['ticket_status'];
                $temp['sold'] = $ti['ticketInformation']['number_of_ticket'] - $t['available_qty'];
                $temp['ready_to_ship'] = $ti['paymentAgreement']['ticket_ready'];
                $temp['preference'] = $ti['ticketInformation']['sell_preference'];
                $temp['buyer_note'] = isset($ti['ticketInformation']['restrictions'])?$ti['ticketInformation']['restrictions']:'';
                $temp['can_offer_collection'] = $t->hand_delivery;
                foreach($defaultRestriction as $key=>$val) {
                    if(isset($temp['buyer_note'][$val['id']])) {
                        $val['checked'] = 'checked';
                    } else {
                        $val['checked'] = '';
                    } 

                    $defaultRestriction[$key] = $val;
                }

                $temp['default_restriction'] = $defaultRestriction;
                $output[] = $temp;
            }
            return Response::json($output, 200);

        } catch(Exception $e) {
            return Response::json($e->getMessage(), 400);
        }

    }

    public function updateTicket($id) {
        $customer = Session::get('customer');
        $webUser = WebUser::where('user_id', '=', $customer['entity_id'])->first();
        $this->ticketObject = RelatedTicket::find($id);

        $bf = 1 + (intval(Options::getOption('booking_fees'))/100);

        if($this->ticketObject->user_id != $webUser->user_id)
        {
            return Response::json('User not authorized to alter this listing', 404);
        }

        try{
            $type = Input::get('type');
            $method = 'update'.$type;
            if(method_exists($this,$method)) {
                $ti = json_decode($this->ticketObject['ticket'], true);
                $sold = $ti['ticketInformation']['number_of_ticket'] - $this->ticketObject['available_qty'];
                $flag = $this->ticketObject->id;

                if(($sold != 0)&&($type!='Qty'))
                {
                    if(!$this->cloneForUpdate())
                    {
                        Log::info("Could not clone for update: ".$id);
                        throw new Exception('Could not alter ticket, could not clone ticket silently.');
                    }
                }

                if(!$this->ticketObject) {
                    throw new Exception('ticket not found.');
                }

                $this->data = Input::get('value');

                $locale = App::getLocale();
                $conv = DB::table('locales')
                            ->where('lang', $locale)
                            ->first();
                $conv = $conv->conversion;

                if($type == 'Price')
                {
                    $this->data = ($this->data/85)*100;
                    $this->data = $this->data * (1/$conv);
                }

                $data = $this->$method();

                $output = array();
                $output['tickets'] = "";
                $output['flag'] = $flag;
                $output['data'] = $data;
                $output['id'] = $this->ticketObject->event_id;
                $customer = Session::get('customer');

                $wat = array();
                $wat[] = $this->ticketObject;

                $node = FootBallEvent::find($this->ticketObject->event_id);
                $defaultRestriction = [];
                foreach($node->getSelectedRestrictions()  as $val) {

                    $defaultRestriction[] = array(
                        'id'    => $val->id,
                        'title' => $val->title
                    );
                }
                foreach($wat as $t) {
                    $temp = array();
                    $temp['id'] = $t['id'];
                    $ti = json_decode($t['ticket'], true);
                    $temp['price'] = ($t['price'] - (($t['price'] * $t['selling_commission_percentage']) / 100))*$conv;
                    $temp['earning'] = ($t['price'] * $conv)*$bf;
                    $temp['listing_id'] = $t['product_id'];
                    $temp['block'] = $ti['ticketInformation']['loc_block'];
                    $temp['row'] = $ti['ticketInformation']['loc_row'];
                    $temp['number_of_ticket'] = $t['available_qty'];
                    $temp['form_of_ticket'] = $ti['ticketInformation']['form_of_ticket'];
                    $temp['ticket_type'] = $ti['ticketInformation']['ticket_type'];
                    $temp['status'] = $t['ticket_status'];
                    $temp['sold'] = $ti['ticketInformation']['number_of_ticket'] - $t['available_qty'];
                    $temp['ready_to_ship'] = $ti['paymentAgreement']['ticket_ready'];
                    $temp['preference'] = $ti['ticketInformation']['sell_preference'];
                    $temp['buyer_note'] = isset($ti['ticketInformation']['restrictions'])?$ti['ticketInformation']['restrictions']:'';
                    $temp['can_offer_collection'] = $t->hand_delivery;
                    foreach($defaultRestriction as $key=>$val) {
                        if(isset($temp['buyer_note'][$val['id']])) {
                            $val['checked'] = 'checked';
                        } else {
                            $val['checked'] = '';
                        } 

                        $defaultRestriction[$key] = $val;
                    }

                    $temp['default_restriction'] = $defaultRestriction;
                    $output['tickets'] = $temp;
                }
                return Response::json($output, 200);
                

            } else {
                throw new Exception('method not exists');
            }
        } catch (Exception $e) {
            return Response::json($e->getMessage(), 400);
        }


    }

    protected function updateFormOfTicket() {
        $ticket = json_decode($this->ticketObject->ticket, true);
        $ticket['ticketInformation']['form_of_ticket'] = $this->data;
        $this->ticketObject->ticket = json_encode($ticket);
        $this->ticketObject->save();
        return Response::json('success', 200);
    }

    protected function updateTicketType() {
        $ticket = json_decode($this->ticketObject->ticket, true);
        $ticket['ticketInformation']['ticket_type'] = $this->data;
        $this->ticketObject->ticket = json_encode($ticket);
        $this->ticketObject->save();
        return Response::json('success', 200);
    }

    protected function updateSellPreference() {
        $ticket = json_decode($this->ticketObject->ticket, true);
        $ticket['ticketInformation']['sell_preference'] = $this->data;
        $this->ticketObject->ticket = json_encode($ticket);
        $this->ticketObject->save();
        return Response::json('success', 200);
    }

    protected function updateQty() {
        //SELECT product_id, SUM(qty) AS total_sold FROM events_ticket_buy WHERE product_id = 28 GROUP BY product_id;
        $soldTicketObj = DB::table('events_ticket_buy')
             ->select(DB::raw('product_id, SUM(qty) AS total_sold'))
             ->where('product_id', '=',$this->ticketObject->product_id)
             ->first();

        $totalSold = 0;
        if($soldTicketObj) {
            $totalSold = $soldTicketObj->total_sold;
        }
        $ticket = json_decode($this->ticketObject->ticket, true);
        if($totalSold <= $this->data) {
            $ticket['ticketInformation']['number_of_ticket'] = $this->data + $totalSold;

            $this->ticketObject->ticket = json_encode($ticket);
            $this->ticketObject->available_qty = $this->data;
            $this->ticketObject->save();
            $this->setDataToPost(array(
                'product_id'    => $this->ticketObject->product_id,
                'qty'   => $this->data
            ));
            $this->submitPostToApi('customer/product/index');
            //$this->setApiResponseHeader();
            return true;
        } else {
            throw new Exception('unable to change quantity.');
        }
    }

    protected function updateBlockAndRow() {
        $ticket = json_decode($this->ticketObject->ticket, true);
        foreach($this->data as $key=>$val) {
            if($key == 'block') {
                $ticket['ticketInformation']['loc_block'] = $val;
            } else if ($key == 'row') {
                $ticket['ticketInformation']['loc_row'] = $val;
            }
        }

        $this->ticketObject->ticket = json_encode($ticket);
        $this->ticketObject->save();

        return Response::json($this->data, 200);
    }

    protected function updateReadyToShip() {
        $ticket = json_decode($this->ticketObject->ticket, true);
        $ticket['paymentAgreement']['ticket_ready'] = $this->data;
        $this->ticketObject->ticket = json_encode($ticket);
        $this->ticketObject->save();
        return Response::json('success', 200);
    }

    protected function updateBuyerNote() {
        $ticket = json_decode($this->ticketObject->ticket, true);
        $ticket['ticketInformation']['restrictions'] = $this->data;
        $this->ticketObject->ticket = json_encode($ticket);
        $this->ticketObject->save();
        return Response::json('success', 200);
    }

    protected  function updatePrice() {
        $ticket = json_decode($this->ticketObject->ticket, true);
        $ticket['ticketInformation']['price'] = $this->data;
        $this->ticketObject->ticket = json_encode($ticket);
        $this->ticketObject->price = $this->data;

        $this->setDataToPost(array(
            'product_id'    => $this->ticketObject->product_id,
            'price'   => $this->data
        ));
        $this->submitPostToApi('customer/product/index');

        $this->ticketObject->save();

        return Response::json('success', 200);
    }

    protected function updateHandDelivery()
    {
        if(($this->data == 1) || ($this->data==0))
        {
            $this->ticketObject->hand_delivery = $this->data;
            $this->ticketObject->save();
            return true;
        }
        else
        {
            return false;
        }
    }

    /*
        This function will clone a ticket silently, then delete the previous listing, thereby preventing
        issues resulting from someone listing a ticket, having a sale, changing ticket information and being 
        confused.
    */
    protected function cloneForUpdate()
    {
        try {
            if(!$this->ticketObject) {
                return false;
            }
            $this->ticketObject->ticket_status = 'deleted';
            $this->ticketObject->save();

            $customer = Session::get('customer');
            
            $node = FootBallEvent::find($this->ticketObject->event_id);
            $ticketId = $this->ticketObject->event_id;
            if(!$node) {
                Log::info('event not found.');
                return false;
            }
            $info = $node->toArray();

            $ticket =  new RelatedTicket;
            $ticket->fill($this->ticketObject->toArray());

            $data = json_decode($this->ticketObject->ticket, true);
            $ticket->available_qty = $this->ticketObject->available_qty;

            $ndt = json_decode($ticket->ticket);
            $ndt->ticketInfotmation->number_of_ticket = $this->ticketObject->available_qty;

            $ticket->ticket = json_encode($ndt);

            $attributeSet = TicketSoap::process('product_attribute_set.list');

            $product = array(
                'simple',
                $attributeSet[0]['set_id'],
                "-{$ticketId}-{$customer['entity_id']}-{$data['ticketInformation']['ticket_type']}-{$data['ticketInformation']['loc_block']}-{$data['ticketInformation']['loc_row']}-".time(),
                array(
                    'categories' => array(2),
                    'websites' => array(1),
                    'name' => $info['title'],
                    'description' => $info['content'],
                    'short_description' => $info['content'],
                    'weight' => '1',
                    'status' => '1',
                    'url_key' => $info['slug'],
                    'url_path' => $info['slug'],
                    'visibility' => '4',
                    'price' => $data['ticketInformation']['price'],
                    'tax_class_id' => 0,
                    'meta_title' => $info['title'],
                    'meta_keyword' => strip_tags($info['content']),
                    'meta_description' => strip_tags($info['content']),
                    'stock_data' => array(
                        'qty' => $this->ticketObject->available_qty,
                        'is_in_stock'=>1,
                        'min_sale_qty'=> 1

                    ),

                )
            );
            $ticket->product_id = TicketSoap::process('catalog_product.create', $product);
            $ticket->save();
            
            $this->ticketObject = $ticket;
            return true;

        } catch(Exception $e) {
            Log::info(var_export($e->getMessage(),true));
            return false;
        }
    }




}
