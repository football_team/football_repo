<?php

class SellController extends BaseController
{
    public function __construct()
    {

    }

    public function ticketInformation($ticketId = '') {
        if (empty($ticketId)) {
            return '';
        }


        if(Request::isMethod('post'))
        {
            $data = Input::all();
            Session::put('ticket_part_1_'.$ticketId, $data);
            //redirect to second page
            return Redirect::route('ticket.sell.2', array('id'=>$ticketId));
        }

        $locale = App::getLocale();
        $conv = DB::table('locales')
                    ->where('lang', $locale)
                    ->first();
        $conv = $conv->conversion;

        $ticketAggregatedPrice = DB::table('events_related_tickets')
            ->select(DB::raw('MIN(price) AS min_price, MAX(price) AS max_price, AVG(price) AS avg_price'))
            ->where('event_id', '=', $ticketId)
            ->where('ticket_status', '=', 'active' )
            ->groupBy('event_id')
            ->first();

        $booking_fees_perc = intval(Options::getOption('booking_fees'));

        if($ticketAggregatedPrice)
        {

            $ticketAggregatedPrice->min_price = ($ticketAggregatedPrice->min_price * $conv) * (1+($booking_fees_perc/100));
            $ticketAggregatedPrice->max_price = ($ticketAggregatedPrice->max_price * $conv) * (1+($booking_fees_perc/100));
            $ticketAggregatedPrice->avg_price = ($ticketAggregatedPrice->avg_price * $conv) * (1+($booking_fees_perc/100));
        }

        View::share('ticketAggregatedPrice', $ticketAggregatedPrice);
        View::share('bookingPerc', $booking_fees_perc);

        View::share('body_class', 'pages sell-1 sell1');
        $node = FootBallEvent::find($ticketId);

        $linkTeam = DB::table('team_stadium')->where('team_id','=',$node->home_team_id)->first();
        if (!empty($linkTeam->stadium_id)) {
            $stadium = DB::table('stadium')->where('id','=',$linkTeam->stadium_id)->first();
            $stadium = $stadium->svg_map;
        }

        $linkEvent = DB::table('event_stadium')->where('event_id','=',$node->id)->first();
        if (!empty($linkEvent->stadium_id)) {
            $stadium =  DB::table('stadium')->where('id','=',$linkEvent->stadium_id)->first();
            $stadium = $stadium->svg_map;
        }


        if(empty($stadium)){$stadium=0;}
        return View::make(Template::name('frontend.%s.sell.1'), compact('node'))->with('stadium',$stadium);
    }

    public function ticketSellerInfo($ticketId = '') {
        //$ticket = Input::all();

        if(Session::get('ticket_part_1_'.$ticketId) == '') {
            return Redirect::route('ticket.sell.1', array('id'=>$ticketId));
        }

        $customer = Session::get('customer');
        $node = FootBallEvent::find($ticketId);

        try {
            if(Request::isMethod('post'))
            {
                $hour = round((strtotime($node->datetime) - strtotime('NOW'))/(60*60));
                WebUser::validateSellerAllowedPublishTime($customer['entity_id'],$hour);
                $data = Input::all();
                Session::put('ticket_part_2_'.$ticketId, $data);
                //redirect to second page
                return Redirect::route('ticket.sell.3', array('id'=>$ticketId));
            }
        } catch (Exception $e) {
            Notification::error($e->getMessage());
            return Redirect::route('ticket.sell.2', array('id'=>$ticketId));
        }

        View::share('body_class', 'pages sell-2 sell2');
        View::share('customer', $customer);

        return View::make(Template::name('frontend.%s.sell.2'), compact('node'));
    }

    public function ticketSellerAgreement($ticketId = '') {

        if(Session::get('ticket_part_1_'.$ticketId) == '' || Session::get('ticket_part_2_'.$ticketId) == '') {
            return Redirect::route('ticket.sell.1', array('id'=>$ticketId));
        }

        if(Request::isMethod('post'))
        {
            $data = Input::all();
            Session::put('ticket_part_3_'.$ticketId, $data);
            //redirect to second page
            return Redirect::route('ticket.sell.4', array('id'=>$ticketId));
        }

        $customer = Session::get('customer');
        View::share('body_class', 'pages sell-3 sell3');
        View::share('customer', $customer);
        View::share('allCard', true);
        View::share('ticketId', $ticketId);
        $node = FootBallEvent::find($ticketId);
        return View::make(Template::name('frontend.%s.sell.3'), compact('node'));
    }

    public function publishTicket($ticketId = '') {
        $locale = App::getLocale();
        $conv = DB::table('locales')
                    ->where('lang', $locale)
                    ->first();
        $conv = $conv->conversion;

        $customer = Session::get('customer');
        $cObj = WebUser::where('user_id', $customer['entity_id'])->first();
        $cType = $cObj->seller_type;

        try {
            $data =  array();
            $customer = Session::get('customer');

            $data['ticketInformation']   = Session::get('ticket_part_1_'.$ticketId);
            $data['paymentMethod']       = Session::get('ticket_part_2_'.$ticketId);
            $data['paymentAgreement']    = Session::get('ticket_part_3_'.$ticketId);
            if(Session::get('ticket_part_1_'.$ticketId) == ''
                || Session::get('ticket_part_2_'.$ticketId) == ''
                || Session::get('ticket_part_2_'.$ticketId) == ''
            ) {
                return Redirect::route('ticket.sell.1', array('id'=>$ticketId));
            }
            $data['ticketInformation']['price'] = round(floatval((($data['ticketInformation']['price']/85)*100) * (1/$conv)),2);

            $node = FootBallEvent::find($ticketId);
            $info = $node->toArray();
            $attributeSet = TicketSoap::process('product_attribute_set.list');
            $time = time();

            $product = array(
                'simple',
                $attributeSet[0]['set_id'],
                "-{$ticketId}-{$customer['entity_id']}-{$data['ticketInformation']['ticket_type']}-{$data['ticketInformation']['loc_block']}-{$data['ticketInformation']['loc_row']}-{$time}",
                array(
                    'categories' => array(2),
                    'websites' => array(1),
                    'name' => $info['title'],
                    'description' => '-',
                    'short_description' => '-',
                    'weight' => '1',
                    'status' => '1',
                    'url_key' => $info['slug'],
                    'url_path' => $info['slug'],
                    'visibility' => '4',
                    'price' => $data['ticketInformation']['price'],
                    'tax_class_id' => 0,
                    'meta_title' => $info['title'],
                    'meta_keyword' => strip_tags($info['content']),
                    'meta_description' => strip_tags($info['content']),
                    'stock_data' => array(
                        'qty' => $data['ticketInformation']['number_of_ticket'],
                        'is_in_stock'=>1,
                        'min_sale_qty'=> 1

                    ),

                )
            );
            $response = TicketSoap::process('catalog_product.create', $product);

            $data['commission_in_per'] = Config::get('ticket.selling_commission');

            $total_selling = $data['ticketInformation']['number_of_ticket'] * $data['ticketInformation']['price'];
            $selling_fees_amount = (($data['ticketInformation']['number_of_ticket'] * $data['ticketInformation']['price']) * Config::get('ticket.selling_commission')) / 100;

            if ($response) {
                $relatedTicket = new RelatedTicket();
                $relatedTicket->event_id = $ticketId;
                $relatedTicket->product_id = $response;
                $relatedTicket->ticket = json_encode($data);
                $relatedTicket->price = $data['ticketInformation']['price'];
                $relatedTicket->available_qty = $data['ticketInformation']['number_of_ticket'];
                $relatedTicket->user_id = $customer['entity_id'];
                $relatedTicket->selling_commission_percentage = Config::get('ticket.selling_commission');
                $relatedTicket->selling_commission_amount = $selling_fees_amount;
                $relatedTicket->hand_delivery = intval($data['ticketInformation']['hand_delivery']);
                $relatedTicket->ticket_status = 'active';
                if($cType == "customer")
                {
                    $relatedTicket->sec_check = 1;
                    $relatedTicket->ticket_status = 'blocked';
                }
                $relatedTicket->save();
            }
        } catch (Exception $e) {
            $response = Response::make(json_encode(['message' => $e->getMessage()]), '400');
            $response->header('Content-Type', 'application/json');
            return $response;
        }

        Session::put('ticket_part_1_'.$ticketId, '');
        Session::put('ticket_part_2_'.$ticketId, '');
        Session::put('ticket_part_3_'.$ticketId, '');

        $ticket = RelatedTicket::getTicketByTicketId($relatedTicket->product_id);
        $event = FootBallEvent::find($ticketId);

        $booking_fees_perc = intval(Options::getOption('booking_fees'));
        $total_selling_plus_booking = ($total_selling * $conv) * (1 + ($booking_fees_perc/100));

        $formData = array(
            'seller_name' => "{$customer['firstname']} {$customer['lastname']}",
            'number_of_ticket' => $data['ticketInformation']['number_of_ticket'],
            'selling_price' => $data['ticketInformation']['price'] * $conv,
            'selling_fees'=> Config::get('ticket.selling_commission'),
            'selling_fees_amount' => $selling_fees_amount * $conv,
            'total_selling' => $total_selling * $conv,
            'net_payment' => ($total_selling * $conv) - ($selling_fees_amount * $conv),
            'game'      => $info['title'],
            'event_date'    => $info['datetime'],
            'web_price' => $total_selling_plus_booking/intval($data['ticketInformation']['number_of_ticket']),
            'web_price_total' => $total_selling_plus_booking

        );

        $formData['event_location'] = $event->event_location;

        $formData['formOfTicket'] = @$ticket->formOfTicket['title'];
        $formData['location'] = @$ticket->ticketType['title'];

        $temp = array();
        if(isset($data['ticketInformation']['loc_block']) && !empty($data['ticketInformation']['loc_block'])) {
            $temp[] =     'Block: '.$data['ticketInformation']['loc_block'];
        }

        if(isset($data['ticketInformation']['loc_row']) && !empty($data['ticketInformation']['loc_row'])) {
            $temp[] =     'Row: '.$data['ticketInformation']['loc_row'];
        }

        $formData['location'] = $formData['location'].', '.implode(', ', $temp);

        $temp = array();

        $defaultRestriction = [];
        foreach($event->getSelectedRestrictions()  as $val) {

            $defaultRestriction[] = array(
                'id'    => $val->id,
                'title' => $val->title
            );
        }

        foreach($defaultRestriction as $r):
            if(in_array($r['id'],$ticket->info['restrictions'])):
                $temp[] = $r['title'];
            endif;
        endforeach;

        if($ticket->buyerNot != '' ):
            $temp[] = '<br />Note: '.$ticket->buyerNot;
        endif;

        $formData['restrictions'] = implode(', ', $temp);

        $description = $this->ticketInfo($formData);

        $response = TicketSoap::process('catalog_product.update', array($relatedTicket->product_id, array(
            'description' => $description,
            'short_description' => $description
        )));

        if($cType == "customer")
        {
            Mail::send('emails.tickets.listing-blocked', $formData, function ($message) {
                $customer = Session::get('customer');
                $message->from(trans('homepage.contact-email'), Config::get('ticket.default_name'));
                $email = Options::getOption('ticket_listing_cc') == ''? 'info@footballticketpad.com': Options::getOption('ticket_listing_cc');
                $message->to($customer['email'], "{$customer['firstname']} {$customer['lastname']}")
                    ->bcc($email, 'Football Ticket Pads')
                    ->subject("Ticket Listing on hold");
                if(trim($email) != 'info@footballticketpad.com' ) {
                    $message->bcc('info@footballticketpad.com', 'Football Ticket Pad');
                }
            });
        }
        else
        {
            Mail::send('emails.tickets.listing-confirmation', $formData, function ($message) {
                $customer = Session::get('customer');
                $message->from(trans('homepage.contact-email'), Config::get('ticket.default_name'));
                $email = Options::getOption('ticket_listing_cc') == ''? 'info@footballticketpad.com': Options::getOption('ticket_listing_cc');
                $message->to($customer['email'], "{$customer['firstname']} {$customer['lastname']}")
                    ->bcc($email, 'Football Ticket Pads')
                    ->subject("Ticket Listing confirmation");
                if(trim($email) != 'info@footballticketpad.com' ) {
                    $message->bcc('info@footballticketpad.com', 'Football Ticket Pad');
                }
            });
        }


        $response = Response::make(json_encode(['message' => $response]), '200');

        if($cType == 'customer')
        {
            $response = Response::make(json_encode(['message'=>'held']),200);
        }

        $response->header('Content-Type', 'application/json');
        return $response;
    }

    public function ticketInfo($option) {
        ob_start();
        ?>
        <table cellspacing="0" cellpadding="0" border="0" width="650">
            <thead>
            <tr>
                <th align="left" width="325" bgcolor="#EAEAEA" style="font-size:13px;padding:5px 9px 6px 9px;line-height:1em">Game</th>
                <th width="10"></th>
                <th align="left" width="325" bgcolor="#EAEAEA" style="font-size:13px;padding:5px 9px 6px 9px;line-height:1em">Ticket Information</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td valign="top" style="font-size:12px;padding:7px 9px 9px 9px;border-left:1px solid #eaeaea;border-bottom:1px solid #eaeaea;border-right:1px solid #eaeaea">
                    <?php echo @$option['game'];?>, <?php echo @$option['event_location']; ?> <br/>

                    Date: <?php echo date('l, dS F Y, h:ia', strtotime(@$option['event_date']));?>
                </td>
                <td>&nbsp;</td>
                <td valign="top" style="font-size:12px;padding:7px 9px 9px 9px;border-left:1px solid #eaeaea;border-bottom:1px solid #eaeaea;border-right:1px solid #eaeaea">
                    Location: <?php echo @$option['location'];?> <br />
                    Ticket Type: <?php echo @$option['formOfTicket']; ?> <br />
                    Restrictions: <?php echo !empty($option['restrictions'])?$option['restrictions']: 'No restrictions'; ?>
                </td>
            </tr>
            </tbody>
        </table>
        <?php
        $output = ob_get_contents();
        ob_end_clean();
        return $output;
    }

}
