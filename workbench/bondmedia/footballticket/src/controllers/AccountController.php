<?php
class AccountController extends BaseController {

    public function index() {

    }

    public function ticketListing() {
        $node = new stdClass();
        $node->title = 'My Account';
        View::share('body_class', 'ticket-listing static');
        View::share('tabs', 'listing');
        View::share('node', $node);
        $customer = Session::get('customer');
        $events = RelatedTicket::getEventsByUser($customer['entity_id']);
        foreach($events as $event)
        {
            if(empty($event->feature_image))
            {
                $team = "";
                if($event->event_in_home)
                {
                    $team = $event->home_team_id;
                }
                else
                {
                    $team = $event->away_team_id;
                }
                $team = FootballTickets::find($team);
                if(!empty($team))
                {
                    $event->feature_image = $team->venue_splash;
                }
            }
            if(empty($event->ticket_type_ids)||$event->ticket_type_ids == '')
            {
                $ev = FootBallEvent::where('id', $event->id)->first();
                $ttypes = $ev->getTicketTypes();
                $ttstr = "";
                foreach($ttypes as $type)
                {
                    $ttstr.=$type->id.',';
                }

                $event->ticket_type_ids = $ttstr;
            }
        }
        View::share('events', $events);
        $formOfTicket = FormOfTicket::where('status', '=', 1)->get();
        foreach($formOfTicket as $f)
        {
            $f->title = trans('homepage.'.$f->title);
        }
        $ticketType = TicketType::where('status', '=', 1)->orderBy('sort_order','ASC')->get();

        View::share('formOfTicket', $formOfTicket->toArray());
        View::share('ticketType', $ticketType->toArray());

        return View::make(Template::name('frontend.%s.account'));
    }

    public function ticketPurchase() {
        $node = new stdClass();
        $node->title = 'My Account';
        View::share('body_class', 'ticket-purchase static');
        View::share('tabs', 'purchases');
        View::share('node', $node);

        $locale = App::getLocale();
        $conv = DB::table('locales')
                    ->where('lang', $locale)
                    ->first();
        $conv = $conv->conversion;

        $customer = Session::get('customer');
        $purchasedTickets = FootballTicketBuy::getTicketsByUserId($customer['entity_id']);
        $puchasedTicketOutput = array();
        $i = 0;
        if( $purchasedTickets ) {
            foreach($purchasedTickets as $pt) 
            {
                $puchasedTicketOutput[$i] = $pt;
                $puchasedTicketOutput[$i]->event = FootBallEvent::find($pt->event_id);
                $puchasedTicketOutput[$i]->ticket = json_decode($pt->ticket, true);
                if(isset($pt->agent_discount_amount) && $pt->agent_discount_amount != null)
                {
                    $puchasedTicketOutput[$i]->ticket['ticketInformation']['price'] = ($puchasedTicketOutput[$i]->ticket['ticketInformation']['price'] - $pt->agent_discount_amount) * $conv;
                }
                else
                {
                    $puchasedTicketOutput[$i]->ticket['ticketInformation']['price'] = $puchasedTicketOutput[$i]->ticket['ticketInformation']['price'] * $conv;
                }

                $ticketType = $puchasedTicketOutput[$i]->event->getTicketTypes();

                foreach($ticketType as $tt) {
                    if($tt->id == $puchasedTicketOutput[$i]->ticket['ticketInformation']['ticket_type']) {
                        $puchasedTicketOutput[$i]->ticketType = $tt->title;
                    }
                }

                $puchasedTicketOutput[$i]->delivery_action = "No Actions Available";

                $mOrder = Mage::getModel('sales/order')->loadByIncrementId($pt->order_id);
                $tracking = RoyalMailTracking::where('order_id','=',$pt->order_id)->first();
                if($pt->ticket['ticketInformation']['form_of_ticket'] == '21')
                {
                    $eticket = ETicket::where('order_id', '=', $pt->order_id)->where('available', '=', 1)->first();

                    if(!empty($eticket))
                    {
                        $puchasedTicketOutput[$i]->delivery_action = "Download E-Ticket(s)";
                        $puchasedTicketOutput[$i]->delivery_flag = 1;
                    }
                }
                else if($mOrder->getShippingMethod() == "flatrate13_flatrate13")
                {        
                    $point = CollectionPoint::where('order_id','=', $pt->order_id)->first();
                    if(!empty($point))
                    {
                        $puchasedTicketOutput[$i]->delivery_action = "View Collection Point";
                        $puchasedTicketOutput[$i]->delivery_flag = 1;
                    }
                }
                else if(!empty($tracking))
                {
                    $puchasedTicketOutput[$i]->delivery_action = "Tracking Code: ". $tracking->tracking_code;
                }
                $i++;

            }
        }
        View::share('purchasedTickets', $puchasedTicketOutput);
        //View::share('events', $events);
        return View::make(Template::name('frontend.%s.purchases'));
    }

    public function getDeliveryAction($oId)
    {
        $customer = Session::get('customer');
        $cId = $customer['entity_id'];

        $valid_access = FootballTicketBuy::getBuyerSellerIdByOrderId($oId);
        $webUser = WebUser::where('user_id', '=', $cId)->first();
        if($valid_access[0]->buyer_id == $customer['entity_id'])
        {
            $formOfTicket = FootballTicketBuy::getTicketTypeByOrderId($oId);
            $mOrder = Mage::getModel('sales/order')->loadByIncrementId($oId);
            if($formOfTicket == 21)
            {
                $eticket = ETicket::where('order_id', '=', $oId)->where('available', '=', 1)->first();
                if(!empty($eticket))
                {
                    $tickets = ETicket::where('order_id','=',$oId)->where('available', '=', 1)->get();
                    return View::make('footballticket::frontend.account.purchases.eticket-links', ['order_id'=>$oId, 'tickets'=>$tickets]);
                }
            }
            else if($mOrder->getShippingMethod() == "flatrate13_flatrate13")
            {        
                $point = CollectionPoint::where('order_id','=', $oId)->first();
                if(!empty($point))
                {
                    return View::make('footballticket::frontend.account.purchases.collection-info', ['order_id'=>$oId, 'point'=>$point]);
                }
            }
            else
            {
                return Response::json([0=>'No available actions'],404);
            }
        }
        else
        {
            return Response::json([0=>'Invalid Access'], 404);
        }
    }

    public function ticketSales() {
        $node = new stdClass();
        $node->title = 'My Account';
        View::share('body_class', 'ticket-sales static');
        View::share('tabs', 'sales');
        View::share('node', $node);

        $locale = App::getLocale();
        $conv = DB::table('locales')
                    ->where('lang', $locale)
                    ->first();
        $conv = $conv->conversion;

        $customer = Session::get('customer');
        $soldTickets = FootballTicketBuy::getSoldTicketsBySellerId($customer['entity_id']);
        $soldGBE = FootballTicketBuy::getAccountSalesViewByUserID($customer['entity_id']);
        $now = time();
        $puchasedTicketOutput = array();
        $i = 0;
        if( $soldGBE ) {
            foreach($soldGBE as $pt) {
                $od = new DateTime($pt->orderDate);
                $may = new DateTime("2015/05/31 11:59:59");

                if((empty($pt->feature_image))||($pt->feature_image == ""))
                {
                    if($pt->isHome)
                    {
                        $pt->feature_image = $pt->home_splash;
                    }
                    else
                    {
                        $pt->feature_image = $pt->away_splash;
                    }
                }


                $puchasedTicketOutput[$i]  = $pt;

                if($now < strtotime($puchasedTicketOutput[$i]->datetime))
                {
                    $puchasedTicketOutput[$i]->isFuture = "future";
                }
                else
                {
                    $puchasedTicketOutput[$i]->isFuture = "past";
                }

                $i++;
            }
        }
        View::share('soldTickets', $puchasedTicketOutput);
        View::share('deliveryStatus', ['pending','in-transit','delivered','cancelled']);

        return View::make(Template::name('frontend.%s.sales'));
    }

    public function getCustomerSalesByEventId($id)
    {
        $locale = App::getLocale();
        $conv = DB::table('locales')
                    ->where('lang', $locale)
                    ->first();
        $conv = $conv->conversion;

        $customer = Session::get('customer');
        $cId = $customer['entity_id'];

        $orders = FootballTicketBuy::getSalesByEventAndUserId($cId, $id);

        foreach($orders as $order)
        {
            $ticket = json_decode($order->ticket);

            $order->block = $ticket->ticketInformation->loc_block;
            $order->row = $ticket->ticketInformation->loc_row;
            $order->ticket_type = $ticket->ticketInformation->ticket_type;
            $order->form_of_ticket = $ticket->ticketInformation->form_of_ticket;

            $typestr = DB::table('events_ticket_type')
                    ->where('id', '=', $order->ticket_type)
                    ->first();
            $order->ticket_type = $typestr->title;

            $typestr = "";

            $typestr = DB::table('events_form_of_ticket')
                    ->where('id', '=', $order->form_of_ticket)
                    ->first();
            $fID = $order->form_of_ticket;
            $order->form_of_ticket = $typestr->title;
            if($fID == 21)
            {
                $eTicket = ETicket::where('order_id', '=', $order->order_id)->first();
                if($eTicket)
                {
                    $order->tcode = "View e-ticket(s)";
                }
                else
                {
                    if($order->tcode != ""){$order->tcode.="<br>";}
                    $order->tcode .= "Upload e-ticket(s)";
                }
            }
            else
            {
                $mOrder = Mage::getModel('sales/order')->loadByIncrementId($order->order_id);
                if($mOrder->getShippingMethod() == "flatrate13_flatrate13")
                {
                    $point = CollectionPoint::where('order_id','=', $order->order_id)->first();
                    if(!empty($point))
                    {
                        $order->tcode = "View Collection Point";
                    }
                    else
                    {
                       $order->tcode = "Specify Collection Point"; 
                    }
                }
            }

            $order->ticket = "";
            
            $order->listed_amount = number_format((($order->listed_amount * 0.85)*$conv)*$order->qty, 2, '.', ',');
            if($order->tcode == "")
            {
                $order->tcode = "Send Now";
            }
        }
        return Response::json($orders, 200);
    }

    public function getDispatchViewByOrderID($oId)
    {
        $customer = Session::get('customer');
        $cId = $customer['entity_id'];

        $valid_access = FootballTicketBuy::getBuyerSellerIdByOrderId($oId);
        $webUser = WebUser::where('user_id', '=', $cId)->first();
        if($valid_access[0]->user_id == $customer['entity_id'])
        {
            $formOfTicket = FootballTicketBuy::getTicketTypeByOrderId($oId);
            if($formOfTicket == 21)
            {
                $tickets = ETicket::where('order_id', '=', $oId)->get();
                return View::make('footballticket::frontend.account.sale.eticket-upload', ['order_id'=>$oId, 'tickets'=>$tickets]);
            }
            else
            {
                //get vars to pass into this view
                $mOrder = Mage::getModel('sales/order')->loadByIncrementId($oId);
                if($mOrder->getShippingMethod() == "flatrate13_flatrate13")
                {
                    $point = CollectionPoint::where('order_id', '=', $oId)->first();
                    return View::make('footballticket::frontend.account.sale.collection-input', ['order_id'=>$oId, 'point'=>$point]);
                }
                else
                {
                    $eandp = FootballTicketBuy::getEventAndProductIdByOrderId($oId);
                    return View::make('footballticket::frontend.account.sale.tracking-update', ['order_id'=>$oId, 'event_id'=>$eandp->event_id, 'product_id'=>$eandp->product_id, 'tcode'=>$eandp->tcode]);
                }
                
            }
        }
        else
        {
            return Response::json([0=>'You do not have access to this resource'], 404);
        }
    }

    public function saveCollectionPointForId($oId)
    {
        $customer = Session::get('customer');
        $cId = $customer['entity_id'];

        $valid_access = FootballTicketBuy::getBuyerSellerIdByOrderId($oId);
        $webUser = WebUser::where('user_id', '=', $cId)->first();
        if($valid_access[0]->user_id == $customer['entity_id'])
        {

            $mOrder = Mage::getModel('sales/order')->loadByIncrementId($oId);
            if($mOrder->getShippingMethod() == "flatrate13_flatrate13")
            {
                $etb = FootballTicketBuy::where('order_id', '=', $oId)->first();
                if($etb->delivery_status == "pending")
                {
                    
                    $input = Input::all();
                    $validator = Validator::make(
                        // Value for reqfile;
                        array(
                            'datetime' => $input['collection_date'],
                            'l1' => $input['location_1'],
                            'l2' => $input['location_2'],
                            'l3' => $input['location_3'],
                            'postcode' => $input['postcode']
                        ),
                        // Validator rule for reqfile
                        array(
                            'datetime' => 'required',
                            'l1' => 'required',
                            'l2' => 'required',
                            'l3' => 'required',
                            'postcode' => 'required'
                        )
                    );
                    if(!$validator->fails())
                    {
                        $collection = new CollectionPoint;
                        $collection->collection_date = $input['collection_date'];
                        $collection->order_id = $oId;
                        $collection->location_1 = $input['location_1'];
                        $collection->location_2 = $input['location_2'];
                        $collection->location_3 = $input['location_3'];
                        $collection->postcode = $input['postcode'];
                        $collection->save();

                        $etb->delivery_status = "in-transit";
                        $etb->save();

                        $ert = RelatedTicket::where('product_id','=',$etb->product_id)->first();
                        $event = FootBallEvent::where('id','=',$ert->event_id)->first();

                        //Send Mail to user here, cc in us and broker. 
                        $buyerDetails = WebUser::where('user_id', '=', $etb->buyer_id)->first();
                        $toEmail = array();
                        $toEmail['collection']=$collection;
                        $toEmail['event']=$event;
                        $toEmail['customer_name']=$buyerDetails->firstname." ".$buyerDetails->lastname;

                        Mail::send('footballticket::email.collection-point-dispatch', $toEmail, function ($message) use($buyerDetails) {
                            $from = Config::get('mail.from');
                            $from['address'] = trans('homepage.contact-email');
                            $message->from($from['address'], $from['name']);
                            $email = 'info@footballticketpad.com';

                            $message->to($buyerDetails->email, $buyerDetails->firstname." ".$buyerDetails->lastname)
                                ->bcc($email, 'Football Ticket Pad')
                                ->subject("Collection point updated.");

                            if(trim($email) != 'info@footballticketpad.com' ) {
                                $message->bcc('info@footballticketpad.com', 'Football Ticket Pads');
                            }
                        });

                        return Response::json([0=>'Success'], 200);
                    }
                    else
                    {
                        return Response::json([0=>'Invalid data'], 404);
                    }
                }
                else
                {   
                    return Response::json([0=>'Cannot set collection point for order if status isn\'t pending'], 404);
                }   
            }
            else
            {
                return Response::json([0=>'Cannot set collection of none hand delivery order.'], 404);
            } 
            
        }
        else
        {
            return Response::json([0=>'You do not have access to this resource'], 404);
        }
    }

    public function accountInformation() {
        $node = new stdClass();
        $node->title = 'My Account';
        $countryList = CountryList::getCountryListWithCountryCode();
        $country = [];
        foreach($countryList as $c) {
            $country[$c->Code] = $c->Country;
        }

        View::share('country', $country);
        View::share('body_class', 'ticket-account-information static');
        View::share('tabs', 'account-information');
        View::share('node', $node);
        return View::make(Template::name('frontend.%s.account-information'));
    }

    public function address() {
        $node = new stdClass();
        $node->title = 'Shipping Address';
        $countryList = CountryList::getCountryListWithCountryCode();
        $country = [];
        foreach($countryList as $c) {
            $country[$c->Code] = $c->Country;
        }
        View::share('country', $country);
        View::share('body_class', 'ticket-account-address static');
        View::share('tabs', 'addresses');
        View::share('node', $node);
        return View::make(Template::name('frontend.%s.addresses'));
    }

    public function getCustomerBankInfo() {
        $customer = Session::get('customer');
        $this->setDataToPost(array(
            'customer_id'   => $customer['entity_id'],
            'type'          => 'get'
        ));
        $this->submitPostToApi('customer/index/bank');
        $this->setApiResponseHeader();
        echo $this->getApiResponse();
    }

    public function getCustomerAllCardInfo() {
        $customer = Session::get('customer');
        $this->setDataToPost(array(
            'customer_id'   => $customer['entity_id'],
            'type'          => 'get',
            'return'        => 'all'
        ));
        $this->submitPostToApi('customer/index/card');
        $this->setApiResponseHeader();
        echo $this->getApiResponse();
    }

    public function setCustomerBankInfo() {
        $customer = Session::get('customer');
        $params = Input::all();
        $params['customer_id']  = $customer['entity_id'];
        $params['type']         = 'set';
        $this->setDataToPost($params);
        $this->submitPostToApi('customer/index/bank');
        $this->setApiResponseHeader();
        echo $this->getApiResponse();
    }


    public function getCustomerCardInfo() {
        $customer = Session::get('customer');
        $this->setDataToPost(array(
            'customer_id'   => $customer['entity_id'],
            'type'          => 'get'
        ));
        $this->submitPostToApi('customer/index/card');
        $this->setApiResponseHeader();
        echo $this->getApiResponse();
    }

    public function setCustomerCardInfo() {
        $customer = Session::get('customer');
        $params = Input::all();
        $params['customer_id']  = $customer['entity_id'];
        $params['type']         = 'set';
        $this->setDataToPost($params);
        $this->submitPostToApi('customer/index/card');
        $this->setApiResponseHeader();
        echo $this->getApiResponse();
    }

    public function getCustomerInfo() {
        $customer = Session::get('customer');
        $this->setDataToPost(array(
            'customer_id'   => $customer['entity_id'],
            'type'          => 'get'
        ));

        $this->submitPostToApi('customer/index/personal');
        $this->setApiResponseHeader();
        $response = json_decode($this->getApiResponse(), true);
        $data = $response['data'];
        $webUser = WebUser::where('user_id', '=', $customer['entity_id'])->first();
        $data['subscribed'] = (int) $webUser->newsletter;
        $data['phone'] = $webUser->telephone;
        $response['data'] = $data;
        echo json_encode($response);
        //echo
    }

    public function setCustomerInfo() {
        $customer = Session::get('customer');
        $params = Input::all();
        $params['customer_id']  = $customer['entity_id'];
        $params['type']         = 'set';
        $email = $params['email'];
        unset($params['email']);
        $this->setDataToPost($params);
        $this->submitPostToApi('customer/index/personal');
        $this->setApiResponseHeader();

        $customer = json_decode($this->getApiResponse(),true);

        $webUser = WebUser::where('user_id', '=', $params['customer_id'])->first();
        if(!$webUser) {
            $webUser = new WebUser();
            $webUser->user_id = $params['customer_id'];
            $webUser->email = $email;
        }
        if($webUser) {

            $webUser->firstname = $params['firstname'];
            $webUser->lastname = $params['lastname'];
            $webUser->telephone = $params['phone'];
            $webUser->newsletter = Input::get('subscribed');
            if(Input::get('subscribed') == 1) {
                try {
                    MailchimpWrapper::lists()->subscribe('071c5abfb5', array('email'=> $email));
                } catch (Exception $e) {
                    // do nothing
                }
            } else {
                try {
                    MailchimpWrapper::lists()->unsubscribe('071c5abfb5',$email);
                } catch (Exception $e) {
                    // do nothing
                }

            }
            $webUser->save();
        }

        echo json_encode(array('data'=>$params));
    }

    public function getCustomerBillingAddress() {
        $customer = Session::get('customer');
        $this->setDataToPost(array(
            'customer_id'   => $customer['entity_id'],
            'type'          => 'get',
            'address_type'  => 'billing'
        ));
        $this->submitPostToApi('customer/index/address');
        $this->setApiResponseHeader();
        echo $this->getApiResponse();
    }

    public function setCustomerBillingAddress() {
        $customer = Session::get('customer');
        $params = Input::all();
        $params['customer_id']  = $customer['entity_id'];
        $params['type']         = 'set';
        $params['address_type'] = 'billing';

        $this->setDataToPost($params);
        $this->submitPostToApi('customer/index/address');
        $this->setApiResponseHeader();
        echo $this->getApiResponse();
    }

    public function getCustomerShippingAddress() {
        $customer = Session::get('customer');
        $this->setDataToPost(array(
            'customer_id'   => $customer['entity_id'],
            'type'          => 'get',
            'address_type'  => 'shipping'
        ));
        $this->submitPostToApi('customer/index/address');
        $this->setApiResponseHeader();
        echo $this->getApiResponse();
    }

    public function setCustomerShippingAddress() {
        $customer = Session::get('customer');
        $params = Input::all();
        $params['customer_id']  = $customer['entity_id'];
        $params['type']         = 'set';
        $params['address_type'] = 'shipping';
        
        $this->setDataToPost($params);
        $this->submitPostToApi('customer/index/address');
        $this->setApiResponseHeader();
        echo $this->getApiResponse();
    }

    public function passwordResetRequest() {

        $params = Input::all();
        $notification = '';
        if(isset($params['email'])) {
            $this->setDataToPost($params);
            $this->submitPostToApi('customer/account/passwordchange');
            $response = json_decode($this->getApiResponse(), true);

            if($response['data'] && $response['data'] == 'success') {
                $notification = '<p class="success">Your password has been reset please check your email account for details.</p>';
            } else {

                $m = isset($response['data'])? $response['data']:'Unknown error. Password can rest at the moment.' ;
                $notification = '<p class="error">'.$m.'</p>';
            }

        }
        $node = new stdClass();
        $node->title =trans('homepage.fgPwd');
        View::share('body_class', 'reset-password');
        return View::make(Template::name('frontend.%s.forget-password'), compact('node', 'notification'));
    }

    //get ticket event listing

    public function getEventList($id) {
        $locale = App::getLocale();
        $conv = DB::table('locales')
                    ->where('lang', $locale)
                    ->first();
        $conv=$conv->conversion;
        $bf = 1 + (intval(Options::getOption('booking_fees'))/100);

        $output = array();
        $customer = Session::get('customer');

        $eventTickets = RelatedTicket::where('event_id', '=',$id)
                        ->where('user_id', '=',$customer['entity_id'])
                        ->where('ticket_status', '<>', 'deleted')
                        ->where('ticket_status', '<>', 'blocked')
                        ->get();

        $node = FootBallEvent::find($id);
        $defaultRestriction = [];
        foreach($node->getSelectedRestrictions()  as $val) {

            $defaultRestriction[] = array(
                'id'    => $val->id,
                'title' => $val->title
            );
        }
        foreach($eventTickets as $t) {
            $temp = array();
            $temp['id'] = $t['id'];
            $ti = json_decode($t['ticket'], true);
            $temp['price'] = ($t['price'] - (($t['price'] * $t['selling_commission_percentage']) / 100))*$conv;
            $temp['earning'] = ($t['price'] * $conv)*$bf;
            $temp['listing_id'] = $t['product_id'];
            $temp['block'] = $ti['ticketInformation']['loc_block'];
            $temp['row'] = $ti['ticketInformation']['loc_row'];
            $temp['number_of_ticket'] = $t['available_qty'];
            $temp['form_of_ticket'] = $ti['ticketInformation']['form_of_ticket'];
            $temp['ticket_type'] = $ti['ticketInformation']['ticket_type'];
            $temp['status'] = $t['ticket_status'];
            $temp['sold'] = $ti['ticketInformation']['number_of_ticket'] - $t['available_qty'];
            $temp['ready_to_ship'] = $ti['paymentAgreement']['ticket_ready'];
            $temp['preference'] = $ti['ticketInformation']['sell_preference'];
            $temp['buyer_note'] = isset($ti['ticketInformation']['restrictions'])?$ti['ticketInformation']['restrictions']:'';
            $temp['can_offer_collection'] = $t->hand_delivery;
            foreach($defaultRestriction as $key=>$val) {
                if(isset($temp['buyer_note'][$val['id']])) {
                    $val['checked'] = 'checked';
                } else {
                    $val['checked'] = '';
                } 

                $defaultRestriction[$key] = $val;
            }

            $temp['default_restriction'] = $defaultRestriction;
            $output[] = $temp;
        }
        return Response::json($output, 200);
    }

    public function updateEvent($id) {
        $customer = Session::get('customer');
        $eventTicket = RelatedTicket::where('id', '=',$id)
            ->where('user_id', '=',$customer['entity_id'])->first();


        if(empty($eventTicket)) {
            return Response::json(array('not found!'), 404);
        }

        $input = Input::all();

        if($input['type'] == 'status') {
            $eventTicket->ticket_status = $input['value'];
            $eventTicket->save();
        }
        if($input['type'] == 'hand_delivery'){
            $eventTicket->hand_delivery = $input['value'];
            $eventTicket->save();
        }

        return Response::json('success', 200);
    }

    public function getCustomerAddressByOrderId($orderId) {
        $customer = Session::get('customer');
        $valid_access = FootballTicketBuy::getBuyerSellerIdByOrderId($orderId);
        $webUser = WebUser::where('user_id', '=', $customer['entity_id'])->first();
        if(($valid_access[0]->user_id == $customer['entity_id']) || ($valid_access[0]->buyer_id == $customer['entity_id']))
        {
            try{
                $order = TicketSoap::process('sales_order.info', $orderId);
                $ticket_buy = FootballTicketBuy::where('order_id', '=', $orderId)->first();
                if($webUser->seller_type == "broker")
                {
                    $tUser = WebUser::where('user_id', '=', $valid_access[0]->buyer_id)->first();
                    $order['shipping_address']['tel']=$order['shipping_address']['telephone']?$order['shipping_address']['telephone']:$tUser->telephone;
                    $sm = $order['shipping_method'];

                    $rateName = explode('_', $sm);
                    $rateName = $rateName[0];

                    $ships = DB::table('shipping_method_assoc')
                                ->where('name', '=', $rateName)
                                ->first();
                                
                    $order['shipping_address']['method']=$ships->description;
                }
                $order['shipping_address']['orderId']=$orderId;
                return View::make('footballticket::frontend.account.sale.address', $order['shipping_address']);
            } catch (Exception $e) {
                return Response::json($e->getMessage(), 404);
            }
        }
        else
        {
            return Response::json("You do not have access to this resource.", 404);
        }
    }
    
    public function getSetCustomerAddressByOrderId($orderId) {
        $customer = Session::get('customer');
        $valid_access = FootballTicketBuy::getBuyerSellerIdByOrderId($orderId);
        $webUser = WebUser::where('user_id', '=', $customer['entity_id'])->first();
        if(($valid_access[0]->user_id == $customer['entity_id']) || ($valid_access[0]->buyer_id == $customer['entity_id']))
        {
            try{
                $order = TicketSoap::process('sales_order.info', $orderId);
                $ticket_buy = FootballTicketBuy::where('order_id', '=', $orderId)->first();
                if($webUser->seller_type == "broker")
                {
                    $tUser = WebUser::where('user_id', '=', $valid_access[0]->buyer_id)->first();
                    //$order['shipping_address'].="<br><br>".$tUser->telephone;
                    $order['shipping_address']['tel']=$tUser->telephone;
                    
                }
                $order['shipping_address']['orderId']=$orderId;
                if($ticket_buy && $ticket_buy->delivery_status == "pending") {
                    return View::make('footballticket::frontend.account.sale.addressUpdate',$order['shipping_address']);
                }
                return View::make('footballticket::frontend.account.sale.address', $order['shipping_address']);
            } catch (Exception $e) {
                return Response::json($e->getMessage(), 404);
            }
        }
        else
        {
            return Response::json("You do not have access to this resource.", 404);
        }
    }

    public function updateShipping($orderId)
    {
        $customer = Session::get('customer');
        $valid_access = FootballTicketBuy::getBuyerSellerIdByOrderId($orderId);
        $webUser = WebUser::where('user_id', '=', $customer['entity_id'])->first();
        $webSeller = WebUser::where('user_id', '=', $valid_access[0]->user_id)->first();
        $data = Input::all();
        $address=array( 'orderId' => intval($orderId)-100000000,
                        'fname'=>$data['fname'], 
                        'sname'=>$data['sname'], 
                        'street'=>$data['street'], 
                        'city'=>$data['city'], 
                        'country'=>$data['country'], 
                        'postcode'=>$data['postcode']);

        if($valid_access[0]->buyer_id == $customer['entity_id'])
        {
            $ticket_buy = FootballTicketBuy::where('order_id', '=', $orderId)->first();
            if($ticket_buy && $ticket_buy->delivery_status == 'pending')
            {
                $response = TicketSoap::process('sales_order.update', $address);
                if($response)
                {
                    Mail::send('emails.contact-form.shipping-change', $address, function ($message) use ($webSeller, $orderId) {
                        $message->from(trans('homepage.contact-email'), 'FootballTicketPad');
                        $message->to($webSeller->email, $webSeller->firstname.' '.$webSeller.lastname)->cc('info@footballticketpad.com');
                        $message->subject('Shipping Address Update For '.$orderId);
                    });

                    Mail::send('emails.contact-form.shipping-change', $address, function ($message) use ($webUser, $orderId) {
                        $message->from(trans('homepage.contact-email'), 'FootballTicketPad');
                        $message->to($webUser->email, $webUser->firstname.' '.$webUser->lastname);
                        $message->subject('Shipping Address Update For '.$orderId);
                    });
                    return Redirect::to('/account/purchases');
                }
            }
        }   
        else
        {
            return Response::json("You do not have access to this resource.", 404);
        }     
    }

    public function saveTrackingCode() {
        try{
            $input = Input::all();
            $customer = Session::get('customer');
            $valid_access = FootballTicketBuy::getBuyerSellerIdByOrderId($input['order_id']);
            if($valid_access[0]->user_id == $customer['entity_id'])
            {
                $tracking = RoyalMailTracking::where('event_id', '=', $input['event_id'])
                    ->where('product_id', '=', $input['product_id'])
                    ->where('order_id', '=', $input['order_id'])
                    ->first();
                if(!$tracking) {
                    $tracking = new RoyalMailTracking();
                    $shipmentIncrementId = TicketSoap::process('order_shipment.create', array($input['order_id']));
                    $input['tracking_id_number'] = TicketSoap::process('sales_order_shipment.addTrack', array('shipmentIncrementId' => $shipmentIncrementId, 'carrier' => 'custom', 'title' => 'Royal Mail tracking ID', 'trackNumber' => $input['tracking_code']));
                    TicketSoap::process('sales_order_shipment.addComment', array('shipmentIncrementId' => $shipmentIncrementId, 'comment' => 'ticket has been dispatched', 'email' => false ));

                    $ticket_buy = FootballTicketBuy::where('order_id', '=', $input['order_id'])->first();
                    if($ticket_buy) {
                        $ticket_buy->delivery_status = 'in-transit';
                        $ticket_buy->save();
                    }

                    

                    $soldTicket = FootballTicketBuy::where('order_id', '=', $input['order_id'])->first();
                    $tracking->event_id = @$input['event_id'];
                    $tracking->product_id = @$input['product_id'];
                    $tracking->order_id = @$input['order_id'];
                    $tracking->tracking_id_number = @$input['tracking_id_number'];
                    $tracking->tracking_code = @$input['tracking_code'];
                    $tracking->save();

                    $event = FootBallEvent::where('id', '=', $input['event_id'])->first();

                    if($soldTicket->buyer_id) {
                        $buyerDetails = WebUser::where('user_id', '=', $soldTicket->buyer_id)->first();
                        $formData = array('customer_name'=> $buyerDetails->firstname." ".$buyerDetails->lastname, 'tracking_code'=> $input['tracking_code'], 'event'=>$event, 'order'=>$soldTicket);
                        Mail::send('footballticket::email.tracking-email', $formData, function ($message) use($buyerDetails) {
                            $from = Config::get('mail.from');
                            $from['address'] = trans('homepage.contact-email');
                            $message->from($from['address'], $from['name']);
                            $email = 'info@footballticketpad.com';

                            $message->to($buyerDetails->email, $buyerDetails->firstname." ".$buyerDetails->lastname)
                                ->bcc($email, 'Football Ticket Pads')
                                ->subject("Football ticket dispatched.");

                            if(trim($email) != 'info@footballticketpad.com' ) {
                                $message->bcc('info@footballticketpad.com', 'Football Ticket Pads');
                            }
                        });
                    }

                }
                return Response::json('success', 200);
            }
            else
            {
                return Response::json("You do not have access to this resource.", 404);
            }
        } catch (Exception $e) {
            return Response::json($e->getMessage(), 404);
        }
    }

    public function getSimilarListingsByListingID($id)
    {
        $customer = Session::get('customer');
        $ticket = RelatedTicket::find($id);

        $locale = App::getLocale();

        $conv = DB::table('locales')
                    ->where('lang', $locale)
                    ->first();
        $conv=$conv->conversion;

        if(!$ticket)
        {
            return Response::json("Could not find ticket for this ID", 404);
        }

        if($ticket->user_id != $customer['entity_id'])
        {
            return Response::json('Access Denied.', 404);
        }
        $tt = json_decode($ticket->ticket);

        $res = DB::table("events_related_tickets")
            ->select(
                DB::raw(
                    'price, '.
                    'if(user_id = '.$ticket->user_id.', id, 0) as id, '.
                    'user_id = '.$ticket->user_id.' as isUsers, '.
                    'ticket, '.
                    'available_qty'
                )
            )
            ->where('event_id', '=', $tt->ticketInformation->event_id)
            ->whereRaw('ticket LIKE \'%"ticket_type":"'.$tt->ticketInformation->ticket_type.'"%\'')
            ->where('ticket_status', '=', 'active')
            ->where('available_qty', '>', 0)
            ->orderBy('price', 'ASC')
            ->get();
        foreach($res as $r)
        {
            $t = json_decode($r->ticket);
            $r->loc_block = $t->ticketInformation->loc_block;
            $r->loc_row = $t->ticketInformation->loc_row;
            $r->pref = $t->ticketInformation->sell_preference;

            $r->ticket = "";
            $r->price = ($r->price*0.85)*$conv;
        }

        $typestr = DB::table('events_ticket_type')
                    ->where('id', '=', $tt->ticketInformation->ticket_type)
                    ->first();
        if($typestr)
        {
            foreach($res as $r)
            {
                $r->ticket_type = $typestr->title;
            }  
        }
        else
        {
            foreach($res as $r)
            {
                $r->ticket_type = "Could not correlate type information.";
            }  
        }
        return Response::json($res, 200);
    }

    public function getAllListingsByListingID($id)
    {
        $customer = Session::get('customer');
        $ticket = RelatedTicket::find($id);

        $locale = App::getLocale();

        $conv = DB::table('locales')
                    ->where('lang', $locale)
                    ->first();
        $conv=$conv->conversion;

        if(!$ticket)
        {
            return Response::json("Could not find ticket for this ID", 404);
        }

        if($ticket->user_id != $customer['entity_id'])
        {
            return Response::json('Access Denied.', 404);
        }

        $tt = json_decode($ticket->ticket);
        
        $res = DB::table("events_related_tickets")
                    ->select(
                        DB::raw(
                            'price, '.
                            'if(user_id = '.$ticket->user_id.', id, 0) as id, '.
                            'user_id = '.$ticket->user_id.' as isUsers, '.
                            'ticket, '.
                            'available_qty'
                        )
                    )
                    ->where('event_id', '=', $tt->ticketInformation->event_id)
                    ->where('ticket_status', '=', 'active')
                    ->where('available_qty', '>', 0)
                    ->orderBy('price', 'ASC')
                    ->get();

        foreach($res as $r)
        {
            $t = json_decode($r->ticket);
            $r->loc_block = $t->ticketInformation->loc_block;
            $r->loc_row = $t->ticketInformation->loc_row;
            $r->pref = $t->ticketInformation->sell_preference;

            $typestr = DB::table('events_ticket_type')
                    ->where('id', '=', $t->ticketInformation->ticket_type)
                    ->first();
            if($typestr->title)
            {
                $r->ticket_type = $typestr->title;
            }
            else
            {
                $r->ticket_type = "Could not correlate ticket type information";
            }

            $r->ticket = "";

            $r->price = ($r->price*0.85)*$conv;
        } 
        
        return Response::json($res, 200);
    }


}

