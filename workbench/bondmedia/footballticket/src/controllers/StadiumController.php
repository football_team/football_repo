<?php
/**
 * Created by PhpStorm.
 * User: baglad
 * Date: 27/05/2016
 * Time: 16:57
 */
class StadiumController extends BaseController
{
    public function adminIndex()
    {
        View::share('menu', 'stadium');
        return View::make('footballticket::admin.footballticket.admin-stadium-index');
    }

    public function adminGetList()
    {
        $start = 0;
        $length = 10;
        $search = "";

        if(Input::has('start'))
        {
            $start = Input::get('start');
        }

        if(Input::has('length'))
        {
            $length = Input::get('length');
        }

        if(Input::has('search'))
        {
            $sN = Input::get('search');

            if(array_key_exists('value', $sN))
            {
                $search = $sN['value'];
            }
        }

        $stadiums = Stadium::where('title', 'LIKE', $search.'%')->skip($start)->take($length)->get();

        $output = [];

        $total = DB::table('stadium')
                    ->select(DB::raw('COUNT(*) as ttl'))
                    ->first();

        $total = $total->ttl;

        foreach($stadiums as $stadium)
        {
            $temp = [];
            $country = DB::table('country_list')
                            ->where('Sort Order', $stadium->country_id)
                            ->first();

            $temp['0'] = $stadium->id;
            $temp['1'] = $stadium->title;
            $temp['2'] = $country->{'Common Name'};
            $temp['3'] = '<a href="/admin/stadiums/edit/'.$stadium->id.'">Edit</a>';
            $temp['4'] = '<button class="del-btn" data-url="/admin/stadiums/delete/'.$stadium->id.'">Delete</button>';
            $output[] = $temp;

        }

        $results['data'] = $output;
        $results['recordsTotal'] = $total;
        $results['recordsFiltered'] = $total;
        $response = Response::make($results);
        $response->header('Content-Type', 'application/json');
        return $response;
    }

    public function adminEdit($id)
    {
        //get stadium return view
        $stadium = Stadium::where('id', $id)->first();
        if($stadium)
        {
            $share = array();
            $countries = DB::table('country_list')->select(['Sort Order','Common Name'])->orderBy('Sort Order', 'ASC')->get();

            $maps = Map::all();
            View::share('listOfMaps', $maps);

            foreach($countries as $country)
            {
                $temp = array();
                $temp[0]=$country->{'Sort Order'};
                $temp[1]=$country->{'Common Name'};
                $share[] = $temp;

            }
            View::share('countries', $share);
            View::share('stadium', $stadium);
            $ttypes = TicketType::where('status',1)->orderBy('sort_order','ASC')->get();
            if(!empty($stadium->ticket_type_ids)&&($stadium->ticket_type_ids != ''))
            {
                $ArrTypes = explode(',', $stadium->ticket_type_ids);
                $types = new TicketType();
                $ttypesSel = $types->ticketByIds($ArrTypes);

                $ttypes = TicketType::where('status',1)->orderBy('sort_order', 'ASC')->get();


                $tArray = array();
                foreach($ttypesSel as $ttype)
                {
                    $tArray[] = $ttype->id;
                }

                foreach($ttypes as $ttype)
                {
                    if(in_array($ttype->id, $tArray))
                    {
                        $ttype->selected = true;
                    }
                    else
                    {
                        $ttype->selected = false;
                    }
                }
            }
            View::share('ttypes', $ttypes);
            View::share('menu', 'stadium/edit');
            return View::make('footballticket::admin.footballticket.admin-stadium-edit');
        }
    }

    public function adminUpdate($id)
    {
        $input = Input::all();

        $validator = Validator::make(
            $input,
            array(
                'country_id'=>'required|integer',
            )
        );

        if(!$validator->fails()) {
            $stadium = Stadium::where('id', $id)->first();
            if($stadium)
            {
                $stadium->country_id = $input['country_id'];

                if(array_key_exists ('title', $input))
                {
                    $stadium->title = $input['title'];
                }

                if(array_key_exists ('al1', $input))
                {
                    $stadium->al1 = $input['al1'];
                }
                if(array_key_exists ('al2', $input))
                {
                    $stadium->al2 = $input['al2'];
                }
                if(array_key_exists ('postcode', $input))
                {
                    $stadium->postcode = $input['postcode'];
                }
                if(array_key_exists ('svgmap', $input))
                {
                    $stadium->svg_map = $input['svgmap'];
                }
                if(array_key_exists ('map_image', $input))
                {
                    $stadium->map_image = $input['map_image'];
                }

                if(array_key_exists('ttypes', $input))
                {

                    $ttypes = $input['ttypes'];
                    $ttypeStr = '';
                    foreach($ttypes as $ttype)
                    {
                        $ttypeStr.=$ttype.',';
                    }

                    $stadium->ticket_type_ids = $ttypeStr;
                }

                $stadium->save();

                return Redirect::to('/admin/stadiums/');
            }
        }
    }

    public function adminCreate()
    {
        $countries = DB::table('country_list')->select(['Sort Order','Common Name'])->orderBy('Sort Order', 'ASC')->get();

        foreach($countries as $country)
        {
            $temp = array();
            $temp[0]=$country->{'Sort Order'};
            $temp[1]=$country->{'Common Name'};
            $share[] = $temp;

        }
        View::share('countries', $share);

        $maps = Map::all();
        View::share('listOfMaps', $maps);

        View::share('menu', 'stadium/create');
        return View::make('footballticket::admin.footballticket.admin-stadium-create');
    }

    public function adminSave()
    {
        if(Input::has('title') && Input::has('country_id'))
        {
            $input = Input::all();

            $stadium = new Stadium;

            $stadium->title = $input['title'];
            $stadium->country_id = $input['country_id'];

            if(array_key_exists ('al1', $input))
            {
                $stadium->al1 = $input['al1'];
            }
            if(array_key_exists ('al2', $input))
            {
                $stadium->al2 = $input['al2'];
            }
            if(array_key_exists ('postcode', $input))
            {
                $stadium->postcode = $input['postcode'];
            }
            if(array_key_exists ('svgmap', $input))
            {
                $stadium->svg_map = $input['svgmap'];
            }
            if(array_key_exists ('map_image', $input))
            {
                $stadium->map_image = $input['map_image'];
            }

            $stadium->save();
            return Redirect::to('/admin/stadiums/');

        }
    }



    public function adminDelete($id)
    {
        DB::table('team_stadium')
                ->where('stadium_id', $id)
                ->delete();

        DB::table('event_stadium')
                ->where('stadium_id', $id)
                ->delete();

        DB::table('stadium_categories')
                ->where('stadium_id', $id)
                ->delete();

        DB::table('stadium')
                ->where('id', $id)
                ->delete();

        return Redirect::to('/admin/stadiums/');
    }

    public function teamChangeStadium()
    {
        //Do nothing for now
    }

    public function eventChangeStadium()
    {
        //Do nothing for now
    }
}