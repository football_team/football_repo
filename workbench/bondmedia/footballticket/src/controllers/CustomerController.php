<?php

class CustomerController extends BaseController
{

    public function __construct()
    {

    }

    /**
     * Register customer
     */
    protected function registrationAction()
    {

            $rules     = array(
                'first_name'            => 'Required|Min:2|Max:60',
                'last_name'             => 'Required|Min:2|Max:60',
                'email'                 => 'Required|Between:3,64|Email|Confirmed',
                'email_confirmation'    => 'Required|Between:3,64|Email',
                'country_code'          => 'Min:3',
                'contact_no'            => 'AlphaNum|Min:8',
                'password'              => 'Required|Between:4,16|Confirmed',
                'password_confirmation' => 'Required|Between:4,16',
                'terms_n_conditions'    => 'Required|Max:1',
                'newsletters'           => 'Max:1',
                'agent_code' => 'exists:agents,agent_code,status,active'
            );
            $input     = Input::all();
            $validator = Validator::make($input, $rules);

            if ($validator->fails()) {
                $messages = $validator->messages();
                $response = Response::make(json_encode($messages), '400');
                $response->header('Content-Type', 'application/json');
                return $response;
            }

            try {
                $result = TicketSoap::process('customer.create', [
                    ['email'      => strip_tags(trim($input['email'])),
                     'firstname'  => strip_tags(trim($input['first_name'])),
                     'lastname'   => strip_tags(trim($input['last_name'])),
                     'password'   => strip_tags(trim($input['password'])),
                     'website_id' => 1,
                     'store_id'   => 1,
                     'group_id'   => 1
                    ]
                ]);



            } catch (Exception $e) {
                $response = Response::make(json_encode(['error' => $e->getMessage()]), '400');
                $response->header('Content-Type', 'application/json');
                return $response;
            }
            try {
                if (is_numeric($result)) {

                    $customerId = $result;
                    $user = new WebUser();
                    $user->user_id = $customerId;
                    $user->seller_type = 'customer';
                    $user->updated_by = -1;
                    $user->status = 'active';
                    $user->firstname = $input['first_name'];
                    $user->lastname = $input['last_name'];
                    $user->telephone = $input['contact_no'];
                    $user->email = $input['email'];

                    if(Input::get('newsletters') == 1) {
                        try {
                            $user->newsletter = Input::get('newsletters');
                            MailchimpWrapper::lists()->subscribe('071c5abfb5', array('email'=> $input['email']));
                        } catch (Exception $e) {
                            // do nothing
                        }
                    }

                    if(Input::has('agent_code'))
                    {
                        Log::info("IN HERE");
                        Log::info($input['agent_code']);
                        $agent = Agent::where('agent_code', $input['agent_code'])->first();
                        Log::info(var_export($agent,true));
                        $user->ref_agent = $agent->id;
                    }

                    $user->save();

                    $result     = TicketSoap::process('customer_address.create', [
                        'customerId'  => $customerId,
                        'addressdata' => ['firstname'           => $input['first_name'],
                                          'lastname'            => $input['last_name'],
                                          'telephone'           => $input['contact_no'],
                                          'email'               => $input['email'],
                                          'street'              => array('not found'),
                                          'city'                => 'not found',
                                          'postcode'            => 'not found',
                                          'country_id'          => 'US',
                                          'is_default_billing'  => false,
                                          'is_default_shipping' => false
                        ]
                    ]);




                    if(isset($input['email'])) {
                        $this->setDataToPost($input);
                        $this->submitPostToApi('customer/account/send_confirmation_email');
                    }


                    $response   = Response::make(json_encode(['message' => 'user successfully created.', 'addressId' => $result, 'userId'=>$customerId]), '200');
                    $response->header('Content-Type', 'application/json');
                    return $response;
                } else {
                    $response = Response::make(json_encode($result), '400');
                    $response->header('Content-Type', 'application/json');
                    return $response;
                }
            } catch (Exception $e) {
                $response = Response::make(json_encode(['error' => 'Address: ' . $e->getMessage()]), '400');
                $response->header('Content-Type', 'application/json');
                return $response;
            }


//        } else {
//            $response = Response::make('Invalid Request', '406');
//            $response->header('Content-Type', 'application/json');
//            return $response;
//        }
    }

    public static function registrationInternalAction($input)
    {

        $rules     = array(
            'first_name'            => 'Required|Min:2|Max:60',
            'last_name'             => 'Required|Min:2|Max:60',
            'email'                 => 'Required|Between:3,64|Email|Confirmed',
            'email_confirmation'    => 'Required|Between:3,64|Email',
            'country_code'          => 'Min:3',
            'contact_no'            => 'AlphaNum|Min:8',
            'password'              => 'Required|Between:4,16|Confirmed',
            'password_confirmation' => 'Required|Between:4,16',
            'terms_n_conditions'    => 'Required|Max:1',
            'newsletters'           => 'Max:1',
            'agent_code' => 'exists:agents,agent_code,status,active'
        );

        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            $messages = $validator->messages();
            $response = Response::make(json_encode($messages), '400');
            $response->header('Content-Type', 'application/json');
            return $response;
        }
        try {
            $result = TicketSoap::process('customer.create', [
                ['email'      => strip_tags(trim($input['email'])),
                    'firstname'  => strip_tags(trim($input['first_name'])),
                    'lastname'   => strip_tags(trim($input['last_name'])),
                    'password'   => strip_tags(trim($input['password'])),
                    'website_id' => 1,
                    'store_id'   => 1,
                    'group_id'   => 1
                ]
            ]);

        } catch (Exception $e) {

            return [$e->getMessage()];
        }
        try {
            if (is_numeric($result)) {
                $customerId = $result;
                $user = new WebUser();
                $user->user_id = $customerId;
                $user->seller_type = 'customer';
                $user->updated_by = -1;
                $user->status = 'active';
                $user->firstname = $input['first_name'];
                $user->lastname = $input['last_name'];
                $user->telephone = $input['contact_no'];
                $user->email = $input['email'];

                if(Input::get('newsletters') == 1) {
                    try {
                        $user->newsletter = Input::get('newsletters');
                        MailchimpWrapper::lists()->subscribe('071c5abfb5', array('email'=> $input['email']));
                    } catch (Exception $e) {
                        // do nothing
                    }
                }

                if(Input::has('agent_code'))
                {
                    Log::info("IN HERE");
                    Log::info($input['agent_code']);
                    $agent = Agent::where('agent_code', $input['agent_code'])->first();
                    Log::info(var_export($agent,true));
                    $user->ref_agent = $agent->id;
                }

                $user->save();

                $result     = TicketSoap::process('customer_address.create', [
                    'customerId'  => $customerId,
                    'addressdata' => ['firstname'           => $input['first_name'],
                        'lastname'            => $input['last_name'],
                        'telephone'           => $input['contact_no'],
                        'email'               => $input['email'],
                        'street'              => array('not found'),
                        'city'                => 'not found',
                        'postcode'            => 'not found',
                        'country_id'          => 'US',
                        'is_default_billing'  => false,
                        'is_default_shipping' => false
                    ]
                ]);

                return ['message' => 'user successfully created.', 'addressId' => $result, 'userId'=>$customerId];
            } else {
                return $result;
            }
        } catch (Exception $e) {
            return ['error' => 'Address: ' . $e->getMessage()];
        }


//        } else {
//            $response = Response::make('Invalid Request', '406');
//            $response->header('Content-Type', 'application/json');
//            return $response;
//        }
    }

    protected function customerList()
    {
        $customers = TicketSoap::process('customer.list');
        var_dump($customers);
    }
}
