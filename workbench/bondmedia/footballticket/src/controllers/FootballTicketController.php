<?php

use App\Controllers\Admin\MapMakerController;
use Bond\Repositories\FootballTicket\FootballTicketRepository as FootballTicket;
use Bond\Repositories\Events\EventsRepository as EventsRepository;
use Bond\Exceptions\Validation\ValidationException;
use Bond\Traits\GridPagination;

class FootballTicketController extends BaseController
{
    use GridPagination;
    protected $footballTicket;

    public function __construct(FootballTicket $footballTicket)
    {
        $this->footballTicket = $footballTicket;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $node = $this->footballTicket->paginate(null, true);
        $menu = $this->getActionType();
        View::share('menu', $menu);
        View::share('type', $menu);
        return View::make('footballticket::admin.footballticket.index', compact('node'));
    }

    public function listAction()
    {
        if (!Request::ajax()) {
            $response = Response::make(array(
                    'draw' => 1,
                    'recordsTotal' => 0,
                    'recordsFiltered' => 0,
                    'data' => []
                )
            );

            $response->header('Content-Type', 'application/json');
            return $response;
        }

        $columns = array(
            0 => 'id',
            1 => 'title',
            2 => 'slug',
            3 => 'is_published',
            4 => 'updated_at',
            5 => 'created_at'
        );
        $this->setFilter(array('type' => Input::get('type')));
        $response = Response::make($this->getGridData('football_ticket', $columns));
        $response->header('Content-Type', 'application/json');
        return $response;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $menu = $this->getActionType();

        $stadiums = Stadium::all();

        View::share('stadiums', $stadiums);
        View::share('type', $menu);
        View::share('menu', "$menu/new");
        View::share('mode', "new");

        return View::make('footballticket::admin.footballticket.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {

        try {
            $input = Input::all();
            $type = $input['action_type'];
            unset($input['action_type']);

            $this->footballTicket->setSlugPrefix($type);
            $id = $this->footballTicket->create($input);

            if (!empty($input['stadium'])) {
                $stad = $input['stadium'];
                if ($stad && $stad != "na") {
                    DB::table('team_stadium')
                        ->insert(['team_id' => $id, 'stadium_id' => $stad]);
                }
                unset($input['stadium']);
            }
            Notification::success('Football Ticket was successfully added');
            return Redirect::to(URL::to('admin/footballticket?action_type=' . $type));
        } catch (ValidationException $e) {
            return Redirect::back()->withInput()->withErrors($e->getErrors());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($type, $slug)
    {
        $locale = App::getLocale();

        $modifier = 0;
        if(Session::has('customer'))
        {
            $customer = Session::get('customer');
            $user = WebUser::where('user_id', $customer['entity_id'])->first();

            if($user)
            {
                if($user->ref_agent)
                {
                    $agent = Agent::where('id', $user->ref_agent)->first();
                    $modifier = $agent->discount_perc;
                }
            }
        }
        $ap = false;
        $show_fees = true;

        if(Session::has('refCode'))
        {
            $ap = AffiliateProperty::where('ref_code', Session::get('refCode'))->first();
            if($ap)
            {
                $show_fees = $ap->show_fees;
            }
        }

        //$template = 'frontend.%s.group-two-column-left';
        $node = $this->footballTicket->findByUri($type, $slug);

        if (empty($node)) {
            App::abort(404);
        }

        View::share('body_class', "page {$type} {$slug}");
        View::share('type', $type);
        $meta = FootballTicketMeta::where('football_ticket_id', '=', $node->id)->get();
        $season = '';
        foreach ($meta->toArray() as $val) {
            $var_view = 'meta_' . $val['key'];
            if ($var_view == 'meta_tournament') {
                $meta_var = json_decode($val['value'], true);
                $meta_var['id'] = $val['id'];
                $var_value[] = $meta_var;
                View::share($var_view, $var_value);
            } else {
                View::share($var_view, $val['value']);
            }

            if ($var_view == 'meta_season') {
                $season = $val['value'];
            }
        }
        if ($this->footballTicket->checkTrans($locale, $node->id)) {
            $trans = $this->footballTicket->getLText($node->id, $locale);
            View::share('meta_title', $trans['fbtmt']->lstring);
            View::share('meta_description', $trans['fbtmd']->lstring);
            View::share('meta_keywords', $trans['fbtmc']->lstring);

            $node->content = $trans['fbtc']->lstring;
            $node->title = $trans['fbtt']->lstring;
        } else if ($locale == 'en_gb') {
            View::share('meta_title', $node->meta_title);
            View::share('meta_description', $node->meta_description);
            View::share('meta_keywords', $node->meta_content);
        } else {
            View::share('meta_title', '');
            View::share('meta_description', '');
            View::share('meta_keywords', '');
            $node->content = '';
        }

        if ($type == 'club') {
            //Get all of the locale based tabs, if there are any.
            $tabs = DB::table('fbt_tabs')
                ->where('locale', $locale)
                ->where('fbt_id', $node->id)
                ->where('enabled', 1)
                ->where('new_layout',0)
                ->orderBy('order_shown', 'ASC')
                ->get();
            if ($tabs) {
                View::share('tabs', $tabs);
            }

            $newTabs = DB::table('fbt_tabs')
                ->where('locale',$locale)
                ->where('fbt_id', $node->id)
                ->where('enabled',1)
                ->where('new_layout',1)
                ->orderBy('order_shown', 'ASC')
                ->get();
            if($newTabs)
            {
                View::share('new_tabs', $newTabs);
            }

            $th = FootBallEvent::getClubRelatedTicketsOrderByDateTimeHA($node->id,true, $show_fees);
            $ta = FootBallEvent::getClubRelatedTicketsOrderByDateTimeHA($node->id,false, $show_fees);
            $ts = FootBallEvent::getClubRelatedTicketsOrderByDateTime($node->id, $show_fees);

            foreach($th as $ticket)
            {
                if(property_exists($ticket, 'price'))
                {
                    $ticket->price = $ticket->price - ($ticket->price*$modifier/100);
                }
            }

            foreach($ta as $ticket)
            {
                if(property_exists($ticket, 'price'))
                {
                    $ticket->price = $ticket->price - ($ticket->price*$modifier/100);
                }
            }

            foreach($ts as $ticket)
            {
                if(property_exists($ticket, 'price'))
                {
                    $ticket->price = $ticket->price - ($ticket->price*$modifier/100);
                }
            }

            View::share('ticketsHome', $th);
            View::share('ticketsAway', $ta);
            View::share('tickets', FootBallEvent::getClubRelatedTicketsOrderByDateTime($node->id));

            $rich_snippet = FootballTickets::getTeamRichSnippets($node->id);
            View::share('rich_snippets', $rich_snippet);


            $articleIds = DB::table('article_fbt_assoc')
                ->where('fbt_id', $node->id)
                ->orderBy('articles_id', 'DESC')
                ->take(3)
                ->get();
            $articles = [];

            if ($locale == "en_gb") {
                foreach ($articleIds as $aId) {
                    $article = Article::where('id', $aId->articles_id)->first();
                    if ($article) {
                        $articles[] = $article;
                    }
                }
            }


            View::share('articles', $articles);

            if(count($newTabs))
            {
                return View::make(Template::name('frontend.%s.new-team'), compact('node'));
            }

            return View::make(Template::name('frontend.%s.team'), compact('node'));
        } else if ($type == 'league') {
            $articleIds = DB::table('article_fbt_assoc')
                ->where('fbt_id', $node->id)
                ->orderBy('articles_id', 'DESC')
                ->take(3)
                ->get();
            $articles = [];

            if ($locale == "en_gb") {
                foreach ($articleIds as $aId) {
                    $article = Article::where('id', $aId->articles_id)->first();
                    if ($article) {
                        $articles[] = $article;
                    }
                }
            }

            View::share('articles', $articles);
            View::share('tickets', FootBallEvent::getLeagueRelatedTickets($node->id));
            View::share('clubs', FootballTickets::getClubs($node->id, $season));
            View::share('upcomingEvents', FootBallEvent::upcomingWithoutJoin($node->id));
            return View::make(Template::name('frontend.%s.league'), compact('node'));
        } else {
            return View::make('footballticket::frontend.group-two-column-left', compact('node'));
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function showTest($type, $slug)
    {
        $locale = App::getLocale();

        //$template = 'frontend.%s.group-two-column-left';
        $node = $this->footballTicket->findByUri($type, $slug);

        if (empty($node)) {
            App::abort(404);
        }

        View::share('body_class', "page {$type} {$slug}");
        View::share('type', $type);
        $meta = FootballTicketMeta::where('football_ticket_id', '=', $node->id)->get();
        $season = '';
        foreach ($meta->toArray() as $val) {
            $var_view = 'meta_' . $val['key'];
            if ($var_view == 'meta_tournament') {
                $meta_var = json_decode($val['value'], true);
                $meta_var['id'] = $val['id'];
                $var_value[] = $meta_var;
                View::share($var_view, $var_value);
            } else {
                View::share($var_view, $val['value']);
            }

            if ($var_view == 'meta_season') {
                $season = $val['value'];
            }
        }

        if ($this->footballTicket->checkTrans($locale, $node->id)) {
            $trans = $this->footballTicket->getLText($node->id, $locale);
            View::share('meta_title', $trans['fbtmt']->lstring);
            View::share('meta_description', $trans['fbtmd']->lstring);
            View::share('meta_keywords', $trans['fbtmc']->lstring);

            $node->content = $trans['fbtc']->lstring;
            $node->title = $trans['fbtt']->lstring;
        } else if ($locale == 'en_gb') {
            View::share('meta_title', $node->meta_title);
            View::share('meta_description', $node->meta_description);
            View::share('meta_keywords', $node->meta_content);
        } else {
            View::share('meta_title', '');
            View::share('meta_description', '');
            View::share('meta_keywords', '');
            $node->title = '';
            $node->content = '';
        }

        if ($type == 'club') {
            //Get all of the locale based tabs, if there are any.
            $tabs = DB::table('fbt_tabs')
                ->where('locale', $locale)
                ->where('fbt_id', $node->id)
                ->where('enabled', 1)
                ->orderBy('order_shown', 'ASC')
                ->get();
            if ($tabs) {
                View::share('tabs', $tabs);
            }
            View::share('tickets', FootBallEvent::getClubRelatedTicketsOrderByDateTime($node->id));

            $articleIds = DB::table('article_fbt_assoc')
                ->where('fbt_id', $node->id)
                ->orderBy('articles_id', 'DESC')
                ->take(3)
                ->get();
            $articles = [];

            if (substr($locale, 0, 2) == "en") {
                foreach ($articleIds as $aId) {
                    $article = Article::where('id', $aId->articles_id)->first();
                    if ($article) {
                        $articles[] = $article;
                    }
                }
            }


            View::share('articles', $articles);

            return View::make(Template::name('frontend.%s.team'), compact('node'));
        } else if ($type == 'league') {
            $articleIds = DB::table('article_fbt_assoc')
                ->where('fbt_id', $node->id)
                ->orderBy('articles_id', 'DESC')
                ->take(3)
                ->get();
            $articles = [];

            if (substr($locale, 0, 2) == "en") {
                foreach ($articleIds as $aId) {
                    $article = Article::where('id', $aId->articles_id)->first();
                    if ($article) {
                        $articles[] = $article;
                    }
                }
            }

            View::share('articles', $articles);
            View::share('tickets', FootBallEvent::getLeagueRelatedTickets($node->id));
            View::share('clubs', FootballTickets::getClubs($node->id, $season));
            View::share('upcomingEvents', FootBallEvent::upcoming($node->id));
            return View::make(Template::name('frontend.%s.league'), compact('node'));
        } else {
            return View::make('footballticket::frontend.group-two-column-left', compact('node'));
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */

    public function edit($id)
    {
        $menu = $this->getActionType();
        $node = $this->footballTicket->find($id);

        $ss = DB::table('team_stadium')->where('team_id', $id)->first();


        $stadiums = Stadium::all();

        View::share('stadiums', $stadiums);
        View::share('type', $menu);
        View::share('menu', "$menu/edit");
        View::share('mode', "edit");

        if ($ss) {
            View::share('curStad', $ss->stadium_id);
        }


        $meta = FootballTicketMeta::where('football_ticket_id', '=', $id)->get();
        foreach ($meta->toArray() as $val) {
            $var_view = 'meta_' . $val['key'];
            if ($var_view == 'meta_tournament') {
                $meta_var = json_decode($val['value'], true);
                $meta_var['id'] = $val['id'];
                $var_value[] = $meta_var;
                View::share($var_view, $var_value);
            } else {
                View::share($var_view, $val['value']);
            }

        }

        $countries = DB::table('football_ticket')->where('type', '=', 'country')
            ->orderBy('created_at', 'ASC')
            ->groupBy('slug')
            ->get();

        $tournament = DB::table('football_ticket')->where('type', '=', 'league')
            ->orderBy('created_at', 'ASC')
            ->groupBy('slug')
            ->get();

        $season = DB::table('football_ticket')->where('type', '=', 'season')
            ->orderBy('created_at', 'ASC')
            ->groupBy('slug')
            ->get();

        View::share('countries', $countries);
        View::share('tournament', $tournament);
        View::share('season', $season);

        return View::make('footballticket::admin.footballticket.edit', compact('node'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {

        try {
            $menu = $this->getActionType();
            $input = Input::all();

            if(array_key_exists('is_international', $input))
            {
                $input['is_international'] = true;
            }
            else
            {
                $input['is_international'] = false;
            }
            $type = $input['action_type'];
            unset($input['action_type']);
            $stad = false;
            if (array_key_exists('stadium', $input)) {
                $stad = $input['stadium'];
            }
            if ($stad) {
                $res = DB::table('team_stadium')
                    ->where('team_id', $id)
                    ->first();

                if ($stad != 'na') {
                    if ($res) {
                        DB::table('team_stadium')
                            ->where('team_id', $id)
                            ->update(['stadium_id' => $stad]);
                    } else {
                        DB::table('team_stadium')
                            ->insert(['team_id' => $id, 'stadium_id' => $stad]);
                    }
                } else if ($res) {
                    DB::table('team_stadium')
                        ->where('team_id', $id)
                        ->delete();
                }
                unset($input['stadium']);
            }

            $this->footballTicket->update($id, $input);
            Notification::success($type . ' was successfully updated');
            $redirect = Redirect::to(URL::to('admin/footballticket?action_type=' . $type));
            return $redirect;
        } catch (ValidationException $e) {
            return Redirect::back()->withInput()->withErrors($e->getErrors());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        $menu = $this->getActionType();
        $this->footballTicket->destroy($id);
        $type = Input::get('action_type');
        Notification::success($type . ' was successfully deleted');
        return Redirect::to(URL::to('admin/footballticket?action_type=' . $type));
    }

    public function confirmDestroy($id)
    {
        $menu = $this->getActionType();
        $node = $this->footballTicket->find($id);
        View::share('type', $menu);
        return View::make('footballticket::admin.footballticket.confirm-destroy', compact('node'))
            ->with('menu', 'footballticket/edit');
    }

    public function togglePublish($id)
    {

        return $this->footballTicket->togglePublish($id);
    }

    public function getActionType()
    {
        $type = Input::get('action_type');
        $output = '';
        switch ($type) {
            case "club":
                $output = $type;
                break;
            case "league":
                $output = $type;
                break;
            case "country":
                $output = $type;
                break;
            case "season":
                $output = $type;
                break;
            default:
                App::abort(403, 'Unauthorized action.');
        }

        return $output;
    }


    public function saveMeta()
    {
        $input = Input::all();

        $key = $input['key'];
        $value = json_encode($input['value']);
        $id = $input['id'];

        $footballMeta = new FootballTicketMeta();

        if (isset($input['value']['tournament'])) {
            $ftct = new FootballTicketClubTournament();
            $ftct->fill(array(
                'tournament_id' => $input['value']['tournament'],
                'club_id' => $input['id'],
                'season_id' => $input['value']['season']
            ))->save();
        }

        $footballMeta->fill(array(
            'football_ticket_id' => $id,
            'key' => $key,
            'value' => $value
        ))->save();


        if ($footballMeta->id && $footballMeta->id != -'') {
            $response = Response::make(json_encode(['id' => $footballMeta->id]), '200');
            $response->header('Content-Type', 'application/json');
            return $response;
        } else {
            $response = Response::make(json_encode(['message' => 'unable to create']), '400');
            $response->header('Content-Type', 'application/json');
            return $response;
        }
    }

    public function deleteMeta()
    {
        $input = Input::all();
        $meta = FootballTicketMeta::find($input['id']);
        if ($meta != null) {
            if ($meta->value != '') {
                $val = json_decode($meta->value, true);

                if (isset($val['tournament'])) {
                    $ftct = FootballTicketClubTournament::where('tournament_id', '=', $val['tournament'])
                        ->where('club_id', '=', $meta->football_ticket_id)->first();
                    if ($ftct != null) {
                        $ftct->delete();
                    }
                }
            }

            $meta->delete();
            $response = Response::make(json_encode(['id' => $input['id']]), '200');
            $response->header('Content-Type', 'application/json');
            return $response;
        } else {
            $response = Response::make(json_encode(['message' => 'unable to create']), '400');
            $response->header('Content-Type', 'application/json');
            return $response;
        }
    }

    public function searchEvent()
    {

        $locale = App::getLocale();

        $input = Input::all();
        $output = array();

        $ids = array();
        if ($input['q']) {
            $results = DB::table('locale_text')
                ->where('lstring', 'like', "%" . $input['q'] . "%")
                ->where('id', 'like', 'event_title_%')
                ->where('locale', $locale)
                ->take(100)
                ->get();

            foreach ($results as $r) {
                $id = explode('_', $r->id);
                $id = end($id);

                if (!in_array(intval($id), $ids)) {
                    $ids[] = intval($id);
                }
            }

            $results = DB::table('events')->where('events.title', 'LIKE', "%{$input['q']}%")
                ->take(100)
                ->get();

            foreach ($results as $r) {
                if (!in_array(intval($r->id), $ids)) {
                    $ids[] = intval($r->id);
                }
            }
        }

        $results = DB::table('events')->whereIn('events.id', $ids)
            ->leftJoin('football_ticket', 'events.tournament_id', '=', 'football_ticket.id')
            ->select('events.id', 'events.title AS name', 'football_ticket.title AS league', 'events.datetime', 'events.feature_image', 'events.meta_description as content', 'events.slug', 'events.season_id', 'events.tournament_id')
            ->orderBy('events.datetime', 'ASC')
            ->whereRaw('events.datetime >="' . date('Y-m-d H:i:s') . '"')
            ->take(100)
            ->get();


        foreach ($results as $result) {
            $result->_url = FootBallEvent::getUrl($result);

            $event = FootBallEvent::find($result->id);
            $er = new EventsRepository($event);

            if ($er->checkTrans($locale, $event->id)) {
                $lText = $er->getLText($event->id, $locale);
                if ((!empty($lText['ec']->lstring)) || (!empty($lText['et']->lstring))) {

                    if (!empty($lText['emd'])) {
                        $result->content = mb_convert_encoding($lText['emd']->lstring, 'UTF-8', 'UTF-8');
                    }
                    if (!empty($lText['et'])) {
                        $result->name = mb_convert_encoding($lText['et']->lstring, 'UTF-8', 'UTF-8');
                    }
                } else {
                    $result->content = '';
                }
            } else if ($locale != 'en_gb') {
                $result->content = '';
            }

            $output[] = $result;
        }
        return Response::json($output, '200');
    }

    public function searchEventCategory()
    {
        //Search for trans
        //Get id's
        //Search reg
        //get id's
        //translate
        $input = Input::all();
        $locale = App::getLocale();
        $ids = array();

        $bf = 1 + (intval(Options::getOption('booking_fees'))/100);

        $output = array();
        try {
            if ($input['q']) {
                $results = DB::table('locale_text')
                    ->where('lstring', 'like', "%" . $input['q'] . "%")
                    ->where('id', 'like', 'fbt_title_%')
                    ->where('locale', $locale)
                    ->take(5)
                    ->get();

                foreach ($results as $r) {
                    $id = explode('_', $r->id);
                    $id = end($id);

                    if (!in_array(intval($id), $ids)) {
                        $ids[] = intval($id);
                    }
                }

                $results = DB::table('football_ticket')
                    ->where('title', 'LIKE', "%{$input['q']}%")
                    ->where("type", 'club')
                    ->get();

                foreach ($results as $r) {
                    if (!in_array(intval($r->id), $ids)) {
                        $ids[] = intval($r->id);
                    }
                }

                $results = $results = DB::table('football_ticket')
                    ->select('football_ticket.*', 'football_ticket_club_tournaments.tournament_id')
                    ->leftJoin('football_ticket_club_tournaments', 'football_ticket_club_tournaments.club_id', '=', 'football_ticket.id')
                    ->whereIn('football_ticket.id', $ids)
                    ->whereRaw("football_ticket.type='club'")
                    ->take(5)
                    ->groupBy('football_ticket.id')
                    ->get();
            }

            foreach ($results as $result) {
                $temp = array();
                $ttl = DB::table('locale_text')
                    ->where('id', 'fbt_title_' . $result->id)
                    ->where('locale', $locale)
                    ->first();

                if (!empty($ttl->lstring)) {
                    $temp['name'] = $ttl->lstring;
                } else {
                    $temp['name'] = $result->title;
                }

                $temp['url'] = url('/group/club/' . $result->slug);
                $tournament = FootballTickets::find($result->tournament_id);
                if ($tournament) {
                    $ttl = DB::table('locale_text')
                        ->where('id', 'fbt_title_' . $tournament->id)
                        ->where('locale', $locale)
                        ->first();

                    if (!empty($ttl->lstring)) {
                        $temp['league']['name'] = $ttl->lstring;
                    } else {
                        $temp['league']['name'] = $tournament->title;
                    }

                    $temp['league']['url'] = url('group/league/' . $tournament->slug);
                } else {
                    $temp['league']['name'] = '';
                    $temp['league']['url'] = '#';
                }
                $output[] = $temp;
            }
        } catch (Exception $e) {
            $response = Response::make(json_encode($e->getMessage()), 400);
            $response->header('Content-Type', 'application/json');
            return $response;
        }

        $response = Response::make(json_encode($output), '200');
        $response->header('Content-Type', 'application/json');
        return $response;
    }

    public function displayEventsNoSeason($league = '', $slug = '')
    {
        return $this->displayEvents($league, null, $slug);
    }

    public function displayEvents($league = '', $season = '', $slug = '')
    {
        $locale = \App::getLocale();
        $conv = DB::table('locales')
            ->where('lang', $locale)
            ->first();


        $conv = $conv->conversion;
        $bf = 1 + (intval(Options::getOption('booking_fees'))/100);

        $modifier = 0;
        if(Session::has('customer'))
        {
            $customer = Session::get('customer');
            $user = WebUser::where('user_id', $customer['entity_id'])->first();

            if($user)
            {
                if($user->ref_agent)
                {
                    $agent = Agent::where('id', $user->ref_agent)->first();
                    $modifier = $agent->discount_perc;
                }
            }
        }

        $af = false;
        $show_fees = true;

        if(Session::has('refCode'))
        {
            $af = AffiliateProperty::where('ref_code', Session::get('refCode'))->first();
            if($af)
            {
                $show_fees = $af->show_fees;
            }
        }

        $node = DB::table('events')->where('events.slug', '=', "$slug")
            ->leftJoin('football_ticket AS h', 'h.id', '=', 'events.home_team_id')
            ->leftJoin('football_ticket AS a', 'a.id', '=', 'events.away_team_id')
            ->join('football_ticket AS s', 's.id', '=', 'events.season_id')
            ->join('football_ticket AS l', 'l.id', '=', 'events.tournament_id')
            ->select('events.*', 'h.title AS homeTeam', 'a.title AS awayTeam')
            ->where('l.slug', '=', $league)
            ->orderBy('events.id', 'desc')
            ->first();

        /**
        if (!empty($node->migrate)) {
            if ($node->migrate == 1) {
                if (!empty($node->magrate_id)) {
                    $node = DB::table('events')->where('id', '=', $node->magrate_id)
                        ->first();
                    $tornment = DB::table('football_ticket')->where('id', '=', $node->tournament_id)->first();

                }
                return Redirect::to("/".$tornment->slug."/".$node->slug, 301);
            }
        }
         */

        if (!empty($node->id)) {
            $stadium = DB::table('event_stadium')
                ->where('event_id', $node->id)
                ->first();
        }

        if (!empty($stadium)) {
            if (!empty($stadium->stadium_id)) {
                $stadium = Stadium::where('id', $stadium->stadium_id)->first();
            }
        }


        if (empty($node)) {
            App::abort(404);
        }

        $event = FootBallEvent::find($node->id);
        $er = new EventsRepository($event);

        if ($er->checkTrans($locale, $node->id)) {
            $lText = $er->getLText($node->id, $locale);

            if ($locale != 'en_gb') {
                $node->title = "";
                $node->content = "";
                $node->meta_title = "";
                $node->meta_description = "";
                $node->meta_keyword = '';
            }

            if (!empty($lText['et']->lstring)) {
                $node->title = $lText['et']->lstring;
            }

            if (!empty($lText['ec']->lstring)) {
                $node->content = $lText['ec']->lstring;
            }

            if (!empty($lText['emt']->lstring)) {
                $node->meta_title = $lText['emt']->lstring;
            }

            if (!empty($lText['emd']->lstring)) {
                $node->meta_description = $lText['emd']->lstring;
            }

            if (!empty($lText['emk']->lstring)) {
                $node->meta_keyword = $lText['emk']->lstring;
            }
        } else if ($locale != 'en_gb') {
                $node->title = '';
            $node->content = '';
            $node->meta_title = '';
            $node->meta_description = '';
            $node->meta_keyword = '';
        }

        $tickets = array();
        $ticketTypes = array();

        $groundImage = '';
        if (isset($node->id)) {
            $results = DB::table('events_related_tickets')
                ->where('event_id', '=', $node->id)
                ->where('available_qty', '>', 0)
                ->where('ticket_status', '=', 'active')
                ->orWhere('ticket_status', '=', 'sold')
                ->where('event_id', '=', $node->id)
                ->where('available_qty', '>', 0)
                ->orderBy('price', 'ASC')
                ->get();

            $lowestPrice = 99999999;
            $lowestPricedId = 0;
            $ticketCounter = [];

            foreach ($results as $r) {
                $temp = json_decode($r->ticket, true);
                $temp['product_id'] = $r->product_id;
                $temp['available_qty'] = $r->available_qty;


                $temp['ticketInformation']['loc_block'] = trim($temp['ticketInformation']['loc_block']);
                $temp['ticketInformation']['loc_row'] = trim($temp['ticketInformation']['loc_row']);
                if($show_fees)
                {
                    $temp['ticketInformation']['price'] = number_format((($temp['ticketInformation']['price'] * $conv) * $bf) - (((($temp['ticketInformation']['price'] * $conv) * $bf)*$modifier)/100), 2, '.', '');
                }
                else
                {
                    $temp['ticketInformation']['price'] = number_format($temp['ticketInformation']['price'] * $conv, 2, '.', '');
                }
                $temp['bestbuy'] = 1;

                /**
                 * Check If this Current ticket price is lower Than the rest on this page ( FOR THE TICKET TAGS )
                 */
                if ($temp['ticketInformation']['price'] <= $lowestPrice) {
                    $lowestPrice = $temp['ticketInformation']['price'];
                    $lowestPricedId = $temp['product_id'];
                }


                $temp['note'] = RelatedTicket::getTicketByTicketId($r->product_id);

                $tForm = DB::table('events_form_of_ticket')
                    ->where('id', $temp['ticketInformation']['form_of_ticket'])
                    ->first();

                if ($tForm && $tForm->title == 'E-tickets') {
                    $temp['isETicket'] = true;
                } else {
                    $temp['isETicket'] = false;
                }

                if (empty($ticketCounter[$temp['ticketInformation']['ticket_type']])) {
                    $ticketCounter[$temp['ticketInformation']['ticket_type']] = 1;
                } else {
                    $ticketCounter[$temp['ticketInformation']['ticket_type']] = 2;
                }

                $tickets[] = $temp;
            }

            /**
             * Check What ticket is the cheaoest
             * And What is the Only Ticket Of that type
             */


            foreach ($tickets as $y => $ticket) {
                if ($ticket['product_id'] == $lowestPricedId) {
                    $tickets[$y]['bestbuy'] = 2;
                } else {
                    if ($ticket['bestbuy'] != 2) {
                        $ticket['bestbuy'] = 1;
                    }
                }

                if ($ticketCounter[$ticket['ticketInformation']['ticket_type']] == 1) {
                    $tickets[$y]['onlyTicket'] = 1;
                } else {
                    $tickets[$y]['onlyTicket'] = 2;
                }

            }

            $groundImage = $node->venue_image;
            //get home ground stadium images
            $homeTeam = DB::table('football_ticket')->where('id', '=', $node->home_team_id)
                ->first();

            $trans = \DB::table('locale_text')
                ->where('id', 'fbt_title_' . $homeTeam->id)
                ->where('locale', $locale)
                ->first();

            if ($trans) {
                $homeTeam->title = $trans->lstring;
            }

            View::share('homeTeam', $homeTeam);

            $awayTeam = DB::table('football_ticket')->where('id', '=', $node->away_team_id)
                ->first();

            $trans = \DB::table('locale_text')
                ->where('id', 'fbt_title_' . $awayTeam->id)
                ->where('locale', $locale)
                ->first();
            if ($trans) {
                $awayTeam->title = $trans->lstring;
            }

            View::share('awayTeam', $awayTeam);

            View::share('lSlug', $league);

            if ($node->event_in_home) {
                $homeStad = DB::table('team_stadium')
                    ->where('team_id', $homeTeam->id)
                    ->first();


            } else {
                $homeStad = DB::table('team_stadium')
                    ->where('team_id', $awayTeam->id)
                    ->first();
            }

            if ($homeStad) {
                $homeStad = Stadium::where('id', $homeStad->stadium_id)->first();
            }
            $mapSvg = null;
            $svgmapfile = null;
            $sharestad = null;

            if ($stadium) {
                if (!empty($stadium->svg_map)) {
                    if (is_numeric($stadium->svg_map)) {
                        $mapImg = new MapMakerController;
                        $mapSvg = $mapImg->mapBuilderFrontEnd($stadium->svg_map);
                    } else {
                        $svgmapfile = $stadium->svg_map;
                        $mapSvg = null;
                    }
                }

                if (!empty($stadium->map_image) && $stadium->map_image != "") {
                    $groundImage = $stadium->map_image;
                }

                $sharestad = $stadium;
            } else if ($node->svgmap || $node->venue_image) {
                if (is_numeric($node->svgmap)) {
                    $mapImg = new MapMakerController;
                    $mapSvg = $mapImg->mapBuilderFrontEnd($node->svgmap);
                } else {
                    $svgmapfile = $node->svgmap;
                    $mapSvg = null;
                }

                if ($node->venue_image && $node->venue_image != "") {
                    $groundImage = $node->venue_image;
                }
            } else if ($homeStad) {
                if (is_numeric($homeStad->svg_map)) {
                    $mapImg = new MapMakerController;
                    $mapSvg = $mapImg->mapBuilderFrontEnd($homeStad->svg_map);
                } else {
                    $svgmapfile = $homeStad->svg_map;
                    $mapSvg = null;
                }

                if ($homeStad->map_image && $homeStad->map_image != "") {
                    $groundImage = $homeStad->map_image;
                }

                $sharestad = $homeStad;
            } else {
                $team = '';
                if ($node->event_in_home) {
                    $team = $homeTeam;
                } else {
                    $team = $awayTeam;
                }

                $groundImage = $team->venue_image;
            }

            if (empty($node->feature_image)) {
                $team = "";
                if ($node->event_in_home) {
                    $team = $homeTeam;
                } else {
                    $team = $awayTeam;
                }
                $node->feature_image = $team->venue_splash;
            }

            if (!empty($sharestad)) {
                if (!empty($sharestad->country_id)) {
                    $country = DB::table('country_list')
                        ->where('Sort Order', $sharestad->country_id)
                        ->select(DB::raw('`Common Name` as cname'))
                        ->first();
                }

                if (!empty($country)) {
                    $sharestad->countryname = $country->cname;
                }
            }

            View::share('stadium', $sharestad);

            //home team events
            $homeTeamEvents = DB::table('events')
                ->whereRaw('(events.home_team_id =' . $node->home_team_id . ' OR events.away_team_id = ' . $node->home_team_id . ')
                                        AND events.datetime >="' . date('Y-m-d H:i:s') . '"
                                        AND events.id <>' . $node->id)
                ->take(5)
                ->orderBy('events.datetime', 'ASC')
                ->get();

            View::share('homeTeamEvents', $homeTeamEvents);
        }

        $event = FootBallEvent::find($node->id);
        $defaultRestriction = [];
        foreach ($event->getSelectedRestrictions() as $val) {

            $defaultRestriction[] = array(
                'id' => $val->id,
                'title' => $val->title
            );
        }

        View::share('defaultRestriction', $defaultRestriction);

        $ticketTypes = DB::table('events_ticket_type')->orderBy('sort_order', 'ASC')->get();

        $tournament = FootballTickets::find($node->tournament_id);
        $ttitle = $tournament->title;

        if (new DateTime() > new DateTime($event->datetime)) {
            View::share('is_past', true);
        } else {
            View::share('is_past', false);
        }

        View::share('TournamentTitle', $ttitle);

        View::share('meta_title', $node->meta_title);
        View::share('meta_description', $node->meta_description);
        View::share('meta_keywords', $node->meta_keyword);


        View::share('body_class', 'page buy');
        View::share('node', $node);
        View::share('tickets', $tickets);
        View::share('ticketTypes', $ticketTypes);
        View::share('groundImage', $groundImage);

        $articleIds = DB::table('article_event_assoc')
            ->where('event_id', $node->id)
            ->orderBy('articles_id', 'DESC')
            ->take(3)
            ->get();
        $articles = [];

        if ($locale == "en_gb") {
            foreach ($articleIds as $aId) {
                $article = Article::where('id', $aId->articles_id)->first();
                if ($article) {
                    $articles[] = $article;
                }
            }
        }

        View::share('articles', $articles);

        $listOfMapClasses = DB::table('map_compare')->get();
        $catconvarray = [];
        $smallCatList = [];
        foreach ($listOfMapClasses as $a) {
            $catconvarray[$a->left] = $a->right;
            if (empty($smallCatList[$a->right])) {
                $smallCatList[$a->right]['name'] = $a->frontend;
                $smallCatList[$a->right]['vis'] = $a->onmap;
            }

        }


        unset($listOfMapClasses);
        $fullnames = [];
        foreach ($smallCatList as $a => $b) {

            $fullnames[$a]['name'] = $b['name'];
            $fullnames[$a]['vis'] = $b['vis'];

        }

        /**
         * Days Remaining
         */
        $date = strtotime($node->datetime);
        $dateToday = strtotime(date("Y-m-d H:i:s"));
        $comparedDate = $date - $dateToday;
        $daysRemaining = ceil($comparedDate / 60 / 60 / 24);
        $daysRemaining = $daysRemaining;
        View::share('daysRemaining', $daysRemaining);

        return View::make(Template::name('frontend.%s.buy'))->with('svgmap', $mapSvg)->with('catconvarray', $catconvarray)->with('fullnames', $fullnames);
    }

    public function sitemap()
{

    $output = "";
    $output .= '<?xml version="1.0" encoding="UTF-8"?><urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
    $leagues = DB::table('football_ticket')->where('type', '=', 'league')->get();
    $tournament = [];
    foreach ($leagues as $row) {
        $tournament[$row->id] = $row->slug;
        $output .= "\n\n    <url>   \n      <loc>https://".$_SERVER['SERVER_NAME']. '/group/league/'.  $row->slug."</loc>\n       <changefreq>weekly</changefreq>\n       <priority>0.8</priority>\n   </url> ";
    }

    $leagues = DB::table('football_ticket')->where('type', '=', 'club')->get();
    foreach ($leagues as $row) {

        $output .= "\n\n    <url>   \n      <loc>https://".$_SERVER['SERVER_NAME']. '/group/club/'.  $row->slug."</loc>\n       <changefreq>weekly</changefreq>\n       <priority>0.8</priority>\n   </url> ";
    }

    $Date  = date("d-m-y");
    $leagues = DB::table('events')->orderBy('id','DESC')->get();
    foreach ($leagues as $row) {
        if (!empty($tournament[$row->tournament_id])) {
            $x = "\n\n    <url>   \n      <loc>https://";
            $x .= $_SERVER['SERVER_NAME'] . "/";
            $x .= $tournament[$row->tournament_id];

            $x .= "/";
            $x .= $row->slug;
            $x .= "</loc>\n       <changefreq>weekly</changefreq>\n       <priority>0.8</priority>\n   </url> ";
            $output .= $x;
        }
    }


    $leagues = DB::table('articles')->get();
    foreach ($leagues as $row) {

        $x = "\n\n    <url>   \n      <loc>https://";
        $x .= $_SERVER['SERVER_NAME'] . "/";

        $x .= "news/";
        $x .= $row->slug;
        $x .= "</loc>\n       <changefreq>weekly</changefreq>\n       <priority>0.8</priority>\n   </url> ";
        $output .= $x;
    }

    return $output."</urlset>";

}

    public function sitemaphtml()
    {

        $output = "<ul>";
        $output .= "<h2>Leagues Pages</h2>";
        $leagues = DB::table('football_ticket')->where('type', '=', 'league')->get();
        $tx = [];
        foreach ($leagues as $row) {
            $x = "<li class='col-md-4'><a href='https://";
            $x .= $_SERVER['SERVER_NAME'] . "/group/league/";
            $x .=  $row->slug;

            $x .= "'>";
            $x .= $row->title;
            $x .= "</a></li>";
            $output .= $x;
         //   $output .= "\n\n    <url>   \n      <loc>".$_SERVER['SERVER_NAME']. '/group/league/'.  $row->slug."</loc>\n       <changefreq>weekly</changefreq>\n       <priority>0.8</priority>\n   </url> ";

            $tx[$row->id] = $row->slug;
        }

        $output .= "<h2>Clubs Pages</h2>";
        $leagues = DB::table('football_ticket')->where('type', '=', 'club')->get();
        foreach ($leagues as $row) {
            $x = "<li class='col-md-4'><a href='";
            $x .= 'https://'.$_SERVER['SERVER_NAME'] . "/group/club/";
            $x .=  $row->slug;

            $x .= "'>";
            $x .= $row->title;
            $x .= "</a></li>";
            $output .= $x;
       //     $output .= "\n\n    <url>   \n      <loc>".$_SERVER['SERVER_NAME']. '/group/club/'.  $row->slug."</loc>\n       <changefreq>weekly</changefreq>\n       <priority>0.8</priority>\n   </url> ";
        }
        $output .= "<h2>Event Pages</h2>";
        $date = new DateTime();
        $date = $date->format('Y-m-d H:i:s');

        $leagues = DB::table('events')->orderBy('id','DESC')->where('datetime', '>=', $date)->get();
        foreach ($leagues as $row) {
            if (!empty($tx[$row->tournament_id])) {

                $x = "<li class='col-md-4'><a href='";
                $x .=  "/";
                $x .= $tx[$row->tournament_id];
                $x .=  "/";
                $x .= $row->slug;
                $x .= "'>";
                $x .= $row->title;
                $x .= "</a></li>";
                $output .= $x;
            }
        }

        $output .= "<h2>News Articles</h2>";
        $leagues = DB::table('articles')->get();
        foreach ($leagues as $row) {

                $x = "<li class='col-md-4'><a href='";
                $x .=  "/news/";
                $x .= $row->slug;
                $x .= "'>";
                $x .= $row->title;
                $x .= "</a></li>";
                $output .= $x;
        }

        $output.= "</ul>
<style>
@media (min-width: 769px) {
li.col-md-4 {
    width: 33%;
    float: left;
        padding-right: 28px;
}
}
@media (max-width: 768px) {
.homepageFeatureForceRow{
PADDING:20PX;

}
}
h2{
    width: 100%;
    font-size: 24px;
    margin-bottom: 15px !important;
    clear: both;
    padding-top: 20px;
    padding-bottom: 20px;
}
</style>

";
        return View::make(Template::name('frontend.bm2014.sitemap'))->with('sitemap', $output)->with('body_class','');

    }
}


