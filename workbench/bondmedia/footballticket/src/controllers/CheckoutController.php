<?php
use TransactPRO\Gate\GateClient;

class CheckoutController extends BaseController {
    public function test() {
        var_dump(TicketSoap::test("cart.info", array(["test variable"])));
    }

    public function order($id) {
        $customer = Session::get('customer');
        $input = Input::all();
        $shoppingCartId = Session::get($id.'_shoppingCartId');
        $_ticket = RelatedTicket::getTicketByTicketId($id);
        $_ticket->price = round($_ticket->price, 2);

        try
        {
            $cart = TicketSoap::process("cart.info", array($shoppingCartId));
            $store_id = $cart['store_id'];
            if($cart)
            {
                $itemArray = array();

                foreach($cart['items'] as $item)
                {
                    TicketSoap::process("cart_product.remove", array($shoppingCartId, array(array('product_id' => $item['product_id'],'sku'=>$item['sku'], 'quantity'=>$item['qty'], 'options' => array())), $store_id));
                }

            }
        }
        catch(Exeption $e)
        {
        }

        if($_ticket->ticket_status != 'active')
        {
            throw new Exception( json_encode("Error."));
        }

        $shipSame = intval(Input::get('ship-same'));
        $countryId = isset($input['country_id']) ? $input['country_id'] : 'GB';
        $countryIdBill = isset($input['country_id_bill']) ? $input['country_id_bill'] : 'GB';
        $nameOnCard = isset($customer['firstname'])? $customer['firstname']." ".$customer['lastname'] : $input['first_name']." ".$input['last_name'];
        $customerEmail = isset($customer['email'])? $customer['email']: '';
        $hotelString = "";
        $hotelMethods = array("flatrate3", "flatrate6", "flatrate8", "flatrate10", "flatrate12");
        $rName = Input::get("reservation_name");
        $hName = Input::get('hotel_name');
        $cIn = Input::get('checkin_dt');
        $cOut = Input::get('checkout_dt');

        if (!empty($rName)) {
            $hotelString = "Reservation Name: ".$rName.'; Check-in time:'.$cIn.'; Check-out time:'.$cOut.' Hotel name: '.$hName.';';
        }

        try {
            //validating card info:
            //  if(Config::get('transactpro.COD_Enable') ==  true) {
            //      CreditCard::validateCardNumber($input['card_number']);
            //      CreditCard::validateExpireDate($input['card_expire']);
            //      CreditCard::validateCVV($input['card_cvv']);
            //  }

            if (!$customer) {
                //Log::info('New Customer Request');
                $newCustomer = array(
                    'first_name'            => $input['first_name'],
                    'last_name'             => $input['last_name'],
                    'email'                 => $input['email'],
                    'email_confirmation'    => $input['email_confirmation'],
                    'password_confirmation' => $input['password_confirmation'],
                    'password'              => $input['password'],
                    'terms_n_conditions'    => true,
                    'contact_no'            => $input['contact_no']
                );

                $this->setDataToPost($newCustomer);
                $response = $this->submitPostToApi( route('ticket.registrationsInternal'),false)->getApiResponse();
                $user = json_decode($response);

                if(!isset($user->message) || $user->message != "user successfully created.") {
                    $response = json_decode($response, true);
                    throw new Exception(print_r( $response));
                }
                $customerEmail = $input['email'];

                //update user address
                $params = array();
                $params['customer_id']  = $user->userId;
                $params['type']         = 'set';
                $params['address_type'] = 'shipping';
                $params['vat_id']       = '';
                $params['company']      = '';
                $params['street']       = $input['street'];



                $params['country_id']   = $countryId;
                $params['postcode']     = $input['postcode'];
                $params['city']         = $input['city'];
                $params['telephone']    = $input['country_code'].$input['contact_no'];
                $_address               = $params;
                //set billing
                $this->setDataToPost($params);
                $this->submitPostToApi('customer/index/address');

                //set shipping
                $params['address_type'] = 'billing';
                if(!$shipSame)
                {
                    $params['street']       = $input['street_bill'];
                    $params['country_id']   = $countryIdBill;
                    $params['postcode']     = $input['postcode_bill'];
                    $params['city']         = $input['city_bill'];
                }
                if(!empty($hotelString) && $hotelString != ''){
                    $params['street']  .= $params['street'] ;
                }


                $this->setDataToPost($params);
                $this->submitPostToApi('customer/index/address');
                $customerId = $user->userId;

                //update customer phone
                $params = array('phone' => $params['telephone']);
                $params['customer_id']  = $user->userId;
                $params['type']         = 'set';
                $this->setDataToPost($params);
                $this->submitPostToApi('customer/index/personal');

            } else {
                //customer id from, session
                $customerId = $customer['entity_id'];
                if (isset($input['new_shipping_address']) && $input['new_shipping_address'] == 'shipping') {
                    //update user address
                    $params = array();
                    $params['customer_id']  = $customerId;
                    $params['type']         = 'set';
                    $params['address_type'] = 'shipping';
                    $params['vat_id']       = '';
                    $params['company']      = '';
                    $params['street']       = $input['street'];
                    $params['country_id']   = $countryId;
                    $params['postcode']     = $input['postcode'];
                    $params['city']         = $input['city'];
                    $params['telephone']    = $input['country_code'].$input['contact_no'];
                    $_address               = $params;
                    //set billing
                    $this->setDataToPost($params);
                    $this->submitPostToApi('customer/index/address');

                    //set shipping
                    $params['address_type'] = 'billing';
                    if(!$shipSame)
                    {
                        $params['street']       = $input['street_bill'];
                        $params['country_id']   = $countryIdBill;
                        $params['postcode']     = $input['postcode_bill'];
                        $params['city']         = $input['city_bill'];
                    }

                    $this->setDataToPost($params);
                    $this->submitPostToApi('customer/index/address');
                }
            }
            //get customer address ids, shipping address
            $addressParams = array(
                'customer_id'   => $customerId,
                'type'          => 'get',
                'address_type'  => 'billing'
            );

            $this->setDataToPost($addressParams);
            $this->submitPostToApi('customer/index/address');
            $response = $this->getApiResponse();
            $billing = json_decode($response, true);

            $addressParams['address_type'] = 'shipping';
            $this->setDataToPost($addressParams);
            $this->submitPostToApi('customer/index/address');
            $response = $this->getApiResponse();
            $shipping = json_decode($response, true);
            if(empty($billing['data']['street'])) {
                $params = array();
                $params['customer_id']  = $customerId;
                $params['type']         = 'set';
                $params['address_type'] = 'billing';
                $params['vat_id']       = '';
                $params['company']      = '';
                $params['street']       = $shipping['data']['street'];
                $params['country_id']   = $shipping['data']['country_id'];
                $params['postcode']     = $shipping['data']['postcode'];
                $params['city']         = $shipping['data']['city'];
                $params['telephone']    = isset($shipping['data']['telephone'])?$shipping['data']['telephone']: "0000000000";

                $this->setDataToPost($params);
                $this->submitPostToApi('customer/index/address');

                $addressParams = array(
                    'customer_id'   => $customerId,
                    'type'          => 'get',
                    'address_type'  => 'billing'
                );

                $this->setDataToPost($addressParams);
                $this->submitPostToApi('customer/index/address');
                $response = $this->getApiResponse();
                $billing = json_decode($response, true);
            }
            //var_dump($shipping);

            // Set customer, for example guest
            $resultCustomerSet = TicketSoap::process('cart_customer.set', array( $shoppingCartId, array(
                'entity_id' => $customerId,
                'mode'      => 'customer'
            )));

            // Log::info(var_export($customer, true));
            // Log::info(var_export($shipping, true));
            // Log::info(var_export($billing, true));

            /*
            TicketSoap::process("customer_address.update", array(
                'addressId' =>  $shipping['data']['entity_id'],
                'firstname' =>  $customer['firstname'],
                'lastname'  =>  $customer['lastname']
            ));
            */
            TicketSoap::process("customer_address.update", array(
                'addressId' =>  $billing['data']['entity_id'],
                'addressdata' => array(
                    'firstname' =>  $customer['firstname'],
                    'lastname'  =>  $customer['lastname']
                )
            ));

            TicketSoap::process("customer_address.update", array(
                'addressId' =>  $shipping['data']['entity_id'],
                'addressdata' => array(
                    'firstname' =>  $input['first_name'],
                    'lastname'  =>  $input['last_name']
                )
            ));

            $arrAddresses = array(
                array(
                    "mode" => "shipping",
                    "address_id" => $shipping['data']['entity_id'],
                ),
                array(
                    "mode" => "billing",
                    "address_id" => $billing['data']['entity_id'],
                )
            );

            //address added to checkout
            $resultCustomerAddresses = TicketSoap::process("cart_customer.addresses", array($shoppingCartId, $arrAddresses ));
            // product added to checkout
            $arrProducts = array(
                array(
                    "product_id" => $id,
                    "qty" => $input['qty']
                )
            );
            $resultCartProductAdd = TicketSoap::process("cart_product.add", array($shoppingCartId, $arrProducts));
            $shoppingCartProducts = TicketSoap::process("cart_product.list", array($shoppingCartId));


            $shippingMethd = isset($input['other_country']) && $input['other_country'] != ''? $input['other_country'] : 'flatrate';

            $shipping  = TicketSoap::process("cart_shipping.method", array($shoppingCartId, "{$shippingMethd}_{$shippingMethd}"));


            // get total Amount
            $resultTotalAmount = TicketSoap::process("cart.totals", $shoppingCartId);

            $totalAmount = 0;

            foreach($resultTotalAmount as $_amount) {
                if($_amount['title'] == 'Grand Total') {
                    $totalAmount = $_amount;
                }
            }

            $totalGatewayAmount  = number_format ($totalAmount['amount'], 2, '', '');

            $sessionProductInfo = array();
            $userIP = App::environment('local', 'stage') ? '81.137.255.206' : $_SERVER['REMOTE_ADDR'];
            $country = App::environment('local', 'stage') ?  'LV': $billing['data']['country_id'];
            $currency = App::environment('local', 'stage') ?  'USD': 'GBP';

            $sessionProductInfo['shoppingCartId'] = $shoppingCartId;

            $sessionProductInfo['order'] = array(
                'order_id'          => '',
                'buyer_id'          => $customerId ,
                'event_id'          => '',
                'product_id'        => $id,
                'qty'               => $input['qty'],
                'amount'            => '',
                'delivery_status'   => 'pending'
            );

            if(Input::has('voucherCode'))
            {
                $coupon = Input::get('voucherCode');
                try
                {
                    $addedCoupon = TicketSoap::process("cart_coupon.add", array($shoppingCartId, $coupon));
                    if(!$addedCoupon)
                    {

                    }
                }
                catch(Exception $e)
                {

                }
            }

            if(Session::has('refCode'))
            {
                $refCode = Session::get('refCode');

                $affil = AffiliateProperty::where('ref_code', $refCode)->first();

                $perc = $affil->getCurrentRate();

                $rid = null;

                if(Session::has('rid'))
                {

                    $rid = Session::get('rid');
                    Log::info($rid);
                }
                else
                {
                    Log::info("Didnt work");
                }

                $sessionProductInfo['af_id'] = $affil->id;
                $sessionProductInfo['com_perc'] = $perc;
                $sessionProductInfo['rid'] = $rid;
            }

            $ses = Session::all();
            if(Config::get('transactpro.COD_Enable') ==  true) {
                $paymentMethod = array(
                    "method" => "checkmo"
                );
                $resultPaymentMethod = TicketSoap::process("cart_payment.method", array($shoppingCartId, $paymentMethod));
                $uid = uniqid();
                Session::put('mid_'.$uid, json_encode($sessionProductInfo));
                $response = Response::make(json_encode(array('_3d'=>'no', 'link'=> '/gateway/cod?merchant_transaction_id='.$uid )), '200');
                $response->header('Content-Type', 'application/json');
                return $response;
            }

            //transaction_pro
            if(Options::getOption('payment_gateway') == 'tp') {
                //payment gateway
                $auth = Config::get('transactpro.auth');
                $gateClient = new GateClient($auth);

                $transaction = new Transaction();
                $merchantId = str_pad ($shoppingCartId, 20, '0', STR_PAD_LEFT);

                $transaction->order_id = $merchantId;

                $gatewayAgs = array(
                    'rs'                      => Config::get('transactpro.rs'),
                    'merchant_transaction_id' => $merchantId,
                    'user_ip'                 => $userIP,
                    'description'             => $_ticket->title,
                    'amount'                  => $totalGatewayAmount,
                    'currency'                => $currency,
                    'name_on_card'            => $nameOnCard,
                    'street'                  => $billing['data']['street'],
                    'zip'                     => $billing['data']['postcode'],
                    'city'                    => $billing['data']['city'],
                    'country'                 => $country,
                    'state'                   => 'NA',
                    'email'                   => $customerEmail,
                    'phone'                   => isset($billing['data']['telephone']) ? $billing['data']['telephone'] : "0000000000",
                    'merchant_site_url'       => Config::get('transactpro.merchant_url')
                );

                $response = $gateClient->init($gatewayAgs);


                if(preg_match('/~RedirectOnsite:/', $response->getResponseContent())
                ) {
                    $responseArr = explode('~', $response->getResponseContent());
                    $respContentArray = explode(':',$responseArr[0]);
                    $sessionProductInfo['transactionId'] = $respContentArray[1];
                    $transaction->tid = $respContentArray[1];
                    $transaction->status = 0;

                    $transaction->order = json_encode($sessionProductInfo);
                    $transaction->save();
                    $redirection = str_replace('RedirectOnsite:', '', $respContentArray[1]);

                    $paymentMethod = array(
                        "method" => "checkmo"
                    );

                    $resultPaymentMethod = TicketSoap::process("cart_payment.method", array($shoppingCartId, $paymentMethod));

                    $redirection = explode('tid=', $responseArr[1]);
                    Session::put('mid_'.$merchantId, json_encode($sessionProductInfo));
                    $response = Response::make(json_encode(array('_3d'=>'yes', 'link'=> route('checkout.payment', array('tid'=>$redirection[1], 'input' => Input::except('VoucherCode'), 'id' => $id)))), '200');
                    $response->header('Content-Type', 'application/json');
                    return $response;



                }
                else if($response instanceof TransactPRO\Gate\Response\Response
                    && $response->isSuccessful()
                    && !preg_match('/ERROR:/', $response->getResponseContent())
                ) {



                    $responseContent = $response->getResponseContent();
                    $respContentArray = explode(':',$responseContent);
                    $tid = $respContentArray[1];
                    $sessionProductInfo['transactionId'] = $tid;


                    $response = $gateClient->makeHold(array(
                        'f_extended'          => '5',
                        'init_transaction_id' => $tid,
                        'cc'                  => $input['card_number'],
                        'cvv'                 => $input['card_cvv'],
                        'expire'              => $input['card_expire']
                    ));

                    $responseContent = $response->getResponseContent();

                    if(preg_match('/Redirect:/', $responseContent) && $response->isSuccessful())  {
                        $maonthYearARR = explode('/',$input['card_expire']);
                        $paymentMethod = array(
                            "method" => "checkmo"
                        );

                        $resultPaymentMethod = TicketSoap::process("cart_payment.method", array($shoppingCartId, $paymentMethod));
                        $redirection = str_replace('Redirect:', '', $responseContent);
                        $response = Response::make(json_encode(array('_3d'=>'yes', 'link'=> $redirection )), '200');
                        $response->header('Content-Type', 'application/json');
                        Session::put('mid_'.$merchantId, json_encode($sessionProductInfo));
                        return $response;
                    } else if ($response->isSuccessful()){
                        $paymentMethod = array(
                            "method" => "checkmo"
                        );
                        $resultPaymentMethod = TicketSoap::process("cart_payment.method", array($shoppingCartId, $paymentMethod));
                        $response = Response::make(json_encode(array('_3d'=>'no', 'link'=> '/gateway/response?merchant_transaction_id='.$shoppingCartId )), '200');
                        $response->header('Content-Type', 'application/json');
                        Session::put('mid_'.$merchantId, json_encode($sessionProductInfo));
                        return $response;
                    } else {
                        throw new Exception( json_encode($response));
                    }

                } else {
                    throw new Exception( json_encode( $response));
                }
            } else if(Options::getOption('payment_gateway') == 'g2s') {
                $cart = TicketSoap::process("cart.info", array($shoppingCartId));

                //g2s programming

                $transaction = new Transaction();
                $merchantId = str_pad ($shoppingCartId, 20, '0', STR_PAD_LEFT);

                $transaction->order_id = $merchantId;


                $item_price     = $_ticket->price;
                $handling       = $cart['base_grand_total'] - ($_ticket->price * $input['qty']);
                if($handling < 0)
                {
                    $handling = 0;
                }

                $post = array();
                $post['merchant_site_id']   = Config::get('gate2shop.merchant_site_id');
                $post['merchant_id']        = Config::get('gate2shop.merchant_id');
                $post['time_stamp']         = gmdate('Y-m-d.H:i:s');
                $post['currency']           = Config::get('gate2shop.currency');
                $post['total_amount']       = number_format ($cart['base_grand_total'], 2, '.', '');
                $post['numberofitems']      = 1;

                $post['item_name_1']        = $_ticket->title;
                $post['encoding']           = 'utf-8';
                $post['item_number_1']      = 1;
                $post['item_quantity_1']    = $input['qty'];
                $post['item_amount_1']      = number_format ($item_price, 2, '.', '');

                $post['first_name']         = $customer['firstname'];
                $post['last_name']          = $customer['lastname'];
                $post['email']              = $customerEmail;
                $post['user_token_id']      = $customerEmail;
                $post['user_token']         = 'auto';
                $post['version']            = '4.0.0';
                $post['handling']           = number_format ($handling, 2, '.', '');
                $post['merchant_unique_id'] = $merchantId;

                $post['success_url']        = Config::get('app.url').'gateway/success';
                $post['notify_url']         = Config::get('app.url').'gateway/response';
                $post['error_url']          = Config::get('app.url').'gateway/error';
                $post['pending_ur']         = Config::get('app.url').'gateway/pending';
                $post['back_url']           = Config::get('app.url').'gateway/back';
                $post['url']                = Config::get('app.url');


                array_map('trim', $post);

                $JoinedInfo = join('', array_values($post));
                $post["checksum"] = md5(Config::get('gate2shop.secret_string') . $JoinedInfo);

                $fields_string="";

                foreach($post as $key=>$value) {
                    $fields_string .= $key."=".urlencode($value)."&";
                }

                $fields_string = rtrim($fields_string, "&");

                $redirect_link = Config::get('gate2shop.server_URL').$fields_string;

                $sessionProductInfo['redirect_link'] = $redirect_link;
                $sessionProductInfo['post'] = $post;
                $sessionProductInfo['product_id'] = $id ;

                if(Session::has('refCode'))
                {
                    $refCode = Session::get('refCode');

                    $affil = AffiliateProperty::where('ref_code', $refCode)->first();

                    $perc = $affil->getCurrentRate();

                    $sessionProductInfo['af_id'] = $affil->id;
                    $sessionProductInfo['com_perc'] = $perc;
                }


                Session::put('mid_'.$shoppingCartId, json_encode($sessionProductInfo));

                $transaction->tid = $shoppingCartId;
                $transaction->status = 0;

                $transaction->order = json_encode($sessionProductInfo);
                $transaction->save();

                $paymentMethod = array(
                    "method" => "checkmo"
                );
                $resultPaymentMethod = TicketSoap::process("cart_payment.method", array($shoppingCartId, $paymentMethod));

                $response = Response::make(json_encode(array('_3d'=>'yes', 'link'=> route('checkout.payment', array('tid'=>$shoppingCartId, 'input' => Input::except('VoucherCode'), 'id' => $id), '200'))));
                //$response = Response::make(json_encode(array('_3d'=>'yes', 'link'=> $redirect_link )), '200');
                $response->header('Content-Type', 'application/json');
                return $response;

        }
        else {
                throw new Exception('Unable process your order. Payment gateway is not selected.');
            }





        } catch (Exception $e) {

            $shoppingCartId = TicketSoap::process('cart.create', array('default'));
            Session::put($id.'_shoppingCartId', $shoppingCartId);
            $errorMessage = $e->getMessage();
            if(is_array($errorMessage)) {
                $response = Response::make(json_encode($errorMessage), '400');
            } else {
                $response = Response::make($errorMessage, '400');
            }

            $response->header('Content-Type', 'application/json');
            Log::error( $e->getTraceAsString());
            return $response;
        }
    }


    public function order2($id) {
        $customer = Session::get('customer');
        $input = Input::all();

        $shoppingCartId = Session::get($id.'_shoppingCartId');
        $_ticket = RelatedTicket::getTicketByTicketId($id);
        $_ticket->price = round($_ticket->price, 2);

        try
        {
            $cart = TicketSoap::process("cart.info", array($shoppingCartId));
            $store_id = $cart['store_id'];
            if($cart)
            {
                $itemArray = array();

                foreach($cart['items'] as $item)
                {
                    TicketSoap::process("cart_product.remove", array($shoppingCartId, array(array('product_id' => $item['product_id'],'sku'=>$item['sku'], 'quantity'=>$item['qty'], 'options' => array())), $store_id));
                }

            }
        }
        catch(Exeption $e)
        {
            \Log::info("Couldnt remove items from cart");
        }

        if($_ticket->ticket_status != 'active')
        {
            throw new Exception( json_encode("Error."));
        }

        $shipSame = intval(Input::get('ship-same'));
        $countryId = isset($input['country_id']) ? $input['country_id'] : 'GB';
        $countryIdBill = isset($input['country_id_bill']) ? $input['country_id_bill'] : 'GB';
        $nameOnCard = isset($customer['firstname'])? $customer['firstname']." ".$customer['lastname'] : $input['first_name']." ".$input['last_name'];
        $customerEmail = isset($customer['email'])? $customer['email']: '';
        $hotelString = "";
        $hotelMethods = array("flatrate3", "flatrate6", "flatrate8", "flatrate10", "flatrate12");

        $rName = Input::get("reservation_name");
        $hName = Input::get('hotel_name');
        $cIn = Input::get('checkin_dt');
        $cOut = Input::get('checkout_dt');

        if (!empty($rName)) {
            $hotelString = "Reservation Name: ".$rName.'; Check-in time:'.$cIn.'; Check-out time:'.$cOut.' Hotel name: '.$hName.';';

        }

        try {
            //validating card info:
            // if(Config::get('transactpro.COD_Enable') ==  true) {
            //     CreditCard::validateCardNumber($input['card_number']);
            //     CreditCard::validateExpireDate($input['card_expire']);
            //     CreditCard::validateCVV($input['card_cvv']);
            // }

            if (!$customer) {
                //Log::info('New Customer Request');
                $newCustomer = array(
                    'first_name'            => $input['first_name'],
                    'last_name'             => $input['last_name'],
                    'email'                 => $input['email'],
                    'email_confirmation'    => $input['email_confirmation'],
                    'password_confirmation' => $input['password_confirmation'],
                    'password'              => $input['password'],
                    'terms_n_conditions'    => true,
                    'contact_no'            => $input['contact_no']
                );

                $this->setDataToPost($newCustomer);
                $response = $this->submitPostToApi( route('ticket.registrationsInternal'),false)->getApiResponse();
                $user = json_decode($response);

                if(!isset($user->message) || $user->message != "user successfully created.") {
                    $response = json_decode($response, true);
                    throw new Exception(print_r( $response));
                }
                $customerEmail = $input['email'];

                //update user address
                $params = array();
                $params['customer_id']  = $user->userId;
                $params['type']         = 'set';
                $params['address_type'] = 'shipping';
                $params['vat_id']       = '';
                $params['company']      = '';
                $params['street']       = $input['street'];


                $params['country_id']   = $countryId;
                $params['postcode']     = $input['postcode'];
                $params['city']         = $input['city'];
                $params['telephone']    = $input['country_code'].$input['contact_no'];
                $_address               = $params;
                //set billing
                $this->setDataToPost($params);
                $this->submitPostToApi('customer/index/address');

                //set shipping
                $params['address_type'] = 'billing';
                if(!$shipSame)
                {
                    $params['street']       = $input['street_bill'];
                    $params['country_id']   = $countryIdBill;
                    $params['postcode']     = $input['postcode_bill'];
                    $params['city']         = $input['city_bill'];
                }

                if(!empty($hotelString) && $hotelString != ''){
                    $params['street']  .= $params['street'] ;
                }


                $this->setDataToPost($params);
                $this->submitPostToApi('customer/index/address');
                $customerId = $user->userId;

                //update customer phone
                $params = array('phone' => $params['telephone']);
                $params['customer_id']  = $user->userId;
                $params['type']         = 'set';
                $this->setDataToPost($params);
                $this->submitPostToApi('customer/index/personal');

            } else {
                //customer id from, session
                $customerId = $customer['entity_id'];
                if (isset($input['new_shipping_address']) && $input['new_shipping_address'] == 'shipping') {
                    //update user address
                    $params = array();
                    $params['customer_id']  = $customerId;
                    $params['type']         = 'set';
                    $params['address_type'] = 'shipping';
                    $params['vat_id']       = '';
                    $params['company']      = '';
                    $params['street']       = $input['street'];
                    $params['country_id']   = $countryId;
                    $params['postcode']     = $input['postcode'];
                    $params['city']         = $input['city'];
                    $params['telephone']    = $input['country_code'].$input['contact_no'];
                    $_address               = $params;
                    //set billing
                    $this->setDataToPost($params);
                    $this->submitPostToApi('customer/index/address');

                    //set shipping
                    $params['address_type'] = 'billing';
                    if(!$shipSame)
                    {
                        $params['street']       = $input['street_bill'];
                        $params['country_id']   = $countryIdBill;
                        $params['postcode']     = $input['postcode_bill'];
                        $params['city']         = $input['city_bill'];
                    }

                    $this->setDataToPost($params);
                    $this->submitPostToApi('customer/index/address');
                }
            }
            //get customer address ids, shipping address
            $addressParams = array(
                'customer_id'   => $customerId,
                'type'          => 'get',
                'address_type'  => 'billing'
            );

            $this->setDataToPost($addressParams);
            $this->submitPostToApi('customer/index/address');
            $response = $this->getApiResponse();
            $billing = json_decode($response, true);

            $addressParams['address_type'] = 'shipping';
            $this->setDataToPost($addressParams);
            $this->submitPostToApi('customer/index/address');
            $response = $this->getApiResponse();
            $shipping = json_decode($response, true);
            if(empty($billing['data']['street'])) {
                $params = array();
                $params['customer_id']  = $customerId;
                $params['type']         = 'set';
                $params['address_type'] = 'billing';
                $params['vat_id']       = '';
                $params['company']      = '';
                $params['street']       = $shipping['data']['street'];
                $params['country_id']   = $shipping['data']['country_id'];
                $params['postcode']     = $shipping['data']['postcode'];
                $params['city']         = $shipping['data']['city'];
                $params['telephone']    = isset($shipping['data']['telephone'])?$shipping['data']['telephone']: "0000000000";

                $this->setDataToPost($params);
                $this->submitPostToApi('customer/index/address');

                $addressParams = array(
                    'customer_id'   => $customerId,
                    'type'          => 'get',
                    'address_type'  => 'billing'
                );

                $this->setDataToPost($addressParams);
                $this->submitPostToApi('customer/index/address');
                $response = $this->getApiResponse();
                $billing = json_decode($response, true);
            }
            // var_dump($shipping);

            // Set customer, for example guest
            $resultCustomerSet = TicketSoap::process('cart_customer.set', array( $shoppingCartId, array(
                'entity_id' => $customerId,
                'mode'      => 'customer'
            )));

            // Log::info(var_export($customer, true));
            // Log::info(var_export($shipping, true));
            // Log::info(var_export($billing, true));

            /*TicketSoap::process("customer_address.update", array(
                'addressId' =>  $shipping['data']['entity_id'],
                'firstname' =>  $customer['firstname'],
                'lastname'  =>  $customer['lastname']
            ));
            */
            TicketSoap::process("customer_address.update", array(
                'addressId' =>  $billing['data']['entity_id'],
                'addressdata' => array(
                    'firstname' =>  $customer['firstname'],
                    'lastname'  =>  $customer['lastname']
                )
            ));

            TicketSoap::process("customer_address.update", array(
                'addressId' =>  $shipping['data']['entity_id'],
                'addressdata' => array(
                    'firstname' =>  $input['first_name'],
                    'lastname'  =>  $input['last_name']
                )
            ));

            $arrAddresses = array(
                array(
                    "mode" => "shipping",
                    "address_id" => $shipping['data']['entity_id'],
                ),
                array(
                    "mode" => "billing",
                    "address_id" => $billing['data']['entity_id'],
                )
            );

            //address added to checkout
            $resultCustomerAddresses = TicketSoap::process("cart_customer.addresses", array($shoppingCartId, $arrAddresses ));
            // product added to checkout
            $arrProducts = array(
                array(
                    "product_id" => $id,
                    "qty" => $input['qty']
                )
            );
            $resultCartProductAdd = TicketSoap::process("cart_product.add", array($shoppingCartId, $arrProducts));
            $shoppingCartProducts = TicketSoap::process("cart_product.list", array($shoppingCartId));


            $shippingMethd = isset($input['other_country']) && $input['other_country'] != ''? $input['other_country'] : 'flatrate';

            $shipping  = TicketSoap::process("cart_shipping.method", array($shoppingCartId, "{$shippingMethd}_{$shippingMethd}"));
            if(Input::has('voucherCode'))
            {
                $coupon = Input::get('voucherCode');
                try
                {
                    $addedCoupon = TicketSoap::process("cart_coupon.add", array($shoppingCartId, $coupon));
                    if(!$addedCoupon)
                    {
                        \Log::info("CANT ADD VOUCHER CODE");
                    }
                }
                catch(Exception $e)
                {
                    \Log::info("AN EXCEPTION");
                }
            }

            // get total Amount
            $resultTotalAmount = TicketSoap::process("cart.totals", $shoppingCartId);

            $totalAmount = 0;

            foreach($resultTotalAmount as $_amount) {
                if($_amount['title'] == 'Grand Total') {
                    $totalAmount = $_amount;
                }
            }

            $totalGatewayAmount  = number_format($totalAmount['amount'], 2, '', '');

            $sessionProductInfo = array();

            $userIP = App::environment('local', 'stage') ? '81.137.255.206' : $_SERVER['REMOTE_ADDR'];
            $country = App::environment('local', 'stage') ?  'LV': $billing['data']['country_id'];
            $currency = App::environment('local', 'stage') ?  'USD': 'GBP';

            $sessionProductInfo['shoppingCartId'] = $shoppingCartId;

            $sessionProductInfo['order'] = array(
                'order_id'          => '',
                'buyer_id'          => $customerId ,
                'event_id'          => '',
                'product_id'        => $id,
                'qty'               => $input['qty'],
                'amount'            => '',
                'delivery_status'   => 'pending'
            );


            if(Session::has('refCode'))
            {
                $refCode = Session::get('refCode');

                $affil = AffiliateProperty::where('ref_code', $refCode)->first();

                $perc = $affil->getCurrentRate();

                $sessionProductInfo['af_id'] = $affil->id;
                $sessionProductInfo['com_perc'] = $perc;
            }

            if(Config::get('transactpro.COD_Enable') ==  true) {
                $paymentMethod = array(
                    "method" => "checkmo"
                );
                $resultPaymentMethod = TicketSoap::process("cart_payment.method", array($shoppingCartId, $paymentMethod));
                $uid = uniqid();
                Session::put('mid_'.$uid, json_encode($sessionProductInfo));
                $response = Response::make(json_encode(array('_3d'=>'no', 'link'=> '/gateway/cod?merchant_transaction_id='.$uid )), '200');
                $response->header('Content-Type', 'application/json');
                return $response;
            }

            //transaction_pro
            if(Options::getOption('payment_gateway') == 'tp') {
                //payment gateway
                $auth = Config::get('transactpro.auth');
                $gateClient = new GateClient($auth);

                $transaction = new Transaction();
                $merchantId = str_pad ($shoppingCartId, 20, '0', STR_PAD_LEFT);

                $transaction->order_id = $merchantId;

                $gatewayAgs = array(
                    'rs'                      => Config::get('transactpro.rs'),
                    'merchant_transaction_id' => $merchantId,
                    'user_ip'                 => $userIP,
                    'description'             => $_ticket->title,
                    'amount'                  => $totalGatewayAmount,
                    'currency'                => $currency,
                    'name_on_card'            => $nameOnCard,
                    'street'                  => $billing['data']['street'],
                    'zip'                     => $billing['data']['postcode'],
                    'city'                    => $billing['data']['city'],
                    'country'                 => $country,
                    'state'                   => 'NA',
                    'email'                   => $customerEmail,
                    'phone'                   => isset($billing['data']['telephone']) ? $billing['data']['telephone'] : "0000000000",
                    'merchant_site_url'       => Config::get('transactpro.merchant_url')
                );


                $response = $gateClient->init($gatewayAgs);


                if(preg_match('/~RedirectOnsite:/', $response->getResponseContent())
                ) {
                    $responseArr = explode('~', $response->getResponseContent());
                    $respContentArray = explode(':',$responseArr[0]);
                    $sessionProductInfo['transactionId'] = $respContentArray[1];
                    $transaction->tid = $respContentArray[1];
                    $transaction->status = 0;

                    $transaction->order = json_encode($sessionProductInfo);
                    $transaction->save();
                    $redirection = str_replace('RedirectOnsite:', '', $respContentArray[1]);

                    $paymentMethod = array(
                        "method" => "checkmo"
                    );

                    $resultPaymentMethod = TicketSoap::process("cart_payment.method", array($shoppingCartId, $paymentMethod));

                    $redirection = explode('tid=', $responseArr[1]);
                    Session::put('mid_'.$merchantId, json_encode($sessionProductInfo));
                    $response = Response::make(json_encode(array('_3d'=>'yes', 'link'=> route('checkout.payment', array('tid'=>$redirection[1], 'input' => Input::except('VoucherCode'), 'id' => $id)))), '200');
                    $response->header('Content-Type', 'application/json');
                    return $response;



                }
                else if($response instanceof TransactPRO\Gate\Response\Response
                    && $response->isSuccessful()
                    && !preg_match('/ERROR:/', $response->getResponseContent())
                ) {



                    $responseContent = $response->getResponseContent();
                    $respContentArray = explode(':',$responseContent);
                    $tid = $respContentArray[1];
                    $sessionProductInfo['transactionId'] = $tid;


                    $response = $gateClient->makeHold(array(
                        'f_extended'          => '5',
                        'init_transaction_id' => $tid,
                        'cc'                  => $input['card_number'],
                        'cvv'                 => $input['card_cvv'],
                        'expire'              => $input['card_expire']
                    ));

                    $responseContent = $response->getResponseContent();

                    if(preg_match('/Redirect:/', $responseContent) && $response->isSuccessful())  {
                        $maonthYearARR = explode('/',$input['card_expire']);
                        $paymentMethod = array(
                            "method" => "checkmo"
                        );

                        $resultPaymentMethod = TicketSoap::process("cart_payment.method", array($shoppingCartId, $paymentMethod));
                        $redirection = str_replace('Redirect:', '', $responseContent);
                        $response = Response::make(json_encode(array('_3d'=>'yes', 'link'=> $redirection )), '200');
                        $response->header('Content-Type', 'application/json');
                        Session::put('mid_'.$merchantId, json_encode($sessionProductInfo));
                        return $response;
                    } else if ($response->isSuccessful()){
                        $paymentMethod = array(
                            "method" => "checkmo"
                        );
                        $resultPaymentMethod = TicketSoap::process("cart_payment.method", array($shoppingCartId, $paymentMethod));
                        $response = Response::make(json_encode(array('_3d'=>'no', 'link'=> '/gateway/response?merchant_transaction_id='.$shoppingCartId )), '200');
                        $response->header('Content-Type', 'application/json');
                        Session::put('mid_'.$merchantId, json_encode($sessionProductInfo));
                        return $response;
                    } else {
                        throw new Exception( json_encode($response));
                    }

                } else {
                    throw new Exception( json_encode( $response));
                }
            } else if(Options::getOption('payment_gateway') == 'g2s') {
                $cart = TicketSoap::process("cart.info", array($shoppingCartId));

                //g2s programming

                $transaction = new Transaction();
                $merchantId = str_pad ($shoppingCartId, 20, '0', STR_PAD_LEFT);

                $transaction->order_id = $merchantId;


                $item_price     = $_ticket->price;
                $handling       = $cart['base_grand_total'] - ($_ticket->price * $input['qty']);
                if($handling < 0)
                {
                    $handling = 0;
                }

                $post = array();
                $post['merchant_site_id']   = Config::get('gate2shop.merchant_site_id');
                $post['merchant_id']        = Config::get('gate2shop.merchant_id');
                $post['time_stamp']         = gmdate('Y-m-d.H:i:s');
                $post['currency']           = Config::get('gate2shop.currency');
                $post['total_amount']       = number_format ($cart['base_grand_total'], 2, '.', '');
                $post['numberofitems']      = 1;

                $post['item_name_1']        = $_ticket->title;
                $post['encoding']           = 'utf-8';
                $post['item_number_1']      = 1;
                $post['item_quantity_1']    = $input['qty'];
                $post['item_amount_1']      = number_format ($item_price, 2, '.', '');

                $post['first_name']         = $customer['firstname'];
                $post['last_name']          = $customer['lastname'];
                $post['email']              = $customerEmail;
                $post['user_token_id']      = $customerEmail;
                $post['user_token']         = 'auto';
                $post['version']            = '4.0.0';
                $post['handling']           = number_format ($handling, 2, '.', '');
                $post['merchant_unique_id'] = $merchantId;

                $post['success_url']        = Config::get('app.url').'gateway/success';
                $post['notify_url']         = Config::get('app.url').'gateway/response';
                $post['error_url']          = Config::get('app.url').'gateway/error';
                $post['pending_ur']         = Config::get('app.url').'gateway/pending';
                $post['back_url']           = Config::get('app.url').'gateway/back';
                $post['url']                = Config::get('app.url');


                array_map('trim', $post);

                $JoinedInfo = join('', array_values($post));
                $post["checksum"] = md5(Config::get('gate2shop.secret_string') . $JoinedInfo);

                $fields_string="";

                foreach($post as $key=>$value) {
                    $fields_string .= $key."=".urlencode($value)."&";
                }

                $fields_string = rtrim($fields_string, "&");

                $redirect_link = Config::get('gate2shop.server_URL').$fields_string;

                $sessionProductInfo['redirect_link'] = $redirect_link;
                $sessionProductInfo['post'] = $post;
                $sessionProductInfo['product_id'] = $id ;

                if(Session::has('refCode'))
                {
                    $refCode = Session::get('refCode');

                    $affil = AffiliateProperty::where('ref_code', $refCode)->first();

                    $perc = $affil->getCurrentRate();

                    $sessionProductInfo['af_id'] = $affil->id;
                    $sessionProductInfo['com_perc'] = $perc;
                }


                Session::put('mid_'.$shoppingCartId, json_encode($sessionProductInfo));

                $transaction->tid = $shoppingCartId;
                $transaction->status = 0;

                $transaction->order = json_encode($sessionProductInfo);
                $transaction->save();

                $paymentMethod = array(
                    "method" => "checkmo"
                );
                $resultPaymentMethod = TicketSoap::process("cart_payment.method", array($shoppingCartId, $paymentMethod));

                $response = Response::make(json_encode(array('_3d'=>'yes', 'link'=> route('checkout.payment', array('tid'=>$shoppingCartId, 'input' => Input::except('VoucherCode'), 'id' => $id), '200'))));
                //$response = Response::make(json_encode(array('_3d'=>'yes', 'link'=> $redirect_link )), '200');
                $response->header('Content-Type', 'application/json');
                return $response;

            } else {
                throw new Exception('Unable process your order. Payment gateway is not selected.');
            }





        } catch (Exception $e) {

            $shoppingCartId = TicketSoap::process('cart.create', array('default'));
            Session::put($id.'_shoppingCartId', $shoppingCartId);
            Log::error( $e->getTraceAsString());
            $errorMessage = $e->getMessage();
            if(is_array($errorMessage)) {
                $response = Response::make(json_encode($errorMessage), '400');
            } else {
                $response = Response::make($errorMessage, '400');
            }

            $response->header('Content-Type', 'application/json');
            return $response;
        }
    }

    public function getShipping() {
       if (Cache::has('shipping')) {

            return Cache::get('shipping');

        } else {

            $shipping = file_get_contents(Config::get("api.mage_soap_api_url").'customer/shipping/index');
            Cache::put('shipping', $shipping, 600);
            return $shipping;
        }
    }

    private function getNumberOfTicketDisplay($qty, $preference) {
        $output = array();

        switch($preference) {
            case 1:
                $output[$qty] = $qty;
                break;

            case 2:
                for($i=1; $i<=$qty; $i++)
                {
                    if(($qty - $i) != 1)
                    {
                        $output[$i]=$i;
                    }
                }
                break;

            case 3:
                for($i=1; $i<=$qty; $i++) {
                    if($i%2 == 0) {
                        $output[$i] = $i;
                    }
                }
                break;

            default:
                for($i=1; $i<=$qty; $i++) {
                    $output[$i] = $i;
                }
        }

        return $output;
    }

    public function getShippingMethods($id, $rtID, $delCountry){
        $hdAvailable = 0;
        $isEticket = 0;
        $requiresInternational = 0;
        $locale = App::getLocale();



        $rt = RelatedTicket::where('product_id', '=', intval($rtID))->first();
        $hdAvailable = $rt->hand_delivery;

        $ticket = $rt->ticket;
        $ticket=json_decode($ticket);
        $form = DB::table('events_form_of_ticket')
            ->where('id', $ticket->ticketInformation->form_of_ticket)
            ->first();

        if($form->title == "E-tickets"){
            $isEticket = 1;
        }


        $event = FootBallEvent::findOrFail(intval($id));
        $home = $event->home_team_id;
        $away = $event->away_team_id;

        $homeCountry = DB::table('football_ticket_meta')
            ->where('football_ticket_id', $home)
            ->where('key', 'country')
            ->first();

        $awayCountry = DB::table('football_ticket_meta')
            ->where('football_ticket_id', $away)
            ->where('key', 'country')
            ->first();

        $shipping = 0;

        if (Cache::has('shipping')){
            $shipping = Cache::get('shipping');
        }
        else{
            $shipping = file_get_contents(Config::get("api.mage_soap_api_url").'customer/shipping/index');
            $shipping = json_decode($shipping);
            Cache::put('shipping', $shipping, 600);
        }

        if(($homeCountry->value) && ($awayCountry->value)){
            $homeCountry = DB::table('football_ticket')
                ->where('id', $homeCountry->value)
                ->first();

            $awayCountry = DB::table('football_ticket')
                ->where('id', $awayCountry->value)
                ->first();
        }
        else{
            return $shipping;
        }

        if(($delCountry != $homeCountry->title)&&($delCountry != $awayCountry->title)){
            $delCountry = "International";
            $requiresInternational = 1;
        }

        $now = new DateTime();
        $eventDateTime = $event->datetime;
        $eventDateTime = DateTime::createFromFormat('Y-m-d H:i:s', $eventDateTime);

        $dispatchedDate = $now;
        $requiredArrivalDate = $eventDateTime;
        $requiredArrivalDate->modify("-1 hour");
        $hour = $requiredArrivalDate->format('H');
        $requiredArrivalDate->setTime($hour, 0);

        $dispatchDay = $dispatchedDate->format('l');
        $arrivalDay = $requiredArrivalDate->format('l');

        $canBeDel = 1;
        $requiresHandOrHotel = 0;
        $requiresWeekend = 0;

        $workingDays = $this->weekday_diff($dispatchedDate->format('d-m-Y'), $requiredArrivalDate->format('d-m-Y'));

        $tt1 = DateTime::createFromFormat('Y-m-d', $dispatchedDate->format('Y-m-d'));
        $tt2 = DateTime::createFromFormat('Y-m-d', $requiredArrivalDate->format('Y-m-d'));

        if($isEticket &&($dispatchedDate < $requiredArrivalDate))
        {
            $availOptions = DB::table('shipping_method_assoc')
                ->where('id', 35)
                ->get();

            $shipping2 = new stdClass;
            $shipping2->totalResult=0;
            $shipping2->currentPage = 0;
            $shipping2->numPages = 0;
            $shipping2->totalPages=0;
            $shipping2->data = array();
            $i = 0;

            foreach($availOptions as $option){
                foreach($shipping->data as $entry){
                    if($option->name == $entry->value){
                        $pahLD = DB::table('locale_text')
                            ->where('id', 'ship_pah')
                            ->where('locale', $locale)
                            ->first();
                        if($pahLD)
                        {
                            $entry->label = $pahLD->lstring;
                        }
                        else
                        {
                            $entry->label = "Print at home";
                        }

                        $entry->sort = $option->sort_order;

                        $shipping2->data[] = $entry;
                        $i++;
                    }
                }
            }
            $shipping2->totalResult = $i;

            return $shipping2;
        }

        if($tt1 > $tt2){
            $canBeDel = 0;
        }
        else if($tt1 == $tt2){
            if($dispatchedDate->format('H') < $requiredArrivalDate->format('H')){
                $requiresHandOrHotel = 1;
            }
            else{
                $canBeDel = 0;
            }
        }

        if(($workingDays == 0)&&($tt1<$tt2)){
            $requiresHandOrHotel = 1;
        }
        else if($workingDays == 1)
        {
            if($dispatchDay == "Sunday"){
                $requiresHandOrHotel = 1;
            }
            if(($dispatchDay == "Friday")&&($arrivalDay == "Saturday")){
                $requiresWeekend = 1;
            }
        }

        if(($requiresHandOrHotel)&&(!$hdAvailable)){
            $canBeDel = 0;
        }

        $availOptions = 0;

        if(!$canBeDel){
            $shipping2 = new stdClass;
            $shipping2->totalResult=0;
            $shipping2->currentPage = 0;
            $shipping2->numPages = 0;
            $shipping2->totalPages=0;
            $shipping2->data = array();
            return $shipping2;
        }

        $maxDelHour = $requiredArrivalDate->format('H');
        if($requiresInternational){
            \Log::info('1');
            $availOptions = DB::table('shipping_method_assoc')
                ->where('id', 15)
                ->whereRaw('((working_days < '. $workingDays.') OR ((working_days = '. $workingDays .') AND (latest_at <= '. $maxDelHour .'))) AND (id != 35)')
                ->get();
        }
        else if($requiresWeekend){
            \Log::info('2');
            $availOptions = DB::table('shipping_method_assoc')
                ->whereIn('country', array($delCountry, "International"))
                ->whereRaw('((working_days < '. $workingDays.') OR ((working_days = '. $workingDays .') AND (latest_at <= '. $maxDelHour .'))) AND (id != 35)')
                ->where('isWeekend', '1')
                ->get();
        }
        else if($requiresHandOrHotel){
            \Log::info('3');
            $availOptions = DB::table('shipping_method_assoc')
                ->whereIn('country', array($delCountry, "International"))
                ->whereRaw('((working_days < '. $workingDays.') OR ((working_days = '. $workingDays .') AND (latest_at <= '. $maxDelHour .'))) AND (id != 35)')
                ->whereIn('id', array(3,16))
                ->get();
        }
        else if($hdAvailable){
            \Log::info('4');
            $availOptions = DB::table('shipping_method_assoc')
                ->whereIn('country', array($delCountry, "International"))
                ->whereRaw('((working_days < '. $workingDays.') OR ((working_days = '. $workingDays .') AND (latest_at <= '. $maxDelHour .'))) AND (id != 35)')
                ->get();

            \Log::info(var_export($availOptions,true));
        }
        else{
            \Log::info('5');
            $availOptions = DB::table('shipping_method_assoc')
                ->whereIn('country', array($delCountry, "International"))
                ->whereRaw('((working_days < '. $workingDays.') AND (id != 16) AND (id != 35)) OR ((working_days = '. $workingDays .') AND (latest_at <= '. $maxDelHour .') AND (id != 16) AND (id != 35))')
                ->get();
        }

        $shipping2 = new stdClass;
        $shipping2->currentPage = 0;
        $shipping2->numPages = 0;
        $shipping2->totalPages = 0;
        $shipping2->data = array();
        $i = 0;

        $conv = DB::table('locales')
            ->where('lang', $locale)
            ->first();
        $conv = $conv->conversion;

        foreach($availOptions as $option){
            foreach($shipping->data as $entry){
                if($option->name == $entry->value){
                    $tranTitle = DB::table('locale_text')
                        ->where('id', 'ship_desc_'.$option->id)
                        ->where('locale', $locale)
                        ->first();
                    if($tranTitle)
                    {
                        $entry->label = $tranTitle->lstring;
                    }
                    $entry->price = number_format($entry->price * $conv,2,'.','');
                    $entry->sort = $option->sort_order;
                    $shipping2->data[] = $entry;
                    $i++;
                }
            }
        }
        $shipping2->totalResult = $i;
        return $shipping2;
    }

    private function weekday_diff($from, $to, $normalise=true) {
        $_from = is_int($from) ? $from : strtotime($from);
        $_to   = is_int($to) ? $to : strtotime($to);
        // normalising means partial days are counted as a complete day.
        if ($normalise) {
            $_from = strtotime(date('Y-m-d', $_from));
            $_to = strtotime(date('Y-m-d', $_to));
        }
        $all_days = @range($_from, $_to, 60*60*24);
        if (empty($all_days)) return 0;

        $week_days = array_filter(
            $all_days,
            create_function('$t', '$d = date("w", strtotime("+{$t} seconds", 0)); return !in_array($d, array(6,7));')
        );
        return count($week_days);
    }

    public function indextwo($id='') {
        $locale = App::getLocale();
        $conv = DB::table('locales')
            ->where('lang', $locale)
            ->first();
        $conv = $conv->conversion;

        $customer = Session::get('customer');
        $user = WebUser::where('user_id', $customer['entity_id'])->first();
        if (!empty($user->telephone)) {
            $telephone = $user->telephone;
        }else{
            $telephone = "";
        }

        $ticket = RelatedTicket::getTicketByTicketId($id);
        $ticketInfo = RelatedTicket::where('product_id', '=', $id)->first();

        if(isset($customer['entity_id'] ) && $customer['entity_id'] == $ticketInfo->user_id) {
            return Redirect::route('customer.account.ticket.listing');
        }

        $shoppingCartId = TicketSoap::process('cart.create', array('default'));
        $curItems = TicketSoap::process("cart_product.list", array($shoppingCartId));

        if (isset($customer['entity_id'])) {
            //get customer address ids, shipping address
            $addressParams = array(
                'customer_id'   => $customer['entity_id'],
                'type'          => 'get',
                'address_type'  => 'billing'
            );

            $this->setDataToPost($addressParams);
            $this->submitPostToApi('customer/index/address');
            $response = $this->getApiResponse();
            $billing = json_decode($response, true);
            View::share('billing', $billing['data']);

            $addressParams['address_type'] = 'shipping';
            $this->setDataToPost($addressParams);
            $this->submitPostToApi('customer/index/address');
            $response = $this->getApiResponse();
            $shipping = json_decode($response, true);
            View::share('shipping', $shipping['data']);
        }

        $shoppingCartId = Session::put($id.'_shoppingCartId', $shoppingCartId);
        View::share('body_class', 'pages checkout checkout');
        View::share('customer', $customer);
        View::share('ticket', $ticket);
        View::share('productId', $id);

        $countryList = CountryList::getCountryListWithCountryCode();
        $country = [];
        foreach($countryList as $c) {
            $country[$c->Code] = $c->Country;
        }

        View::share('_country', $country);

        $country_list = $users = DB::table('country_list')
            ->select(DB::raw("`ITU-T Telephone Code` AS `telephone_code`, CONCAT(`Common Name`,' ( ' ,`ITU-T Telephone Code`, ' )') AS Country"))
            ->where('ITU-T Telephone Code', '<>', '')
            ->where('Type', '=', 'Independent State')
            ->orderBy('Common Name', 'asc')
            ->get();
        $country_with_phone = array();
        foreach($country_list as $c ) {
            $phone_code = str_replace('+', '00', $c->telephone_code);
            $country_with_phone[$phone_code] = $c->Country;
        }

        View::share('country_with_phone', $country_with_phone);

        $event= FootBallEvent::find($ticket->event_id);
        $defaultRestriction = [];
        foreach($event->getSelectedRestrictions()  as $val) {

            $defaultRestriction[] = array(
                'id'    => $val->id,
                'title' => $val->title
            );
        }
        View::share('defaultRestriction', $defaultRestriction);
        $preference = (int) isset($ticket->ticket->ticketInformation->sell_preference)? $ticket->ticket->ticketInformation->sell_preference: 0;
        $ticket->ticket->ticketInformation->price = $ticket->ticket->ticketInformation->price * $conv;
        View::share('ticketQty', $this->getNumberOfTicketDisplay($ticket->available_qty, $preference));
        View::share('sell_preference', $preference);
        View::share('ticketInfo', $ticketInfo);
        $node = new stdClass();
        $node->title = "checkout";
        $fImage = '';
        $event = FootBallEvent::find($ticketInfo->event_id);
        if($event)
        {
            $fImage = $event->feature_image;
        }
        View::share('feature_image', $fImage);

        if(empty($_COOKIE[$id])){

            $timer = setcookie($id, date('Y-m-d H:i:s', strtotime('+10 min')));
            $timer = "10";
        }else{
            $dateTimeNow = strtotime(date('Y-m-d H:i:s'));
            $timerThen = strtotime($_COOKIE[$id]);
            $mins = (($timerThen - $dateTimeNow )/ 60);
            $timer = $mins;
        }

        return View::make(Template::name('frontend.bm2014.page_checkout.index'), compact('node'))->with('timer',$timer)->with('telephone',$telephone);
    }

    //MY FUNCTIONS
    public function index($id='') {

        //Getting locale and conversion rate
        $locale = App::getLocale();
        $conv = DB::table('locales')
            ->where('lang', $locale)
            ->first();
        $conv = $conv->conversion;
        //End locale and conversion rate
        if(Options::getOption('payment_gateway') == 'ezpay'){
            $rev_conv = 1/$conv;
            View::share('conv_rate', $rev_conv);
            View::share('gbp_rate', $conv);
        }
        $customer = Session::get('customer');

        $ticket = RelatedTicket::getTicketByTicketId($id);
        $ticketInfo = RelatedTicket::where('product_id', '=', $id)->first();

        if(!$this->checkTicketState($id))
        {
            return Redirect::to('/');
        }

        $modifier = 0;

        if (isset($customer['entity_id']))
        {

            if($customer['entity_id'] == $ticketInfo->user_id)
            {
                return Redirect::route('customer.account.ticket.listing');
            }

            $user = WebUser::where('user_id', $customer['entity_id'])->first();

            if($user)
            {
                if($user->ref_agent)
                {
                    $agent = Agent::where('id', $user->ref_agent)->first();
                    $modifier = $agent->discount_perc;
                }
            }


            //get customer address ids, shipping address
            $addressParams = array(
                'customer_id'   => $customer['entity_id'],
                'type'          => 'get',
                'address_type'  => 'billing'
            );

            $this->setDataToPost($addressParams);
            $this->submitPostToApi('customer/index/address');
            $response = $this->getApiResponse();
            $billing = json_decode($response, true);
            View::share('billing', $billing['data']);

            $addressParams['address_type'] = 'shipping';
            $this->setDataToPost($addressParams);
            $this->submitPostToApi('customer/index/address');
            $response = $this->getApiResponse();
            $shipping = json_decode($response, true);
            View::share('shipping', $shipping['data']);
        }

        Session::put($id.'_shoppingCartProgress', 0);

        View::share('body_class', 'pages checkout checkout');
        View::share('customer', $customer);
        View::share('ticket', $ticket);
        View::share('productId', $id);

        $countryList = CountryList::getCountryListWithCountryCode();
        $country = [];
        foreach($countryList as $c) {
            $country[$c->Code] = $c->Country;
        }

        View::share('_country', $country);

        $country_list = $users = DB::table('country_list')
            ->select(DB::raw("`ITU-T Telephone Code` AS `telephone_code`, CONCAT(`Common Name`,' ( ' ,`ITU-T Telephone Code`, ' )') AS Country"))
            ->where('ITU-T Telephone Code', '<>', '')
            ->where('Type', '=', 'Independent State')
            ->orderBy('Common Name', 'asc')
            ->get();
        $country_with_phone = array();
        foreach($country_list as $c ) {
            $phone_code = str_replace('+', '00', $c->telephone_code);
            $country_with_phone[$phone_code] = $c->Country;
        }

        View::share('country_with_phone', $country_with_phone);

        $event = FootBallEvent::find($ticket->event_id);

        View::share('event',$event);

        $defaultRestriction = [];
        foreach($event->getSelectedRestrictions()  as $val) {
            $defaultRestriction[] = array(
                'id'    => $val->id,
                'title' => $val->title
            );
        }
        View::share('defaultRestriction', $defaultRestriction);

        $preference = (int) isset($ticket->ticket->ticketInformation->sell_preference)? $ticket->ticket->ticketInformation->sell_preference: 0;
        $ticket->ticket->ticketInformation->price = ($ticket->ticket->ticketInformation->price - (($ticket->ticket->ticketInformation->price * $modifier)/100)) * $conv;
        View::share('ticketQty', $this->getNumberOfTicketDisplay($ticket->available_qty, $preference));
        View::share('sell_preference', $preference);
        View::share('ticketInfo', $ticketInfo);

        $node = new stdClass();
        $node->title = "checkout";

        if(empty($_COOKIE[$id])){

            $timer = setcookie($id, date('Y-m-d H:i:s', strtotime('+10 min')));
            $timer = "10";
        }else{
            $dateTimeNow = strtotime(date('Y-m-d H:i:s'));
            $timerThen = strtotime($_COOKIE[$id]);
            $mins = (($timerThen - $dateTimeNow )/ 60);
            $timer = $mins;
        }

        View::share('timer', $timer);

        return View::make(Template::name('frontend.%s.SlidingCheckout.index'), compact('node'));
    }

    public function initializeCheckout($id)
    {
        $cartProg = 0;

        if(Session::has($id.'_shoppingCartProgress'))
        {
            $cartProg = Session::get($id.'_shoppingCartProgress');
            Session::put($id.'_shoppingCartProgress', 0);
        }
        else
        {
            Session::put($id.'_shoppingCartProgress', 0);
        }

        for($i=0; $i<=6; $i++)
        {
            if(Session::has($id.'_sd_'.$i))
            {
                Session::forget($id.'_sd_'.$i);
            }
        }

        return $this->buildPage1View($id);
    }

    public function gotoPage($id)
    {
        if(!Input::has('page'))
        {
            return Response::json("Requires page number",404);
        }
        $pageNum = intval(Input::get('page'));

        if($pageNum >= 0 && $pageNum < 6)
        {
            if(Session::has($id.'_shoppingCartProgress'))
            {
                $cartProg = Session::get($id.'_shoppingCartProgress');

                if(($cartProg+1) >= $pageNum)
                {
                    for($i=$pageNum; $i<=$cartProg; $i++)
                    {
                        if(Session::has($id.'_sd_'.$i))
                        {
                            Session::forget($id.'_sd_'.$i);
                        }
                    }

                    $pageNum2=$pageNum-1;
                    if($pageNum2 < 0)
                    {
                        $pageNum2 = 0;
                    }
                    Session::put($id.'_shoppingCartProgress', $pageNum2);
                    switch($pageNum){
                        case 1:
                            return $this->buildPage1View($id);
                            break;
                        case 2:
                            return $this->buildPage2View($id);
                            break;
                        case 3:
                            return $this->buildPage3View($id);
                            break;
                        case 4:
                            return $this->buildPage4View($id);
                            break;
                        case 5:
                            return $this->buildPage5View($id);
                            break;
                    }

                }
                else
                {
                    return Response::json("Cannot Skip Forward",404);
                }
            }
            else
            {
                return Response::json("No session info");
            }
        }
        else
        {
            return Response::json("Out of range", 404);
        }
    }

    public function getNext($id)
    {

        $input = Input::all();
        if(Session::has($id.'_shoppingCartProgress'))
        {
            $progress = Session::get($id.'_shoppingCartProgress');
            if($input = $this->validateInput($input, $progress, $id))
            {
                if(!$this->checkTicketState($id))
                {
                    return Response::json("Sorry. This listing is no longer available.",404);
                }
                if(is_array($input))
                {

                    $inp = $progress + 1;
                    Session::put($id.'_sd_'.$inp, $input);
                    Session::put($id.'_shoppingCartProgress', $inp);

                    switch($inp){
                        case 1:
                            // About You
                            return $this->buildPage2View($id);
                            break;
                        case 2:
                            // Shipping
                            return $this->buildPage3View($id);
                            break;
                        case 3:
                            // Billing Address
                            return $this->buildPage4View($id);
                            break;
                        case 4:
                            // Process credit card
                            if(Options::getOption('payment_gateway') == 'ezpay'){
                                $cardNumber = $input['cc_number'];
                                $cardNumber = preg_replace('/\D/', '', $cardNumber);
                                $len = strlen($cardNumber);
                                if ($len < 15 || $len > 16) {
                                     $ret = array();
                                     // Require Translations
                                     $ret['Error:'] = "Invalid credit card number: must be 15 or 16 digits.";
                                     return Response::json($ret, 404);
                                }
                                else {
                                    if( preg_match ('/^4/', $cardNumber) >= 1 || preg_match ('/^5[1-5]/', $cardNumber) >= 1 || preg_match ('/^3[47]/', $cardNumber) >= 1 || preg_match ('/^3(?:0[0-5]|[68])/', $cardNumber) >= 1 || preg_match ('/^6(?:011|5)/', $cardNumber) >= 1 || preg_match ('/^(?:2131|1800|35\d{3})/', $cardNumber) >= 1) {
                                        $card =1;
                                    }
                                    else {
                                        $card = 0;
                                    }
                                    if(!$card){
                                        $ret = array();
                                        // Require Translations
                                        $ret['Error:'] = "Could not determine the credit card type.";
                                        return Response::json($ret,404);
                                    }
                                }

                                $expires = \DateTime::createFromFormat('m/y', $input['cc_expire']);
                                $now     = new \DateTime();
                                if ($expires < $now) {
                                 $ret = array();
                                    // Require Translations
                                    $ret['Error:'] = "Invalid Expire date.";
                                    //return Response::json($ret,404);
                                }
                                $len = strlen($input['cc_cvv']);
                                if(empty($input['cc_cvv']) || $len < 3 ) {
                                    $ret = array();
                                    // Require Translations
                                    $ret['Error:'] = "Invalid CVV entered.";
                                    return Response::json($ret,404);
                                }
                            }
                            return $this->buildPage5View($id);
                            break;
                        case 5:
                            //  empty - does nothing
                            return $this->buildPage6View($id);
                            break;
                    }
                }
                else
                {
                    return Response::json($input,404);
                }

            }
            else
            {
                return Response::json("Invalid input.",404);
            }
        }
        else
        {
            return $this->initializeCheckout($id);
        }
    }

    public function getPrev($id)
    {
        if(Session::has($id.'_shoppingCartProgress')) {
            $progress = Session::get($id . '_shoppingCartProgress');
            $inp = $progress-1;
            if($inp <=0)
            {
                $inp = 0;
            }
            Session::put($id . '_shoppingCartProgress', $inp);
            switch($progress)
            {
                case 0:
                    return $this->buildPage1View($id);
                    break;
                case 1:
                    return $this->buildPage1View($id);
                    break;
                case 2:
                    return $this->buildPage2View($id);
                    break;
                case 3:
                    return $this->buildPage3View($id);
                    break;
                case 4:
                    return $this->buildPage4View($id);
                    break;
                case 5:
                    return $this->buildPage5View($id);
                    break;
            }
            return Response::json("Cannot go back",404);
        }
        else
        {
            return Response::json("Could not locate shooping cart progress", 404);
        }
        //Discard this pages data
    }

    /**
     * Checkout Page
     */
    public function buildPage1View($id)
    {
        $locale = App::getLocale();
        $conv = DB::table('locales')
            ->where('lang', $locale)
            ->first();
        $conv = $conv->conversion;
        if(Options::getOption('payment_gateway') == 'ezpay'){
            $rev_conv = 1/$conv;
            View::share('conv_rate', $rev_conv);
            View::share('gbp_rate', $conv);
        }


        $ticket = RelatedTicket::getTicketByTicketId($id);

        $show_fees = true;

        if(Session::has('refCode'))
        {
            $ap = AffiliateProperty::where('ref_code', Session::get('refCode'))->first();
            if($ap)
            {
                $show_fees = $ap->show_fees;
            }
        }

        View::share('show_fees', $show_fees);

        $modifier = 0;
        if(Session::has('customer'))
        {
            $customer = Session::get('customer');
            $user = WebUser::where('user_id', $customer['entity_id'])->first();

            if($user)
            {
                if($user->ref_agent)
                {
                    $agent = Agent::where('id', $user->ref_agent)->first();
                    $modifier = $agent->discount_perc;
                }
            }
        }

        if(!$ticket)
        {
            return Response::json("Could not find ticket", 404);
        }

        $modP = ($ticket->ticket->ticketInformation->price * $modifier)/100;

        $preference = (int) isset($ticket->ticket->ticketInformation->sell_preference)? $ticket->ticket->ticketInformation->sell_preference: 0;
        $ticket->ticket->ticketInformation->price = number_format(($ticket->ticket->ticketInformation->price-$modP) * $conv, 2 ,'.', '');

        View::share('ticket', $ticket);
        View::share('ticketQty', $this->getNumberOfTicketDisplay($ticket->available_qty, $preference));
        View::share('sell_preference', $preference);

        return View::make(Template::name('frontend.%s.SlidingCheckout.pages.1.page'));
    }

    /**
     * About You
     */
    public function buildPage2View($id)
    {
        $user = false;

        if(Session::has('customer'))
        {
            $c = Session::get('customer');
            if(isset($c['entity_id']))
            {
                $user = WebUser::where('user_id', $c['entity_id'])->first();
            }
        }

        $countryList = CountryList::getCountryListWithCountryCode();
        $country = [];
        foreach($countryList as $c) {
            $country[$c->Code] = $c->Country;
        }

        View::share('_country', $country);

        $country_list = $users = DB::table('country_list')
            ->select(DB::raw("`ITU-T Telephone Code` AS `telephone_code`, CONCAT(`Common Name`,' ( ' ,`ITU-T Telephone Code`, ' )') AS Country"))
            ->where('ITU-T Telephone Code', '<>', '')
            ->where('Type', '=', 'Independent State')
            ->orderBy('Country', 'asc')
            ->get();
        $country_with_phone = array();
        foreach($country_list as $c ) {
            $phone_code = str_replace('+', '00', $c->telephone_code);
            //$country_with_phone[$phone_code] = $c->Country;
            $country_with_phone[] = array($phone_code, $c->Country);
        }

        View::share('country_with_phone', $country_with_phone);

        View::share('customer',$user);
        return View::make(Template::name('frontend.%s.SlidingCheckout.pages.2.page'));
    }

    /**
     * Shipping Select Page
     */
    public function buildPage3View($id)
    {
        $locale = App::getLocale();
        $conv = DB::table('locales')
            ->where('lang', $locale)
            ->first();
        $conv = $conv->conversion;
        if(Options::getOption('payment_gateway') == 'ezpay'){
            $rev_conv = 1/$conv;
            View::share('conv_rate', $rev_conv);
            View::share('gbp_rate', $conv);
        }

        $rt = RelatedTicket::where('product_id', $id)->first();
        $event = FootBallEvent::where('id', $rt->event_id)->first();
        $htc = FootballTicketMeta::where('football_ticket_id', $event->home_team_id)->where('key', 'country')->first();

        $countryL = false;
        if($htc)
        {
            $countryL = FootballTickets::where('id', $htc->value)->first();
        }

        if($countryL && !empty($countryL))
        {
            $countryList = CountryList::where('Common Name', $countryL->title)
                ->select(
                    DB::raw('`Common Name` AS Country, `ISO 3166-1 2 Letter Code` As `Code`, `ITU-T Telephone Code` AS `tcode` ')
                )
                ->get();
        }
        else
        {
            $countryList = CountryList::getCountryListWithCountryCode();
        }

        $country = [];
        foreach($countryList as $c) {
            $country[$c->Code] = $c->Country;
        }

        View::share('_country', $country);
        View::share('productID', $id);
        //So based on the drop down i need to be displaying
        return View::make(Template::name('frontend.%s.SlidingCheckout.pages.4.page'));
    }

    /**
     * Billing Address form
     */
    public function buildPage4View($id)
    {
        $vars = Session::get($id.'_sd_3');
        $cbs = false;
        if($vars['allow_same_as_ship']=="true")
        {
            $cbs = true;
        }

        View::share('cbs',$cbs);

        $countryList = CountryList::getCountryListWithCountryCode();
        $country = [];
        foreach($countryList as $c) {
            $country[$c->Code] = $c->Country;
        }

        View::share('_country', $country);

        $customer = Session::get('customer');

        if (isset($customer['entity_id'])) {
           //Getting locale and currency
            $locale = App::getLocale();
            $localeSettings = DB::table('locales') ->where('lang', $locale) ->first();
            $prefCur = $localeSettings->currency;
            $conv = $localeSettings->conversion;
            View::share('_prefCur', $prefCur);

            // get ticket Info & calculate correct price
            $modifier = 0;
            $ticket = RelatedTicket::getTicketByTicketId($id);
            // calculate correct price
            $user = WebUser::where('user_id', $customer['entity_id'])->first();
            if($user) {
                if($user->ref_agent) {
                    $agent = Agent::where('id', $user->ref_agent)->first();
                    $modifier = $agent->discount_perc;
                }
            }
            // set price
            $ticket->ticket->ticketInformation->price = ($ticket->ticket->ticketInformation->price - (($ticket->ticket->ticketInformation->price * $modifier)/100)) * $conv;
            View::share('ticket', $ticket);

            //get customer address ids, shipping address
            $addressParams = array(
                'customer_id'   => $customer['entity_id'],
                'type'          => 'get',
                'address_type'  => 'billing'
            );

            $this->setDataToPost($addressParams);
            $this->submitPostToApi('customer/index/address');
            $response = $this->getApiResponse();
            $billing = json_decode($response, true);
            View::share('billing', $billing['data']);
            //Session::forget($id . '_shoppingCartProgress');
            return View::make(Template::name('frontend.%s.SlidingCheckout.pages.3.page'));
        }
        else
        {
            return Response::json("Not logged in", 404);
        }
    }

    /**

     * Process credit card
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function buildPage5View($id)
    {
        \Log::info('1');
        $locale = App::getLocale();
        $conv = DB::table('locales')->where('lang', $locale)->first();
        $conv = $conv->conversion;
        $bf = 1 + (intval(Options::getOption('booking_fees'))/100);
        //THIS IS THE CHECKOUT SCRIPT TIME TO GET THE GATEWAY WORKING
        $customer = Session::get('customer');
        Log::info("2");
        $in1 = Session::get($id.'_sd_1');
        Log::info("3");
        $in2 = Session::get($id.'_sd_2');
        Log::info("4");
        $in3 = Session::get($id.'_sd_3');
        Log::info("5");
        $in4 = Session::get($id.'_sd_4');
        Log::info("6");

        Session::forget($id.'_sd_1');
        Session::forget($id.'_sd_2');
        Session::forget($id.'_sd_3');
        Session::forget($id.'_sd_4');
        Session::forget($id . '_shoppingCartProgress');

        Log::info("7");

        $input = array_merge($in1, $in2, $in3, $in4);

        Log::info("8");

        $input['qty']=$input['number_of_ticket'];

        if($input['same_as_ship']=='true')
        {
            $input['city_bill']=$input['city'];
            $input['street_bill']=$input['street'];
            $input['postcode_bill']=$input['postcode'];
            $cId = DB::table('country_list')
                ->where('ISO 3166-1 2 Letter Code', $input['country'])
                ->select(DB::raw('`ISO 3166-1 2 Letter Code` as code'))
                ->first();

            \Log::info(var_export($cId,true));

            $input['country_id_bill']=$cId->code;
        }

        Log::info("9");

        $shoppingCartId = TicketSoap::process('cart.create', array('default'));
        $_ticket = RelatedTicket::getTicketByTicketId($id);
        $_ticket->price = round($_ticket->price, 2);

        Log::info("10");

        try
        {
            $cart = TicketSoap::process("cart.info", array($shoppingCartId));
            $store_id = $cart['store_id'];
            if($cart)
            {
                $itemArray = array();

                foreach($cart['items'] as $item)
                {
                    TicketSoap::process("cart_product.remove", array($shoppingCartId, array(array('product_id' => $item['product_id'],'sku'=>$item['sku'], 'quantity'=>$item['qty'], 'options' => array())), $store_id));
                }

            }
        }
        catch(Exeption $e)
        {
            \Log::info("Couldnt remove items from cart");
        }

        if($_ticket->ticket_status != 'active')
        {
            throw new Exception( json_encode("Error."));
        }

        Log::info("11");


        $countryId = isset($input['country_id']) ? $input['country_id'] : 'GB';
        $countryIdBill = isset($input['country_id_bill']) ? $input['country_id_bill'] : 'GB';
        $nameOnCard = isset($customer['firstname'])? $customer['firstname']." ".$customer['lastname'] : $input['first_name']." ".$input['last_name'];
        $customerEmail = isset($customer['email'])? $customer['email']: '';

        Log::info("12");


        $rName = $input['reservation_name'];
        $hName = $input['hotel_name'];
        $cIn = $input['checkin_dt'];
        $cOut = $input['checkout_dt'];

        Log::info("13");

        $hotelString = "";
        //IMPROVE HOTEL STUFF
        if ($input['is_hotel'] != "false") {
            $hotelString = "Reservation Name: ".$rName.'; Check-in time: '.$cIn.'; Check-out time: '.$cOut.'; Hotel name: '.$hName.';';
        }
        else if($input['is_office'] != "false"){
            $hotelString = "Office: ".$input['office_address']."; Address: ".$input['street'];
        }
        else
        {
            $hotelString = $input['street'];
        }

        Log::info("14");

        try {
            $customerId = $customer['entity_id'];
            Log::info("15");

            //update user address
            $params = array();
            $params['customer_id']  = $customerId;
            $params['type']         = 'set';
            $params['address_type'] = 'shipping';
            $params['vat_id']       = '';
            $params['company']      = '';
            $params['street']       = $hotelString;
            $params['country_id']   = $countryId;
            $params['postcode']     = $input['postcode'];
            $params['city']         = $input['city'];
            $params['telephone']    = $input['country_code'].$input['contact_no'];
            $_address               = $params;
            //set billing
            $this->setDataToPost($params);
            $this->submitPostToApi('customer/index/address');

            Log::info("16");

            //set shipping
            $params['address_type'] = 'billing';

            $params['street']       = $input['street_bill'];
            $params['country_id']   = $countryIdBill;
            $params['postcode']     = $input['postcode_bill'];
            $params['city']         = $input['city_bill'];

            $this->setDataToPost($params);
            $this->submitPostToApi('customer/index/address');

            Log::info("17");

            //get customer address ids, shipping address
            $addressParams = array(
                'customer_id'   => $customerId,
                'type'          => 'get',
                'address_type'  => 'billing'
            );

            $this->setDataToPost($addressParams);
            $this->submitPostToApi('customer/index/address');
            $response = $this->getApiResponse();
            $billing = json_decode($response, true);

            $addressParams['address_type'] = 'shipping';
            $this->setDataToPost($addressParams);
            $this->submitPostToApi('customer/index/address');
            $response = $this->getApiResponse();

            $shipping = json_decode($response, true);

            Log::info("18");

            if(empty($billing['data']['street'])) {
                $params = array();
                $params['customer_id']  = $customerId;
                $params['type']         = 'set';
                $params['address_type'] = 'billing';
                $params['vat_id']       = '';
                $params['company']      = '';
                $params['street']       = $shipping['data']['street'];
                $params['country_id']   = $shipping['data']['country_id'];
                $params['postcode']     = $shipping['data']['postcode'];
                $params['city']         = $shipping['data']['city'];
                $params['telephone']    = isset($shipping['data']['telephone'])?$shipping['data']['telephone']: "0000000000";

                $this->setDataToPost($params);
                $this->submitPostToApi('customer/index/address');

                Log::info("19");

                $addressParams = array(
                    'customer_id'   => $customerId,
                    'type'          => 'get',
                    'address_type'  => 'billing'
                );

                $this->setDataToPost($addressParams);
                $this->submitPostToApi('customer/index/address');
                $response = $this->getApiResponse();
                $billing = json_decode($response, true);

                Log::info("20");
            }
            //var_dump($shipping);

            // Set customer, for example guest
            $resultCustomerSet = TicketSoap::process('cart_customer.set', array( $shoppingCartId, array(
                'entity_id' => $customerId,
                'mode'      => 'customer'
            )));

            Log::info("21");


            TicketSoap::process("customer_address.update", array(
                'addressId' =>  $billing['data']['entity_id'],
                'addressdata' => array(
                    'firstname' =>  $customer['firstname'],
                    'lastname'  =>  $customer['lastname']
                )
            ));

            Log::info("22");

            TicketSoap::process("customer_address.update", array(
                'addressId' =>  $shipping['data']['entity_id'],
                'addressdata' => array(
                    'firstname' =>  $input['first_name'],
                    'lastname'  =>  $input['last_name']
                )
            ));

            Log::info("23");

            $arrAddresses = array(
                array(
                    "mode" => "shipping",
                    "address_id" => $shipping['data']['entity_id'],
                ),
                array(
                    "mode" => "billing",
                    "address_id" => $billing['data']['entity_id'],
                )
            );

            Log::info("24");

            //address added to checkout
            $resultCustomerAddresses = TicketSoap::process("cart_customer.addresses", array($shoppingCartId, $arrAddresses ));
            // product added to checkout
            $arrProducts = array(
                array(
                    "product_id" => $id,
                    "qty" => $input['qty']
                )
            );
            $resultCartProductAdd = TicketSoap::process("cart_product.add", array($shoppingCartId, $arrProducts));
            $shoppingCartProducts = TicketSoap::process("cart_product.list", array($shoppingCartId));

            Log::info("25");

            $shippingMethd = isset($input['shipping_method']) && $input['shipping_method'] != ''? $input['shipping_method'] : 'flatrate';

            $shipping  = TicketSoap::process("cart_shipping.method", array($shoppingCartId, "{$shippingMethd}_{$shippingMethd}"));



            Log::info("26");

            $modifier = 0;
            $da = 0;
            $agent_sale = false;

            if(Session::has('customer'))
            {
                $customer = Session::get('customer');
                $user = WebUser::where('user_id', $customer['entity_id'])->first();

                if($user)
                {
                    if($user->ref_agent)
                    {
                        $agent = Agent::where('id', $user->ref_agent)->first();
                        $modifier = $agent->discount_perc;
                    }
                }
            }

            if($modifier != 0)
            {
                $da = (($_ticket->price*$bf)*$modifier)/100;
                $da = number_format($da, 2, '.','');

                $api_path = Config::get('api.mage_soap_api_path');
                require_once("{$api_path}app/Mage.php");
                umask(0);
                Mage::app('default');

                $ruleString = $this->generateRandomString();

                $customerGroupIds = Mage::getModel('customer/group')->getCollection()->getAllIds();
                // SalesRule Rule model
                $rule = Mage::getModel('salesrule/rule');

                // Rule data
                $rule->setName('Agent Discount '.time())
                    ->setDescription('Sales Agent Discount')
                    ->setFromDate('')
                    ->setCouponType(Mage_SalesRule_Model_Rule::COUPON_TYPE_SPECIFIC)
                    ->setCouponCode($ruleString)
                    ->setUsesPerCustomer(1)
                    ->setUsesPerCoupon(1)
                    ->setCustomerGroupIds($customerGroupIds)
                    ->setIsActive(1)
                    ->setConditionsSerialized('')
                    ->setActionsSerialized('')
                    ->setStopRulesProcessing(1)
                    ->setIsAdvanced(1)
                    ->setProductIds('')
                    ->setSortOrder(0)
                    ->setSimpleAction(Mage_SalesRule_Model_Rule::BY_FIXED_ACTION)
                    ->setDiscountAmount($da)
                    ->setDiscountQty()
                    ->setDiscountStep(0)
                    ->setSimpleFreeShipping('0')
                    ->setApplyToShipping('0')
                    ->setIsRss(0)
                    ->setWebsiteIds(array(1))
                    ->setStoreLabels(array('Agent Discount'));

                // Here we go
                $rule->save();
                $rId = $rule->getId();

                try {
                    $addedCoupon = TicketSoap::process("cart_coupon.add", array($shoppingCartId, $ruleString));
                    if (!$addedCoupon) {
                        \Log::info("CANT ADD AGENT VOUCHER");
                    }
                    else {
                        $agent_sale = array('agent_id' => $user->ref_agent, 'amt' => $da, 'rId'=>$rId);
                    }

                } catch (Exception $e) {
                    \Log::info("AN EXCEPTION ADDING AGENT COUPON");
                    \Log::info($e->getMessage());
                }

            }

            if(array_key_exists('voucherCode', $input) && $input['voucherCode'] != '') {
                $coupon = $input['voucherCode'];
                try {
                    $addedCoupon = TicketSoap::process("cart_coupon.add", array($shoppingCartId, $coupon));
                    if (!$addedCoupon) {
                        \Log::info("CANT ADD VOUCHER CODE");
                    }
                } catch (Exception $e) {
                    \Log::info("AN EXCEPTION");
                }

            }

            Log::info("27");

            // get total Amount
            $resultTotalAmount = TicketSoap::process("cart.totals", $shoppingCartId);
            $totalAmount = 0;
            $shipAmt = 0;
            $sStr = "";

            \Log::info(var_export($resultTotalAmount,true));

            foreach($resultTotalAmount as $_amount) {
                if($_amount['title'] == 'Grand Total') {
                    $totalAmount = $_amount;
                }
                if(substr($_amount['title'],0,8)=="Shipping")
                {
                    $shipAmt = number_format($_amount['amount'],2,'.','');
                    $sStr = substr($_amount['title'],20);
                }
            }

            Log::info("28");
            $totalGatewayAmount  = number_format($totalAmount['amount'], 2, '', '');
            $input['ttl']=number_format(($totalGatewayAmount/100)*$conv, 2,'.','');
            $input['ship_cost']=number_format($shipAmt*$conv,2,'.','');
            $input['indiv']=number_format(($_ticket->price*$bf)*$conv,2,'.','');
            $input['sstr'] = $sStr;

            Log::info("29");

            $sessionProductInfo = array();
            $userIP = App::environment('local', 'stage') ? '81.137.255.206' : $_SERVER['REMOTE_ADDR'];
            $country = App::environment('local', 'stage') ?  'LV': $billing['data']['country_id'];
            $currency = App::environment('local', 'stage') ?  'USD': 'GBP';

            $sessionProductInfo['shoppingCartId'] = $shoppingCartId;

            $referer = '';

            if(Session::has('referer_url'))
            {
                $referer = Session::get('referer_url');
            }

            Log::info("30");

            $thisUrl = Request::getHttpHost();
            Log::info($thisUrl);

            $sessionProductInfo['order'] = array(
                'order_id'          => '',
                'buyer_id'          => $customerId ,
                'event_id'          => '',
                'product_id'        => $id,
                'qty'               => $input['qty'],
                'amount'            => '',
                'delivery_status'   => 'pending',
                'referral_url'      => $referer,
                'site_sold'         => $thisUrl,
                'agent_discount'    => $agent_sale
            );

            Log::info("31");

            if(Session::has('refCode'))
            {
                $refCode = Session::get('refCode');

                $affil = AffiliateProperty::where('ref_code', $refCode)->first();

                $perc = $affil->getCurrentRate();

                $sessionProductInfo['af_id'] = $affil->id;
                $sessionProductInfo['com_perc'] = $perc;

                $rid = null;

                if(Session::has('rid'))
                {

                    $rid = Session::get('rid');
                    Log::info($rid);
                }
                else
                {
                    Log::info("Didnt work");
                }

                $sessionProductInfo['rid'] = $rid;
            }

            Log::info("32");

            if(Config::get('transactpro.COD_Enable') ==  true) {
                $paymentMethod = array(
                    "method" => "checkmo"
                );
                $resultPaymentMethod = TicketSoap::process("cart_payment.method", array($shoppingCartId, $paymentMethod));
                $uid = uniqid();
                Session::put('mid_'.$uid, json_encode($sessionProductInfo));
                $response = Response::make(json_encode(array('_3d'=>'no', 'link'=> '/gateway/cod?merchant_transaction_id='.$uid )), '200');
                $response->header('Content-Type', 'application/json');
                return $response;
            }

            Log::info("33");

            //transaction_pro
            if(Options::getOption('payment_gateway') == 'tp') {
                //payment gateway
                Log::info("34");

                $auth = Config::get('transactpro.auth');
                $gateClient = new GateClient($auth);

                $transaction = new Transaction();
                $merchantId = str_pad ($shoppingCartId, 20, '0', STR_PAD_LEFT);

                $transaction->order_id = $merchantId;

                $gatewayAgs = array(
                    'rs'                      => Config::get('transactpro.rs'),
                    'merchant_transaction_id' => $merchantId,
                    'user_ip'                 => $userIP,
                    'description'             => $_ticket->title,
                    'amount'                  => $totalGatewayAmount,
                    'currency'                => $currency,
                    'name_on_card'            => $nameOnCard,
                    'street'                  => $billing['data']['street'],
                    'zip'                     => $billing['data']['postcode'],
                    'city'                    => $billing['data']['city'],
                    'country'                 => $country,
                    'state'                   => 'NA',
                    'email'                   => $customerEmail,
                    'phone'                   => isset($billing['data']['telephone']) ? $billing['data']['telephone'] : "0000000000",
                    'merchant_site_url'       => Config::get('transactpro.merchant_url')
                );

                Log::info("35");
                \Log::info(var_export($gatewayAgs,true));
                $response = $gateClient->init($gatewayAgs);

                Log::info('36');

                if(preg_match('/~RedirectOnsite:/', $response->getResponseContent()))
                {
                    $responseArr = explode('~', $response->getResponseContent());
                    $respContentArray = explode(':',$responseArr[0]);
                    $sessionProductInfo['transactionId'] = $respContentArray[1];
                    $transaction->tid = $respContentArray[1];
                    $transaction->status = 0;

                    Log::info("36");

                    $transaction->order = json_encode($sessionProductInfo);
                    $transaction->save();
                    $redirection = str_replace('RedirectOnsite:', '', $respContentArray[1]);

                    $paymentMethod = array(
                        "method" => "checkmo"
                    );

                    $resultPaymentMethod = TicketSoap::process("cart_payment.method", array($shoppingCartId, $paymentMethod));

                    $redirection = explode('tid=', $responseArr[1]);
                    Session::put('mid_'.$merchantId, json_encode($sessionProductInfo));
                    $response = Response::make(json_encode(array('_3d'=>'yes', 'link'=> Config::get('transactpro.gatewayUrl').$redirection[1])), '200');
                    $response->header('Content-Type', 'application/json');

                    Log::info("37");

                    return $response;


                }
                else if($response instanceof TransactPRO\Gate\Response\Response
                    && $response->isSuccessful()
                    && !preg_match('/ERROR:/', $response->getResponseContent()))
                {


                    $responseContent = $response->getResponseContent();
                    $respContentArray = explode(':',$responseContent);
                    $tid = $respContentArray[1];
                    $sessionProductInfo['transactionId'] = $tid;

                    Log::info("38");

                    $response = $gateClient->makeHold(array(
                        'f_extended'          => '5',
                        'init_transaction_id' => $tid,
                        'cc'                  => $input['card_number'],
                        'cvv'                 => $input['card_cvv'],
                        'expire'              => $input['card_expire']
                    ));

                    $responseContent = $response->getResponseContent();

                    Log::info("39");

                    if(preg_match('/Redirect:/', $responseContent) && $response->isSuccessful())  {

                        Log::info("40");

                        $maonthYearARR = explode('/',$input['card_expire']);
                        $paymentMethod = array(
                            "method" => "checkmo"
                        );

                        $resultPaymentMethod = TicketSoap::process("cart_payment.method", array($shoppingCartId, $paymentMethod));
                        $redirection = str_replace('Redirect:', '', $responseContent);
                        $response = Response::make(json_encode(array('_3d'=>'yes', 'link'=> $redirection )), '200');
                        $response->header('Content-Type', 'application/json');
                        Session::put('mid_'.$merchantId, json_encode($sessionProductInfo));

                        Log::info("41");

                        return $response;
                    } else if ($response->isSuccessful()){
                        $paymentMethod = array(
                            "method" => "checkmo"
                        );
                        $resultPaymentMethod = TicketSoap::process("cart_payment.method", array($shoppingCartId, $paymentMethod));
                        $response = Response::make(json_encode(array('_3d'=>'no', 'link'=> '/gateway/response?merchant_transaction_id='.$shoppingCartId )), '200');
                        $response->header('Content-Type', 'application/json');
                        Session::put('mid_'.$merchantId, json_encode($sessionProductInfo));

                        Log::info("42");

                        return $response;
                    } else {
                        throw new Exception( json_encode($response));
                    }

                } else {
                    throw new Exception( json_encode( $response));
                }
            } else if(Options::getOption('payment_gateway') == 'g2s') {
                $cart = TicketSoap::process("cart.info", array($shoppingCartId));

                //g2s programming

                $transaction = new Transaction();
                $merchantId = str_pad ($shoppingCartId, 20, '0', STR_PAD_LEFT);

                $transaction->order_id = $merchantId;


                $item_price     = $_ticket->price;
                $handling       = $cart['base_grand_total'] - ($_ticket->price * $input['qty']);
                if($handling < 0)
                {
                    $handling = 0;
                }

                $post = array();
                $post['merchant_site_id']   = Config::get('gate2shop.merchant_site_id');
                $post['merchant_id']        = Config::get('gate2shop.merchant_id');
                $post['time_stamp']         = gmdate('Y-m-d.H:i:s');
                $post['currency']           = Config::get('gate2shop.currency');
                $post['total_amount']       = number_format ($cart['base_grand_total'], 2, '.', '');
                $post['numberofitems']      = 1;

                $post['item_name_1']        = $_ticket->title;
                $post['encoding']           = 'utf-8';
                $post['item_number_1']      = 1;
                $post['item_quantity_1']    = $input['qty'];
                $post['item_amount_1']      = number_format ($item_price, 2, '.', '');

                $post['first_name']         = $customer['firstname'];
                $post['last_name']          = $customer['lastname'];
                $post['email']              = $customerEmail;
                $post['user_token_id']      = $customerEmail;
                $post['user_token']         = 'auto';
                $post['version']            = '4.0.0';
                $post['handling']           = number_format ($handling, 2, '.', '');
                $post['merchant_unique_id'] = $merchantId;

                $post['success_url']        = Config::get('app.url').'gateway/success';
                $post['notify_url']         = Config::get('app.url').'gateway/response';
                $post['error_url']          = Config::get('app.url').'gateway/error';
                $post['pending_ur']         = Config::get('app.url').'gateway/pending';
                $post['back_url']           = Config::get('app.url').'gateway/back';
                $post['url']                = Config::get('app.url');

                array_map('trim', $post);

                $JoinedInfo = join('', array_values($post));
                $post["checksum"] = md5(Config::get('gate2shop.secret_string') . $JoinedInfo);

                $fields_string="";

                foreach($post as $key=>$value) {
                    $fields_string .= $key."=".urlencode($value)."&";
                }

                $fields_string = rtrim($fields_string, "&");

                $redirect_link = Config::get('gate2shop.server_URL').$fields_string;

                $sessionProductInfo['redirect_link'] = $redirect_link;
                $sessionProductInfo['post'] = $post;
                $sessionProductInfo['product_id'] = $id ;

                if(Session::has('refCode'))
                {
                    $refCode = Session::get('refCode');

                    $affil = AffiliateProperty::where('ref_code', $refCode)->first();

                    $perc = $affil->getCurrentRate();

                    $sessionProductInfo['af_id'] = $affil->id;
                    $sessionProductInfo['com_perc'] = $perc;
                }


                Session::put('mid_'.$shoppingCartId, json_encode($sessionProductInfo));

                $transaction->tid = $shoppingCartId;
                $transaction->status = 0;

                $transaction->order = json_encode($sessionProductInfo);
                $transaction->save();

                $paymentMethod = array(
                    "method" => "checkmo"
                );
                $resultPaymentMethod = TicketSoap::process("cart_payment.method", array($shoppingCartId, $paymentMethod));

                $response = Response::make(json_encode(array('_3d'=>'yes', 'link'=> route('checkout.payment', array('tid'=>$shoppingCartId, 'input' => Input::except('VoucherCode'), 'id' => $id), '200'))));
                //$response = Response::make(json_encode(array('_3d'=>'yes', 'link'=> $redirect_link )), '200');
                $response->header('Content-Type', 'application/json');
                return $response;

            }
            else if(Options::getOption('payment_gateway') == 'ezpay') {
                $cart = TicketSoap::process("cart.info", array($shoppingCartId));
                $sessionProductInfo['product_id'] = $id ;
                $transaction = new Transaction();
                $merchantId = str_pad ($shoppingCartId, 20, '0', STR_PAD_LEFT);

                $transaction->order_id = $merchantId;
                Session::put('mid_'.$shoppingCartId, json_encode($sessionProductInfo));

                $transaction->tid = $shoppingCartId;
                $transaction->status = 0;

                $transaction->order = json_encode($sessionProductInfo);
                $transaction->save();
                //Retrieving token, Amount and currency
                $token = Input::get('token');
                // $amount = $totalGatewayAmount;
                // $curr = $currency;
                $obj = array (
                    "PrivateMerchantKey" => Config::get('ezpay.private_key'),
                    "Token" => $token,
                    "Request" => array (
                        "References" => array (
                            "Client"=>$sessionProductInfo['order']['buyer_id'],
                            "Order"=>$shoppingCartId,
                        ),
                        "Card" => array ("HolderName"=>$input['name']),
                        "Billing" => array (
                            "Email" => $customerEmail,
                            "CityName"=>$input['city_bill'],
                            "CountryCode"=>$input['country_id_bill'],
                            "PostalCode"=>$input['postcode_bill'],
                            "StreetName"=>$input['street_bill'],
                        )
                    ),
                    "RequestType" => "0" // 0 = Sale Request Type
                );
                $response = Ezpay::PayWithToken($obj);

                if((@$response->Error)){
                    Session::forget('ezPayError');
                    Session::put('ezPayError', $response->Error->Message);
                    return '/gateway/ezerror';
                }else if(@$response->Result->Code != 0){
                    Session::forget('ezPayError');
                    Session::put('ezPayError', $response->Result->Message);
                    return '/gateway/ezerror';
                }
                else{
                    $transact = Transaction::where('order_id',$merchantId)->first();
                    $transact->status = 1;
                    $transact->save();
                        $paymentMethod = array(
                            "method" => "checkmo"
                        );
                    $resultPaymentMethod = TicketSoap::process("cart_payment.method", array($shoppingCartId, $paymentMethod));
                    Session::put('ezpayMerchantId', $merchantId);
                    //object to multidimensional array
                    //$resp_array = $this->flatten($trans_resp);
                    //saving transaction response from  gateway to sessioion
                    $trans_resp = json_decode(json_encode($response),true);
                    $resp = json_encode($trans_resp);
                    Session::put('respezpay',$resp);
                    return '/gateway/success';
                }

            }
            else {
                throw new Exception('Unable process your order. Payment gateway is not selected.');
            }

        } catch (Exception $e){
            $shoppingCartId = TicketSoap::process('cart.create', array('default'));
            Session::put($id.'_shoppingCartId', $shoppingCartId);
            Log::error( $e->getTraceAsString());
            $errorMessage = $e->getMessage();

            Log::info($errorMessage);
            if(is_array($errorMessage)) {
                $response = Response::make(json_encode($errorMessage), '400');
            } else {
                $response = Response::make($errorMessage, '400');
            }

            $response->header('Content-Type', 'application/json');

            return $response;
        }
    }

    public function buildPage6View($id)
    {

    }

    function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    function flatten($array, $prefix = '') {
          $result = array();
          foreach($array as $key=>$value) {
                if(is_array($value)) {
                     $result = $result + $this->flatten($value, $key );
                }
                else {
                     $result[$key] = $value;
                }
          }
          return $result;
    }


    public function validateInput($input, $page, $id)
    {
        $theRule = false;
        $rules1 = array(
            'number_of_ticket'=>'required|integer|between:0,1000',
            'voucherCode'=>''
        );

        $rules2 = array(
            'first_name'=>'required',
            'last_name'=>'required',
            'country_code'=>'required|Min:3',
            'contact_no'=>'required|numeric'
        );

        $rules3=array(
            'shipping_method'=>'required',
            'office_address'=>'',
            'country'=>'required',
            'street'=>'required',
            'postcode'=>'required',
            'city'=>'required',
            'checkin_dt'=>'date_format:Y-m-d H:i',
            'checkout_dt'=>'date_format:Y-m-d H:i',
            'hotel_name'=>'',
            'reservation_name'=>'',
            'is_hotel'=>'required',
            'is_office'=>'required',
            'allow_same_as_ship'=>'required'
        );

        $rules4 = array(
            'same_as_ship'=>'required',
            'street_bill' => 'required_if:same_as_ship,false',
            'postcode_bill' => 'required_if:same_as_ship,false',
            'city_bill' => 'required_if:same_as_ship,false',
            'country_id_bill' => 'required_if:same_as_ship,false',

        );




        $output = array();
        switch($page){
            case 0:
                //So, get the listing, find number of tickets
                $tick = RelatedTicket::where('product_id', $id)->first();
                if($tick)
                {
                    if(array_key_exists('number_of_ticket', $input))
                    {
                        if(intval($input['number_of_ticket'] > 0))
                        {
                            if($input['number_of_ticket'] > $tick->available_qty)
                            {
                                return false;
                            }

                            if(!$this->validateQty($id, $input['number_of_ticket']))
                            {
                                return false;
                            }

                        }
                        else
                        {
                            return false;
                        }
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }

                $theRule = $rules1;
                break;
            case 1:
                $theRule = $rules2;
                break;
            case 2:
                $theRule = $rules3;
                break;
            case 3:
                $theRule = $rules4;
                break;
            case 4:
                $theRule = $rules4;
                break;
        }

        if($theRule)
        {
            $validator = Validator::make(
                $input,
                $theRule
            );

            if(!$validator->fails())
            {
                foreach($theRule as $key=>$val)
                {
                    $output[$key] = $input[$key];
                }
                return $input;
            }
            else
            {
                $m = $validator->messages();
                return $m;
            }
        }
        return false;
    }

    public function validateQty($id, $qty)
    {
        $ticket = RelatedTicket::getTicketByTicketId($id);
        $t = RelatedTicket::where('product_id', $id)->first();

        if (!$ticket) {
            return false;
        }

        if(!$t)
        {
            return false;
        }

        $avail = $t->available_qty;

        $preference = (int)isset($ticket->ticket->ticketInformation->sell_preference) ? $ticket->ticket->ticketInformation->sell_preference : 0;

        switch($preference)
        {
            case 1:
                if($avail != $qty)
                {
                    return false;
                }
                break;
            case 2:
                if(($avail - $qty) == 1)
                {
                    return false;
                }
                break;
            case 3:
                if(($qty % 2) != 0)
                {
                    return false;
                }
                break;
            default:
                return true;
                break;
        }

        return true;
    }

    public function getLoginForm()
    {
        return View::make(Template::name('frontend.%s.SlidingCheckout.pages.2.partials.login'));
    }

    public function getRegisterForm()
    {
        $countryList = CountryList::getCountryListWithCountryCode();
        $country = [];
        foreach($countryList as $c) {
            $country[$c->Code] = $c->Country;
        }

        $country_list = DB::table('country_list')
            ->select(DB::raw("`ITU-T Telephone Code` AS `telephone_code`, CONCAT(`Common Name`,' ( ' ,`ITU-T Telephone Code`, ' )') AS Country"))
            ->where('ITU-T Telephone Code', '<>', '')
            ->where('Type', '=', 'Independent State')
            ->orderBy('Sort Order', 'ASC')
            ->get();

        $country_with_phone = array();
        foreach($country_list as $c ) {
            $phone_code = str_replace('+', '00', $c->telephone_code);
            //$country_with_phone[$phone_code] = $c->Country;
            $country_with_phone[] = array($phone_code, $c->Country);
        }

        View::share('country_with_phone', $country_with_phone);
        return View::make(Template::name('frontend.%s.SlidingCheckout.pages.2.partials.register'));
    }

    public function register()
    {
        $input = Input::all();

        $rules = array(
            'first_name'=>'required|min:3',
            'last_name'=>'required|min:3',
            'email'=>'required|email',
            'email_confirmation' => 'required|same:email',
            'password'=>'required',
            'password_confirmation'=>'required|same:password',
            'contact_no'=>'required|numeric',
            'country_code'=>'required|numeric',
            'terms_and_conditions'=>'required'
        );

        $v = Validator::make($input,$rules);

        if($v->fails())
        {
            $msgs = $v->messages();
            $ret = array();

            foreach($rules as $name=>$v)
            {
                $ret[$name] = $msgs->get($name);
            }

            return Response::json($ret,404);
        }

        $newCustomer = array(
            'first_name'            => $input['first_name'],
            'last_name'             => $input['last_name'],
            'email'                 => $input['email'],
            'email_confirmation'    => $input['email_confirmation'],
            'password_confirmation' => $input['password_confirmation'],
            'password'              => $input['password'],
            'terms_n_conditions'    => true,
            'country_code'          => $input['country_code'],
            'contact_no'            => $input['contact_no']
        );

        $response = CustomerController::registrationInternalAction($newCustomer);
        $user = $response;
        if(!isset($user['message']) || $user['message'] != "user successfully created.") {
            if(isset($user['message']))
            {
                return Response::json($user['message']?$user['message']:'Error', 404);
            }

            foreach($user as $error)
            {
                if(is_array($error))
                {
                    foreach($error as $e)
                    {
                        return Response::json($e,404);
                    }
                }
                else
                {
                    return Response::json($error,404);
                }
            }

            return Response::json("An error occurred. Please check all fields and try again.",404);
        }

        $api_path = Config::get('api.mage_soap_api_path');
        $email = $input['email'];
        $password = $input['password'];
        require_once("{$api_path}app/Mage.php");
        umask(0);
        Mage::app('default');

        Mage::getSingleton('core/session', array('name' => 'frontend'));
        $session = Mage::getSingleton('customer/session', array('name' => 'frontend'));

        if ($session->isLoggedIn()) {
            return Response::json('Already logged in.', 200);
        }

        try {
            try {
                $session->login($email, $password);

                $customer = Mage::getSingleton('customer/session')->getCustomer()->getData();
                Session::put('customer', $customer);

                return Response::json("success",200);

            } catch (Mage_Core_Exception $e) {
                switch ($e->getCode()) {
                    case Mage_Customer_Model_Customer::EXCEPTION_EMAIL_NOT_CONFIRMED:
                        throw new Exception ('Email is not confirmed. <a href="#">Resend confirmation email.</a>', 404);
                        break;
                    default:
                        throw new Exception ($e->getMessage(), 404);
                }
            }
        } catch (Exception $e) {
            return Response::json($e->getMessage(), 404);
        }

        return Response::json("success",200);
    }

    public function getShippingType($method)
    {
        $hotelMethods=array(
            'flatrate3',
            'flatrate6',
            'flatrate8',
            'flatrate10',
            'flatrate12',
            'flatrate16',
            'flatrate18',
            'flatrate20',
            'flatrate22',
            'flatrate24',
            'flatrate26',
            'flatrate28',
        );

        $standardMethods=array(
            'flatrate',
            'flatrate2',
            'flatrate5',
            'flatrate7',
            'flatrate9',
            'flatrate11',
            'flatrate14',
            'flatrate15',
            'flatrate17',
            'flatrate19',
            'flatrate21',
            'flatrate23',
            'flatrate25',
            'flatrate27',
        );

        $handMethods=array(
            'flatrate13',
        );

        $intMethods=array(
            'flatrate4'
        );

        $officeMethods=array(
            'flatrate29',
        );

        $printMethods = array(
            'flatrate30'
        );

        if(in_array($method, $hotelMethods))
        {
            return "hotel";
        }

        if(in_array($method, $standardMethods))
        {
            return 'standard';
        }

        if(in_array($method, $handMethods))
        {
            return 'hand';
        }

        if(in_array($method, $intMethods))
        {
            return 'international';
        }

        if(in_array($method, $officeMethods))
        {
            return 'office';
        }

        if(in_array($method, $printMethods))
        {
            return 'print';
        }

        return false;

    }

    public function sGetShipping($id)
    {
        $event = RelatedTicket::where('product_id', $id)->first();
        $event = $event->event_id;
        $dCountry = Input::get('del_country');

        $methods = $this->getShippingMethods($event, $id, $dCountry);

        if($methods->totalResult != 0)
        {
            foreach($methods->data as $method)
            {
                $type = $this->getShippingType($method->value);
                if($type=='hotel')
                {
                    $method->img = '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 50 50"><path fill="white" d="M 9 0 L 7.90625 2.3125 L 5.40625 2.59375 L 7.3125 4.3125 L 6.8125 6.90625 L 9 5.59375 L 11.1875 6.90625 L 10.6875 4.3125 L 12.59375 2.59375 L 10.09375 2.3125 L 9 0 z M 17 0 L 15.90625 2.3125 L 13.40625 2.59375 L 15.3125 4.3125 L 14.8125 6.90625 L 17 5.59375 L 19.1875 6.90625 L 18.6875 4.3125 L 20.59375 2.59375 L 18.09375 2.3125 L 17 0 z M 25 0 L 23.90625 2.3125 L 21.40625 2.59375 L 23.3125 4.3125 L 22.8125 6.90625 L 25 5.59375 L 27.1875 6.90625 L 26.6875 4.3125 L 28.59375 2.59375 L 26.09375 2.3125 L 25 0 z M 33 0 L 31.90625 2.3125 L 29.40625 2.59375 L 31.3125 4.3125 L 30.8125 6.90625 L 33 5.59375 L 35.1875 6.90625 L 34.6875 4.3125 L 36.59375 2.59375 L 34.09375 2.3125 L 33 0 z M 41 0 L 39.90625 2.3125 L 37.40625 2.59375 L 39.3125 4.3125 L 38.8125 6.90625 L 41 5.59375 L 43.1875 6.90625 L 42.6875 4.3125 L 44.59375 2.59375 L 42.09375 2.3125 L 41 0 z M 8.3125 7 C 5.3405 7 3 9.3405 3 12.3125 L 3 49 C 3 49.553 3.448 50 4 50 L 46 50 C 46.553 50 47 49.553 47 49 L 47 12.3125 C 47 9.3405 44.6595 7 41.6875 7 L 8.3125 7 z M 10 14 L 16 14 L 16 18 L 10 18 L 10 14 z M 22 14 L 28 14 L 28 18 L 22 18 L 22 14 z M 34 14 L 40 14 L 40 18 L 34 18 L 34 14 z M 10 22 L 16 22 L 16 26 L 10 26 L 10 22 z M 22 22 L 28 22 L 28 26 L 22 26 L 22 22 z M 34 22 L 40 22 L 40 26 L 34 26 L 34 22 z M 10 30 L 16 30 L 16 34 L 10 34 L 10 30 z M 22 30 L 28 30 L 28 34 L 22 34 L 22 30 z M 34 30 L 40 30 L 40 34 L 34 34 L 34 30 z M 10 38 L 16 38 L 16 42 L 10 42 L 10 38 z M 22 38 L 28 38 L 28 48 L 22 48 L 22 38 z M 34 38 L 40 38 L 40 42 L 34 42 L 34 38 z"></path></svg>';
                    $method->typeD = $type;
                }

                if($type=='standard')
                {
                    $method->img = '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 50 50"> <path fill="white" style="text-indent:0;text-align:start;line-height:normal;text-transform:none;block-progression:tb;-inkscape-font-specification:Bitstream Vera Sans" d="M 24.8125 0.0625 A 1.0001 1.0001 0 0 0 24.28125 0.375 L 0.375 25.25 A 1.0059637 1.0059637 0 1 0 1.84375 26.625 L 5 23.34375 L 5 49 L 5 50 L 6 50 L 44 50 L 45 50 L 45 49 L 45 23.34375 L 48.15625 26.625 A 1.0059637 1.0059637 0 1 0 49.625 25.25 L 25.71875 0.375 A 1.0001 1.0001 0 0 0 24.8125 0.0625 z M 25 2.5 L 43 21.25 L 43 48 L 32 48 L 32 28 L 32 27 L 31 27 L 19 27 L 18 27 L 18 28 L 18 48 L 7 48 L 7 21.25 L 25 2.5 z M 35 5 L 35 8.03125 L 37 10.09375 L 37 7 L 39 7 L 39 12.1875 L 41 14.28125 L 41 5 L 35 5 z M 20 29 L 30 29 L 30 48 L 20 48 L 20 29 z" color="#000" overflow="visible" enable-background="accumulate" font-family="Bitstream Vera Sans"></path></svg>';
                    $method->typeD = $type;
                }

                if($type=='hand')
                {
                    $method->img = '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 26 26"><path fill="white" d="M 6.5 3.0625 C 2.949 3.0625 0.0625 5.952 0.0625 9.5 C 0.0625 11.289839 0.81952384 12.897633 2 14.0625 L 2 22 C 2 22.551 2.449 23 3 23 L 5 23 C 5.245 21.277 6.71 19.9375 8.5 19.9375 C 10.29 19.9375 11.755 21.277 12 23 L 14 23 C 14.551 23 15 22.551 15 22 L 15 10 C 15 9.449 14.551 9 14 9 L 12.90625 9 C 12.649727 5.6868315 9.8760711 3.0625 6.5 3.0625 z M 6.5 4.46875 C 9.27 4.46875 11.53125 6.731 11.53125 9.5 C 11.53125 12.269 9.27 14.53125 6.5 14.53125 C 3.731 14.53125 1.46875 12.269 1.46875 9.5 C 1.46875 6.731 3.731 4.46875 6.5 4.46875 z M 6.5 5.53125 C 6.25 5.53125 6.16125 5.72875 6.15625 5.84375 L 6 8.71875 C 5.7369699 8.8875466 5.5625 9.1663596 5.5625 9.5 C 5.5625 10.021 5.978 10.4375 6.5 10.4375 C 6.5211622 10.4375 6.5416912 10.438878 6.5625 10.4375 L 8.28125 11.90625 C 8.36325 11.98025 8.5835 12.0405 8.8125 11.8125 C 9.0405 11.5835 9.0095 11.333 8.9375 11.25 L 7.4375 9.5 C 7.4375 9.1503288 7.2512845 8.8515184 6.96875 8.6875 L 6.84375 5.84375 C 6.83575 5.72875 6.743 5.53125 6.5 5.53125 z M 17 11 C 16.449 11 16 11.449 16 12 L 16 22 C 16 22.549 16.452 22.996 17 23 C 17.246 21.277 18.711 19.9375 20.5 19.9375 C 22.29 19.9375 23.755 21.277 24 23 L 25 23 C 25.551 23 26 22.551 26 22 L 26 17 C 26 16.053 25.21875 14.96875 25.21875 14.96875 L 23 12 C 22.543 11.449 22.234 11 21.5 11 L 17 11 z M 19 13 L 21.4375 13 C 21.5985 13 21.84375 13.25 21.84375 13.25 L 23.9375 16.15625 C 24.1245 16.43025 24.3125 16.75 24.3125 17 L 19 17 L 19 13 z M 8.5 21.1875 C 7.2225654 21.1875 6.1875 22.222565 6.1875 23.5 C 6.1875 24.777435 7.2225654 25.8125 8.5 25.8125 C 9.7774346 25.8125 10.8125 24.777435 10.8125 23.5 C 10.8125 22.222565 9.7774346 21.1875 8.5 21.1875 z M 20.5 21.1875 C 19.222565 21.1875 18.1875 22.222565 18.1875 23.5 C 18.1875 24.777435 19.222565 25.8125 20.5 25.8125 C 21.777435 25.8125 22.8125 24.777435 22.8125 23.5 C 22.8125 22.222565 21.777435 21.1875 20.5 21.1875 z"></path></svg>';
                    $method->typeD = $type;
                }

                if($type=='office')
                {
                    $method->img = '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 26 26"><path fill="white" d="M 4 0 C 2.343 0 1 1.343 1 3 L 1 26 L 25 26 L 25 3 C 25 1.343 23.657 0 22 0 L 4 0 z M 4 2 L 22 2 C 22.551 2 23 2.448 23 3 L 23 24 L 15 24 L 15 19 L 11 19 L 11 24 L 3 24 L 3 3 C 3 2.448 3.449 2 4 2 z M 5 4 L 5 7 L 9 7 L 9 4 L 5 4 z M 11 4 L 11 7 L 15 7 L 15 4 L 11 4 z M 17 4 L 17 7 L 21 7 L 21 4 L 17 4 z M 5 9 L 5 12 L 9 12 L 9 9 L 5 9 z M 11 9 L 11 12 L 15 12 L 15 9 L 11 9 z M 17 9 L 17 12 L 21 12 L 21 9 L 17 9 z M 5 14 L 5 17 L 9 17 L 9 14 L 5 14 z M 11 14 L 11 17 L 15 17 L 15 14 L 11 14 z M 17 14 L 17 17 L 21 17 L 21 14 L 17 14 z M 5 19 L 5 22 L 9 22 L 9 19 L 5 19 z M 17 19 L 17 22 L 21 22 L 21 19 L 17 19 z"></path></svg>';
                    $method->typeD = $type;
                }

                if($type=='international')
                {
                    $method->img='<svg xmlns="http://www.w3.org/2000/svg" version="1" width="50" height="50" viewBox="0 0 50 50"><path style="text-indent:0;text-align:start;line-height:normal;text-transform:none;block-progression:tb;-inkscape-font-specification:Bitstream Vera Sans" d="M 46.40625 0.1875 C 46.086446 0.21132235 45.758047 0.2754079 45.4375 0.34375 C 44.155314 0.61711849 42.779361 1.2126313 41.4375 1.90625 C 38.76406 3.2881729 36.342459 5.037241 35.3125 6.125 L 35.28125 6.125 L 28.53125 12.84375 L 4.53125 9.59375 A 1.0001 1.0001 0 0 0 3.6875 9.90625 L 0.28125 13.40625 A 1.0001 1.0001 0 0 0 0.625 15.03125 L 19.125 22.28125 L 9.59375 32 L 5 32 A 1.0001 1.0001 0 0 0 4.34375 32.25 L 0.34375 35.625 A 1.0001 1.0001 0 0 0 0.75 37.375 L 10.28125 39.71875 L 12.625 49.25 A 1.0001 1.0001 0 0 0 14.25 49.75 L 17.65625 46.75 A 1.0001 1.0001 0 0 0 18 46 L 18 40.40625 L 27.8125 30.875 L 34.96875 49.375 A 1.0001 1.0001 0 0 0 36.59375 49.71875 L 40.09375 46.3125 A 1.0001 1.0001 0 0 0 40.40625 45.46875 L 37.15625 21.46875 L 43.90625 14.71875 C 44.943425 13.681575 46.706772 11.201506 48.09375 8.53125 C 48.787239 7.1961219 49.382468 5.837792 49.65625 4.5625 C 49.930032 3.287208 49.935391 1.945238 49.03125 1 A 1.0001 1.0001 0 0 0 49 0.96875 C 48.526073 0.51542866 47.956727 0.296013 47.34375 0.21875 C 47.037261 0.1801185 46.726054 0.16367765 46.40625 0.1875 z M 46.5 2.21875 C 47.070506 2.1807654 47.403799 2.2967153 47.5625 2.4375 C 47.752495 2.6512583 47.894352 3.1927202 47.6875 4.15625 C 47.476907 5.137208 46.956511 6.3851281 46.3125 7.625 C 45.024478 10.104744 43.162825 12.618425 42.5 13.28125 L 35.40625 20.40625 A 1.0001 1.0001 0 0 0 35.09375 21.21875 L 38.34375 45.25 L 36.3125 47.21875 L 29.125 28.75 A 1.0001 1.0001 0 0 0 27.5 28.375 L 16.3125 39.28125 A 1.0001 1.0001 0 0 0 16 40 L 16 45.5625 L 14.15625 47.15625 L 12.0625 38.65625 A 1.0001 1.0001 0 0 0 11.34375 37.9375 L 3.125 35.90625 L 5.375 34 L 10 34 A 1.0001 1.0001 0 0 0 10.71875 33.6875 L 21.625 22.59375 A 1.0001 1.0001 0 0 0 21.25 20.96875 L 2.78125 13.71875 L 4.75 11.65625 L 28.78125 14.90625 A 1.0001 1.0001 0 0 0 29.59375 14.59375 L 36.71875 7.5 C 37.295744 6.8869434 39.852471 4.9752625 42.34375 3.6875 C 43.589389 3.0436187 44.854061 2.5235065 45.84375 2.3125 C 46.086776 2.2606856 46.309831 2.2314115 46.5 2.21875 z" color="#000" overflow="visible" enable-background="accumulate" font-family="Bitstream Vera Sans"></path></svg>';
                    $method->typeD = $type;
                }

                if($type=='print')
                {
                    $method->img = '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 26 26"><path fill="white" d="M 6.5 3.0625 C 2.949 3.0625 0.0625 5.952 0.0625 9.5 C 0.0625 11.289839 0.81952384 12.897633 2 14.0625 L 2 22 C 2 22.551 2.449 23 3 23 L 5 23 C 5.245 21.277 6.71 19.9375 8.5 19.9375 C 10.29 19.9375 11.755 21.277 12 23 L 14 23 C 14.551 23 15 22.551 15 22 L 15 10 C 15 9.449 14.551 9 14 9 L 12.90625 9 C 12.649727 5.6868315 9.8760711 3.0625 6.5 3.0625 z M 6.5 4.46875 C 9.27 4.46875 11.53125 6.731 11.53125 9.5 C 11.53125 12.269 9.27 14.53125 6.5 14.53125 C 3.731 14.53125 1.46875 12.269 1.46875 9.5 C 1.46875 6.731 3.731 4.46875 6.5 4.46875 z M 6.5 5.53125 C 6.25 5.53125 6.16125 5.72875 6.15625 5.84375 L 6 8.71875 C 5.7369699 8.8875466 5.5625 9.1663596 5.5625 9.5 C 5.5625 10.021 5.978 10.4375 6.5 10.4375 C 6.5211622 10.4375 6.5416912 10.438878 6.5625 10.4375 L 8.28125 11.90625 C 8.36325 11.98025 8.5835 12.0405 8.8125 11.8125 C 9.0405 11.5835 9.0095 11.333 8.9375 11.25 L 7.4375 9.5 C 7.4375 9.1503288 7.2512845 8.8515184 6.96875 8.6875 L 6.84375 5.84375 C 6.83575 5.72875 6.743 5.53125 6.5 5.53125 z M 17 11 C 16.449 11 16 11.449 16 12 L 16 22 C 16 22.549 16.452 22.996 17 23 C 17.246 21.277 18.711 19.9375 20.5 19.9375 C 22.29 19.9375 23.755 21.277 24 23 L 25 23 C 25.551 23 26 22.551 26 22 L 26 17 C 26 16.053 25.21875 14.96875 25.21875 14.96875 L 23 12 C 22.543 11.449 22.234 11 21.5 11 L 17 11 z M 19 13 L 21.4375 13 C 21.5985 13 21.84375 13.25 21.84375 13.25 L 23.9375 16.15625 C 24.1245 16.43025 24.3125 16.75 24.3125 17 L 19 17 L 19 13 z M 8.5 21.1875 C 7.2225654 21.1875 6.1875 22.222565 6.1875 23.5 C 6.1875 24.777435 7.2225654 25.8125 8.5 25.8125 C 9.7774346 25.8125 10.8125 24.777435 10.8125 23.5 C 10.8125 22.222565 9.7774346 21.1875 8.5 21.1875 z M 20.5 21.1875 C 19.222565 21.1875 18.1875 22.222565 18.1875 23.5 C 18.1875 24.777435 19.222565 25.8125 20.5 25.8125 C 21.777435 25.8125 22.8125 24.777435 22.8125 23.5 C 22.8125 22.222565 21.777435 21.1875 20.5 21.1875 z"></path></svg>';
                    $method->typeD = $type;
                }
            }

            usort($methods->data, array($this, "sortShippingFields"));

            return Response::json($methods, 200);
        }

        return Response::json("No shipping methods available", 404);
    }

    public function sortShippingFields($a, $b)
    {
        if(isset($a->sort) && isset($b->sort))
        {
            return $a->sort > $b->sort;
        }
        return false;
    }

    public function sGetShippingForm()
    {
        if(Input::has('method'))
        {
            $method = Input::get('method');
            $dCountry = Input::get('dCountry');

            $customer = Session::get('customer');

            if (isset($customer['entity_id'])) {
                //get customer address ids, shipping address
                $addressParams = array(
                    'customer_id'   => $customer['entity_id'],
                    'type'          => 'get',
                    'address_type'  => 'shipping'
                );

                $this->setDataToPost($addressParams);
                $this->submitPostToApi('customer/index/address');
                $response = $this->getApiResponse();
                $shipping = json_decode($response, true);
                View::share('shipping', $shipping['data']);

                //If the shipping method is hand delivery just like...show the next and back
                //If the shipping method is hotel then show the hotel
                //If the shipping method is anything else show just standard

                $type = $this->getShippingType($method);
                if($type=='hotel')
                {
                    View::share('sm', $method);
                    return View::make(Template::name('frontend.%s.SlidingCheckout.pages.4.partials.hotel'));
                }

                if($type=='standard')
                {
                    View::share('sm', $method);
                    return View::make(Template::name('frontend.%s.SlidingCheckout.pages.4.partials.address'));
                }

                if($type=='hand')
                {
                    View::share('sm', $method);
                    return View::make(Template::name('frontend.%s.SlidingCheckout.pages.4.partials.hand'));
                }

                if($type=='international')
                {
                    View::share('sm', $method);
                    return View::make(Template::name('frontend.%s.SlidingCheckout.pages.4.partials.address'));
                }

                if($type=='office')
                {
                    View::share('sm', $method);
                    return View::make(Template::name('frontend.%s.SlidingCheckout.pages.4.partials.office'));
                }

                if($type=='print')
                {
                    View::share('sm', $method);
                    return View::make(Template::name('frontend.%s.SlidingCheckout.pages.4.partials.print'));
                }
            }
            else
            {
                return Response::json("Not logged in", 404);
            }
        }
        return Response::json("Requires shipping method input",404);
    }

    public function checkTicketState($id)
    {
        $ticket = RelatedTicket::where('product_id', $id)->first();

        if($ticket)
        {
            if($ticket->ticket_status == "active")
            {

                if(!$ticket->available_qty <= 0)
                {
                    $event = FootBallEvent::where('id', $ticket->event_id)->first();

                    if($event)
                    {
                        $et = DateTime::createFromFormat('Y-m-d H:i:s', $event->datetime);
                        $dt = new DateTime;

                        if($et)
                        {
                            if($dt < $et)
                            {
                                return true;
                            }
                        }
                    }
                }
            }
        }

        return false;
    }
}
