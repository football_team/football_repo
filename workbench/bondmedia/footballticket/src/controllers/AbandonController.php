<?php

class AbandonController extends BaseController {
    public function processInput()
    {
        if((Input::has('val'))&&(Input::has('field'))&&(Input::has('pid')))
        {
            $pid = Input::get('pid');
            $cid = Session::get($pid.'_shoppingCartId');
            $res = DB::table('follow_up_carts')
                ->where('cid', $cid)
                ->first();

            $field = Input::get('field');
            $val = Input::get('val');

            if(Session::has('customer'))
            {
                $customer = Session::get('customer');
                $user = WebUser::where('user_id', $customer['entity_id'])->first();
                if(!$res)
                {
                    DB::table('follow_up_carts')
                        ->insert(['cid'=>$cid, 'pid'=>$pid ,'firstname'=>$customer['firstname'],  'lastname'=>$customer['lastname'], 'email'=>$customer['email'], 'telephone'=>$user->telephone]);
                }
                else
                {
                    DB::table('follow_up_carts')
                        ->where('cid', $cid)
                        ->update(['firstname'=>$customer['firstname'], 'pid'=>$pid ,'lastname'=>$customer['lastname'], 'email'=>$customer['email'], 'telephone'=>$user->telephone]);
                }
            }
            else if(in_array($field, ['firstname', 'lastname', 'email', 'telephone']))
            {
                if(!$res)
                {
                    DB::table('follow_up_carts')
                        ->insert(['cid'=>$cid, $field=>$val, 'pid'=>$pid]);
                }
                else
                {
                    DB::table('follow_up_carts')
                        ->where('cid', $cid)
                        ->update([$field=>$val]);
                }
            }
            else
            {
                return Response::json("Invalid Arg", 404);
            }

            return Response::json("Success", 200);
        }
    }

    public function processLogin($pid)
    {
        $cid = Session::get($pid.'_shoppingCartId');
        $res = DB::table('follow_up_carts')
            ->where('cid', $cid)
            ->first();

        if(Session::has('customer'))
        {
            $customer = Session::get('customer');
            $user = WebUser::where('user_id', $customer['entity_id'])->first();
            if(!$res)
            {
                DB::table('follow_up_carts')
                    ->insert(['cid'=>$cid, 'pid'=>$pid ,'firstname'=>$customer['firstname'],  'lastname'=>$customer['lastname'], 'email'=>$customer['email'], 'telephone'=>$user->telephone]);
            }
            else
            {
                DB::table('follow_up_carts')
                    ->where('cid', $cid)
                    ->update(['firstname'=>$customer['firstname'], 'pid'=>$pid ,'lastname'=>$customer['lastname'], 'email'=>$customer['email'], 'telephone'=>$user->telephone]);
            }
        }

        return Response::json('success', 200);
    }
}