<?php
class AgentController extends BaseController {

    public function index(){
        return View::make('footballticket::admin.footballticket.agent.agent-index');
    }

    public function agentDT(){
        $start = 0;
        $length = 10;

        if(Input::has('start'))
        {
            $start = Input::get('start');
        }

        if(Input::has('length'))
        {
            $length = Input::get('length');
        }

        $output = [];

        $agents = Agent::where('status','active')->get();
        $total = Agent::where('status','active')->count();

        foreach($agents as $agent)
        {
            $temp = [];

            $temp['0'] = $agent->id;
            $temp['1'] = '<input type="text" id="fname-edit-'.$agent->id.'" value="'.$agent->fname.'">';
            $temp['2'] = '<input type="text" id="lname-edit-'.$agent->id.'" value="'.$agent->lname.'">';
            $temp['3'] = '<input type="text" id="code-edit-'.$agent->id.'" value="'.$agent->agent_code.'">';
            $temp['4'] = '<input type="text" id="discount-edit-'.$agent->id.'" value="'.$agent->discount_perc.'">';
            $temp['5'] = '<input type="text" id="agent-perc-edit-'.$agent->id.'" value="'.$agent->agent_perc.'">';
            if($agent->status == "active")
            {
                $temp['6'] = '<select id="status-edit-'.$agent->id.'">';
                $temp['6'] .= '<option value="active" selected>Active</option>';
                $temp['6'] .= '<option value="deleted">Deleted</option>';
                $temp['6'].='</select>';
            }
            else
            {
                $temp['6'] = '<select id="status-edit-'.$agent->id.'">';
                $temp['6'] .= '<option value="active">Active</option>';
                $temp['6'] .= '<option value="deleted" selected>Deleted</option>';
                $temp['6'] .='</select>';
            }

            $temp['7'] = '<div class="btn btn-success agent-save" data-id="'.$agent->id.'">Save</div>';

            $output[]=$temp;
        }

        $results['data'] = $output;
        $results['recordsTotal'] = $total;
        $results['recordsFiltered'] = $total;
        $response = Response::make($results);
        $response->header('Content-Type', 'application/json');
        return $response;
    }

    public function addAgent(){
        $input = Input::all();

        $validator = Validator::make($input,
                array(
                    'fname'=>'required',
                    'lname'=>'required',
                    'agent_code'=>'required',
                    'discount_perc'=>'required|integer',
                    'agent_perc'=>'required|integer'
                )
            );

        if(!$validator->fails())
        {
            $agent = new Agent();
            $agent->fill($input);
            $agent->save();

            return Response::json("success",200);
        }

        return Response::json('Please make sure all fields are filled in. Discount must be a whole number',404);
    }

    public function updateAgent($id){
        $input = Input::all();

        $validator = Validator::make($input,
            array(
                'fname'=>'required',
                'lname'=>'required',
                'agent_code'=>'required',
                'discount_perc'=>'required|integer',
                'status' => 'required',
                'agent_perc'=>'required|integer',
            )
        );

        if(!$validator->fails())
        {
            if($input['status']=="active" || $input['status']=='deleted')
            {
                $agent = Agent::where('id',$id)->first();

                if($agent)
                {
                    $agent->fill($input);
                    $agent->save();

                    return Response::json("success",200);
                }

                return Response::json("Could not find agent",404);
            }

            return Response::json("Invalid status value",404);
        }

        return Response::json('Please make sure all fields are filled in. Discount must be a whole number',404);
    }

    public function sales(){
        return View::make('footballticket::admin.footballticket.agent.agent-sales');
    }

    public function salesTotalDT()
    {

        $output = [];
        
        $agentAffilAT = DB::table('affiliate_sales')
            ->leftJoin('events_ticket_buy as etb', 'affiliate_sales.order_id', '=', 'etb.order_id')
            ->select(DB::raw('
                COUNT(*) as salesAT,
                SUM(etb.amount) as amtAT,
                SUM((etb.amount/100)*affiliate_sales.agent_perc) as earningAT,
                SUM(IF(affiliate_sales.agent_paid = 0, (etb.amount/100)*affiliate_sales.agent_perc, 0)) as liabilityAT

            '))
            ->whereRaw('affiliate_sales.agent_id IS NOT NULL')
            ->get();

        $agentDirectAT = DB::table('agent_sale')
            ->leftJoin('events_ticket_buy as etb', 'agent_sale.order_id', '=', 'etb.order_id')
            ->select(DB::raw('COUNT(*) as salesAT,
                SUM(etb.amount) as amtAT,
                SUM((etb.amount/100) * agent_sale.agent_perc) as earningAT,
                SUM(IF(agent_sale.paid = 0, agent_sale.agent_earning, 0)) as liabilityAT'))
            ->get();



        return Response::json("ok");
    }


}