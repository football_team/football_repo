<?php

class ContactController extends BaseController {
    public function index() {

    }

    public function contactFromSeller() {
        $formData = array(
            '_message'      => Input::get('message'),
            'event_id'      => Input::get('event_id'),
            'order_id'      => Input::get('order_id'),
            'game'          => Input::get('game')
        );

        $rules = array(
            '_message'      => 'required',
            'event_id'      => 'required',
            'order_id'      => 'required',
            'game'          => 'required'
        );

        $validation = Validator::make($formData, $rules);

        try {
            if ($validation->fails()) {
                throw new Exception($validation->messages());

            }

            $etb = FootballTicketBuy::where('order_id', $formData['order_id'])->first();
            $ert = RelatedTicket::where('product_id', $etb->product_id)->first();
            $event = FootBallEvent::where('id', $ert->event_id)->first();
            $u = WebUser::where('user_id', $ert->user_id)->first();

            $formData['user_object'] = $u;
            $formData['event_object'] = $event;
            $formData['ticket_object'] = $ert;

            Mail::send('footballticket::email.contact-from-seller', $formData, function ($message) {
                $from = Config::get('mail.from');
                $from['address'] = trans('homepage.contact-email');
                $to = Config::get('mail.seller_contact');
                $message->from($from['address'], $from['name']);
                $email = trans('homepage.contact-email');
                $message->to($email, 'Football Ticket Pads')->subject("Contact From Seller");
            });

            return Response::json('success', 200);

        } catch (Exception $e) {
            return Response::json($e->getMessage(), 400);
        }

    }

    public function groupPurchased() {
        $formData = array(
            '_message'      => Input::get('message'),
            'event_id'      => Input::get('event_id'),
            'game'          => Input::get('game'),
            'name'          => Input::get('name'),
            'company_name'  => Input::get('company_name'),
            'email'         => Input::get('email'),
            'tel'           => Input::get('tel'),
            'captcha'       => Input::get('captcha')
        );

        $rules = array(
            '_message'      => 'required',
            'event_id'      => 'required',
            'game'          => 'required',
            'name'          => 'required',
            'company_name'  => 'required',
            'email'         => 'required',
            'tel'           => 'required',
            'captcha'       => 'required|captcha'
        );

        $validation = Validator::make($formData, $rules);

        try {
            if ($validation->fails()) {
                $errors = $validation->messages()->toArray();;
                $errors['captcha_image'] = HTML::image(Captcha::img(), 'Captcha image');

                $response = Response::make(json_encode(array('result' => $errors )), 400);
                $response->header('Content-Type', 'application/json');
                return $response;
            }

            Mail::send('footballticket::email.group-purchasede', $formData, function ($message) {
                $from = Config::get('mail.from');
                $to = trans('homepage.contact-email');
                $from['address'] = trans('homepage.contact-email');
                $message->from($from['address'], $from['name']);
                $email = trans('homepage.contact-email');

                $message->to($email, 'Football Ticket Pad')
                    ->subject("Group Ticket Request");
                if(trim($email) != 'info@footballticketpad.com' ) {
                    $message->bcc('info@footballticketpad.com', 'Football Ticket Pads');
                }
            });

            return Response::json('success', 200);
        } catch (Exception $e) {
            return Response::json($e->getMessage(), 400);
        }

    }
}
