<?php

use Bond\Interfaces\BaseModelInterface as BaseModelInterface;

class FootballTickets extends BaseModel implements BaseModelInterface {

    public $table = 'football_ticket';
    public $fillable=['title', 'slug', 'content', 'is_published', 'type',
        'feature_image', 'venue_image', 'featured', 'order',
        'meta_title', 'meta_content', 'meta_description', 'widget', 'widget_order', 'venue_splash', 'is_international'
    ];
    protected $appends = ['url'];

    public function __construct() {
        parent::__construct();
    }

    public function setUrlAttribute($value) {

        $this->attributes['url'] = $value;
    }

    public function getUrlAttribute() {
        return "{$this->slug_prefix}/{$this->attributes['slug']}";
    }

    public static function getDataForOptions($type) {
        $results = DB::table('football_ticket')->select('id', 'title')->where('type', '=', $type)->get();
        $output = array(''=>'Select');
        foreach($results as $r) {
            $output[$r->id] = $r->title;
        }
        return $output;
    }


    public static function getClubs($tournament_id, $season_id) {
        $locale = \App::getLocale();
        $output = array();
        $results = DB::table('football_ticket_club_tournaments')
                   ->join('football_ticket', 'football_ticket.id', '=','football_ticket_club_tournaments.club_id')
                   ->select('football_ticket.*')
                   ->where('football_ticket_club_tournaments.tournament_id','=',$tournament_id)
                   ->where('football_ticket_club_tournaments.season_id', '=', $season_id)
                   ->groupBy('football_ticket.id')
                   ->orderBy('football_ticket.title', 'ASC')
                   ->get();
        foreach($results as $result) {
            $trans = DB::table('locale_text')
                ->where('id', 'fbt_title_'.$result->id)
                ->where('locale', $locale)
                ->first();

            if($trans)
            {
                $result->title = $trans->lstring;
            }

            $result->clubLogo = FootballTicketMeta::getTicketMeta($result->id, 'club_logo');
            $output[] = $result;
        }
        return $output;
    }

    public static function getTeamRichSnippets($clubID)
    {

        //Get all future events where the clubId is either home team or away team
        $results = DB::table('events')
            ->leftJoin('event_stadium', 'events.id', '=', 'event_stadium.event_id')
            ->leftJoin('stadium', 'event_stadium.stadium_id', '=', 'stadium.id')
            ->leftJoin('football_ticket as tournament', 'events.tournament_id', '=', 'tournament.id')
            ->whereRaw('events.datetime >= CURDATE() AND ( events.away_team_id = '.$clubID.' OR events.home_team_id='.$clubID.'  )' )
            ->select('stadium.id as stadium_id', 'stadium.al1', 'stadium.al2', 'stadium.postcode', 'stadium.country_id','events.datetime',
                'events.home_team_id', 'events.away_team_id', 'events.event_in_home', 'events.title as event_title', 'events.slug as event_slug' ,'stadium.title as stadium_title',
                'tournament.title as tournament_title', 'tournament.slug as tournament_slug')
            ->orderBy('events.datetime', 'ASC')
            ->take(3)
            ->get();

        $out = array();

        foreach($results as $result){
            if((isset($result->stadium_id))&&($result->stadium_id))
            {
                $dt = DateTime::createFromFormat('Y-m-d H:i:s', $result->datetime);
                $temp = array();
                $temp['event_title'] = $result->event_title;
                $temp['url'] = '/'.$result->tournament_slug.'/'.$result->event_slug;
                $temp['stadium_name'] = $result->stadium_title;
                $temp['address_al1'] = $result->al1;
                $temp['address_al2'] = $result->al2;
                $temp['address'] = $result->al1.', ';
                if($result->al2 != '')
                {
                    $temp['address'] .= $result->al2.', ';
                }
                $temp['address'] .= $result->postcode;
                $temp['postcode'] = $result->postcode;
                $temp['event_date'] = $dt->format('Y-m-d');
                $out[] = $temp;
            }
            else
            {
                if($result->event_in_home)
                {
                    $tIQ = $result->home_team_id;
                }
                else{
                    $tIQ = $result->away_team_id;
                }

                $wa = DB::table('team_stadium')
                            ->leftJoin('stadium', 'team_stadium.stadium_id','=','stadium.id')
                            ->where('team_stadium.team_id','=',$tIQ)
                            ->select('stadium.id as stadium_id', 'stadium.al1', 'stadium.al2', 'stadium.postcode', 'stadium.title as stadium_title')
                            ->first();

                if($wa)
                {
                    $dt = DateTime::createFromFormat('Y-m-d H:i:s', $result->datetime);
                    $temp = array();
                    $temp['event_title'] = $result->event_title;
                    $temp['url'] = '/'.$result->tournament_slug.'/'.$result->event_slug;
                    $temp['stadium_name'] = $wa->stadium_title;
                    $temp['address_al1'] = $wa->al1;
                    $temp['address_al2'] = $wa->al2;
                    $temp['postcode'] = $wa->postcode;
                    $temp['event_date'] = $dt->format('Y-m-d');

                    $temp['address'] = $wa->al1.', ';
                    if($wa->al2 != '')
                    {
                        $temp['address'] .= $wa->al2.', ';
                    }
                    $temp['address'] .= $wa->postcode;
                    $out[] = $temp;
                }
            }
        }
        return $out;

    }
}

