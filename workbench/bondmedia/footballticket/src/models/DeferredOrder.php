<?php

class DeferredOrder extends BaseModel {
    public $table = 'deferred_orders';
    public $fillable=['order_id', 'buyer_id','event_id', 'product_id', 'qty', 'amount', 'delivery_status','payment_status','seller_payment_status', 'seller_amount_paid', 'listed_amount', 'tracked'];
    public function __construct() {
        parent::__construct();
    }
}