<?php

use Bond\Interfaces\BaseModelInterface as BaseModelInterface;

class CollectionPoint extends BaseModel implements BaseModelInterface {

    public $table = 'collection_points';
    public $fillable=['order_id', 'collection_date', 'location_1', 'location_2', 'location_3', 'postcode'];
    public $timestamps = false;

    public function __construct() {
        parent::__construct();
    }
    public function getUrlAttribute()
    {
        return true;
    }
    public function setUrlAttribute($value)
    {
        return false;
    }
}