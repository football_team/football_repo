<?php

class Agent extends \Illuminate\Database\Eloquent\Model{
    public $table = 'agents';
    public $timestamps = false;
    protected $fillable = ['fname', 'lname', 'agent_code', 'discount_perc', 'status', 'agent_perc'];
}