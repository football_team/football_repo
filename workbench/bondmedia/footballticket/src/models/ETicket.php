<?php

use Bond\Interfaces\BaseModelInterface as BaseModelInterface;

class ETicket extends BaseModel implements BaseModelInterface {

    public $table = 'e_ticket_uploads';
    public $fillable=['order_id', 'pdf_path', 'available'];
    public $timestamps = false;

    public function __construct() {
        parent::__construct();


    }
    public function getUrlAttribute()
    {
        return true;
    }
    public function setUrlAttribute($value)
    {
        return false;
    }
}

