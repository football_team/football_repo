<?php

class FootballTicketBuy extends BaseModel {
    public $table = 'events_ticket_buy';
    public $fillable=['order_id', 'buyer_id','event_id', 'product_id', 'qty', 'amount', 'delivery_status','payment_status','seller_payment_status', 'seller_amount_paid', 'listed_amount', 'tracked', 'referral_url', 'site_sold'];
    public function __construct() {
        parent::__construct();
    }

    public static function getTicketsByUserId($userId) {
        $results = DB::table('events_ticket_buy')
            ->join('events_related_tickets', 'events_ticket_buy.product_id', '=', 'events_related_tickets.product_id')
            ->join('events', 'events.id', '=', 'events_related_tickets.event_id')
            ->leftJoin('agent_sale', 'events_ticket_buy.order_id', '=', 'agent_sale.order_id')
            ->where('buyer_id', '=', $userId)
            ->select('events_related_tickets.*', 'events_ticket_buy.qty', 'events_ticket_buy.amount','events_ticket_buy.payment_status', 'events_ticket_buy.order_id', 'events_ticket_buy.created_at AS orderDate', 'events.datetime as eventDate', 'events_ticket_buy.delivery_status', 'agent_sale.discount_amount as agent_discount_amount')
            ->where('events.datetime', '>=', date('Y-m-d 00:00:00'))
            ->orderBy('events_ticket_buy.created_at', 'DESC')
            ->groupBy('events_ticket_buy.id')
            ->get();
        return $results;
    }

    public static function getSoldTicketsBySellerId($userId) {
        $results = DB::table('events_ticket_buy')
            ->join('events_related_tickets', 'events_ticket_buy.product_id', '=', 'events_related_tickets.product_id')
            ->leftJoin('shipping_tracking', 'shipping_tracking.order_id', '=', 'events_ticket_buy.order_id')
            ->where('events_related_tickets.user_id', '=', $userId)
            ->select('events_related_tickets.*','events_ticket_buy.listed_amount','shipping_tracking.tracking_code','events_ticket_buy.qty', 'events_ticket_buy.amount','events_ticket_buy.payment_status', 'events_ticket_buy.order_id','events_ticket_buy.seller_payment_status', 'events_ticket_buy.created_at AS orderDate', 'events_ticket_buy.delivery_status')
            ->groupBy('events_ticket_buy.order_id')
            ->orderBy('events_ticket_buy.created_at','DESC')
            ->get();
        return $results;
    }

    public static function getBuyerSellerIdByOrderId($orderId)
    {
      $results = DB::table('events_ticket_buy')
        ->join('events_related_tickets', 'events_ticket_buy.product_id', '=', 'events_related_tickets.product_id')
        ->where('events_ticket_buy.order_id', '=', $orderId)
        ->select('events_ticket_buy.buyer_id', 'events_related_tickets.user_id')
        ->get();
      return $results;
    }

    public static function createSaleView() {
        if(!Schema::hasTable('sales')) {
            DB::statement('CREATE OR REPLACE VIEW `sales` AS
                       SELECT
                       `etb`.`id` AS `id`,
                       `etb`.`order_id` AS `order_id`,
                       `etb`.`buyer_id` AS `buyer_id`,
                       `etb`.`product_id` AS `product_id`,
                       `etb`.`qty` AS `qty`,
                       `etb`.`amount` AS `amount`,
                       `etb`.`commission` AS `commission`,
                       `etb`.`commission_amount` AS `commission_amount`,
                       `etb`.`fees_amount` AS `fees_amount`,
                       `etb`.`delivery_status` AS `delivery_status`,
                       `etb`.`payment_status` AS `payment_status`,
                       `etb`.`updated_at` AS `updated_at`,
                       `etb`.`created_at` AS `created_at`,
                       `etb`.`listed_amount` AS `listed_amount`,
                       `bwu`.`firstname` AS `buyer_firstname`,
                       `bwu`.`lastname` AS `buyer_lastname`,
                       `bwu`.`email` AS `buyer_email`,
                       `ert`.`user_id` AS `seller_id`,
                       `ert`.`event_id` AS `event_id`,

                       `swu`.`firstname` AS `seller_firstname`,
                       `swu`.`lastname` AS `seller_lastname`,
                       `swu`.`email` AS `seller_email`,
                        etb.seller_payment_status As seller_payment_status,
                        e.`title` As title,
                        e.`datetime` AS event_date
                        FROM `events_ticket_buy` `etb`

                        left join `web_user` `bwu` on `bwu`.`user_id` = `etb`.`buyer_id`
                        left join `events_related_tickets` `ert` on `ert`.`product_id` = `etb`.`product_id`
                        left join `web_user` `swu` on `swu`.`user_id` = `ert`.`user_id`
                        left join events as e on e.id = ert.`event_id`');
        }
    }

    public function getAccountSalesViewByUserID($id)
    {
        $results = DB::table("events_ticket_buy")
            ->leftJoin('events_related_tickets', 'events_ticket_buy.product_id', '=', 'events_related_tickets.product_id')
            ->leftJoin('events', 'events_related_tickets.event_id', '=', 'events.id')
            ->leftJoin('web_user', 'events_related_tickets.user_id', '=' ,'web_user.user_id')
            ->leftJoin('football_ticket as home', 'events.home_team_id', '=', 'home.id')
            ->leftJoin('football_ticket as away', 'events.away_team_id', '=', 'away.id')
            ->groupBy('events_related_tickets.event_id')
            ->where('web_user.user_id','=', $id)
            ->select(
                DB::raw('
                    count(*) as total,
                    SUM(IF(events_ticket_buy.delivery_status = "pending", 1, 0)) as undelivered
                '),
                'events.title as title',
                'events.datetime',
                'events_ticket_buy.created_at as orderDate',
                'events.feature_image',
                'home.venue_splash as home_splash',
                'away.venue_splash as away_splash',
                'events.event_in_home as isHome',
                'events.slug as slug',
                'events.datetime as datetime',
                'events.event_location as location',
                'events.id as event_id'
            )
            ->orderBy('events.datetime', 'asc')
            ->get();

        return $results;
    }

    public function getSalesByEventAndUserId($cId, $eId)
    {   
        $results = DB::table("events_ticket_buy")
            ->leftJoin('events_related_tickets', 'events_ticket_buy.product_id', '=', 'events_related_tickets.product_id')
            ->leftJoin('web_user as seller', 'events_related_tickets.user_id', '=' ,'seller.user_id')
            ->leftJoin('web_user as buyer', 'events_ticket_buy.buyer_id', '=', 'buyer.user_id')
            ->leftJoin('shipping_tracking', 'events_ticket_buy.order_id', '=', 'shipping_tracking.order_id')
            ->where('seller.user_id','=', $cId)
            ->where('events_related_tickets.event_id', '=', $eId)
            ->where('events_ticket_buy.created_at', '>', '2015/06/06')
            ->orderBy('events_ticket_buy.order_id', 'desc')
            ->select(
                'events_related_tickets.ticket as ticket',
                'events_ticket_buy.qty as qty',
                'events_ticket_buy.order_id as order_id',
                'events_ticket_buy.listed_amount as listed_amount',
                'buyer.firstname as buyer_fname',
                'buyer.lastname as buyer_lname',
                'buyer.telephone as buyer_cnum',
                'shipping_tracking.tracking_code as tcode',
                'events_ticket_buy.delivery_status as delivery_status',
                'events_ticket_buy.seller_payment_status as seller_payment_status',
                'events_related_tickets.product_id as product_id',
                'events_related_tickets.event_id as event_id'
            )
            ->get();
        return $results;
    }

    public function getTicketTypeByOrderId($oId)
    {
        $result = DB::table('events_ticket_buy')
            ->leftJoin('events_related_tickets', 'events_ticket_buy.product_id', '=', 'events_related_tickets.product_id')
            ->where('events_ticket_buy.order_id','=',$oId)
            ->select('events_related_tickets.ticket as ticket')
            ->first();
        $ticket = json_decode($result->ticket);
        $formOfTicket = $ticket->ticketInformation->form_of_ticket;

        return $formOfTicket;
    }
    public function getEventAndProductIdByOrderId($oId)
    { 
        $result = DB::table('events_ticket_buy')
            ->leftJoin('events_related_tickets', 'events_ticket_buy.product_id', '=', 'events_related_tickets.product_id')
            ->leftJoin('shipping_tracking', 'events_ticket_buy.order_id', '=', 'shipping_tracking.order_id')
            ->where('events_ticket_buy.order_id','=',$oId)
            ->select(
                'events_related_tickets.event_id as event_id',
                'events_related_tickets.product_id as product_id',
                'shipping_tracking.tracking_code as tcode'
            )
            ->first();
        return $result;
    }
}
