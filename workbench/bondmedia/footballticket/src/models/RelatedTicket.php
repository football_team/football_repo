<?php

class RelatedTicket extends \Illuminate\Database\Eloquent\Model{
    public $table = 'events_related_tickets';
    protected $fillable = ['price','event_id', 'product_id', 'ticket' , 'available_qty', 'user_id', 'selling_commission_percentage', 'selling_commission_amount', 'hand_delivery', 'sec_check','site_sold'];

    public static function getTicketsByEvent($eventId = '') {
        if (empty($eventId)) {
            return null;
        }
        $output = array();
        return $output;
    }

    public static function getTicketByTicketId($id = '') {
        if(empty($id)) {
            App::abort(404);
        }

        $output = [];
        $t = 'events_related_tickets';
        $e = 'events';
        $result= DB::table($t)
            ->select("{$t}.ticket", "{$t}.event_id", "{$e}.title", "{$e}.datetime", "{$e}.event_location", "{$t}.available_qty", "{$t}.price", "{$t}.ticket_status", "{$e}.is_tbc")
            ->leftJoin($e, "{$t}.event_id", "=", "{$e}.id")
            ->where("{$t}.product_id",'=',$id)->get();
        if(count($result) > 0) {
            $output = $result[0];
            $output->ticket = json_decode($output->ticket);
            $output->ticketType = TicketType::find($output->ticket->ticketInformation->ticket_type)->toArray();
            $formOfTicket = FormOfTicket::find($output->ticket->ticketInformation->form_of_ticket);
            $output->formOfTicket = $formOfTicket?$formOfTicket->toArray():array();
            $output->info = json_decode(json_encode($output->ticket->ticketInformation), true);
            $output->buyerNot = isset($output->info['restrictions']['others'])? $output->info['restrictions']['others']: '';
            if(isset($output->info['restrictions']['others'])) {
                unset($output->info['restrictions']['others']);
            }

            if(!isset($output->info['restrictions'])) {
                $output->info['restrictions'] = array();
            }

        }
        return $output;
    }

    public static function getEventsByUser($id = '') {
        if(empty($id)) {
            App::abort(404);
        }
        $t = 'events_related_tickets';
        $e = 'events';
        $results = DB::table($t)
            ->select("{$t}.ticket", "{$t}.event_id", "{$e}.*", "{$t}.available_qty", "{$t}.ticket_status")
            ->leftJoin($e, "{$t}.event_id", "=", "{$e}.id")
            ->groupBy("{$t}.event_id")
            ->where("{$t}.user_id",'=',$id)->where("{$t}.ticket_status", '<>', 'deleted')
            ->where("{$e}.datetime", ">=", date('Y-m-d H:i:s'))
            ->orderBy("{$e}.datetime", "ASC")

            ->get();
        $output = array();

        foreach($results as $result) {
            $result->earning = ($result->price - ($result->selling_commission_percentage * $result->price / 100));
            $output[] = $result;
        }
        return $output;
    }

    public static function updateTicket($product_id, $qty) {
        $ticket = RelatedTicket::where('product_id', '=', $product_id)->first();
        $ticket->available_qty = $ticket->available_qty - $qty;
        $ticket->save();
    }


}