<?php
class CountryList extends BaseModel {
    public $table ='country_list';

    public static function getCountryListWithCountryCode() {
        $results = DB::table('country_list')
            ->select( DB::raw('`Common Name` AS Country, `ISO 3166-1 2 Letter Code` As `Code`, `ITU-T Telephone Code` AS `tcode` '))
            ->whereRaw("`Type` = 'Independent State'")
            ->orderBy('Sort Order', 'ASC')
            ->get();

        return $results;
    }
}