<?php

class Stadium extends \Illuminate\Database\Eloquent\Model{
    public $table = 'stadium';
    public $timestamps = false;
    protected $fillable = ['title', 'al1', 'al2', 'postcode', 'country_id', 'svg_map', 'map_image', 'ticket_type_ids'];

}