<?php
use Illuminate\Database\Eloquent\Model;

class RoyalMailTracking extends Model{
    public $table = 'shipping_tracking';
    public $fillable = ['event_id', 'product_id', 'order_id', 'tracking_code', 'tracking_id_number'];
}