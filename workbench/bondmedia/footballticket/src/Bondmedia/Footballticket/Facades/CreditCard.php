<?php
namespace Bondmedia\Footballticket\Facades;

use DaveJamesMiller\Breadcrumbs\Exception;
use Illuminate\Support\Facades\Facade;
use Config;

class CreditCard extends Facade {

    public static function validateCardNumber($cardNumber) {
        $cardNumber = preg_replace('/\D/', '', $cardNumber);
        $len = strlen($cardNumber);
        if ($len < 15 || $len > 16) {
            throw new Exception("Invalid credit card number: must be 15 or 16 digits.");
        }
        else {
            switch($cardNumber) {
                case(preg_match ('/^4/', $cardNumber) >= 1):
                    return 'Visa';
                case(preg_match ('/^5[1-5]/', $cardNumber) >= 1):
                    return 'Mastercard';
                case(preg_match ('/^3[47]/', $cardNumber) >= 1):
                    return 'Amex';
                case(preg_match ('/^3(?:0[0-5]|[68])/', $cardNumber) >= 1):
                    return 'Diners Club';
                case(preg_match ('/^6(?:011|5)/', $cardNumber) >= 1):
                    return 'Discover';
                case(preg_match ('/^(?:2131|1800|35\d{3})/', $cardNumber) >= 1):
                    return 'JCB';
                default:
                    throw new Exception("Could not determine the credit card type.");
                    break;
            }
        }
    }

    public static function validateExpireDate($expire) {

        $expires = \DateTime::createFromFormat('m/y', $expire);
        $now     = new \DateTime();
        if ($expires < $now) {
            throw new Exception("Invalid Expire date");
        }
    }

    public static function validateCVV($cvv) {
        $len = strlen($cvv);
        if(empty($cvv) || $len < 3 ) {
            throw new Exception("Invalid CVV entered");
        }
    }
}