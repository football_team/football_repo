<?php
namespace Bondmedia\Footballticket\ShortCode;

use Bond\Abstracts\BaseShortCodeAbstract;
use Bond\Interfaces\BaseShortCodeInterface;
use View;
use DB;


class RegistrationForm extends BaseShortCodeAbstract implements BaseShortCodeInterface{
    private $url = '/ticket/registration';
    protected $params = array('reload'  => 'none');
    public function __construct() {
        $this->setTemplate("footballticket::registration");
        return $this;
    }

    /**
     * @return mixed
     */
    public function render() {
        $country_list = $users = DB::table('country_list')
            ->select(DB::raw("`ITU-T Telephone Code` AS `telephone_code`, CONCAT(`Common Name`,' ( ' ,`ITU-T Telephone Code`, ' )') AS Country"))
            ->where('ITU-T Telephone Code', '<>', '')
            ->where('Type', '=', 'Independent State')
            ->get();
        $country_with_phone = array();
        foreach($country_list as $c ) {
            $phone_code = str_replace('+', '00', $c->telephone_code);
            $country_with_phone[$phone_code] = $c->Country;
        }
        return View::make($this->getTemplate(), $this->params)
            ->with('url', $this->url)
            ->with('country_list', $country_with_phone)
            ->render();
    }
}



