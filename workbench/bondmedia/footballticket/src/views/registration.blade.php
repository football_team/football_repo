<div class="columns twelve message" style="display:none">
</div>

{{ Form::open(array('url' => $url, 'method' => 'post', 'id'=>'registration-form')) }}
    <span class="label-seperator">{{trans('homepage.pDeets')}}</span>
    <div class="columns six">
        <label>
            {{ Form::text('first_name', '', ['class'=>'input first-name', 'placeholder' => trans('homepage.fname') ]) }}
        </label>
    </div>

    <div class="columns six">
        <label>
            {{ Form::text('last_name', '', ['class'=>'input last-name', 'placeholder' => trans('homepage.sname') ]) }}
        </label>
    </div>

    <span class="label-seperator">{{trans('homepage.emailAndContact')}}</span>
    <div class="columns six">
        <label>
            {{ Form::email('email', '', ['class'=>'input email', 'placeholder' => trans('homepage.Email') ]) }}
        </label>
    </div>

    <div class="columns six">
        <label>
            {{ Form::email('email_confirmation', '', ['class'=>'input confirmEmail', 'placeholder' => trans('homepage.Confirm Email') ]) }}
        </label>
    </div>


    <div class="columns six">
        <label>
            {{ Form::select('country_code', $country_list, '0044', ['class'=>'input country-code', 'placeholder' => trans('homepage.Country Code') ] ) }}
        </label>

    </div>

    <div class="columns six">
        <label>
            {{ Form::text('contact_no', '', ['class'=>'input contact-no', 'placeholder' => trans('homepage.Mobile Number') ]) }}
        </label>
    </div>




    <div class="columns six">
        <label>
            <span class="input-field-left pull-left"> {{ Form::checkbox('newsletters', '1') }}</span>
            <span class="sub-texts pull-left"> {{trans('homepage.subForPromDis')}} </span>
        </label>

    </div>

    <div class="columns six">

    </div>

    <span class="label-seperator">Agent Referral (not required)</span>

    <div class="columns six">
        <label>
            <span class="mbtm">Agent Code</span>
            {{ Form::text('agent_code', '') }}
        </label>

    </div>

    <span class="label-seperator">{{trans('homepage.Security')}}</span>

    <div class="columns six">
        <label>
            <span class="mbtm"> {{trans('homepage.Password')}}</span>
            {{ Form::password('password', '') }}
        </label>

    </div>

    <div class="columns six">
        <label>
            <span class="mbtm">{{trans('homepage.Confirm Password')}}</span>
            {{ Form::password('password_confirmation', '') }}
        </label>
    </div>



    <div class="columns six">
        <label>
            {{ Form::checkbox('terms_n_conditions', '1') }}
            {{trans('homepage.tncAgree')}} <a href="/terms-conditions" target="_blank"> {{trans('homepage.terms and conditions')}}</a>
        </label>

    </div>

    <div class="columns six">
        {{ Form::submit(trans('homepage.Register'), ['class' => 'btn pinkbtn pull-right clearboth']) }}
    </div>




{{ Form::close() }}


{{
    Assets::setScripts(
    [
        'serializejson'          => 'js/jquery.serializejson.min.js',
        'jquery-form'            => 'js/jquery-form.min.js',
        'jquery-validator'       => 'js/jquery-validator/jquery.validate.min.js',
        'validator-add-method'   => 'js/jquery-validator/additional-methods.min.js',
        'password-strongify'     => 'js/password-strongify.js',
        'underscore'             => 'js/underscore.min.js'
    ], false, true);
}}
{{ Assets::jsStart() }}
<script>
    (function ($) {
        $.validator.setDefaults({
            success: "valid"
        });
        var form = $('#registration-form');
        var body = $("body");
        $(document).ready(function () {
            form.validate({
                rules: {
                    first_name: {
                        required: true,
                        minlength: 2,
                        maxlength:60
                    },
                    last_name: {
                        required: true,
                        minlength: 2,
                        maxlength:60
                    },
                    email: {
                        required: true,
                        email: true
                    },
                    email_confirmation: {
                        equalTo: '[name="email"]'
                    },
                    password: {
                        required: true,
                        minlength: 8,
                        maxlength: 16
                    },
                    password_confirmation: {
                        equalTo: '[name="password"]'
                    },
                    terms_n_conditions: {
                        required: true
                    },
                    contact_no: {
                        digits: true,
                        minlength: 8,
                        maxlength: 20
                    }
                },
                messages: {
                    first_name: {
                        required: "{{trans('homepage.This field is required')}}",
                        minlength: $.validator.format("{{trans('homepage.min2chars')}}")
                    },
                    last_name: {
                        required: "{{trans('homepage.This field is required')}}",
                        minlength: $.validator.format("{{trans('homepage.min2chars')}}")
                    },
                    email: {
                        required: "{{trans('homepage.This field is required')}}"
                    },
                    email_confirmation: {
                        equalTo: "{{trans('homepage.emailMismatch')}}"
                    },
                    password: {
                        required: "{{trans('homepage.This field is required')}}",
                        minlength: $.validator.format("{{trans('homepage.passMinLen')}}"),
                        maxlength: $.validator.format("{{trans('homepage.passMaxLen')}}")
                    },
                    password_confirmation: {
                        equalTo: "{{trans('homepage.passMismatch')}}"
                    },
                    terms_n_conditions: {
                        required: "{{trans('homepage.mustAcceptTerms')}}"
                    },
                    contact_no: {
                        digits: "{{trans('homepage.onlyNumeric')}}"
                    }

                }
            });

            /*
            Ajax form submission
             */
            form.ajaxForm({
                dataType:  'json',
                beforeSubmit: function (arr, $form, options) {
                    if(form.valid()) {
                        body.append('<div class="ajax-loading-modal"></div>');
                        body.addClass("loading");
                        return true;
                    } else {
                        return false;
                    }
                },
                success:   function ($response) {
                    body.removeClass("loading");
                    $('.ajax-loading-modal').remove();
                    //redirect to account page
                    $.ajax({
                        url: '/customer/account/login',
                        data:{
                            login : {
                            username: $('input[name=email]').val(),
                            password: $('input[name=password]').val()
                        }},
                        type: 'POST',
                        dataType: 'json',
                        success: function (response) {
                            body.removeClass("loading");
                            @if(isset($reload) && trim($reload) == 'self')
                                window.location.reload();
                            @else
                                    window.location.href = '/account/listing';
                            @endif

                        },
                        error: function (response) {
                            body.removeClass("loading");
                            console.log(response);
                        }
                    });


                },
                error: function ($response) {
                    body.removeClass("loading");
                    $('.ajax-loading-modal').remove();
                    $('.message').css({display: "block"});
                    var template = _.template($('#error-message-template').html());
                    var message = jQuery.parseJSON($response.responseText);
                    if(message.error) {
                        $('.message').html(template({message: message.error }));
                    } else {
                        var output = '';
                        $.each(message, function (i, val){
                            output += val.join('<br />');
                        } );

                        $('.message').html(template({message: output}));
                    }

                }
            });

            $('input[name="password"]').passStrengthify({
                minimum: 8,
                labels: {
                    tooShort: '',
                    passwordStrength: ''
                }
            });
        });
    })(jQuery)
</script>

<script type="text/x-template" id="error-message-template">
    <div data-alert class="alert-box alert radius">
        <%= message %>
        <a href="#" class="close">&times;</a>
    </div>
</script>
{{ Assets::jsEnd() }}