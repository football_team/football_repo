@extends('emails/_layout/layout')
@section('content')
<!-- body -->
<table class="body-wrap">
    <tr>
        <td></td>
        <td class="container" bgcolor="#FFFFFF">

            <!-- content -->
            <div class="content">
                <table>
                    <tr>
                        <td>
                            Dear {{$customer_name}},
                            <br />
                            Your order for <b>{{$event->title}}</b> on <b>{{$event->datetime}}</b> has been upadated.<br><br>
                            Your collection point is:<br><br>
                            {{$collection->collection_date}}<br>
                            {{$collection->location_1}}<br>
                            {{$collection->location_2}}<br>
                            {{$collection->location_3}}<br>
                            {{$collection->postcode}}<br>
                            <br>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <p>To view a list of FAQs please click <a href="https://www.footballticketpad.com/faq">here</a></p>
                        </td>
                    </tr>

                    <tr>
                        <td bgcolor="#D4D4D4" align="center" style="padding:20px">Thank you, <strong>Football Ticket Pad</strong></td>
                    </tr>
                </table>
            </div>
            <!-- /content -->

        </td>
        <td></td>
    </tr>
</table>
<!-- /body -->

@stop