@extends('emails/_layout/layout')
@section('content')
<!-- body -->
<table class="body-wrap">
    <tr>
        <td></td>
        <td class="container" bgcolor="#FFFFFF">

            <!-- content -->
            <div class="content">
                <table>
                    <tr>
                        <td>
                            Dear {{$customer_name}},
                            <br />
                            Your order : {{$order->order_id}} for {{$event->title}} on {{$event->datetime}} has been dispatched. Tracking Code: {{$tracking_code}}
                            <a href="https://www3.royalmail.com/track-your-item" target="_blank">Click here to check your item status</a>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <p>To view a list of FAQs please click <a href="https://www.footballticketpad.com/faq">here</a></p>
                        </td>
                    </tr>

                    <tr>
                        <td bgcolor="#D4D4D4" align="center" style="padding:20px">Thank you, <strong>Football Ticket Pad</strong></td>
                    </tr>
                </table>
            </div>
            <!-- /content -->

        </td>
        <td></td>
    </tr>
</table>
<!-- /body -->

@stop