@extends('emails/_layout/layout')
@section('content')
<!-- body -->
<table class="body-wrap">
    <tr>
        <td></td>
        <td class="container" bgcolor="#FFFFFF">

            <!-- content -->
            <div class="content">
                <table>
                    <tr>
                        <td>
                            <h2>Corporate Ticket Request</h2>
                            <p>Game # {{$game}}</p>
                            <p>Date # {{$date_of_match}}</p>
                            <p>&nbsp;</p>
                            <p>Name: # {{$name}}</p>
                            <p>&nbsp;</p>
                            <p>Company# {{$company}}</p>
                            <p>&nbsp;</p>
                            <p>Budget # {{$budget}}</p>
                            <p>&nbsp;</p>
                            <p>Email # {{$email}} </p>
                            <p>&nbsp;</p>
                            <p>Tel: # {{$tel}} </p>
                            <p>&nbsp;</p>
                        </td>
                    </tr>
                </table>
            </div>
            <!-- /content -->

        </td>
        <td></td>
    </tr>
</table>
<!-- /body -->

@stop