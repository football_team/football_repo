@extends('emails/_layout/layout')
@section('content')
<!-- body -->
<table class="body-wrap">
    <tr>
        <td></td>
        <td class="container" bgcolor="#FFFFFF">

            <!-- content -->
            <div class="content">
                <h2>New Contact from seller : {{$user_object->firstname}} {{$user_object->lastname}} - {{$user_object->email}}</h2>
                <p>This is an enquiry regarding the event : {{$event_object->title}} - {{$event_object->datetime}}</p>
                <table>
                    <tr>
                        <td>
                            <h2>Event ID # {{$event_id}}</h2>
                            <h2>Order Id</h2>
                            <p>{{$order_id}}</p>
                            <h2>Message</h2>
                            <p>{{$_message}}</p>
                        </td>
                    </tr>
                </table>
            </div>
            <!-- /content -->

        </td>
        <td></td>
    </tr>
</table>
<!-- /body -->

@stop