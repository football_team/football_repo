@extends('emails/_layout/layout')
@section('content')
        <!-- body -->
<table class="body-wrap">
    <tr>
        <td></td>
        <td class="container" bgcolor="#FFFFFF">
            <!-- content -->
            <div class="content">

                <a href="https://www.footballticketpad.com/"><img src="{{ Assets::Path('images/email-logo.png') }}" alt="Football Ticket Pad" /></a>

                <p><strong>The Home of Football Tickets!</strong></p>

                <h2>There is a new deferred order</h2>

                <p>We have just sold some tickets for {{$event->title}}. This order has been deferred and is available to reacquire.</p>
                <table>
                    <tr>
                        <td>
                            <p><strong>Game details:</strong><br/>
                                {{$event->title}}</p>

                            <p><strong>Order Id/ Transaction#</strong><br/>
                            <p>{{$order->order_id}}</p>

                            <p><strong>Form Of ticket</strong><br/>
                                {{$form_of_ticket}}</p>

                            <p><strong>Location</strong><br/>
                                {{$tInfo['ticketInformation']['loc_block']}} {{$tInfo['ticketInformation']['loc_row']}}</p>

                            <p>&nbsp;</p>

                            <p><strong>Qty (No. of ticket purchased):</strong> {{$order->qty}} </p>

                            <p><strong>Total Amount:</strong> £{{number_format($order->amount, 2)}} </p>

                            <p>--------------------------------</p>

                            <p>Please go to admin > tickets > deferred orders <a href="https://www.footballticketpad.com/admin/tickets/deferred-orders">here</a></p>
                        </td>
                    </tr>
                    <tr>
                        <td bgcolor="#D4D4D4" align="center" style="padding:20px">Thank you, <strong>Football Ticket Pad</strong></td>
                    </tr>
                </table>
            </div>
            <!-- /content -->

        </td>
        <td></td>
    </tr>
</table>
<!-- /body -->

@stop