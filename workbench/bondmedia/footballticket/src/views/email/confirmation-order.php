@extends('emails/_layout/layout')
@section('content')
<!-- body -->
<table class="body-wrap">
    <tr>
        <td></td>
        <td class="container" bgcolor="#FFFFFF">
			
            
        	
            
            
            <!-- content -->
            <div class="content">
            
            	<a href="https://www.footballticketpad.com/"><img src="{{ Assets::Path('images/email-logo.png') }}" alt="Football Ticket Pad" /></a>
                
                <p><strong>The Home of Football Tickets!</strong></p>
            
                <h2>Hello FAZLEEEE,</h2>

                <p>Thank you for your order from Football Ticket Pad. Once your ticket(s) have been sent you will be notfied by email. You can check the status of your order by <a href="https://www.footballticketpad.com/login">logging into your account</a>. If you have any questions about your order please contact us by email on <a href="mailto:info@footballticketpad.com">info@footballticketpad.com</a> or check out our <a href="https://www.footballticketpad.com/faq">FAQ’s</a>
Your order confirmation is below. Thank you again for your business.</p>

				
                <h3>Your Order #100000068 (placed on 22 April 2015 10:57:05 BST)</h3>
                

             <table width="100%">
                    <tr>
                        <td>
                        	<h4 style="background:rgba(224,224,224,1.00); padding:10px">  Billing Information: </h4>
                            <p>
                            Fazle Elahee <br/>
                            Bondmedia<br/>
                            43 Plashet Road<br/>
                            London, E13 0qa<br/>
                            United Kingdom<br/>
                            T: 09877778877 <br/>
                            F: 0 <br/>
                            VAT: 123456<br/>
                            
                            </p>
                            
                        </td>
                        
                        
                         <td>
                        	<h4 style="background:rgba(224,224,224,1.00); padding:10px"> Payment Method: </h4>
                            <p>
                            Credit Card
                            
                            </p>
                            
                        </td>
                        
                        
                        
                        <td>
                        	<h4 style="background:rgba(224,224,224,1.00); padding:10px"> Shipping Information: </h4>
                            <p>
                           Fazle Elahee<br/>
                            43 Plashet Road<br/>
                            London, e13 0qa<br/>
                            United Kingdom<br/>
                            T: 09877778877 <br/>
                            F: 0 <br/>
                            
                            </p>
                            
                        </td>
                        
                        
                        <td>
                        	<h4 style="background:rgba(224,224,224,1.00); padding:10px"> Shipping Method: </h4>
                            <p>
                            UK - Mon – Fri Special Free - Fixed
                            </p>
                            
                        </td>
                        
                        
                        
                    </tr>
                    
                </table>
                
                
                <table width="100%">
                
                	<tr>
                    	
                        <th bgcolor="#F4F4F4">Item</th>
                    	<th bgcolor="#F4F4F4">Sku</th>
                        <th bgcolor="#F4F4F4">Qty</th>
                        <th bgcolor="#F4F4F4"  width="200">Subtotal</th>
                    </tr>
                    
                    
                    <tr>
                    	
                        <td><b>Arsenal v Chelsea</b></td>
                        <td>-198-16-23- A- 1</td>
                        <td>3</td>
                        <td width="200">£3.00</td>
                    </tr>
                    
                </table>
                
                
                <div style="text-align:right;">
                	
                    <table >
                    	<tr>
                        	<td><b>Subtotal</b></td>
                            <td width="200">£3.00</td>
                        </tr>
                        <tr>
                        	<td><b>Shipping & Handling</b></td>
                           <td width="200">£0.00</td>
                        </tr>
                        <tr>
                        	<td><b>Additional Fees</b></td>
                            <td width="200">£0.30</td>
                        </tr>
                        <tr>
                        	<td><b>Grand Total</b></td>
                          <td width="200"><b>£3.00</b></td>
                        </tr>
                    </table>
                
                </div>
                
                
                
                
                <div style="text-align: center; padding:20px; background:rgba(216,216,216,1.00)"><strong>Thank you again, Football Ticket Pad</strong></div>
                
                
            </div>
            <!-- /content -->

        </td>
        <td></td>
    </tr>
</table>
<!-- /body -->

@stop