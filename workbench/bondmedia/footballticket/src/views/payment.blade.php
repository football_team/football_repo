@extends(Template::name('frontend.%s._layout.layout'))
@section('style')
	@parent
	@include('frontend.bm2014.page_checkout.style')
	@include('frontend.bm2014.page_checkout.form.pageFour.thankyouOnlyStyling')
@stop
@section('script')

	@parent
	@include('frontend.bm2014.page_checkout.js.includes')
	@include('frontend.bm2014.page_checkout.js.clock')
	@include('frontend.bm2014.page_checkout.js.validation')
	@include('frontend.bm2014.page_checkout.js.submitform')
	@include('frontend.bm2014.page_checkout.js.thankyou')
@stop

@section('content')
	<script>
		!function(){"use strict";var t=document,e=function(){var e,r,o;if(t.getElementsByClassName)e=t.getElementsByClassName("trustpilot-widget");else if(t.querySelectorAll)e=t.querySelectorAll(".trustpilot-widget");else{var s=[],a=new RegExp("(^| )trustpilot-widget( |$)"),l=t.body.getElementsByTagName("*");for(r=0,o=l.length;r<o;r++)a.test(l[r].className)&&s.push(l[r]);e=s}for(r=0;r<e.length;++r)for(var i=e[r];i.firstChild;)i.removeChild(i.firstChild)};e(),t.addEventListener?t.addEventListener("DOMContentLoaded",e):t.attachEvent("onreadystatechange",function(){"complete"==t.readyState&&e()});var r="https:"==t.location.protocol?"https:":"http:",o=r+"//widget.trustpilot.com",s=t.createElement("script");s.src=o+"/bootstrap/v5/tp.widget.bootstrap.min.js",s.async=!0,s.defer=!0,t.querySelector("head").appendChild(s)}();
		//# sourceMappingURL=tp.widget.sync.bootstrap.min.js.map
	</script>
	<div style="width: 100%;text-align: left;" >
		<a href="/" >
			<img src="/footballticketpadlogo-whitebackground.png" style="margin: 10px;" >
		</a >
	</div >
	<div class="" >
		<div class="container" >
			<div class="col-md-7" style="" >
				<div class="checkoutarea hidden-xs" >
					@include('frontend.bm2014.SlidingCheckout.partials.breadcrumbs')
				</div >
				@if(Options::getOption('payment_gateway') == 'tp')
					<iframe style="overflow-y: hidden;" height="600" scrolling="no" seamless="seamless" width="100%" src="{{Config::get('transactpro.gatewayUrl')}}{{$tid}}" frameborder="0" allowfullscreen id="aframe"></iframe >
				@elseif(Options::getOption('payment_gateway') == 'g2s' && isset($redirect_link))
					<iframe style="overflow-y: hidden;" height="1450" scrolling="no" seamless="seamless" width="100%" src="{{$redirect_link}}" frameborder="0" allowfullscreen id="aframe"></iframe >
				@endif
			</div >
			<div class="col-md-4 col-xs-12" style="padding-left: 20px;" >
				<div class="framed hidden-xs" style="
    float: left;" >
					<div class="trustpilot-widget" data-locale="en-GB" data-template-id="539ad60defb9600b94d7df2c" data-businessunit-id="54f5f5ad0000ff00057dce30" data-style-height="500px" data-style-width="100%" data-tags="checkout" data-stars="4,5">
						<a href="https://uk.trustpilot.com/review/footballticketpad.com" target="_blank">Trustpilot</a>
					</div>
				</div >
			</div >
		</div >
	</div >
@stop

