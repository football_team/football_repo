@if(count($ticketsHome) > 0)
    <table class="responsive">
        <thead>
        <tr class="hidden-xs"><th>{{trans('homepage.Game')}}</th>
            <th class="hidden-xs">Date</th>
            <th class="hidden-xs">{{trans('homepage.PriceFrom')}}</th>
            <th >&nbsp;</th>
        </tr></thead>

        <tbody>
        @foreach($ticketsHome as $ticket)
            <tr @if(!empty($ticket->tag_feat) && $ticket->tag_feat) class="click-row featuredrow" @else class="click-row" @endif data-href="{{ FootBallEvent::getUrl($ticket) }}">
                <td>
                    <span class="game" style="width:100% !important;">
                        <div class="" style=" float: left;  padding-top: 0;font-size: 16px; width: 100% !important; ">
                            @if(!empty($ticket->tag_feat) && $ticket->tag_feat)   <p style="
    padding: 0;
    margin: 0;
    color: #e13d7f;
    font-weight: 700;
">{{trans('homepage.featured')}}</p> @endif
                            <p>{{$ticket->title}}</p>
                            <div class="dayMob visible-xs">@if(!$ticket->is_tbc) {{date('d/m/y', strtotime($ticket->datetime))}} @else TBC @endif</div>
                            <div class="tt">{{$ticket->tourney_title}}</div>
                            <span class="red visible-xs">   {{{isset($ticket->price) ? trans("homepage.from")." ".trans("homepage.currencyInUse") . number_format($ticket->price, 2).' '.trans('homepage.currencyAfter') : ''}}}</span>
                        </div>
                    </span>
                </td>
                <td class="hidden-xs">
                    <div class="dayMob">@if(!$ticket->is_tbc) {{date('d/m/y', strtotime($ticket->datetime))}} @else TBC @endif</div>
                </td>
                <td class="hidden-xs">
                    {{{isset($ticket->price) ? trans("homepage.currencyInUse") . number_format($ticket->price, 2).' '.trans('homepage.currencyAfter') : ''}}}
                </td>
                <td>
                    <a class="hidden-xs btn pinkbtn squarebutton" href="{{ FootBallEvent::getUrl($ticket) }}" class="btn pinkbtn"> {{trans('homepage.BUY')}} </a>
                    <a class="visible-xs btn pinkbtn squarebutton" href="{{ FootBallEvent::getUrl($ticket) }}" class="">{{trans('homepage.BUY')}} </a>


                    <!-- SELLING FAST -->
                    @if(!empty($ticket->tag_sellingfast) && $ticket->tag_sellingfast)
                        <div class="sellingfast">{{trans('homepage.sellingfast')}}
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1"
                                 width="16" height="16" viewBox="0 0 16 16" style="fill: #57cadd;">
                                <path fill="#57cadd"
                                      d="M4.9 15.8c0 0-3.9-0.4-3.9-5.7 0-4.1 3.1-6.5 3.1-6.5s1.3 1.4 2.3 1.9c1 0.6 1.4-5.5 1.4-5.5s7.2 3.9 7.2 9.8c0 6.1-4 5.9-4 5.9s1.8-2.4 1.8-5.2c0-3-3.9-6.7-3.9-6.7s-0.5 4.4-2.1 5c-1.6-0.9-2.5-2.3-2.5-2.3s-3.7 5.8 0.6 9.3z"
                                      style="fill:#57cadd;"></path>
                                <path fill="#57cadd"
                                      d="M8.2 16.1c-2-0.1-3.7-1.4-3.7-3.2s0.7-2.6 0.7-2.6 0.5 1 1.1 1.5 1.8 0.8 2.4 0.1c0.6-0.6 0.8-2.3 0.8-2.3s1.4 1.1 1.2 3c-0.1 2-0.9 3.5-2.5 3.5z"
                                      style="fill: #57cadd;"></path>
                            </svg>
                        </div>
                        @endif

                                <!-- STACK PICKS -->
                        @if(!empty($ticket->tag_topgame) && $ticket->tag_topgame)
                            <div class="staffPick">{{trans('homepage.staffpick')}}
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1"
                                     id="Capa_1" x="0px" y="0px" viewBox="0 0 47.94 47.94"
                                     style="enable-background:new 0 0 47.94 47.94; width:17px;" xml:space="preserve">
                        <path style="fill:#72bd5f;"
                              d="M26.285,2.486l5.407,10.956c0.376,0.762,1.103,1.29,1.944,1.412l12.091,1.757  c2.118,0.308,2.963,2.91,1.431,4.403l-8.749,8.528c-0.608,0.593-0.886,1.448-0.742,2.285l2.065,12.042  c0.362,2.109-1.852,3.717-3.746,2.722l-10.814-5.685c-0.752-0.395-1.651-0.395-2.403,0l-10.814,5.685  c-1.894,0.996-4.108-0.613-3.746-2.722l2.065-12.042c0.144-0.837-0.134-1.692-0.742-2.285l-8.749-8.528  c-1.532-1.494-0.687-4.096,1.431-4.403l12.091-1.757c0.841-0.122,1.568-0.65,1.944-1.412l5.407-10.956  C22.602,0.567,25.338,0.567,26.285,2.486z"/>
                        </svg>
                            </div>
                        @endif
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endif