@if(count($tickets) > 0)
<table class="responsive">
    <thead>
    <tr><th>{{trans('homepage.Game')}}</th>
        <th>{{trans('homepage.Date')}}</th>
        <th>{{trans('homepage.PriceFrom')}}:</th>
        <th>&nbsp;</th>
    </tr></thead>

    <tbody>
        @foreach($tickets as $ticket)
        <tr>
            <td>
            	<span class="game">
            		{{$ticket->title}}
            	</span>
            </td>
            <td>
                {{date('d-m-y <\b\r> ', strtotime($ticket->datetime))}}
            </td>
            <td>
                {{{isset($ticket->price) ? trans("homepage.currencyInUse") . number_format($ticket->price, 2).' '.trans('homepage.currencyAfter') : '--'}}}
            </td>
            <td>
                <a href="{{ FootBallEvent::getUrl($ticket) }}" class="btn pinkbtn"> {{trans('homepage.BUY')}} </a>
                <a href="{{ '/ticket/sell/'.$ticket->id }}" class="btn bluebtn"> {{trans('homepage.SELL')}} </a>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endif