<h2>{{trans('homepage.Address')}}</h2>
<p class="ticket-information">
</p>

<p>
    <form action="/account/orders/update-delivery-address/{{$orderId}}" method="post">
        <input type="hidden" name="_token" value="{{csrf_token()}}"/>
        {{trans('homepage.fname')}}: <input name="fname" type="text" value="{{$firstname}}"/><br>
        {{trans('homepage.accSname')}}: <input name="sname" type="text" value="{{$lastname}}"/><br>
        {{trans('homepage.Address')}}: <input name="street" type="text" value="{{$street}}"/><br>
        {{trans('homepage.City')}}: <input name="city" type="text" value="{{$city}}"/><br>
        {{trans('homepage.Postcode')}}: <input name="postcode" type="text" value="{{$postcode}}"/><br>
        {{trans('homepage.Country')}}: <input name="country" type="text" value="{{$country_id}}"/><br>
        <input type="submit" value="{{trans('homepage.Save')}}" class="pink-btn small" style="float:right;">
    </form>
</p>
<a class="close-reveal-modal">&#215;</a>