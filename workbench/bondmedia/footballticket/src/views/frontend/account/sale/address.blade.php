<h2>{{trans('homepage.Address')}}</h2>
<p class="ticket-information">
</p>

<p>
    <strong>{{$firstname}} {{$lastname}}</strong>
    <br />
    {{$street}}
    <br />
    {{$city}}
    <br />
    {{$postcode}}
    <br />
    {{$country_id}}
    <br />
    {{{$tel or ''}}}
</p>
<p>
{{trans('homepage.Shipping Method')}}: {{$method}}
</p>
<a class="close-reveal-modal">&#215;</a>