<h2>Upload e-tickets for order: {{$order_id}}</h2>
{{ Form::open(array('url'=>'/account/sales/upload/'.$order_id,'files'=>true, 'class'=>'uploadForm', 'data-id'=>$order_id )) }}
<table>
	@foreach($tickets as $ticket)
	<tr>
		<td><a href="/account/download-ticket/{{$ticket->id}}" target="_blank">Download</a></td>
		<td>{{$ticket->created}}</td>
		<td>@if($ticket->available) {{"Released"}} @else {{"Verifying"}} @endif</td>
		<td><a class="deleteUpload" data-id = "{{$ticket->id}}"> Delete </a> </td>	
	</tr>
	@endforeach
	<tr>
		<td>{{ Form::label('file','File',array('id'=>'','class'=>'')) }}</td>
		<td>{{ Form::file('files[]', array('multiple'=>true)); }}</td>
		<td>{{ Form::submit('Save') }}</td>
		<td>{{ Form::reset('Reset') }}</td>
	</tr>
</table>
{{ Form::close() }}
