<h3>Update Collection Point For: {{$order_id}}</h3>
<p>All Fields Required</p>
<form id = "updCollectionForm" data-id="{{$order_id}}">
<table style="width:100%;">
	<tr>
		<td>Time and Date</td>
		<td><input @if(($point->collection_date)&&($point->collection_date != "")) {{'disabled = ""'}} @endif value="{{$point->collection_date}}" type="text" id="datetimepicker" name="collection_date"></td>
	</tr>
	<tr>
		<td>Your name</td>
		<td><input @if(($point->location_1)&&($point->location_1 != "")) {{'disabled = ""'}} @endif value="{{$point->location_1}}" name = "location_1" type="text"></td>
	</tr>
	<tr>
		<td>Your Contact Number</td>
		<td><input @if(($point->location_2)&&($point->location_2 != "")) {{'disabled = ""'}} @endif value="{{$point->location_2}}" name="location_2" type="text"></td>
	</tr>
	<tr>
		<td>Street address / description</td>
		<td><input @if(($point->location_3)&&($point->location_3 != "")) {{'disabled = ""'}} @endif value="{{$point->location_3}}" name="location_3" type="text"></td>
	</tr>
	<tr>
		<td>Postcode</td>
		<td><input @if(($point->postcode)&&($point->postcode != "")) {{'disabled = ""'}} @endif value="{{$point->postcode}}" name="postcode" type="text"></td>
	</tr>
	@if(!$point->postcode)
	<tr>
		<td></td>
		<td><input type="submit" value="save"></td>
	</tr>
	@endif
</table>
</form>

<script>
	$("#datetimepicker").datetimepicker();
</script>