<h2>Download e-tickets for order: {{$order_id}}</h2>
<table style="width:100%">
	@foreach($tickets as $ticket)
	<tr>
		<td><a href="/account/download-ticket/{{$ticket->id}}" target="_blank">Download</a></td>
		<td>Uploaded on: {{$ticket->created}}</td>
	</tr>
	@endforeach
</table>
