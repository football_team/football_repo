<h3>Collection Point For: {{$order_id}}</h3>
<table style="width:100%;">
	<tr>
		<td>Time and Date</td>
		<td>{{$point->collection_date}}</td>
	</tr>
	<tr>
		<td>Broker Name</td>
		<td>{{$point->location_1}}</td>
	</tr>
	<tr>
		<td>Broker Contact Number</td>
		<td>{{$point->location_2}}</td>
	</tr>
	<tr>
		<td>Address</td>
		<td>{{$point->location_3}}</td>
	</tr>
	<tr>
		<td>Postcode</td>
		<td>{{$point->postcode}}</td>
	</tr>
</table>