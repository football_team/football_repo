@extends('backend/_layout/layout')
{{
    Assets::setScripts([
        'jquery.dataTables'         => 'js/plugin/datatables/jquery.dataTables.min.js',
        'dataTables.colVis'         => 'js/plugin/datatables/dataTables.colVis.min.js',
        'dataTables.tableTools'     => 'js/plugin/datatables/dataTables.tableTools.min.js',
        'dataTables.bootstrap'      => 'js/plugin/datatables/dataTables.bootstrap.min.js',
    ], true);
}}

@section('content')

    <div class="row">
        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
            <h1 class="page-title txt-color-blueDark">
                <i class="fa fa-table fa-fw "></i>
                Stadiums
            </h1>
        </div>
    </div>
    {{ Notification::showAll() }}
    <section id="widget-grid" class="">
        <div class="row">
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false">
                    <header>
                        <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                        <h2>Available Stadiums</h2>
                    </header>
                    <div>

                        <div class="jarviswidget-editbox">
                        </div>
                        <div class="widget-body no-padding">

                            <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                                <thead>
                                <tr>
                                    <th><i class="fa fa-fw  text-muted hidden-md hidden-sm hidden-xs"></i> ID</th>
                                    <th><i class="fa fa-fw  text-muted hidden-md hidden-sm hidden-xs"></i> Title </th>
                                    <th><i class="fa fa-fw  text-muted hidden-md hidden-sm hidden-xs"></i>Country</th>
                                    <th><i class="fa fa-fw  txt-color-blue hidden-md hidden-sm hidden-xs"></i>Edit</th>
                                    <th><i class="fa fa-fw  txt-color-blue hidden-md hidden-sm hidden-xs"></i>Delete</th>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </article>
        </div>
    </section>
@stop

@section ('script')
    <script>
        $(document).ready(function() {
            $('#dt_basic').dataTable({
                serverSide: true,
                ajax: {
                    url: "/admin/stadiums/list.json",
                    type: 'POST'
                },
                order: [[0, 'DESC']]
            });

            $('body').on('click', '.del-btn', function(){
                if(confirm("Are you sure you want to delete this stadium?"))
                {
                    $.ajax({
                        url: $(this).data('url'),
                        type:"POST",
                        success:function(response){
                            var table = $('#dt_basic').DataTable();
                            table.ajax.reload();
                        },
                        error:function(response){
                            alert('An error occurred');
                        }
                    })
                }
            });
        })


    </script>
@stop
