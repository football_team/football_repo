@extends('backend/_layout/layout')
@section('breadcrumb')
    <li>Home</li><li>Agents</li>
@stop

{{
    Assets::setScripts([
        'jquery.dataTables'         => 'js/plugin/datatables/jquery.dataTables.min.js',
        'dataTables.colVis'         => 'js/plugin/datatables/dataTables.colVis.min.js',
        'dataTables.tableTools'     => 'js/plugin/datatables/dataTables.tableTools.min.js',
        'dataTables.bootstrap'      => 'js/plugin/datatables/dataTables.bootstrap.min.js',
        'bootstrap-datepicker'      => 'js/plugin/bootstrap-datepicker/bootstrap-datepicker.js',
        'bootstrap-datepicker.tr'   => 'js/plugin/bootstrap-datepicker/locales/bootstrap-datepicker.tr.js',
    ], true);
}}

{{  Assets::setStyles([
    'datepicker'          => 'js/plugin/bootstrap-datepicker/datepicker.css'
], true) }}

@section('content')
    <!-- widget grid -->
    <section id="widget-grid" class="">
        <div class="row">
            <div class="col-xs-12" id="main-gooey">
                <table id="agent-table" class="table">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>First Name</th>
                        <th>Second Name</th>
                        <th>Agent Share Code</th>
                        <th>Discount %</th>
                        <th>Agent %</th>
                        <th>Status</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
        <br><br>
        <h3>Add new agent</h3>
        <div class="row">
            <div class="col-xs-12">
                <table id="addAgent" class="table">
                    <thead>
                    <tr>
                        <th>First Name</th>
                        <th>Second Name</th>
                        <th>Agent Share Code</th>
                        <th>Discount %</th>
                        <th>Agent %</th>
                        <th>Save</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td><input type="text" id="add-fname"></td>
                        <td><input type="text" id="add-lname"></td>
                        <td><input type="text" id="add-code"></td>
                        <td><input type="text" id="add-discount"></td>
                        <td><input type="text" id="add-agent-perc"></td>
                        <td><div class="btn btn-success" id="agent-new">Save</div></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
@stop

@section ('script')

    <script>
        $('#agent-table').dataTable({
            serverSide: true,
            bFilter: false, bInfo: false,
            ajax: {
                url: "/{{ Config::get('bondcms.admin_prefix') }}/agents/agentDT",
                type: 'POST'
            },
            order: [[0, 'DESC']]
        });

        $('#agent-new').click(function(){
            var fname = $('#add-fname').val();
            var lname = $('#add-lname').val();
            var code = $('#add-code').val();
            var discount = $('#add-discount').val();
            var agent_perc = $('#add-agent-perc').val();

            $.ajax({
               url:'/admin/agents/add-agent',
                type:"POST",
                data:{
                    fname:fname,
                    lname:lname,
                    agent_code:code,
                    discount_perc:discount,
                    agent_perc:agent_perc
                },
                success:function(response)
                {
                    var table = $('#agent-table').DataTable();
                    table.ajax.reload();
                },
                error:function(response)
                {
                    alert(response);
                }
            });
        });

        $('body').on('click','.agent-save',function(){
           var id = $(this).data('id');
            var fname = $('#fname-edit-'+id).val();
            var lname = $('#lname-edit-'+id).val();
            var code = $('#code-edit-'+id).val();
            var discount = $('#discount-edit-'+id).val();
            var agent_perc = $('#agent-perc-edit-'+id).val();
            var status = $('#status-edit-'+id).val();

            $.ajax({
                url:'/admin/agents/update-agent/'+id,
                type:"POST",
                data:{
                    fname:fname,
                    lname:lname,
                    agent_code:code,
                    discount_perc:discount,
                    status:status,
                    agent_perc:agent_perc,
                },
                success:function(response)
                {
                    var table = $('#agent-table').DataTable();
                    table.ajax.reload();
                },
                error:function(response)
                {
                    alert(response);
                }
            });
        });
    </script>
@stop


