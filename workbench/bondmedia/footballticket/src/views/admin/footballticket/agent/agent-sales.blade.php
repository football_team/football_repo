@extends('backend/_layout/layout')
@section('breadcrumb')
    <li>Home</li><li>Agents</li>
    @stop

    {{
        Assets::setScripts([
            'jquery.dataTables'         => 'js/plugin/datatables/jquery.dataTables.min.js',
            'dataTables.colVis'         => 'js/plugin/datatables/dataTables.colVis.min.js',
            'dataTables.tableTools'     => 'js/plugin/datatables/dataTables.tableTools.min.js',
            'dataTables.bootstrap'      => 'js/plugin/datatables/dataTables.bootstrap.min.js',
            'bootstrap-datepicker'      => 'js/plugin/bootstrap-datepicker/bootstrap-datepicker.js',
            'bootstrap-datepicker.tr'   => 'js/plugin/bootstrap-datepicker/locales/bootstrap-datepicker.tr.js',
        ], true);
    }}

    {{  Assets::setStyles([
        'datepicker'          => 'js/plugin/bootstrap-datepicker/datepicker.css'
    ], true) }}

    @section('content')
            <!-- widget grid -->
    <section id="widget-grid" class="">
        <div class="row">
            <div class="col-xs-12" id="main-gooey">
                <table id="total-sales" class="table">
                    <thead>
                    <tr>
                        <th>Sales at</th>
                        <th>Total sale value at</th>
                        <th>Agent earning at</th>
                        <th>Agent liability at</th>
                        <th>Sales tm</th>
                        <th>Total sale value tm</th>
                        <th>Total agent earning tm</th>
                        <th>Agent liability tm</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12" id="main-gooey">
                <table id="indiv-sales" class="table">
                    <thead>
                    <tr>
                        <th>Agent ID</th>
                        <th>Agent name</th>
                        <th>Sales tm</th>
                        <th>Total sale value tm</th>
                        <th>Agent liability tm</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
    </section>
@stop

@section ('script')

    <script>
        $('#total-sales').dataTable({
            serverSide: true,
            bFilter: false, bInfo: false,
            ajax: {
                url: "/{{ Config::get('bondcms.admin_prefix') }}/agents/sales/totalDT",
                type: 'POST'
            },
            order: [[0, 'DESC']]
        });

        /*$('#indiv-sales').dataTable({
            serverSide: true,
            bFilter: false, bInfo: false,
            ajax: {
                url: "/{{ Config::get('bondcms.admin_prefix') }}/agents/sales/indivDT",
                type: 'POST'
            },
            order: [[0, 'DESC']]
        });
        */
    </script>
@stop


