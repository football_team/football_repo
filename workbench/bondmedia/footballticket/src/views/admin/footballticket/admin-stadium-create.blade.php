@extends('backend/_layout/layout')
@section('content')

    {{  Assets::setStyles([
        'bootstrap-tagsinput'       => 'js/plugin/bootstrap-tags/bootstrap-tagsinput.css',
        'jquery-datetime'           => 'js/plugin/jquery-datetime/jquery.datetimepicker.css',
    ], true) }}

    {{
    Assets::setScripts([
        'bootstrap-tagsinput'       => 'js/plugin/bootstrap-tags/bootstrap-tagsinput.min.js',
        'slug'                      => 'js/plugin/jquery-slug/jquery.slug.js',
        'jquery-datetime'           => 'js/plugin/jquery-datetime/jquery.datetimepicker.js',
        'underscore'                => 'js/libs/underscore.min.js'
        ], true);
    }}


    <div class="page-header">
        <h3>
            Create Stadium
        </h3>
    </div>
    {{ Form::open( array( 'url' => '/admin/stadiums/save', 'method' => 'POST')) }}

            <!-- Title -->
    <div class="control-group {{ $errors->has('title') ? 'has-error' : '' }}">
        <label class="control-label" for="title">Title</label>

        <div class="controls">
            {{ Form::text('title', '', array('class'=>'form-control', 'id' => 'title', 'placeholder'=>'Title')) }}
            @if ($errors->first('title'))
                <span class="help-block">{{ $errors->first('title') }}</span>
            @endif
        </div>
    </div>
    <br>

    <!-- Address Line 1 -->
    <div class="control-group {{ $errors->has('al1') ? 'has-error' : '' }}">
        <label class="control-label" for="title">Address Line 1</label>

        <div class="controls">
            {{ Form::text('al1', '',array('class'=>'form-control slug', 'id' => 'al1', 'placeholder'=>'Address Line 1')) }}
            @if ($errors->first('al1'))
                <span class="help-block">{{ $errors->first('al1') }}</span>
            @endif
        </div>
    </div>
    <br>

    <div class="control-group {{ $errors->has('al2') ? 'has-error' : '' }}">
        <label class="control-label" for="title">Address Line 2</label>

        <div class="controls">
            {{ Form::text('al2', '',array('class'=>'form-control slug', 'id' => 'al2', 'placeholder'=>'Address Line 2')) }}
            @if ($errors->first('al1'))
                <span class="help-block">{{ $errors->first('al1') }}</span>
            @endif
        </div>
    </div>
    <br>

    <div class="control-group {{ $errors->has('postcode') ? 'has-error' : '' }}">
        <label class="control-label" for="title">Postcode</label>

        <div class="controls">
            {{ Form::text('postcode', '',array('class'=>'form-control slug', 'id' => 'postcode', 'placeholder'=>'Postcode')) }}
            @if ($errors->first('postcode'))
                <span class="help-block">{{ $errors->first('postcode') }}</span>
            @endif
        </div>
    </div>
    <br>

    <div style="" class="control-group {{ $errors->has('country') ? 'has-error' : '' }}">
        <label class="control-label" for="title">Country</label>

        <div class="controls">
            <select name="country_id" id="country">
                @foreach($countries as $country)
                    <option value="{{ $country[0] }}">{{ $country[1] }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <br>

    <div style="margin-top:50px;" class="control-group {{ $errors->has('svgmap') ? 'has-error' : '' }}" >
        <h3 class="control-label" for="title" >SVG MAPS</h3 >
        <div class="controls" >
            <select class="form-control" name="svgmap" id="svgmap" >
                <option value=""></option>
                @foreach($listOfMaps as $item)
                    <option value="{{$item->id}}">{{$item->name}}</option >
                @endforeach
            </select >
            @if ($errors->first('svgmap'))
                <span class="help-block" >{{ $errors->first('svgmap') }}</span >
            @endif
        </div >
    </div >

    <br><br><br>

    <div class="control-group {{ $errors->has('venue_image') ? 'has-error' : '' }}">
        <label class="control-label" for="title">Map Image</label>
        <div class="controls venue-image-container">

            <input name="map_image" value="" type="hidden" id="venue_image"/>

            {{ Form::button('Map Image',  array('class'=>'btn btn-info', 'id'=>'venue_image_btn')) }}
            {{ Form::button('Remove',  array('class'=>'btn btn-info', 'id'=>'remove_venue_image_btn', 'style'=>"display: none")) }}
            @if ($errors->first('venue_image'))
                <span class="help-block">{{ $errors->first('venue_image') }}</span>
            @endif
        </div>
    </div>
    <br>


    {{ Assets::jsStart() }}
    <script type="text/javascript">
        window.AppFileManager = {};


        function SetUrl(p,w,h) {
            AppFileManager.setImage(p,w,h);
        }

        (function ($, f) {
            f.fileManagerWindow =null;
            f.elem = null;
            f.intervalId = null;
            f.type = '';
            f.openFileManager = function () {
                if ( f.fileManagerWindow !== null) {
                    alert("You already have opened window, please close that before open another one");
                    return null;
                }
                this.fileManagerWindow = window.open("/admin/filemanager/show?CKEditorFuncNum=1&langCode=en", "_blank", "toolbar=yes, scrollbars=yes, resizable=yes, top=100, left=500, width=900, height=600");
                this.intervalId = window.setInterval(this.checkwindow, 500);
            };
            f.closeFileManager = function () {

                this.fileManagerWindow.close();
                this.fileManagerWindow = null;
            };
            f.setImage= function(p,w,h) {
                this.closeFileManager();
                window.clearInterval(f.intervalId);
                if(p) {
                    var pathExtract = /^[a-z]+:\/\/\/?[^\/]+(\/[^?]*)/i;
                    var imageUrl = (pathExtract.exec(p))[1];

                    if(imageUrl && this.type == 'venue') {
                        $('.venue-image-container').find('.venue-img-preview').remove();
                        $('.venue-image-container').find('.image-preview-div').remove();
                        var image = $('<img class="venue-img-preview" src="'+imageUrl+'" style="max-width:300px; max-height:200px;" />');
                        this.elem.prepend("<hr class='image-preview-div' style='margin-top: 10px;' />");
                        this.elem.prepend(image);
                        this.elem.find('#venue_image').val(imageUrl);
                        this.elem.find('#remove_venue_image_btn').css({display: 'block'});
                    }
                }
            }

            f.checkwindow =  function () {
                if (f.fileManagerWindow  && f.fileManagerWindow  == null) {
                    window.clearInterval(f.intervalId);
                }

                try {
                    if(!f.fileManagerWindow.top) {
                        window.clearInterval(f.intervalId);
                        f.fileManagerWindow = null;
                    }
                } catch (err) {
                    window.clearInterval(f.intervalId);
                    f.fileManagerWindow = null;
                }

            }


            $.fn.fileManager = function (option) {
                f.elem = $(this).closest(option.container);
                f.openFileManager();
            }

            $.fn.removeFeatureImage = function (option) {
                var elem = $(this).closest(option.container);
                elem.find(option.img_pre).remove();
                elem.find(option.img_prev_div).remove();
                elem.find(option.feature_img).val('');
                elem.find(option.remove).css({display: 'none'});
            }
        })(jQuery, AppFileManager)

        $(function() {

            $('#venue_image_btn').click(function () {
                AppFileManager.type = 'venue';
                $(this).fileManager({
                    container: '.venue-image-container'
                });
            });

            $('#remove_venue_image_btn').click(function () {
                $(this).removeFeatureImage({
                    container: '.venue-image-container',
                    img_pre: ".venue-img-preview",
                    img_prev_div: ".image-preview-div",
                    feature_img: '#venue_image',
                    remove: '#remove_venue_image_btn'
                });
            });
        });


    </script>

    {{ Assets::jsEnd() }}

    {{ Form::submit('Update', array('class' => 'btn btn-success')) }}
    {{ Form::close() }}

@stop