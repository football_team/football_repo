<input name="type" type="hidden" value="{{$type}}" />

<div class="control-group">
    <div class="controls">
        <label class="control-label" for="country">
            &nbsp; Country</label>
        <select name="country" class="select input">
            <option value=""> - </option>
            @foreach($countries as $country)
            @if (isset($meta_country))
            <option value="{{$country->id}}" {{ $meta_country == $country->id?  'selected': '' }}>{{$country->title}}</option>
            @else
            <option value="{{$country->id}}">{{$country->title}}</option>
            @endif
            @endforeach
        </select>
    </div>
</div>
<br>

<div class="control-group">
    <div class="controls">
        <label class="control-label" for="season">
            &nbsp; Current Season</label>
        <select name="season" class="select input">
            <option value=""> - </option>
            @foreach($season as $s)
            @if (isset($meta_season))
            <option value="{{$s->id}}" {{ $meta_season == $s->id?  'selected': '' }}>{{$s->title}}</option>
            @else
            <option value="{{$s->id}}">{{$s->title}}</option>
            @endif
            @endforeach
        </select>
    </div>
</div>
<br>

<div class="control-group {{ $errors->has('club_logo') ? 'has-error' : '' }}">
    <label class="control-label" for="title">Tournament Logo</label>

    <div class="controls club-logo-container">

        @if (isset($meta_club_logo) && $meta_club_logo != '' )
        <img class="club-logo-preview" src="{{$meta_club_logo}}" style="max-width:300px; max-height:200px;" />
        <hr class='image-preview-div' style='margin-top: 10px;' />
        <input name="club_logo" value="{{$meta_club_logo}}" type="hidden" id="club_logo"/>
        @else
        <input name="club_logo" value="" type="hidden" id="club_logo"/>
        @endif

        {{ Form::button('Tournament Logo',  array('class'=>'btn btn-info', 'id'=>'club_logo_btn')) }}
        {{ Form::button('Remove',  array('class'=>'btn btn-info', 'id'=>'remove_club_logo_btn', 'style'=>"display: none")) }}
        @if ($errors->first('club_logo'))
        <span class="help-block">{{ $errors->first('club_logo') }}</span>
        @endif
    </div>
</div>
<br/>

{{ Assets::jsStart() }}
<script type="text/javascript">
    window.AppFileManager = {};


    function SetUrl(p,w,h) {
        AppFileManager.setImage(p,w,h);
    }

    (function ($, f) {
        f.fileManagerWindow =null;
        f.elem = null;
        f.intervalId = null;
        f.type = '';
        f.openFileManager = function () {
            if ( f.fileManagerWindow !== null) {
                alert("You already have opened window, p[lease close that before open another one");
                return null;
            }
            this.fileManagerWindow = window.open("/admin/filemanager/show?CKEditorFuncNum=1&langCode=en", "_blank", "toolbar=yes, scrollbars=yes, resizable=yes, top=100, left=500, width=900, height=600");
            this.intervalId = window.setInterval(this.checkwindow, 500);
        };
        f.closeFileManager = function () {

            this.fileManagerWindow.close();
            this.fileManagerWindow = null;
        };
        f.setImage= function(p,w,h) {
            this.closeFileManager();
            window.clearInterval(f.intervalId);
            if(p) {
                var pathExtract = /^[a-z]+:\/\/\/?[^\/]+(\/[^?]*)/i;
                var imageUrl = (pathExtract.exec(p))[1];
                if(imageUrl && this.type == 'feature') {
                    $('.feature-image-container').find('.feature-img-preview').remove();
                    $('.feature-image-container').find('.image-preview-div').remove();
                    var image = $('<img class="feature-img-preview" src="'+imageUrl+'" style="max-width:300px; max-height:200px;" />');
                    this.elem.prepend("<hr class='image-preview-div' style='margin-top: 10px;' />");
                    this.elem.prepend(image);
                    this.elem.find('#feature_image').val(imageUrl);
                    this.elem.find('#remove_feature_image_btn').css({display: 'block'});
                    return;
                }

                if(imageUrl && this.type == 'venue') {
                    $('.venue-image-container').find('.venue-img-preview').remove();
                    $('.venue-image-container').find('.image-preview-div').remove();
                    var image = $('<img class="venue-img-preview" src="'+imageUrl+'" style="max-width:300px; max-height:200px;" />');
                    this.elem.prepend("<hr class='image-preview-div' style='margin-top: 10px;' />");
                    this.elem.prepend(image);
                    this.elem.find('#venue_image').val(imageUrl);
                    this.elem.find('#remove_venue_image_btn').css({display: 'block'});
                    return;
                }

                if(imageUrl && this.type == 'logo') {
                    $('.club-logo-container').find('.club-logo-preview').remove();
                    $('.club-logo-container').find('.image-preview-div').remove();
                    var image = $('<img class="venue-img-preview" src="'+imageUrl+'" style="max-width:300px; max-height:200px;" />');
                    this.elem.prepend("<hr class='image-preview-div' style='margin-top: 10px;' />");
                    this.elem.prepend(image);
                    this.elem.find('#club_logo').val(imageUrl);
                    this.elem.find('#remove_club_logo_btn').css({display: 'block'});
                    return;
                }
            }
        }

        f.checkwindow =  function () {
            if (f.fileManagerWindow  && f.fileManagerWindow  == null) {
                window.clearInterval(f.intervalId);
            }

            try {
                if(!f.fileManagerWindow.top) {
                    window.clearInterval(f.intervalId);
                    f.fileManagerWindow = null;
                }
            } catch (err) {
                window.clearInterval(f.intervalId);
                f.fileManagerWindow = null;
            }

        }


        $.fn.fileManager = function (option) {
            f.elem = $(this).closest(option.container);
            f.openFileManager();
        }

        $.fn.removeFeatureImage = function (option) {
            var elem = $(this).closest(option.container);
            elem.find(option.img_pre).remove();
            elem.find(option.img_prev_div).remove();
            elem.find(option.feature_img).val('');
            elem.find(option.remove).css({display: 'none'});
        }
    })(jQuery, AppFileManager)

    $(function() {

        $('#feature_image_btn').click(function () {
            AppFileManager.type = 'feature';
            $(this).fileManager({
                container: '.feature-image-container'
            });
        });

        $('#remove_feature_image_btn').click(function () {
            $(this).removeFeatureImage({
                container: '.feature-image-container',
                img_pre: ".feature-img-preview",
                img_prev_div: ".image-preview-div",
                feature_img: '#feature_image',
                remove: '#remove_feature_image_btn'
            });
        });

        $('#venue_image_btn').click(function () {
            AppFileManager.type = 'venue';
            $(this).fileManager({
                container: '.venue-image-container'
            });
        });

        $('#remove_venue_image_btn').click(function () {
            $(this).removeFeatureImage({
                container: '.venue-image-container',
                img_pre: ".venue-img-preview",
                img_prev_div: ".image-preview-div",
                feature_img: '#venue_image',
                remove: '#remove_venue_image_btn'
            });
        });


        $('#club_logo_btn').click(function () {
            AppFileManager.type = 'logo';
            $(this).fileManager({
                container: '.club-logo-container'
            });
        });

        $('#remove_club_logo_btn').click(function () {
            $(this).removeFeatureImage({
                container: '.club-logo-container',
                img_pre: ".club-logo-preview",
                img_prev_div: ".image-preview-div",
                feature_img: '#club_logo',
                remove: '#remove_club_logo_btn'
            });
        });
    });


</script>

{{ Assets::jsEnd() }}

