@extends('backend/_layout/layout')
@section('content')

{{ HTML::style('assets/bootstrap/css/bootstrap-tagsinput.css') }}
{{ HTML::style('bootstrap_datepicker/css/datepicker.css') }}


    <div class="page-header">
        <h3>
            Update {{$type}}
        </h3>
    </div>
    <form action="{{route('admin.footballticket.update', array('id'=>$node->id)).'?action_type='.$type}}" method="post" data-id="{{$node->id}}" class="edit-form">

    <input name="action_type" type="hidden" value="{{$type}}" />
    <!-- Title -->
    <div class="control-group {{ $errors->has('title') ? 'has-error' : '' }}">
        <label class="control-label" for="title">Title</label>

        <div class="controls">
            {{ Form::text('title', $node->title, array('class'=>'form-control', 'id' => 'title', 'placeholder'=>'Title', 'value'=>Input::old('title'))) }}
            @if ($errors->first('title'))
            <span class="help-block">{{ $errors->first('title') }}</span>
            @endif
        </div>
    </div>
    <br>

    <!-- Slug -->
    <div class="control-group {{ $errors->has('slug') ? 'has-error' : '' }}">
        <label class="control-label" for="title">Slug</label>

        <div class="controls">
            <div class="input-group">
                <span class="input-group-addon">{{Config::get('app.url')}}group/{{$type}}/</span>
                {{ Form::text('slug', $node->slug, array('class'=>'form-control slug', 'id' => 'slug', 'placeholder'=>'Slug', 'value'=>Input::old('slug'))) }}
            </div>
            @if ($errors->first('slug'))
            <span class="help-block">{{ $errors->first('slug') }}</span>
            @endif
        </div>
    </div>
    <br>

    <!-- Content -->
    <div class="control-group {{ $errors->has('content') ? 'has-error' : '' }}">
        <label class="control-label" for="title">Content</label>

        <div class="controls">
            {{ Form::textarea('content', $node->content, array('class'=>'form-control', 'id' => 'content_page', 'placeholder'=>'Content', 'value'=>Input::old('content'))) }}
            @if ($errors->first('content'))
            <span class="help-block">{{ $errors->first('content') }}</span>
            @endif
        </div>
    </div>
    <br>

    <!-- Published -->
    <div class="control-group {{ $errors->has('is_published') ? 'has-error' : '' }}">

        <div class="controls">
            <label class="checkbox">{{ Form::checkbox('is_published', 'is_published',$node->is_published) }} Publish ?</label>
            @if ($errors->first('is_published'))
            <span class="help-block">{{ $errors->first('is_published') }}</span>
            @endif
        </div>
    </div>
    <br>
    <!-- Published -->
    <div class="control-group {{ $errors->has('is_international') ? 'has-error' : '' }}">

        <div class="controls">
            <label class="checkbox">{{ Form::checkbox('is_international', 'is_international',$node->is_international) }} International team?</label>
            @if ($errors->first('is_international'))
                <span class="help-block">{{ $errors->first('is_international') }}</span>
            @endif
        </div>
    </div>
    <br>

    @if(trim($type) == 'league' )
        <input name="type" type="hidden" value="{{$type}}" />

        <div class="control-group">
            <div class="controls">
                <label class="control-label" for="country">
                    &nbsp; Country</label>
                <select name="country" class="select input">
                    <option value=""> - </option>
                    @foreach($countries as $country)
                    @if (isset($meta_country))
                    <option value="{{$country->id}}" {{ $meta_country == $country->id?  'selected': '' }}>{{$country->title}}</option>
                    @else
                    <option value="{{$country->id}}">{{$country->title}}</option>
                    @endif
                    @endforeach
                </select>
            </div>
        </div>
        <br>

        <div class="control-group">
            <div class="controls">
                <label class="control-label" for="season">
                    &nbsp; Current Season</label>
                <select name="season" class="select input">
                    <option value=""> - </option>
                    @foreach($season as $s)
                    @if (isset($meta_season))
                    <option value="{{$s->id}}" {{ $meta_season == $s->id?  'selected': '' }}>{{$s->title}}</option>
                    @else
                    <option value="{{$s->id}}">{{$s->title}}</option>
                    @endif
                    @endforeach
                </select>
            </div>
        </div>
        <br>
        <div class="control-group {{ $errors->has('club_logo') ? 'has-error' : '' }}">
            <label class="control-label" for="title">Tournament Logo</label>

            <div class="controls club-logo-container">

                @if (isset($meta_club_logo) && $meta_club_logo != '' )
                <img class="club-logo-preview" src="{{$meta_club_logo}}" style="max-width:300px; max-height:200px;" />
                <hr class='image-preview-div' style='margin-top: 10px;' />
                <input name="club_logo" value="{{$meta_club_logo}}" type="hidden" id="club_logo"/>
                @else
                <input name="club_logo" value="" type="hidden" id="club_logo"/>
                @endif

                {{ Form::button('Tournament Logo',  array('class'=>'btn btn-info', 'id'=>'club_logo_btn')) }}
                {{ Form::button('Remove',  array('class'=>'btn btn-info', 'id'=>'remove_club_logo_btn', 'style'=>"display: none")) }}
                @if ($errors->first('club_logo'))
                <span class="help-block">{{ $errors->first('club_logo') }}</span>
                @endif
            </div>
        </div>
        <br/>
        <div class="control-group {{ $errors->has('feature_image') ? 'has-error' : '' }}">
            <label class="control-label" for="title">Feature Image</label>

            <div class="controls feature-image-container">

                @if (isset($node->feature_image) && $node->feature_image != '' )
                    <img class="feature-img-preview" src="{{$node->feature_image}}" style="max-width:300px; max-height:200px;" />
                    <hr class='image-preview-div' style='margin-top: 10px;' />
                    <input name="feature_image" value="{{$node->feature_image}}" type="hidden" id="feature_image"/>
                @else
                    <input name="feature_image" value="" type="hidden" id="feature_image"/>
                @endif

                {{ Form::button('Feature Image',  array('class'=>'btn btn-info', 'id'=>'feature_image_btn')) }}
                {{ Form::button('Remove',  array('class'=>'btn btn-info', 'id'=>'remove_feature_image_btn', 'style'=>"display: none")) }}
                @if ($errors->first('feature_image'))
                    <span class="help-block">{{ $errors->first('feature_image') }}</span>
                @endif
            </div>
        </div>
        <br>


    {{ Assets::jsStart() }}
    <script type="text/javascript">
        window.AppFileManager = {};


        function SetUrl(p,w,h) {
            AppFileManager.setImage(p,w,h);
        }

        (function ($, f) {
            f.fileManagerWindow =null;
            f.elem = null;
            f.intervalId = null;
            f.type = '';
            f.openFileManager = function () {
                if ( f.fileManagerWindow !== null) {
                    alert("You already have opened window, p[lease close that before open another one");
                    return null;
                }
                this.fileManagerWindow = window.open("/admin/filemanager/show?CKEditorFuncNum=1&langCode=en", "_blank", "toolbar=yes, scrollbars=yes, resizable=yes, top=100, left=500, width=900, height=600");
                this.intervalId = window.setInterval(this.checkwindow, 500);
            };
            f.closeFileManager = function () {

                this.fileManagerWindow.close();
                this.fileManagerWindow = null;
            };
            f.setImage= function(p,w,h) {
                this.closeFileManager();
                window.clearInterval(f.intervalId);
                if(p) {
                    var pathExtract = /^[a-z]+:\/\/\/?[^\/]+(\/[^?]*)/i;
                    var imageUrl = (pathExtract.exec(p))[1];
                    if(imageUrl && this.type == 'feature') {
                        $('.feature-image-container').find('.feature-img-preview').remove();
                        $('.feature-image-container').find('.image-preview-div').remove();
                        var image = $('<img class="feature-img-preview" src="'+imageUrl+'" style="max-width:300px; max-height:200px;" />');
                        this.elem.prepend("<hr class='image-preview-div' style='margin-top: 10px;' />");
                        this.elem.prepend(image);
                        this.elem.find('#feature_image').val(imageUrl);
                        this.elem.find('#remove_feature_image_btn').css({display: 'block'});
                        return;
                    }

                    if(imageUrl && this.type == 'venue') {
                        $('.venue-image-container').find('.venue-img-preview').remove();
                        $('.venue-image-container').find('.image-preview-div').remove();
                        var image = $('<img class="venue-img-preview" src="'+imageUrl+'" style="max-width:300px; max-height:200px;" />');
                        this.elem.prepend("<hr class='image-preview-div' style='margin-top: 10px;' />");
                        this.elem.prepend(image);
                        this.elem.find('#venue_image').val(imageUrl);
                        this.elem.find('#remove_venue_image_btn').css({display: 'block'});
                        return;
                    }

                    if(imageUrl && this.type == 'logo') {
                        $('.club-logo-container').find('.club-logo-preview').remove();
                        $('.club-logo-container').find('.image-preview-div').remove();
                        var image = $('<img class="venue-img-preview" src="'+imageUrl+'" style="max-width:300px; max-height:200px;" />');
                        this.elem.prepend("<hr class='image-preview-div' style='margin-top: 10px;' />");
                        this.elem.prepend(image);
                        this.elem.find('#club_logo').val(imageUrl);
                        this.elem.find('#remove_club_logo_btn').css({display: 'block'});
                        return;
                    }
                }
            }

            f.checkwindow =  function () {
                if (f.fileManagerWindow  && f.fileManagerWindow  == null) {
                    window.clearInterval(f.intervalId);
                }

                try {
                    if(!f.fileManagerWindow.top) {
                        window.clearInterval(f.intervalId);
                        f.fileManagerWindow = null;
                    }
                } catch (err) {
                    window.clearInterval(f.intervalId);
                    f.fileManagerWindow = null;
                }

            }


            $.fn.fileManager = function (option) {
                f.elem = $(this).closest(option.container);
                f.openFileManager();
            }

            $.fn.removeFeatureImage = function (option) {
                var elem = $(this).closest(option.container);
                elem.find(option.img_pre).remove();
                elem.find(option.img_prev_div).remove();
                elem.find(option.feature_img).val('');
                elem.find(option.remove).css({display: 'none'});
            }
        })(jQuery, AppFileManager)

        $(function() {

            $('#feature_image_btn').click(function () {
                AppFileManager.type = 'feature';
                $(this).fileManager({
                    container: '.feature-image-container'
                });
            });

            $('#remove_feature_image_btn').click(function () {
                $(this).removeFeatureImage({
                    container: '.feature-image-container',
                    img_pre: ".feature-img-preview",
                    img_prev_div: ".image-preview-div",
                    feature_img: '#feature_image',
                    remove: '#remove_feature_image_btn'
                });
            });

            $('#venue_image_btn').click(function () {
                AppFileManager.type = 'venue';
                $(this).fileManager({
                    container: '.venue-image-container'
                });
            });

            $('#remove_venue_image_btn').click(function () {
                $(this).removeFeatureImage({
                    container: '.venue-image-container',
                    img_pre: ".venue-img-preview",
                    img_prev_div: ".image-preview-div",
                    feature_img: '#venue_image',
                    remove: '#remove_venue_image_btn'
                });
            });


            $('#club_logo_btn').click(function () {
                AppFileManager.type = 'logo';
                $(this).fileManager({
                    container: '.club-logo-container'
                });
            });

            $('#remove_club_logo_btn').click(function () {
                $(this).removeFeatureImage({
                    container: '.club-logo-container',
                    img_pre: ".club-logo-preview",
                    img_prev_div: ".image-preview-div",
                    feature_img: '#club_logo',
                    remove: '#remove_club_logo_btn'
                });
            });
        });


    </script>

    {{ Assets::jsEnd() }}



    @else
        @include('footballticket::admin.footballticket.partials.'.$type)
    @endif


    @include('footballticket::admin.footballticket.partials.common')

    {{ Form::submit('Update', array('class' => 'btn btn-success')) }}
    {{ Form::close() }}

@stop

{{ Assets::jsStart() }}
{{ HTML::script('assets/bootstrap/js/bootstrap-tagsinput.js') }}
{{ HTML::script('assets/js/jquery.slug.js') }}
{{ HTML::script('bootstrap_datepicker/js/bootstrap-datepicker.js') }}
{{ HTML::script('bootstrap_datepicker/js/locales/bootstrap-datepicker.tr.js') }}

@include('backend.partials.ckeditor')

<script type="text/javascript">
    $(document).ready(function () {
        $("#title").slug();

        $('#datetime').datepicker({
            format: "yyyy-mm-dd",
            todayBtn: "linked",
            orientation: "top auto"
        });
    });
</script>
{{ Assets::jsEnd() }}