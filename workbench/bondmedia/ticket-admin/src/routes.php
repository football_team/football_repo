<?php

Route::group(array('prefix' => Config::get('bondcms.admin_prefix'), 'before' => array('auth.admin', 'assets_admin')), function () {
    //customer
    Route::get('customer', 'AdminCustomerController@index');
    Route::any('customer/list.json', 'AdminCustomerController@customerList');
    Route::get('customer/edit/{id}', array('as'=>'admin.customer.info.edit', 'uses'=>'AdminCustomerController@edit'))->where('id', '[0-9]+');
    Route::post('customer/update/{id}', array('before'=>'csrf','as'=> 'admin.customer.info.update', 'uses'=>'AdminCustomerController@update'))->where('id', '[0-9]+');

    Route::any('customer/get-favourites', 'AdminCustomerController@getFavouriteTeams');

    //ticket
    Route::get('tickets', 'AdminTicketsController@index');
    Route::any('tickets/list.json', 'AdminTicketsController@ticketList');
    Route::get('tickets/edit/{id}', array('as'=>'admin.tickets.info.edit', 'uses'=>'AdminTicketsController@edit'))->where('id', '[0-9]+');
    Route::post('tickets/update/{id}', array('before'=>'csrf','as'=> 'admin.ticket.info.update', 'uses'=>'AdminTicketsController@update'))->where('id', '[0-9]+');
    Route::any('tickets/full-details/{id}', array('as'=>'admin.tickets.info.full', 'uses'=>'AdminTicketsController@getFullListingDetailsById'))->where('id', '[0-9]+');

    Route::get('tickets/deferred-orders/', array('uses'=>'AdminSoldTicketsController@deferredOrders'));
    Route::post('tickets/deferred-orders/list.json', array('uses'=>'AdminSoldTicketsController@deferredOrdersList'));
    Route::post('tickets/deferred-orders/release/{order_id}', array('uses'=>'AdminSoldTicketsController@releaseDeferred'))->where('order_id','[0-9]+');
    Route::post('tickets/deferred-orders/reacquire/{order_id}', array('uses'=>'AdminSoldTicketsController@reacquireOrder'))->where('order_id','[0-9]+');

    //sold-ticket
    Route::get('sold-tickets/{id}', 'AdminSoldTicketsController@index')->where('id', '[0-9]+');
    Route::get('sold-tickets', 'AdminSoldTicketsController@index');
    Route::any('sold-tickets/list.json', 'AdminSoldTicketsController@ticketList');
    Route::get('sold-tickets/edit/{id}', array('as'=>'admin.sold-tickets.info.edit', 'uses'=>'AdminSoldTicketsController@edit'))->where('id', '[0-9]+');
    Route::post('sold-tickets/update/{id}', array('before'=>'csrf','as'=> 'admin.sold-tickets.info.update', 'uses'=>'AdminSoldTicketsController@update'))->where('id', '[0-9]+');
    Route::post('sold-tickets/update/status/', array('before'=>'csrf','as' => 'admin.sold-tickets.status.update', 'uses'=>'AdminSoldTicketsController@secureDeliveryStatusUpdate'));
    Route::post('sold-tickets/update/tracking-number/', array('before'=>'csrf','as' => 'admin.sold-tickets.status.update.tracking', 'uses'=>'AdminSoldTicketsController@saveTrackingCode'));
    Route::post('sold-tickets/update/payment-status/{id}', array('before'=>'csrf', 'as' => 'admin.sold-tickets.update.payment-status', 'uses'=>'AdminSoldTicketsController@updatePaidStatusByOrderId'))->where('id', '[0-9]+');
    Route::any('sold-tickets/full-details/{id}', array('as'=>'admin.sold-tickets.full-details', 'uses'=>'AdminSoldTicketsController@getFullOrderDetailsByOrderId'))->where('id', '[0-9]+');

    Route::get('sold-tickets/etickets', array('uses'=>'AdminSoldTicketsController@eTicketIndex'));
    Route::any('sold-tickets/etickets/list.json', array('uses'=>'AdminSoldTicketsController@getUnapprovedETickets'));
    Route::any('sold-tickets/etickets/approve-view/{id}', array('uses'=>'AdminSoldTicketsController@getAdminETicketApproveById'))->where('id', '[0-9]+');
    Route::get('sold-tickets/etickets/view/{id}', array('uses'=>'ETicketController@adminViewETicket'))->where('id', '[0-9]+');
    Route::post('sold-tickets/etickets/upload/{id}', array('uses'=>'ETicketController@adminSaveTicketsForOrderId', 'before'=>'csrf'))->where('id', '[0-9]+');
    Route::post('sold-tickets/etickets/actions', array('uses'=>'ETicketController@adminTicketActions', 'before'=>'csrf'));

    Route::get('finances/', array('as'=>'admin.finance.dashboard', 'uses'=>'AdminFinanceController@index'));
    Route::post('finances/list.json', array('as'=>'admin.finance.list', 'uses'=>'AdminFinanceController@listLiabilitiesByMonth'));
    Route::post('finances/totals.json', array('as'=>'admin.finance.totals', 'uses'=>'AdminFinanceController@getMonthsLiabilities'));
    
    Route::post('finances/seller-liabilities/json/{id}', array('as'=>'admin.finance.indiv.list', 'uses'=>'AdminFinanceController@getLiabilityReportBySellerId'))->where('id', '[0-9]+');
    Route::get('finances/seller-liabilities/{id}', array('as'=>'admin.finance.indiv', 'uses'=>'AdminFinanceController@showSellerLiabilities'))->where('id', '[0-9]+');
    Route::post('finances/seller-liabilities/totals/{id}', array('as'=>'admin.finance.indiv.total', 'uses'=>'AdminFinanceController@getLiabilityReportBySellerIdTotals'))->where('id', '[0-9]+');
    Route::post('finances/seller-liabilities/update-paid-multi', array('as'=>'admin.finance.indiv.update.multi', 'uses'=>'AdminFinanceController@updatePaidStatusMulti'));
    Route::post('finances/seller-liabilities/test', array('as'=>'admin.finance.indiv.update.multi', 'uses'=>'AdminFinanceController@test'));
    Route::post('finances/seller-liabilities/penalties/{id}', array('as'=>'admin.finance.indiv.penalties', 'uses'=>'AdminFinanceController@getPenaltiesByUserId'))->where('id', '[0-9]+');
    Route::post('finances/seller-liabilities/penalties/add/{id}', array('as'=>'admin.finance.indiv.penalties.add', 'uses'=>'AdminFinanceController@addPenaltiesByUserId'))->where('id', '[0-9]+');
    Route::post('finances/seller-liabilities/penalties/delete/{id}', array('as'=>'admin.finance.indiv.penalties.delete', 'uses'=>'AdminFinanceController@deletePenaltyByPenaltyId'))->where('id', '[0-9]+');
    
    Route::get('finances-futures/', array('as'=>'admin.finances.futures', 'uses'=>'AdminFinanceController@futures'));
    Route::post('finances-futures/main-totals.json', array('as'=>'admin.finances.futures.totals', 'uses'=>'AdminFinanceController@getFullSpeculatedFinanceTotals'));
    Route::post('finances-futures/main-indiv.json', array('as'=>'admin.finances.futures.indiv', 'uses'=>'AdminFinanceController@getFullSpeculatedFinanceUserTotals'));

    Route::get('finances-futures/indiv/{id}', array('as'=>'admin.finances.futures.indiv', 'uses'=>'AdminFinanceController@futuresIndiv'))->where('id', '[0-9]+');
    Route::post('finances-futures/indiv/{id}/main-totals.json', array('as'=>'admin.finances.futures.totals.indiv', 'uses'=>'AdminFinanceController@getFullFutureFinanceTotalsByUserID'));
    Route::post('finances-futures/indiv/{id}/main-indiv.json', array('as'=>'admin.finances.futures.indiv.full', 'uses'=>'AdminFinanceController@getFullFutureFinanceByUserID'));

    Route::get('finances/payment-history', 'AdminFinanceController@paymentHistoryIndex');
    Route::post('finances/payment-history/list', 'AdminFinanceController@paymentHistoryList');
    Route::get('finances/payment-history/indiv/{id}', 'AdminFinanceController@paymentHistoryIndiv')->where('id', '[0-9]+');

    Route::post('finances/payment-history/indiv/totals/{id}', 'AdminFinanceController@paymentHistoryIndivTotalList')->where('id', '[0-9]+');
    Route::post('finances/payment-history/indiv/sales/{id}', 'AdminFinanceController@paymentHistoryIndivOrderList')->where('id', '[0-9]+');
    Route::post('finances/payment-history/indiv/penalties/{id}', 'AdminFinanceController@paymentHistoryIndivPenaltyList')->where('id', '[0-9]+');
    Route::post('finances/payment-history/indiv/getNote/{id}', 'AdminFinanceController@paymentHistoryIndivNote')->where('id', '[0-9]+');

    Route::get('finances/payment-history/indiv/printable/{id}', 'AdminFinanceController@paymentHistoryPrintable')->where('id','[0-9]+');


    Route::get('fix', array('uses'=>'AdminSoldTicketsController@getFixForm'));
    Route::post('fix/getInfo', array('uses'=>'AdminSoldTicketsController@getAllInfoFromMess'));
    Route::post('fix/setpid', array('uses'=>'AdminSoldTicketsController@updateThePID'));

    Route::post('repSeller', array('uses'=>'AdminSoldTicketsController@replaceSeller'));
    Route::post('repFormOfTicket', array('uses'=>'AdminSoldTicketsController@replaceFormOfTicket'));

    Route::get('locales', 'AdminLocalesController@index');
    Route::post('locales/getDD', 'AdminLocalesController@getLocaleDrop');
    Route::post('locales/set-locale/{id}', 'AdminLocalesController@adminSetLocale')->where('id', '[0-9]+');
    Route::post('locales/get-locales.json', 'AdminLocalesController@getAllLocaleData');
    Route::post('locales/full-details/{id}', 'AdminLocalesController@fullLocaleDetails');
    Route::post('locales/update-locale/{id}', 'AdminLocalesController@editLocale');

    Route::any('genProductFeed', 'IndexController@generateXMLFeed');

    Route::post('buy/get-admin-buy-form/{id}', 'AdminPurchaseController@getOrderForm')->where('id', '[0-9]+');
    Route::post('buy/get-shipping/{id}', 'AdminPurchaseController@getShippingMethods')->where('id', '[0-9]+');
    Route::post('buy/admin-buy-action/{id}', 'AdminPurchaseController@makeOrder')->where('id', '[0-9]+');

    Route::get('affiliates/requests', 'AdminAffiliateController@requests');
    Route::get('affiliates/payment-requests', 'AdminAffiliateController@paymentRequests');
    Route::post('affiliates/requests/get-unnaproved', 'AdminAffiliateController@getRequests');
    Route::post('affiliates/requests/get-pending', 'AdminAffiliateController@getPending');
    Route::post('affiliates/requests/approve/{id}', array('before'=>'csrf', 'uses'=>'AdminAffiliateController@approveAffiliateRequest'))->where('id', '[0-9]+');
    Route::post('affiliates/requests/reject/{id}', array('before'=>'csrf', 'uses'=>'AdminAffiliateController@rejectAffiliateRequest'))->where('id', '[0-9]+');
    Route::post('affiliates/requests/cancel/{id}', array('before'=>'csrf', 'uses'=>'AdminAffiliateController@cancelPendingAffiliate'))->where('id', '[0-9]+');

    Route::post('affiliates/payment-requests/get', array('uses'=>'AdminAffiliateController@getPaymentRequestDataTable'));
    Route::post('affiliates/payment-requests/update-status', array('uses'=>'AdminAffiliateController@updatePaymentRequestStatus', 'before'=>'csrf'));
    Route::post('affiliates/payment-requests/totals', array('uses'=>'AdminAffiliateController@paymentRequestTotals', 'before'=>'csrf'));

    Route::get('affiliates/closed-payment-requests', array('uses'=>'AdminAffiliateController@closedAffiliatePayments'));
    Route::post('affiliates/closed-payment-requests/paid', array('uses'=>'AdminAffiliateController@paidAffiliatePayments'));
    Route::post('affiliates/closed-payment-requests/cancelled', array('uses'=>'AdminAffiliateController@cancelledAffiliatePayments'));

    Route::get('affiliates/config', array('uses'=>'AdminAffiliateController@config'));

    Route::post('affiliates/rates/list', array('uses'=>'AdminAffiliateController@getMainRateList'));
    Route::post('affiliates/rates/update-name', array('uses'=>'AdminAffiliateController@updateRateName', 'before'=>'csrf'));
    Route::post('affiliates/rates/delete', array('uses'=>'AdminAffiliateController@deleteRate', 'before'=>'csrf'));
    Route::post('affiliates/rates/full', array('uses'=>'AdminAffiliateController@getAllRateMilestones', 'before'=>'csrf'));
    Route::post('affiliates/rates/update-full', array('uses'=>'AdminAffiliateController@updateMilestone', 'before'=>'csrf'));
    Route::post('affiliates/rates/delete-full', array('uses'=>'AdminAffiliateController@deleteMilestone', 'before'=>'csrf'));
    Route::post('affiliates/rates/add-full', array('uses'=>'AdminAffiliateController@addNewMilestone', 'before'=>'csrf'));
    Route::post('affiliates/rates/add-new-rate', array('uses'=>'AdminAffiliateController@addNewRate', 'before'=>'csrf'));

    Route::post('affiliates/config/update', array('uses'=>'AdminAffiliateController@updateConfig', 'before'=>'csrf'));

    Route::get('affiliates/properties', array('uses'=>'AdminAffiliateController@affiliateProperties'));
    Route::post('affiliates/properties/datatable', array('uses'=>'AdminAffiliateController@affiliatePropertiesDataTable'));
    Route::post('affiliates/properties/update-rate', array('uses'=>'AdminAffiliateController@updateAffiliateRate', 'before'=>'csrf'));
    Route::post('affiliates/properties/update-agent/{id}', array('uses'=>'AdminAffiliateController@updateAffiliateAgent', 'before'=>'csrf'))->where('id', '[0-9]+');
    Route::post('affiliates/properties/update-show-fees/{id}', array('uses'=>'AdminAffiliateController@updateAffiliateShowFees', 'before'=>'csrf'))->where('id', '[0-9]+');

    Route::get('ticket-types', array('uses'=>'AdminTicketTypeController@index'));
    Route::post('ticket-types/delete/{id}', array('uses'=>'AdminTicketTypeController@delete', 'before'=>'csrf'))->where('id', '[0-9]+');
    Route::post('ticket-types/addNew', array('uses'=>'AdminTicketTypeController@addNew', 'before'=>'csrf'));
    Route::post('ticket-types/save-order', array('uses'=>'AdminTicketTypeController@saveOrder', 'before'=>'csrf'));

    Route::get('language-tester', 'AdminLocalesController@checkLangIndex');
    Route::post('language-tester/api-test', 'AdminLocalesController@apiTest');
    Route::post('language-tester/{locale}/{expected}/{prefix}/{key}', array('uses'=>'AdminLocalesController@checkLTextLanguages'));

    Route::get('redirector', 'AdminRedirectorController@index');
    Route::post('redirector/datatable', 'AdminRedirectorController@datatable');
    Route::post('redirector/update-rdr/{id}', array('uses'=>'AdminRedirectorController@update', 'before'=>'csrf'))->where('id', '[0-9]+');
    Route::post('redirector/delete/{id}', array('uses'=>'AdminRedirectorController@delete', 'before'=>'csrf'))->where('id', '[0-9]+');
    Route::post('redirector/add', array('uses'=>'AdminRedirectorController@add', 'before'=>'csrf'));

    Route::get('affiliates/referrals', array('uses'=>'AdminAffiliateController@referralIndex'));
    Route::post('affiliates/referrals/data', array('uses'=>'AdminAffiliateController@getReferralDataByDate'));
    Route::get('affiliates/referrals-indiv/{id}', array('uses'=>'AdminAffiliateController@referralIndivIndex'))->where('id', '[0-9]+');
    Route::post('affiliates/referrals-indiv/data/{id}', array('uses'=>'AdminAffiliateController@getReferralIndivDataByDate'))->where('id', '[0-9]+');

    Route::get('affiliates/sales', array('uses'=>'AdminAffiliateController@salesIndex'));
    Route::post('affiliates/sales/data', array('uses'=>'AdminAffiliateController@getSalesDataByDate'));
    Route::post('affiliates/sales/data-totals', array('uses'=>'AdminAffiliateController@getSalesTotalsByDate'));
    Route::get('affiliates/sales-indiv/{id}', array('uses'=>'AdminAffiliateController@salesIndivIndex'))->where('id', '[0-9]+');
    Route::post('affiliates/sales-indiv/data/{id}', array('uses'=>'AdminAffiliateController@getSalesIndivDataByDate'))->where('id', '[0-9]+');
    Route::post('affiliates/sales-indiv/data-totals/{id}', array('uses'=>'AdminAffiliateController@getSalesTotalsIndivDataByDate'))->where('id', '[0-9]+');

    Route::post('affiliates/skiptAfilliateVerification/{id}',array('uses'=>'AffiliateRegistrationController@skipVerifyDomain','before'=>'csrf'))->where('id', '[0-9]+');
    Route::any('hiddentools/af_rate_test/{id}', 'AffiliateAccountController@getRate');
});

Route::get('/forgot-password', array('as'=>'customer.password.reset','uses'=>'AccountController@passwordResetRequest'));
Route::post('/forgot-password', array('as'=>'customer.password.reset','uses'=>'AccountController@passwordResetRequest'), array('before' => 'csrf'));



