<?php
use Illuminate\Database\Eloquent\Model;
class WebUser extends Model{
    public $table = 'web_user';
    public $fillable = ['user_id', 'firstname', 'lastname','email', 'telephone', 'seller_type', 'updated_by', 'status', 'newsletter', 'ref_agent'];

    public function getList() {
        return $this->select('*')->get();
    }

    public static function validateSellerAllowedPublishTime($userId, $hour) {
        $user = WebUser::where('user_id', '=',$userId )->first();

        if(!$user) {
            $user = new WebUser();
            $user->user_id = $userId;
            $user->seller_type = 'customer';
            $user->save();
        }

        $allowedHour = (int) Options::getOption('basic_seller_publish_time');
        $allowedHour = $allowedHour == 0 ? 36: $allowedHour;

        if(($user->seller_type == 'customer' || $user->seller_type == 'seller') && $hour >= $allowedHour) {
            return true;
        } else if($user->seller_type == 'broker' && $hour >= 1) {
            return true;
        } else {
            throw new Exception ('User not allowed to publish ticket at this time. Standard users must allow 24 hours of time before the event, brokers must leave at least one hour.');
        }
    }
}