<div>
    <h2>Seller Info</h2>
    <div class="Seller">
        <table class="table-striped" style="width:100%;">
            <tr>
                <td>Name</td>
                <td>{{$return['seller']['name']}}</td>
            </tr>
            <tr>
                <td>
                    Telephone
                </td>
                <td>
                    {{$return['seller']['tel']}}
                </td>
            </tr>
            <tr>
                <td>
                    Email
                </td>
                <td>
                    {{$return['seller']['email']}}
                </td>
            </tr>
            <tr>
                <td>
                    Payment method
                </td>
                <td>
                    {{$return['seller']['payment_option']}}
                </td>
            </tr>
        </table>
    </div >
    <h2>Order Info</h2>
    <div class="Order">
        <table class="table-striped" style="width:100%;">
            <tr>
                <td>
                    Event title
                </td>
                <td>
                    {{$return['event']['name']}}
                </td>
            </tr>
            <tr>
                <td>
                    Event date
                </td>
                <td>
                    {{$return['event']['time']}}
                </td>
            </tr>
            <tr>
                <td>
                    Event location
                </td>
                <td>
                    {{$return['event']['location']}}
                </td>
            </tr>
            <tr>
                <td>
                    Ticket type
                </td>
                <td>
                    {{$return['event']['type']}}
                </td>
            </tr>
            <tr>
                <td>
                    Ticket location
                </td>
                <td>
                    {{$return['event']['block']}} {{$return['event']['row']}}
                </td>
            </tr>
            <tr>
                <td>
                    Additional info
                </td>
                <td>
                    {{$return['event']['additional_info']}}
                </td>
            </tr>
            <tr>
                <td>
                    Restrictions
                </td>
                <td>
                    @if($return['event']['restrictions'])
                        @foreach($return['event']['restrictions'] as $val)
                            {{$val}}<br>
                        @endforeach
                    @endif
                </td>
            </tr>
            <tr>
                <td>
                    Total listed
                </td>
                <td>
                    {{$return['event']['total_listed']}}
                </td>
            </tr>
        </table>
        <div id="admin-order">
            
        </div>
    </div>
</div>

<script>
    $.ajax({
        method:"POST",
        url:"/admin/buy/get-admin-buy-form/{{$return['product_id']}}",
        success:function(e){
            $('#admin-order').html(e);
        }
    });
</script>