@extends('backend/_layout/layout')

{{
    Assets::setScripts([
    'dataTables.bootstrap'      => 'js/plugin/datatables/dataTables.bootstrap.min.js',
    'jquer-ui'                  => 'js/libs/jquery-ui-1.10.3.min.js',
    ], true);
}}

@section('content')

    <div class="row">
        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
            <h1 class="page-title txt-color-blueDark">
                <i class="fa fa-table fa-fw "></i>
                Ticket Types
            </h1>
        </div>

    </div>
    {{ Notification::showAll() }}
            <!-- widget grid -->

        <!-- row -->
        <div class="row">

            <!-- NEW WIDGET START -->
            <article class="col-xs-6">
                <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false">
                    <header>
                        <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                        <h2>Ticket Types</h2>
                    </header>
                    <div>
                        <ul id="sortable">
                            @foreach($ttypes as $ttype)
                                <li class="ui-state-default" id="ttlist-{{$ttype->id}}" data-id="{{$ttype->id}}">
                                    <span class="ui-icon ui-icon-trash delete-ttype" style="cursor:pointer;" data-id="{{$ttype->id}}"></span>
                                    {{$ttype->title}}
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </article>
            <div class="col-xs-6">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false">
                            <header>
                                <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                                <h2>Save List</h2>
                            </header>
                            <div>
                                <button class="btn btn-small" id="save-sort-order" style="background-color:mediumspringgreen">Save Order</button>
                                <br>
                                <br>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false">
                            <header>
                                <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                                <h2>Add Ticket Type</h2>
                            </header>
                            <div>
                                <div class="row">
                                    <div class="col-xs-10">
                                        <input id="add-new-input" type="text" style="width:100%;" placeholder="Ticket Type Name">
                                    </div>
                                    <div class="col-xs-2">
                                        <button class="button" id="add-new-btn">Save</button>
                                    </div>
                                </div>
                                <br>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

@stop
@section('script')
    <style>
        #sortable { list-style-type: none; margin: 0; padding: 0; width: 100%; }
        #sortable li { margin: 0 3px 3px 3px; padding-left: 1.5em; font-size: 1.4em; }
        #sortable li span { position: absolute; margin-left: -1.3em; }
    </style>
    <script>
        $(function() {
            $( "#sortable" ).sortable({
                update: function (event, ui) {
                    //This is where I ajax the whole list and process server side.
                    console.log(ui);
                    console.log(event);
                }
            });

            $( "#sortable" ).disableSelection();
        });

        $('body').on('click', '.delete-ttype', function(){
            var id = $(this).data('id');
            if(window.confirm('Delete this ticket type?'))
            {
                $.ajax({
                    url:'/admin/ticket-types/delete/'+id,
                    type:'POST',
                    success:function(response){
                        $('#ttlist-'+response).remove();
                    },
                    error:function(response){
                        alert("Error: "+response);
                    }
                });
            }
        });

        $('#add-new-btn').click(function(){
           var ttl = $('#add-new-input').val();

            $.ajax({
                url:'/admin/ticket-types/addNew',
                type:'POST',
                data:{
                  title:ttl
                },
                success:function(response){
                    var appStr = '<li class="ui-state-default" id="ttlist-'+response+'" data-id="'+response+'"><span class="ui-icon ui-icon-trash delete-ttype" style="cursor:pointer;" data-id="'+response+'"></span>'+ttl+'</li>';
                    $('#sortable').append(appStr);
                },
                error:function(response){
                    alert(response);
                }
            })
        });

        $('#save-sort-order').click(function(){
            //Get all of the things and get the sort orders and iddddd
            var elemArray = Array();
            $('#sortable li').each(function(){
                elemArray.push($(this).data('id'));
            });

            console.log(elemArray);

            $.ajax({
                url:"/admin/ticket-types/save-order",
                type:'POST',
                data:{
                    oArray:elemArray
                },
                success:function(response){
                    alert("Saved");
                },
                error:function(response){
                    alert("Not Saved");
                }
            })
        });
    </script>
@stop




