@extends('backend/_layout/layout')
@section('breadcrumb')
<li>Home</li><li>Customer</li><li>Edit</li>
@stop

{{
    Assets::setScripts([
    'dataTables.bootstrap'      => 'js/plugin/datatables/dataTables.bootstrap.min.js',
    ], true);
}}

@section('content')

<div class="row">
    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
        <h1 class="page-title txt-color-blueDark">
            <i class="fa fa-table fa-fw "></i>
            Ticket
        </h1>
    </div>

</div>
{{ Notification::showAll() }}
<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- row -->
    <div class="row">
        <!-- NEW WIDGET START -->
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false">
                <header>
                    <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                    <h2>Game # {{$node->title}} </h2>
                </header>
                <div>
                    <div class="jarviswidget-editbox">
                    </div>
                <form name="customer_update" action="{{ route('admin.ticket.info.update', array('id'=>$node->id)) }}" method="post">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <input name="event_id" value="{{ $node->event_id }}" type="hidden">
                        <input name="product_id" value="{{ $node->product_id }}" type="hidden">

                        <div class="widget-body">
                            <div class="row">
                                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">Game</div>
                                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">{{$node->title}} </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">Location</div>
                                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">{{$node->location}} </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">Date</div>
                                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">{{ date('d/m/Y H:i',strtotime($node->event_date)) }}</div>
                            </div>

                            <div class="row">
                                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">Seller First Name</div>
                                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">{{ $node->firstname }}</div>
                            </div>

                            <div class="row">
                                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">Seller Last Name</div>
                                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">{{ $node->lastname }}</div>
                            </div>

                            <div class="row">
                                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">Ticket Price</div>
                                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3"><input name="ticket_price" value="{{$node->price}}" class="text form-control" type="text" disabled></div>
                            </div>


                            <div class="row">
                                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">Ticket Status</div>
                                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                                    <select name="ticket_status" class="form-control" >
                                        <option value="active" <?php echo $node->ticket_status == 'active'? 'selected': ''; ?> >Active</option>
                                        <option value="paused" <?php echo $node->ticket_status == 'paused'? 'selected': ''; ?> >Paused</option>
                                        <option value="pending" <?php echo $node->ticket_status == 'pending'? 'selected': ''; ?> >Pending</option>
                                        <option value="sold" <?php echo $node->ticket_status == 'sold'? 'selected': ''; ?> >Sold</option>
                                        <option value="blocked" <?php echo $node->ticket_status == 'blocked'? 'selected': ''; ?> >Block</option>
                                        <option value="deleted" <?php echo $node->ticket_status == 'deleted'? 'selected': ''; ?> >Deleted</option>
                                    </select>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-3 col-sm-3col-md-3 col-lg-3">
                                    <input type="submit" class="btn btn-info" value="Save">
                                </div>

                            </div>

                        </div>

                </form>
                </div>
                </div>
            </div>
        </article>
    </div>
</section>
@stop



