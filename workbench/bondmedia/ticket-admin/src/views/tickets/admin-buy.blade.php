<form id = "admin-buy-form" data-pid="{{$shared['pid']}}" data-eid="{{$shared['event_id']}}" action="/admin/buy/admin-buy-action/{{$shared['pid']}}" method="POST"><br>
	Number of Tickets:
	<select name="qty" id="qty_sel">
		@foreach($shared['available_to_buy'] as $option)
			<option value="{{$option}}">{{$option}}</option>
		@endforeach
	</select><br><br>

	<input type="text" name="firstname" placeholder="First Name">
	<input type="text" name="lastname" placeholder = "Last Name"><br>

	<input type="text" name="street" placeholder="Street">
	<input type="text" name="city" placeholder="City"><br>

	<input type="text" name="postcode" placeholder="Postcode">
	<input type="text" name="telephone" placeholder="Phone Number"><br><br>
	Country: 
	<select name="country" id="country_sel" autocomplete="off">
		<option value="" selected >--- Select ---</option>
		@foreach($shared['country'] as $key=>$country)
			<option value="{{$key}}">{{$country}}</option>
		@endforeach
	</select><br><br>

	Delivery Method:
	<select name="delivery_method" id="delivery_select">
	</select> 
	<br><br>
	<input type="submit" value="Order">
</form>

<script>
	$('#country_sel').change(function(e){
		$.ajax({
			url:"/admin/buy/get-shipping/{{$shared['event_id']}}",
			method:"POST",
			data:{
				rtID:"{{$shared['pid']}}"
			},
			success:function(e){
				$('#delivery_select').html("");

				for(var i = 0; i < e.data.length; i++)
				{
					$('#delivery_select').append('<option value="'+ e.data[i].value +'">'+ e.data[i].label +'</option>');
				}
			},
			error:function(e){
				alert("Oh no!");
			}
		});
	});

	$('#admin-buy-form').submit(function(e){
		var data = $(this).serialize();
		$.ajax({
			url:"/admin/buy/admin-buy-action/{{$shared['pid']}}",
			method:"POST",
			data:data,
			success:function(e){
				alert("success");
			},
			error:function(e){
				alert(e);
			}
		})
		e.preventDefault();
	})
</script>