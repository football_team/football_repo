@extends('emails/_layout/layout')
@section('content')
<!-- body -->
<table class="body-wrap">
    <tr>
        <td></td>
        <td class="container" bgcolor="#FFFFFF">

            <!-- content -->
            <div class="content">
            
            	<a href="https://www.footballticketpad.com/"><img src="{{ Assets::Path('images/email-logo.png') }}" alt="Football Ticket Pad" /></a>
                
                <p><strong>The Home of Football Tickets!</strong></p>
                <h2>Confirmation of Payment </h2>
                <p>Hello {{$rs[0]->seller_firstname.' '.$rs[0]->seller_lastname}},</p>

                <p>We have made a payment for the ticket(s) you have sold. Please find below details of the ticket(s).</p>
                <table>
                    <tr>
                        <th>
                            Event title
                        </th>
                        <th>
                            Order ID
                        </th>
                        <th >
                            Tickets sold
                        </th>
                        <th>
                            Your payment
                        </th>
                    </tr>
                    @foreach($rs as $r)
                    <tr>
                        <td>
                            {{$r->title}}</p>
                        </td> 
                        <td>
                            <p>{{$r->order_id}}</p>
                        </td>
                        <td style="text-align: center;"> 
                            <p>{{$r->qty}}</p>
                        </td>
                        <td>
                            <p>£{{number_format(($r->listed_amount*$r->qty)*0.85, 2)}} </p>
                        </td>
                    </tr>
                    @endforeach
                </table>

                <p> Payment:  £{{number_format($rs2->total_payment, 2) }} has been paid to your chosen method. </p>
                <p> If you have any questions about your account or any other matter, please feel free to contact us at <a href="mailto:info@footballticketpad.com">info@footballticketpad.com</a></p>
                <p>To view a list of FAQs please click <a href="https://www.footballticketpad.com/faq">here</a></p>

        	    <p>Thank you, <strong>Football Ticket Pad</p>                
            </div>
            <!-- /content -->

        </td>
        <td></td>
    </tr>
</table>
<!-- /body -->

@stop