@extends('emails/_layout/layout')
@section('content')
<!-- body -->
<table class="body-wrap">
    <tr>
        <td></td>
        <td class="container" bgcolor="#FFFFFF">

            <!-- content -->
            <div class="content">
            
            	<a href="https://www.footballticketpad.com/"><img src="{{ Assets::Path('images/email-logo.png') }}" alt="Football Ticket Pad" /></a>
                
                <p><strong>The Home of Football Tickets!</strong></p>
                <h2>Confirmation of Payment </h2>
                <p>Hello {{$rs[0]->seller_firstname.' '.$rs[0]->seller_lastname}},</p>

                <p>We have made a payment for the ticket(s) you have sold. Please find below details of the ticket(s) and bonuses/penalties.</p>
                <table>
                    <tr>
                        <th>
                            Event title
                        </th>
                        <th>
                            Order ID
                        </th>
                        <th >
                            Tickets sold
                        </th>
                        <th>
                            Your payment
                        </th>
                    </tr>
                    @foreach($rs as $r)
                    <tr>
                        <td>
                            <p>{{$r->title}}</p>
                        </td> 
                        <td>
                            <p>{{$r->order_id}}</p>
                        </td>
                        <td style="text-align: center;"> 
                            <p>{{$r->qty}}</p>
                        </td>
                        <td>
                            <p>£{{number_format(($r->listed_amount*$r->qty)*0.85, 2)}} </p>
                        </td>
                    </tr>
                    @endforeach
                    <tr>
                        <td>

                        </td> 
                        <td>

                        </td>
                        <td style="text-align: center;"> 
                            <p>Total:</p>
                        </td>
                        <td>
                            <p>£{{number_format($rs2->total_payment, 2) }}</p>
                        </td>
                    </tr>
                </table><br>
                <h2>Penalties</h2>
                <table>
                    <thead>
                    <tr>
                        <th>Penalty/Bonus Note</th>
                        <th>Penalty/Bonus Amount</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($rs3 as $rs)
                    <tr>
                        <td>{{$rs->comment}}</td>
                        <td>£{{number_format($rs->ammount, '2', '.','')}}</td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
                <br>
                <h2>Totals</h2>
                <table>
                    <tr>
                        <th>Earnings</th>
                        <th>Additional earnings/penalties</th>
                        <th>Total</th>
                    </tr>
                    <tr>
                        <td>£{{number_format($rs2->total_payment, 2)}}</td>
                        <td>£{{number_format($rs4->total_pens, 2)}}</td>
                        <td>£{{number_format(($rs2->total_payment + $rs4->total_pens), 2)}}</td>
                    </tr>
                </table>
                <h2>Account details</h2>
                <table>
                    <tr>
                        <td>Account Holder</td>
                        <td>{{$acc_details['ahn']}}</td>
                    </tr>
                    <tr>
                        <td>Account Number</td>
                        <td>{{$acc_details['acc_num']}}</td>
                    </tr>
                    <tr>
                        <td>Sort Code</td>
                        <td>{{$acc_details['sortcode']}}</td>
                    </tr>
                    <tr>
                        <td>IBAN</td>
                        <td>{{$acc_details['iban']}}</td>
                    </tr>
                    <tr>
                        <td>SWIFT</td>
                        <td>{{$acc_details['swift']}}</td>
                    </tr>
                    <tr>
                        <td>Note</td>
                        <td>{{$acc_details['note']}}</td>
                    </tr>
                </table>
                <p><b>NOTE If this total value is negative then you have been paid £0.00 and there is still a penalty of that amount to be taken. This will be deducted next month.</b></p>
                <p> If you have any questions about your account or any other matter, please feel free to contact us at <a href="mailto:info@footballticketpad.com">info@footballticketpad.com</a></p>
                <p>To view a list of FAQs please click <a href="https://www.footballticketpad.com/faq">here</a></p>

        	    <p>Thank you, <strong>Football Ticket Pad</strong></p>
            </div>
            <!-- /content -->
        </td>
        <td></td>
    </tr>
</table>
<!-- /body -->

@stop