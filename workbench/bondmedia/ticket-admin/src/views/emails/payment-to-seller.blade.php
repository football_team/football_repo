@extends('emails/_layout/layout')
@section('content')
<!-- body -->
<table class="body-wrap">
    <tr>
        <td></td>
        <td class="container" bgcolor="#FFFFFF">

            <!-- content -->
            <div class="content">
                <table>
                    <tr>
                        <td>
                            <h2>Game</h2>
                            <p>{{$game}}</p>
                            <h2>Event ID # {{$event_id}}</h2>
                            <h2>Order Id/ Transaction</h2>
                            <p>{{$order_id}}</p>
                            <h2>Message</h2>
                            <p>Football Ticket pad has been made payment.</p>
                            <p>Total Amount: £{{$total_amount}}</p>
                            <p>Commission:   £{{$commission}} </p>
                            <p>----------------------</p>
                            <p>Paid      : £{{$total_amount_after_commission}} </p>

                        </td>
                    </tr>
                </table>
            </div>
            <!-- /content -->

        </td>
        <td></td>
    </tr>
</table>
<!-- /body -->

@stop