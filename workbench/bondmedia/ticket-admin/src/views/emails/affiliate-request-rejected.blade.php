@extends('emails/_layout/layout')
@section('content')
        <!-- body -->
<table class="body-wrap">
    <tr>
        <td></td>
        <td class="container" bgcolor="#FFFFFF">

            <!-- content -->
            <div class="content">

                <a href="https:///www.footballticketpad.com/"><img src="{{ Assets::Path('images/email-logo.png') }}" alt="Football Ticket Pad" /></a>

                <h2>{{trans('homepage.affilARD1')}}</h2>
                <p>{{trans('homepage.affilARD2')}} {{$contact_name}},</p>

                <p>{{trans('homepage.affilARD3')}}</p>

                <p>{{trans('homepage.affilARD4')}} <strong>Football Ticket Pad</p>
            </div>
            <!-- /content -->

        </td>
        <td></td>
    </tr>
</table>
<!-- /body -->

@stop