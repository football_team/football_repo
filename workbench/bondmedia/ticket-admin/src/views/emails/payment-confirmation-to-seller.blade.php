@extends('emails/_layout/layout')
@section('content')
<!-- body -->
<table class="body-wrap">
    <tr>
        <td></td>
        <td class="container" bgcolor="#FFFFFF">

            <!-- content -->
            <div class="content">
            
            	<a href="https://www.footballticketpad.com/"><img src="{{ Assets::Path('images/email-logo.png') }}" alt="Football Ticket Pad" /></a>
                
                <p><strong>The Home of Football Tickets!</strong></p>
                <h2>Confirmation of Payment </h2>
                <p>Hello {{$seller_name}},</p>

                <p>We have made a payment for the ticket(s) you have sold. Please find below details of the ticket(s).</p>
                <table>
                    <tr>
                        <td>
                            <p><strong>Game details:</strong><br/>
                           {{$game}}</p>
                            
                             <p><strong>Event ID</strong><br/>
                             {{$event_id}}</p>
                            
                              <p><strong>Order Id/ Transaction#</strong><br/>
                              <p>{{$order_id}}</p>
                            
                             <p><strong>Form Of ticket</strong><br/>
                            {{$formOfTicket}}</p>
                            
                            <p><strong>Location</strong><br/>
                            {{$location}}</p>
                            
                            <p><strong>Restrictions</strong><br/>
                            @if(!empty($restrictions))
                            {{$restrictions}}
                            @else
                          	 No restrictions specified.
                            @endif
                            <p>&nbsp;</p>
                            
                            <p><strong>Qty (No. of ticket purchased):</strong> {{$qty}} </p>
                            
                            <p><strong>Total Amount:</strong> £{{number_format($total_amount, 2)}} </p>
                            
                            <p><strong>Commission:</strong>   £{{number_format($commission, 2)}}</p>
                            
                            <p>--------------------------------</p>

                            <p>&nbsp;</p>
                            
                            <p> Payment:  £{{number_format($total_amount_after_commission, 2) }} has been paid to your chosen method. </p>
                            <p> If you have any questions about your account or any other matter, please feel free to contact us at <a href="mailto:info@footballticketpad.com">info@footballticketpad.com</a></p>
                            <p>To view a list of FAQs please click <a href="https://www.footballticketpad.com/faq">here</a></p>
                        </td>
                    </tr>
                    <tr>
                    	<td bgcolor="#D4D4D4" align="center" style="padding:20px">Thank you, <strong>Football Ticket Pad</strong></td>
                    </tr>
                </table>
            </div>
            <!-- /content -->

        </td>
        <td></td>
    </tr>
</table>
<!-- /body -->

@stop