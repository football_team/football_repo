@extends('emails/_layout/layout')
@section('content')
<!-- body -->
<table class="body-wrap">
    <tr>
        <td></td>
        <td class="container" bgcolor="#FFFFFF">

            <!-- content -->
            <div class="content">
            
            	<a href="https://www.footballticketpad.com/"><img src="{{ Assets::Path('images/email-logo.png') }}" alt="Football Ticket Pad" /></a>
                
                <h2>E-Tickets require approval!</h2>

                <p>Event Name: {{$event->title}}</p>
                <p>Event Date: {{$event->datetime}}</p>
                <p>Ticket Type: {{$event->ticket_type}}</p>
                <p>Block: {{$event->block}}</p>
                <p>Row: {{$event->row}}</p>
                <p>Qty: {{$event->qty}}</p>
                <br><br>
                <p>Buyer: {{$buyer->firstname}} {{$buyer->lastname}}</p>
                <p>Seller: {{$seller->firstname}} {{$seller->lastname}}</p>              
            </div>
            <!-- /content -->

        </td>
        <td></td>
    </tr>
</table>
<!-- /body -->

@stop