@extends('emails/_layout/layout')
@section('content')
<!-- body -->
<table class="body-wrap">
    <tr>
        <td></td>
        <td class="container" bgcolor="#FFFFFF">

            <!-- content -->
            <div class="content">
            
            	<a href="https://www.footballticketpad.com/"><img src="{{ Assets::Path('images/email-logo.png') }}" alt="Football Ticket Pad" /></a>
                
                <p><strong>The Home of Football Tickets!</strong></p>
                <h2>Your E-Tickets are available to download!</h2>
                <p>Hello {{$buyer->firstname}} {{$buyer->lastname}},</p>

                <p>All of your E-tickets are now available for:</p>

                <p>Event Name: {{$event->title}}</p>
                <p>Event Date: {{$event->datetime}}</p>
                <p>Ticket Type: {{$event->ticket_type}}</p>
                <p>Block: {{$event->block}}</p>
                <p>Row: {{$event->row}}</p>

                <table>
                    <tr>
                        <th>
                            Uploaded At
                        </th>
                        <th>
                            Download Link (sign in first)
                        </th>
                    </tr>
                    @foreach($tickets as $ticket)
                    <tr>
                        <td>
                            <p>{{$ticket->created}}</p>
                        </td> 
                        <td>
                            <p><a href="https://www.footballticketpad.com/account/download-ticket/{{$ticket->id}}">Download</a></p>
                        </td>
                    </tr>
                    @endforeach
                </table><br>

                <p> If you have any questions about your account or any other matter, please feel free to contact us at <a href="mailto:info@footballticketpad.com">info@footballticketpad.com</a></p>
                <p>To view a list of FAQs please click <a href="https://www.footballticketpad.com/faq">here</a></p>

        	    <p>Thank you, <strong>Football Ticket Pad</p>                
            </div>
            <!-- /content -->

        </td>
        <td></td>
    </tr>
</table>
<!-- /body -->

@stop