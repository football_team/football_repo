@extends('emails/_layout/layout')
@section('content')
<!-- body -->
<table class="body-wrap">
    <tr>
        <td></td>
        <td class="container" bgcolor="#FFFFFF">

            <!-- content -->
            <div class="content">
            
            	<a href="https://www.footballticketpad.com/"><img src="{{ Assets::Path('images/email-logo.png') }}" alt="Football Ticket Pad" /></a>
                
                <p><strong>The Home of Football Tickets!</strong></p>
                <h2>Some of your e-tickets have been rejected</h2>
                <p>Hello {{$seller->firstname}} {{$seller->lastname}},</p>

                <p>Some of your tickets have been deleted for your order:</p>

                <p>Event Name: {{$event->title}}</p>
                <p>Event Date: {{$event->datetime}}</p>
                <p>Customer Name: {{$buyer->firstname}} {{$buyer->lastname}}</p>
                <p>Ticket Type: {{$event->ticket_type}}</p>
                <p>Block: {{$event->block}}</p>
                <p>Row: {{$event->row}}</p>

                <table>
                    <tr>
                        <th>
                            Uploaded At
                        </th>
                        <th>
                            Action
                        </th>
                    </tr>
                    @foreach($tickets as $ticket)
                    <tr>
                        <td>
                            <p>{{$ticket->created}}</p>
                        </td> 
                        <td>
                            <p>Deleted</p>
                        </td>
                    </tr>
                    @endforeach
                    <tr>
                        <td style="text-align: center;"> 
                            <p>Total:</p>
                        </td>
                        <td>
                            <p>{{$num_deleted}}</p>
                        </td>
                    </tr>
                </table><br>
                <h2>Common Reasons for Deletion</h2>
                
                <p>E-Tickets may be deleted if they do not suit the order description, if they are duplicated or if the PDF's contain E-Tickets that a customer should not receive.</p><br>
                <p> If you have any questions about your account or any other matter, please feel free to contact us at <a href="mailto:info@footballticketpad.com">info@footballticketpad.com</a></p>
                <p>To view a list of FAQs please click <a href="https://www.footballticketpad.com/faq">here</a></p>

        	    <p>Thank you, <strong>Football Ticket Pad</p>                
            </div>
            <!-- /content -->

        </td>
        <td></td>
    </tr>
</table>
<!-- /body -->

@stop