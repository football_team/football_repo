@extends('emails/_layout/layout')
@section('content')
        <!-- body -->
<table class="body-wrap">
    <tr>
        <td></td>
        <td class="container" bgcolor="#FFFFFF">

            <!-- content -->
            <div class="content">

                <a href="https:///www.footballticketpad.com/"><img src="{{ Assets::Path('images/email-logo.png') }}" alt="Football Ticket Pad" /></a>

                <h2>{{trans('homepage.affilARA1')}}</h2>
                <p>{{trans('homepage.affilARA2')}} {{$contact_name}},</p>

                <p>{{trans('homepage.affilARA3')}}</p>

                <p>{{trans('homepage.affilARA4')}} ({{$company_url}}). {{trans('homepage.affilARA5')}}</p>

                <p>
                    &lt;meta name=&quot;af_network&quot; content=&quot;{{$ver_code}}&quot;&gt;
                </p>

                <p>
                    {{trans('homepage.affilARA6')}}
                </p>

                <p>
                    <a href="http:///www.footballticketpad.com/affiliates/verify">http:///www.footballticketpad.com/affiliates/verify</a>
                </p>

                <p>
                    {{trans('homepage.affilARA7')}}
                </p>

                <p>{{trans('homepage.affilARA8')}} <strong>Football Ticket Pad</strong></p>
            </div>
            <!-- /content -->

        </td>
        <td></td>
    </tr>
</table>
<!-- /body -->

@stop