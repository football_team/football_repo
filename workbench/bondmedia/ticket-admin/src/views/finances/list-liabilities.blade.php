@extends('backend/_layout/layout')
@section('breadcrumb')
<li>Home</li><li>Tickets</li>
@stop

{{
    Assets::setScripts([
        'jquery.dataTables'         => 'js/plugin/datatables/jquery.dataTables.min.js',
        'dataTables.colVis'         => 'js/plugin/datatables/dataTables.colVis.min.js',
        'dataTables.tableTools'     => 'js/plugin/datatables/dataTables.tableTools.min.js',
        'dataTables.bootstrap'      => 'js/plugin/datatables/dataTables.bootstrap.min.js',
    ], true);
}}

@section('content')
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Full order details</h4>
      </div>
      <div class="modal-body" id="fill-this">
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<div class="row">
    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
        <h1 class="page-title txt-color-blueDark">
            <i class="fa fa-table fa-fw "></i>
            Finances - Monthly Broker Liabilities
        </h1>
    </div>

</div>
{{ Notification::showAll() }}
<!-- widget grid -->
<section id="widget-grid" class="">

    <div class="row">

        <!-- NEW WIDGET START -->
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="jarviswidget jarviswidget-color-darken" id="wid-id-1" data-widget-editbutton="false" >
                <header>
                    <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                    <h2>Totals</h2>
                </header>
                <div>
                    <div class="jarviswidget-editbox">
                    </div>
                    <div class="myFilters" style="margin-bottom:20px;">
                    <label for="filterDate">Select Month: </label><input type="text" id="filterDate" name="filterDate" value="{{$todaysDate}}">
                    </div>
                    <div class="widget-body no-padding">
                        <table id="dt_basic2" class="table table-striped table-bordered table-hover" width="100%">
                            <thead>
                            <tr>
                                <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i> Tickets Sold this month </th>
                                <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i> This month total sales </th>
                                <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i> This months broker liabiliy </th>
                                <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i> Historic broker liability </th>
                                <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i> All time quantity sold </th>
                                <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i> All time broker liability </th>
                            </tr>
                            </thead>
                            <tbody>    

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </article>
    </div>
    <!-- row -->
    <div class="row">

        <!-- NEW WIDGET START -->
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false" >
                <header>
                    <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                    <h2>Sellers</h2>
                </header>
                <div>
                    <div class="jarviswidget-editbox">
                    </div>
                    <div class="widget-body no-padding">
                        <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i> Name </th>
                                <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i> Tickets Sold </th>
                                <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i> Tickets sold this month </th>
                                <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i> This months sales total </th>
                                <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i> This months due payment </th>
                                <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i> Historic broker liability </th>
                                <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i> Most recent sale </th>
                                <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i> Future broker liability </th>
                                <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i> All time broker liability </th>
                            </tr>
                            </thead>
                            <tbody>    

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </article>
    </div>

</section>
@stop

@section ('script')
<script>
    $(document).ready(function() {
        $(function() {
            $( "#filterDate" ).datepicker({ dateFormat: 'dd-mm-yy' });
            console.log("Hello");
        });
        $('#dt_basic').dataTable({
            serverSide: true,
            ajax: {
                url: "/{{ Config::get('bondcms.admin_prefix') }}/finances/list.json",
                type: 'POST',
                data: {
                    'dateFilter': function(){ return $('#filterDate').val()}
                },
            },
            order: [[0, 'ASC']],
            "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
                $('.delete-node', nRow).remove();
            }

        });
        $('#dt_basic2').dataTable({
            serverSide: true,
            ajax: {
                url: "/{{ Config::get('bondcms.admin_prefix') }}/finances/totals.json",
                type: 'POST',
                data: {
                    'dateFilter': function(){ return $('#filterDate').val()}
                },
            },
            order: [[0, 'ASC']],
            "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
                $('.delete-node', nRow).remove();
            }

        });
    });
    $('#filterDate').change(function(){
        var table = $('#dt_basic').DataTable();
        table.ajax.reload();
        var table = $('#dt_basic2').DataTable();
        table.ajax.reload();
    });
</script>
@stop


