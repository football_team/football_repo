@extends('backend/_layout/layout')
@section('breadcrumb')
    <li>Home</li><li>Tickets</li>
@stop

{{
    Assets::setScripts([
        'jquery.dataTables'         => 'js/plugin/datatables/jquery.dataTables.min.js',
        'dataTables.colVis'         => 'js/plugin/datatables/dataTables.colVis.min.js',
        'dataTables.tableTools'     => 'js/plugin/datatables/dataTables.tableTools.min.js',
        'dataTables.bootstrap'      => 'js/plugin/datatables/dataTables.bootstrap.min.js',
    ], true);
}}

@section('content')
    <div class="row">
        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
            <h1 class="page-title txt-color-blueDark">
                <i class="fa fa-table fa-fw "></i>
                Broker Payment Data
            </h1>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <a href="/admin/finances/payment-history/indiv/printable/{{$id}}" class="btn btn-info print">Printable</a>
        </div>
    </div>
    {{ Notification::showAll() }}
            <!-- widget grid -->
    <section id="widget-grid" class="">
        <div class="row">
            <!-- NEW WIDGET START -->
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="jarviswidget jarviswidget-color-darken" id="wid-id-1" data-widget-editbutton="false" >
                    <header>
                        <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                        <h2>Totals</h2>
                    </header>
                    <div>
                        <div class="widget-body no-padding">
                            <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                                <thead>
                                <tr>
                                    <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i> Amount Paid </th>
                                    <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i> Date Created </th>
                                    <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i> Date Paid </th>
                                    <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i> Broker Name </th>
                                    <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i> Account Name </th>
                                    <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i> Account Number </th>
                                    <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i> SWIFT </th>
                                    <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i> IBAN </th>
                                    <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i> Sort Code </th>
                                    <th>Note</th>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </article>
        </div>
        <!-- row -->
        <div class="row">
            <!-- NEW WIDGET START -->
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="jarviswidget jarviswidget-color-darken" id="wid-id-1" data-widget-editbutton="false" >
                    <header>
                        <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                        <h2>Sales</h2>
                    </header>
                    <div>
                        <div class="widget-body no-padding">
                            <table id="dt_basic2" class="table table-striped table-bordered table-hover" width="100%">
                                <thead>
                                <tr>
                                    <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i> id </th>
                                    <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i> Order id </th>
                                    <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i> order date </th>
                                    <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i> Broker earnings </th>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </article>
        </div>

        <div class="row">
            <!-- NEW WIDGET START -->
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="jarviswidget jarviswidget-color-darken" id="wid-id-1" data-widget-editbutton="false" >
                    <header>
                        <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                        <h2>Penalties</h2>
                    </header>
                    <div>
                        <div class="widget-body no-padding">
                            <table id="dt_basic3" class="table table-striped table-bordered table-hover" width="100%">
                                <thead>
                                <tr>
                                    <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i> ID </th>
                                    <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i> Amount </th>
                                    <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i> Comment </th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </article>
        </div>

    </section>
@stop

@section ('script')
    <script>
        $(document).ready(function() {


            $('#dt_basic').dataTable({
                serverSide: true,
                ajax: {
                    url: "/{{ Config::get('bondcms.admin_prefix') }}/finances/payment-history/indiv/totals/{{$id}}",
                    type: 'POST',
                },
                order: [[0, 'ASC']],
                "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
                    $('.delete-node', nRow).remove();
                }

            });
            $('#dt_basic2').dataTable({
                serverSide: true,
                ajax: {
                    url: "/{{ Config::get('bondcms.admin_prefix') }}/finances/payment-history/indiv/sales/{{$id}}",
                    type: 'POST',
                },
                order: [[0, 'ASC']],
                "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
                    $('.delete-node', nRow).remove();
                }

            });

            $('#dt_basic3').dataTable({
                serverSide: true,
                ajax: {
                    url: "/{{ Config::get('bondcms.admin_prefix') }}/finances/payment-history/indiv/penalties/{{$id}}",
                    type: 'POST',
                },
                order: [[0, 'ASC']],
                "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
                    $('.delete-node', nRow).remove();
                }

            });

            $('body').on('click','.view-note' ,function(){
                var url = "/admin/finances/payment-history/indiv/getNote/{{$id}}";
                $.ajax({
                    url: url,
                    type: 'POST',
                    success: function (response) {
                        alert(response);
                    },
                    error: function (response) {
                        console.log(response);
                    }
                });

            });

        });
    </script>
@stop


