@extends('backend/_layout/layout')
@section('breadcrumb')
    <li>Home</li><li>Tickets</li>
@stop

{{
    Assets::setScripts([
        'jquery.dataTables'         => 'js/plugin/datatables/jquery.dataTables.min.js',
        'dataTables.colVis'         => 'js/plugin/datatables/dataTables.colVis.min.js',
        'dataTables.tableTools'     => 'js/plugin/datatables/dataTables.tableTools.min.js',
        'dataTables.bootstrap'      => 'js/plugin/datatables/dataTables.bootstrap.min.js',
    ], true);
}}

@section('content')
    <div class="row">
        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
            <h1 class="page-title txt-color-blueDark">
                <i class="fa fa-table fa-fw "></i>
                Finances - Monthly Broker Liabilities
            </h1>
        </div>

    </div>
    {{ Notification::showAll() }}
    <label for="filterDate">Select Month: </label><input type="text" id="filterDate" name="filterDate" value="{{$today}}">

    <!-- widget grid -->
    <section id="widget-grid" class="">
        <!-- row -->
        <div class="row">
            <!-- NEW WIDGET START -->
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false" >
                    <header>
                        <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                        <h2>Payments made</h2>
                    </header>
                    <div>
                        <div class="widget-body no-padding">
                            <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i> Date Created </th>
                                    <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i> Name </th>
                                    <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i> Amount Paid </th>
                                    <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i> Date Paid </th>
                                    <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i> Account Name </th>
                                    <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i> Account Number </th>
                                    <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i> IABN </th>
                                    <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i> SWIFT </th>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </article>
        </div>

    </section>
@stop

@section ('script')
    <script>
        $(document).ready(function() {
            $(function() {
                $( "#filterDate" ).datepicker({ dateFormat: 'dd-mm-yy' });
            });

            $('#dt_basic').dataTable({
                serverSide: true,
                ajax: {
                    url: "/{{ Config::get('bondcms.admin_prefix') }}/finances/payment-history/list",
                    type: 'POST',
                    data: {
                        'dateFilter': function(){ return $('#filterDate').val()}
                    }
                },
                order: [[0, 'ASC']],
                "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
                    $('.delete-node', nRow).remove();
                }

            });


            $('#filterDate').change(function(){
                var table = $('#dt_basic').DataTable();
                table.ajax.reload();
            });
        });
    </script>
@stop


