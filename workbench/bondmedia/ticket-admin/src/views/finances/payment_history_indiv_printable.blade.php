<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Individual Payment History - {{$ph->firstname . " " . $ph->lastname}}</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h2>Payment Report: {{$ph->firstname." ".$ph->lastname}} - {{$ph->created_at}}</h2>
            </div>
        </div>
        <br><br>
        <div class="row">
            <div class="col-xs-4">
                <table class="table table-striped">
                    <tbody>
                    <tr>
                        <td>
                            Date Paid
                        </td>
                        <td>
                            {{$ph->payment_date}}
                        </td>
                    </tr>
                    <tr>
                        <td>Total Paid</td>
                        <td>£{{$ph->total_paid}}</td>
                    </tr>
                    <tr>
                        <td>Account Holder Name</td>
                        <td>{{$ph->account_holder_name}}</td>
                    </tr>
                    <tr>
                        <td>IBAN</td>
                        <td>{{$ph->IBAN}}</td>
                    </tr>
                    <tr>
                        <td>SWIFT</td>
                        <td>{{$ph->SWIFT}}</td>
                    </tr>
                    <tr>
                        <td>Account Number</td>
                        <td>{{$ph->acc_num}}</td>
                    </tr>
                    <tr>
                        <td>Sort Code</td>
                        <td>{{$ph->sort_code}}</td>
                    </tr>
                    <tr>
                        <td>Note:</td>
                        <td>{{$ph->note}}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="col-xs-8">
                <h3 style="margin-top:0px;">Orders</h3>
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>Order ID</th>
                        <th>Order Date</th>
                        <th>Order Total</th>
                        <th>Broker Earning</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($sales as $sale)
                            <tr>
                                <td>{{$sale->order_id}}</td>
                                <td>{{$sale->created_at}}</td>
                                <td>£{{number_format($sale->amount,2,'.','')}}</td>
                                <td>£{{number_format(($sale->amount-$sale->fees_amount)*0.85,2,'.','')}}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

                <h3>Penalties</h3>
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>Penalty ID</th>
                        <th>Amount</th>
                        <th>Comment</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($penalties as $penalty)
                        <tr>
                            <td>{{$penalty->id}}</td>
                            <td>£{{number_format($penalty->ammount,2,'.','')}}</td>
                            <td>{{$penalty->comment}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

        </div>
    </div>
</body>
</html>