@extends('backend/_layout/layout')
@section('breadcrumb')
<li>Home</li><li>Tickets</li>
@stop

{{
    Assets::setScripts([
        'jquery.dataTables'         => 'js/plugin/datatables/jquery.dataTables.min.js',
        'dataTables.colVis'         => 'js/plugin/datatables/dataTables.colVis.min.js',
        'dataTables.tableTools'     => 'js/plugin/datatables/dataTables.tableTools.min.js',
        'dataTables.bootstrap'      => 'js/plugin/datatables/dataTables.bootstrap.min.js',
    ], true);
}}

@section('content')
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Full order details</h4>
      </div>
      <div class="modal-body" id="fill-this">
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<div id="myModal2" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add penalty</h4>
      </div>
      <div class="modal-body">
        <table style="width:100%;">
        <form method="post" id="addPenForm" action="/{{ Config::get('bondcms.admin_prefix') }}/finances/seller-liabilities/penalties/add/{{$id}}">
            <tr>
                <td><label for="order_number">Order Number : </label></td><td><input type="text" name="order_number" id="aPOID"></td>
            </tr>
            <tr>
                <td><label for="ammount">Ammount (- for a penalty) : </label></td><td><input type="text" name="ammount" id='aPAm'></td>
            </tr>
            <tr>
                <td><label for="comment">Comment : </label></td><td><input type="text" name="comment" id="aPCom"></td>
            </tr>
            <tr><td></td><td><input type="submit" value="Add penalty"></td></tr>
        </form>
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<div class="row">
    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
        <h1 class="page-title txt-color-blueDark">
            <i class="fa fa-table fa-fw "></i>
            {{$seller_name}} - Individual Liabilities
        </h1>
    </div>

</div>
{{ Notification::showAll() }}
<!-- widget grid -->
<section id="widget-grid" class="">
    <div class="row">

        <!-- NEW WIDGET START -->
        <!--article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="jarviswidget jarviswidget-color-darken" id="wid-id-1" data-widget-editbutton="false" >
                <header>
                    <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                    <h2>Totals</h2>
                </header>
                <div>
                    <div class="jarviswidget-editbox">
                    </div>
                    <div class="widget-body no-padding">
                        <table id="dt_basic2" class="table table-striped table-bordered table-hover" width="100%">
                            <thead>
                            <tr>
                                <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i> Tickets Sold this month </th>
                                <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i> This month total sales </th>
                                <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i> This months broker liabiliy </th>
                                <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i> Historic broker liability </th>
                                <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i> All time quantity sold </th>
                                <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i> All time broker liability </th>
                            </tr>
                            </thead>
                            <tbody>    

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </article-->
    </div>
    <div class="row">

        <!-- NEW WIDGET START -->
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="jarviswidget jarviswidget-color-darken" id="wid-id-1" data-widget-editbutton="false" >
                <header>
                    <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                    <h2>Individual Totals</h2>
                </header>
                <div>
                    <div class="jarviswidget-editbox">
                    </div>
                    <div class="myFilters" style="margin-bottom:20px;">
                    <label for="filterDate">Select Month: </label><input type="text" id="filterDate" name="filterDate" value="{{$todaysDate}}">
                    </div>
                    <div class="widget-body no-padding">
                        <table id="dt_basic2" class="table table-striped table-bordered table-hover" width="100%">
                            <thead>
                            <tr>
                                <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i> Tickets Sold this month</th>
                                <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i> Tickets Sold all time</th>
                                <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i> This months sales total </th>
                                <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i> All time sales total </th>
                                <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i> This months broker liability </th>
                                <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i> Historic broker liability </th>
                                <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i> Total broker liability </th>
                            </tr>
                            </thead>
                            <tbody>    

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </article>
    </div>
    <!-- row -->
    <div class="row">

        <!-- NEW WIDGET START -->
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false" >
                <header>
                    <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                    <h2>Sales</h2>
                </header>
                <div>
                    <div class="jarviswidget-editbox">
                    </div>
                    <input type="checkbox" name="checkAll" id="checkAll" value="check"> Select/deselect all
                    <button type="button" class="edit-node" id="saveBtn" style="margin:0 0 20px 0;">Save Paid</button>
                    <div class="widget-body no-padding">
                        <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i> Order ID </th>
                                <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i> Tickets Sold </th>
                                <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i> Sale Price </th>
                                <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i> Broker earnings </th>
                                <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i> Payment Status </th>
                                <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i> Event Date </th>
                                <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i> Buyer Name </th>
                                <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i> Tracking Code </th>
                                <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i> Paid </th>
                            </tr>
                            </thead>
                            <tbody>    

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </article>
    </div>
    <div class="row">

        <!-- NEW WIDGET START -->
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="jarviswidget jarviswidget-color-darken" id="wid-id-2" data-widget-editbutton="false" >
                <header>
                    <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                    <h2>Broker bonuses and Penalties</h2>
                </header>
                <div>
                    <div class="jarviswidget-editbox">
                    </div>
                    <button type="button" class="edit-node" id="addBtnPens" style="margin:0 0 20px 0;" data-toggle="modal" data-target="#myModal2">Add Penalties</button>
                    <div class="widget-body no-padding">
                        <table id="dt_basic3" class="table table-striped table-bordered table-hover" width="100%">
                            <thead>
                            <tr>
                                <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i> Penalty ID </th>
                                <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i> Order ID </th>
                                <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i> Sale Date </th>
                                <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i> Comment </th>
                                <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i> Penalty ammount </th>
                                <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i> Paid </th>
                                <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i> Delete </th>
                            </tr>
                            </thead>
                            <tbody>    

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </article>
    </div>

</section>
@stop

@section ('script')
<script>
    $(document).ready(function() {
        $(function() {
            $( "#filterDate" ).datepicker({ dateFormat: 'dd-mm-yy' });
            console.log("Hello");
        });
        $('#dt_basic').dataTable({
            serverSide: true,
            ajax: {
                url: "/{{ Config::get('bondcms.admin_prefix') }}/finances/seller-liabilities/json/{{$id}}",
                type: 'POST',
                data: {
                    'dateFilter': function(){ return $('#filterDate').val()}
                },
            },
            order: [[0, 'ASC']],
            "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
                $('.delete-node', nRow).remove();
                $('#checkAll').prop("checked", false);
            }
        });
        $('#dt_basic2').dataTable({
            serverSide: true,
            ajax: {
                url: "/{{ Config::get('bondcms.admin_prefix') }}/finances/seller-liabilities/totals/{{$id}}",
                type: 'POST',
                data: {
                    'dateFilter': function(){ return $('#filterDate').val()}
                },
            },
            order: [[0, 'ASC']],
            "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
                $('.delete-node', nRow).remove();
            }
        });
        $('#dt_basic3').dataTable({
            serverSide: true,
            ajax: {
                url: "/{{ Config::get('bondcms.admin_prefix') }}/finances/seller-liabilities/penalties/{{$id}}",
                type: 'POST',
                data: {
                    'dateFilter': function(){ return $('#filterDate').val()}
                },
            },
            order: [[0, 'ASC']],
            "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
                $('.delete-node', nRow).remove();
            }
        });
        $('#checkAll').change(function() {
            if($(this).is(":checked")) {
                $('.paidCheckboxs').prop("checked", true);
            }
            else{
                $('.paidCheckboxs').prop("checked", false);
            }      
        }); 
        $('#saveBtn').click(function(){
            var ids = new Array();

            $('.paidCheckboxs').each(function( index ) {
                if($(this).prop("checked"))
                {
                    ids.push($(this).val());
                }
            });
            postPaidStatusChangeMulti(ids);
        });
        $('#filterDate').change(function(){
            var table = $('#dt_basic').DataTable();
            table.ajax.reload();
            var table = $('#dt_basic2').DataTable();
            table.ajax.reload();
            var table = $('#dt_basic3').DataTable();
            table.ajax.reload();
        });

        $('#addPenForm').submit(function(event){
            event.preventDefault();
            var data = {
                uID : {{$id}},
                order_id : function(){ return $('#aPOID').val()},
                ammount: function(){ return $('#aPAm').val()},
                comment: function(){ return $('#aPCom').val()}
            };

            $.ajax({
                type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
                url         : "/{{ Config::get('bondcms.admin_prefix') }}/finances/seller-liabilities/penalties/add/{{$id}}", // the url where we want to POST
                data        : data, // our data object
                dataType    : 'json', // what type of data do we expect back from the server
                encode      : true,

                success: function(response){
                    var table = $('#dt_basic').DataTable();
                    table.ajax.reload();
                    var table = $('#dt_basic2').DataTable();
                    table.ajax.reload();
                    var table = $('#dt_basic3').DataTable();
                    table.ajax.reload();
                    $('#myModal2').modal({"show":"false"});
                    alert("Success");
                },
                error: function(response){
                    alert("Error: check console log.");
                    console.log(response);
                }
            })
        });

    });

    function deletePenaltyById(id){
        $.ajax({
            type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
            url         : "/{{ Config::get('bondcms.admin_prefix') }}/finances/seller-liabilities/penalties/delete/"+id.toString(), // the url where we want to POST
            dataType    : 'json', // what type of data do we expect back from the server
            encode      : true,

            success: function(response){
                var table = $('#dt_basic').DataTable();
                table.ajax.reload();
                var table = $('#dt_basic2').DataTable();
                table.ajax.reload();
                var table = $('#dt_basic3').DataTable();
                table.ajax.reload();
                $('#myModal2').modal({"show":"false"});
                alert("Success");
            },
            error: function(response){
                console.log(response);
            }
        })
    }

    function postShowFullInfo(order_id)
    {
        var urlTo = "/{{ Config::get('bondcms.admin_prefix') }}/sold-tickets/full-details/"+order_id;
        $.ajax({
            url:urlTo,
            type:'POST',
            success: function(response){
                $('#fill-this').html(response);
                $('#myModal').modal({"show":"true"});
            },
            error: function(response){
                console.log(response);
            }
        })
    }

    function postPaidStatusChange(id, sel){
        var data = {
            payment_status: sel.value
        };

        var urlTo="/{{ Config::get('bondcms.admin_prefix') }}/sold-tickets/update/payment-status/"+id;
        $.ajax({
            url: urlTo,
            data: data,
            type: 'POST',
            success: function (response) {
                alert(response);
                var table = $('#dt_basic').DataTable();
                table.ajax.reload();
                var table = $('#dt_basic2').DataTable();
                table.ajax.reload();
            },
            error: function (response) {
                console.log(response);
            }
        })
    }

    function postPaidStatusChangeMulti(ids){
        var ahn = prompt("Account Holder Name", "{{$acc_holder_ph}}");
        if(ahn === null){return;}

        var iban = prompt("IBAN", "{{$iban_ph}}");
        if(iban === null){return;}

        var swift = prompt("SWIFT", "{{$bic_swift_ph}}");
        if(swift === null){return;}

        var acc_num = prompt("Account Number");
        if(acc_num === null){return;}

        var sortcode = prompt("Sort Code");
        if(sortcode === null){return;}

        var note = prompt("Note");
        if(note === null){return;}

        var date_paid = prompt("Date Paid");
        if(date_paid === null){return;}

        var urlTo="/{{ Config::get('bondcms.admin_prefix') }}/finances/seller-liabilities/update-paid-multi";
        var data = {
            id_array: ids,
            ahn : ahn,
            iban : iban,
            swift : swift,
            acc_num : acc_num,
            sortcode : sortcode,
            note : note,
            date_paid : date_paid
        };

        $.ajax({
            url: urlTo,
            data: data,
            type: 'POST',
            success: function (response) {
                alert(response);
                var table = $('#dt_basic').DataTable();
                table.ajax.reload();
                var table = $('#dt_basic2').DataTable();
                table.ajax.reload();
            },
            error: function (response) {
                console.log(response);
            }  
        });
    }
</script>
@stop


