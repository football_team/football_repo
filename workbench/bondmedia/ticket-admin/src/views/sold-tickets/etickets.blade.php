@extends('backend/_layout/layout')
@section('breadcrumb')
<li>Home</li><li>Tickets</li>
@stop

{{
    Assets::setScripts([
        'jquery.dataTables'         => 'js/plugin/datatables/jquery.dataTables.min.js',
        'dataTables.colVis'         => 'js/plugin/datatables/dataTables.colVis.min.js',
        'dataTables.tableTools'     => 'js/plugin/datatables/dataTables.tableTools.min.js',
        'dataTables.bootstrap'      => 'js/plugin/datatables/dataTables.bootstrap.min.js',
    ], true);
}}

@section('content')
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">E-Ticket Actions</h4>
      </div>
      <div class="modal-body" id="fill-this">
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<style>
.Buyer td:first-child, .Seller td:first-child, .Order td:first-child{
    font-family: Arial;
    font-weight: bold;
}
</style>
<div class="row">
    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
        <h1 class="page-title txt-color-blueDark">
            <i class="fa fa-table fa-fw "></i>
            E-Tickets
        </h1>
    </div>

</div>
{{ Notification::showAll() }}
<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- row -->
    <div class="row">

        <!-- NEW WIDGET START -->
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false" >
                <header>
                    <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                    <h2>E-Tickets</h2>
                </header>
                <div>
                    <div class="jarviswidget-editbox">
                    </div>
                    <div class="widget-body no-padding">
                        <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                            <thead>
                            <tr>
                                <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i> Order Number </th>
                                <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i> Buyer Name </th>
                                <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i> Ticket Type </th>
                                <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i> Block </th>
                                <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i> Row </th>
                                <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i> Seller Name </th>
                                <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i> Qty </th>
                                <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i> Event Title </th>
                                <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i> Event Date </th>
                                <th>View</th>
                            </tr>
                            </thead>
                            <tbody>    

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </article>
    </div>
</section>


<style>
    .modal-body table tbody tr td {
        color: #333;
        padding: 9px 10px;
        vertical-align: top;
        border: none;
    }

    .modal-body table tbody tr:nth-child(2n) {
        background: none repeat scroll 0 0 #f2f2f2;
        border-top: 1px solid #e6e6e6;
    }
</style>
@stop

@section ('script')
<script>
    $(document).ready(function() {
        $('#dt_basic').dataTable({
            serverSide: true,
            ajax: {
                url: "/admin/sold-tickets/etickets/list.json",
                type: 'POST',
                data: {
                    'filterByDate': function(){ return $('#futureFilter').is(':checked')},
                    'filterByPaid': function(){ return $('#filterPaidValue').val();
                    },
                },
            },
            order: [[2, 'ASC']],
            "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
            }

        });
    });


</script>

<script>
function postShowFullInfo(order_id)
{
    console.log("here2");
    var urlTo = "/{{ Config::get('bondcms.admin_prefix') }}/sold-tickets/full-details/"+order_id;
    $.ajax({
        url:urlTo,
        type:'POST',
        success: function(response){
            $('#fill-this').html(response);
        },
        error: function(response){
            console.log(response);
        }
    })
}

$('body').on('click', '.viewA', function(e){
    e.preventDefault();
    id = $(this).data('id');

    var urlTo = "/admin/sold-tickets/etickets/approve-view/"+id;
    $.ajax({
        url:urlTo,
        type:'POST',
        success: function(response){
            $('#fill-this').html(response);
            $('#myModal').modal({"show":"true"});
        },
        error: function(response){
            console.log(response);
        }
    })
});

$('body').on('submit', '.approveForm', function(e){
    e.preventDefault();
    console.log("WHEEEY");
    var data = $(this).serialize();
    console.log(data);

    var urlTo = "/admin/sold-tickets/etickets/actions";
    $.ajax({
        url:urlTo,
        data:data,
        type:'POST',
        success: function(response){
            
            $('#fill-this').html("");
            $('#fill-this').html(response);
        },
        error: function(response){
            alert("There was an error");
        }
    })
});
</script>
@stop


