@extends('backend/_layout/layout')
@section('breadcrumb')
    <li>Home</li><li>Tickets</li>
@stop

{{
    Assets::setScripts([
        'jquery.dataTables'         => 'js/plugin/datatables/jquery.dataTables.min.js',
        'dataTables.colVis'         => 'js/plugin/datatables/dataTables.colVis.min.js',
        'dataTables.tableTools'     => 'js/plugin/datatables/dataTables.tableTools.min.js',
        'dataTables.bootstrap'      => 'js/plugin/datatables/dataTables.bootstrap.min.js',
    ], true);
}}

@section('content')
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Reacquire</h4>
                </div>
                <div class="modal-body" id="fill-this">
                    <select name="bselect" id="broker-reacq" data-cur="">
                        <option value="-" selected>--</option>
                        @foreach($brokers as $broker)
                            <option value="{{$broker->user_id}}">{{$broker->firstname}} {{$broker->lastname}}</option>
                        @endforeach
                    </select>
                    <button id="subBrokerReacq">Reacquire</button>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
    <style>
        .Buyer td:first-child, .Seller td:first-child, .Order td:first-child{
            font-family: Arial;
            font-weight: bold;
        }
    </style>
    <div class="row">
        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
            <h1 class="page-title txt-color-blueDark">
                <i class="fa fa-table fa-fw "></i>
                Deferred Orders
            </h1>
        </div>

    </div>
    {{ Notification::showAll() }}
            <!-- widget grid -->
    <section id="widget-grid" class="">

        <!-- row -->
        <div class="row">

            <!-- NEW WIDGET START -->
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false" >
                    <header>
                        <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                        <h2>Deferred Orders</h2>
                    </header>
                    <div>
                        <div class="widget-body no-padding">
                            <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                                <thead>
                                <tr>
                                    <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i> Game </th>
                                    <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i> Event Date </th>
                                    <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i> Order Date </th>
                                    <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i> Order/Transaction ID </th>
                                    <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i> Seller Name </th>
                                    <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i> Seller Type </th>
                                    <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i> Number of Tickets </th>
                                    <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i> Total Price </th>
                                    <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i> Form of Ticket </th>
                                    <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i> Ticket Location </th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </article>
        </div>
    </section>
@stop

@section ('script')
    <script>
        $(document).ready(function() {
            $('#dt_basic').dataTable({
                serverSide: true,
                ajax: {
                    url: "/{{ Config::get('bondcms.admin_prefix') }}/tickets/deferred-orders/list.json",
                    type: 'POST',
                },
                order: [[3, 'ASC']],
                "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
                    $('.delete-node', nRow).remove();
                }
            });
        });

        $(function(){
            $('body').on('click','.release-button',function(){
                var oid = $(this).data('id');
                if(window.confirm("Are you sure you would like to release the order:"+oid+" back to the broker?"))
                {
                    $.ajax({
                        url:'/admin/tickets/deferred-orders/release/'+oid,
                        type:"POST",
                        success:function(response){
                            var table = $('#dt_basic').DataTable();
                            table.ajax.reload();
                        },
                        error:function(response){
                            alert("There was an error. Please contact Byron with enough scorn to scare him into fixing the issue (he's a big baby just say he has fat jowls, that will crush his spirit).");
                        }
                    })
                }
            });

            $('body').on('click', '.reacq-button', function(){
                var oid = $(this).data('id');
                $('#broker-reacq').data('cur', oid);

                $('#myModal').modal({"show":"true"});
            });

            $('#subBrokerReacq').click(function(){
                var oid = $('#broker-reacq').data('cur');
                var broker_id = $('#broker-reacq').val();
                console.log(oid);
                if(broker_id !="-")
                {
                    $.ajax({
                        url:'/admin/tickets/deferred-orders/reacquire/'+oid,
                        type:"POST",
                        data:{
                            broker_id:broker_id
                        },
                        success:function(){
                            $('#myModal').modal({"show":"false"});
                            var table = $('#dt_basic').DataTable();
                            table.ajax.reload();
                        },
                        error:function(){
                            alert("nope");
                        }
                    })
                }
            });
        })
    </script>

    <script>


    </script>
@stop


