@extends('backend/_layout/layout')
@section('breadcrumb')
<li>Home</li><li>Customer</li><li>Edit</li>
@stop

{{
    Assets::setScripts([
    'dataTables.bootstrap'      => 'js/plugin/datatables/dataTables.bootstrap.min.js',
    ], true);
}}

@section('content')

<div class="row">
    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
        <h1 class="page-title txt-color-blueDark">
            <i class="fa fa-table fa-fw "></i>
            Ticket
        </h1>
    </div>

</div>
{{ Notification::showAll() }}
<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- row -->
    <div class="row">
        <!-- NEW WIDGET START -->
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false">
                <header>
                    <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                    <h2>Game # {{$node->title}} </h2>
                </header>
                <div>
                    <div class="jarviswidget-editbox">
                    </div>
                <form name="customer_update" action="{{ route('admin.sold-tickets.info.update', array('id'=>$node->id)) }}" method="post">
                        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                        <input name="event_id" value="{{ $node->event_id }}" type="hidden">
                        <input name="product_id" value="{{ $node->product_id }}" type="hidden">
                        <input name="order_id" value="{{ $node->order_id }}" type="hidden">
                        <input name="seller_id" value="{{ $node->seller_id }}" type="hidden">
                        <input name="sender_email" value="{{$node->seller_email}}" type="hidden">
                        <div class="widget-body">
                            <div class="row">
                                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">Game</div>
                                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">{{ $node->title }}</div>
                                <input name="game" value="{{ $node->title }}" type="hidden">
                            </div>

                            <div class="row">
                                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">Date</div>
                                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">{{ $node->event_date }}</div>
                            </div>

                            <div class="row">
                                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">Seller First Name</div>
                                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3"> {{ $node->seller_firstname }}</div>
                            </div>

                            <div class="row">
                                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">Seller Last Name</div>
                                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">{{ $node->seller_lastname }}</div>
                            </div>

                            <div class="row">
                                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">Ticket Qty</div>
                                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">{{$node->qty }}</div>
                            </div>

                            <div class="row">
                                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">Delivery Status</div>
                                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">{{$node->delivery_status }}</div>

                            </div>
                            @if(isset($tracking->tracking_code))
                            <div class="row">
                                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">Tracking Code</div>
                                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">{{$tracking->tracking_code }}</div>
                            </div>
                            @endif
                            <div class="row">
                                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3"> Commission ({{$commission}}%) </div>
                                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3"> &pound;{{ number_format($ourCommission, 2) }} </div>
                            </div>



                            <div class="row">
                                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">Total Amount payable to seller</div>
                                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3"> &pound;{{ number_format( $totalSellerAmount, 2) }} </div>

                            </div>
                            <div class="row">
                                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">Account payable to: </div>
                                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3"> {{$payment_method}} </div>

                            </div>
                            <div class="row">
                                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">Payment Status</div>
                                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                                    <select name="payment_status">
                                       @foreach($payment_status as $status)
                                            @if($status == $node->payment_status)
                                            <option value="{{$status}}" selected>{{$status}}</option>
                                            @else
                                            <option value="{{$status}}">{{$status}}</option>
                                            @endif
                                       @endforeach
                                    </select>
                                </div>
                            </div>



                            <div class="row">
                                <div class="col-xs-3 col-sm-3col-md-3 col-lg-3">
                                    <input name="commission_amount" value="{{ $ourCommission }}" type="hidden">
                                    <input name="amount_paid" value="{{ $totalSellerAmount }}" type="hidden">
                                    <input type="submit" class="btn btn-info" value="Save">
                                </div>
                            </div>
                        </div>

                </form>
                </div>
                </div>
            </div>
        </article>
    </div>
</section>
@stop



