<h2>Verify e-tickets for order: {{$order_id}}</h2>
{{ Form::open(array('url'=>'/admin/sold-tickets/etickets/actions', 'class'=>'approveForm', 'data-id'=>$order_id)) }}
<table style="width:100%;">
	@foreach($tickets as $ticket)
	<tr>
		<td><a href="/admin/sold-tickets/etickets/view/{{$ticket->id}}" target="_blank">Download</a></td>
		<td>{{$ticket->created}}</td>
		<td>@if($ticket->available) {{"Released"}} @else {{"Verifying"}} @endif</td>
		<td>{{Form::select($ticket->id, array('D' => 'Delete', 'A' => 'Approve', 'N'=>'None'), 'N')}}</td>	
	</tr>
	@endforeach
	<tr>
		<td></td>
		<td></td>
		<td></td>
		<td>{{ Form::submit('Update') }}</td>
	</tr>
</table>
<br>
{{ Form::close() }}


{{ Form::open(array('url'=>'/admin/sold-tickets/etickets/upload/'.$order_id,'files'=>true, 'class'=>'uploadForm', 'data-id'=>$order_id )) }}
<table>	
	<tr>
		<td>{{ Form::label('file','File',array('id'=>'','class'=>'')) }}</td>
		<td>{{ Form::file('files[]', array('multiple'=>true)); }}</td>
		<td>{{ Form::submit('Save') }}</td>
		<td>{{ Form::reset('Reset') }}</td>
	</tr>
</table>
{{ Form::close() }}