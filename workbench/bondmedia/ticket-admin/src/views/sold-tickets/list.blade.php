@extends('backend/_layout/layout')
@section('breadcrumb')
<li>Home</li><li>Tickets</li>
@stop

{{
    Assets::setScripts([
        'jquery.dataTables'         => 'js/plugin/datatables/jquery.dataTables.min.js',
        'dataTables.colVis'         => 'js/plugin/datatables/dataTables.colVis.min.js',
        'dataTables.tableTools'     => 'js/plugin/datatables/dataTables.tableTools.min.js',
        'dataTables.bootstrap'      => 'js/plugin/datatables/dataTables.bootstrap.min.js',
    ], true);
}}

@section('content')
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Full order details</h4>
      </div>
      <div class="modal-body" id="fill-this">
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<style>
.Buyer td:first-child, .Seller td:first-child, .Order td:first-child{
    font-family: Arial;
    font-weight: bold;
}
</style>
<div class="row">
    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
        <h1 class="page-title txt-color-blueDark">
            <i class="fa fa-table fa-fw "></i>
            Sold Tickets
        </h1>
    </div>

</div>
{{ Notification::showAll() }}
<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- row -->
    <div class="row">

        <!-- NEW WIDGET START -->
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false" >
                <header>
                    <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                    <h2>Sold Tickets</h2>
                </header>
                <div>
                    <div class="jarviswidget-editbox">
                    </div>
                    <div class="box-header">
                        <h3 class="box-title">
                            Advanced Search
                        </h3>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-xs-12">
                                <input type="text" class="form-control pull-right" value="{{$dateRange}}" id="reservation">
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-xs-12 filter-buttons">
                                <button class="btn adv-sh" id="adv-sh"><span class="fa fa-search" ></span></button>
                            </div>
                        </div>
                        <br>
                        <div class="row" id="adv-search-row">
                            <div class="col-xs-12 adv-search-fields">
                                <div class="row search-fields">
                                    <div class="col-sm-6">
                                        Buyer First Name:<br> <input type="text" data-name="bfn">
                                    </div>
                                    <div class="col-sm-6">
                                        Buyer Second Name:<br> <input type="text" data-name="bln">
                                    </div>
                                    <div class="col-sm-6">
                                        Buyer Email:<br> <input type="text" data-name="bemail">
                                    </div>
                                    <div class="col-sm-6">
                                        Order Number:<br> <input type="text" data-name="sOn">
                                    </div>

                                    <div class="col-sm-6">
                                        Seller First Name:<br> <input type="text" data-name="sfn">
                                    </div>
                                    <div class="col-sm-6">
                                        Seller Second Name:<br> <input type="text" data-name="sln">
                                    </div>
                                    <div class="col-sm-6">
                                        Seller Email:<br> <input type="text" data-name="semail">
                                    </div>
                                    <div class="col-sm-6">
                                        Event Title:<br> <input type="text" data-name="evn">
                                    </div>
                                    <div class="col-sm-6">
                                        Ignore Date Range:<br> <input type="checkbox" data-name="swdr" value="true">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="myFilters" style="margin-bottom:20px;">
                    <label for="filterPaidOption">Only show payment status: </label>
                    <select id="filterPaidValue" name="filterPaidOption">
                        <option value="all" selected="selected">--</option>
                        <option value="paid">Paid</option>
                        <option value="pending">Pending</option>
                    </select>
                        <label for="filterDeliveryOption">Only show delivery status: </label>
                        <select id="filterDeliveryOption" name="filterDeliveryOption">
                            <option value="all" selected="selected">--</option>
                            <option value="delivered">Delivered</option>
                            <option value="pending">Pending</option>
                            <option value="in-transit">In-transit</option>
                            <option value="cancelled">Cancelled</option>
                        </select>
                    </div>
                    <div class="widget-body no-padding">
                        <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i> Game </th>
                                <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i> Date </th>
                                <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i> Order Date </th>
                                <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i> Order/Transaction ID </th>
                                <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i> Seller First Name </th>
                                <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i> Seller Last Name </th>
                                <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i> Payment status </th>
                                <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i> Delivery status </th>
                                <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i> Tracking Code </th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>    

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </article>
    </div>
</section>

<style>
    .search-fields input{
        width:100% !important;
        margin:5px 0 !important;
    }

    .adv-sh{
        padding:10px 15px !important;
    }

    .adv-sh.active{
        background-color:#c6a5f9 !important;
    }

    .search-fields{
        overflow:hidden;
        height:0px;
    }

    .search-fields.active{
        height:initial !important;
    }
</style>
@stop

@section ('script')
<script>
    $(document).ready(function() {
        $('#dt_basic').dataTable({
            serverSide: true,
            ajax: {
                url: "/{{ Config::get('bondcms.admin_prefix') }}/sold-tickets/list.json",
                type: 'POST',
                data: {
                    'filterByDelivery': function(){return $('#filterDeliveryOption').val()},
                    'filterByPaid': function(){ return $('#filterPaidValue').val()},
                    searchFilters: function(){
                        $sFields = $('.search-fields input');
                        var retArray = new Array();
                        if($('#adv-sh').hasClass('active')) {


                            $.each($sFields, function (index, value) {
                                var val = $(value).val();
                                var name = $(value).data('name');

                                if(name == 'swdr')
                                {
                                    if($(value).prop('checked') === true)
                                    {
                                        retArray.push(name + ":" + val);
                                    }
                                }
                                else if (val != "") {
                                    var n = val.indexOf(':');
                                    if(n == -1) {
                                        retArray.push(name + ":" + val);
                                    }
                                    else {
                                        alert("String contained : character. Has been ommited from search");
                                    }
                                }
                            });
                        }
                        return JSON.stringify(retArray);
                    },
                    daterange: function(){return $('#reservation').val()},
                },
            },
            order: [[2, 'ASC']],
            "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
                $('.delete-node', nRow).remove();

                if ( $(aData[8]).val() == "delivered" )
                {
                    $('td', nRow).css('background-color', '#CEFFB9');
                }
                else if ( $(aData[8]).val() == "in-transit" )
                {
                    $('td', nRow).css('background-color', '#FEFC7C');
                }
                else if ( $(aData[8]).val() == "pending")
                {
                    $('td', nRow).css('background-color', '#FECA7C');
                }
            }

        });

        var edit_page_url = "sold-tickets/edit/";

        $('#dt_basic_wrapper').on('click', '.edit-node', function (e) {
            e.preventDefault();
            var page_id = $(this).attr('href');
            window.location = edit_page_url+page_id;
        });

        $('.adv-sh').click(function(){
           $(this).toggleClass('active');
            $('.search-fields').toggleClass('active');

            var table = $('#dt_basic').DataTable();
            table.ajax.reload();
        });

        $('#reservation').daterangepicker({
            format: 'DD/MM/YYYY'
        });
    });
</script>

<script>
function postDelStatusChange(n, sel){
    var urlTo = "/{{ Config::get('bondcms.admin_prefix') }}/sold-tickets/update/status/";
    var data = {
        order_id: n,
        status: sel.value
    };

    $.ajax({
        url: urlTo,
        data: data,
        type: 'POST',
        success: function (response) {
            alert(response);
            var table = $('#dt_basic').DataTable();
            table.ajax.reload();

        },
        error: function (response) {
            alert("An error occurred: "+response);
        }
    })
}
function postTrackingChange(id, eventIn, sel){
    var tracking_number = prompt("Please enter the tracking number", "");
    var order_number = id;
    var product_id = id;

    var data = {
        order_id: id,
        event_id: eventIn,
        product_id: product_id,
        tracking_code: tracking_number
    };
    if (tracking_number) {
        var urlTo="/{{ Config::get('bondcms.admin_prefix') }}/sold-tickets/update/tracking-number/";
        $.ajax({
            url: urlTo,
            data: data,
            type: 'POST',
            success: function (response) {
                alert(response);
                var table = $('#dt_basic').DataTable();
     
                table.ajax.reload();
            },
            error: function (response) {
                console.log(response);
            }
        });
        return true;//do something
    }
    else{
        alert("Cannot submit an empty tracking number");
    }
}
function postPaidStatusChange(id, sel){
    console.log("here");
    var data = {
        payment_status: sel.value
    };

    var urlTo="/{{ Config::get('bondcms.admin_prefix') }}/sold-tickets/update/payment-status/"+id;
    $.ajax({
        url: urlTo,
        data: data,
        type: 'POST',
        success: function (response) {
            alert(response);
        },
        error: function (response) {
            console.log(response);
        }
    })
}

function postShowFullInfo(order_id)
{
    console.log("here2");
    var urlTo = "/{{ Config::get('bondcms.admin_prefix') }}/sold-tickets/full-details/"+order_id;
    $.ajax({
        url:urlTo,
        type:'POST',
        success: function(response){
            $('#fill-this').html(response);
            $('#myModal').modal({"show":"true"});
        },
        error: function(response){
            console.log(response);
        }
    })
}


$('#filterPaidValue').change(function(){
    var table = $('#dt_basic').DataTable();
    table.ajax.reload();
});

$('#filterDeliveryOption').change(function(){
    var table = $('#dt_basic').DataTable();
    table.ajax.reload();
});

$('.search-fields input').on('change', function(){
    var table = $('#dt_basic').DataTable();
    table.ajax.reload();
});

$('#reservation').on('change',function(){
    var table = $('#dt_basic').DataTable();
    table.ajax.reload();
});

</script>


<link rel="stylesheet" href="/assets/adminlte/plugins/daterangepicker/daterangepicker-bs3.css">
<script src="/assets/adminlte/bootstrap/js/bootstrap.min.js"></script>
<script src="/assets/adminlte/plugins/daterangepicker/moment.min.js"></script>
<script src="/assets/adminlte/plugins/daterangepicker/daterangepicker.js"></script>
@stop


