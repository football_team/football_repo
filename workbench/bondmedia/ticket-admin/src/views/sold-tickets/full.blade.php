<div>
    <div class="Buyer">
        <h2>Buyer info</h2>
        <table class="table-striped" style="width:100%;">
            <tr>
                <td>Name</td>
                <td>{{$return['buyer']['name']}}</td>
            </tr>
            <tr>
                <td>Delivery Address</td>
                <td>
                    {{$return['buyer']['address']['firstname']}} {{$return['buyer']['address']['lastname']}}<br>
                    {{$return['buyer']['address']['street']}}<br>
                    {{$return['buyer']['address']['city']}}<br>
                    {{$return['buyer']['address']['postcode']}}<br>
                    {{$return['buyer']['address']['country_id']}}<br>
                </td>
            </tr>
            <tr>
                <td>
                    Telephone
                </td>
                <td>
                    {{$return['buyer']['tel']}}
                </td>
            </tr>
            <tr>
                <td>
                    Email
                </td>
                <td>
                    {{$return['buyer']['email']}}
                </td>
            </tr>
        </table>
    </div>
    <h2>Seller Info</h2>
    <div class="Seller">
        <table class="table-striped" style="width:100%;">
            <tr>
                <td>Name</td>
                <td>{{$return['seller']['name']}}</td>
            </tr>
            <tr>
                <td>
                    Telephone
                </td>
                <td>
                    {{$return['seller']['tel']}}
                </td>
            </tr>
            <tr>
                <td>
                    Email
                </td>
                <td>
                    {{$return['seller']['email']}}
                </td>
            </tr>
            <tr>
                <td>
                    Seller earning
                </td>
                <td>
                    £{{$return['seller']['seller_returns']}}
                </td>
            </tr>
            <tr>
                <td>
                    Payment method
                </td>
                <td>
                    {{$return['seller']['payment_method']}}
                </td>
            </tr>
        </table>
        <br><br>
        <table class="table-striped" style="width:100%;">
            <tr>
                <th>Change seller account to broker</th>
                <th>Save</th>
            </tr>
            <tr>
                <td>
                    <select name="brokerSwap" id="brokerSwap">
                        @foreach ($return['brokers'] as $broker)
                            <option value='{{$broker->user_id}}'>{{$broker->firstname}} {{$broker->lastname}} - {{$broker->email}}</option>
                        @endforeach
                    </select>
                </td>
                <td>
                    <button class="btn" id="cSellerId">Save</button>
                </td>
            </tr>
        </table>
        <br><br>
        <table class="table-striped" style="width:100%;">
            <tr>
                <th>Change form of ticket</th>
                <th>Save</th>
            </tr>
            <tr>
                <td>
                    <select name="formID" id="formID">
                        @foreach ($return['forms'] as $form)
                            <option value='{{$form->id}}'>{{$form->title}}</option>
                        @endforeach
                    </select>
                </td>
                <td>
                    <button class="btn" id="cFormID">Save</button>
                </td>
            </tr>
        </table>
    </div >
    <h2>Order Info</h2>
    <div class="Order">
        <table class="table-striped" style="width:100%;">
            <tr>
                <td>
                    Event title
                </td>
                <td>
                    {{$return['order']['event_name']}}
                </td>
            </tr>
            <tr>
                <td>
                    Order date:
                </td>
                <td>
                    {{$return['order']['order_date']}}
                </td>
            </tr>
            <tr>
                <td>
                    Event date
                </td>
                <td>
                    {{$return['order']['event_date']}}
                </td>
            </tr>
            <tr>
                <td>
                    Quantity Ordered
                </td>
                <td>
                    {{$return['order']['qty']}}
                </td>
            </tr>
            <tr>
                <td>
                    Listed price
                </td>
                <td>
                    £{{$return['order']['listed_ammount']}}
                </td>
            </tr>
            <tr>
                <td>
                    Ticket type
                </td>
                <td>
                    {{$return['order']['ticket_type']}}
                </td>
            </tr>
            <tr>
                <td>
                    Ticket location
                </td>
                <td>
                    {{$return['order']['block']}} {{$return['order']['row']}}
                </td>
            </tr>
            <tr>
                <td>
                    Additional info
                </td>
                <td>
                    {{$return['order']['additional_info']}}
                </td>
            </tr>
            <tr>
                <td>
                    Restrictions
                </td>
                <td>
                    @if($return['order']['restrictions'])
                        @foreach($return['order']['restrictions'] as $val)
                            {{$val}}<br>
                        @endforeach
                    @endif
                </td>
            </tr>
            <tr>
                <td>
                    Total sale price
                </td>
                <td>
                    £{{$return['order']['total_revenue']}}
                </td>
            </tr>
            <tr>
                <td>
                    Commision
                </td>
                <td>
                    £{{$return['order']['commission']}}
                </td>
            </tr>
            <tr>
                <td>
                    Booking Fee
                </td>
                <td>
                    £{{$return['order']['booking_fee']}}
                </td>
            </tr>
            <tr>
                <td>
                    Tracking number
                </td>
                <td>
                    {{$return['order']['tracking']}}
                </td>
            </tr>
            <tr>
                <td>Referral Url</td>
                <td>{{$return['order']['referral_url']}}</td>
            </tr>
            <tr>
                <td>Site Url</td>
                <td>{{$return['order']['site_sold']}}</td>
            </tr>
        </table>    
    </div>
</div>
<script>
    $('#cSellerId').click(function(){
        var id = $('#brokerSwap').val();
        var name = $('#brokerSwap option:selected').text();
        var conf = confirm("Are you sure you want to change the seller account to: "+name);

        if(conf == true)
        {
            var urlTo = "/{{ Config::get('bondcms.admin_prefix') }}/repSeller";
            var data = {
                sid:"{{$return['order']['sid']}}",
                nsid: id
            };

            $.ajax({
                url: urlTo,
                data: data,
                type: 'POST',
                success: function (response) {
                    alert(response);
                },
                error: function (response) {
                    alert("An error occurred: "+response);
                }
            })
            var table = $('#dt_basic').DataTable();
            table.ajax.reload();
        }
    });

    $('#cFormID').click(function(){
       var id = $('#formID').val();

        var urlTo = "/{{ Config::get('bondcms.admin_prefix') }}/repFormOfTicket";
        var data = {
            sid:"{{$return['order']['sid']}}",
            fid: id
        };

        $.ajax({
            url: urlTo,
            data: data,
            type: 'POST',
            success: function (response) {
                alert(response);
            },
            error: function (response) {
                alert("An error occurred: "+response);
            }
        })
        var table = $('#dt_basic').DataTable();
        table.ajax.reload();
    });
</script>