@extends('backend/_layout/layout')
@section('breadcrumb')
<li>Home</li><li>Customer</li><li>Edit</li>
@stop

{{
    Assets::setScripts([
    'dataTables.bootstrap'      => 'js/plugin/datatables/dataTables.bootstrap.min.js',
    ], true);
}}

@section('content')

<div class="row">
    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
        <h1 class="page-title txt-color-blueDark">
            <i class="fa fa-table fa-fw "></i>
            Customer
        </h1>
    </div>

</div>
{{ Notification::showAll() }}
<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- row -->
    <div class="row">

        <!-- NEW WIDGET START -->
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false">
                <header>
                    <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                    <h2>Customers</h2>
                </header>
                <form name="customer_update" action="{{ route('admin.customer.info.update', array('id'=>$node->id)) }}" method="post">
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                <div>
                    <div class="jarviswidget-editbox">
                    </div>
                    <div class="widget-body">
                        <div class="row">
                            <div class="col-xs-3 col-sm-3col-md-3 col-lg-3">
                                <label> First Name: <strong >{{ $node->firstname }}</strong> </label>
                            </div>
                            <div class="col-xs-3 col-sm-3col-md-3 col-lg-3">
                                <label> Last Name: <strong >{{ $node->lastname }}</strong> </label>
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-xs-3 col-sm-3col-md-3 col-lg-3">
                                <label> Email: <strong >{{ $node->email }}</strong> </label>
                            </div>
                            <div class="col-xs-3 col-sm-3col-md-3 col-lg-3">
                                <label> Phone: <strong >{{ $node->telephone }}</strong> </label>
                            </div>


                        </div>

                        <div class="row">
                            <div class="col-xs-3 col-sm-3col-md-3 col-lg-3">
                                <label> Status: <select name="status">
                                        <option value="active" {{ $node->status == 'active'? 'selected': '' }} > Active </option>
                                        <option value="blocked" {{ $node->status == 'blocked'? 'selected': '' }} > Blocked </option>
                                    </select>
                                </label>
                            </div>
                            <div class="col-xs-3 col-sm-3col-md-3 col-lg-3">
                                <label> Type: <select name="type">
                                        <option value="customer" {{ $node->seller_type == 'customer'? 'selected': '' }} > Customer </option>
                                        <option value="seller" {{ $node->seller_type == 'seller'? 'selected': '' }} > Seller </option>
                                        <option value="broker" {{ $node->seller_type == 'broker'? 'selected': '' }} > Broker </option>
                                    </select>
                                </label>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-3 col-sm-3col-md-3 col-lg-3">
                                <input type="submit" class="btn btn-info" value="Save">
                            </div>

                        </div>
                    </div>
                </div>

                </div>
            </div>
        </article>
    </div>
</section>
@stop



