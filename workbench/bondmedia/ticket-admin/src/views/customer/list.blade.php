@extends('backend/_layout/layout')
@section('breadcrumb')
<li>Home</li><li>Customer</li>
@stop

{{
    Assets::setScripts([
        'jquery.dataTables'         => 'js/plugin/datatables/jquery.dataTables.min.js',
        'dataTables.colVis'         => 'js/plugin/datatables/dataTables.colVis.min.js',
        'dataTables.tableTools'     => 'js/plugin/datatables/dataTables.tableTools.min.js',
        'dataTables.bootstrap'      => 'js/plugin/datatables/dataTables.bootstrap.min.js',
    ], true);
}}

@section('content')

<div class="row">
    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
        <h1 class="page-title txt-color-blueDark">
            <i class="fa fa-table fa-fw "></i>
            Customers
        </h1>
    </div>

</div>
{{ Notification::showAll() }}
<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- row -->
    <div class="row">

        <!-- NEW WIDGET START -->
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false">
                <header>
                    <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                    <h2>Customers</h2>
                </header>
                <div>
                    <div class="jarviswidget-editbox">
                    </div>
                    <div class="widget-body no-padding">
                        <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th><i class="fa fa-fw fa-user text-muted hidden-md hidden-sm hidden-xs"></i> First Name</th>
                                <th><i class="fa fa-fw fa-user text-muted hidden-md hidden-sm hidden-xs"></i> Last Name</th>
                                <th><i class="fa fa-fw fa-phone text-muted hidden-md hidden-sm hidden-xs"></i> Telephone </th>
                                <th><i class="fa fa-fw fa-phone text-muted hidden-md hidden-sm hidden-xs"></i> Email </th>
                                <th><i class="fa fa-fw fa-phone text-muted hidden-md hidden-sm hidden-xs"></i> Type </th>
                                <th><i class="fa fa-fw fa-phone text-muted hidden-md hidden-sm hidden-xs"></i> Status </th>
                                <th><i class="fa fa-fw fa-calendar txt-color-blue hidden-md hidden-sm hidden-xs"></i>Updated at</th>
                                <th><i class="fa fa-fw fa-calendar txt-color-blue hidden-md hidden-sm hidden-xs"></i>Updated By</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </article>
    </div>
</section>
@stop

@section ('script')
<script>
    $(document).ready(function() {
        $('#dt_basic').dataTable({
            serverSide: true,
            ajax: {
                url: "/{{ Config::get('bondcms.admin_prefix') }}/customer/list.json",
                type: 'POST'
            },
            order: [[0, 'DESC']],
            "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
                $('.delete-node', nRow).remove();
            }

        });

        var edit_page_url = "customer/edit/";

        $('#dt_basic_wrapper').on('click', '.edit-node', function (e) {
            e.preventDefault();
            var page_id = $(this).attr('href');
            window.location = edit_page_url+page_id;
        });

    })
</script>
@stop


