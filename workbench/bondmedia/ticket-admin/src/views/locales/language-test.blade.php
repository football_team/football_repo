@extends('backend/_layout/layout')
@section('breadcrumb')
    <li>Home</li><li>Language Test</li>
@stop

{{
    Assets::setScripts([
        'jquery.dataTables'         => 'js/plugin/datatables/jquery.dataTables.min.js',
        'dataTables.colVis'         => 'js/plugin/datatables/dataTables.colVis.min.js',
        'dataTables.tableTools'     => 'js/plugin/datatables/dataTables.tableTools.min.js',
        'dataTables.bootstrap'      => 'js/plugin/datatables/dataTables.bootstrap.min.js',
    ], true);
}}

@section('content')
    {{ Notification::showAll() }}
    <section>
        <div class="row">
            <!-- NEW WIDGET START -->
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false" >
                    <header>
                        <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                        <h2>API CHECK</h2>
                    </header>
                    <div>
                        <div class="widget-body" style="min-height:0px;">
                            API Key: <input type="text" id="api-test-key"> <button id="api-test-button">Test API</button>
                            <br><br><div id="api-info"></div>
                        </div>
                    </div>
                </div>
            </article>
        </div>
    </section>
    <section>
        <div class="row">
            <!-- NEW WIDGET START -->
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false" >
                    <header>
                        <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                        <h2>Language Tester</h2>
                    </header>
                    <div>
                        <div class="widget-body" style="min-height:0px;">
                            <table class="table table-striped table-bordered table-hover">
                                <tr>
                                    <td>
                                        Locale:
                                        <select name="" id="locale-sel">
                                            @foreach($locales as $locale)
                                                <option value="{{$locale->lang}}">{{$locale->title}}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                    <td>
                                        Language Expected: <input type="text" id="lang-input">
                                    </td>
                                    <td>
                                        Prefix:
                                        <select name="" id="prefix-sel">
                                            @foreach($prefix as $p)
                                                <option value="{{$p->prefix}}">{{$p->prefix}}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                    <td>
                                        Key: <input type="text" id="key-input">
                                    </td>
                                    <td>
                                        <button id="run-button">Run Test</button>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </article>
        </div>
    </section>

    <section>
        <div class="row">
            <!-- NEW WIDGET START -->
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false" >
                    <div>
                        <div class="widget-body" id="results" style="min-height:0px;">

                        </div>
                    </div>
                </div>
            </article>
        </div>
    </section>


@stop

@section ('script')
    <script>
        $(function(){
           $('#api-test-button').click(function(){
              var key = $('#api-test-key').val();

               $.ajax({
                   url:'/admin/language-tester/api-test',
                   type:'POST',
                   data:{
                       key:key
                   },
                   success:function(response){
                       $('#api-info').html(response);
                   },
                   error:function(response){

                   }
               })
           });

            $('#run-button').click(function(){
               var key = $('#key-input').val();
                var expected = $('#lang-input').val();
                var locale = $('#locale-sel').val();
                var prefix = $('#prefix-sel').val();

                $.ajax({
                    url:'/admin/language-tester/'+locale+'/'+expected+'/'+prefix+'/'+key,
                    type:'POST',
                    success:function(response){
                        $('#results').html(response);
                    },
                    error:function(response){

                    }
                })
            });
        });
    </script>
@stop