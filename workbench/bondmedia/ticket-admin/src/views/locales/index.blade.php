@extends('backend/_layout/layout')
@section('breadcrumb')
<li>Home</li><li>Locales</li>
@stop

{{
    Assets::setScripts([
        'jquery.dataTables'         => 'js/plugin/datatables/jquery.dataTables.min.js',
        'dataTables.colVis'         => 'js/plugin/datatables/dataTables.colVis.min.js',
        'dataTables.tableTools'     => 'js/plugin/datatables/dataTables.tableTools.min.js',
        'dataTables.bootstrap'      => 'js/plugin/datatables/dataTables.bootstrap.min.js',
    ], true);
}}

@section('content')
{{ Notification::showAll() }}
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Full order details</h4>
      </div>
      <div class="modal-body" id="fill-this">
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<h1>Please do not modify locale information unless you know what you are doing. Altering the lang field can ruin the front end translations for users. Altering the urls can do the same or present an incorrect locale to the user.</h1>
<section>
    <div class="row">
        <!-- NEW WIDGET START -->
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false" >
                <header>
                    <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                    <h2>Locales</h2>
                </header>
                <div>
                    <div class="jarviswidget-editbox">
                    </div>
                    <div class="widget-body no-padding">
                        <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                            <thead>
                            <tr>
                                <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i> ID </th>
                                <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i> Title </th>
                                <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i> Language short </th>
                                <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i> Default url </th>
                                <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i> &nbsp; </th>
                            </tr>
                            </thead>
                            <tbody>    

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </article>
    </div>
</section>
@stop

@section ('script')
<script>
    $('#dt_basic').dataTable({
        serverSide: true,
        ajax: {
            url: "/{{ Config::get('bondcms.admin_prefix') }}/locales/get-locales.json",
            type: 'POST',
        },
        order: [[0, 'ASC']],
        "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
            $('.delete-node', nRow).remove();
        }
    });

   function editLocale(id)
   {
        var urlTo = "/{{ Config::get('bondcms.admin_prefix') }}/locales/full-details/"+id;
        $.ajax({
            url:urlTo,
            type:'POST',
            success: function(response){
                $('#fill-this').html(response);
                $('#myModal').modal({"show":"true"});
            },
            error: function(response){
                console.log(response);
            }
        })
   }

</script>
@stop