<table class="table table-striped table-bordered table-hover">
    <thead>
        <tr>
            <td>
                ID
            </td>
            <td>
                Translation
            </td>
            <td>
                Results
            </td>
        </tr>
    </thead>
    <tbody>
        @foreach($fails as $fail)
            <tr>
                <td>
                    {{$fail['identifier']}}
                </td>
                <td>
                    {{{ $fail['translation'] }}}
                </td>
                <td>
                    @if(isset($fail['others']) && !empty($fail['others']))
                        @foreach($fail['others'] as $other)
                            Detected Language: {{$other['language']}}, guess is @if(!$other['is_reliable']) NOT @endif reliable with a score of {{$other['score']}} <br>
                        @endforeach
                    @endif
                </td>
            </tr>
        @endforeach
    </tbody>
</table>