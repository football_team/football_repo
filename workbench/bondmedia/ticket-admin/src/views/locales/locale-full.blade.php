<div>
    <div class="Buyer">
        <h2>Buyer info</h2>
        <table class="table-striped" style="width:100%;">
            <tr>
                <td>
                    ID
                </td>
                <td>
                    {{$data['id']}}
                </td>
            </tr>
            <tr>
                <td>
                    Title
                </td>
                <td>
                    <input id="ttl_input" type = "text" value="{{$data['title']}}" />
                </td>
            </tr>
            <tr>
                <td>
                    Lang
                </td>
                <td>
                    <input id="lang_input" type = "text" value="{{$data['lang']}}" />
                </td>
            </tr>
            <tr>
                <td>
                    url
                </td>
                <td>
                    <input id="url_input" type = "text" value="{{$data['url']}}" />
                </td>
            </tr>
            <tr>
                <td></td>
                <td><button id="save">Save</button></td>
            </tr>
        </table>
    </div>
</div>
<script>
    $('#save').click(function(){
        var ttl = $('#ttl_input').val();
        var lang = $('#lang_input').val();
        var url = $('#url_input').val();

        var conf = confirm("Are you sure you want to change the locale information?");

        if(conf == true)
        {
            var urlTo = "/{{ Config::get('bondcms.admin_prefix') }}/locales/update-locale/{{$data['id']}}";
            var data = {
                title: ttl,
                lang: lang,
                url : url
            };

            $.ajax({
                url: urlTo,
                data: data,
                type: 'POST',
                success: function (response) {
                    alert(response);
                },
                error: function (response) {
                    alert("An error occurred: "+response);
                }
            })
            var table = $('#dt_basic').DataTable();
            table.ajax.reload();
        }
    });
</script>