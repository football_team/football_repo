@extends('backend/_layout/layout')
@section('breadcrumb')
    <li>Home</li><li>Affiliates</li>
@stop

{{
    Assets::setScripts([
    'jquery.dataTables'         => 'js/plugin/datatables/jquery.dataTables.min.js',
    'dataTables.colVis'         => 'js/plugin/datatables/dataTables.colVis.min.js',
    'dataTables.tableTools'     => 'js/plugin/datatables/dataTables.tableTools.min.js',
    'dataTables.bootstrap'      => 'js/plugin/datatables/dataTables.bootstrap.min.js',
    ], true);
}}

@section('content')
    <section id="widget-grid" class="">
        <div class="row">
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false" >
                    <header>
                        <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                        <h2>Payment Requests</h2>
                    </header>
                    <div>
                        <div class="jarviswidget-editbox">
                        </div>
                        <div class="widget-body no-padding">
                            <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                                <thead>
                                <tr>
                                    <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i>ID</th>
                                    <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i>Company Name</th>
                                    <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i>Company URL</th>
                                    <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i>Email</th>
                                    <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i>Contact Name</th>
                                    <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i>Ref Code</th>
                                    <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i>Balance</th>
                                    <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i>Current Commission Percentage</th>
                                    <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i>Rate Name</th>
                                    <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i>Agent ID</th>
                                    <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i>Show Fees</th>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </article>
        </div>
    </section>
@stop

@section ('script')
    <script>
        $('#dt_basic').dataTable({
            serverSide: true,
            ajax: {
                url: "/{{ Config::get('bondcms.admin_prefix') }}/affiliates/properties/datatable",
                type: 'POST'
            },
            order: [[0, 'ASC']],
            "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
                $('.delete-node', nRow).remove();
                $('#checkAll').prop("checked", false);
            }
        });

        $('body').on('change', '.rateSelect',function(e){
            e.preventDefault();
            var val = $(this).val();
            var prop = $(this).data('id');

            $.ajax({
                url:"/{{ Config::get('bondcms.admin_prefix') }}/affiliates/properties/update-rate",
                type:"POST",
                data:{
                    property:prop,
                    newRate:val
                },
                success:function(response){
                    var table = $('#dt_basic').DataTable();
                    table.ajax.reload();
                },
                error:function(response){
                    alert(response.responseJSON);
                    console.log(response);
                }
            });
        })

        $('body').on('change', '.agent_assign', function(){
           var agent_id = $(this).val();
            var af_id = $(this).data('id');

            $.ajax({
                url:'/admin/affiliates/properties/update-agent/'+af_id,
                type:"POST",
                data:{
                    agent_id:agent_id
                },
                success:function(response){
                    var table = $('#dt_basic').DataTable();
                    table.ajax.reload();
                },
                error:function(response){
                    alert(response.responseText);
                }
            });

        });

        $('body').on('change', '.fees_toggle', function(){
            var id = $(this).data('id');
            var show_fees = $(this).val();

            $.ajax({
                url:'/admin/affiliates/properties/update-show-fees/'+id,
                type:'POST',
                data:{
                    show_fees:show_fees
                },
                success:function(response){
                    var table = $('#dt_basic').DataTable();
                    table.ajax.reload();
                },
                error:function(response){
                    alert(response.responseText);
                }
            });

        });
    </script>
@stop


