@extends('backend/_layout/layout')
@section('breadcrumb')
    <li>Home</li><li>Affiliates</li>
@stop

{{
    Assets::setScripts([
    'jquery.dataTables'         => 'js/plugin/datatables/jquery.dataTables.min.js',
    'dataTables.colVis'         => 'js/plugin/datatables/dataTables.colVis.min.js',
    'dataTables.tableTools'     => 'js/plugin/datatables/dataTables.tableTools.min.js',
    'dataTables.bootstrap'      => 'js/plugin/datatables/dataTables.bootstrap.min.js',
    'bootstrap-datepicker'      => 'js/plugin/bootstrap-datepicker/bootstrap-datepicker.js',
    'bootstrap-datepicker.tr'   => 'js/plugin/bootstrap-datepicker/locales/bootstrap-datepicker.tr.js',
    ], true);
}}

{{  Assets::setStyles([
    'datepicker'          => 'js/plugin/bootstrap-datepicker/datepicker.css'
], true) }}

@section('content')
    <section id="widget-grid" class="">
        <div class="col-sm-4 col-xs-12">
            Month Selector: <input type="text" id="datetime" value="{{$dt}}">
        </div>
        <br><br>
        <div class="row">
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false" >
                    <header>
                        <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                        <h2>Payment Referrals</h2>
                    </header>
                    <div>
                        <div class="jarviswidget-editbox">
                        </div>
                        <div class="widget-body no-padding">
                            <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                                <thead>
                                <tr>
                                    <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i>Affiliate ID</th>
                                    <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i>Name</th>
                                    <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i>Number of Referrals</th>
                                    <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i>Number of Conversions</th>
                                    <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i>Conversion Rate %</th>
                                    <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i>Actions</th>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </article>
        </div>
    </section>
@stop

@section ('script')
    <script>
        $('#datetime').datepicker({
            format: "yyyy-mm-dd"
        });

        $('#dt_basic').dataTable({
            serverSide: true,
            ajax: {
                url: "/{{ Config::get('bondcms.admin_prefix') }}/affiliates/referrals/data",
                type: 'POST',
                data:{
                    date:function(){return $('#datetime').val()}
                }
            },
            order: [[0, 'ASC']],
            "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
                $('.delete-node', nRow).remove();
                $('#checkAll').prop("checked", false);
            }
        });

        $('#datetime').change(function(){
            var table = $('#dt_basic').DataTable();
            table.ajax.reload();
        });
    </script>
@stop


