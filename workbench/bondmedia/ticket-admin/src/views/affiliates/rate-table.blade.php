<h2>{{$rates{0}->rate_name}}</h2>
<table class="table">
    <thead>
        <tr>
            <th>
                Sales Threshold
            </th>
            <th>
                Commission Percentage
            </th>
            <th>
                Update
            </th>
            <th>
                Delete
            </th>
        </tr>
    </thead>
    <tbody>
        @foreach($rates as $rate)
        <tr>
            <td><input type="number" min="0" id="rate-milestone-{{$rate->id}}" value="{{$rate->sale_milestone}}"></td>
            <td><input type="number" min="0" max="100" id="rate-perc-{{$rate->id}}" value="{{$rate->rate_perc}}"></td>
            <td><button data-id="{{$rate->id}}" class="rateUpdateFull">Update</button></td>
            <td><button data-id="{{$rate->id}}" class="rateDeleteFull">Delete</button></td>
        </tr>
        @endforeach
    </tbody>
</table>

<table>
    <thead>
    <tr>
        <th>
            Threshold
        </th>
        <th>
            Percentage
        </th>
        <th>

        </th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>
            <input type="number" min="0" id="addRateMilestone">
        </td>
        <td>
            <input type="number" min="0" max="100" id="addRatePerc">
        </td>
        <td>
            <button id="addNewRateFull" data-id="{{$rate->rate_id}}">Add</button>
        </td>
    </tr>
    </tbody>
</table>


