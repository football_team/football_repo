@extends('backend/_layout/layout')
@section('breadcrumb')
    <li>Home</li><li>Affiliates</li>
@stop

{{
    Assets::setScripts([
    'jquery.dataTables'         => 'js/plugin/datatables/jquery.dataTables.min.js',
    'dataTables.colVis'         => 'js/plugin/datatables/dataTables.colVis.min.js',
    'dataTables.tableTools'     => 'js/plugin/datatables/dataTables.tableTools.min.js',
    'dataTables.bootstrap'      => 'js/plugin/datatables/dataTables.bootstrap.min.js',
    ], true);
}}

@section('content')
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Payment Rate</h4>
                </div>
                <div class="modal-body" id="fill-this">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
    <div class="row">
        <div class="col-md-6 col-sm-12">
            <div role="widget" class="jarviswidget jarviswidget-sortable" id="wid-id-0">
                <header role="heading">
                    <h2>Configuration guide</h2>
                </header>
                <!-- widget div-->
                <div role="content">
                    <p>
                        This is the configuration section for the affiliate system.
                    </p>
                    <p>
                        min_withdrawal: This sets the minimum withdrawal amount required for an affiliate payment/withdrawal request. Must be a number and can only have up to 2 decimal places. £
                    </p>
                    <p>
                        payout_day: The day of the month you wish to make affiliate payments. Be careful when you change this, make sure it is a valid date for the upcoming month (i.e. 31 is invalid of it is september), ideally have it set at or below 28.
                    </p>
                    <p>
                        default_rate: The ID of the default commission rate new affiliates will be placed on.
                    </p>
                </div>
                <!-- end widget div -->
            </div>
        </div>
        <div class="col-md-6 col-sm-12">
            <div role="widget" class="jarviswidget jarviswidget-sortable" id="wid-id-0">
                <header role="heading">
                    <h2>Configuration</h2>
                </header>
                <!-- widget div-->
                <div role="content">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>
                                    Option
                                </th>
                                <th>
                                    Value
                                </th>
                                <th>
                                    Save
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($cfgs as $cfg)
                                <tr>
                                    <td>{{$cfg->key}}</td>
                                    <td><input type="number" autocomplete="off" min="1" id="key-{{$cfg->key}}" value="{{$cfg->value}}"></td>
                                    <td><button class="btn saveConfig" data-name="{{$cfg->key}}">Save</button></td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- end widget div -->
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 col-sm-12">
            <div role="widget" class="jarviswidget jarviswidget-sortable" id="wid-id-0">
                <header role="heading">
                    <h2>Commission Rates</h2>
                </header>
                <!-- widget div-->
                <div role="content">
                    <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                        <thead>
                            <tr>
                                <th>
                                    ID
                                </th>
                                <th>
                                    Rate Name
                                </th>
                                <th>
                                    Rate Usage
                                </th>
                                <th>
                                    Update
                                </th>
                                <th>
                                    Delete
                                </th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
                <!-- end widget div -->
            </div>
        </div>
        <div class="col-md-6 col-sm-12">
            <div role="widget" class="jarviswidget jarviswidget-sortable" id="wid-id-0">
                <header role="heading">
                    <h2>Add Rate</h2>
                </header>
                <!-- widget div-->
                <div role="content">
                    <table class="table table-striped table-bordered table-hover" width="100%">
                        <thead>
                        <tr>
                            <td>
                                Rate Name
                            </td>
                            <td>
                                Starting Percentage
                            </td>
                            <td>

                            </td>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>
                                <input type="text" id="newBaseRateName">
                            </td>
                            <td>
                                <input type="number" min="0" max="100" id="newBaseRatePercentage">
                            </td>
                            <td>
                                <button id="addNewBaseRate">Add</button>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <!-- end widget div -->
            </div>
        </div>
    </div>
@stop

@section ('script')
    <script>
        $('#dt_basic').dataTable({
            serverSide: true,
            ajax: {
                url: "/{{ Config::get('bondcms.admin_prefix') }}/affiliates/rates/list",
                type: 'POST'
            },
            order: [[0, 'ASC']],
            "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
                $('.delete-node', nRow).remove();
                $('#checkAll').prop("checked", false);
            }
        });

        $(function(){
            $('body').on('click', '.updateRate', function(e){
                e.preventDefault();

                var id  = $(this).data('id');

                var name = $('#rate-name-'+id).val();

                $.ajax({
                   url:"/{{ Config::get('bondcms.admin_prefix') }}/affiliates/rates/update-name",
                    type:"POST",
                    data:{
                      rateID:id,
                        rateName:name
                    },
                    success:function(response){
                        var table = $('#dt_basic').DataTable();
                        table.ajax.reload();
                    },
                    error:function(response){
                        alert("Could not save.");
                    }
                });
            });

            $('body').on('click', '.rateModalShow', function(e){
                e.preventDefault();

                var id = $(this).data('id');

                $.ajax({
                    url:"/{{ Config::get('bondcms.admin_prefix') }}/affiliates/rates/full",
                    type:"POST",
                    data:{
                        rateID:id
                    },
                    success:function(response){
                        $('#fill-this').html(response);
                        $('#myModal').modal({"show":"true"});
                    },
                    error:function(response){
                        alert(response.responseJSON);
                    }
                });
            })

            $('body').on('click', '.deleteRate', function(e){
                e.preventDefault();

                var id = $(this).data('id');

                $.ajax({
                   url:"/{{ Config::get('bondcms.admin_prefix') }}/affiliates/rates/delete",
                    type:"POST",
                    data:{
                        rateID:id
                    },
                    success:function(response){
                        var table = $('#dt_basic').DataTable();
                        table.ajax.reload();
                    },
                    error:function(response){
                       alert(response.responseJSON);
                    }
                });
            });

            $('.saveConfig').click(function(e){
                var key = $(this).data('name');
                var value = $('#key-'+key).val();

                console.log(value);

                if(!isNaN(value))
                {
                    $.ajax({
                        url:"/{{ Config::get('bondcms.admin_prefix') }}/affiliates/config/update",
                        type:"POST",
                        data:{
                            key:key,
                            value:value
                        },
                        success:function(response){
                            location.reload();
                        },
                        error:function(response){
                            alert("An error occured.");
                            console.log(response);
                        }
                    });
                }
                else
                {
                    alert("Value must be numeric.");
                }
            });

            $('body').on('click','.rateUpdateFull', function(e){
                var id = $(this).data('id');
                var milestone = $('#rate-milestone-'+id).val();
                var percentage = $('#rate-perc-'+id).val();

                $.ajax({
                    url:"/{{ Config::get('bondcms.admin_prefix') }}/affiliates/rates/update-full",
                    type:"POST",
                    data:{
                        milestone:milestone,
                        percentage:percentage,
                        id:id
                    },
                    success:function(response){
                        $('#fill-this').html(response);
                    },
                    error:function(response){
                        alert("An error occured.");
                        console.log(response);
                    }
                });
            });

            $('body').on('click','.rateDeleteFull', function(e){
                var id = $(this).data('id');

                $.ajax({
                    url:"/{{ Config::get('bondcms.admin_prefix') }}/affiliates/rates/delete-full",
                    type:"POST",
                    data:{
                        id:id
                    },
                    success:function(response){
                        $('#fill-this').html(response);
                    },
                    error:function(response){
                        alert("An error occured.");
                        console.log(response);
                    }
                });
            });

            $('body').on('click','#addNewRateFull', function(e){
                var id = $(this).data('id');
                var milestone = $('#addRateMilestone').val();
                var percentage = $('#addRatePerc').val();

                $.ajax({
                    url:"/{{ Config::get('bondcms.admin_prefix') }}/affiliates/rates/add-full",
                    type:"POST",
                    data:{
                        rate_id:id,
                        milestone:milestone,
                        percentage:percentage
                    },
                    success:function(response){
                        $('#fill-this').html(response);
                    },
                    error:function(response){
                        alert("An error occured.");
                        console.log(response);
                    }
                });
            });


            $('body').on('click','#addNewBaseRate', function(e){
                var name = $('#newBaseRateName').val();
                var perc = $('#newBaseRatePercentage').val();

                $.ajax({
                    url:"/{{ Config::get('bondcms.admin_prefix') }}/affiliates/rates/add-new-rate",
                    type:"POST",
                    data:{
                        name:name,
                        startRate:perc
                    },
                    success:function(response){
                        var table = $('#dt_basic').DataTable();
                        table.ajax.reload();
                    },
                    error:function(response){
                        alert(response.responseJSON);
                        console.log(response);
                    }
                });
            });

        });


    </script>
@stop


