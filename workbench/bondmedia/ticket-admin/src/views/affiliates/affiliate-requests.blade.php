@extends('backend/_layout/layout')
@section('breadcrumb')
<li>Home</li><li>Affiliates</li>
@stop

{{
    Assets::setScripts([
    'jquery.dataTables'         => 'js/plugin/datatables/jquery.dataTables.min.js',
    'dataTables.colVis'         => 'js/plugin/datatables/dataTables.colVis.min.js',
    'dataTables.tableTools'     => 'js/plugin/datatables/dataTables.tableTools.min.js',
    'dataTables.bootstrap'      => 'js/plugin/datatables/dataTables.bootstrap.min.js',
    ], true);
}}

@section('content')
<div class="row">
    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
        <h1 class="page-title txt-color-blueDark">
            <i class="fa fa-table fa-fw "></i>
            Affiliates - Affiliate Requests
        </h1>
    </div>

</div>

<section id="widget-grid" class="">
    <div class="row">
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="jarviswidget jarviswidget-color-darken" id="wid-id-1" data-widget-editbutton="false" >
                <header>
                    <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                    <h2>Unnaproved Requests</h2>
                </header>
                <div>
                    <div class="jarviswidget-editbox">
                    </div>
                    <div class="widget-body no-padding">
                        <table id="dt_basic2" class="table table-striped table-bordered table-hover" width="100%">
                            <thead>
                            <tr>
                                <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i>ID</th>
                                <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i>Company Name</th>
                                <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i>Company URL</th>
                                <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i>Contact Email</th>
                                <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i>Contact Name</th>
                                <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i>Add agent</th>
                                <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i>Actions</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </article>
    </div>

    <div class="row">
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false" >
                <header>
                    <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                    <h2>Unnactivated Affiliates</h2>
                </header>
                <div>
                    <div class="jarviswidget-editbox">
                    </div>
                    <div class="widget-body no-padding">
                        <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                            <thead>
                            <tr>
                                <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i>ID</th>
                                <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i>Company Name</th>
                                <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i>Company URL</th>
                                <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i>Contact Email</th>
                                <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i>Contact Name</th>
                                <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i>Verification Code</th>
                                <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i>Agent ID</th>
                                <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i>Actions</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </article>
    </div>
</section>
@stop

@section ('script')
<script>
    $(document).ready(function() {

        $('#dt_basic').dataTable({
            serverSide: true,
            ajax: {
                url: "/{{ Config::get('bondcms.admin_prefix') }}/affiliates/requests/get-pending",
                type: 'POST',
                data: {
                    'dateFilter': function(){ return $('#filterDate').val()}
                },
            },
            order: [[0, 'ASC']],
            "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
                $('.delete-node', nRow).remove();
                $('#checkAll').prop("checked", false);
            }
        });

        $('#dt_basic2').dataTable({
            serverSide: true,
            ajax: {
                url: "/{{ Config::get('bondcms.admin_prefix') }}/affiliates/requests/get-unnaproved",
                type: 'POST',
                data: {
                    'dateFilter': function(){ return $('#filterDate').val()}
                },
            },
            order: [[0, 'ASC']],
            "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
                $('.delete-node', nRow).remove();
            }
        });

        $('body').on('click', '.approve-request', function(e){
            var id = $(this).data('id');
            var agent_id = $('#agent-assign-'+id).val();
            if(agent_id == "")
            {
                agent_id = "none";
            }
            if(confirm("Approve affiliate request with id: "+id))
            {
                $.ajax({
                    type:'POST',
                    url:"/{{ Config::get('bondcms.admin_prefix') }}/affiliates/requests/approve/"+id,
                    data:{
                      agent_id:agent_id
                    },
                    success:function(response){
                        var table = $('#dt_basic').DataTable();
                        table.ajax.reload();
                        var table = $('#dt_basic2').DataTable();
                        table.ajax.reload();
                    },
                    error:function(response){
                        //Inform of error

                    }
                });
            }
        });

        $('body').on('click', '.cancel-request', function(e){
            var id = $(this).data('id');
            if(confirm("Reject affiliate request with id: "+id))
            {
                $.ajax({
                    type:'POST',
                    url:"/{{ Config::get('bondcms.admin_prefix') }}/affiliates/requests/reject/"+id,
                    success:function(response){
                        var table = $('#dt_basic').DataTable();
                        table.ajax.reload();
                        var table = $('#dt_basic2').DataTable();
                        table.ajax.reload();
                    },
                    error:function(response){
                        //Inform of error

                    }
                });
            }
        });

        $('body').on('click', '.cancel-inactive', function(e){
            var id = $(this).data('id');
            if(confirm("Reject affiliate request with id: "+id))
            {
                $.ajax({
                    type:'POST',
                    url:"/{{ Config::get('bondcms.admin_prefix') }}/affiliates/requests/cancel/"+id,
                    success:function(response){
                        var table = $('#dt_basic').DataTable();
                        table.ajax.reload();
                        var table = $('#dt_basic2').DataTable();
                        table.ajax.reload();
                    },
                    error:function(response){
                        //Inform of error

                    }
                });
            }
        });

        $('body').on('click', '.skip-ver', function(){
            id = $(this).data('id');

            $.ajax({
                type:'post',
                url:"/admin/affiliates/skiptAfilliateVerification/"+id,
                success:function(response){
                    var table = $('#dt_basic').DataTable();
                    table.ajax.reload();
                    var table = $('#dt_basic2').DataTable();
                    table.ajax.reload();
                },
                error:function(response){
                    alert("Error:" + response);
                }
            })
        })

    });
</script>
@stop


