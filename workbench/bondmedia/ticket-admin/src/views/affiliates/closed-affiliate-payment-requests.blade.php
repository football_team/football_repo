@extends('backend/_layout/layout')
@section('breadcrumb')
    <li>Home</li><li>Affiliates</li>
@stop

{{
    Assets::setScripts([
    'jquery.dataTables'         => 'js/plugin/datatables/jquery.dataTables.min.js',
    'dataTables.colVis'         => 'js/plugin/datatables/dataTables.colVis.min.js',
    'dataTables.tableTools'     => 'js/plugin/datatables/dataTables.tableTools.min.js',
    'dataTables.bootstrap'      => 'js/plugin/datatables/dataTables.bootstrap.min.js',
    ], true);
}}

@section('content')
    <div class="row">
        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
            <h1 class="page-title txt-color-blueDark">
                <i class="fa fa-table fa-fw "></i>
                Affiliates -  Closed Affiliate Payment Requests
            </h1>
        </div>

    </div>

    <section id="widget-grid" class="">
        <div class="row">
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false" >
                    <header>
                        <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                        <h2>Paid Withdrawal Requests</h2>
                    </header>
                    <div>
                        <div class="jarviswidget-editbox">
                        </div>
                        <div class="widget-body no-padding">
                            <table id="dt_basic2" class="table table-striped table-bordered table-hover" width="100%">
                                <thead>
                                <tr>
                                    <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i>Request ID</th>
                                    <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i>Property Name</th>
                                    <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i>Requested For</th>
                                    <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i>Amount Paid</th>
                                    <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i>Date Paid</th>
                                    <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i>Account Holder Name</th>
                                    <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i>Account Number</th>
                                    <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i>Sort Code</th>
                                    <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i>SWIFT</th>
                                    <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i>IBAN</th>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </article>
        </div>
    </section>

    <section id="widget-grid" class="">
        <div class="row">
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false" >
                    <header>
                        <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                        <h2>Cancelled Withdrawal Requests</h2>
                    </header>
                    <div>
                        <div class="jarviswidget-editbox">
                        </div>
                        <div class="widget-body no-padding">
                            <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                                <thead>
                                <tr>
                                    <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i>Request ID</th>
                                    <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i>Property Name</th>
                                    <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i>Requested For</th>
                                    <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i>Amount Requested</th>
                                    <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i>Date Cancelled</th>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </article>
        </div>
    </section>
@stop

@section ('script')
    <script>
        $(document).ready(function() {

            $('#dt_basic').dataTable({
                serverSide: true,
                ajax: {
                    url: "/{{ Config::get('bondcms.admin_prefix') }}/affiliates/closed-payment-requests/cancelled",
                    type: 'POST',
                },
                order: [[0, 'ASC']],
                "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
                    $('.delete-node', nRow).remove();
                    $('#checkAll').prop("checked", false);
                }
            });

            $('#dt_basic2').dataTable({
                serverSide: true,
                ajax: {
                    url: "/{{ Config::get('bondcms.admin_prefix') }}/affiliates/closed-payment-requests/paid",
                    type: 'POST',
                },
                order: [[0, 'ASC']],
                "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
                    $('.delete-node', nRow).remove();
                    $('#checkAll').prop("checked", false);
                }
            });
        });
    </script>
@stop


