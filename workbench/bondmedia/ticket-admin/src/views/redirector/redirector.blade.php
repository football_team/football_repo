@extends('backend/_layout/layout')
@section('breadcrumb')
    <li>Home</li><li>Redirector</li>
@stop

{{
    Assets::setScripts([
    'jquery.dataTables'         => 'js/plugin/datatables/jquery.dataTables.min.js',
    'dataTables.colVis'         => 'js/plugin/datatables/dataTables.colVis.min.js',
    'dataTables.tableTools'     => 'js/plugin/datatables/dataTables.tableTools.min.js',
    'dataTables.bootstrap'      => 'js/plugin/datatables/dataTables.bootstrap.min.js',
    ], true);
}}

@section('content')
    <section id="widget-grid" class="">
        <div class="row">
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false" >
                    <header>
                        <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                        <h2>Redirects</h2>
                    </header>
                    <div>
                        <div class="jarviswidget-editbox">
                        </div>
                        <div class="widget-body no-padding">
                            <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                                <thead>
                                <tr>
                                    <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i>ID</th>
                                    <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i>From</th>
                                    <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i>To</th>
                                    <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i>Code</th>
                                    <th><i class="fa fa-fw text-muted hidden-md hidden-sm hidden-xs"></i>Actions</th>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </article>


            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false" >
                    <header>
                        <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                        <h2>Add new</h2>
                    </header>
                    <div>
                        <div class="jarviswidget-editbox">
                        </div>
                        <div class="widget-body">
                            <div class="row">
                                <div class="col-sm-3">From:<br><input type="text" id="new-from"></div>
                                <div class="col-sm-3">To:<br><input type="text" id="new-to"></div>
                                <div class="col-sm-3">Code:<br><input type="text" id="new-code"></div>
                                <div class="col-sm-3"><br><button class="save-new">Add</button></div>
                            </div>
                        </div>
                    </div>
                </div>
            </article>
        </div>
    </section>
@stop

@section ('script')
    <script>
        $('#dt_basic').dataTable({
            serverSide: true,
            ajax: {
                url: "/{{ Config::get('bondcms.admin_prefix') }}/redirector/datatable",
                type: 'POST'
            },
            order: [[0, 'ASC']],
            "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
                $('.delete-node', nRow).remove();
                $('#checkAll').prop("checked", false);
            }
        });

        $('body').on('click', '.save', function(){
            var id = $(this).data('id');

            var frm = $('#frm-'+id).val();
            var to = $('#to-'+id).val();
            var code = $('#code-'+id).val();

            console.log(frm);
            console.log(to);
            console.log(code);

            $.ajax({
                url:"/{{ Config::get('bondcms.admin_prefix') }}/redirector/update-rdr/"+id,
                type:"POST",
                data:{
                    from:frm,
                    to:to,
                    code:code
                },
                success:function(response){
                    var table = $('#dt_basic').DataTable();
                    table.ajax.reload();
                },
                error:function(response){
                    alert(response.responseJSON);
                    console.log(response);
                }
            });
        });

        $('body').on('click', '.delete', function(){
            var id = $(this).data('id');

            $.ajax({
                url:"/{{ Config::get('bondcms.admin_prefix') }}/redirector/delete/"+id,
                type:"POST",
                data:{
                },
                success:function(response){
                    var table = $('#dt_basic').DataTable();
                    table.ajax.reload();
                },
                error:function(response){
                    alert(response.responseJSON);
                    console.log(response);
                }
            });
        });

        $('.save-new').click(function(){
            var from = $('#new-from').val();
            var to = $('#new-to').val();
            var code = $('#new-code').val();

            $.ajax({
                url:"/{{ Config::get('bondcms.admin_prefix') }}/redirector/add/",
                type:"POST",
                data:{
                    from:from,
                    to:to,
                    code:code
                },
                success:function(response){
                    var table = $('#dt_basic').DataTable();
                    table.ajax.reload();
                },
                error:function(response){
                    alert(response.responseJSON);
                    console.log(response);
                }
            });
        });
    </script>
@stop


