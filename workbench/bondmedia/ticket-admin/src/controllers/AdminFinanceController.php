<?php
use Bond\Traits\GridPagination;
use Illuminate\Database\Schema\Blueprint;

class AdminFinanceController extends \BaseController {
    use GridPagination;

    public function index()
    {
        View::share('menu', 'finances');
        $today = new DateTime();
        View::share('todaysDate', $today->format('d-m-Y'));
        return View::make('ticketAdmin::finances.list-liabilities');
    }

    public function listLiabilitiesByMonth()
    {  
        $start = 0;
        $limit = 10;
        if(Input::has('start'))
        {
            $start = Input::get('start');
        }
        if(Input::has('length'))
        {
            $limit = Input::get('length');
        }
        if(Input::has('dateFilter'))
        {
            $dateFilter = Input::get('dateFilter');
            if($dateFilter != "")
            {
                $dateFilter = DateTime::createFromFormat('d-m-Y', $dateFilter);
            }
            else
            {
                $dateFilter = new DateTime();
            }
        }
        else
        {
            $dateFilter = new DateTime();
        }

        $selMonth = $dateFilter->format('m');
        $selYear =  $dateFilter->format('Y');
        $total = DB::table('sales')
                    ->select(DB::raw("COUNT(DISTINCT seller_id) as total"))
                    ->where('delivery_status', 'delivered')
                    ->where('payment_status', '=', 'pending')
                    ->first();

        $total = $total->total;
        $rs = DB::table('sales')
            ->select(
                'seller_id', 
                DB::raw('CONCAT_WS(" ", seller_firstname, seller_lastname) as seller_name'),
                DB::raw(
                    "SUM(
                        IF(
                            ((MONTH(event_date) = {$selMonth})AND(YEAR(event_date) = {$selYear})) , qty, 0
                        )
                    ) as this_months_qty"
                ),
                DB::raw(
                    "TRUNCATE(
                        SUM(
                            IF(
                                ((MONTH(event_date) = {$selMonth})AND(YEAR(event_date) = {$selYear})), amount, 0
                            )                           
                        )
                        +
                        IF(
                            wup.this_months_penalties_total IS NOT NULL, wup.this_months_penalties_total, 0
                        )
                        ,2
                    ) as this_months_total"
                ),
                DB::raw(
                    "TRUNCATE(SUM(
                        IF(
                            ((MONTH(event_date) = ".$selMonth.")AND(YEAR(event_date) = ".$selYear.")), (qty * listed_amount) * 0.85, 0
                        )
                    )
                    +
                    IF(
                        wup.this_months_penalties_total IS NOT NULL, wup.this_months_penalties_total, 0
                    )
                    ,2 )as this_months_broker_credit"
                ),
                DB::raw(
                    "TRUNCATE(SUM(
                        IF(
                            ((MONTH(event_date) <= ".$selMonth.")AND(YEAR(event_date) <= ".$selYear.")), (qty*listed_amount) * 0.85, 0
                        )
                    )
                    +
                    IF(
                        wup.historic_penalties_total IS NOT NULL, wup.historic_penalties_total, 0
                    )
                    ,2) as this_months_broker_credit_including_past"
                ),
                DB::raw(
                    "MAX(
                        event_date
                    ) as most_recent_sale"
                ),
                DB::raw(
                    "SUM(qty) as all_time_qty"
                ),
                DB::raw(
                    "TRUNCATE(SUM(
                        IF(
                            ((MONTH(event_date)>{$selMonth})AND(YEAR(event_date)>={$selYear})), (qty*listed_amount) * 0.85, 0
                        )
                    ) 
                    +
                    IF(
                        wup.future_penalties_total IS NOT NULL, wup.future_penalties_total, 0
                    )
                    , 2) as future_broker_credit"
                ),
                DB::raw(
                    "TRUNCATE(SUM((listed_amount*qty)*0.85) + 
                    IF(
                        wup.penalties_total IS NOT NULL, wup.penalties_total, 0
                    )
                    ,2) as all_time_owed"
                )
            )
            ->where('delivery_status', 'delivered')
            ->where('payment_status', '=', 'pending')
            ->groupBy('seller_id')
            ->leftJoin(DB::raw('
                (SELECT 
                    user_id,
                    TRUNCATE(SUM(
                        IF(
                            (YEAR(sale_date)='.$selYear.') AND (MONTH(sale_date)='.$selMonth.'), ammount, 0
                        )
                    ),2) as this_months_penalties_total,
                    TRUNCATE(SUM(
                        IF(
                            (YEAR(sale_date)<='.$selYear.') AND (MONTH(sale_date)<='.$selMonth.'), ammount, 0
                        )
                    ),2) as historic_penalties_total,

                    TRUNCATE(SUM(
                        IF(
                            (YEAR(sale_date)>='.$selYear.') AND (MONTH(sale_date)>'.$selMonth.'), ammount, 0
                        )
                    ),2) as future_penalties_total,

                    TRUNCATE(SUM(ammount)
                    ,2) as penalties_total

                    FROM web_user_penalties 
                    WHERE (paid = 0) 
                    GROUP BY user_id
                ) as wup'),
                function( $query ){
                    $query->on( 'sales.seller_id', '=', 'wup.user_id' );
                }
            )
            ->take($limit)
            ->skip($start)
            ->get();
        
        $output = [];
        $count = 0;
        foreach($rs as $r)
        {   

            $count++;
            $temp = [];
            $temp['0'] = '<a href="https://www.footballticketpad.com/admin/finances/seller-liabilities/'.$r->seller_id.'">'.$r->seller_id.'</a>';
            $temp['1'] = $r->seller_name;
            $temp['2'] = $r->all_time_qty;
            $temp['3'] = $r->this_months_qty;
            $temp['4'] = '£'. $r->this_months_total;
            $temp['5'] = '£'.$r->this_months_broker_credit;
            $temp['6'] = '£'.$r->this_months_broker_credit_including_past;
            if($r->all_time_owed != 0.00)
            {
                if($r->this_months_broker_credit_including_past != 0.00)
                {
                    $aDate = new DateTime();
                    $temp['7'] = $aDate->add(new DateInterval('P1M'))->format('Y-m');
                    $temp['7'].='-05';
                }
                else
                {
                    $aDate = new DateTime($r->most_recent_sale);
                    $temp['7'] = $aDate->add(new DateInterval('P1M'))->format('Y-m'); 
                    $temp['7'].='-05';
                }
            }
            else
            {
                $temp['7'] = "N/A";
            }
            $temp['8'] = '£'. $r->future_broker_credit;
            $temp['9'] = '£'.$r->all_time_owed;
            $output[] = $temp;
        }

        $results['data'] = $output;
        $results['recordsTotal'] = $total;
        $results['recordsFiltered'] = $total;
        $response = Response::make($results);
        $response->header('Content-Type', 'application/json');
        return $response;
        
    }

    public function getMonthsLiabilities()
    {
        $start = 0;
        $length = 10;
        if(Input::has('start'))
        {
            $start = Input::get('start');
        }
        if(Input::has('length'))
        {
            $limit = Input::get('length');
        }
        if(Input::has('dateFilter'))
        {
            $dateFilter = Input::get('dateFilter');
            if($dateFilter != "")
            {
                $dateFilter = DateTime::createFromFormat('d-m-Y', $dateFilter);
            }
            else
            {
                $dateFilter = new DateTime();
            }
        }
        else
        {
            $dateFilter = new DateTime();
        }
        $selMonth = $dateFilter->format('m');
        $selYear = $dateFilter->format('Y');
        $results = DB::table('sales')
                    ->select(
                        DB::raw("
                            SUM(
                                IF(
                                    ((MONTH(event_date) = {$selMonth})AND(YEAR(event_date) = {$selYear})) , qty, 0
                                )
                            ) as this_months_qty
                        "),
                        DB::raw("
                            TRUNCATE(SUM(
                                IF(
                                    ((MONTH(event_date) = {$selMonth})AND(YEAR(event_date) = {$selYear})), amount, 0
                                )
                            ),2) as this_months_amount
                        "),
                        DB::raw("
                            TRUNCATE(SUM(
                                IF(
                                    ((MONTH(event_date) = {$selMonth})AND(YEAR(event_date) = {$selYear})), (qty*listed_amount) * 0.85, 0
                                )
                            ),2) as this_months_broker_credit
                        "),
                        DB::raw("
                            TRUNCATE(SUM(
                                IF(
                                    ((MONTH(event_date) <= {$selMonth})AND(YEAR(event_date) <= {$selYear})), (qty*listed_amount) * 0.85, 0
                                )
                            ),2) as this_months_broker_credit_including_past
                        "),
                        DB::raw("
                            SUM(qty) as all_time_qty
                        "),
                        DB::raw(
                            "TRUNCATE(SUM(
                                (qty*listed_amount)*0.85
                            ),2) as all_time_owed"
                        )
                    )
                    ->where('delivery_status', 'delivered')
                    ->where('payment_status', '=', 'pending')
                    ->first();

        $results2 = DB::table('web_user_penalties')
            ->select(
                DB::raw("
                    TRUNCATE(SUM(
                        IF(
                            (YEAR(sale_date)=".$selYear.") AND (MONTH(sale_date)=".$selMonth."), ammount, 0
                        )
                    ),2) as this_months_penalties_total
                "),
                DB::raw("
                    TRUNCATE(SUM(
                        IF(
                            (YEAR(sale_date)<=".$selYear.") AND (MONTH(sale_date)<=".$selMonth."), ammount, 0
                        )
                    ),2) as historic_penalties_total
                "),
                DB::raw("
                    TRUNCATE(SUM(
                         ammount
                    ),2) as penalties_total    
                ")
            )
            ->where('paid', 0)
            ->first();

        $output = [];
        $output['data'] = [];
        $output['data']['0'] = [];
        $output['data']['0']['0'] = $results->this_months_qty;
        $output['data']['0']['1'] = '£'. ($results->this_months_amount + $results2->this_months_penalties_total);
        $output['data']['0']['2'] = '£'.($results->this_months_broker_credit + $results2->this_months_penalties_total);
        $output['data']['0']['3'] = '£'.($results->this_months_broker_credit_including_past + $results2->historic_penalties_total);
        $output['data']['0']['4'] = $results->all_time_qty;
        $output['data']['0']['5'] = '£'.($results->all_time_owed + $results2->penalties_total); 

        $output['recordsTotal'] = 1;
        $output['recordsFiltered'] = 1;

        $response = Response::make($output);
        $response->header('Content-Type', 'application/json');
        return $response;

    }
    public function showSellerLiabilities($id)
    {
        View::share('menu', 'finances');
        View::share('id', $id);
        $user = WebUser::where('user_id', '=', $id)->first();
        $user=$user['attributes'];
        $name = $user['firstname'].' '.$user['lastname'];
        View::share('seller_name', $name); 
        $today = new DateTime();
        View::share('todaysDate', $today->format('d-m-Y'));

        $sale = DB::table('events_related_tickets')
                    ->where('user_id', $id)
                    ->select(array('ticket'))
                    ->orderBy('id', 'DESC')
                    ->first();

        $iban = '';
        $account_holder = '';
        $bic_swift = '';

        if($sale)
        {
            $ticket = $sale->ticket;

            $ticket = json_decode($ticket);

            $account_holder = $ticket->paymentMethod->account_holder;
            $iban = $ticket->paymentMethod->iban;
            $bic_swift = $ticket->paymentMethod->bic_swift;
        }

        View::share('acc_holder_ph', $account_holder);
        View::share('iban_ph', $iban);
        View::share('bic_swift_ph', $bic_swift);


        return View::make('ticketAdmin::finances.individual-liabilities');
    }
    public function getLiabilityReportBySellerId($id)
    {
        $limit = 10;
        $skip = 0;

        if(Input::has('length'))
        {
            $limit = Input::get('length');
        }
        if(Input::has('start'))
        {
            $skip = Input::get('start');
        }

        if(Input::has('dateFilter'))
        {
            $dateFilter = Input::get('dateFilter');
            if($dateFilter != "")
            {
                $dateFilter = DateTime::createFromFormat('d-m-Y', $dateFilter);
            }
            else
            {
                $dateFilter = new DateTime();
            }
        }
        else
        {
            $dateFilter = new DateTime();
        }
        $selMonth = $dateFilter->format('m');
        $selYear = $dateFilter->format('Y');

        $total = DB::table('sales')
                    ->select(DB::raw('COUNT(*) as total'))
                    ->where('payment_status', 'pending')
                    ->where('delivery_status', 'delivered')
                    ->where('seller_id', $id)
                    ->first();
        $total = $total->total;
        $rs = DB::table('sales')
                    ->select(
                        'id',
                        'order_id',
                        'qty',
                        DB::raw('TRUNCATE(amount, 2) as total_sale_price'),
                        DB::raw('TRUNCATE((qty*listed_amount) * 0.85, 2) as seller_payment'),
                        'payment_status',
                        'event_date',
                        'buyer_firstname',
                        'buyer_lastname',
                        'tcode'
                    )
                    ->where('payment_status', 'pending')
                    ->where('delivery_status', 'delivered')
                    ->where('seller_id', $id)
                    ->orderBy('event_date', 'asc')
                    ->whereRaw("YEAR(event_date) = ".$selYear)
                    ->whereRaw("DATE_FORMAT(event_date, '%m') = ".$selMonth)
                    ->skip($skip)
                    ->take($limit)
                    ->get();
        $output = [];
        $count = 0;
        $pStatus = array();
        $pStatus[] = "paid";
        $pStatus[] = "pending";

        foreach($rs as $r)
        {   
            $tempChangePaid = '<select class="paid-st-select" onchange="postPaidStatusChange('. $r->id .', this)">';
            foreach($pStatus as $st)
            {
                if($r->payment_status == $st)
                {
                    $tempChangePaid.='<option value="'.$st.'" selected>'.$st.'</option>';
                }

                else
                {
                    $tempChangePaid.='   <option value="'.$st.'">'.$st.'</option>';
                }
            }
            $tempChangePaid.='</select>';  
 
            $count++;
            $temp = [];
            $temp['0'] = $r->id;
            $temp['1'] = '<a href="javascript:;" onclick="postShowFullInfo('. $r->order_id .')" class="trackingChange">'. $r->order_id .'</a>';
            $temp['2'] = $r->qty;
            $temp['3'] = '£'.$r->total_sale_price;
            $temp['4'] = '£'. $r->seller_payment;
            $temp['5'] = $tempChangePaid;
            $temp['6'] = $r->event_date;
            $temp['7'] = $r->buyer_firstname.' '.$r->buyer_lastname;
            $temp['8'] = $r->tcode;
            $temp['9'] = '<input type="checkbox" class="paidCheckboxs" name="mark_paid" autocomplete="off" value="'.$r->id.'">';
            $output[] = $temp;
        }
        $results['data'] = $output;
        $results['recordsTotal'] = $total;
        $results['recordsFiltered'] = $total;
        $response = Response::make($results);
        $response->header('Content-Type', 'application/json');
        return $response;
    }
    public function getLiabilityReportBySellerIdTotals($id){
        
        if(Input::has('dateFilter'))
        {
            $dateFilter = Input::get('dateFilter');
            if($dateFilter != "")
            {
                $dateFilter = DateTime::createFromFormat('d-m-Y', $dateFilter);
            }
            else
            {
                $dateFilter = new DateTime();
            }
        }
        else
        {
            $dateFilter = new DateTime();
        }
        $selMonth = $dateFilter->format('m');
        $selYear = $dateFilter->format('Y');

        $rs = DB::table('sales')
                    ->select(
                        DB::raw('
                            SUM(
                                IF(
                                    ((MONTH(event_date) = '.$selMonth.')AND(YEAR(event_date) = '.$selYear.')) , qty, 0
                                )
                            ) as this_months_qty
                        '),
                        DB::raw('
                            SUM(qty) as all_time_qty
                        '),
                        DB::raw('
                            TRUNCATE(SUM(
                                IF(
                                    ((MONTH(event_date) = '.$selMonth.')AND(YEAR(event_date) = '.$selYear.')), amount, 0
                                )
                            ),2) as this_months_amount
                        '),
                        DB::raw('
                            TRUNCATE(SUM(
                                amount
                            ),2) all_time_amount
                        '),
                        DB::raw('
                            TRUNCATE(SUM(
                                IF(
                                    ((MONTH(event_date) = '.$selMonth.')AND(YEAR(event_date) = '.$selYear.')), (qty*listed_amount) * 0.85, 0
                                )
                            ),2) as this_months_broker_credit
                        '),
                        DB::raw("
                            TRUNCATE(SUM(
                                IF(
                                    ((MONTH(event_date) <= ".$selMonth.")AND(YEAR(event_date) <= ".$selYear.")), (qty*listed_amount) * 0.85, 0
                                )
                            ),2) as this_months_broker_credit_including_past
                        "),
                        DB::raw(
                            "TRUNCATE(SUM((qty*listed_amount)*0.85),2) as all_time_owed"
                        )
                    )
                    ->where('delivery_status', 'delivered')
                    ->where('payment_status', 'pending')
                    ->where('seller_id', $id)
                    ->orderBy('event_date', 'asc')
                    ->first();

        $results2 = DB::table('web_user_penalties')
            ->select(
                DB::raw("
                    TRUNCATE(SUM(
                        IF(
                            (YEAR(sale_date)=".$selYear.") AND (MONTH(sale_date)=".$selMonth."), ammount, 0
                        )
                    ),2) as this_months_penalties_total
                "),
                DB::raw("
                    TRUNCATE(SUM(
                        IF(
                            (YEAR(sale_date)<=".$selYear.") AND (MONTH(sale_date)<=".$selMonth."), ammount, 0
                        )
                    ),2) as historic_penalties_total
                "),
                DB::raw("
                    TRUNCATE(SUM(
                         ammount
                    ),2) as penalties_total    
                ")
            )
            ->where('paid', 0)
            ->where('user_id', '=', $id)
            ->first();

        $output = [];
        $output['data'] = [];
        $output['data']['0'] = [];
        $output['data']['0']['0'] = $rs->this_months_qty;
        $output['data']['0']['1'] = $rs->all_time_qty;
        $output['data']['0']['2'] = '£'.($rs->this_months_amount + $results2->this_months_penalties_total);
        $output['data']['0']['3'] = '£'.($rs->all_time_amount + $results2->penalties_total);
        $output['data']['0']['4'] = '£'.($rs->this_months_broker_credit + $results2->this_months_penalties_total);
        $output['data']['0']['5'] = '£'.($rs->this_months_broker_credit_including_past + $results2->historic_penalties_total); 
        $output['data']['0']['6'] = '£'.($rs->all_time_owed + $results2->penalties_total); 

        $output['recordsTotal'] = 1;
        $output['recordsFiltered'] = 1;

        $response = Response::make($output);
        $response->header('Content-Type', 'application/json');
        return $response;
    }

    public function updatePaidStatusMulti()
    {
        if(Input::has('id_array'))
        {
            $idArr = Input::get('id_array');
            $idsDone = array();
            $sid = null;
            foreach ($idArr as $id) 
            {
                $payment_status = 'paid';
                $node = DB::table('sales')->where('id', '=',$id)->first();
                $ticketBuy = FootballTicketBuy::where('order_id', '=', $node->order_id)->first();
                $ticketPublishBySeller = RelatedTicket::where('event_id', '=',$node->event_id)->where('product_id', '=',$node->product_id)->first();
                
                $commission = isset($ticketPublishBySeller->selling_commission_percentage) ? $ticketPublishBySeller->selling_commission_percentage: 0;
                $sellerAmount = $node->qty * $ticketBuy->listed_amount;
                $commission_amount = $sellerAmount * $commission / 100;
                $amountWithoutCommission = $sellerAmount - $commission_amount;
                $info = json_decode($ticketPublishBySeller->ticket, true);
                $royalMailTracking = RoyalMailTracking::where('order_id', '=', $node->order_id)->first();
                $event_id = $node->event_id;
                $product_id = $node->product_id;
                $seller_id = $node->seller_id;
                $sid = $seller_id;
                $seller_information = WebUser::where('user_id', '=', $node->seller_id)->first();
                try {
                    if(!$seller_information) {
                        throw new Exception('Seller not found!');
                    }

                    $ticket = FootballTicketBuy::where('order_id', '=', $node->order_id)->first();

                    if(empty($ticket)) {
                        return "Could not find ticket for order id: ".$node->order_id;
                    }

                    $ticket->payment_status = $payment_status;
                    $ticket->seller_payment_status = $payment_status;
                    $ticket->seller_amount_paid    = $amountWithoutCommission;
                    $ticket->save(); 

                    $idsDone[] = $id;
                }
                catch(Exception $e) {
                    return "There was an error: ".$e;
                }   
            }

            $accDetailsArray = array();
            $accDetailsArray['ahn'] = '';
            $accDetailsArray['iban'] = '';
            $accDetailsArray['swift'] = '';
            $accDetailsArray['acc_num'] = '';
            $accDetailsArray['sortcode'] = '';
            $accDetailsArray['note'] = '';
            $accDetailsArray['date_paid'] = '';

            if(Input::has('ahn'))
            {
                $accDetailsArray['ahn'] = Input::get('ahn');
            }

            if(Input::has('iban'))
            {
                $accDetailsArray['iban'] = Input::get('iban');
            }

            if(Input::has('swift'))
            {
                $accDetailsArray['swift'] = Input::get('swift');
            }

            if(Input::has('acc_num'))
            {
                $accDetailsArray['acc_num'] = Input::get('acc_num');
            }

            if(Input::has('sortcode'))
            {
                $accDetailsArray['sortcode'] = Input::get('sortcode');
            }

            if(Input::has('note'))
            {
                $accDetailsArray['note'] = Input::get('note');
            }

            if(Input::has('date_paid'))
            {
                $accDetailsArray['date_paid'] = Input::get('date_paid');
            }

            $this->sendBrokerPaymentEmail($idsDone, $accDetailsArray);
            return "Updated and notified seller";
        }
        return "There was an error, seller not notified";
    }

    private function sendBrokerPaymentEmail($idArr, $ada)
    {
        $rs = DB::table('sales')
                ->whereIn('id', $idArr)
                ->get();

        $rs2 = DB::table('sales')
                ->select(DB::raw('SUM((qty*listed_amount)*0.85) as total_payment'))
                ->whereIn('id', $idArr)
                ->first();

        $listing = DB::table('events_related_tickets')
                ->whereIn('id', $idArr)
                ->get();


        $uID = $rs[0]->seller_id;

        $rs3 = DB::table('web_user_penalties')
            ->where('user_id', $uID)
            ->where('paid', 0)
            ->get();

        $rs4 = DB::table('web_user_penalties')
            ->select(DB::raw('
                SUM(ammount) as total_pens
            '))
            ->where('user_id', $uID)
            ->where('paid', 0)
            ->first();

        $emails = $rs[0]->seller_email;
        $name = $rs[0]->seller_firstname.' '.$rs[0]->seller_lastname;
        $data=array();
        $data['rs']=$rs;
        $data['rs2']=$rs2;


        $net_payment = $rs2->total_payment;
        $unpaid_pen = null;

        $cada = array();

        foreach($ada as $name => $val)
        {
            if(in_array($name, array('iban', 'swift', 'sortcode', 'acc_num')))
            {
                if(strlen($val > 4))
                {
                    $cada[$name] = substr_replace($val, str_repeat('*', strlen($val) - 4), 0, -4);
                }
                else
                {
                    $cada[$name] = '****';
                }
            }
            else
            {
                $cada[$name] = $val;
            }
        }

        $data['acc_details']=$cada;

        $emailTemplate = 'ticketAdmin::emails.monthly-payment-confirmation-to-seller';
        if(($rs3) && ($rs4))
        {
            $totalAfterFines = number_format($rs2->total_payment+$rs4->total_pens, 2);
            $net_payment = $totalAfterFines;

            $data['rs3']=$rs3;
            $data['rs4']=$rs4;

            $emailTemplate = 'ticketAdmin::emails.monthly-payment-confirmation-to-seller-with-add';


            $set=DB::table('web_user_penalties')
                    ->where('user_id', $uID)
                    ->update(array('paid'=>1)); 

            if($rs4->total_pens<($rs2->total_payment*-1))
            {
                
                $newComment = 'Remainder Penalty for penalty IDs: ';
                $highO = 0;
                $order_date = '';

                foreach($rs3 as $r){
                    $newComment.=$r->id;
                    $highO = $r->order_id;
                    $order_date = $r->sale_date;
                }

                $addNew = DB::table('web_user_penalties')
                    ->insert([
                        'user_id'=>$uID,
                        'order_id'=>$highO,
                        'ammount' => $totalAfterFines,
                        'comment' => $newComment,
                        'sale_date' => $order_date,
                        'paid' => 0
                    ]);

            }

            $unpaid_pen = DB::table('web_user_penalties')
                ->where('user_id', $uID)
                ->where('paid', 0)
                ->get();

        }
        

        Mail::send($emailTemplate, $data, function ($message) use($emails, $name) {
            $from = Config::get('mail.from');
            $message->from($from['address'], $from['name']);
            $email = Options::getOption('contact_email') == ''? 'hello@bondmedia.co.uk': Options::getOption('contact_email');

            $message->to($emails, $name )
                ->bcc($email, 'Football Ticket Pad')
                ->subject("Football ticket payment report");
        });

        $dt = new DateTime();
        $ins_array = array(
            'broker_id'=>$uID,
            'created_at'=>$dt->format('Y-m-d H:i:s'),
            'total_paid' => $net_payment,
            'account_holder_name'=>$ada['ahn'],
            'IBAN'=>$ada['iban'],
            'SWIFT'=>$ada['swift'],
            'acc_num'=>$ada['acc_num'],
            'sort_code'=>$ada['sortcode'],
            'note' =>$ada['note'],
            'payment_date' => $ada['date_paid']
        );

        $bph_id = DB::table('broker_payment_history')
                    ->insertGetId($ins_array);

        foreach($idArr as $oid)
        {
            DB::table('broker_payment_history_object')
                    ->insert(array(
                       'bph_id'=>$bph_id,
                        'obj_type'=>'order',
                        'obj_id'=>$oid
                    ));
        }

        foreach($rs3 as $rs3min)
        {
            DB::table('broker_payment_history_object')
                    ->insert(array(
                       'bph_id'=>$bph_id,
                        'obj_type'=>'penalty',
                        'obj_id'=>$rs3min->id
                    ));
        }

        //So, got the user input, got the sales and totals and the penalties, inserted bph and bpho

        return $rs;
    } 

    public function getPenaltiesByUserId($id)
    {
        if(Input::has('dateFilter'))
        {
            $dateFilter = Input::get('dateFilter');
            if($dateFilter != "")
            {
                $dateFilter = DateTime::createFromFormat('d-m-Y', $dateFilter);
            }
            else
            {
                $dateFilter = new DateTime();
            }
        }
        else
        {
            $dateFilter = new DateTime();
        }

        $selMonth = $dateFilter->format('m');
        $selYear = $dateFilter->format('Y');

        $rs = DB::table('web_user_penalties')
            ->where('user_id', $id)
            ->where('paid', 0)
            ->whereRaw("
                MONTH(sale_date) = {$selMonth}"
            )
            ->whereRaw("
                YEAR(sale_date) = {$selYear}"
            )
            ->get();


        $output = [];
        $output['data'] = [];

        $i = 0;
        foreach($rs as $r)
        {
            $output['data'][$i]=[];
            $output['data'][$i][0]=$r->id;
            $output['data'][$i][1]=$r->order_id;
            $output['data'][$i][2]=$r->sale_date;
            $output['data'][$i][3]=$r->comment;
            $output['data'][$i][4]='£'.$r->ammount;
            $output['data'][$i][5]=$r->paid;
            $output['data'][$i][6]='<a href="javascript:;" onclick="deletePenaltyById('.$r->id.')" class="del"><img src="/assets/frontend/bm2014/images/trash.png"></a>';
            $i++;
        }

        $output['recordsTotal'] = $i;
        $output['recordsFiltered'] = $i;

        $response = Response::make($output);
        $response->header('Content-Type', 'application/json');
        return $response;
    }

    public function addPenaltiesByUserId($id)
    {
        $oID = Input::get('order_id');
        $ammount = Input::get('ammount');
        $comment = Input::get('comment');


        $ammount = (float) $ammount;
        $oId = intval($oID);

        $order = FootballTicketBuy::where('order_id', '=', $oID)->firstOrFail();

        $ert = RelatedTicket::where('product_id', '=', $order->product_id)->firstOrFail();
        $event = FootBallEvent::findOrFail($ert->event_id);

        $date = $event->datetime;

        $rs = DB::table('web_user_penalties')
            ->insert(
                array(
                    'user_id' => $id, 
                    'order_id' => $oID,
                    'ammount' => $ammount,
                    'comment' => $comment,
                    'sale_date'=>$date,
                    'paid'=>0
                )
            );

        return Response::json("Success", 200);
    }

    public function deletePenaltyByPenaltyId($id)
    {

        $id = intval($id);
        $rs = DB::table('web_user_penalties')
            ->where('id', $id)
            ->delete();

        if($rs)
        {
            return Response::json("Success", 200);
        }
        return Response::json("Fail", 404);
    }

    public function getFullSpeculatedFinanceTotals()
    {
        $results = DB::table('sales')
                    ->select(
                        DB::raw("
                            SUM(qty) as all_time_qty
                        "),
                        DB::raw("
                            TRUNCATE(SUM((qty*listed_amount) * 0.85),2) as total_credit_nip
                        ")
                    )
                    ->whereIn('delivery_status', ['pending','in-transit','delivered'])
                    ->where('payment_status', '=', 'pending')
                    ->first();

        $results2 = DB::table('web_user_penalties')
            ->select(
                DB::raw("
                    TRUNCATE(SUM(ammount),2) as penalties_total    
                ")
            )
            ->where('paid', '=', 0)
            ->first();


        $output = [];
        $output['data'] = [];
        $output['data']['0'] = [];
        $output['data']['0']['0'] = $results->all_time_qty;
        $output['data']['0']['1'] = '£'.$results->total_credit_nip;
        $output['data']['0']['2'] = '£'.$results2->penalties_total;
        $output['data']['0']['3'] = '£'.($results->total_credit_nip + $results2->penalties_total);

        $output['recordsTotal'] = 1;
        $output['recordsFiltered'] = 1;

        $response = Response::make($output);
        $response->header('Content-Type', 'application/json');
        return $response;
    }

    public function getFullSpeculatedFinanceUserTotals()
    {

        $start = 0;
        $limit = 10;
        if(Input::has('start'))
        {
            $start = Input::get('start');
        }
        if(Input::has('length'))
        {
            $limit = Input::get('length');
        }

        $total = DB::table('sales')
                    ->select(DB::raw("COUNT(DISTINCT seller_id) as total"))
                    ->whereIn('delivery_status', ['delivered','in-transit','pending'])
                    ->where('payment_status', '=', 'pending')
                    ->first();

        $total = $total->total;
        $rs = DB::table('sales')
            ->select(
                'seller_id',
                'wup.penalties_total',
                DB::raw('CONCAT_WS(" ", seller_firstname, seller_lastname) as seller_name'),
                DB::raw(
                    "SUM(qty) as user_qty"
                ),
                DB::raw(
                    "TRUNCATE(SUM((listed_amount*qty)*0.85),2) as all_time_owed_nip"
                ),
                DB::raw(
                    "TRUNCATE(SUM((listed_amount*qty)*0.85) + 
                    IF(
                        wup.penalties_total IS NOT NULL, wup.penalties_total, 0
                    )
                    ,2) as all_time_owed"
                )
            )
            ->whereIn('delivery_status', ['delivered', 'in-transit', 'pending'])
            ->where('payment_status', '=', 'pending')
            ->groupBy('seller_id')
            ->leftJoin(DB::raw('
                (SELECT 
                    user_id,
                    TRUNCATE(SUM(ammount),2) as penalties_total
                    FROM web_user_penalties 
                    WHERE paid = 0
                    GROUP BY user_id
                ) as wup'),
                function( $query ){
                    $query->on( 'sales.seller_id', '=', 'wup.user_id' );
                }
            )
            ->take($limit)
            ->skip($start)
            ->get();
        

        $output = [];
        $count = 0;
        foreach($rs as $r)
        {   

            $count++;
            $temp = [];
            $temp['0'] = '<a href="/admin/finances-futures/indiv/'.$r->seller_id.'">'.$r->seller_id.'</a>';
            $temp['1'] = $r->seller_name;
            $temp['2'] = $r->user_qty;
            $temp['3'] = $r->all_time_owed_nip;
            $temp['4'] = '£'.$r->penalties_total;
            $temp['5'] = '£'.$r->all_time_owed;
            
            $output[] = $temp;
        }

        $results['data'] = $output;
        $results['recordsTotal'] = $total;
        $results['recordsFiltered'] = $total;
        $response = Response::make($results);
        $response->header('Content-Type', 'application/json');
        return $response;
    }

    public function getFullFutureFinanceByUserID($id)
    {
        $limit = 10;
        $skip = 0;

        if(Input::has('length'))
        {
            $limit = Input::get('length');
        }
        if(Input::has('start'))
        {
            $skip = Input::get('start');
        }

        $total = DB::table('sales')
                    ->select(DB::raw('COUNT(*) as total'))
                    ->where('payment_status', 'pending')
                    ->whereIn('delivery_status', ['delivered', 'in-transit', 'pending'])
                    ->where('seller_id', $id)
                    ->first();
        $total = $total->total;
        $rs = DB::table('sales')
                    ->select(
                        'id',
                        'order_id',
                        'qty',
                        DB::raw('TRUNCATE(amount, 2) as total_sale_price'),
                        DB::raw('TRUNCATE((qty*listed_amount) * 0.85, 2) as seller_payment'),
                        'event_date',
                        'buyer_firstname',
                        'buyer_lastname',
                        'tcode'
                    )
                    ->where('payment_status', 'pending')
                    ->whereIn('delivery_status', ['delivered', 'pending', 'in-transit'])
                    ->where('seller_id', $id)
                    ->orderBy('event_date', 'asc')
                    ->skip($skip)
                    ->take($limit)
                    ->get();
        $output = [];
        $count = 0;

        foreach($rs as $r)
        {   
            $count++;
            $temp = [];
            $temp['0'] = $r->id;
            $temp['1'] = '<a href="javascript:;" onclick="postShowFullInfo('. $r->order_id .')" class="trackingChange">'. $r->order_id .'</a>';
            $temp['2'] = $r->qty;
            $temp['3'] = '£'.$r->total_sale_price;
            $temp['4'] = '£'. $r->seller_payment;
            $temp['5'] = $r->event_date;
            $temp['6'] = $r->buyer_firstname.' '.$r->buyer_lastname;
            $temp['7'] = $r->tcode;
            $output[] = $temp;
        }
        $results['data'] = $output;
        $results['recordsTotal'] = $total;
        $results['recordsFiltered'] = $total;
        $response = Response::make($results);
        $response->header('Content-Type', 'application/json');
        return $response;
    }

    public function getFullFutureFinanceTotalsByUserID($user_id)
    {
        $results = DB::table('sales')
                    ->select(
                        DB::raw("
                            SUM(qty) as all_time_qty
                        "),
                        DB::raw("
                            TRUNCATE(SUM((qty*listed_amount) * 0.85),2) as total_credit_nip
                        ")
                    )
                    ->whereIn('delivery_status', ['pending','in-transit','delivered'])
                    ->where('payment_status', '=', 'pending')
                    ->where('seller_id', $user_id)
                    ->first();

        $results2 = DB::table('web_user_penalties')
            ->select(
                DB::raw("
                    TRUNCATE(SUM(ammount),2) as penalties_total    
                ")
            )
            ->where('paid', '=', 0)
            ->where('user_id', $user_id)
            ->first();

        $output = [];
        $output['data'] = [];
        $output['data']['0'] = [];
        $output['data']['0']['0'] = $results->all_time_qty;
        $output['data']['0']['1'] = '£'.$results->total_credit_nip;
        $output['data']['0']['2'] = '£'.($results2->penalties_total)?$results2->penalties_total:0.00;
        $output['data']['0']['3'] = '£'.($results->total_credit_nip + $results2->penalties_total);

        $output['recordsTotal'] = 1;
        $output['recordsFiltered'] = 1;

        $response = Response::make($output);
        $response->header('Content-Type', 'application/json');
        return $response;
    }

    public function futures()
    {
        View::share('menu', 'futures');
        return View::make('ticketAdmin::finances.futures');
    }

    public function futuresIndiv($id)
    {
        View::share('menu', 'futures');
        View::share('id', $id);
        $user = WebUser::where('user_id', '=', $id)->first();
        $user=$user['attributes'];
        $name = $user['firstname'].' '.$user['lastname'];
        View::share('seller_name', $name); 
        return View::make('ticketAdmin::finances.futures-indiv');
    }

    public function getFiguresByDate()
    {
        $date = Input::get('date');
        $data = array();

        $dt = DateTime::createFromFormat('Y-m-d', $date);
        $year = $dt->format('Y');
        $month = $dt->format('m');
        //So this will return pure figgins.

        $on_site_sales_total = DB::table('events_ticket_buy')
            ->select(DB::raw(
                'SUM(amount) as total'
            ))
            ->whereRaw('
                YEAR(created_at) = "'.$year.'" AND MONTH(created_at) = "'.$month.'"
            ')
            ->first();

        $on_site_sales = DB::table('events_ticket_buy')
            ->whereRaw('
                YEAR(created_at) = "'.$year.'" AND MONTH(created_at) = "'.$month.'"
            ')
            ->get();


        $off_site_sales = DB::table('profit_loss_entities')
            ->where('type', '=', "off-site-sales")
            ->whereRaw('
                YEAR(date_applicable) = '.$year.' AND MONTH(date_applicable) = '.$month.'
            ')
            ->get();

        $off_site_sales_total = DB::table('profit_loss_entities')
            ->select(DB::raw('
                SUM(val) as total
            '))
            ->where('type', '=', "off-site-sales")
            ->whereRaw('
                YEAR(date_applicable) = '.$year.' AND MONTH(date_applicable) = '.$month.'
            ')
            ->first();

        $total_income = $on_site_sales_total->total + $off_site_sales_total->total;

        $cost_of_goods = DB::table('profit_loss_entities')
            ->where('type', '=', "cost_of_goods")
            ->whereRaw('
                YEAR(date_applicable) = '.$year.' AND MONTH(date_applicable) = '.$month.'
            ')
            ->get();

        $cost_of_goods_total = DB::table('profit_loss_entities')
            ->select(DB::raw('
                SUM(val) as total
            '))
            ->where('type', '=', "cost_of_goods")
            ->whereRaw('
                YEAR(date_applicable) = '.$year.' AND MONTH(date_applicable) = '.$month.'
            ')
            ->first();

        $gross_profit = $total_income - $cost_of_goods_total->total;

        $expenses = DB::table('profit_loss_entities')
            ->where('type', '=', "expense")
            ->whereRaw('
                YEAR(date_applicable) = '.$year.' AND MONTH(date_applicable) = '.$month.'
            ')
            ->get();

        $expenses_total = DB::table('profit_loss_entities')
            ->select(DB::raw('
                SUM(val) as total
            '))
            ->where('type', '=', "expense")
            ->whereRaw('
                YEAR(date_applicable) = '.$year.' AND MONTH(date_applicable) = '.$month.'
            ')
            ->first();

        $net_ordinary_income = $gross_profit - $expenses_total->total;

        $other_income = DB::table('profit_loss_entities')
            ->where('type', '=', "other_income")
            ->whereRaw('
                YEAR(date_applicable) = '.$year.' AND MONTH(date_applicable) = '.$month.'
            ')
            ->get();

        $other_income_total = DB::table('profit_loss_entities')
            ->select(DB::raw('
                SUM(val) as total
            '))
            ->where('type', '=', "other_income")
            ->whereRaw('
                YEAR(date_applicable) = '.$year.' AND MONTH(date_applicable) = '.$month.'
            ')
            ->first();

        $other_expenses = DB::table('profit_loss_entities')
            ->where('type', '=', "other_expense")
            ->whereRaw('
                YEAR(date_applicable) = '.$year.' AND MONTH(date_applicable) = '.$month.'
            ')
            ->get();

        $other_expenses_total = DB::table('profit_loss_entities')
            ->select(DB::raw('
                SUM(val) as total
            '))
            ->where('type', '=', "other_expense")
            ->whereRaw('
                YEAR(date_applicable) = '.$year.' AND MONTH(date_applicable) = '.$month.'
            ')
            ->first();

        $net_other = $other_income_total->total - $other_expenses_total->total;

        $net = $net_ordinary_income + $net_other;

        $data['on_site_sales_total'] = $on_site_sales_total->total;
        $data['on_site_sales'] = $on_site_sales;
        $data['off_site_sales'] = $off_site_sales;
        $data['off_site_sales_total'] = $off_site_sales_total->total;
        $data['total_income'] = $total_income;
        $data['cost_of_goods'] = $cost_of_goods;
        $data['cost_of_goods_total'] = $cost_of_goods_total->total;
        $data['gross_profit'] = $gross_profit;
        $data['expenses'] = $expenses;
        $data['expenses_total'] = $expenses_total->total;
        $data['net_ordinary_income'] = $net_ordinary_income;
        $data['other_income'] = $other_income;
        $data['other_income_total'] = $other_income_total->total;
        $data['other_expenses'] = $other_expenses;
        $data['other_expenses_total'] = $other_expenses_total->total;
        $data['net_other'] = $net_other;
        $data['net'] = $net;

        return Response::json($data, 200);
    }


    public function addProfitLossEntity()
    {

    }

    public function deleteProfitLossEntity($id)
    {

    }


    public function paymentHistoryIndex()
    {
        $dt = new DateTime();
        View::share('today', $dt->format('d-m-Y'));
        View::share('menu', 'history');

        return View::make('ticketAdmin::finances.payment_history_list');
    }

    public function paymentHistoryList()
    {
        $dt = new DateTime();
        if(Input::has('dateFilter'))
        {
            $ds = Input::get('dateFilter');
            $ds = DateTime::createFromFormat('d-m-Y', $ds);
            if($ds)
            {
                $dt = $ds;
            }
        }

        $limit = 10;
        $skip = 0;

        if(Input::has('length'))
        {
            $limit = Input::get('length');
        }
        if(Input::has('start'))
        {
            $skip = Input::get('start');
        }

        $rs = DB::table('broker_payment_history as bph')
            ->leftJoin('web_user', 'bph.broker_id', '=', 'web_user.user_id')
            ->whereRaw('MONTH(bph.created_at) = '.$dt->format('m'))
            ->whereRaw('YEAR(bph.created_at) = '.$dt->format('Y'))
            ->orderBy('bph.created_at', 'DESC')
            ->select(array(
                    'bph.id', 'bph.payment_date', 'bph.created_at', 'bph.total_paid', 'bph.account_holder_name',
                    'bph.acc_num', 'bph.IBAN', 'bph.SWIFT', 'web_user.firstname', 'web_user.lastname'
                )
            )
            ->take($limit)
            ->skip($skip)
            ->get();

        $total = DB::table('broker_payment_history')
            ->whereRaw('MONTH(created_at) = '.$dt->format('m'))
            ->whereRaw('YEAR(created_at) = '.$dt->format('Y'))
            ->select(DB::raw('count(*) as count'))
            ->first();


        $output = [];

        $count = 0;

        foreach($rs as $r)
        {
            $count++;
            $temp = [];
            \Log::info(var_export($r,true));

            if($r->total_paid <= 0)
            {
                $r->total_paid = 0.00;
            }
            $temp['0'] = '<a href="/admin/finances/payment-history/indiv/'.$r->id.'">'.$r->id.'</a>';
            $temp['1'] = $r->created_at;
            $temp['2'] = $r->firstname.' '.$r->lastname;
            $temp['3'] = '£'.$r->total_paid;
            $temp['4'] = $r->payment_date;
            $temp['5'] = $r->account_holder_name;
            $temp['6'] = $r->acc_num;
            $temp['7'] = $r->IBAN;
            $temp['8'] = $r->SWIFT;
            $output[] = $temp;
        }
        $results['data'] = $output;
        $results['recordsTotal'] = $total->count;
        $results['recordsFiltered'] = $count;
        $response = Response::make($results);
        $response->header('Content-Type', 'application/json');
        return $response;

    }

    public function paymentHistoryIndiv($id)
    {
        $dt = new DateTime();
        View::share('today', $dt->format('d-m-Y'));
        View::share('id', $id);

        View::share('menu','history');

        return View::make('ticketAdmin::finances.payment_history_indiv');
    }

    public function paymentHistoryIndivTotalList($id)
    {
        $rs = DB::table('broker_payment_history as bph')
                ->leftJoin('web_user', 'bph.broker_id', '=', 'web_user.user_id')
                ->where('bph.id', $id)
                ->select(array(
                    'bph.total_paid', 'bph.created_at', 'bph.payment_date', 'web_user.firstname', 'web_user.lastname', 'bph.account_holder_name',
                    'bph.IBAN', 'bph.SWIFT', 'bph.acc_num', 'bph.sort_code', 'bph.total_paid', 'bph.id', 'bph.note'
                ))
                ->first();

        if($rs)
        {
            $output=[];
            $temp = [];

            $total = 1;

            $temp['0'] = $rs->total_paid;
            $temp['1'] = $rs->created_at;
            $temp['2'] = $rs->payment_date;
            $temp['3'] = $rs->firstname.' '.$rs->lastname;
            $temp['4'] = $rs->account_holder_name;
            $temp['5'] = $rs->acc_num;
            $temp['6'] = $rs->SWIFT;
            $temp['7'] = $rs->IBAN;
            $temp['8'] = $rs->sort_code;
            if(sizeof($rs->note) > 20)
            {
                $temp['9'] = '<div class="view-note btn" data-id="'.$rs->id.'">View Note</div>';
            }
            else
            {
                $temp['9'] = $rs->note;
            }

            $output[] = $temp;

            $results['data'] = $output;
            $results['recordsTotal'] = 1;
            $results['recordsFiltered'] = 1;
            $response = Response::make($results);
            $response->header('Content-Type', 'application/json');
            return $response;

        }
        return Response::json("Couldnt find payment",404);
    }

    public function paymentHistoryIndivOrderList($id)
    {

        $limit = 10;
        $start = 0;
        if(Input::has('length'))
        {
            $limit = Input::get('length');
        }
        if(Input::has('start'))
        {
            $skip = Input::get('start');
        }

        $rs = DB::table('broker_payment_history_object as bpho')
                    ->leftJoin('events_ticket_buy', 'bpho.obj_id', '=', 'events_ticket_buy.id')
                    ->where('bpho.bph_id', $id)
                    ->where('bpho.obj_type', 'order')
                    ->select(DB::raw('(events_ticket_buy.amount-events_ticket_buy.fees_amount)*0.85 as earning, bpho.bph_id, events_ticket_buy.order_id, events_ticket_buy.created_at, bpho.id'))
                    ->take($limit)
                    ->skip($start)
                    ->get();

        $total = DB::table('broker_payment_history_object')
            ->where('bph_id', $id)
            ->where('obj_type', 'order')
            ->select(DB::raw('count(*) as count'))
            ->first();

        if($rs)
        {
            $output = [];
            $count = 0;

            foreach($rs as $r)
            {
                $count++;
                $temp = [];
                $temp['0'] = $r->id;
                $temp['1'] = $r->order_id;
                $temp['2'] = $r->created_at;
                $temp['3'] = '£'.number_format($r->earning,2,'.','');
                $output[] = $temp;
            }
            $results['data'] = $output;
            $results['recordsTotal'] = $total->count;
            $results['recordsFiltered'] = $count;
            $response = Response::make($results);
            $response->header('Content-Type', 'application/json');
            return $response;
        }

        return Response::json('error',404);
    }

    public function paymentHistoryIndivPenaltyList($id)
    {
        $limit = 10;
        $start = 0;
        if(Input::has('length'))
        {
            $limit = Input::get('length');
        }
        if(Input::has('start'))
        {
            $skip = Input::get('start');
        }

        $rs = DB::table('broker_payment_history_object as bpho')
            ->leftJoin('web_user_penalties', 'bpho.obj_id', '=', 'web_user_penalties.id')
            ->where('bpho.bph_id', $id)
            ->where('bpho.obj_type', 'penalty')
            ->select(array(
                'web_user_penalties.id', 'web_user_penalties.ammount', 'web_user_penalties.comment'
            ))
            ->take($limit)
            ->skip($start)
            ->get();

        $total = DB::table('broker_payment_history_object')
            ->where('bph_id', $id)
            ->where('obj_type', 'penalty')
            ->select(DB::raw('count(*) as count'))
            ->first();

        if($rs)
        {
            $output = [];
            $count = 0;

            foreach($rs as $r)
            {
                $count++;
                $temp = [];
                $temp['0'] = $r->id;
                $temp['1'] = '£'.$r->ammount;
                $temp['2'] = $r->comment;
                $output[] = $temp;
            }
            $results['data'] = $output;
            $results['recordsTotal'] = $total->count;
            $results['recordsFiltered'] = $count;
            $response = Response::make($results);
            $response->header('Content-Type', 'application/json');
            return $response;
        }

        return Response::json('error',404);
    }

    public function paymentHistoryIndivNote($id){
        $r = DB::table('broker_payment_history')
                ->where('id', $id)
                ->select('note')
                ->first();

        if($r)
        {
            return Response::json($r->note,200);
        }

        return Response::json("Error",404);
    }


    public function paymentHistoryPrintable($id)
    {
        $ph = DB::table('broker_payment_history')
                ->leftJoin('web_user', 'broker_payment_history.broker_id', '=', 'web_user.user_id')
                ->select(array('broker_payment_history.*', 'web_user.firstname', 'web_user.lastname'))
                ->where('broker_payment_history.id', $id)
                ->first();

        if($ph)
        {
            $sales = DB::table('broker_payment_history_object as bpho')
                        ->leftJoin('events_ticket_buy as etb', 'bpho.obj_id', '=', 'etb.id')
                        ->select(array(
                            'etb.*'
                        ))
                        ->where('bpho.obj_type', 'order')
                        ->where('bpho.bph_id', $id)
                        ->get();

            $penalties = DB::table('broker_payment_history_object as bpho')
                ->leftJoin('web_user_penalties as wup', 'bpho.obj_id', '=', 'wup.id')
                ->select(array('wup.*'))
                ->where('bpho.obj_type', 'penalty')
                ->where('bpho.bph_id', $id)
                ->get();

            View::share('ph', $ph);
            View::share('sales', $sales);
            View::share('penalties', $penalties);

            return View::make('ticketAdmin::finances.payment_history_indiv_printable');
        }

        return Response::json("Error, could not find payment history object",404);
    }
}