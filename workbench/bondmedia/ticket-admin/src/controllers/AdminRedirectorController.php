<?php

/**
 * Created by PhpStorm.
 * User: baglad
 * Date: 19/07/2016
 * Time: 10:45
 */
class AdminRedirectorController extends \BaseController {

    public function index()
    {
        View::share('menu', 'redirector');
        return View::make('ticketAdmin::redirector.redirector');
    }

    public function datatable()
    {
        $start = 0;
        $limit = 10;
        $search = '';
        if(Input::has('start'))
        {
            $start = Input::get('start');
        }
        if(Input::has('length'))
        {
            $limit = Input::get('length');
        }
        if(Input::has('search'))
        {
            $search = Input::get('search');
            $search = $search['value'];
        }


        $output = [];


        $rdrs = DB::table('rdr')
            ->where('to', 'like', '%'.$search.'%')
            ->orWhere('from', 'like', '%'.$search.'%')
            ->take($limit)
            ->skip($start)
            ->get();

        $ttl = DB::table('rdr')
                ->where('to', 'like', '%'.$search.'%')
                ->orWhere('from', 'like', '%'.$search.'%')
                ->select(DB::raw('COUNT(*) as ttl'))
                ->first();


        foreach($rdrs as $request)
        {
            $temp = [];
            $temp['0'] = $request->id;
            $temp['1'] = '<input type="text" id="frm-'.$request->id.'" value="'.$request->from.'">';
            $temp['2'] = '<input type="text" id="to-'.$request->id.'" value="'.$request->to.'">';
            $temp['3'] = '<input type="text" id="code-'.$request->id.'" value="'.$request->code.'">';
            $temp['4'] = '<button class="save" data-id="'.$request->id.'">Save</button>';
            $temp['4'] .= '<button class="delete" data-id="'.$request->id.'">Delete</button>';

            $output[] = $temp;
        }

        $results['data'] = $output;
        $results['recordsTotal'] = $ttl->ttl;
        $results['recordsFiltered'] = $ttl->ttl;
        $response = Response::make($results);
        $response->header('Content-Type', 'application/json');
        return $response;
    }

    public function update($id)
    {
        $input = Input::all();

        DB::table('rdr')
            ->where('id', $id)
            ->update([
                'code' => $input['code'],
                'from' => $input['from'],
                'to' => $input['to']
            ]);


        return Response::json('success', 200);
    }

    public function delete($id)
    {
        DB::table('rdr')
                ->where('id', $id)
                ->delete();

        return Response::json('success',200);
    }

    public function add()
    {
        $input = Input::all();
        DB::table('rdr')
            ->insert([
                'code'=>$input['code'],
                'from' => $input['from'],
                'to' => $input['to']
            ]);

        return Response::json('success', 200);
    }
}