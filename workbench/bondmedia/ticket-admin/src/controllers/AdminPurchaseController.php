<?php

class AdminPurchaseController extends \BaseController {
	public function getOrderForm($product_id)
	{
		$r_ticket = RelatedTicket::where('product_id', $product_id)->first();

		$ticket = json_decode($r_ticket->ticket);

		$event = FootBallEvent::where('id', $r_ticket->event_id)->first();
		$now=time();
		$edate=strtotime($event->datetime);

		if($now>$edate)
		{
			return Response::json("", 200);
		}

		$share_vars = array();
		$share_vars['pid'] = $product_id;
		$share_vars['available_to_buy'] = array();

		$preference = $ticket->ticketInformation->sell_preference?intval($ticket->ticketInformation->sell_preference):0;
		$available = $r_ticket->available_qty;
		$share_vars['available'] = $available;
		$share_vars['preference'] = $preference;

		if($preference == 0)
		{
			for($i=1; $i<=$available; $i++)
			{
				$share_vars['available_to_buy'][] = $i;
			}
		}
		else if($preference == 1)
		{
			$share_vars['available_to_buy'][]=$available;
		}
		else if($preference == 2)
		{
			for($i=1; $i<($available-1); $i++)
			{
				$share_vars['available_to_buy'][] = $i;
			}
			$share_vars['available_to_buy'][] = $available;
		}
		else if($preference == 3)
		{
			for($i = 2; $i <= $available; $i+=2)
			{
				$share_vars['available_to_buy'][] = $i;
			}
		}

		$countryList = CountryList::getCountryListWithCountryCode();
        $country = [];
        foreach($countryList as $c) {
            $country[$c->Code] = $c->Country;
        }

        $share_vars['country']=$country;
        $share_vars['pid'] = $product_id;
        $share_vars['event_id'] = $r_ticket->event_id;

		View::share('shared',$share_vars);

		return View::make("ticketAdmin::tickets.admin-buy");
	}

	public function makeOrder($id)
	{
		$input = Input::all();
		$ticket = RelatedTicket::getTicketByTicketId($id); 
        $ticketInfo = RelatedTicket::where('product_id', '=', $id)->first();

        $shoppingCartId = TicketSoap::process('cart.create', array('default'));
        $curItems = TicketSoap::process("cart_product.list", array($shoppingCartId));

        $event= FootBallEvent::find($ticket->event_id);

        $customer = array (
		  'website_id' => '1',
		  'entity_id' => '986',
		  'email' => 'info@footballticketpad.com',
		  'firstname' => 'FTP',
		  'lastname' => 'Joint',
		  'default_billing' => '1780',
		  'default_shipping' => '1781',
		);

		$_ticket = RelatedTicket::getTicketByTicketId($id);
        $_ticket->price = round($_ticket->price, 2);

        try
        {
            $cart = TicketSoap::process("cart.info", array($shoppingCartId));
            $store_id = $cart['store_id'];
            if($cart)
            {
                $itemArray = array();
                
                foreach($cart['items'] as $item)
                {
                    TicketSoap::process("cart_product.remove", array($shoppingCartId, array(array('product_id' => $item['product_id'],'sku'=>$item['sku'], 'quantity'=>$item['qty'], 'options' => array())), $store_id));
                }
                
            }
        }
        catch(Exeption $e)
        {
        }

        if($_ticket->ticket_status != 'active')
        {
            throw new Exception( json_encode("Error."));
        }

        $customerId = $customer['entity_id'];

        $params = array();
        $params['customer_id']  = $customerId;
        $params['type']         = 'set';
        $params['address_type'] = 'shipping';
        $params['vat_id']       = '';
        $params['company']      = '';
        $params['street']       = $input['street'];
        $params['country_id']   = $input['country'];
        $params['postcode']     = $input['postcode'];
        $params['city']         = $input['city'];
        $params['telephone']    = $input['telephone'];
        $_address               = $params;

        //set billing
        $this->setDataToPost($params);
        $this->submitPostToApi('customer/index/address');

        //set shipping
        $params['address_type'] = 'billing';
        
        $this->setDataToPost($params);
        $this->submitPostToApi('customer/index/address');
        
        $addressParams = array(
            'customer_id'   => $customerId,
            'type'          => 'get',
            'address_type'  => 'billing'
        );

        $this->setDataToPost($addressParams);
        $this->submitPostToApi('customer/index/address');
        $response = $this->getApiResponse();
        $billing = json_decode($response, true);

        $addressParams['address_type'] = 'shipping';
        $this->setDataToPost($addressParams);
        $this->submitPostToApi('customer/index/address');
        $response = $this->getApiResponse();
        $shipping = json_decode($response, true);

        $resultCustomerSet = TicketSoap::process('cart_customer.set', array( $shoppingCartId, array(
            'entity_id' => $customerId,
            'mode'      => 'customer'
        )));

	    TicketSoap::process("customer_address.update", array(
            'addressId' =>  $billing['data']['entity_id'],
            'addressdata' => array(
            'firstname' =>  $input['firstname'],
            'lastname'  =>  $input['lastname']
            )
        ));

        TicketSoap::process("customer_address.update", array(
            'addressId' =>  $shipping['data']['entity_id'],
            'addressdata' => array(
            'firstname' =>  $input['firstname'],
            'lastname'  =>  $input['lastname']
            )
        ));
        
        $arrAddresses = array(
            array(
                "mode" => "shipping",
                "address_id" => $shipping['data']['entity_id'],
            ),
            array(
                "mode" => "billing",
                "address_id" => $billing['data']['entity_id'],
            )
        );

        $resultCustomerAddresses = TicketSoap::process("cart_customer.addresses", array($shoppingCartId, $arrAddresses ));

        $arrProducts = array(
            array(
                "product_id" => $id,
                "qty" => $input['qty']
            )
        );

        $resultCartProductAdd = TicketSoap::process("cart_product.add", array($shoppingCartId, $arrProducts));
        $shoppingCartProducts = TicketSoap::process("cart_product.list", array($shoppingCartId));

        $shippingMethd = $input['delivery_method'];

        $shipping  = TicketSoap::process("cart_shipping.method", array($shoppingCartId, "{$shippingMethd}_{$shippingMethd}"));

        $sessionProductInfo = array();

        $sessionProductInfo['shoppingCartId'] = $shoppingCartId;

        $sessionProductInfo['order'] = array(
            'order_id'          => '',
            'buyer_id'          => $customerId ,
            'event_id'          => '',
            'product_id'        => $id,
            'qty'               => $input['qty'],
            'amount'            => '',
            'delivery_status'   => 'pending'
        );

        $cart = TicketSoap::process("cart.info", array($shoppingCartId));

		$paymentMethod = array(
            "method" => "checkmo"
        );

        $resultPaymentMethod = TicketSoap::process("cart_payment.method", array($shoppingCartId, $paymentMethod));

        $bf = intval(Options::getOption('booking_fees'));

        $orderInfo=$sessionProductInfo['order'];
        $orderInfo['event_id'] = $_ticket->event_id;

        FootBallEvent::updateMagentoDescription($id);

        $orderNumber = TicketSoap::process("cart.order", array($shoppingCartId, null, null));
        $orderInfo['order_id'] = $orderNumber;

        $buyModel = new FootballTicketBuy();
        $order = TicketSoap::process('sales_order.info', $orderNumber);
        $orderInfo['amount'] = $order['grand_total'];
        $orderInfo['payment_status'] = 'Pending';
        $orderInfo['listed_amount'] = isset($order['items'][0]['price']) ? $order['items'][0]['price'] : $order['base_subtotal'] / $order['total_qty_ordered'];

        $buyModel->fill($orderInfo);
        $buyModel->fees_amount = ($orderInfo['listed_amount'] * ($bf/100))*$order['total_qty_ordered'];
        $buyModel->save();

        TicketSoap::process('sales_order.addComment', array('orderIncrementId' => $orderNumber, 'status' => 'processing', 'comment' => 'Payment has been successful'));

        $paymentInfo['order_id'] = $orderNumber;

        RelatedTicket::updateTicket($orderInfo['product_id'], $orderInfo['qty']);

        //clean session
        Session::put('mid_' . $shoppingCartId, null);

        //if order success create automated invoice
        // Create new invoice
        $newInvoiceId   = TicketSoap::process('sales_order_invoice.create', array($orderNumber, array(), 'Invoice Created', true, true));
        $invoice        = TicketSoap::process('sales_order_invoice.info', $newInvoiceId);

        $this->sendEmailConfirmationToSeller($orderNumber);

	}

	private function sendEmailConfirmationToSeller($orderId = '')
    {
        try {
            FootballTicketBuy::createSaleView();

            $orderDetails = DB::table('sales')->where('order_id', '=', $orderId)->first();

            $event = FootBallEvent::find($orderDetails->event_id);
            $eventTicket = RelatedTicket::where('product_id', '=', $orderDetails->product_id)->first();
            $ticket = RelatedTicket::getTicketByTicketId($orderDetails->product_id);
            $order = TicketSoap::process('sales_order.info', $orderId);
            $tUser = WebUser::where('user_id', '=', $orderDetails->buyer_id)->first();
            $order['shipping_address']['tel']=$order['shipping_address']['telephone'];
            
            $sm = $order['shipping_method'];
            $rateName = explode('_', $sm);
            $rateName = $rateName[0];

            $ships = DB::table('shipping_method_assoc')
                        ->where('name', '=', $rateName)
                        ->first();
                                
            $order['shipping_address']['method']=$ships->description;

            $data = array();
            $data['game'] = $event->title . " - " . date('l, d F Y, h:ia', strtotime($event->datetime)) . ", " . $event->event_location;
            $data['event_id'] = $event->id;
            $data['order_id'] = $orderId;
            $data['total_amount'] = $eventTicket->price * $orderDetails->qty;
            $data['commission'] = ($data['total_amount'] * (empty($eventTicket->selling_commission_percentage) ? 15 : (int)$eventTicket->selling_commission_percentage) / 100);
            $data['total_amount_after_commission'] = $data['total_amount'] - $data['commission'];
            $data['seller_name'] = $orderDetails->seller_firstname . " " . $orderDetails->seller_lastname;
            $data['qty'] = $orderDetails->qty;
            $data['shipping'] = $order['shipping_address'];
            $data['shipping_method'] = $order['shipping_description'];
            $data['site'] = 'www.footballticketpad.com';

            $ticketInfo = json_decode($eventTicket->ticket, true);

            $data['formOfTicket'] = @$ticket->formOfTicket['title'];
            $data['location'] = @$ticket->ticketType['title'];
            $temp = array();
            if (isset($ticketInfo['ticketInformation']['loc_block']) && !empty($ticketInfo['ticketInformation']['loc_block'])) {
                $temp[] = 'Block: ' . $ticketInfo['ticketInformation']['loc_block'];
            }

            if (isset($ticketInfo['ticketInformation']['loc_row']) && !empty($ticketInfo['ticketInformation']['loc_row'])) {
                $temp[] = 'Row: ' . $ticketInfo['ticketInformation']['loc_row'];
            }

            $data['location'] = $data['location'] . ', ' . implode(', ', $temp);

            $temp = array();

            $defaultRestriction = [];
            foreach ($event->getSelectedRestrictions() as $val) {

                $defaultRestriction[] = array(
                    'id' => $val->id,
                    'title' => $val->title
                );
            }

            foreach ($defaultRestriction as $r):
                if (in_array($r['id'], $ticket->info['restrictions'])):
                    $temp[] = $r['title'];
                endif;
            endforeach;

            if ($ticket->buyerNot != ''):
                $temp[] = '<br /> Note: ' . $ticket->buyerNot;
            endif;

            $data['restrictions'] = implode(', ', $temp);

            Mail::send('footballticket::email.sold-ticket-confirmation-to-seller', $data, function ($message) use ($orderDetails, $orderId) {
                $from = Config::get('mail.from');
                $from['address'] = trans('homepage.contact-email');
                $message->from($from['address'], $from['name']); 
                $email = Options::getOption('contact_email') == '' ? 'hello@bondmedia.co.uk' : Options::getOption('contact_email');

                $message->to($orderDetails->seller_email, $orderDetails->seller_firstname . " " . $orderDetails->seller_lastname)
                    ->bcc($email, 'Football Ticket Pad')
                    ->subject("Confirmation of ticket sale: New Order # {$orderId}");
            });

        
        } catch (Exception $e) {
            Log::info($e->getMessage());
            echo $e->getMessage();

        }


    }

	public function getShippingMethods($id){
        $hdAvailable = 0;
        $isEticket = 0;
        $requiresInternational = 0;
        $locale = App::getLocale();

        if(Input::has('rtID')){
            $rtID = Input::get('rtID');
            $rt = RelatedTicket::where('product_id', '=', intval($rtID))->first();
            $hdAvailable = $rt->hand_delivery;

            $ticket = $rt->ticket;
            $ticket=json_decode($ticket);
            $form = DB::table('events_form_of_ticket')
                ->where('id', $ticket->ticketInformation->form_of_ticket)
                ->first();

            if($form->title == "E-tickets"){
                $isEticket = 1;
            }
        }

        if(Input::has('del_country')){
           $delCountry = Input::get('del_country');
        }
        else{
            $delCountry = "United Kingdom";
        }

        $event = FootBallEvent::findOrFail(intval($id));
        $home = $event->home_team_id;
        $away = $event->away_team_id;

        $homeCountry = DB::table('football_ticket_meta')
            ->where('football_ticket_id', $home)
            ->where('key', 'country')
            ->first();

        $awayCountry = DB::table('football_ticket_meta')
            ->where('football_ticket_id', $away)
            ->where('key', 'country')
            ->first();

        $shipping = 0;

        if (Cache::has('shipping')){
           $shipping = Cache::get('shipping');
        } 
        else{
            $shipping = file_get_contents(Config::get("api.mage_soap_api_url").'customer/shipping/index');
            $shipping = json_decode($shipping);
            Cache::put('shipping', $shipping, 600);
        }

        if(($homeCountry->value) && ($awayCountry->value)){
           $homeCountry = DB::table('football_ticket')
                ->where('id', $homeCountry->value)
                ->first(); 

            $awayCountry = DB::table('football_ticket')
                ->where('id', $awayCountry->value)
                ->first();
        }
        else{
            return Response::json($shipping);
        }

        if(($delCountry != $homeCountry->title)&&($delCountry != $awayCountry->title)){
            $delCountry = "International";
            $requiresInternational = 1;
        }

        $now = new DateTime();
        $eventDateTime = $event->datetime;
        $eventDateTime = DateTime::createFromFormat('Y-m-d H:i:s', $eventDateTime);

        $dispatchedDate = $now;
        $requiredArrivalDate = $eventDateTime;
        $requiredArrivalDate->modify("-1 hour");
        $hour = $requiredArrivalDate->format('H');
        $requiredArrivalDate->setTime($hour, 0);

        $dispatchDay = $dispatchedDate->format('l');
        $arrivalDay = $requiredArrivalDate->format('l');

        $canBeDel = 1;
        $requiresHandOrHotel = 0;
        $requiresWeekend = 0;

        $workingDays = $this->weekday_diff($dispatchedDate->format('d-m-Y'), $requiredArrivalDate->format('d-m-Y'));

        $tt1 = DateTime::createFromFormat('Y-m-d', $dispatchedDate->format('Y-m-d'));
        $tt2 = DateTime::createFromFormat('Y-m-d', $requiredArrivalDate->format('Y-m-d'));

        if($isEticket &&($dispatchedDate < $requiredArrivalDate))
        {
            $availOptions = DB::table('shipping_method_assoc')
                ->where('id', 16)
                ->get();

            $shipping2 = new stdClass;
            $shipping2->totalResult=0;
            $shipping2->currentPage = 0;
            $shipping2->numPages = 0;
            $shipping2->totalPages=0;
            $shipping2->data = array();
            $i = 0;
            foreach($availOptions as $option){
                foreach($shipping->data as $entry){
                    if($option->name == $entry->value){ 
                        $pahLD = DB::table('locale_text')
                                    ->where('id', 'ship_pah')
                                    ->where('locale', $locale)
                                    ->first();
                        if($pahLD)
                        {
                            $entry->label = $pahLD->lstring;
                        }  
                        else
                        {
                           $entry->label = "Print at home"; 
                        }
                        
                        $shipping2->data[] = $entry;
                        $i++;
                    }
                }
            }
            $shipping2->totalResult = $i;
            return Response::json($shipping2, 200);
        }

        if($tt1 > $tt2){
            $canBeDel = 0;
        }
        else if($tt1 == $tt2){
            if($dispatchedDate->format('H') < $requiredArrivalDate->format('H')){
                $requiresHandOrHotel = 1;
            }
            else{
                $canBeDel = 0;
            }
        }

        if(($workingDays == 0)&&($tt1<$tt2)){
            $requiresHandOrHotel = 1;
        }
        else if($workingDays == 1)
        {
            if($dispatchDay == "Sunday"){
                $requiresHandOrHotel = 1;
            }
            if(($dispatchDay == "Friday")&&($arrivalDay == "Saturday")){
                $requiresWeekend = 1;
            }
        }

        if(($requiresHandOrHotel)&&(!$hdAvailable)){
            $canBeDel = 0;
        }

        $availOptions = 0;

        if(!$canBeDel){
            $shipping2 = new stdClass;
            $shipping2->totalResult=0;
            $shipping2->currentPage = 0;
            $shipping2->numPages = 0;
            $shipping2->totalPages=0;
            $shipping2->data = array(); 
            return Response::json($shipping2, 200);
        }
         
        $maxDelHour = $requiredArrivalDate->format('H');

        if($requiresInternational){
            $availOptions = DB::table('shipping_method_assoc')
                ->where('id', 15)
                ->whereRaw('(working_days < '. $workingDays.') OR ((working_days = '. $workingDays .') AND (latest_at <= '. $maxDelHour .'))')
                ->get();
        }
        else if($requiresWeekend){
            $availOptions = DB::table('shipping_method_assoc')
                ->whereIn('country', array($delCountry, "International"))
                ->whereRaw('(working_days < '. $workingDays.') OR ((working_days = '. $workingDays .') AND (latest_at <= '. $maxDelHour .'))')
                ->where('isWeekend', '1')
                ->get();
        }
        else if($requiresHandOrHotel){
            $availOptions = DB::table('shipping_method_assoc')
                ->whereIn('country', array($delCountry, "International"))
                ->whereRaw('(working_days < '. $workingDays.') OR ((working_days = '. $workingDays .') AND (latest_at <= '. $maxDelHour .'))')
                ->whereIn('id', array(3,16))
                ->get();
        }
        else if($hdAvailable){
            $availOptions = DB::table('shipping_method_assoc')
                ->whereIn('country', array($delCountry, "International"))
                ->whereRaw('(working_days < '. $workingDays.') OR ((working_days = '. $workingDays .') AND (latest_at <= '. $maxDelHour .'))')
                ->get();
        }
        else{
            $availOptions = DB::table('shipping_method_assoc')
                ->whereIn('country', array($delCountry, "International"))
                ->whereRaw('((working_days < '. $workingDays.') AND (id != 16)) OR ((working_days = '. $workingDays .') AND (latest_at <= '. $maxDelHour .') AND (id != 16))')
                ->get();
        }
        
        $shipping2 = new stdClass;
        $shipping2->currentPage = 0;
        $shipping2->numPages = 0;
        $shipping2->totalPages=0;
        $shipping2->data = array();
        $i = 0;

        $conv = DB::table('locales')
                    ->where('lang', $locale)
                    ->first();
        $conv = $conv->conversion;

        foreach($availOptions as $option){
            foreach($shipping->data as $entry){
                if($option->name == $entry->value){
                    $tranTitle = DB::table('locale_text')
                                    ->where('id', 'ship_desc_'.$option->id)
                                    ->where('locale', $locale)
                                    ->first();
                    if($tranTitle)
                    {
                        $entry->label = $tranTitle->lstring;
                    }
                    $entry->price = $entry->price * $conv;
                    $shipping2->data[] = $entry;
                    $i++;
                }
            }
        }
        $shipping2->totalResult = $i;
        return Response::json($shipping2, 200);
    }

    private function weekday_diff($from, $to, $normalise=true) {
        $_from = is_int($from) ? $from : strtotime($from);
        $_to   = is_int($to) ? $to : strtotime($to);
        // normalising means partial days are counted as a complete day.
        if ($normalise) {
            $_from = strtotime(date('Y-m-d', $_from));
            $_to = strtotime(date('Y-m-d', $_to));
        }
        $all_days = @range($_from, $_to, 60*60*24);
        if (empty($all_days)) return 0;

        $week_days = array_filter(
            $all_days,
            create_function('$t', '$d = date("w", strtotime("+{$t} seconds", 0)); return !in_array($d, array(6,7));')
        );
        return count($week_days);
    }
}