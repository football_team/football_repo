<?php
use Bond\Traits\GridPagination;
use Illuminate\Database\Schema\Blueprint;

class AdminLocalesController extends \BaseController {
    use GridPagination;

    public function index()
    {
    	View::share('menu', 'Locales');
    	return View::make('ticketAdmin::locales.index');
    }

    public function getLocaleDrop()
    {
    	$curLocale = App::getLocale();
    	$locales = DB::table('locales')
    		->select('id', 'title', 'lang')
    		->get();

    	$retString='<select id="locale-select">';
    	foreach($locales as $locale)
    	{
    		$retString .= '<option value="'. $locale->id .'"'. ($locale->lang == $curLocale ? 'selected="selected"':'').'>';
            $retString .= $locale->title;
            $retString .= '</option>';  
    	}  
        $retString.='</select>';

    	return Response::json($retString, 200);
    }

    public function adminSetLocale($id)
    {
    	if(!$id)
    	{
    		return Response::json('No id', 404);
    	}
    	$id = intval($id);
    	$locale = DB::table('locales')
    		->select('lang')
    		->where('id', '=', $id)
    		->first();

    	if(!$locale)
    	{
    		return Response::json('Could not find a locale with id: '.$id, 404);
    	}

    	App::setLocale($locale->lang);
    	Session::put('locale_set', $locale->lang);
    	return Response::json('Locale set to: '.$locale->lang, 200);
    }

    public function getAllLocaleData()
    {
    	$locales = DB::table('locales')
    		->get();

    	$return = array();
    	$data = array();
    	$i = 0;
    	foreach($locales as $locale)
    	{
    		$data[$i] = array();
    		$data[$i]['0'] = $locale->id;
    		$data[$i]['1'] = $locale->title;
    		$data[$i]['2'] = $locale->lang;
    		$data[$i]['3'] = $locale->url;
    		$data[$i]['4'] = '<a href="javascript:;" onclick="editLocale('. $locale->id .')" class="trackingChange">Edit</a>';
    		$i++;
    	}
    	$return['data'] = $data;
    	$return['recordsTotal'] = $i;
    	$return['recordsFiltered'] = $i;

    	$response = Response::make($return);
        $response->header('Content-Type', 'application/json');
        return $response;
    }

    public function editLocale($id)
    {
    	if(!Input::has('title') || !Input::has('lang') || !Input::has('url'))
    	{
    		return Response::json("Fail", 404);
    	}
    	
    	$data = Input::all();

    	$response = DB::table('locales')
    		->where('id', '=', intval($id))
    		->update(['title' => $data['title'], 'lang' => $data['lang'], 'url' => $data['url']]);

    	return Response::json("Success", 200);
    }

    public function fullLocaleDetails($id)
    {
    	$id = intval($id);
    	$locale = DB::table('locales')
    		->where('id', '=', $id)
    		->first();

    	if(!$locale)
    	{
    		return Response::json("Fail", 404);
    	}
    	$data = array();
    	$data['id'] = $locale->id;
    	$data['title'] = $locale->title;
    	$data['lang'] = $locale->lang;
    	$data['url'] = $locale->url;

    	return View::make('ticketAdmin::locales.locale-full')->with('data',$data);
    }

	public function checkLangIndex()
	{
		View::share('menu', 'check-lang');

		$locales = DB::table('locales')
					->get();

		View::share('locales', $locales);

		$r = DB::table('locale_text')
				->select(DB::raw('SUBSTRING_INDEX(id, "_", 1) as prefix'))
				->distinct()
				->get();

		View::share('prefix', $r);

		return View::make('ticketAdmin::locales.language-test');
	}

	public function apiTest()
	{
		$key = '';
		$input = Input::all();
		if(array_key_exists('key', $input))
		{
			$key = Input::get('key');
		}

		$curl = curl_init("http://ws.detectlanguage.com/0.2/user/status");
		curl_setopt($curl, CURLOPT_POST, false);
		curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query(['key'=>$key]));
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($curl);
		curl_close($curl);

		$response = json_decode($response);

		$remaining_requests = $response->daily_requests_limit - $response->requests;
		$remaining_bytes = $response->daily_bytes_limit - $response->bytes;

		return Response::json("Remaining requests: ".$remaining_requests." Remaining Bytes: ".$remaining_bytes);

	}

	public function checkLTextLanguages($locale, $expected, $prefix, $key)
	{
		$allTrans = DB::table('locale_text')
						->where('id', 'LIKE', $prefix.'_%')
						->where('locale', $locale)
						->get();

		//So, these are all translations to be checked for languages
		$responses = array();
		$fails = array();
		foreach($allTrans as $tran)
		{
			$curl = curl_init("http://ws.detectlanguage.com/0.2/detect");
			curl_setopt($curl, CURLOPT_POST, false);
			curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query(['q'=>$tran->lstring, 'key'=>$key]));
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			$response = curl_exec($curl);
			curl_close($curl);

			$comp = array();
			$comp['expected'] = array();
			$comp['others'] = array();
			$comp['identifier'] = $tran->id;
			$comp['translation']=$tran->lstring;

			$response = json_decode($response);

			$exFound = 0;

			foreach($response->data->detections as $detected)
			{
				//Need to check if expected language is here

				if($detected->language == $expected && $detected->isReliable)
				{
					$comp['expected']['language'] = $expected;
					$comp['expected']['is_reliable'] = $detected->isReliable;
					$comp['expected']['score'] = $detected->confidence;
					$exFound = 1;
				}
				else
				{
					$temp = array();
					$temp['language'] = $detected->language;
					$temp['is_reliable'] = $detected->isReliable;
					$temp['score'] = $detected->confidence;
					$comp['others'][] = $temp;
				}
			}

			if(!$exFound)
			{
				$fails[] = $comp;
			}

			$responses[] = $comp;
		}

		View::share('fails', $fails);

		return View::make('ticketAdmin::locales.test-results');
	}
}