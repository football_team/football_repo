<?php
use Bond\Traits\GridPagination;
use Illuminate\Database\Schema\Blueprint;
use \FootBallEvent;

class AdminTicketsController extends \BaseController {
    use GridPagination;
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */

    public function __construct() {
        if(!Schema::hasTable('ticket_view')) {
            DB::statement('CREATE OR REPLACE VIEW  `ticket_view` AS
                        SELECT
                           `ert`.`id` AS `id`,
                           `ert`.`event_id` AS `event_id`,
                           `e`.`title` AS `title`,
                           `ert`.`product_id` AS `product_id`,
                           `ert`.`ticket_status` AS `ticket_status`,
                           `ert`.`price` AS `price`,
                           ert.`ticket` as ticket_info,
                           e.datetime as event_date,
                           e.event_location as location,
                           u.user_id AS user_id,
                           u.firstname,
                           u.lastname
                        FROM `events_related_tickets` `ert`
                        join `events` `e` on `e`.`id` = `ert`.`event_id`
                        left join web_user as u ON u.user_id = ert.`user_id`');
        }
    }
    public function index()
    {
        View::share('menu', 'tickets');
        return View::make('ticketAdmin::tickets.list');
    }


    public function ticketList () {

        $columns  = array(
            0 => 'product_id',
            1 => 'title',
            2 => 'firstname',
            3 => 'lastname',
            4 => 'price',
            5 => 'ticket_info',
            6 => 'event_date',
            7 => 'ticket_status'
        );

        $bf = 1 + (intval(Options::getOption('booking_fees'))/100);

        $results = $this->getGridData('ticket_view', $columns);
        $data = $results['data'];
        $output = [];
        foreach($data as $d) {
            $idLink = '<a href="javascript:;" onclick="postShowFullInfo('. $d[0] .')" class="trackingChange">'. $d[0] .'</a>';
            $temp = [];
            $temp[0] = $idLink;
            $temp[1] = $d[1];
            $temp[2] = date('d/m/y H:i', strtotime( $d[6]));
            $temp[3] = $d[2]." ".$d[3];
            $temp[4] = "£".number_format(($d[4]*$bf), 2);
            $ticket = @json_decode($d[5], true);
            $temp[5] = (int) isset($ticket['ticketInformation']['number_of_ticket'])? $ticket['ticketInformation']['number_of_ticket']: 0;
            $temp[6] = $d[7];
            $temp[7] =  $d[8];
            $output[] = $temp;
        }


        $results['data'] = $output;
        $response = Response::make($results);
        $response->header('Content-Type', 'application/json');
        return $response;
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {

    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {

    }


    /**
     * Display the specified resource.
     *rm
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        View::share('menu', 'tickets/edit');

        $node = DB::table('ticket_view')->where('id', '=',$id)->first();

        if(!$node) {
            App::abort(404);
        }

        View::share('node', $node);

        return View::make('ticketAdmin::tickets.edit');
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        $event_id = (int) Input::get('event_id');
        $product_id = (int) Input::get('product_id');

        $ticket = RelatedTicket::where('event_id', '=', $event_id)->where('product_id', '=',$product_id)->first();

        $ticket->ticket_status = Input::get('ticket_status');
        $ticket->save();

        Notification::success('Ticket has been updated!');
        return Redirect::route('admin.tickets.info.edit', array('id' => $id));
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    public function remove() {

    }

    public function getFullListingDetailsById($id)
    {
        if(!$id)
        {
            return Response::json('Must supply an order_id', 404);
        }

        $ticket = RelatedTicket::getTicketByTicketId($id);
        $info = $ticket->ticket;

        $uid = DB::table('events_related_tickets')->where('product_id', $id)->pluck('user_id');
        $user = DB::table('web_user')->where('user_id', $uid)->first();

        $return = array();
        $return['seller'] = array();
        $return['event'] = array();

        //var_export($user,false);s
        $payment_method = '';
        if(isset($info->paymentMethod->payment_option) && $info->paymentMethod->payment_option == 'bank') {
            $payment_method = 'Bank <br/>Account Holder:'.$info->paymentMethod->account_holder."<br />".'IBAN:'.$info->paymentMethod->iban."<br />BIC/SWIFT:".$info->paymentMethod->bic_swift;
        } else {
            $payment_method = "PayPal: ".$info->paymentMethod->paypal_email;
        }

        $return['seller']['name'] = $user->firstname . ' ' . $user->lastname;
        $return['seller']['tel'] = $user->telephone;
        $return['seller']['email'] = $user->email;
        $return['seller']['type'] = $user->seller_type;
        $return['seller']['payment_option'] = $payment_method;

        $return['event']['name'] = $ticket->title;
        $return['event']['time'] = $ticket->datetime;
        $return['event']['location'] = $ticket->event_location;
        $return['event']['type'] = $ticket->ticketType['title'];
        $return['event']['block'] = $ticket->info['loc_block'];
        $return['event']['row'] = $ticket->info['loc_row'];
        $return['event']['total_listed'] = $ticket->available_qty;
        $return['event']['additional_info'] = $ticket->buyerNot;
        $restrictions = false;
        $return['event']['restrictions'] = $restrictions;
        $return['product_id']=$id;

        return View::make('ticketAdmin::tickets.full')->with('return',$return);
    }
}
