<?php

class AdminAffiliateController extends \BaseController {
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function __construct()
    {
        $group = 0;
        try
        {
            $group = Sentry::findGroupByName('Affiliate');
        }
        catch(Cartalyst\Sentry\Groups\GroupNotFoundException $ex)
        {
            Log::info("Affiliate user group not found.");
        }
        catch(Exception $ex)
        {
            Log::info('Exception finding Affiliate user group.');
        }

        if (!$group)
        {
            try
            {
                $group = Sentry::createGroup(array(
                    'name' => 'Affiliate',
                    'permissions' => array(
                        'users' => 1
                    ),
                ));
            }
            catch(Cartalyst\Sentry\Groups\GroupExistsException $ex)
            {
                Log::info("Group already exists.");
            }
            catch(Exception $ex)
            {
                Log::info("Exception creating Affiliate user group.");
            }
        }
    }

    public function requests()
    {
        View::share('menu', "affiliates/requests");
        return View::make('ticketAdmin::affiliates.affiliate-requests');
    }

    public function getRequests()
    {
        $start = 0;
        $limit = 10;
        if(Input::has('start'))
        {
            $start = Input::get('start');
        }
        if(Input::has('length'))
        {
            $limit = Input::get('length');
        }

        $output = [];
        $total = 0;

        $requests = AffiliateRequest::where('is_approved', 0)->where('is_rejected', 0)->where('is_approved',0)->where('ver_code', "")->take($limit)->skip($start)->get();

        foreach($requests as $request)
        {
            $temp = [];
            $temp['0'] = $request->id;
            $temp['1'] = $request->company_name;
            $temp['2'] = '<a href="'.$request->company_url.'" target="blank">'.$request->company_url.'</a>';
            $temp['3'] = '<a href="mailto:'.$request->email.'">'.$request->email.'</a>';
            $temp['4'] = $request->contact_name;

            $temp['5'] = '<select id = "agent-assign-'. $request->id .'" >';
            $temp['5'] .= '<option value="">--</option>';
            $agents = Agent::where('status', 'active')->get();
            foreach($agents as $agent)
            {
                $temp['5'] .= '<option value="'.$agent->id.'">'.$agent->fname.' '.$agent->lname.' : '.$agent->agent_perc.'%</option>';
            }
            $temp['5'].='</select>';

            $temp['6'] = '<button class="approve-request" data-id="'.$request->id.'">Approve</button>';
            $temp['6'] .= '<button class="cancel-request" data-id="'.$request->id.'">Reject</button>';

            $output[] = $temp;

            $total++;
        }

        $results['data'] = $output;
        $results['recordsTotal'] = $total;
        $results['recordsFiltered'] = $total;
        $response = Response::make($results);
        $response->header('Content-Type', 'application/json');
        return $response;
    }

    public function getPending()
    {
        $start = 0;
        $limit = 10;
        if(Input::has('start'))
        {
            $start = Input::get('start');
        }
        if(Input::has('length'))
        {
            $limit = Input::get('length');
        }

        $output = [];
        $total = 0;

        $requests = AffiliateRequest::where('is_approved', 0)->where('is_rejected', 0)->where('ver_code', '!=' ,"")->take($limit)->skip($start)->get();
        foreach($requests as $request)
        {
            $agName = "";
            if($request->agent_id)
            {
                $agent = Agent::where('id', $request->agent_id)->first();
                $agName = $agent->fname.' '.$agent->lname.' : '.$agent->agent_perc;
            }
            $temp = [];

            $temp['0'] = $request->id;
            $temp['1'] = $request->company_name;
            $temp['2'] = $request->company_url;
            $temp['3'] = $request->email;
            $temp['4'] = $request->contact_name;
            $temp['5'] = $request->ver_code;
            $temp['6'] = $agName;
            $temp['7'] = '<button class="cancel-inactive" data-id="'.$request->id.'">Cancel</button><br><button class="skip-ver" data-id="'.$request->id.'">Skip Verification</button>';

            $output[] = $temp;

            $total++;
        }

        $results['data'] = $output;
        $results['recordsTotal'] = $total;
        $results['recordsFiltered'] = $total;
        $response = Response::make($results);
        $response->header('Content-Type', 'application/json');
        return $response;
    }

    public function approveAffiliateRequest($id)
    {
        $agent_id = Input::get('agent_id');
        if($agent_id == "none")
        {
            $agent_id = null;
        }

        $request = AffiliateRequest::where('id', $id)->first();
        $code = md5($request->email.md5($request->contact_name.$request->company_name.time()));
        $approver_id = Sentry::getUser()->id;

        $request->ver_code = $code;
        $request->approver_id = $approver_id;
        $request->agent_id = $agent_id;

        $request->save();

        $emailTemplate = 'ticketAdmin::emails.affiliate-request-approval';

        Mail::send($emailTemplate, $request->toArray(), function ($message) use($request) {
            $from = Config::get('mail.from');
            $message->from($from['address'], $from['name']);
            $email = Options::getOption('contact_email') == ''? 'birkystomper99@gmail.com': Options::getOption('contact_email');

            $message->to($request->email, $request->contact_name )
                ->bcc($email, 'Football Ticket Pad')
                ->subject("Affiliate Request Approval");
        });

        return Response::json('Approved', 200);
    }

    public function rejectAffiliateRequest($id)
    {
        $request = AffiliateRequest::where('id', $id)->first();
        $request->is_rejected = 1;
        $request->save();

        $emailTemplate = 'ticketAdmin::emails.affiliate-request-rejected';

        Mail::send($emailTemplate, $request->toArray(), function ($message) use($request) {
            $from = Config::get('mail.from');
            $message->from($from['address'], $from['name']);
            $email = Options::getOption('contact_email') == ''? 'birkystomper99@gmail.com': Options::getOption('contact_email');

            $message->to($request->email, $request->contact_name )
                ->bcc($email, 'Football Ticket Pad')
                ->subject("Affiliate Request Denial");
        });

        return Response::json('Rejected', 200);
    }

    public function cancelPendingAffiliate($id)
    {
        $request = AffiliateRequest::where('id', $id)->first();
        $request->is_rejected = 1;
        $request->ver_code = "";

        $request->save();

        $emailTemplate = 'ticketAdmin::emails.affiliate-request-cancel';

        Mail::send($emailTemplate, $request->toArray(), function ($message) use($request) {
            $from = Config::get('mail.from');
            $message->from($from['address'], $from['name']);
            $email = Options::getOption('contact_email') == ''? 'birkystomper99@gmail.com': Options::getOption('contact_email');

            $message->to($request->email, $request->contact_name )
                ->bcc($email, 'Football Ticket Pad')
                ->subject("Affiliate Request Cancelled");
        });

        return Response::json('Cancelled', 200);
    }

    public function paymentRequests()
    {
        View::share('menu', "affiliates/payment-requests");
        return View::make('ticketAdmin::affiliates.affiliate-payment-requests');
    }

    public function getPaymentRequestDataTable()
    {
        $start = 0;
        $limit = 10;
        if(Input::has('start'))
        {
            $start = Input::get('start');
        }
        if(Input::has('length'))
        {
            $limit = Input::get('length');
        }

        //So get the payment requests, left join the properties, get the current balance.

        $output = [];
        $total = 0;

        $paymentRequests = DB::table('affiliate_withdrawal_requests as apr')
                            ->leftJoin('affiliate_properties as ap', 'apr.affiliate_id', '=' , 'ap.id')
                            ->leftJoin('affiliate_balance as ab', 'ap.id', '=', 'ab.affiliate_id')
                            ->where('apr.status', 'pending')
                            ->select('ap.id as id',
                                'ap.company_name',
                                'ap.contact_name as contact_name',
                                'ab.balance as balance',
                                'apr.requested_for as requested_for',
                                'apr.amount as amount',
                                'ap.account_holder_name as account_holder_name',
                                'ap.account_number as account_number',
                                'ap.sort_code as sort_code',
                                'ap.IBAN as IBAN',
                                'ap.SWIFT as SWIFT',
                                'apr.id as apr_id'
                            )
                            ->get();

        foreach($paymentRequests as $request)
        {
            $temp = [];

            $temp['0'] = $request->id;
            $temp['1'] = $request->company_name;
            $temp['2'] = $request->contact_name;
            $temp['3'] = '£'.$request->balance;
            $temp['4'] = $request->requested_for;
            $temp['5'] = '£'.$request->amount;
            $temp['6'] = $request->account_holder_name;
            $temp['7'] = $request->account_number;
            $temp['8'] = $request->sort_code;
            $temp['9'] = $request->IBAN;
            $temp['10'] = $request->SWIFT;
            $temp['11'] = '<select id="status-'.$request->apr_id.'"><option val="pending">Pending</option><option val="paid">Paid</option><option val="cancelled">Cancelled</option></select>';
            $temp['12'] = '<button class="save" data-id="'.$request->apr_id.'">Save</button>';

            $output[] = $temp;

            $total++;
        }

        $results['data'] = $output;
        $results['recordsTotal'] = $total;
        $results['recordsFiltered'] = $total;
        $response = Response::make($results);
        $response->header('Content-Type', 'application/json');
        return $response;

    }

    public function updatePaymentRequestStatus(){

        if(Input::has('id')&&Input::has('status'))
        {
            $id = Input::get('id');
            $status = Input::get('status');
            $paymentRequest = AffiliateWithdrawal::where('id', $id)->first();

            switch($status){
                case "Pending":{
                    $paymentRequest->status = "pending";
                    $paymentRequest->save();
                    return Response::json('success', 200);
                    break;
                }
                case "Cancelled":{
                    $paymentRequest->status = "cancelled";
                    $paymentRequest->save();
                    $this->emailCancelledRequest($paymentRequest);
                    break;
                }
                case "Paid":{
                    $paymentRequest->status = "paid";
                    if($this->deductFromBalance($paymentRequest))
                    {
                        $paymentRequest->save();
                        $this->emailPaidRequest($paymentRequest);
                    }
                    else
                    {
                        return Response::json("Requested withdrawal is greater than affiliate balance.", 404);
                    }
                    break;
                }
                default:{
                    return Response::json("Invalid payment request status posted.", 404);
                }
            }
        }
        else
        {
            return Response::json("Requires id and status input");
        }
    }

    public function emailCancelledRequest($paymentRequest)
    {
        //email the affiliate and say it is paid
        $ap = AffiliateProperty::where('id', $paymentRequest->affiliate_id)->first();
        $contactName = $ap->contact_name;

        $balance = AffiliateBalance::where('affiliate_id', $paymentRequest->affiliate_id)->first();
        $balance = $balance->balance;
        $amount = $paymentRequest->amount;

        $user = Sentry::findUserById($ap->user_id);
        $mail = $user->email;

        $ap->email = $mail;

        $r2['reqId'] = $paymentRequest->id;
        $r2['amount'] = '£'.$amount;
        $r2['contact_name'] = $contactName;

        $emailTemplate = 'Affiliates::emails.affiliate-withdrawal-cancelled';

        Mail::send($emailTemplate, $r2, function ($message) use($ap) {
            $from = Config::get('mail.from');
            $message->from($from['address'], $from['name']);
            $email = Options::getOption('contact_email') == ''? 'birkystomper99@gmail.com': Options::getOption('contact_email');

            $message->to($ap->email, $ap->contact_name )
                ->bcc($email, 'Football Ticket Pad')
                ->subject("Affiliate Payment Request Cancelled");
        });

        return true;
    }

    public function emailPaidRequest($paymentRequest)
    {
        //email the affiliate and say it is paid
        $ap = AffiliateProperty::where('id', $paymentRequest->affiliate_id)->first();
        $contactName = $ap->contact_name;

        $balance = AffiliateBalance::where('affiliate_id', $paymentRequest->affiliate_id)->first();
        $balance = $balance->balance;
        $amount = $paymentRequest->amount;

        $user = Sentry::findUserById($ap->user_id);
        $mail = $user->email;

        $ap->email = $mail;

        $r2['balance'] = $balance;
        $r2['amount'] = '£'.$amount;
        $r2['contact_name'] = $contactName;

        $emailTemplate = 'Affiliates::emails.affiliate-withdrawal-paid';

        Mail::send($emailTemplate, $r2, function ($message) use($ap) {
            $from = Config::get('mail.from');
            $message->from($from['address'], $from['name']);
            $email = Options::getOption('contact_email') == ''? 'birkystomper99@gmail.com': Options::getOption('contact_email');

            $message->to($ap->email, $ap->contact_name )
                ->bcc($email, 'Football Ticket Pad')
                ->subject("Affiliate Payment Request Completed");
        });

        return true;
    }

    public function deductFromBalance($paymentRequest)
    {
        $balance = AffiliateBalance::where('affiliate_id', $paymentRequest->affiliate_id)->first();

        if($balance->balance >= $paymentRequest->amount)
        {
            $balance->balance = $balance->balance - $paymentRequest->amount;
            $balance->save();
            return true;
        }
        return false;
    }

    public function paymentRequestTotals()
    {
        $reqTotals = AffiliateWithdrawal::getTotals();
        $balanceTotals = AffiliateBalance::getTotals();

        $temp = array();

        $temp['0'] = $reqTotals->numberPending;
        $temp['1'] = '£'.$reqTotals->amountPending;
        $temp['2'] = '£'.$balanceTotals->total;
        $temp['3'] = '£'.($balanceTotals->total - $reqTotals->amountPending);

        $results = array();
        $results['data'] = array();
        $results['data']['0'] = $temp;
        $results['recordsTotal'] = 1;
        $results['recordsFiltered'] = 1;
        $response = Response::make($results);
        $response->header('Content-Type', 'application/json');
        return $response;
    }


    public function closedAffiliatePayments()
    {
        View::share('menu', "affiliates/closed-requests");
        return View::make('ticketAdmin::affiliates.closed-affiliate-payment-requests');
    }

    public function paidAffiliatePayments()
    {
        $start = 0;
        $limit = 10;
        if(Input::has('start'))
        {
            $start = Input::get('start');
        }
        if(Input::has('length'))
        {
            $limit = Input::get('length');
        }

        //So get the payment requests, left join the properties, get the current balance.

        $output = [];
        $total = 0;

        $data = [];

        $paid = AffiliateWithdrawal::getPaid($limit, $start);

        foreach($paid as $req)
        {
            $temp = [];

            $temp['0'] = $req->id;
            $temp['1'] = $req->company_name;
            $temp['2'] = $req->requested_for;
            $temp['3'] = '£'.$req->amount;
            $temp['4'] = $req->updated_at;
            $temp['5'] = $req->account_holder_name;
            $temp['6'] = $req->account_number;
            $temp['7'] = $req->sort_code;
            $temp['8'] = $req->SWIFT;
            $temp['9'] = $req->IBAN;

            $data[] = $temp;

            $total++;

        }

        $results = array();
        $results['data'] = $data;
        $results['recordsTotal'] = $total;
        $results['recordsFiltered'] = $total;
        $response = Response::make($results);
        $response->header('Content-Type', 'application/json');

        return $response;

    }

    public function cancelledAffiliatePayments()
    {
        $start = 0;
        $limit = 10;
        if(Input::has('start'))
        {
            $start = Input::get('start');
        }
        if(Input::has('length'))
        {
            $limit = Input::get('length');
        }

        $output = [];
        $total = 0;

        $data = [];

        $paid = AffiliateWithdrawal::getCancelled($limit, $start);

        foreach($paid as $req)
        {
            $temp = [];

            $temp['0'] = $req->id;
            $temp['1'] = $req->company_name;
            $temp['2'] = $req->requested_for;
            $temp['3'] = '£'.$req->amount;
            $temp['4'] = $req->updated_at;

            $data[] = $temp;

            $total++;
        }

        $results = array();
        $results['data'] = $data;
        $results['recordsTotal'] = $total;
        $results['recordsFiltered'] = $total;
        $response = Response::make($results);
        $response->header('Content-Type', 'application/json');

        return $response;
    }

    public function config()
    {
        View::share('menu', "affiliates/config");

        $cfg_options = AffiliateConfig::all();

        View::share('cfgs', $cfg_options);
        return View::make('ticketAdmin::affiliates.affiliate-config');

    }

    public function getMainRateList()
    {
        //Group by rate id
        $rates = DB::table('affiliate_payment_rate')
                    ->groupBy('rate_id')
                    ->selectRaw('
                        rate_id, rate_name, (SELECT COUNT(*) FROM affiliate_rate WHERE affiliate_rate.rate_id = affiliate_payment_rate.rate_id) as inUse
                    ')
                    ->get();

        $total = 0;

        $data = [];

        foreach($rates as $rate)
        {
            $temp = array();
            $temp['0'] = '<a href="" class="rateModalShow" data-id="'.$rate->rate_id.'">'.$rate->rate_id.'</a>';
            $temp['1'] = '<input type="text" id="rate-name-'.$rate->rate_id.'" value="'.$rate->rate_name.'">';
            $temp['2'] = $rate->inUse;
            $temp['3'] = '<button class="updateRate" data-id="'.$rate->rate_id.'">Save</button>';
            $temp['4'] = '<button class="deleteRate" data-id="'.$rate->rate_id.'">Delete</button>';

            $data[] = $temp;
            $total++;
        }

        $results = array();
        $results['data'] = $data;
        $results['recordsTotal'] = $total;
        $results['recordsFiltered'] = $total;
        $response = Response::make($results);
        $response->header('Content-Type', 'application/json');

        return $response;
    }

    public function updateRateName()
    {
        if(Input::has('rateName') && Input::has('rateID'))
        {
            $name = Input::get('rateName');
            $rate_id = Input::get('rateID');


            $rate = DB::table('affiliate_payment_rate')
                    ->where('rate_id', $rate_id)
                    ->first();

            if($rate)
            {
                DB::table('affiliate_payment_rate')
                    ->where('rate_id', $rate_id)
                    ->update([
                        'rate_name'=> $name
                    ]);
            }
            else
            {
                return Response::json("Could not find rate", 404);
            }
        }
        else
        {
            return Response::json("Requires rate name and id", 404);
        }
    }

    public function deleteRate()
    {
        if(Input::has('rateID'))
        {

            $rate_id = Input::get('rateID');

            $rate = DB::table('affiliate_payment_rate')
                ->groupBy('rate_id')
                ->where('rate_id', $rate_id)
                ->selectRaw('
                        rate_id, rate_name, (SELECT COUNT(*) FROM affiliate_rate WHERE affiliate_rate.rate_id = affiliate_payment_rate.rate_id) as inUse
                    ')
                ->first();

            if($rate)
            {
                if($rate->inUse > 0)
                {
                    return Response::json("Could not complete as rate is in use.", 404);
                }

                DB::table('affiliate_payment_rate')
                    ->where('rate_id', $rate_id)
                    ->delete();

                return Response::json("Success", 200);
            }
            else
            {
                return Response::json("Could not find rate", 404);
            }


        }
        else
        {
            return Response::json("Requires rateID", 404);
        }
    }

    public function updateConfig(){
        if(Input::has('key') && Input::has('value'))
        {
            $key = Input::get('key');
            $value = intval(Input::get('value'));

            if(is_numeric($value))
            {
                $cfg = AffiliateConfig::where('key', $key)->first();

                if($cfg)
                {
                    $cfg->value = $value;
                    $cfg->save();

                    return Response::json("Success", 200);
                }
                else
                {
                    return Response::json("Could not find config.",404);
                }
            }
            else
            {
                return Response::json("Value must be an integer.",404);
            }
        }
        return Response::json("Requires key value pair.",404);
    }

    public function getAllRateMilestones()
    {
        if(Input::has('rateID'))
        {
            $rateID = Input::get('rateID');

            $rate = DB::table('affiliate_payment_rate')
                        ->where('rate_id', $rateID)
                        ->first();

            if($rate)
            {
                $allRates = DB::table('affiliate_payment_rate')
                                ->where('rate_id', $rateID)
                                ->orderBy('sale_milestone', 'ASC')
                                ->get();

                View::share('rates', $allRates);
                return View::make('ticketAdmin::affiliates.rate-table');
            }
            else
            {
                return Response::json("Could not find matching rate.", 404);
            }
        }
        return Response::json('Requires rate id', 404);
    }

    public function updateMilestone()
    {
        if(Input::has('milestone') && Input::has('percentage') && Input::has('id'))
        {
            $ms = Input::get('milestone');
            $perc = Input::get('percentage');
            $id = Input::get('id');

            if(is_numeric($ms) && is_numeric($perc) && is_numeric($id))
            {
                $rate = DB::table('affiliate_payment_rate')
                        ->where('id', $id)
                        ->first();

                if($rate)
                {
                    DB::table('affiliate_payment_rate')
                        ->where('id', $id)
                        ->update([
                            'sale_milestone' => $ms,
                            'rate_perc' => $perc
                        ]);

                    $allRates = DB::table('affiliate_payment_rate')
                        ->where('rate_id', $rate->rate_id)
                        ->get();

                    View::share('rates', $allRates);
                    return View::make('ticketAdmin::affiliates.rate-table');
                }
                return Response::json('Rate not found', 404);
            }
            else
            {
                return Response::json("All entries must be numeric", 404);
            }
        }
        return Response::json('Requires milestone, percentage and id',404);
    }

    public function deleteMilestone()
    {
        if(Input::has('id'))
        {
            $id = Input::get('id');

            $rate = DB::table('affiliate_payment_rate')
                ->where('id', $id)
                ->first();

            if($rate)
            {
                $count = DB::table('affiliate_payment_rate')
                    ->where('rate_id', $rate->rate_id)
                    ->select(DB::raw('COUNT(*) as count'))
                    ->first();

                if($count->count > 1)
                {
                    DB::table('affiliate_payment_rate')
                        ->where('id', $rate->id)
                        ->delete();

                    $allRates = DB::table('affiliate_payment_rate')
                        ->where('rate_id', $rate->rate_id)
                        ->get();

                    View::share('rates', $allRates);
                    return View::make('ticketAdmin::affiliates.rate-table');
                }
                else
                {
                    return Response::json('Requires > 1 milestone associated with rate.',404);
                }
            }
            else
            {
                return Response::json('Could not find rate.',404);
            }
        }
        else
        {
            return Response::json('Requires id.',400);
        }
    }

    public function addNewMilestone(){
        if(Input::has('rate_id') && Input::has('milestone') && Input::has('percentage'))
        {
            $rID = Input::get('rate_id');
            $ms = Input::get('milestone');
            $perc = Input::get('percentage');

            if(is_numeric($rID) && is_numeric($ms) && is_numeric($perc))
            {
                $rID = intval($rID);
                $ms = intval($ms);
                $perc = intval($perc);

                $exRate = DB::table('affiliate_payment_rate')
                                ->where('rate_id', $rID)
                                ->first();

                if($exRate)
                {
                    DB::table('affiliate_payment_rate')
                        ->insert([
                            'rate_id'=>$rID,
                            'sale_milestone' => $ms,
                            'rate_perc' => $perc,
                            'rate_name' => $exRate->rate_name
                        ]);

                    $allRates = DB::table('affiliate_payment_rate')
                        ->where('rate_id', $rID)
                        ->get();

                    View::share('rates', $allRates);
                    return View::make('ticketAdmin::affiliates.rate-table');
                }
                else
                {
                    return Response::json("Could not find rate.");
                }
            }
            else
            {
                return Response::json("All fields must be whole numbers.", 404);
            }
        }
        else
        {
            return Response::json('requires rate id, milestone and percentage.');
        }
    }

    public function addNewRate()
    {
        if(Input::has('name') && Input::has('startRate'))
        {
            $name = Input::get('name');
            $perc = Input::get('startRate');
            if(is_numeric($perc))
            {
                if($name != "")
                {
                    $perc = intval($perc);

                    $max = DB::table('affiliate_payment_rate')
                        ->select(DB::raw('MAX(rate_id) as max'))
                        ->first();

                    $max = $max->max;
                    $max++;

                    DB::table('affiliate_payment_rate')
                        ->insert([
                            'rate_id'=>$max,
                            'sale_milestone'=>0,
                            'rate_perc'=>$perc,
                            'rate_name'=>$name
                        ]);

                    return Response::json("Success",200);
                }
                else
                {
                    return Response::json("Name cannot be blank", 404);
                }
            }
            else
            {
                return Response::json("Start rate must be a whole number", 404);
            }
        }
        else
        {
            return Response::json("Requires name and start rate", 404);
        }
    }

    public function affiliateProperties()
    {
        View::share('menu', "affiliates/properties");

        return View::make('ticketAdmin::affiliates.affiliate-properties');
    }

    public function affiliatePropertiesDataTable()
    {
        $start = 0;
        $limit = 10;
        if(Input::has('start'))
        {
            $start = Input::get('start');
        }
        if(Input::has('length'))
        {
            $limit = Input::get('length');
        }

        $output = [];
        $total = 0;

        $data = [];

        $results = DB::table('affiliate_properties as ap')
                    ->leftJoin('users as u', 'ap.user_id', '=', 'u.id')
                    ->leftJoin('affiliate_rate as ar', 'ap.id', '=', 'ar.affiliate_id')
                    ->leftJoin('affiliate_payment_rate as apr', 'ar.rate_id', '=', 'apr.id')
                    ->leftJoin('affiliate_balance as bal', 'ap.id', '=', 'bal.affiliate_id')
                    ->select('ap.id as id', 'ap.company_name as company_name', 'ap.company_url as url' ,'u.email as email', 'ap.contact_name as contact_name',
                        'ap.ref_code as ref_code', 'ar.rate_id as rate_id', 'apr.rate_name as rate_name', 'bal.balance as balance', 'ap.agent_id as agent_id', 'ap.show_fees')
                    ->take($limit)
                    ->skip($start)
                    ->get();

        $rates = DB::table('affiliate_payment_rate')
                    ->select('rate_id', 'rate_name')
                    ->distinct()
                    ->get();

        foreach($results as $req)
        {
            $prop = AffiliateProperty::where('id', $req->id)->first();
            $temp = [];

            $sel = '<select class="rateSelect" data-id="'.$req->id.'">';

            foreach($rates as $rate)
            {
                if($req->rate_id == $rate->rate_id)
                {
                    $sel .= '<option value="'.$rate->rate_id.'" selected="">'.$rate->rate_name.'</option>';
                }
                else
                {
                    $sel .= '<option value="'.$rate->rate_id.'">'.$rate->rate_name.'</option>';
                }
            }
            $sel .= '</select>';

            $temp['0'] = $req->id;
            $temp['1'] = $req->company_name;
            $temp['2'] = $req->url;
            $temp['3'] = $req->email;
            $temp['4'] = $req->contact_name;
            $temp['5'] = $req->ref_code;
            $temp['6'] = '£'.number_format($req->balance,2,'.','');
            $temp['7'] = $prop->getCurrentRate();
            $temp['8'] = $sel;

            $temp['9'] = '<select id = "agent_assign_'.$req->id.'" class="agent_assign" data-id="'.$req->id.'">';
            $agents = Agent::where('status', 'active')->get();
            if($req->agent_id)
            {
                $temp['9'].='<option value="none">--</option>';
                foreach($agents as $agent)
                {
                    if($agent->id == $req->agent_id)
                    {
                        $temp['9'].='<option value="'.$agent->id.'" selected>'.$agent->fname.' '.$agent->lname.' : '.$agent->agent_perc.'</option>';
                    }
                    else
                    {
                        $temp['9'].='<option value="'.$agent->id.'">'.$agent->fname.' '.$agent->lname.' : '.$agent->agent_perc.'</option>';
                    }
                }
            }
            else
            {
                $temp['9'].='<option value="none" selected>--</option>';
                foreach($agents as $agent)
                {
                    $temp['9'].='<option value="'.$agent->id.'">'.$agent->fname.' '.$agent->lname.' : '.$agent->agent_perc.'</option>';
                }
            }
            $temp['9'] .="</select>";

            $temp['10'] = '<select class="fees_toggle" data-id="'.$req->id.'">';
            if($req->show_fees)
            {
                $temp['10'] .= '<option value="1" selected>Yes</option>';
                $temp['10'] .= '<option value="0">No</option>';
            }
            else
            {
                $temp['10'] .= '<option value="1">Yes</option>';
                $temp['10'] .= '<option value="0" selected>No</option>';
            }
            $temp['10'] .= '</select>';

            $data[] = $temp;

            $total++;
        }

        $results = array();
        $results['data'] = $data;
        $results['recordsTotal'] = $total;
        $results['recordsFiltered'] = $total;
        $response = Response::make($results);
        $response->header('Content-Type', 'application/json');

        return $response;
    }

    public function updateAffiliateRate()
    {
        if(Input::has('property') && Input::has('newRate'))
        {
            $pID = Input::get('property');
            $rID = Input::get('newRate');
            $prop = AffiliateProperty::where('id', $pID)->first();

            if($prop)
            {
                $rate = DB::table("affiliate_payment_rate")->where('rate_id',$rID)->first();
                if($rate)
                {
                    DB::table('affiliate_rate')
                        ->where('affiliate_id', $pID)
                        ->update([
                            'rate_id'=>$rID
                        ]);

                    $prop->getCurrentRate();

                    return Response::json('success',200);
                }
                else
                {
                    return Response::json("Could not find rate",404);
                }
            }
            else
            {
                return Response::json("Could not find property",404);
            }
        }
        else
        {
            return Response::json("Requires property and rate id's",404);
        }
    }

    public function updateAffiliateAgent($id)
    {
        $af = AffiliateProperty::where('id', $id)->first();

        if($af)
        {
            if(Input::has('agent_id'))
            {
                $agent_id = Input::get('agent_id');
                if($agent_id == "none")
                {
                    $af->agent_id = null;
                    $af->save();

                    return Response::json("success",200);
                }
                else
                {
                    $agent = Agent::where('id', $agent_id)->first();

                    if($agent)
                    {
                        $af->agent_id = $agent->id;
                        $af->save();

                        return Response::json('success',200);
                    }
                    return Response::json("Could not find agent with id: ".$agent_id, 404);
                }
            }
            return Response::json("Required agent id or none",404);
        }

        return Response::json("Could not find affiliate proprty.",404);
    }

    public function referralIndex(){
        $dt = new DateTime();
        $dt = $dt->format('Y-m-d');

        View::share('dt', $dt);

        View::share('menu', 'affiliates/referrals');

        return View::make('ticketAdmin::affiliates.affiliate-referral');
    }

    public function getReferralDataByDate()
    {
        if(Input::has('date'))
        {
            $date = Input::get('date');

            $da = DateTime::createFromFormat('Y-m-d', $date);
            $dt = DateTime::createFromFormat('Y-m-d', $date);
            $dt->modify('+1 month');

            $da = $da->format('Y-m');
            $dt = $dt->format('Y-m');

            if($da && $dt)
            {
                $start = 0;
                $limit = 10;
                if(Input::has('start'))
                {
                    $start = Input::get('start');
                }
                if(Input::has('length'))
                {
                    $limit = Input::get('length');
                }

                $output = [];
                $total = 0;

                $data = [];

                $results = DB::table('affiliate_properties as ap')
                    ->get();

                foreach($results as $result)
                {
                    $refs = DB::table('referral_hits')
                                ->where('affiliate_id', $result->id)
                                ->where('created_at', '>=', $da)
                                ->where('created_at', '<', $dt)
                                ->select(DB::raw('COUNT(*) as num_hits'))
                                ->first();

                    $sales = DB::table('affiliate_sales')
                                ->leftJoin('events_ticket_buy', 'events_ticket_buy.order_id', '=', 'affiliate_sales.order_id')
                                ->where('affiliate_id', $result->id)
                                ->where('created_at', '>=', $da)
                                ->where('created_at', '<', $dt)
                                ->select(DB::raw('COUNT(*) as num_sales'))
                                ->first();

                    $temp = [];

                    $temp['0'] = $result->id;
                    $temp['1'] = $result->company_name;
                    $temp['2'] = $refs->num_hits;
                    $temp['3'] = $sales->num_sales;
                    $temp['5'] = '<a href="/admin/affiliates/referrals-indiv/'.$result->id.'">View Full</a>';

                    if($sales->num_sales && $refs->num_hits)
                    {
                        $temp['4'] = number_format(($sales->num_sales/$refs->num_hits)*100, 2,'.','');
                    }
                    else
                    {
                        $temp['4'] = 0;
                    }

                    $data[] = $temp;

                    $total++;

                }

                $results = array();
                $results['data'] = $data;
                $results['recordsTotal'] = $total;
                $results['recordsFiltered'] = $total;
                $response = Response::make($results);
                $response->header('Content-Type', 'application/json');

                return $response;
            }

            return Response::json('invalid date',404);
        }

        return Response::json("Error, requires date", 404);
    }

    public function referralIndivIndex($id)
    {
        $dt = new DateTime();
        $dt = $dt->format('Y-m-d');

        View::share('dt', $dt);

        $af = AffiliateProperty::where('id', $id)->first();
        if($af)
        {
            View::share('affiliate_name', $af->company_name);
            View::share('affiliate_id', $af->id);
            View::share('menu', 'affiliates/referrals');
            return View::make('ticketAdmin::affiliates.affiliate-referral-indiv');
        }


    }

    public function getReferralIndivDataByDate($id)
    {
        if(Input::has('date')) {
            $date = Input::get('date');

            $da = DateTime::createFromFormat('Y-m-d', $date);
            $dt = DateTime::createFromFormat('Y-m-d', $date);
            $dt->modify('+1 month');

            $da = $da->format('Y-m');
            $dt = $dt->format('Y-m');

            if ($da && $dt)
            {
                $start = 0;
                $limit = 10;
                if(Input::has('start'))
                {
                    $start = Input::get('start');
                }
                if(Input::has('length'))
                {
                    $limit = Input::get('length');
                }

                $ttl = DB::table('referral_hits')
                        ->where('affiliate_id', $id)
                        ->where('created_at', '>=', $da)
                        ->where('created_at', '<', $dt)
                        ->select(DB::raw('COUNT(*) as ttl'))
                        ->first();

                $res = DB::table('referral_hits')
                            ->where('affiliate_id', $id)
                            ->where('created_at', '>=', $da)
                            ->where('created_at', '<', $dt)
                            ->orderBy('created_at', 'DESC')
                            ->take($limit)
                            ->skip($start)
                            ->get();

                $data = [];
                $total = 0;
                foreach($res as $r)
                {
                    $temp = [];

                    $temp['0'] = $r->created_at;
                    $temp['1'] = $r->url;

                    $data[] = $temp;
                    $total++;
                }

                $results = array();
                $results['data'] = $data;
                $results['recordsTotal'] = $ttl->ttl;
                $results['recordsFiltered'] = $ttl->ttl;
                $response = Response::make($results);
                $response->header('Content-Type', 'application/json');

                return $response;
            }

            return Response::json("1", 404);
        }

        return Response::json('2', 404);
    }

    public function salesIndex(){
        $dt = new DateTime();
        $dt = $dt->format('Y-m-d');

        View::share('dt', $dt);

        View::share('menu', 'affiliates/sales');

        return View::make('ticketAdmin::affiliates.affiliate-sales');
    }

    public function getSalesDataByDate()
    {
        if(Input::has('date')) {
            $date = Input::get('date');

            $da = DateTime::createFromFormat('Y-m-d', $date);
            $dt = DateTime::createFromFormat('Y-m-d', $date);
            $dt->modify('+1 month');

            $da = $da->format('Y-m');
            $dt = $dt->format('Y-m');

            if ($da && $dt)
            {
                $start = 0;
                $limit = 10;
                if(Input::has('start'))
                {
                    $start = Input::get('start');
                }
                if(Input::has('length'))
                {
                    $limit = Input::get('length');
                }

                $res = DB::table('affiliate_properties')
                        ->get();


                $data = [];
                $total = 0;
                foreach($res as $r)
                {
                    $temp = [];

                    $sales = DB::table('affiliate_sales')
                                ->leftJoin('events_ticket_buy', 'affiliate_sales.order_id', '=', 'events_ticket_buy.order_id')
                                ->where('affiliate_id', $r->id)
                                ->where('created_at', '>=', $da)
                                ->where('created_at', '<', $dt)
                                ->select(DB::raw('COUNT(*) as num_sales, SUM(amount) as sales_amt, AVG(commission_perc) as avg_com, SUM(amount * (commission_perc/100)) as com'))
                                ->first();

                    $temp['0'] = $r->id;
                    $temp['1'] = $r->company_name;
                    $temp['2'] = $sales->num_sales;
                    $temp['3'] = $sales->sales_amt?number_format($sales->sales_amt, 2, '.',''):0.00;
                    $temp['4'] = $sales->avg_com?number_format($sales->avg_com,2,'.',''):0;
                    $temp['5'] = $sales->com?number_format($sales->com,2,'.',''):0.00;
                    $temp['6'] = '<a href="/admin/affiliates/sales-indiv/'.$r->id.'">View Full</a>';

                    $data[] = $temp;
                    $total++;
                }

                $results = array();
                $results['data'] = $data;
                $results['recordsTotal'] = $total;
                $results['recordsFiltered'] = $total;
                $response = Response::make($results);
                $response->header('Content-Type', 'application/json');

                return $response;
            }

            return Response::json("1", 404);
        }

        return Response::json('2', 404);
    }

    public function getSalesTotalsByDate()
    {
        if(Input::has('date')) {
            $date = Input::get('date');

            $da = DateTime::createFromFormat('Y-m-d', $date);
            $dt = DateTime::createFromFormat('Y-m-d', $date);
            $dt->modify('+1 month');

            $da = $da->format('Y-m');
            $dt = $dt->format('Y-m');

            if ($da && $dt)
            {
                $start = 0;
                $limit = 10;
                if(Input::has('start'))
                {
                    $start = Input::get('start');
                }
                if(Input::has('length'))
                {
                    $limit = Input::get('length');
                }

                $data = [];
                $total = 1;

                $temp = [];

                $sales = DB::table('affiliate_sales')
                    ->leftJoin('events_ticket_buy', 'affiliate_sales.order_id', '=', 'events_ticket_buy.order_id')
                    ->where('created_at', '>=', $da)
                    ->where('created_at', '<', $dt)
                    ->select(DB::raw('COUNT(*) as num_sales, SUM(amount) as sales_amt, AVG(commission_perc) as avg_com, SUM(amount * (commission_perc/100)) as com'))
                    ->first();


                $temp['0'] = $sales->num_sales;
                $temp['1'] = $sales->sales_amt?number_format($sales->sales_amt, 2, '.',''):0.00;
                $temp['2'] = $sales->avg_com?number_format($sales->avg_com,2,'.',''):0;
                $temp['3'] = $sales->com?number_format($sales->com,2,'.',''):0.00;

                $data[] = $temp;

                $results = array();
                $results['data'] = $data;
                $results['recordsTotal'] = $total;
                $results['recordsFiltered'] = $total;
                $response = Response::make($results);
                $response->header('Content-Type', 'application/json');

                return $response;
            }

            return Response::json("1", 404);
        }

        return Response::json('2', 404);
    }

    public function salesIndivIndex($id){
        $dt = new DateTime();
        $dt = $dt->format('Y-m-d');

        View::share('dt', $dt);

        $af = AffiliateProperty::where('id', $id)->first();
        if($af)
        {
            View::share('affiliate_name', $af->company_name);
            View::share('affiliate_id', $af->id);
            View::share('menu', 'affiliates/sales');
            return View::make('ticketAdmin::affiliates.affiliate-sales-indiv');
        }
    }

    public function getSalesIndivDataByDate($id)
    {
        if(Input::has('date')) {
            $date = Input::get('date');

            $da = DateTime::createFromFormat('Y-m-d', $date);
            $dt = DateTime::createFromFormat('Y-m-d', $date);
            $dt->modify('+1 month');

            $da = $da->format('Y-m');
            $dt = $dt->format('Y-m');

            if ($da && $dt)
            {
                $start = 0;
                $limit = 10;
                if(Input::has('start'))
                {
                    $start = Input::get('start');
                }
                if(Input::has('length'))
                {
                    $limit = Input::get('length');
                }

                $data = [];

                $ttl =  DB::table('affiliate_sales')
                    ->leftJoin('events_ticket_buy', 'affiliate_sales.order_id', '=', 'events_ticket_buy.order_id')
                    ->where('affiliate_id', $id)
                    ->where('created_at', '>=', $da)
                    ->where('created_at', '<', $dt)
                    ->select(DB::raw('COUNT(*) as ttl'))
                    ->first();


                $total = $ttl->ttl;

                $sales = DB::table('affiliate_sales')
                    ->leftJoin('events_ticket_buy', 'affiliate_sales.order_id', '=', 'events_ticket_buy.order_id')
                    ->leftJoin('events_related_tickets', 'events_ticket_buy.product_id', '=','events_related_tickets.product_id')
                    ->leftJoin('events', 'events_related_tickets.event_id','=','events.id')
                    ->where('affiliate_id', $id)
                    ->where('events_ticket_buy.created_at', '>=', $da)
                    ->where('events_ticket_buy.created_at', '<', $dt)
                    ->select('affiliate_sales.order_id', 'events_ticket_buy.created_at', 'events_ticket_buy.amount', 'events_ticket_buy.qty', 'affiliate_sales.commission_perc', 'events.title', 'events.datetime')
                    ->skip($start)
                    ->take($limit)
                    ->orderBy('events_ticket_buy.order_id', 'DESC')
                    ->get();

                foreach($sales as $sale)
                {
                    $temp['0'] = $sale->order_id;
                    $temp['1'] = $sale->created_at;
                    $temp['2'] = $sale->title;
                    $temp['3'] = $sale->datetime;
                    $temp['4'] = $sale->qty;
                    $temp['5'] = number_format($sale->amount,2,'.','');
                    $temp['6'] = $sale->commission_perc;
                    $temp['7'] = number_format(($sale->amount*($sale->commission_perc/100)),2,'.','');

                    $data[] = $temp;
                }




                $results = array();
                $results['data'] = $data;
                $results['recordsTotal'] = $total;
                $results['recordsFiltered'] = $total;
                $response = Response::make($results);
                $response->header('Content-Type', 'application/json');

                return $response;
            }

            return Response::json("1", 404);
        }

        return Response::json('2', 404);
    }

    public function getSalesTotalsIndivDataByDate($id)
    {
        if(Input::has('date')) {
            $date = Input::get('date');

            $da = DateTime::createFromFormat('Y-m-d', $date);
            $dt = DateTime::createFromFormat('Y-m-d', $date);
            $dt->modify('+1 month');

            $da = $da->format('Y-m');
            $dt = $dt->format('Y-m');

            if ($da && $dt)
            {
                $start = 0;
                $limit = 10;
                if(Input::has('start'))
                {
                    $start = Input::get('start');
                }
                if(Input::has('length'))
                {
                    $limit = Input::get('length');
                }

                $data = [];
                $total = 1;

                $temp = [];

                $sales = DB::table('affiliate_sales')
                    ->leftJoin('events_ticket_buy', 'affiliate_sales.order_id', '=', 'events_ticket_buy.order_id')
                    ->where('affiliate_id', $id)
                    ->where('created_at', '>=', $da)
                    ->where('created_at', '<', $dt)
                    ->select(DB::raw('COUNT(*) as num_sales, SUM(amount) as sales_amt, AVG(commission_perc) as avg_com, SUM(amount * (commission_perc/100)) as com'))
                    ->first();


                $temp['0'] = $sales->num_sales;
                $temp['1'] = $sales->sales_amt?number_format($sales->sales_amt, 2, '.',''):0.00;
                $temp['2'] = $sales->avg_com?number_format($sales->avg_com,2,'.',''):0;
                $temp['3'] = $sales->com?number_format($sales->com,2,'.',''):0.00;

                $data[] = $temp;

                $results = array();
                $results['data'] = $data;
                $results['recordsTotal'] = $total;
                $results['recordsFiltered'] = $total;
                $response = Response::make($results);
                $response->header('Content-Type', 'application/json');

                return $response;
            }

            return Response::json("1", 404);
        }

        return Response::json('2', 404);
    }

    public function updateAffiliateShowFees($id)
    {
        $ap = AffiliateProperty::where('id', $id)->first();
        if($ap)
        {
            if(Input::has('show_fees'))
            {
                $show_fees = Input::get('show_fees');

                $ap->show_fees = $show_fees;
                $ap->save();
                return Response::json('success',200);
            }
            return Response::json('Requires show fees param',404);
        }
        return Response::json('Could not locate affiliate',404);
    }

}
