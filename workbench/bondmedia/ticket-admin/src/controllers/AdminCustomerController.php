<?php
use Bond\Traits\GridPagination;

class AdminCustomerController extends \BaseController {
    use GridPagination;
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        View::share('menu', 'customer');
        return View::make('ticketAdmin::customer.list');
    }


    public function customerList () {
        $columns  = array(
            0 => 'user_id',
            1 => 'firstname',
            2 => 'lastname',
            3 => 'telephone',
            4 => 'email',
            5 => 'seller_type',
            6 => 'status',
            7 => 'updated_at',
            8 => 'updated_by'
        );
        $response = Response::make($this->getGridData('web_user', $columns));
        $response->header('Content-Type', 'application/json');
        return $response;
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {

    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {

    }


    /**
     * Display the specified resource.
     *rm
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        View::share('menu', 'customer/edit');
        $node = WebUser::find($id);

        if(!$node) {
            App::abort(404);
        }
        View::share('node', $node);

        return View::make('ticketAdmin::customer.edit');
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        $node = WebUser::find($id);
        if(!$node) {
            App::abort(404);
        }
        $input = Input::all();
        $node->status   = $input['status'];
        $node->seller_type     = $input['type'];
        $node->save();

        Notification::success('Customer information has been updated!');
        return Redirect::route('admin.customer.info.edit', array('id' => $id));
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    public function remove() {

    }

    public function getFavouriteTeams()
    {
        $outstring = '';
        $users = WebUser::all();
        foreach($users as $user)
        {
            $teams = DB::table('events_ticket_buy')
                ->leftJoin('events_related_tickets', 'events_ticket_buy.product_id', '=' ,'events_related_tickets.product_id')
                ->leftJoin('events', 'events_related_tickets.event_id', '=', 'events.id')
                ->leftJoin('football_ticket as home', 'events.home_team_id', '=', 'home.id')
                ->leftJoin('football_ticket as away', 'events.away_team_id', '=', 'away.id')
                ->where('events_ticket_buy.buyer_id', $user->user_id)
                ->select('home.title as home', 'away.title as away')
                ->get();

            $teamNames = array();
            $homeTeams = array();
            $awayTeams = array();

            foreach($teams as $team)
            {
                if(!is_null($team->home))
                {
                    $teamNames[] = $team->home;
                }
                else
                {
                    $teamNames[] = '';
                }

                if(!is_null($team->away))
                {
                    $teamNames[] = $team->away;
                }
                else
                {
                    $teamNames[] = '';
                }

                if(!is_null($team->home))
                {
                    $homeTeams[] = $team->home;
                }
                else
                {
                    $homeTeams[] = '';
                }

                if(!is_null($team->away))
                {
                    $awayTeams[] = $team->away;
                }
                else
                {
                    $awayTeams[] = '';
                }
            }

            $counted = array_count_values($teamNames);
            $homeCounted = array_count_values($homeTeams);
            $awayCounted = array_count_values($awayTeams);



            if(sizeof($homeCounted)>0)
            {
                $highestHome = array_keys($homeCounted, max($homeCounted));
                $highestHomeNum = max($homeCounted);
            }
            else
            {
                $highestHome[0] = '';
                $highestHomeNum =  0;
            }

            if($highestHomeNum != 0)
            {
                $outstring.=$user->firstname.",".$user->lastname.",".$user->email.','.$highestHome[0].','.$highestHomeNum.PHP_EOL;
            }
        }

        return $outstring;

    }
}
