<?php

class AdminTicketTypeController extends \BaseController {

    public function index()
    {
        View::share('menu', 'ticket-types');


        $ttypes = TicketType::where('status', 1)->orderBy('sort_order', 'ASC')->get();
        View::share('ttypes', $ttypes);
        return View::make('ticketAdmin::tickets.ticket-types');
    }

    public function delete($id)
    {
        $res = TicketType::where('id', $id)->first();

        if(!empty($res))
        {
            $res->status = 0;
            $res->save();

            return Response::json($id, 200);
        }
        return Response::json("Error", 404);
    }

    public function addNew(){
        $input = Input::all();

        if(array_key_exists('title', $input) && $input['title'] != '')
        {
            $mx = DB::table('events_ticket_type')
                    ->select(DB::raw('MAX(sort_order) as ms'))
                    ->first();

            if(!empty($mx))
            {
                $mx = $mx->ms + 1;
                $title = $input['title'];

                $tt = new TicketType();

                $tt->title = $title;
                $tt->sort_order = $mx;
                $tt->save();
                return Response::json($tt->id, 200);
            }

            return Response::json("Error", 404);
        }

        return Response::json("Required title",404);
    }

    public function saveOrder()
    {
        $input = Input::all();
        if(array_key_exists('oArray', $input) && !empty($input['oArray']))
        {
            $oArray = $input['oArray'];

            foreach($oArray as $index => $elem)
            {
                $ttype = TicketType::where('id', $elem)->first();
                $ttype->sort_order = $index;
                $ttype->save();
            }

            return Response::json("success", 200);
        }

        return Response::json("Requires oArray",404);
    }
}