<?php
use Bond\Traits\GridPagination;
use Illuminate\Database\Schema\Blueprint;

class AdminSoldTicketsController extends \BaseController {
    use GridPagination;

    public function __construct() {
        if(!Schema::hasTable('sales')) {
            DB::statement('CREATE OR REPLACE VIEW `sales` AS
                       SELECT
                       `etb`.`id` AS `id`,
                       `etb`.`order_id` AS `order_id`,
                       `etb`.`buyer_id` AS `buyer_id`,
                       `etb`.`product_id` AS `product_id`,
                       `etb`.`qty` AS `qty`,
                       `etb`.`amount` AS `amount`,
                       `etb`.`commission` AS `commission`,
                       `etb`.`commission_amount` AS `commission_amount`,
                       `etb`.`fees_amount` AS `fees_amount`,
                       `etb`.`delivery_status` AS `delivery_status`,
                       `etb`.`seller_payment_status` AS `payment_status`,
                       `etb`.`updated_at` AS `updated_at`,
                       `etb`.`created_at` AS `created_at`,
                       `etb`.`listed_amount` AS `listed_amount`,
                       `bwu`.`firstname` AS `buyer_firstname`,
                       `bwu`.`lastname` AS `buyer_lastname`,
                       `bwu`.`email` AS `buyer_email`,
                       `ert`.`user_id` AS `seller_id`,
                       `ert`.`event_id` AS `event_id`,
                       `swu`.`firstname` AS `seller_firstname`,
                       `swu`.`lastname` AS `seller_lastname`,
                       `swu`.`email` AS `seller_email`,
                        e.`title` As title,
                        e.`datetime` AS event_date,
                        `st`.`tracking_code` As tcode
                        FROM `events_ticket_buy` `etb`

                        left join `web_user` `bwu` on `bwu`.`user_id` = `etb`.`buyer_id`
                        left join `events_related_tickets` `ert` on `ert`.`product_id` = `etb`.`product_id`
                        left join `web_user` `swu` on `swu`.`user_id` = `ert`.`user_id`
                        left join `shipping_tracking` `st` on `etb`.`order_id` = `st`.`order_id`
                        left join events as e on e.id = ert.`event_id`');
        }
    }

    public function indexAtX()
    {
        $this->__construct();
        View::share('menu', 'sold-tickets');
        return View::make('ticketAdmin::sold-tickets.list');
    }

    public function index()
    {
        $this->__construct();
        $dt = new DateTime();
        $df = new DateTime();

        $dt = $dt->modify('+1 week');

        $concat = $df->format('d/m/Y');
        $concat .= ' - ' . $dt->format('d/m/Y');

        View::share('dateRange', $concat);

        View::share('menu', 'sold-tickets');
        return View::make('ticketAdmin::sold-tickets.list');
    }


    public function ticketList () {
        $status = array(0=>'pending',1=>'in-transit',2=>'delivered',3=>'cancelled');

        $sFilters = array();
        if(Input::has('searchFilters'))
        {
            $sFilters = Input::get('searchFilters');
            $sFilters = json_decode($sFilters);
        }

        $dr = Input::get('daterange');
        $dsplit = explode('-', $dr);
        $df = DateTime::createFromFormat('d/m/Y', trim($dsplit[0]));
        $dt = DateTime::createFromFormat('d/m/Y', trim($dsplit[1]));


        $res = DB::table('sales');

        if(Input::has('filterByPaid'))
        {
            $fbp = Input::get('filterByPaid');
            if($fbp != 'all')
            {
                Log::info($fbp);
                $res = $res->where('payment_status', '=', Input::get('filterByPaid'));
            }
        }

        if(Input::has('filterByDelivery'))
        {
            $fbd = Input::get('filterByDelivery');
            if($fbd != 'all')
            {
                $res = $res->where('delivery_status', '=', Input::get('filterByDelivery'));
            }
        }

        $swdr = true;

        foreach($sFilters as $filter)
        {
            $split = explode(':', $filter);
            $split[0] = trim($split[0]);
            $split[1] = trim($split[1]);

            if($split[1] != '') {
                $filt = "";

                switch ($split[0]) {
                    case "evn":
                        $filt = "title";
                        break;
                    case "bfn":
                        $filt = "buyer_firstname";
                        break;
                    case "bln":
                        $filt = "buyer_lastname";
                        break;
                    case "bemail":
                        $filt = "buyer_email";
                        break;
                    case "sOn":
                        $filt = "order_id";
                        break;
                    case "sfn":
                        $filt = "seller_firstname";
                        break;
                    case "sln":
                        $filt = "seller_lastname";
                        break;
                    case "semail":
                        $filt = "seller_email";
                        break;
                    case 'swdr':
                        $swdr = false;
                    default :
                        $filt = "";
                        break;
                }

                if ($filt != "") {
                    $res = $res->where($filt, 'LIKE', '%' . $split[1] . '%');
                }
            }
        }

        if($swdr)
        {
            $res = $res->where('event_date', '<=', $dt->format('Y-m-d'))
                        ->where('event_date', '>', $df->format('Y-m-d'));
        }

        $count = $res->count();

        $res = $res->orderBy('event_date', 'asc');

        $res = $res->skip(Input::get('start'))->take(Input::get('length'));
        $res = $res->get();

        //$results = $this->getGridData('sales', $columns, $filtersOut);

        $data = $res;
        $output = [];
        $pStatus = array();
        $pStatus[] = "paid";
        $pStatus[] = "pending";

        foreach($data as $d) {
            $tempChangePaid = '<select class="paid-st-select" onchange="postPaidStatusChange('. $d->id .', this)">';
            foreach($pStatus as $st)
            {
                if($d->payment_status == $st)
                {
                    $tempChangePaid.='<option value="'.$st.'" selected>'.$st.'</option>';
                }

                else
                {
                    $tempChangePaid.='   <option value="'.$st.'">'.$st.'</option>';
                }
            }
            $tempChangePaid.='</select>';  

            $tempChange = '';
            $tempChange.='<select class="delivery-st-select" onchange="postDelStatusChange('. $d->order_id .', this)">';
            
            foreach($status as $st)
            {
                if($d->delivery_status == $st)
                {
                    $tempChange.='<option value="'.$st.'" selected>'.$st.'</option>';
                }

                else
                {
                    $tempChange.='   <option value="'.$st.'">'.$st.'</option>';
                }
            }
            $tempChange.='<option value=""></option>';
            $tempChange.='</select>';  
            $trackingLink = $d->tcode.'<br><a href="javascript:;" onclick="postTrackingChange('. $d->order_id .','.$d->event_id.', this)" class="trackingChange">change</a>';
            $orderNumLink = '<a href="javascript:;" onclick="postShowFullInfo('. $d->order_id .')" class="trackingChange">'. $d->order_id .'</a>';
            $temp = [];
            $temp[0] = $d->id;
            $temp[1] = $d->title;
            $temp[2] = date("d/m/y H:i:s",strtotime($d->event_date));
            $temp[3] = $d->created_at;
            $temp[4] = $orderNumLink;
            $temp[5] = $d->seller_firstname;
            $temp[6] = $d->seller_lastname;
            $temp[7] = $tempChangePaid;
            $temp[8] = $tempChange;
            $temp[9] = $trackingLink;
            $temp[10] = '<a href="'.$d->id.'" class="edit-node">Edit</a>';
            $output[] = $temp;
        }

        $results['data'] = $output;
        $results['recordsTotal'] = $count;
        $results['recordsFiltered'] = $count;
        $response = Response::make($results);
        $response->header('Content-Type', 'application/json');
        return $response;
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {

    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {

    }


    /**
     * Display the specified resource.
     *rm
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        View::share('menu', 'sold-tickets/edit');
        FootballTicketBuy::createSaleView();
        $node = DB::table('sales')->where('id', '=',$id)->first();
        $deliveryStatus = ['pending','in-transit','delivered','cancelled'];
        View::share('payment_status', ['pending','paid','cancelled']);
        if(!$node) {
            App::abort(404);
        }

        $ticketBuy = FootballTicketBuy::where('order_id', '=', $node->order_id)->first();
        $ticketPublishBySeller = RelatedTicket::where('event_id', '=',$node->event_id)->where('product_id', '=',$node->product_id)->first();
        $commission = isset($ticketPublishBySeller->selling_commission_percentage) ? $ticketPublishBySeller->selling_commission_percentage: 0;
        $info = json_decode($ticketPublishBySeller->ticket, true);
        $payment_method = '';

        if(isset($info['paymentMethod']['payment_option']) && $info['paymentMethod']['payment_option'] == 'bank') {
            $payment_method = 'Bank <br/>Account Holder:'.$info['paymentMethod']['account_holder']."<br />".'IBAN:'.$info['paymentMethod']['iban']."<br />BIC/SWIFT:".$info['paymentMethod']['bic_swift'];
            View::share('payment_method', $payment_method);
        } else {
            $payment_method = "PayPal: ".$info['paymentMethod']['paypal_email'];

            View::share('payment_method', $payment_method);
        }


        $sellerAmount = $node->qty * $ticketBuy->listed_amount;
        $totalSellerAmmount = $sellerAmount * 0.85;
        $ourCommission = $sellerAmount * 0.15;

        $royalMailTracking = RoyalMailTracking::where('order_id', '=', $node->order_id)->first();
        View::share('sellerAmount', $sellerAmount);
        View::share('totalSellerAmount', $totalSellerAmmount);
        View::share('ourCommission', $ourCommission);
        View::share('commission', $commission );
        View::share('node', $node);
        View::share('tracking',$royalMailTracking);

        return View::make('ticketAdmin::sold-tickets.edit');
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        $event_id = (int) Input::get('event_id');
        $product_id = (int) Input::get('product_id');
        $order_id = Input::get('order_id');
        $seller_id = Input::get('seller_id');


        $payment_status = Input::get('payment_status');
        $commission_amount = Input::get('commission_amount');
        $amount_paid = Input::get('amount_paid');

        $seller_information = WebUser::where('user_id', '=', $seller_id)->first();



        try {
            if(!$seller_information) {
                throw new Exception('Seller not found!');
            }

            if($payment_status == 'pending') {
                throw new Exception('pending status can not update.');
            }

            if($payment_status == 'paid') {
                $formData = Input::all();
                $ticket = FootballTicketBuy::where('order_id', '=', $order_id)->first();

                if(empty($ticket)) {
                    throw new Exception('Ticket not found!');
                }

                $ticket->seller_payment_status = $payment_status;
                $ticket->payment_status = $payment_status;
                $ticket->seller_amount_paid    = $amount_paid;
                $ticket->save();

                $formData['total_amount'] = $amount_paid + $commission_amount;
                $formData['commission']   = $commission_amount;
                $formData['total_amount_after_commission'] = $amount_paid;

//                Mail::send('ticketAdmin::emails.payment-confirmation-to-seller', $formData, function ($message) use ($seller_information) {
//                    $message->from(Input::get('sender_email'), Input::get('sender_name_surname'));
//                    $message->to($seller_information->email, $seller_information->firstname. " ".$seller_information->lastname)->subject("Football ticket payment");
//                });
                $this->sendEmailConfirmationToSeller($order_id);
            }
            Notification::success('Ticket has been updated!');
            return Redirect::route('admin.sold-tickets.info.edit', array('id' => $id));
        } catch(Exception $e) {
            Notification::error($e->getMessage());
            return Redirect::route('admin.sold-tickets.info.edit', array('id' => $id));
        }

    }

    public function updatePaidStatusByOrderId($id)
    {
        $payment_status = Input::get('payment_status');

        $node = DB::table('sales')->where('id', '=',$id)->first();
       
        $ticketBuy = FootballTicketBuy::where('order_id', '=', $node->order_id)->first();
        $ticketPublishBySeller = RelatedTicket::where('event_id', '=',$node->event_id)->where('product_id', '=',$node->product_id)->first();
        
        $commission = isset($ticketPublishBySeller->selling_commission_percentage) ? $ticketPublishBySeller->selling_commission_percentage: 0;
        $sellerAmount = $node->qty * $ticketBuy->listed_amount;
        $commission_amount = $sellerAmount * $commission / 100;
        $amountWithoutCommission = $sellerAmount - $commission_amount;
        $info = json_decode($ticketPublishBySeller->ticket, true);
        
        $royalMailTracking = RoyalMailTracking::where('order_id', '=', $node->order_id)->first();
        
        $event_id = $node->event_id;
        $product_id = $node->product_id;
        $seller_id = $node->seller_id;

        $seller_information = WebUser::where('user_id', '=', $node->seller_id)->first();

        try {
            if(!$seller_information) {
                throw new Exception('Seller not found!');
            }

            if($payment_status == 'pending') {
               return "Cannot change a payment status to pending.";
            }
            if($payment_status == 'paid') {
                $ticket = FootballTicketBuy::where('order_id', '=', $node->order_id)->first();

                if(empty($ticket)) {
                    return "Could not find ticket for order id: ".$node->order_id;
                }

                $ticket->seller_payment_status = $payment_status;
                $ticket->payment_status = $payment_status;
                $ticket->seller_amount_paid    = $amountWithoutCommission;
                $ticket->save();

                $this->sendEmailConfirmationToSeller($node->order_id);
                return "Updated payment status";
            }
            
        } catch(Exception $e) {
            return "There was an error: ".$e;
        }        
    }




    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    public function remove() {

    }

    public function deliveryStatusUpdate() {
        $order_id = Input::get('order_id');
        $status = Input::get('status');

        $ticket = FootballTicketBuy::where('order_id', '=', $order_id)->first();

        if($ticket) {
            $ticket->delivery_status = $status;
            $ticket->save();
        }
    }

    public function secureDeliveryStatusUpdate(){
        $order_id = Input::get('order_id');
        $status = Input::get('status');

        $ticket = FootballTicketBuy::where('order_id', '=', $order_id)->first();

        if($ticket) {
            $ticket->delivery_status = $status;
            $ticket->save();
            return Response::json($status, 200);
        }
        else{
            return Response::json("Failed to find order id: ".$order_id, 404);
        }
    }

    private function sendEmailConfirmationToSeller($orderId= '') {
        try {
            FootballTicketBuy::createSaleView();

            $orderDetails = DB::table('sales')->where('order_id', '=', $orderId)->first();
            $etb = DB::table('events_ticket_buy')->where('order_id', '=', $orderID)->first();

            $event = FootBallEvent::find($orderDetails->event_id);
            $eventTicket = RelatedTicket::where('product_id', '=', $orderDetails->product_id)->first();
            $ticket = RelatedTicket::getTicketByTicketId($orderDetails->product_id);

            $data = array();
            $data['game'] = $event->title. " - ".date('l, d F Y, h:ia', strtotime($event->datetime)).", ".$event->event_location;
            $data['event_id'] = $event->id;
            $data['order_id'] = $orderId;
            $data['total_amount'] = $orderDetails->listed_amount * $orderDetails->qty;
            $data['commission'] = ($orderDetails->listed_amount * $orderDetails->qty) * 0.15;
            $data['total_amount_after_commission'] = ($orderDetails->listed_amount * $orderDetails->qty) * 0.85;
            $data['seller_name'] = $orderDetails->seller_firstname." ".$orderDetails->seller_lastname;
            $data['qty'] = $orderDetails->qty;

            $ticketInfo = json_decode($eventTicket->ticket, true);

            $data['formOfTicket'] = @$ticket->formOfTicket['title'];
            $data['location'] = @$ticket->ticketType['title'];
            $temp = array();
            if(isset($ticketInfo['ticketInformation']['loc_block']) && !empty($ticketInfo['ticketInformation']['loc_block'])) {
                $temp[] =     'Block: '.$ticketInfo['ticketInformation']['loc_block'];
            }

            if(isset($ticketInfo['ticketInformation']['loc_row']) && !empty($ticketInfo['ticketInformation']['loc_row'])) {
                $temp[] =     'Row: '.$ticketInfo['ticketInformation']['loc_row'];
            }

            $data['location'] = $data['location'].', '.implode(', ', $temp);

            $temp = array();

            $defaultRestriction = [];
            foreach($event->getSelectedRestrictions()  as $val) {

                $defaultRestriction[] = array(
                    'id'    => $val->id,
                    'title' => $val->title
                );
            }

            foreach($defaultRestriction as $r):
                if(in_array($r['id'],$ticket->info['restrictions'])):
                    $temp[] = $r['title'];
                endif;
            endforeach;

            if($ticket->buyerNot != '' ):
                $temp[] = '<br /> Note: '.$ticket->buyerNot;
            endif;

            $data['restrictions'] = implode(', ', $temp);

            Mail::send('ticketAdmin::emails.payment-confirmation-to-seller', $data, function ($message) use($orderDetails, $orderId) {
                $from = trans('homepage.contact-email');
                $message->from($from['address'], $from['name']);
                $email = Options::getOption('contact_email') == ''? 'hello@bondmedia.co.uk': Options::getOption('contact_email');

                $message->to($orderDetails->seller_email, $orderDetails->seller_firstname." ".$orderDetails->seller_lastname )
                    ->bcc($email, 'Football Ticket Pad')
                    ->subject("Football ticket payment # {$orderId}");
            });


        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function saveTrackingCode() {
        try{
            $input = Input::all();
            $tracking = RoyalMailTracking::where('event_id', '=', $input['event_id'])
                ->where('product_id', '=', $input['product_id'])
                ->where('order_id', '=', $input['order_id'])
                ->first();
            if(!isset($tracking->tracking_code)){
                $tracking = new RoyalMailTracking();
                $shipmentIncrementId = TicketSoap::process('order_shipment.create', array($input['order_id']));
                $input['tracking_id_number'] = TicketSoap::process('sales_order_shipment.addTrack', array('shipmentIncrementId' => $shipmentIncrementId, 'carrier' => 'custom', 'title' => 'Royal Mail tracking ID', 'trackNumber' => $input['tracking_code']));
                TicketSoap::process('sales_order_shipment.addComment', array('shipmentIncrementId' => $shipmentIncrementId, 'comment' => 'ticket has been dispatched', 'email' => false ));
                $ticket_buy = FootballTicketBuy::where('order_id', '=', $input['order_id'])->first();
                if($ticket_buy) {
                    $ticket_buy->delivery_status = 'in-transit';
                    $ticket_buy->save();
                }

                $soldTicket = FootballTicketBuy::where('order_id', '=', $input['order_id'])->first();
                \Log::info(var_export($soldTicket,true));
                $tracking->event_id = @$input['event_id'];
                $tracking->product_id = @$input['product_id'];
                $tracking->order_id = @$input['order_id'];
                $tracking->tracking_id_number = @$input['tracking_id_number'];
                $tracking->tracking_code = @$input['tracking_code'];
                $tracking->save();

                if($soldTicket->buyer_id) {
                    $buyerDetails = WebUser::where('user_id', '=', $soldTicket->buyer_id)->first();
                    $event = FootBallEvent::where('id', '=', $input['event_id'])->first();
                    $formData = array('customer_name'=> $buyerDetails->firstname." ".$buyerDetails->lastname, 'tracking_code'=> $input['tracking_code'], 'event'=>$event, 'order'=>$soldTicket);

                    Mail::send('footballticket::email.tracking-email', $formData, function ($message) use($buyerDetails) {
                        $from = Config::get('mail.from');
                        $message->from($from['address'], $from['name']);
                        $email = Options::getOption('contact_email') == ''? 'info@footballticketpad.com': Options::getOption('contact_email');
                        $message->to($buyerDetails->email, $buyerDetails->firstname." ".$buyerDetails->lastname)
                            ->bcc($email, 'Football Ticket Pads')
                            ->subject("Football ticket dispatched.");

                        if(trim($email) != 'info@footballticketpad.com' ) {
                            $message->bcc('info@footballticketpad.com', 'Football Ticket Pads');
                        }
                    });

                    return Response::json('success', 200);  
                }
            }
            return Response::json("Already set.", 200);
        } catch (Exception $e) {
            Log::info($e->getMessage());
            return Response::json($e->getMessage(), 404);
        }
    }

    public function getFullOrderDetailsByOrderId($id)
    {
        if(!$id)
        {
            return Response::json('Must supply an order_id', 404);
        }

        $buyer_seller = FootballTicketBuy::getBuyerSellerIdByOrderId($id);
        if(!isset($buyer_seller[0]))
        {
            return Response::json('Could not find buyer and seller for order id: '.$id, 404);
        }

        $buyer_seller = $buyer_seller[0];
        $buyer = WebUser::where('user_id','=', $buyer_seller->buyer_id)->first();
        $buyer=$buyer['attributes'];
        $seller = WebUser::where('user_id','=', $buyer_seller->user_id)->first();
        $seller=$seller['attributes'];
        $orderDetails = FootballTicketBuy::where('order_id', $id)->first();
        $orderDetails = $orderDetails['attributes'];
        $data=array( 'orderId' => intval($id));
        try{
            $mageOrderInfo = TicketSoap::process('sales_order.info', $data);
        }
        catch(Exception $e)
        {
            Log::info($e);
            return Response::json('Count not find magento order information for id: '.$id);
        }

        $shipping = $mageOrderInfo['shipping_address'];

        $sale = DB::table('sales')
                ->where('order_id', $id)
                ->first();
        $etb = DB::table('events_ticket_buy')
                ->where('order_id', $id)
                ->first();

        $ticketPublishBySeller = RelatedTicket::where('event_id', '=',$sale->event_id)->where('product_id', '=',$sale->product_id)->first();
        $payment_method = '';
        $info = json_decode($ticketPublishBySeller->ticket, true);

        if(isset($info['paymentMethod']['payment_option']) && $info['paymentMethod']['payment_option'] == 'bank') {
            $payment_method = 'Bank <br/>Account Holder:'.$info['paymentMethod']['account_holder']."<br />".'IBAN:'.$info['paymentMethod']['iban']."<br />BIC/SWIFT:".$info['paymentMethod']['bic_swift'];
        } else {
            $payment_method = "PayPal: ".$info['paymentMethod']['paypal_email'];
        }
        
        $sellerAmount = $sale->amount;
        $ticket = RelatedTicket::getTicketByTicketId($sale->product_id);
        $royalMailTracking = RoyalMailTracking::where('order_id', '=', $id)->first();

        $forms = FormOfTicket::where('status', '=','1')->get();


        $return = array();
        $return['buyer'] = array();
        $return['seller'] = array();
        $return['event'] = array();

        $return['buyer']['name'] = $buyer['firstname'].' '.$buyer['lastname'];
        $return['buyer']['address'] = $shipping;
        $return['buyer']['tel'] = $buyer['telephone'];
        $return['buyer']['email'] = $buyer['email'];

        $return['seller']['name'] = $seller['firstname'].' '.$seller['lastname'];
        $return['seller']['tel'] = $seller['telephone'];
        $return['seller']['email'] = $seller['email'];
        $return['seller']['seller_returns'] = ($etb->listed_amount * $etb->qty)*0.85;
        $return['seller']['payment_method'] = $payment_method;

        $return['order']['sid'] = $sale->id;
        $return['order']['qty'] = $etb->qty;
        $return['order']['listed_ammount'] = $etb->listed_amount;
        $return['order']['event_name'] = $sale->title;
        $return['order']['event_date'] = $sale->event_date;
        $return['order']['ticket_type'] = $ticket->ticketType['title'];
        $return['order']['total_revenue'] = $sellerAmount;
        $return['order']['commission'] = ($etb->listed_amount * $etb->qty)*0.15;
        $return['order']['booking_fee'] = $sellerAmount - ($return['order']['commission'] + $return['seller']['seller_returns']);
        $return['order']['tracking'] = !is_null($royalMailTracking) ? $royalMailTracking['tracking_code'] : "None set"; 
        $return['order']['block'] = $ticket->info['loc_block'];
        $return['order']['row'] = $ticket->info['loc_row'];
        $return['order']['additional_info'] = $ticket->buyerNot;
        $return['order']['order_date'] = $orderDetails['created_at'];
        $return['order']['referral_url'] = $etb->referral_url;
        $return['order']['site_sold'] = $etb->site_sold;
        $restrictions = false;
        $return['order']['restrictions'] = $restrictions;
        $return['forms'] = $forms;

        $brokers = DB::table('web_user')
            ->where('seller_type', 'broker')
            ->get();


        $return['brokers']=$brokers;
        return View::make('ticketAdmin::sold-tickets.full')->with('return',$return);
    }

    public function getFixForm()
    {
        return View::make('ticketAdmin::sold-tickets.fixMess');
    }

    public function getAllInfoFromMess()
    {
        $input = Input::all();
        $eventId =intval( $input['eventId']);
        $email = $input['email'];
        $oID = $input['orderId'];

        $ert = DB::table('events_related_tickets as ert')
            ->where('ert.event_id', '=', $eventId)
            ->where('ws.email', '=', $email)
            ->leftJoin('web_user as ws', 'ert.user_id', '=', 'ws.user_id')
            ->get();

        $etb = DB::table('events_ticket_buy')
            ->where('order_id', $oID)
            ->first();
        
        $returnERT = array();
        $ts ="";
        foreach($ert as $ev)
        {
            $ticket = json_decode($ev->ticket);
            $tt = $ticket->ticketInformation->ticket_type;
            $ttype = DB::table('events_ticket_type')
                ->where('id', $tt)
                ->first();
            $ttype=$ttype->title;

            $temp = array();
            $temp['pid'] = $ev->product_id;
            $temp['price'] = $ev->price;
            $temp['event_id']=$ev->event_id;
            $temp['created_at']=$ev->created_at;
            $temp['ticket_type']=$ttype;
            $temp['block']=$ticket->ticketInformation->loc_block;
            $temp['row']=$ticket->ticketInformation->loc_row;

            $ts.= "<tr>";
            $ts.="<td>".$temp['pid']."</td>";
            $ts.="<td>".$temp['price']."</td>";
            $ts.="<td>".$temp['event_id']."</td>";
            $ts.="<td>".$temp['created_at']."</td>";
            $ts.="<td>".$temp['ticket_type']."</td>";
            $ts.="<td>".$temp['block']."</td>";
            $ts.="<td>".$temp['row']."</td>";
            $ts.="<td>".$ticket->ticketInformation->number_of_ticket."</td>";
            $ts.="<td>".$ev->available_qty."</td>";
            $ts.="</tr>";
            
        }

        $returnERT[] = $ts;

        $ts = "";
        $temp=array();
        $temp['id'] = $etb->id;
        $temp['event_id'] = $etb->event_id;
        $temp['product_id'] = $etb->product_id;
        $temp['qty'] = $etb->qty;
        $temp['order_id'] = $etb->order_id;
        $temp['listed_amount'] = $etb->listed_amount;
        $temp['created_at'] = $etb->created_at;

        $ts.= "<tr>";
            $ts.="<td>".$temp['id']."</td>";
            $ts.="<td>".$temp['event_id']."</td>";
            $ts.="<td>".$temp['product_id']."</td>";
            $ts.="<td>".$temp['qty']."</td>";
            $ts.="<td>".$temp['order_id']."</td>";
            $ts.="<td>".$temp['listed_amount']."</td>";
            $ts.="<td>".$temp['created_at']."</td>";
            $ts.="</tr>";
            $returnERT[]=$ts;
            

        return Response::json($returnERT);
        
    }

    public function updateThePID()
    {
        $id = intval(Input::get('id'));
        $pid = intval(Input::get('pid'));


        $rs = DB::table('events_ticket_buy')
            ->where('id', $id)
            ->update(array('product_id'=>$pid));

        return "Done";
    }

    public function replaceSeller()
    {
        $saleId = intval(Input::get('sid'));
        $newSellerId = intval(Input::get('nsid'));
                
        $results = DB::table('events_ticket_buy')
            ->where('id', $saleId)
            ->first();

        $results2 = DB::table('events_related_tickets')
           ->where('product_id', $results->product_id)
           ->first();

        $ticket = json_decode($results2->ticket);
        

        $this->setDataToPost(array(
            'customer_id'   => $newSellerId,
            'type'          => 'get'
        ));
        $this->submitPostToApi('customer/index/bank');
        $this->setApiResponseHeader();
        $bankInfo = json_decode($this->getApiResponse());

        $ticket->paymentMethod->payment_option = 'bank';
        $ticket->paymentMethod->account_holder = $bankInfo->data->account_holder;
        $ticket->paymentMethod->iban = $bankInfo->data->iban;
        $ticket->paymentMethod->bic_swift = $bankInfo->data->bic_swift;
        $ticket->paymentMethod->paypal_email = $bankInfo->data->paypal_email;

        $attributeSet = TicketSoap::process('product_attribute_set.list');
        $info = FootBallEvent::find($results2->event_id);
        $info = $info->toArray();
        $time = time();
        $product = array(
            'simple',
            $attributeSet[0]['set_id'],
            "-{$results2->event_id}-{$newSellerId}-{$ticket->ticketInformation->ticket_type}-{$time}-replaced",
            array(
                'categories' => array(2),
                'websites' => array(1),
                'name' => $info['title'],
                'description' => '-',
                'short_description' => '-',
                'weight' => '1',
                'status' => '1',
                'url_key' => $info['slug'],
                'url_path' => $info['slug'],
                'visibility' => '4',
                'price' => $ticket->ticketInformation->price,
                'tax_class_id' => 0,
                'meta_title' => $info['title'],
                'meta_keyword' => strip_tags($info['content']),
                'meta_description' => strip_tags($info['content']),
                'stock_data' => array(
                    'qty' => $ticket->ticketInformation->number_of_ticket,
                    'is_in_stock'=>1,
                    'min_sale_qty'=> 1
                ),

            )
        );

        $response = TicketSoap::process('catalog_product.create', $product);

        $ticket=json_encode($ticket);

        if ($response) {
            $relatedTicket = new RelatedTicket();
            $relatedTicket->event_id = $results2->event_id;
            $relatedTicket->product_id = $response;
            $relatedTicket->ticket = $ticket;
            $relatedTicket->price = $results2->price;
            $relatedTicket->available_qty = 0;
            $relatedTicket->user_id = $newSellerId;
            $relatedTicket->selling_commission_percentage = $results2->selling_commission_percentage;
            $relatedTicket->selling_commission_amount = $results2->selling_commission_amount;
            $relatedTicket->save();

            $etb = FootballTicketBuy::find($saleId);
            $etb->product_id = $response;
            $etb->save();
            return Response::json("Successfully changed seller", 200);
        }
        else{
            return Response::json("Couldnt create magento product", 404);
        }
        

        return Response::json("There was a problem changing seller", 404);
    }

    //CHANGE TICKET TYPE ERE! PHWOAR

    public function replaceFormOfTicket()
    {
        $saleId = intval(Input::get('sid'));
        $formOfID = intval(Input::get('fid'));

        $results = DB::table('events_ticket_buy')
            ->where('id', $saleId)
            ->first();

        $results2 = DB::table('events_related_tickets')
            ->where('product_id', $results->product_id)
            ->first();

        $ticket = json_decode($results2->ticket);


        $ticket->ticketInformation->form_of_ticket = $formOfID;
        $ticket->ticketInformation->number_of_ticket = $results->qty;

        $attributeSet = TicketSoap::process('product_attribute_set.list');
        $info = FootBallEvent::find($results2->event_id);
        $info = $info->toArray();
        $time = time();
        $product = array(
            'simple',
            $attributeSet[0]['set_id'],
            "-{$results2->event_id}-{$results2->user_id}-{$ticket->ticketInformation->ticket_type}-{$time}-replaced",
            array(
                'categories' => array(2),
                'websites' => array(1),
                'name' => $info['title'],
                'description' => '-',
                'short_description' => '-',
                'weight' => '1',
                'status' => '1',
                'url_key' => $info['slug'],
                'url_path' => $info['slug'],
                'visibility' => '4',
                'price' => $ticket->ticketInformation->price,
                'tax_class_id' => 0,
                'meta_title' => $info['title'],
                'meta_keyword' => strip_tags($info['content']),
                'meta_description' => strip_tags($info['content']),
                'stock_data' => array(
                    'qty' => $ticket->ticketInformation->number_of_ticket,
                    'is_in_stock'=>1,
                    'min_sale_qty'=> 1
                ),

            )
        );

        $response = TicketSoap::process('catalog_product.create', $product);

        $ticket=json_encode($ticket);

        if ($response) {
            $relatedTicket = new RelatedTicket();
            $relatedTicket->event_id = $results2->event_id;
            $relatedTicket->product_id = $response;
            $relatedTicket->ticket = $ticket;
            $relatedTicket->price = $results2->price;
            $relatedTicket->available_qty = 0;
            $relatedTicket->user_id = $results2->user_id;
            $relatedTicket->selling_commission_percentage = $results2->selling_commission_percentage;
            $relatedTicket->selling_commission_amount = $results2->selling_commission_amount;
            $relatedTicket->save();

            $etb = FootballTicketBuy::find($saleId);
            $etb->product_id = $response;
            $etb->save();
            return Response::json("Successfully changed form of ticket", 200);
        }
        else{
            return Response::json("Couldnt create magento product", 404);
        }


        return Response::json("There was a problem changing form of ticket", 404);
    }
    //ETicket index
    public function eTicketIndex()
    {
        $this->__construct();
        View::share('menu', 'e-tickets');
        return View::make('ticketAdmin::sold-tickets.etickets');
    }

    public function getUnapprovedETickets()
    {
        $limit = 10;
        $skip = 0;

        if(Input::has('length'))
        {
            $limit = Input::get('length');
        }
        if(Input::has('start'))
        {
            $skip = Input::get('start');
        }

        $tickets = DB::table('e_ticket_uploads as etu')
                ->leftJoin('events_ticket_buy as etb', 'etu.order_id', '=', 'etb.order_id')
                ->leftJoin('events_related_tickets as ert','etb.product_id', '=', 'ert.product_id')
                ->leftJoin('events as e', 'ert.event_id', '=','e.id')
                ->leftJoin('web_user as buyer', 'etb.buyer_id', '=', 'buyer.user_id')
                ->leftJoin('web_user as seller', 'ert.user_id', '=', 'seller.user_id')
                ->where('etu.available', '=', 0)
                ->orderBy('etu.id', 'DESC')
                ->select(
                    'etb.order_id',
                    'buyer.firstname as bfirst',
                    'buyer.lastname as blast',
                    'buyer.user_id as bid',
                    'ert.ticket as ticket',
                    'etb.qty as qty',
                    'seller.firstname as sfirst',
                    'seller.lastname as slast',
                    'seller.user_id as sid',
                    'e.title as title',
                    'e.datetime as event_date'
                )
                ->groupBy('etb.order_id')
                ->skip($skip)
                ->take($limit)
                ->get();

        $count = count($tickets);
        $results=array();
        $results['recordsTotal'] = $count;
        $results['recordsFiltered']=$count;
        $temp=array();
        $i = 0;
        foreach($tickets as $ticket)
        {
            $temp[$i]=array();
            $tic = json_decode($ticket->ticket);
            $typestr = $tic->ticketInformation->ticket_type;
            $res = DB::table('events_ticket_type')
                        ->where('id', '=', $typestr)
                        ->first();
            $typestr = $res->title;

            $temp[$i]['0'] = $ticket->order_id;
            $temp[$i]['1'] = '<a href="#" data-uid = "'.$ticket->bid.'">'.$ticket->bfirst ." ".$ticket->blast."</a>";
            $temp[$i]['2'] = $typestr;
            $temp[$i]['3'] = $tic->ticketInformation->loc_block;
            $temp[$i]['4'] = $tic->ticketInformation->loc_row;
            $temp[$i]['5'] = '<a href="#" data-uid = "'.$ticket->sid.'">'.$ticket->sfirst ." ".$ticket->slast."</a>";
            $temp[$i]['6'] = $ticket->qty;
            $temp[$i]['7'] = $ticket->title;
            $temp[$i]['8'] = $ticket->event_date;
            $temp[$i]['9'] = '<a href="#" class="viewA" data-id="'.$ticket->order_id.'">Actions</a>';




            $i++;
        }
        $results['data']=$temp;
        return Response::json($results,200);
    }

    public function getAdminETicketApproveById($oId)
    {
        $tickets = ETicket::where('order_id', '=', $oId)->get();
        return View::make('ticketAdmin::sold-tickets.e-ticket-admin', ['order_id'=>$oId, 'tickets'=>$tickets]);
    }


    public function uploadETicket()
    {

    }

        //Create Views
    //Eticket list

    public function eTicketIndivIndex()
    {
        $this->__construct();
        View::share('menu', 'sold-tickets');
        return View::make('ticketAdmin::sold-tickets.list');
    }
    //ETicket indiv-index
        //Create Views




    public function deferredOrders()
    {
        View::share('menu', 'deferred-orders');

        $brokers = WebUser::where('seller_type', "broker")->get();
        View::share('brokers', $brokers);
        return View::make('ticketAdmin::sold-tickets.deferred-orders');
    }

    public function deferredOrdersList()
    {

        $limit = 10;
        $start = 0;

        $input = Input::all();
        if(array_key_exists('length',$input))
        {
            $limit = $input['length'];
        }
        if(array_key_exists('start',$input))
        {
            $start = $input['start'];
        }
        $orders = DB::table('deferred_orders')
                    ->leftJoin('events_related_tickets', 'deferred_orders.product_id', '=', 'events_related_tickets.product_id')
                    ->leftJoin('events', 'events_related_tickets.event_id', '=', 'events.id')
                    ->leftJoin('web_user', 'events_related_tickets.user_id', '=', 'web_user.user_id')
                    ->select('web_user.firstname', 'web_user.lastname', 'web_user.seller_type', 'events.title', 'events.datetime',
                        'deferred_orders.qty', 'deferred_orders.amount', 'events_related_tickets.product_id', 'events_related_tickets.ticket', 'deferred_orders.created_at',
                        'deferred_orders.order_id')
                    ->skip($start)
                    ->limit($limit)
                    ->get();

        $count = count($orders);

        $results=array();
        $results['recordsTotal'] = $count;
        $results['recordsFiltered']=$count;
        $data = array();
        foreach($orders as $order)
        {
            $temp[]=array();

            $tic = json_decode($order->ticket);
            $typestr = $tic->ticketInformation->ticket_type;
            $res = DB::table('events_ticket_type')
                ->where('id', '=', $typestr)
                ->first();
            $typestr = $res->title;

            $formstr = $tic->ticketInformation->form_of_ticket;
            $res = DB::table('events_form_of_ticket')
                    ->where('id', $formstr)
                    ->first();

            $formstr = $res->title;

            $loc_bloc = $tic->ticketInformation->loc_block;
            $loc_row = $tic->ticketInformation->loc_row;

            $locString = $typestr.'<br> BLOCK:'.$loc_bloc.' ROW:'.$loc_row;

            $temp['0'] = $order->title;
            $temp['1'] = $order->datetime;
            $temp['2'] = $order->created_at;
            $temp['3'] = $order->order_id;
            $temp['4'] = $order->firstname." ".$order->lastname;
            $temp['5'] = $order->seller_type;
            $temp['6'] = $order->qty;
            $temp['7'] = $order->amount;
            $temp['8'] = $formstr;
            $temp['9'] = $locString;
            $temp['10'] = '<button class="release-button" data-id="'.$order->order_id.'">Release</button><br><button class="reacq-button" data-id="'.$order->order_id.'">Reacquire</button>';

            $data[] = $temp;
        }
        $results['data']=$data;
        return Response::json($results,200);

    }


    public function releaseDeferred($order_id)
    {
        $dOrder = DeferredOrder::where('order_id', $order_id)->first();
        if($dOrder)
        {
            $etb = new FootballTicketBuy();
            $fillArray = $dOrder->toArray();
            $etb->fill($fillArray);
            $etb->save();
            $rt = RelatedTicket::where('product_id', $dOrder->product_id);
            RelatedTicket::updateTicket($dOrder->product_id, $dOrder->qty);

            $this->sendEmailConfirmationToSellerCopied($order_id);
            DeferredOrder::where('order_id', $order_id)->delete();

            return Response::json("Success", 200);
        }
        else
        {
            return Response::json("Could not find deferred order",404);
        }
    }

    private function sendEmailConfirmationToSellerCopied($orderId = '')
    {
        try {
            FootballTicketBuy::createSaleView();

            $orderDetails = DB::table('sales')->where('order_id', '=', $orderId)->first();

            $event = FootBallEvent::find($orderDetails->event_id);
            $eventTicket = RelatedTicket::where('product_id', '=', $orderDetails->product_id)->first();
            $ticket = RelatedTicket::getTicketByTicketId($orderDetails->product_id);
            $order = TicketSoap::process('sales_order.info', $orderId);


            $data = array();
            $data['game'] = $event->title . " - " . date('l, d F Y, h:ia', strtotime($event->datetime)) . ", " . $event->event_location;
            $data['event_id'] = $event->id;
            $data['order_id'] = $orderId;
            $data['total_amount'] = $eventTicket->price * $orderDetails->qty;
            $data['commission'] = ($data['total_amount'] * (empty($eventTicket->selling_commission_percentage) ? 15 : (int)$eventTicket->selling_commission_percentage) / 100);
            $data['total_amount_after_commission'] = $data['total_amount'] - $data['commission'];
            $data['seller_name'] = $orderDetails->seller_firstname . " " . $orderDetails->seller_lastname;
            $data['qty'] = $orderDetails->qty;

            $ticketInfo = json_decode($eventTicket->ticket, true);

            $data['formOfTicket'] = @$ticket->formOfTicket['title'];
            $data['location'] = @$ticket->ticketType['title'];
            $temp = array();
            if (isset($ticketInfo['ticketInformation']['loc_block']) && !empty($ticketInfo['ticketInformation']['loc_block'])) {
                $temp[] = 'Block: ' . $ticketInfo['ticketInformation']['loc_block'];
            }

            if (isset($ticketInfo['ticketInformation']['loc_row']) && !empty($ticketInfo['ticketInformation']['loc_row'])) {
                $temp[] = 'Row: ' . $ticketInfo['ticketInformation']['loc_row'];
            }

            $data['location'] = $data['location'] . ', ' . implode(', ', $temp);

            $temp = array();

            $defaultRestriction = [];
            foreach ($event->getSelectedRestrictions() as $val) {

                $defaultRestriction[] = array(
                    'id' => $val->id,
                    'title' => $val->title
                );
            }

            foreach ($defaultRestriction as $r):
                if (in_array($r['id'], $ticket->info['restrictions'])):
                    $temp[] = $r['title'];
                endif;
            endforeach;

            if ($ticket->buyerNot != ''):
                $temp[] = '<br /> Note: ' . $ticket->buyerNot;
            endif;

            $data['restrictions'] = implode(', ', $temp);

            $sm = $order['shipping_method'];
            $rateName = explode('_', $sm);
            $rateName = $rateName[0];
            $ships = DB::table('shipping_method_assoc')
                ->where('name', '=', $rateName)
                ->first();
            $data['shipping_method']=$ships->description;

            Mail::send('footballticket::email.sold-ticket-confirmation-to-seller', $data, function ($message) use ($orderDetails, $orderId) {
                $from = Config::get('mail.from');
                $from['address'] = trans('homepage.contact-email');
                $message->from($from['address'], $from['name']);
                $email = Options::getOption('contact_email') == '' ? 'hello@bondmedia.co.uk' : Options::getOption('contact_email');

                $message->to($orderDetails->seller_email, $orderDetails->seller_firstname . " " . $orderDetails->seller_lastname)
                    ->bcc($email, 'Football Ticket Pad')
                    ->subject("Confirmation of ticket sale: New Order # {$orderId}");
            });


        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }


    public function reacquireOrder($order_id)
    {

        if(!Input::has('broker_id'))
        {
            return Response::json("Requires broker id");
        }

        $broker_id = Input::get('broker_id');

        $broker = WebUser::where('user_id', $broker_id)->first();

        if(!$broker)
        {
            return Response::json("Could not find broker");
        }

        $dOrder = DeferredOrder::where('order_id', $order_id)->first();
        if($dOrder)
        {
            try {
                $relatedTicket = RelatedTicket::where('product_id', $dOrder->product_id)->first();

                $attributeSet = TicketSoap::process('product_attribute_set.list');
                $time = time();

                $data = json_decode($relatedTicket->ticket);

                $event = FootBallEvent::where('id', $relatedTicket->event_id)->first();

                $info = $event->toArray();

                $product = array(
                    'simple',
                    $attributeSet[0]['set_id'],
                    "-{$relatedTicket->id}-{$relatedTicket->user_id}-{$data->ticketInformation->ticket_type}-{$data->ticketInformation->loc_block}-{$data->ticketInformation->loc_row}-{$time}",
                    array(
                        'categories' => array(2),
                        'websites' => array(1),
                        'name' => $info['title'],
                        'description' => '-',
                        'short_description' => '-',
                        'weight' => '1',
                        'status' => '1',
                        'url_key' => $info['slug'],
                        'url_path' => $info['slug'],
                        'visibility' => '4',
                        'price' => $data->ticketInformation->price,
                        'tax_class_id' => 0,
                        'meta_title' => $info['title'],
                        'meta_keyword' => strip_tags($info['content']),
                        'meta_description' => strip_tags($info['content']),
                        'stock_data' => array(
                            'qty' => $data->ticketInformation->number_of_ticket,
                            'is_in_stock' => 1,
                            'min_sale_qty' => 1

                        ),

                    )
                );
                $response = TicketSoap::process('catalog_product.create', $product);

                if ($response) {
                    $relatedTicketNew = new RelatedTicket();
                    $relatedTicketNew->fill($relatedTicket->toArray());
                    $relatedTicketNew->product_id = $response;
                    $relatedTicketNew->save();

                    $relatedTicket->user_id = $broker_id;
                    $relatedTicket->available_qty = 0;
                    $relatedTicket->save();

                    $etb = new FootballTicketBuy();
                    $etb->fill($dOrder->toArray());
                    $etb->save();

                    //wrap in try catch
                    //Send email


                    $this->sendEmailConfirmationToSellerCopied($dOrder->order_id);

                    DB::table('deferred_orders')
                            ->where('order_id', $dOrder->order_id)
                            ->delete();

                    return Response::json("Success", 200);

                }
            }
            catch (Exception $e)
            {
                $response = Response::make(json_encode(['message' => $e->getMessage()]), '400');
                $response->header('Content-Type', 'application/json');
                return $response;
            }
        }
        else
        {
            return Response::json("Could not find deferred order.",404);
        }
    }

}
