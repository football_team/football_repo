<?php
namespace Bondmedia\Website\ShortCode;

use Bond\Abstracts\BaseShortCodeAbstract;
use Bond\Interfaces\BaseShortCodeInterface;
use View;
use Article;
class LatestNews extends BaseShortCodeAbstract implements BaseShortCodeInterface{
    protected $params = array();
    public function __construct() {
        $this->setTemplate("website::news.latest-news");
        return $this;
    }

    /**
     * @return mixed
     */
    public function render() {
        $locale = \App::getLocale();
        $i = 0;

        if(\Cache::has($locale.'_footer_news'))
        {
            $ret = \Cache::get($locale.'_footer_news');
        }
        else
        {
            $ret = array();

            if($locale == 'en_gb')
            {
                $latestNews = Article::where('is_published', '=', '1')
                    ->orderBy('created_at', 'DESC')
                    ->take(4)
                    ->get();

                foreach($latestNews as $j=>$news)
                {
                    $news->lText = $news->getLocaleText();
                    $ret[] = $news;
                    $i++;
                }

                \Cache::put($locale.'_footer_news', $ret, 3600);
            }
        }


        /*
        foreach($latestNews as $j=>$news)
        {
            if($i < 4)
            {
                if($locale != 'en_gb')
                {
                    if($news->checkLText($locale))
                    {
                        $news->lText = $news->getLocaleText();
                        $ret[] = $news;
                        $i++;
                    }
                }
                else
                {
                    $news->lText = $news->getLocaleText();
                    $ret[] = $news;
                    $i++;
                }

            }


        }
        */
        View::share('latestNews', $ret);
        return View::make($this->getTemplate(), $this->params)
            ->render();
    }
}



