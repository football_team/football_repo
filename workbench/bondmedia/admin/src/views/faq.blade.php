@if(array_key_exists('faqs', $faqs) && sizeof($faqs['faqs']) > 0)
@if(array_key_exists('cat', $faqs))
    <h2>{{trans('homepage.faq-category-'.$faqs['cat'])}}</h2>
@endif
@if(array_key_exists('faqs', $faqs))
<div class="accordion">
    @foreach($faqs['faqs'] as $faq)
    <h4 class="accordion-toggle">
        {{ $faq->title }}
        <span class="toggle-arrow"></span>
    </h4>
    <div class="accordion-content">
        {{ $faq->content }}
    </div>
    @endforeach
</div>
@endif
@endif