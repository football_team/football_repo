<section class="banner-home">
@if(!empty($images))
    <div class="flexslider">
        <ul class="slides">
            @foreach($images as $n => $image)
                <li class="slider-image slider-image-{{$image->id}}"
                    data-id="{{$image->id}}"
                    data-title="{{addslashes($image->title)}}"
                    data-url="{{$image->url}}"
                    data-description="{{addslashes($image->description)}}"
                    data-order="{{$image->order}}"
                    data-path="{{ $image->path }}">
                    <div class="slide @if($n==0) active @endif" style="background-image:url({{ $image->path }});"></div>
                </li>
            @endforeach
        </ul>
        <div id="controls">
            @foreach($images as $n=>$image)
                <a href="{{$image->url}}" class="control @if($n==0) active @endif" data-slide="{{$image->order}}">
                    <div class="slide-title">{{trans('homepage.frticbefore')}} {{$image->title}} {{trans('homepage.Tickets')}}</div><img src="{{$image->description}}" alt="">
                </a>
            @endforeach
        </div>
    </div>
@endif
</section>
@section('script')
<script>
    $('document').ready(function(){
        var flexInterval = false;
        var flexTime = 8000;
        function flexSlideNext(){
            var activeControl = $('.flexslider #controls .control.active');
            var activeSlide = $('.flexslider .slides .slider-image .slide.active');
            var nextControl = $(activeControl).next();
            var nextSlide = $(activeSlide).parent().next().find('.slide');

            if(!nextControl.length)
            {
                nextControl = $('.flexslider #controls .control').first();
                nextSlide = $('.flexslider .slides .slider-image').first().find('.slide');
            }

            $(activeControl).removeClass('active');
            $(activeSlide).removeClass('active');
            $(nextControl).addClass('active');
            $(nextSlide).addClass('active');
        }

        flexInterval = setInterval(flexSlideNext, flexTime);

        $('.flexslider #controls .control').hover(
            function(){
                clearInterval(flexInterval);
                if(!jQuery(this).hasClass('active'))
                {
                    var otherID = jQuery(this).data('slide');
                    $('.flexslider #controls .control').removeClass('active');
                    $('.flexslider .slides .slider-image .slide').removeClass('active');
                    $(this).addClass('active');
                    $('.flexslider .slides .slider-image[data-order="'+otherID+'"] .slide').addClass('active');
                }
            },
            function(){
                flexInterval = setInterval(flexSlideNext, flexTime);
            }
        )
    });
</script>
@endsection