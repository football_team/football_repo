    @if(!empty($images))
        @foreach($images as $image)
        <article class="three columns">
            <div class="panel newstyle">
                <a href="{{$image->url}}">
                    <span class="hovereffect">
                    <img class="plushover" src="{{ Assets::Path('images/pluspng.png') }}" alt="plus"/>
                    <img class="ftplinehover" src="{{ Assets::Path('images/ftpline.png') }}" alt="line"/>
                    </span>
                    <img src="{{ $image->path }}" alt="{{trans('homepage.Buy')}} {{$image->title}}"/></a> 
                <a class="full-row" href="{{$image->url}}"> <h4 class="linktitles">{{trans('homepage.'.$image->title)}}</h4></a>
                <a class="full-row block-view" href="{{$image->url}}">{{trans('homepage.view')}}</a>
            </div>
        </article>
        @endforeach
    @endif
