<?php
/**
 * Created by PhpStorm.
 * User: baglad
 * Date: 06/06/2016
 * Time: 14:34
 */

class HeaderComposer{

    public function compose($view)
    {
        $user = Sentry::getUser();
        $view->with('user',$user);
    }
}