@extends(Template::name('CustomerServices::backend._layout.layout'))

@section('title')
    Customer Services - Dashboard
@stop

@section('page_title')
    Customer Services
@stop

@section('page_sdesc')
    Overview
@stop

@section('page_content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Customers</h3><br>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <table id="customerDT" class="table table-bordered table-hover" width="100%">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>First Name</th>
                                    <th>Second Name</th>
                                    <th>Email Address</th>
                                    <th>Phone Number</th>
                                    <th>Customer Type</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--
        View customer buttons need linking
        Customer activity feed
        Need to implement customer notes

        ok den so what now?

        I need to implement the customer notes adding thing but the db is done,
        Now I need to do an email log
    -->
@stop

@section('style')

@stop

@section('script')
    <script src="/assets/adminlte/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="/assets/adminlte/plugins/datatables/dataTables.bootstrap.min.js"></script>

    <script>
        $('#customerDT').dataTable({
            paging: true,
            lengthChange: true,
            searching: true,
            ordering: false,
            serverSide: true,
            ajax: {
                url: "/customer-service/customers/list",
                type: 'POST'
            },
            order: [[0, 'ASC']],
            "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
                $('.delete-node', nRow).remove();
            }
        });
    </script>
@stop
