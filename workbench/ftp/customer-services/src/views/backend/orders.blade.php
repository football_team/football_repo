@extends(Template::name('CustomerServices::backend._layout.layout'))

@section('title')
    Customer Services - Orders
@stop


@section('page_title')
    Customer Services
@stop

@section('page_sdesc')
    Orders
@stop

@section('page_content')
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" id="modalTitle"></h4>
                </div>
                <div class="modal-body" id="fill-this">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">
                                Advanced Search
                            </h3>
                        </div>
                        <div class="box-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <input type="text" class="form-control pull-right" value="{{$dateRange}}" id="reservation">
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-xs-12 filter-buttons">
                                    <button class="btn adv-sh" id="adv-sh"><span class="fa fa-search" ></span></button>
                                </div>
                            </div>
                            <div class="row" id="adv-search-row">
                                <div class="col-xs-12 adv-search-fields">
                                    <div class="row search-fields">
                                        <div class="col-sm-6">
                                            Buyer First Name:<br> <input type="text" data-name="bfn">
                                        </div>
                                        <div class="col-sm-6">
                                            Buyer Second Name:<br> <input type="text" data-name="bln">
                                        </div>
                                        <div class="col-sm-6">
                                            Buyer Email:<br> <input type="text" data-name="bemail">
                                        </div>
                                        <div class="col-sm-6">
                                            Order Number:<br> <input type="text" data-name="sOn">
                                        </div>

                                        <div class="col-sm-6">
                                            Seller First Name:<br> <input type="text" data-name="sfn">
                                        </div>
                                        <div class="col-sm-6">
                                            Seller Second Name:<br> <input type="text" data-name="sln">
                                        </div>
                                        <div class="col-sm-6">
                                            Seller Email:<br> <input type="text" data-name="semail">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-xs-12" id="timeline-holder">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('style')
    <style>
        .filter-buttons .btn{
            background-color:#777884 !important;
            color:#fff;
            margin:5px;
        }
        .filter-buttons .btn.active{
            background-color:#8897FF !important;
        }

        .search-fields input{
            width:100%;
        }

        .adv-sh.active{
            background-color:lightgreen;
        }

        #adv-search-row{
            height:0px;
            overflow:hidden;
        }

        #adv-search-row.active{
            height:initial;
        }
    </style>
@stop

@section('script')
    <script>
        $(function(){


            $('#reservation').daterangepicker({
                format: 'DD/MM/YYYY'
            });

            $('#reservation').change(function(){
                refreshFeed();
            });

            $('body').on('click', '.view-full-order', function(){
                var id = $(this).data('id')

                $.ajax({
                    url:"/customer-service/customers/getFullOrder/"+id,
                    type:"POST",
                    success:function(response){
                        $('#fill-this').html(response);
                        $('#myModal').modal('show');
                    },
                    error:function(response){

                    }
                })
            });

            $('.filter-buttons .btn').click(function(){
                $(this).toggleClass('active');

                if($(this).hasClass('adv-sh'))
                {
                    $('#adv-search-row').toggleClass('active');
                }
                refreshFeed();

            });

            $('.search-fields input').change(function(){
                refreshFeed();
            });

            $('body').on('click', '.view-order-notes',function(){
                var id = $(this).data('id');

                $.ajax({
                    url:"/customer-service/customer/view-order-notes/"+id,
                    type:"POST",
                    success:function(response){
                        $('#fill-this').html(response);
                        $('#myModal').modal('show');
                    },
                    error:function(response){
                        alert("Error");
                    }
                });
            });

            $('body').on('click','.save-order-note-btn', function(){
                var id = $(this).data('id');
                var note = $('#edit-o-note-'+id).val();

                $.ajax({
                    url:'/customer-service/customers/update-order-note/'+id,
                    type:"POST",
                    data:{
                        note:note
                    },
                    success:function(){

                    },
                    error:function(){
                        alert("Error, order note not saved.");
                    }
                });
            });

            $('body').on('click','.delete-order-note', function(){
                var id = $(this).data('id');

                $.ajax({
                    url:"/customer-service/customers/delete-order-note/"+id,
                    type:"POST",
                    success:function(response){
                        $('#fill-this').html(response);
                        refreshFeed();
                    },
                    error:function(response){

                    }
                });
            });

            $('body').on('click', '#save-new-order-note', function(){
                var note = $('#new-order-note-note').val();
                var id = $(this).data('id');

                $.ajax({
                    url:"/customer-service/customers/save-new-order-note/"+id,
                    type:"POST",
                    data:{
                        note:note
                    },
                    success:function(response){
                        $('#fill-this').html(response);
                        refreshFeed();
                    },
                    error:function(response){

                    }
                });
            });

            refreshFeed();

            function refreshFeed(){
                var btns = $('.filter-buttons .btn.active').toArray();
                var athing = Array();
                var advSearch = Array();
                var advOrder = Array();

                for(var i = 0; i<btns.length;i++)
                {
                    athing.push($(btns[i]).data('id'));
                }

                if($('#adv-sh').hasClass('active'))
                {
                    //Has class active must gather input data
                    $('.search-fields input').each(function(){
                        var v = $(this).val();
                        var index = $(this).data('name');

                        if(!v == '')
                        {
                            advOrder.push(index);
                            advSearch.push(v);
                        }
                    });
                }
                else
                {
                    $('.search-fields input').val('');
                }
                $.ajax({
                    url:"/customer-service/orders/activity-feed/",
                    type:"POST",
                    data:{
                        range:$('#reservation').val(),
                        filt:athing,
                        adv:advSearch,
                        ord:advOrder
                    },
                    success:function(response){
                        $('#timeline-holder').html(response);
                    },
                    error:function(response){
                        alert("Error");
                    }
                });
            };
        });
    </script>
@stop
