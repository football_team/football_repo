<table class="table">
    <thead>
    <tr>
        <th>
            Date Time
        </th>
        <th>
            Information
        </th>
    </tr>
    </thead>
    <tbody>
    @foreach($abandoned as $object)
            <tr>
                <td>
                    {{$object->created_at}}
                </td>
                <td>
                    <b><a href="/customer-service/customers/{{$object->buyer_id}}">{{$object->buyer_firstname}} {{$object->buyer_lastname}}</a></b><br>
                    Abandoned a cart with tickets for <b>{{$object->title}}</b> on <b>{{$object->event_date}}</b><br>
                    Tickets were priced as {{$object->price}} each.<br>
                    From <b><a href="/customer-service/customers/{{$object->seller_id}}">{{$object->seller_firstname}} {{$object->seller_lastname}}</a></b><br><br>

                    Contact Email: <a href="mailto:{{$object->fucemail}}">{{$object->fucemail}}</a><br>
                    Telephone: {{$object->fuctelephone}}
                </td>
            </tr>
    @endforeach
    </tbody>
</table>