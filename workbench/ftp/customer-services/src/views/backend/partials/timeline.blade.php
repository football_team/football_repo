<table class="table">
    <thead>
        <tr>
            <th>
                Date Time
            </th>
            <th>
                Type
            </th>
            <th>
                Information
            </th>
            <th>
                Actions
            </th>
        </tr>
    </thead>
    <tbody>
    @foreach($tla as $object)
        @if($object->type == "email")
            <tr>
                <td>
                    {{$object->created_at}}
                </td>
                <td>
                    Email
                </td>
                <td>
                    {{$object->subject}}<br>
                    Recipient: {{$object->address}}
                </td>
                <td>
                    <a href="/customer-service/customers/getFullEmail/{{$object->id}}" target="_blank" class="btn btn-primary btn-xs">View Full Email</a>
                </td>
            </tr>
        @endif
        @if($object->type == "user_note")
            <tr>
                <td>
                    {{$object->created_at}}
                </td>
                <td>
                    User Note created by {{$object->first_name}} {{$object->last_name}}
                </td>
                <td>
                    {{$object->note}}
                </td>
                <td>
                    <a class="btn btn-primary btn-xs editNote" data-id="{{$object->id}}">Edit Note</a><br>
                    <a class="btn btn-primary btn-xs deleteNote" data-id="{{$object->id}}">Delete Note</a>
                </td>
            </tr>
        @endif
        @if($object->type == "sale")
            <tr>
                <td>
                    {{$object->created_at}}
                </td>
                <td>
                    Sale - {{$object->order_id}}
                </td>
                <td>
                    Sold <b>{{$object->qty}}</b> ticket(s) for <b>{{$object->title}}</b> on <b>{{$object->event_date}}</b><br>
                    Sale Total: <b>£{{number_format($object->amount, 2)}}</b><br>
                    Ordered by: <b>{{$object->buyer_firstname}} {{$object->buyer_lastname}}</b>
                </td>
                <td>
                    <a class="btn btn-primary btn-xs view-full-order" data-id="{{$object->order_id}}">View Full</a><br>
                    @if(isset($object->notes))
                        <a class="btn btn-primary btn-xs view-order-notes" data-id="{{$object->order_id}}">View {{sizeof($object->notes)}} Note(s)</a>
                    @else
                        <a class="btn btn-primary btn-xs view-order-notes" data-id="{{$object->order_id}}">Add Note</a>
                    @endif
                </td>
            </tr>
        @endif
        @if($object->type == "purchase")
            <tr>
                <td>
                    {{$object->created_at}}
                </td>
                <td>
                    Purchase - {{$object->order_id}}
                </td>
                <td>
                    Purchased <b>{{$object->qty}}</b> ticket(s) for <b>{{$object->title}}</b> on <b>{{$object->event_date}}</b><br>
                    Total: <b>£{{number_format($object->amount, 2)}}</b><br>
                    Ordered From: {{$object->firstname}} {{$object->lastname}}
                </td>
                <td>
                    <a class="btn btn-primary btn-xs view-full-order" data-id="{{$object->order_id}}">View Full</a><br>
                    @if(isset($object->notes))
                        <a class="btn btn-primary btn-xs view-order-notes" data-id="{{$object->order_id}}">View {{sizeof($object->notes)}} Note(s)</a>
                    @else
                        <a class="btn btn-primary btn-xs view-order-notes" data-id="{{$object->order_id}}">Add Note</a>
                    @endif
                </td>
            </tr>
        @endif
        @if($object->type == "listing")
            <tr>
                <td>
                    {{$object->created_at}}
                </td>
                <td>
                    Listed Tickets
                </td>
                <td>
                    Listed <b>{{$object->qty}}</b> ticket(s) for <b>{{$object->title}}</b> on <b>{{$object->event_date}}</b><br>
                    Price: <b>£{{number_format($object->price, 2)}}</b>
                </td>
                <td>

                </td>
            </tr>
        @endif
        @if($object->type == "failed_transaction")
            <tr>
                <td>
                    {{$object->created_at}}
                </td>
                <td>
                    <b>Failed Transaction</b>
                </td>
                <td>
                    Attempted to buy tickets for <b>{{$object->title}}</b> on <b>{{$object->event_date}}</b><br>
                    Tickets were priced as {{$object->price}} each.
                </td>
                <td>
                    <a class="btn btn-primary btn-xs release-transaction" data-id="{{$object->tid}}">Release</a>
                </td>
            </tr>
        @endif
    @endforeach
    </tbody>
</table>