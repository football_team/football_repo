<table class="table">
    <thead>
    <tr>
        <th>
            Date Time
        </th>
        <th>
            Information
        </th>
        <th>
            Actions
        </th>
    </tr>
    </thead>
    <tbody>
    @foreach($failed as $object)
        <tr>
            <td>
                {{$object->created_at}}
            </td>
            <td>
                Customer <b><a href="/customer-service/customers/{{$object->buyer_id}}">{{$object->firstname}} {{$object->lastname}}</a></b>
                Attempted to buy tickets for <b>{{$object->title}}</b> on <b>{{$object->event_date}}</b><br>
                From <b><a href="/customer-service/customers/{{$object->seller_id}}">{{$object->sfirstname}} {{$object->slastname}}</a></b><br>
                Tickets were priced as {{$object->price}} each.
            </td>
            <td>
                <a class="btn btn-primary btn-xs release-transaction" data-id="{{$object->tid}}">Release</a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>