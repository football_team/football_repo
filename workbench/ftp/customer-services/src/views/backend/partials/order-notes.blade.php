<h3>Order ID {{$oId}}</h3>
<table class="table">
    <thead>
    <tr>
        <th>
            Note
        </th>
        <th>
            Created At
        </th>
        <th>
            Created By
        </th>
        <th>
            Actions
        </th>
    </tr>
    </thead>
    <tbody>
        @foreach($notes as $note)
            <tr>
                <td>
                    <textarea id="edit-o-note-{{$note->id}}" cols="30" rows="3">{{$note->note}}</textarea>
                </td>
                <td>
                    {{$note->created_at}}
                </td>
                <td>
                    {{$note->first_name}} {{$note->last_name}}
                </td>
                <td>
                    <button class="btn btn-success save-order-note-btn" data-id="{{$note->id}}">Save</button>
                    <button class="btn btn-danger delete-order-note" data-id="{{$note->id}}">Delete</button>
                </td>
            </tr>
        @endforeach



    </tbody>
</table>
<textarea id="new-order-note-note" cols="30" rows="10" style="width:100%"></textarea> <button class="btn" id="save-new-order-note" data-id="{{$oId}}">Add new</button>