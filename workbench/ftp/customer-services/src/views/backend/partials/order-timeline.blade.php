<table class="table">
    <thead>
    <tr>
        <th>
            Date Time
        </th>
        <th>
            Type
        </th>
        <th>
            Information
        </th>
        <th>
            Actions
        </th>
    </tr>
    </thead>
    <tbody>
    @foreach($sales as $sale)
        <tr>
            <td>
                {{$sale->created_at}}
            </td>
            <td>
                {{$sale->order_id}}
            </td>
            <td>
                Sold <b>{{$sale->qty}}</b> ticket(s) for <b>{{$sale->title}}</b> on <b>{{$sale->event_date}}</b><br>
                Sale Total: <b>£{{number_format($sale->amount, 2)}}</b><br>
                Ordered by: <b><a href="/customer-service/customers/{{$sale->buyer_id}}">{{$sale->buyer_firstname}} {{$sale->buyer_lastname}}</a></b>
                Ordered from: <b><a href="/customer-service/customers/{{$sale->seller_id}}">{{$sale->seller_firstname}} {{$sale->seller_lastname}}</a></b>
            </td>
            <td>
                <a class="btn btn-primary btn-xs view-full-order" data-id="{{$sale->order_id}}">View Full</a><br>
                @if(isset($sale->notes))
                    <a class="btn btn-primary btn-xs view-order-notes" data-id="{{$sale->order_id}}">View {{sizeof($sale->notes)}} Note(s)</a>
                @else
                    <a class="btn btn-primary btn-xs view-order-notes" data-id="{{$sale->order_id}}">Add Note</a>
                @endif
            </td>
        </tr>
    @endforeach
    </tbody>
</table>