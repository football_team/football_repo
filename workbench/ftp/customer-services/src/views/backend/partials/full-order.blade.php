<h1>Seller Details</h1>
<table class="table">
    <thead>
    <tr>
        <th>
            Title
        </th>
        <th>
            Data
        </th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>
            Seller Name
        </td>
        <td>
            {{$data['seller']->firstname.' '.$data['seller']->lastname}}
        </td>
    </tr>
    <tr>
        <td>Seller Email</td>
        <td>{{$data['seller']->email}}</td>
    </tr>
    <tr>
        <td>Seller Number</td>
        <td>{{$data['seller']->telephone}}</td>
    </tr>
    </tbody>
</table>
<h1>Buyer Details</h1>
<table class="table">
    <thead>
    <tr>
        <th>
            Title
        </th>
        <th>
            Data
        </th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>Buyer Name</td>
        <td>{{$data['buyer']->firstname.' '.$data['buyer']->lastname}}</td>
    </tr>
    <tr>
        <td>Buyer Email</td>
        <td>{{$data['buyer']->email}}</td>
    </tr>
    <tr>
        <td>Buyer Number</td>
        <td>{{$data['buyer']->telephone}}</td>
    </tr>
    </tbody>
</table>
<h1>Event Details</h1>
<table class="table">
    <thead>
    <tr>
        <th>
            Title
        </th>
        <th>
            Data
        </th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>Event Title</td>
        <td>{{$data['event']->title}}</td>
    </tr>
    <tr>
        <td>Event Date</td>
        <td>{{$data['event']->datetime}}</td>
    </tr>
    </tbody>
</table>
<h1>Order Details</h1>
<table class="table">
    <thead>
    <tr>
        <th>
            Title
        </th>
        <th>
            Data
        </th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>Order Date</td>
        <td>{{$data['order']->created_at}}</td>
    </tr>
    <tr>
        <td>Ticket Type</td>
        <td>{{$data['misc']['ttype']}}</td>
    </tr>
    <tr>
        <td>Ticket Location</td>
        <td>Block: {{$data['misc']['block']}} Row: {{$data['misc']['row']}}</td>
    </tr>
    <tr>
        <td>Ticket Restrictions</td>
        <td>
            @if(!empty($data['misc']['restrictions']))
                @foreach($data['misc']['restrictions'] as $key=>$restriction)
                    <h4>{{$key}}:</h4> {{$restriction}}<br>
                @endforeach
            @endif
        </td>
    </tr>
    <tr>
        <td>Price Per Ticket</td>
        <td>£{{number_format($data['order']['amount']/$data['order']['qty'],2)}}</td>
    </tr>
    <tr>
        <td>Number Sold</td>
        <td>{{$data['order']['qty']}}</td>
    </tr>
    <tr>
        <td>Total Sale Price</td>
        <td>£{{number_format($data['order']['amount'],2)}}</td>
    </tr>
    <tr>
        <td>Total Commission</td>
        <td>
            £{{number_format(($data['order']['amount']-($data['order']['amount']-$data['order']['fees_amount'])*0.85),2)}}
        </td>
    </tr>
    <tr>
        <td>Broker Earnings</td>
        <td>
            £{{number_format((($data['order']['amount']-$data['order']['fees_amount'])*0.85),2)}}
        </td>
    </tr>
    </tbody>
</table>