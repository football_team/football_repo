@extends(Template::name('CustomerServices::backend._layout.layout'))

@section('title')
    Customer Services - Calendar
@stop

@section('page_title')
    Customer Services
@stop

@section('page_sdesc')
    Calendar
@stop

@section('page_content')
    <div class="row">
        <div class="col-sm-8">
            <div class="box">
                <div class="box-body"><div id="calendar"></div></div>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="row" id="action-row">
                <div class="col-sm-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Actions</h3>
                        </div>
                        <div class="box-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <!-- Sooo, I need to get the input elements that are going to edit these fucking bitches. -->
                                    <label for="title">Title:</label><input name="title" class="form-control" type="text" id="eet"><br>
                                    <button class="btn btn-success" data-target="" id="save-btn">Save</button>
                                    <button class="btn btn-danger"  data-target="" id="delete-btn">Delete</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@stop

@section('style')
    <link rel="stylesheet" href="/assets/backend/js/plugin/full-calendar/fullcalendar.min.css">
    <style>
        .close-event{
            position: absolute;
            right: 0;
            top: 2px;
        }
    </style>
@stop

@section('script')
    <script src="/assets/backend/js/plugin/full-calendar/moment.min.js"></script>
    <script src="/assets/backend/js/plugin/full-calendar/fullcalendar.min.js"></script>

    <script>
        $(function(){

            $('#calendar').fullCalendar({
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month,agendaWeek'
                },
                defaultDate: '{{$today}}',
                selectable: true,
                selectHelper: true,
                select: function(start, end) {
                    var title = prompt('Event Title:');
                    var eventData;
                    if (title) {
                        eventData = {
                            title: title,
                            start: start,
                            end: end,
                            color:"{{$user_colour}}"
                        };

                        var fuckoff = {
                            title: title,
                            start: moment(start._d).format('YYYY-MM-DD HH:MM:SS'),
                            end: moment(end._d).format('YYYY-MM-DD HH:MM:SS')
                        };

                        $.ajax({
                            url:"/customer-service/calendar/addEvent",
                            type:"POST",
                            data:fuckoff,
                            success:function(response){
                                eventData.id = response;
                                $('#calendar').fullCalendar('renderEvent', eventData, true); // stick? = true
                                $('#calendar').fullCalendar('unselect');
                            },
                            error:function(response){
                            }
                        });
                    }
                },
                editable: true,
                eventLimit: true, // allow "more" link when too many events
                events: {
                    url: '/customer-service/calendar/getEvents',
                    type: 'POST',
                    success:function(response){
                      console.log("I dunno");
                        //This worked
                    },
                    error: function(response) {
                        console.log(response);
                    }
                },
                eventClick: function(calEvent, jsEvent, view) {

                    console.log(calEvent);
                    console.log(view);
                    console.log(calEvent._id);
                    $('#save-btn').data('target', calEvent._id);
                    $('#delete-btn').data('target', calEvent._id);
                    $('#eet').val(calEvent.title);
                },
                eventDrop: function(event, delta, revertFunc) {
                    console.log(event);
                    $.ajax({
                        url:'/customer-service/calendar/moveEvent',
                        type:'POST',
                        data:{
                            id:event.id,
                            start: moment(event.start._d).format('YYYY-MM-DD HH:MM:SS'),
                            end: moment(event.end._d).format('YYYY-MM-DD HH:MM:SS')
                        },
                        success:function(response){

                        },
                        error:function(response){
                            revertFunc();
                        }
                    })
                },
                eventResize: function(event, delta, revertFunc) {
                    console.log(event);
                    $.ajax({
                        url:'/customer-service/calendar/moveEvent',
                        type:'POST',
                        data:{
                            id:event.id,
                            start: moment(event.start._d).format('YYYY-MM-DD HH:MM:SS'),
                            end: moment(event.end._d).format('YYYY-MM-DD HH:MM:SS')
                        },
                        success:function(response){

                        },
                        error:function(response){
                            revertFunc();
                        }
                    })
                }
            });



            $('#save-btn').click(function(){
               var id = $(this).data('target');
                var newttl = $('#eet').val();
                //AJAX IT
                //THEN REFERESH CALENDAR

                $.ajax({
                    url:'/customer-service/calendar/updateTitle',
                    type:'POST',
                    data:{
                        id:id,
                        title:newttl
                    },
                    success:function(response){
                        $('#calendar').fullCalendar('refetchEvents');
                    },
                    error:function(response){

                    }
                })
            });

            $('#delete-btn').click(function(){
                var id = $(this).data('target');

                $.ajax({
                    url:'/customer-service/calendar/deleteEvent',
                    type:"POST",
                    data:{
                        id:id
                    },
                    success:function(response){
                        $('#calendar').fullCalendar('removeEvents',id);

                        $('#save-btn').data('target', '');
                        $('#delete-btn').data('target', '');
                        $('#eet').val('');
                    }
                });
            });
        });

    </script>
@stop
