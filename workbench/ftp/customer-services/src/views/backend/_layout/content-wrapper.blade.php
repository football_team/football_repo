<div class="content-wrapper">
    <section class="content-header">
        <h1>
            @yield('page_title')
            <small>@yield('page_sdesc')</small>
        </h1>
    </section>
    <section class="content">
        @yield('page_content')
    </section>
</div>