<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <!-- Optionally, you can add icons to the links -->
            <li @if($menu == "dashboard"){{'class="active"'}} @endif>
                <a href="/customer-service"><i class="fa fa-dashboard"></i><span>Customers</span></a>
            </li>

            <li @if($menu == "orders"){{'class="active"'}} @endif>
                <a href="/customer-service/orders"><i class="fa fa-cart-plus"></i><span>Orders</span></a>
            </li>

            <li @if($menu == "failed-transactions"){{'class="active"'}} @endif>
                <a href="/customer-service/failed-transactions"><i class="fa fa-warning"></i><span>Failed Transaction</span></a>
            </li>

            <li @if($menu == "abandoned-carts"){{'class="active"'}} @endif>
                <a href="/customer-service/abandoned-carts"><i class="fa fa-hourglass-end"></i><span>Abandoned Carts</span></a>
            </li>

            <li @if($menu == "calendar"){{'class="active"'}} @endif>
                <a href="/customer-service/calendar"><i class="fa fa-calendar"></i><span>Calendar</span></a>
            </li>
        </ul>
        <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>