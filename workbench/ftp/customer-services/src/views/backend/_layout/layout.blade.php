<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <link rel="stylesheet" href="/assets/adminlte/bootstrap/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="/assets/adminlte/dist/css/AdminLTE.min.css" type="text/css">
    <link rel="stylesheet" href="/assets/adminlte/dist/css/skins/skin-red-light.min.css" type="text/css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="/assets/adminlte/dist/css/skins/_all-skins.min.css">
    <link rel="stylesheet" href="/assets/adminlte/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
    <link rel="stylesheet" href="/assets/adminlte/plugins/daterangepicker/daterangepicker-bs3.css">
    <title>@yield('title')</title>

    @yield('style')


</head>
<body class="hold-transition skin-purple-light sidebar-mini">
<div class="wrapper" style="overflow:visible;">
    @include('CustomerServices::backend._layout.header')
    @include('CustomerServices::backend._layout.main-sidebar')
    @include('CustomerServices::backend._layout.content-wrapper')
    @include('CustomerServices::backend._layout.footer')
</div>
</body>

<script src="/assets/adminlte/plugins/jQuery/jQuery-2.2.0.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="/assets/adminlte/bootstrap/js/bootstrap.min.js"></script>
<!-- AdminLTE App -->
<script src="/assets/adminlte/dist/js/app.min.js"></script>
<!-- Sparkline -->
<script src="/assets/adminlte/plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="/assets/adminlte/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="/assets/adminlte/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- SlimScroll 1.3.0 -->
<script src="/assets/adminlte/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- ChartJS 1.0.1 -->
<script src="/assets/adminlte/plugins/chartjs/Chart.min.js"></script>
<!-- Data Range Picker -->
<script src="/assets/adminlte/plugins/daterangepicker/moment.min.js"></script>
<script src="/assets/adminlte/plugins/daterangepicker/daterangepicker.js"></script>

@yield('script')

<style>
    #not-links a{
        margin:15px !important;
    }
</style>

<script>
    $(function(){
        /*
        var notification = window.Notification || window.mozNotification || window.webkitNotification;

        // The user needs to allow this
        if ('undefined' == typeof notification)
        {
            alert('Web notification not supported');
        }
        else
        {
            notification.requestPermission(function(permission){});
        }

        //AJAX Post to get notifications.
        //So sales
        //Failed transactions
        //Abandoned carts

        jQuery.ajax({
            url: '/customer-service/notification-centre/get',
            type:"POST",
            success: function(response) {
                notificationPoster(response);
            }
        });

        jQuery.ajax({
            url: '/customer-service/notification-centre/getBox',
            type:"POST",
            success: function(response) {
                notificationBoxUpdater(response);
            }
        });

        window.setInterval(function() {
            jQuery.ajax({
                url: '/customer-service/notification-centre/get',
                type:"POST",
                success: function(response) {
                    console.log(response);
                    notificationPoster(response);
                }
            });
        }, 15000);

        window.setInterval(function() {
            jQuery.ajax({
                url: '/customer-service/notification-centre/getBox',
                type:"POST",
                success: function(response) {
                    notificationBoxUpdater(response);
                }
            });
        }, 15000);

        function notificationPoster(nots)
        {

            var insString = "";
            var i = 0;
            if(nots['orders'] && nots['orders']>0)
            {
                var options = {
                    body: "We have "+nots['orders']+" new order(s)",
                    dir: 'auto', // or ltr, rtl
                    lang: 'EN', //lang used within the notification.
                    tag: 'orderNotification', //An element ID to get/set the content
                    icon: '', //The URL of an image to be used as an icon
                    link:'/customer-service/orders'
                }


                Notify("Orders", options);
            }

            if(nots['failed'] && nots['failed']>0)
            {

                var options = {
                    body: "We have "+nots['failed']+" failed transaction(s)",
                    dir: 'auto', // or ltr, rtl
                    lang: 'EN', //lang used within the notification.
                    tag: 'failedNotification', //An element ID to get/set the content
                    icon: '', //The URL of an image to be used as an icon
                    link:'/customer-service/failed-transactions'
                }
                Notify("Failed transactions", options);
            }

            if(nots['abandoned'] && nots['abandoned']>0)
            {
                var options = {
                    body: "We have "+nots['abandoned']+" abandoned cart(s)",
                    dir: 'auto', // or ltr, rtl
                    lang: 'EN', //lang used within the notification.
                    tag: 'abandonedNotifications', //An element ID to get/set the content
                    icon: '', //The URL of an image to be used as an icon
                    link:'/customer-service/abandoned-carts'
                }
                Notify("Abandoned Carts", options);
            }
        }

        function Notify(titleText, options)
        {
            if ('undefined' === typeof notification)
                return false;       //Not supported....
            var noty = new notification(
                    titleText, options
            );
            noty.onclick = function () {
                console.log('notification.Click');
                window.open(options.link, '_blank');
            };
            noty.onerror = function () {
                console.log('notification.Error');
            };
            noty.onshow = function () {
                console.log('notification.Show');
            };
            noty.onclose = function () {
                console.log('notification.Close');
            };
            return true;
        }

        function notificationBoxUpdater(nots)
        {
            console.log("nbox");
            console.log(nots);
            console.log("endnbox");

            var count = nots['orders'] + nots['failed'] + nots['abandoned'];

            insString = "";

            insString += '<a href="/customer-service/orders">'+nots['orders']+' new orders</a><br>';
            insString += '<a href="/customer-service/failed-transactions">'+nots['failed']+' failed transactions</a><br>';
            insString += '<a href="/customer-service/abandoned-carts">'+nots['abandoned']+' abandoned carts</a><br>';

            $('#not-links').html(insString);
            $('#not-alert').html(count);
        }

        */

        $('body').on('click', '#signout-btn', function(e){
            e.preventDefault();
            $.ajax({
                url: "/customer-service/logout",
                type: 'POST',
                success:function(response){
                    window.location='/customer-service/login';
                },
                error:function(response){
                    alert("fail");
                }
            })
        });
    });
</script>

<script>$.ajaxSetup({headers:{'csrftoken':'{{csrf_token()}}'}});</script>
</body>
</html>