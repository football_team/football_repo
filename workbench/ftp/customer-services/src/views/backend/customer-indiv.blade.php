@extends(Template::name('CustomerServices::backend._layout.layout'))

@section('title')
    Customer Services - Customer
@stop

@section('page_title')
    Customer Services
@stop

@section('page_sdesc')
    {{$user->firstname}} {{$user->lastname}}
@stop

@section('page_content')
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" id="modalTitle"></h4>
                </div>
                <div class="modal-body" id="fill-this">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
    <div class="row">
        <div class="col-sm-8">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">
                                Advanced Search
                            </h3>
                        </div>
                        <div class="box-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <input type="text" class="form-control pull-right" value="{{$dateRange}}" id="reservation">
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-xs-12 filter-buttons">
                                    <button class="btn" data-id="sales">Sales</button><button class="btn" data-id="orders">Orders</button><button class="btn" data-id="listings">Listings</button><button class="btn" data-id="notes">Notes</button><button class="btn" data-id="emails">Emails</button><button class="btn" data-id="failed">Failed Transactions</button><button class="btn adv-sh" id="adv-sh"><span class="fa fa-search" ></span></button>
                                </div>
                            </div>
                            <div class="row" id="adv-search-row">
                                <div class="col-xs-12 adv-search-fields">
                                    <div class="row search-fields">
                                        <div class="col-xs-12">
                                            <div class="h4">Sales</div>
                                        </div>
                                        <div class="col-sm-6">
                                            Buyer First Name:<br> <input type="text" data-name="bfn">
                                        </div>
                                        <div class="col-sm-6">
                                            Buyer Second Name:<br> <input type="text" data-name="bln">
                                        </div>
                                        <div class="col-sm-6">
                                            Buyer Email:<br> <input type="text" data-name="bemail">
                                        </div>
                                        <div class="col-sm-6">
                                            Order Number:<br> <input type="text" data-name="sOn">
                                        </div>
                                    </div>
                                    <div class="row search-fields">
                                        <div class="col-xs-12">
                                            <div class="h4">Orders</div>
                                        </div>
                                        <div class="col-sm-6">
                                            Seller First Name:<br> <input type="text" data-name="sfn">
                                        </div>
                                        <div class="col-sm-6">
                                            Seller Second Name:<br> <input type="text" data-name="sln">
                                        </div>
                                        <div class="col-sm-6">
                                            Seller Email:<br> <input type="text" data-name="semail">
                                        </div>
                                        <div class="col-sm-6">
                                            Order Number:<br> <input type="text" data-name="bOn">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-xs-12" id="timeline-holder">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="col-sm-4">
            <div class="row">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">
                            Customer Data
                        </h3>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-xs-12">
                                <b>Name:</b> {{$user->firstname}} {{$user->lastname}}
                            </div>
                        </div>
                        @if($company && $company != "")
                            <div class="row">
                                <div class="col-xs-12">
                                    <b>Company:</b> {{$company}}
                                </div>
                            </div>
                        @endif
                        <div class="row">
                            <div class="col-xs-12">
                                <b>Email:</b> <a href="mailto:{{$user->email}}">{{$user->email}}</a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <b>Telephone:</b> <a href="tel:{{$user->telephone}}">{{$user->telephone}}</a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <b>Customer Type:</b>
                                <select name="ctype" id="ctype" autocomplete="off">
                                    <option value="customer" @if($user->seller_type == "customer") selected @endif>Customer</option>
                                    <option value="seller" @if($user->seller_type == "seller") selected @endif>Seller</option>
                                    <option value="broker" @if($user->seller_type == "broker") selected @endif>Broker</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">
                            Add user note
                        </h3>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-xs-10">
                                <input type="text" id="new-note" style="width:100%;">
                            </div>
                            <div class="col-xs-2">
                                <button id="add-new-note">Add</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @if(isset($shipping) && !empty($shipping))
            <div class="row">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">
                            Addresses - Shipping
                        </h3>
                    </div>
                    <div class="box-body">
                        @if(array_key_exists('s1', $shipping) && $shipping['s1'] != '')
                        <div class="row">
                            <div class="col-xs-12">
                                {{$shipping['s1']}}
                            </div>
                        </div>
                        @endif
                        @if(array_key_exists('s2', $shipping) && $shipping['s2'] != '')
                        <div class="row">
                            <div class="col-xs-12">
                                {{$shipping['s2']}}
                            </div>
                        </div>
                        @endif
                        @if(array_key_exists('city', $shipping) && $shipping['city'] != '')
                        <div class="row">
                            <div class="col-xs-12">
                                {{$shipping['city']}}
                            </div>
                        </div>
                        @endif
                        @if(array_key_exists('pc', $shipping) && $shipping['pc'] != '')
                        <div class="row">
                            <div class="col-xs-12">
                                {{$shipping['pc']}}
                            </div>
                        </div>
                        @endif
                        @if(array_key_exists('ct', $shipping) && $shipping['ct'] != '')
                        <div class="row">
                            <div class="col-xs-12">
                                {{$shipping['ct']}}
                            </div>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
            @endif
            @if(isset($billing) && !empty($billing))
            <div class="row">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">
                            Addresses - Billing
                        </h3>
                    </div>
                    <div class="box-body">
                        @if(array_key_exists('s1', $billing) && $billing['s1'] != '')
                        <div class="row">
                            <div class="col-xs-12">
                                {{$billing['s1']}}
                            </div>
                        </div>
                        @endif
                        @if(array_key_exists('s2', $billing) && $billing['s2'] != '')
                        <div class="row">
                            <div class="col-xs-12">
                                {{$billing['s2']}}
                            </div>
                        </div>
                        @endif
                        @if(array_key_exists('city', $billing) && $billing['city'] != '')
                        <div class="row">
                            <div class="col-xs-12">
                                {{$billing['city']}}
                            </div>
                        </div>
                        @endif
                        @if(array_key_exists('pc', $billing) && $billing['pc'] != '')
                        <div class="row">
                            <div class="col-xs-12">
                                {{$billing['pc']}}
                            </div>
                        </div>
                        @endif
                        @if(array_key_exists('ct', $billing) && $billing['ct'] != '')
                        <div class="row">
                            <div class="col-xs-12">
                                {{$billing['ct']}}
                            </div>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
            @endif
            @if($highestAll)
            <div class="row">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">
                            Favourite Team
                        </h3>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-xs-12">
                                {{$highestAll}}, having made {{$highestBy}} purchases for games featuring this team
                            </div>
                        </div>
                        @if($highestHome)
                        <div class="row">
                            <div class="col-xs-12">

                                Favourite Home Team: {{$highestHome}} with {{$highestHomeNum}} purchases
                            </div>
                        </div>
                        @endif
                        @if($highestAway)
                        <div class="row">
                            <div class="col-xs-12">
                                Favourite Away Team: {{$highestAway}} with {{$highestAwayNum}} purchases
                            </div>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
            @endif
        </div>
    </div>
@stop

@section('style')
    <style>
        .filter-buttons .btn{
            background-color:#777884 !important;
            color:#fff;
            margin:5px;
        }
        .filter-buttons .btn.active{
            background-color:#8897FF !important;
        }

        .search-fields input{
            width:100%;
        }

        .adv-sh.active{
            background-color:lightgreen;
        }

        #adv-search-row{
            height:0px;
            overflow:hidden;
        }

        #adv-search-row.active{
            height:initial;
        }
    </style>
@stop

@section('script')
    <script>
        $(function(){

            $('#ctype').change(function(){
              var type = $(this).val();
                $.ajax({
                   url:"/customer-service/customers/change-type/{{$user->user_id}}",
                    type:"POST",
                    data:{
                        seller_type:type
                    },
                    success:function(response){

                    },
                    error:function(response){
                        alert("An error occurred");
                    }
                });
            })


           $('#add-new-note').click(function(){
              var note = $('#new-note').val();
               if(note != "")
               {
                   $.ajax({
                       url:"/customer-service/customers/addNote/{{$user->user_id}}",
                       type:"POST",
                       data:{
                           note:note
                       },
                       success:function(response){
                           refreshFeed();
                       },
                       error:function(response){
                           alert(response.responseJSON);
                       }
                   })
               }
               else
               {
                   alert("Cannot add a blank note.");
               }
           });

            $.ajax({
                url:"/customer-service/customers/activity-feed/{{$user->user_id}}",
                type:"POST",
                data:{
                    range:"{{$dateRange}}",
                },
                success:function(response){
                    $('#timeline-holder').html(response);
                },
                error:function(response){

                }
            });

            $('#reservation').daterangepicker({
                format: 'DD/MM/YYYY'
            });

            $('#reservation').change(function(){
                refreshFeed();
            });

            $('body').on('click', '.deleteNote', function(){
                var id = $(this).data('id');
                if(window.confirm("Are you sure you want to delete this user note?"))
                {
                    $.ajax({
                       url:"/customer-service/customers/deleteNote/"+id,
                        type:"POST",
                        success:function(response){
                            refreshFeed();
                        },
                        error:function(response){

                        }
                    });
                }

            });

            $('body').on('click', '.editNote', function(){
                var id = $(this).data('id');
                $.ajax({
                   url:"/customer-service/customers/getNote/"+id,
                    type:"POST",
                    success:function(response){
                        $('#modalTitle').html("Edit User Note");
                        var note = response;
                        $('#fill-this').html('<textarea style="width:100%;" id = "note-edit" data-id="'+id+'">'+note+'</textarea><br><button class="btn btn-primary btn-xs" id="save-note">Save</button>');
                        $('#myModal').modal({"show":"true"});
                    },
                    error:function(response){

                    }
                });
            });

            $('body').on('click', '#save-note', function(){
               var id = $('#note-edit').data('id');
                var note = $('#note-edit').val();

                $.ajax({
                    url:"/customer-service/customers/editNote/"+id,
                    type:"POST",
                    data:{
                        note:note
                    },
                    success:function(response){
                        $('#myModal').modal('hide');
                        refreshFeed();
                    },
                    error:function(response){

                    }
                })
            });

            $('body').on('click', '.view-full-order', function(){
                var id = $(this).data('id')

                $.ajax({
                    url:"/customer-service/customers/getFullOrder/"+id,
                    type:"POST",
                    success:function(response){
                        $('#fill-this').html(response);
                        $('#myModal').modal('show');
                    },
                    error:function(response){

                    }
                })
            });

            $('.filter-buttons .btn').click(function(){
                $(this).toggleClass('active');

                if($(this).hasClass('adv-sh'))
                {
                    $('#adv-search-row').toggleClass('active');
                }
                refreshFeed();

            });

            $('.search-fields input').change(function(){
               refreshFeed();
            });

            $('body').on('click', '.view-order-notes',function(){
               var id = $(this).data('id');

                $.ajax({
                   url:"/customer-service/customer/view-order-notes/"+id,
                    type:"POST",
                    success:function(response){
                        $('#fill-this').html(response);
                        $('#myModal').modal('show');
                    },
                    error:function(response){
                        alert("Error");
                    }
                });
            });

            $('body').on('click', '.release-transaction', function(){
                var id = $(this).data('id');
                if(window.confirm("Are you sure you want to release this failed transaction for delivery?")) {
                    $.ajax({
                        url: "/customer-service/customers/release-failed/" + id,
                        type: "POST",
                        success: function (response) {
                            alert("Released Order");
                            refreshFeed();
                        },
                        error: function (response) {
                            alert("Error");
                        }
                    });
                }
            });

            $('body').on('click','.save-order-note-btn', function(){
                var id = $(this).data('id');
                var note = $('#edit-o-note-'+id).val();

                $.ajax({
                   url:'/customer-service/customers/update-order-note/'+id,
                    type:"POST",
                    data:{
                        note:note
                    },
                    success:function(){

                    },
                    error:function(){
                        alert("Error, order note not saved.");
                    }
                });
                //Ajax the edit
            });

            $('body').on('click','.delete-order-note', function(){
                var id = $(this).data('id');

                $.ajax({
                    url:"/customer-service/customers/delete-order-note/"+id,
                    type:"POST",
                    success:function(response){
                        $('#fill-this').html(response);
                        refreshFeed();
                    },
                    error:function(response){

                    }
                })
                //Ajax the delete
            });

            $('body').on('click', '#save-new-order-note', function(){
                var note = $('#new-order-note-note').val();
                var id = $(this).data('id');

                $.ajax({
                    url:"/customer-service/customers/save-new-order-note/"+id,
                    type:"POST",
                    data:{
                        note:note
                    },
                    success:function(response){
                        $('#fill-this').html(response);
                        refreshFeed();
                    },
                    error:function(response){

                    }
                })

               //Ajax the note
            });

            function refreshFeed(){
                var btns = $('.filter-buttons .btn.active').toArray();
                var athing = Array();
                var advSearch = Array();
                var advOrder = Array();

                for(var i = 0; i<btns.length;i++)
                {
                    athing.push($(btns[i]).data('id'));
                }

                if($('#adv-sh').hasClass('active'))
                {
                    //Has class active must gather input data
                    $('.search-fields input').each(function(){
                        var v = $(this).val();
                        var index = $(this).data('name');

                        if(!v == '')
                        {
                            advOrder.push(index);
                            advSearch.push(v);
                        }
                    });
                }
                else
                {
                    $('.search-fields input').val('');
                }
                $.ajax({
                    url:"/customer-service/customers/activity-feed/{{$user->user_id}}",
                    type:"POST",
                    data:{
                        range:$('#reservation').val(),
                        filt:athing,
                        adv:advSearch,
                        ord:advOrder
                    },
                    success:function(response){
                        $('#timeline-holder').html(response);
                    },
                    error:function(response){
                        alert("Error");
                    }
                });
            };
        });
    </script>
@stop
