<!doctype html>
<html lang="en">
<head>
    <link rel="stylesheet" type="text/css" href="/assets/adminlte/bootstrap/css/bootstrap.css">
    <meta charset="UTF-8">
    <title>Customer Service Portal</title>

    {{
        Assets::setStyles(
        [
            'foundation'         => 'css/foundation.min.css',
            'main'               => 'css/main.css?v=2',
            'jquery-ui'          => 'css/jquery-ui/jquery-ui.min.css',
            'fancybox'           => 'css/jquery.fancybox.css'
        ], false, true);
    }}
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    {{ Assets::dumpStyles(Config::get('bondcms.assets_debug')) }}
</head>
<body>
    <div class="contains">
        <div class="shade">
            <br><br><br>
            <div class="row midthis">
                <div class="col-md-3 col-sm-1"></div>
                <div class="col-md-6 col-sm-10" style="text-align:center;">
                    <h1 style="color:#fff; font-size:50px;">Customer Service Portal</h1>
                </div>
                <div class="col-md-3 col-sm-1"></div>
            </div>
            <br>
            <div class="action row">
                <form id="regForm" action="/affiliates/login" class="fields" method="POST">
                    <div class="row">
                        <div class="col-md-3 col-sm-1"></div>
                        <div class="col-md-6 col-sm-10">
                            <input type="text" name="email" placeholder="Email" required>
                        </div>
                        <div class="col-md-3 col-sm-1"></div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 col-sm-1"></div>
                        <div class="col-md-6 col-sm-10">
                            <input type="password" name="password" placeholder="Password" required>
                        </div>
                        <div class="col-md-3 col-sm-1"></div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 col-sm-1"></div>
                        <div class="col-md-6 col-sm-10">
                            <label for="tnc" style="color:#fff">Remember me</a></label>
                            <input type="checkbox" name="remember_me"><input type="submit" value="Login" style="float:right; height:25px;">
                        </div>
                        <div class="col-md-3 col-sm-1"></div>
                    </div>
                </form>
            </div>
            <br>
            <div class="row">
                <div class="col-md-3 col-sm-1"></div>
                <div class="col-md-6 col-sm-10">
                    <div id="errors" style="color:red;">

                    </div>
                </div>
                <div class="col-md-3 col-sm-1"></div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function(){
            $('#regForm').submit(function(e){
                e.preventDefault();
                var formData = $(this).serialize();

                $.ajax({
                    url:"/customer-service/login",
                    type:"POST",
                    data:formData,
                    success:function(response){
                        //Redirect to dashboard
                        window.location="/customer-service";
                    },
                    error:function(response){
                        $('#errors').html(response.responseText);
                    }
                });
            });
        })
    </script>

    <style>
        body,html{
            height:100vh;
        }
        body{
            background-image:url("/assets/images/customer_service.jpg");
            background-size:cover;
            background-repeat:no-repeat;
            padding-top:0px;
        }
        .shade{
            background-color:rgba(0,0,0,0.6);
            height:100vh;
        }
        .midthis{
            margin-top:20vh;
        }
        .row{
            margin-left:auto !important;
            margin-right:auto !important;
        }
        .fields{
            margin-left:auto !important;
            margin-right:auto !important;
            float: none !important;
        }
        input[type="text"], input[type="password"]{
            border-radius:3px !important;
        }
    </style>

    <script>
        $.ajaxSetup({ headers: { 'csrftoken' : '{{ csrf_token() }}' } });
    </script>
</body>
</html>