<?php

/**
 * Created by PhpStorm.
 * User: baglad
 * Date: 06/06/2016
 * Time: 14:52
 */
class CustomerServiceAuthController extends BaseControllerCS
{

    public function __construct()
    {
        $group = 0;
        try
        {
            $group = Sentry::findGroupByName('cservice');
        }
        catch(Cartalyst\Sentry\Groups\GroupNotFoundException $ex)
        {
            Log::info("cservice user group not found.");
        }
        catch(Exception $ex)
        {
            Log::info('Exception finding cservice user group.');
        }

        if (!$group)
        {
            try
            {
                $group = Sentry::createGroup(array(
                    'name' => 'cservice',
                    'permissions' => array(
                        'users' => 1
                    ),
                ));
            }
            catch(Cartalyst\Sentry\Groups\GroupExistsException $ex)
            {
                Log::info("Group already exists.");
            }
            catch(Exception $ex)
            {
                Log::info("Exception creating cservice user group.");
            }
        }
    }

    public function login()
    {
        return View::make('CustomerServices::frontend.login');
    }

    public function postLogin()
    {
        if(Input::has('email') && Input::has('password'))
        {
            $credentials = array(
                'email'    => Input::get('email'),
                'password' => Input::get('password')
            );

            $rememberMe = Input::get('rememberMe');

            try {
                if(!empty($rememberMe))
                {
                    $this->user = Sentry::authenticate($credentials, true);
                }
                else
                {
                    $this->user = Sentry::authenticate($credentials, false);
                }

                if ($this->user) {
                    $flag = 0;
                    $this->user->getGroups();
                    foreach($this->user->groups as $group)
                    {
                        if($group->name == 'cservice')
                        {
                            $flag = 1;
                            break;
                        }
                    }
                    if(!$flag)
                    {
                        return Response::json('This is not a customer service account.', 404);
                    }

                    return Response::json('success', 200);
                }
            }
            catch (\Exception $e) {
                return Response::json($e->getMessage(), 404);
            }
        }
        else
        {
            return Reponse::json("An email and password are required to login.", 404);
        }
    }

    public function auth()
    {
        if (!Sentry::check()) {
            return Redirect::route('cservices.login');
        }
        else
        {
            $user = Sentry::getUser();
            $user->getGroups();

            $flag = 0;
            foreach($user->groups as $group)
            {
                if($group->name == 'cservice')
                {
                    $flag = 1;
                    break;
                }
            }
            if(!$flag)
            {
                return Redirect::route('cservices.login');
            }
        }

    }

    public function logout()
    {
        Sentry::logout();
        return Response::json("Logged Out", 200);
    }
}