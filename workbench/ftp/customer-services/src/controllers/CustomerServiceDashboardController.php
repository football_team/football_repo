<?php

/**
 * Created by PhpStorm.
 * User: baglad
 * Date: 07/06/2016
 * Time: 11:04
 */
class CustomerServiceDashboardController extends BaseControllerCS
{
    public function index(){
        View::share('menu', 'dashboard');
        return View::make('CustomerServices::backend.index');
    }

    public function customerList()
    {
        $take = 10;
        $skip = 0;
        $search = "";

        if(Input::has('start'))
        {
            $skip = Input::get('start');
        }
        if(Input::has('length'))
        {
            $take = Input::get('length');
        }
        if(Input::has('search'))
        {
            $s = Input::get('search');
            $search = $s['value'];
        }

        $output = [];
        $total = 0;

        $tt = DB::table('web_user')
                ->select(DB::raw('count(*) as ttl'))
                ->first();

        $total = $tt->ttl;


        if($search != "")
        {
            $users = WebUser::where('firstname', 'LIKE', '%'.$search.'%')
                ->orWhere('lastname', 'LIKE', '%'.$search.'%')
                ->orWhere('email', 'LIKE', '%'.$search.'%')
                ->orWhere('telephone', 'LIKE', '%'.$search.'%')
                ->orWhere('id', 'LIKE', '%'.$search.'%')
                ->get();

        }
        else
        {
            $users = DB::table('web_user')->skip($skip)->take($take)->get();

        }
        foreach($users as $user)
        {
            $temp = [];

            $temp['0'] = $user->user_id;
            $temp['1'] = $user->firstname;
            $temp['2'] = $user->lastname;
            $temp['3'] = $user->email;
            $temp['4'] = $user->telephone;
            $temp['5'] = $user->seller_type;
            $temp['6'] = '<a href="/customer-service/customers/'.$user->user_id.'" class="view-user">View</a>';

            $output[] = $temp;
        }

        $results['data'] = $output;
        $results['recordsTotal'] = $total;
        $results['recordsFiltered'] = $total;
        $response = Response::make($results);
        $response->header('Content-Type', 'application/json');
        return $response;
    }

    public function showCustomer($id)
    {
        $user = WebUser::where('user_id', $id)->first();
        View::share('menu', 'dashboard');
        View::share('user', $user);

        $api_path = Config::get('api.mage_soap_api_path');
        require_once("{$api_path}app/Mage.php");
        umask(0);
        Mage::app('default');

        Mage::getSingleton('core/session', array('name'=>'frontend'));
        $customer = Mage::getModel('customer/customer')->load($id);

        $avar=$customer->getPrimaryShippingAddress();

        $shipping = array();
        if($avar)
        {
            $shipping['s1'] = $avar->getStreet1();
            $shipping['s2'] = $avar->getStreet2();
            $shipping['city'] = $avar->getCity();
            $shipping['pc'] = $avar->getPostcode();
            $shipping['ct'] = $avar->getCountry();
        }

        View::share('shipping', $shipping);

        $avar=$customer->getPrimaryBillingAddress();

        $company = "";

        if($avar)
        {
            $billing = array();
            $billing['s1'] = $avar->getStreet1();
            $billing['s2'] = $avar->getStreet2();
            $billing['city'] = $avar->getCity();
            $billing['pc'] = $avar->getPostcode();
            $billing['ct'] = $avar->getCountry();
            $company = $avar->getCompany();
        }

        View::share('company', $company);
        View::share('billing',$billing);

        //Get the most popular teams from past sales

        $teams = DB::table('events_ticket_buy')
                    ->leftJoin('events_related_tickets', 'events_ticket_buy.product_id', '=' ,'events_related_tickets.product_id')
                    ->leftJoin('events', 'events_related_tickets.event_id', '=', 'events.id')
                    ->leftJoin('football_ticket as home', 'events.home_team_id', '=', 'home.id')
                    ->leftJoin('football_ticket as away', 'events.away_team_id', '=', 'away.id')
                    ->where('events_ticket_buy.buyer_id', $id)
                    ->select('home.title as home', 'away.title as away')
                    ->get();

        $teamNames = array();
        $homeTeams = array();
        $awayTeams = array();

        foreach($teams as $team)
        {
            $teamNames[] = $team->home;
            $teamNames[] = $team->away;

            $homeTeams[] = $team->home;
            $awayTeams[] = $team->away;
        }

        $counted = array_count_values($teamNames);
        $homeCounted = array_count_values($homeTeams);
        $awayCounted = array_count_values($awayTeams);

        $highestAll = array_keys($counted, max($counted));
        $highestNum = max($counted);

        $highestHome = array_keys($homeCounted, max($homeCounted));
        $highestHomeNum = max($homeCounted);
        $highestAway = array_keys($awayCounted, max($awayCounted));
        $highestAwayNum = max($awayCounted);



        View::share('highestAll', $highestAll[0]);
        View::share('highestBy', $highestNum);

        View::share('highestHome', $highestHome[0]);
        View::share('highestHomeNum', $highestHomeNum);

        View::share('highestAway', $highestAway[0]);
        View::share('highestAwayNum', $highestAwayNum);

        $dNowObj = new DateTime();
        $dThenObj = new DateTime();
        $dThenObj = $dThenObj->modify('-1 week');
        $dNowObj = $dNowObj->modify('+1 day');

        $dString = $dThenObj->format('d/m/Y')." - ".$dNowObj->format('d/m/Y');
        View::share('dateRange', $dString);

        return View::make('CustomerServices::backend.customer-indiv');
    }

    public function getTimeline($id)
    {
        $user = WebUser::where('user_id', $id)->first();

        if($user)
        {
            //I need to select all of these things according to a date range
            $input = Input::all();

            $filter = ['sales', 'orders','listings','notes','emails','failed'];

            if(!Input::has('range'))
            {
                $adt = new DateTime();
                $range = "01/01/1990 - ".$adt->format('d/m/Y');

            }
            else
            {
                $range = Input::get('range');
                if($range == '')
                {
                    $adt = new DateTime();
                    $range = "01/01/1990 - ".$adt->format('d/m/Y');
                }
            }

            if(Input::has('filt'))
            {
                $filter = Input::get('filt');
            }

            $advSearch = array();

            if(Input::has('adv')&&Input::has('ord'))
            {
                $strs = Input::get('adv');
                $ord = Input::get('ord');
                if(sizeof($strs) == sizeof($ord))
                {
                    foreach($strs as $i=>$str)
                    {
                        $advSearch[$ord[$i]] = $str;
                    }
                }
            }

            $dArray = explode('-', $range);

            if(count($dArray)!=2)
            {
                return Response::json("Invalid date range.", 404);
            }

            $from = trim($dArray[0]);
            $to = trim($dArray[1]);

            $fromDObj = \DateTime::createFromFormat('d/m/Y', $from);
            $toDObj = \DateTime::createFromFormat('d/m/Y', $to);

            if((!$fromDObj) && (!$toDObj))
            {
                return Response::json("Invalid dates entered.",404);
            }

            $notes = array();
            $emails = array();
            $sales = array();
            $purchases = array();
            $listings = array();
            $failed = array();

            if(in_array('notes', $filter))
            {
                $notes = DB::table('web_user_notes')
                    ->leftJoin('users', 'web_user_notes.created_by', '=', 'users.id')
                    ->where('web_user_notes.user_id', $id)
                    ->whereBetween('web_user_notes.created_at', [$fromDObj->format('Y-m-d H:i:s'), $toDObj->format('Y-m-d H:i:s')])
                    ->select('web_user_notes.id as id','web_user_notes.note as note', 'web_user_notes.created_at as created_at', 'web_user_notes.updated_at as updated_at',
                        'users.first_name as first_name', 'users.last_name as last_name', DB::raw('"user_note" as type'))
                    ->get();
            }

            if(in_array('emails', $filter))
            {
                $emails = DB::table('email_log')
                            ->where('user_id', $id)
                            ->whereBetween('created_at', [$fromDObj->format('Y-m-d H:i:s'), $toDObj->format('Y-m-d H:i:s')])
                            ->select('id','address', 'subject', 'body', 'created_at', DB::raw('"email" as type'))
                            ->get();
            }

            if(in_array('sales', $filter)) {
                $sales = DB::table('events_ticket_buy')
                    ->leftJoin('events_related_tickets', 'events_ticket_buy.product_id', '=', 'events_related_tickets.product_id')
                    ->leftJoin('events', 'events_related_tickets.event_id', '=', 'events.id')
                    ->leftJoin('web_user', 'events_ticket_buy.buyer_id', '=', 'web_user.user_id')
                    ->where('events_related_tickets.user_id', $id);

                if(array_key_exists('bfn', $advSearch)) {
                    $sales = $sales->where('web_user.firstname', 'like', $advSearch['bfn'] . '%');
                }

                if(array_key_exists('bln', $advSearch))
                {
                    $sales = $sales->where('web_user.lastname', 'like', $advSearch['bln'].'%');
                }

                if(array_key_exists('bemail', $advSearch))
                {
                    $sales = $sales->where('web_user.email', 'like', $advSearch['bemail'].'%');
                }

                if(array_key_exists('sOn', $advSearch))
                {
                    $sales = $sales->where('events_ticket_buy.order_id', 'like', $advSearch['sOn'].'%');
                }

                $sales = $sales->whereBetween('events_ticket_buy.created_at', [$fromDObj->format('Y-m-d H:i:s'), $toDObj->format('Y-m-d H:i:s')])
                    ->select('events.title as title', 'events.datetime as event_date', 'events_ticket_buy.qty as qty', 'events_ticket_buy.amount as amount',
                        'web_user.firstname as buyer_firstname', 'web_user.lastname as buyer_lastname', 'events_ticket_buy.created_at as created_at',
                        'events_ticket_buy.order_id as order_id', DB::raw('"sale" as type'))
                    ->get();
            }

            if(in_array('orders', $filter)) {
                $purchases = DB::table('events_ticket_buy')
                    ->leftJoin('events_related_tickets', 'events_ticket_buy.product_id', '=', 'events_related_tickets.product_id')
                    ->leftJoin('events', 'events_related_tickets.event_id', '=', 'events.id')
                    ->leftJoin('web_user', 'events_related_tickets.user_id', '=', 'web_user.user_id')
                    ->where('events_ticket_buy.buyer_id', $id);

                if(array_key_exists('sfn', $advSearch)) {
                    $purchases = $purchases->where('web_user.firstname', 'like', $advSearch['sfn'] . '%');
                }

                if(array_key_exists('sln', $advSearch))
                {
                    $purchases = $purchases->where('web_user.lastname', 'like', $advSearch['sln'].'%');
                }

                if(array_key_exists('semail', $advSearch))
                {
                    $purchases = $purchases->where('web_user.email', 'like', $advSearch['semail'].'%');
                }

                if(array_key_exists('bOn', $advSearch))
                {
                    $purchases = $purchases->where('events_ticket_buy.order_id', 'like', $advSearch['bOn'].'%');
                }

                $purchases = $purchases->whereBetween('events_ticket_buy.created_at', [$fromDObj->format('Y-m-d H:i:s'), $toDObj->format('Y-m-d H:i:s')])
                    ->select('events.title as title', 'events.datetime as event_date', 'events_ticket_buy.qty as qty', 'events_ticket_buy.amount as amount',
                        'events_ticket_buy.delivery_status as status', 'events_ticket_buy.created_at as created_at', 'events_ticket_buy.order_id as order_id',
                        'web_user.firstname as firstname', 'web_user.lastname as lastname', DB::raw('"purchase" as type'))
                    ->get();
            }

            if(in_array('listings', $filter)) {
                $listings = DB::table('events_related_tickets')
                    ->leftJoin('events', 'events_related_tickets.event_id', '=', 'events.id')
                    ->where('events_related_tickets.user_id', $id)
                    ->whereBetween('events_related_tickets.created_at', [$fromDObj->format('Y-m-d H:i:s'), $toDObj->format('Y-m-d H:i:s')])
                    ->select('events.title as title', 'events.datetime as event_date', 'events_related_tickets.price as price', 'events_related_tickets.available_qty as qty',
                        'events_related_tickets.created_at as created_at', DB::raw('"listing" as type'))
                    ->get();
            }

            if(in_array('failed', $filter)) {
                $failed = DB::table('failed_transactions')
                    ->leftJoin('follow_up_carts', 'failed_transactions.tid', '=', 'follow_up_carts.cid')
                    ->leftJoin('events_related_tickets', 'follow_up_carts.pid', '=', 'events_related_tickets.product_id')
                    ->leftJoin('web_user', 'follow_up_carts.email', '=', 'web_user.email')
                    ->leftJoin('events', 'events_related_tickets.event_id', '=', 'events.id')
                    ->where('web_user.user_id', $id)
                    ->whereBetween('follow_up_carts.created_at', [$fromDObj->format('Y-m-d H:i:s'), $toDObj->format('Y-m-d H:i:s')])
                    ->select('events.title as title', 'failed_transactions.tid as tid' ,'events.datetime as event_date', 'events_related_tickets.price as price', 'follow_up_carts.created_at as created_at',
                        DB::raw('"failed_transaction" as type'))
                    ->get();
            }

            foreach($sales as $sale)
            {
                $oId = $sale->order_id;
                $sale_notes = DB::table('order_notes')
                            ->where('order_id', $oId)
                            ->get();

                if(!empty($sale_notes))
                {
                    if(sizeof($sale_notes > 1))
                    {
                        usort($sale_notes, array($this, "strToDateCompare"));
                    }
                    $sale->notes = $sale_notes;
                }
            }

            foreach($purchases as $purchase)
            {
                $oId = $purchase->order_id;
                $purchase_notes = DB::table('order_notes')
                    ->where('order_id', $oId)
                    ->get();

                if(!empty($purchase_notes))
                {
                    if(sizeof($purchase_notes)>1)
                    {
                        usort($purchase_notes, array($this, "strToDateCompare"));
                    }
                    $purchase->notes = $purchase_notes;
                }
            }

            $merged = array_merge($notes, $emails, $sales, $purchases, $listings, $failed);
            usort($merged, array($this, "strToDateCompare"));

            //All sorted, now pass to a blade for visual processing
            View::share('tla', $merged);


            return View::make('CustomerServices::backend.partials.timeline');
        }

        return Response::json("Fail", 404);
    }

    public function strToDateCompare($a, $b)
    {
        $atime = DateTime::createFromFormat('Y-m-d H:i:s', $a->created_at);
        $btime = DateTime::createFromFormat('Y-m-d H:i:s', $b->created_at);

        if($atime < $btime)
        {
            return 1;
        }
        else if($atime > $btime)
        {
            return -1;
        }
        else{
            return 0;
        }
    }

    public function addNote($id)
    {
        $thisUser = Sentry::getUser();
        if(!$thisUser)
        {
            return Response::json("Invalid CS User", 404);
        }

        $user = WebUser::where('user_id', $id)->first();

        $now = new DateTime();

        if($user)
        {
            $input = Input::all();
            if(array_key_exists('note', $input) && $input['note']!='')
            {
                DB::table('web_user_notes')
                        ->insert([
                            'user_id'=>$id,
                            'note'=>$input['note'],
                            'created_at'=>$now->format('Y-m-d H:i:s'),
                            'updated_at'=>$now->format('Y-m-d H:i:s'),
                            'created_by'=>$thisUser->id
                        ]);

                return Response::json('success', 200);
            }
        }
        else
        {
            return Response::json("User not found", 404);
        }
    }

    public function changeWebUserType($id)
    {
        $user = WebUser::where('user_id', $id)->first();
        if(!empty($user))
        {
            if(Input::has('seller_type'))
            {
                $sType = Input::get('seller_type');

                if(in_array($sType, array('customer','seller', 'broker')))
                {
                    $user->seller_type = $sType;
                    $user->save();

                    return Response::json("Success", 200);
                }
                else
                {
                    return Response::json("Incorrect seller type added", 404);
                }
            }
            else
            {
                return Response::json("Must provide a user type", 404);
            }
        }
        return Response::json("Web User not found.",404);
    }


    public function deleteNote($id)
    {
        $note = DB::table('web_user_notes')
            ->where('id', $id)
            ->first();


        if($note)
        {
            DB::table('web_user_notes')
                ->where('id', $id)
                ->delete();

            return Response::json("Success",200);
        }
        else
        {
            return Response::json("Note not found", 404);
        }
    }

    public function editNote($id)
    {
        $note = DB::table('web_user_notes')
            ->where('id', $id)
            ->first();

        if($note)
        {
            if(Input::has('note'))
            {
                $dt = new DateTime();
                $note = Input::get('note');
                DB::table('web_user_notes')
                    ->where('id', $id)
                    ->update(['note'=>$note, 'updated_at'=>$dt->format('Y-m-d H:i:s')]);

                return Response::json("Success", 200);
            }
            else
            {
                return Response::json("Must provide updated note",404);
            }
        }
    }

    public function getNote($id){
        $note = DB::table('web_user_notes')
                ->where('id', $id)
                ->first();

        if($note)
        {
            return Response::json($note->note,200);
        }
        else
        {
            return Response::json("Note not found",404);
        }
    }

    public function getFullEmail($id)
    {
        $email = DB::table('email_log')
                    ->where('id', $id)
                    ->first();

        if($email)
        {
            return $email->body;
        }
    }

    public function getFullOrder($order_id)
    {
        //So buyer details, seller details, event details, ticket details
        $order = FootballTicketBuy::where('order_id', $order_id)->first();
        if(!empty($order))
        {
            $rt = RelatedTicket::where('product_id', $order->product_id)->first();
            if(!empty($rt))
            {
                $seller = WebUser::where('user_id', $rt->user_id)->first();
                $buyer = WebUser::where('user_id', $order->buyer_id)->first();

                if(!empty($seller) && !empty($buyer))
                {
                    //This is all of it

                    $event = FootBallEvent::where('id', $rt->event_id)->first();
                    if(!empty($event))
                    {
                        $ticket = json_decode($rt->ticket);
                        $ttype = $ticket->ticketInformation->ticket_type;

                        $tres = DB::table('events_ticket_type')
                                    ->where('id', $ttype)
                                    ->first();

                        $ttype = $tres->title;

                        $ticketRes = $ticket->ticketInformation->restrictions;

                        $ret = array();
                        $ret['order'] = $order;
                        $ret['event'] = $event;
                        $ret['ticket'] = $rt;
                        $ret['seller'] = $seller;
                        $ret['buyer'] = $buyer;

                        $ret['misc'] = array();
                        $ret['misc']['ttype'] = $ttype;
                        $ret['misc']['block'] = $ticket->ticketInformation->loc_block;
                        $ret['misc']['row'] = $ticket->ticketInformation->loc_row;
                        $ret['misc']['restrictions'] = $ticketRes;


                        //So, make a view with tables to return to the modal in the blade
                        View::share('data', $ret);
                        return View::make('CustomerServices::backend.partials.full-order');
                    }
                }
            }
        }

        return Response::json('could not find full order details',404);
    }

    public function makeFailedTransaction($fid)
    {
        $f = DB::table('failed_transactions')->where('tid', $fid)->first();
        if(!empty($f))
        {
            $transaction = Transaction::where('tid', $f->tid)->first();
            if(!empty($transaction))
            {
                $paymentInfo = json_decode($transaction->order, true);
                $shoppingCartId = $paymentInfo['shoppingCartId'];
                $orderInfo      = $paymentInfo['order'];

                $transaction->status = 1;
                $transaction->save();

                FootBallEvent::updateMagentoDescription($orderInfo['product_id']);
                $orderNumber = TicketSoap::process("cart.order", array($shoppingCartId, null, null));
                $orderInfo['order_id'] = $orderNumber;

                $buyModel = new FootballTicketBuy();
                $order = TicketSoap::process('sales_order.info', $orderNumber);
                $orderInfo['amount'] = $order['grand_total'];
                $orderInfo['payment_status'] = 'Pending';

                $orderInfo['listed_amount'] = isset($order['items'][0]['price']) ? $order['items'][0]['price'] : $order['base_subtotal'] / $order['total_qty_ordered'];
                $buyModel->fill($orderInfo);
                $buyModel->save();

                $transaction->order = json_encode($paymentInfo);
                $transaction->save();

                RelatedTicket::updateTicket($orderInfo['product_id'], $orderInfo['qty']);
                $this->sendEmailConfirmationToSeller($orderNumber);

                DB::table('failed_transactions')->where('tid', $fid)->delete();

                return Response::json("Success", 200);
            }
        }
        return Response::json("Fail",404);
    }

    private function sendEmailConfirmationToSeller($orderId = '')
    {
        try {
            FootballTicketBuy::createSaleView();

            $orderDetails = DB::table('sales')->where('order_id', '=', $orderId)->first();

            $event = FootBallEvent::find($orderDetails->event_id);
            $eventTicket = RelatedTicket::where('product_id', '=', $orderDetails->product_id)->first();
            $ticket = RelatedTicket::getTicketByTicketId($orderDetails->product_id);
            $order = TicketSoap::process('sales_order.info', $orderId);


            $data = array();
            $data['game'] = $event->title . " - " . date('l, d F Y, h:ia', strtotime($event->datetime)) . ", " . $event->event_location;
            $data['event_id'] = $event->id;
            $data['order_id'] = $orderId;
            $data['total_amount'] = $eventTicket->price * $orderDetails->qty;
            $data['commission'] = ($data['total_amount'] * (empty($eventTicket->selling_commission_percentage) ? 15 : (int)$eventTicket->selling_commission_percentage) / 100);
            $data['total_amount_after_commission'] = $data['total_amount'] - $data['commission'];
            $data['seller_name'] = $orderDetails->seller_firstname . " " . $orderDetails->seller_lastname;
            $data['qty'] = $orderDetails->qty;

            $ticketInfo = json_decode($eventTicket->ticket, true);

            $data['formOfTicket'] = @$ticket->formOfTicket['title'];
            $data['location'] = @$ticket->ticketType['title'];
            $temp = array();
            if (isset($ticketInfo['ticketInformation']['loc_block']) && !empty($ticketInfo['ticketInformation']['loc_block'])) {
                $temp[] = 'Block: ' . $ticketInfo['ticketInformation']['loc_block'];
            }

            if (isset($ticketInfo['ticketInformation']['loc_row']) && !empty($ticketInfo['ticketInformation']['loc_row'])) {
                $temp[] = 'Row: ' . $ticketInfo['ticketInformation']['loc_row'];
            }

            $data['location'] = $data['location'] . ', ' . implode(', ', $temp);

            $temp = array();

            $defaultRestriction = [];
            foreach ($event->getSelectedRestrictions() as $val) {

                $defaultRestriction[] = array(
                    'id' => $val->id,
                    'title' => $val->title
                );
            }

            foreach ($defaultRestriction as $r):
                if (in_array($r['id'], $ticket->info['restrictions'])):
                    $temp[] = $r['title'];
                endif;
            endforeach;

            if ($ticket->buyerNot != ''):
                $temp[] = '<br /> Note: ' . $ticket->buyerNot;
            endif;

            $data['restrictions'] = implode(', ', $temp);

            $sm = $order['shipping_method'];
            $rateName = explode('_', $sm);
            $rateName = $rateName[0];
            $ships = DB::table('shipping_method_assoc')
                ->where('name', '=', $rateName)
                ->first();
            $data['shipping_method']=$ships->description;

            Mail::send('footballticket::email.sold-ticket-confirmation-to-seller', $data, function ($message) use ($orderDetails, $orderId) {
                $from = Config::get('mail.from');
                $from['address'] = trans('homepage.contact-email');
                $message->from($from['address'], $from['name']);
                $email = Options::getOption('contact_email') == '' ? 'hello@bondmedia.co.uk' : Options::getOption('contact_email');

                $message->to($orderDetails->seller_email, $orderDetails->seller_firstname . " " . $orderDetails->seller_lastname)
                    ->bcc($email, 'Football Ticket Pad')
                    ->subject("Confirmation of ticket sale: New Order # {$orderId}");
            });


        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function addOrderNote($oId)
    {
        //fuck off >.>
        if(Input::has('note'))
        {
            $input = Input::all();
            if($input['note']!='')
            {
                $dt = new DateTime();
                $user = Sentry::getUser();


                $note = DB::table('order_notes')
                    ->insert([
                        'order_id'=>$oId,
                        'note'=>$input['note'],
                        'created_at'=>$dt->format('Y-m-d H:i:s'),
                        'updated_at'=>$dt->format('Y-m-d H:i:s'),
                        'created_by'=>$user->id
                    ]);

                return $this->getOrderNotes($oId);
            }
        }
        return Response::json("Error",404);
    }

    public function saveOrderNote($id)
    {
        if(Input::has('note'))
        {
            $input = Input::all();
            if($input['note']!='')
            {
                $note = DB::table('order_notes')
                        ->where('id', $id)
                        ->first();

                if($note)
                {
                    DB::table('order_notes')
                        ->where('id', $id)
                        ->update(['note'=>$input['note']]);

                    return Response::json("Success",200);
                }
            }
        }

        return Response::json('error',404);
    }

    public function deleteOrderNote($id)
    {
        $oId = DB::table('order_notes')
                ->where('id', $id)
                ->first();

        if(!empty($oId))
        {
            $oId = $oId->order_id;

            DB::table('order_notes')
                ->where('id', $id)
                ->delete();

            return $this->getOrderNotes($oId);
        }
        return Response::json("Error",404);

    }

    public function getOrderNotes($id)
    {
        $notes = DB::table('order_notes')
                    ->where('order_id', $id)
                    ->leftJoin('users', 'order_notes.created_by', '=' ,'users.id')
                    ->select('order_notes.*','users.first_name', 'users.last_name')
                    ->get();


        View::share('oId', $id);
        View::share('notes',$notes);
        return View::make('CustomerServices::backend.partials.order-notes');

    }
}