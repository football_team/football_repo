<?php

/**
 * Created by PhpStorm.
 * User: baglad
 * Date: 22/06/2016
 * Time: 10:43
 */
class CustomerServiceFailedController extends BaseControllerCS
{
    public function index()
    {
        View::share('menu', 'failed-transactions');

        $dNowObj = new DateTime();
        $dThenObj = new DateTime();
        $dThenObj = $dThenObj->modify('-1 week');
        $dNowObj = $dNowObj->modify('+1 day');

        $dString = $dThenObj->format('d/m/Y')." - ".$dNowObj->format('d/m/Y');
        View::share('dateRange', $dString);

        $user = Sentry::getUser();

        $abandoned = DB::table('user_notifications')
            ->whereIn('notification_id', function($q)
            {
                $q->select('id')
                    ->where('type', "failed")
                    ->distinct()
                    ->from('notifications');
            })
            ->where('user_id', $user->id)
            ->update(['notified'=>1]);

        return View::make('CustomerServices::backend.failed');
    }


    public function getTimeline()
    {
        $input = Input::all();

        if(!Input::has('range'))
        {
            $adt = new DateTime();
            $range = "01/01/1990 - ".$adt->format('d/m/Y');

        }
        else
        {
            $range = Input::get('range');
            if($range == '')
            {
                $adt = new DateTime();
                $range = "01/01/1990 - ".$adt->format('d/m/Y');
            }
        }


        $dArray = explode('-', $range);

        if(count($dArray)!=2)
        {
            return Response::json("Invalid date range.", 404);
        }

        $from = trim($dArray[0]);
        $to = trim($dArray[1]);

        $fromDObj = \DateTime::createFromFormat('d/m/Y', $from);
        $toDObj = \DateTime::createFromFormat('d/m/Y', $to);

        if((!$fromDObj) && (!$toDObj))
        {
            return Response::json("Invalid dates entered.",404);
        }

        $advSearch = array();

        if(Input::has('adv')&&Input::has('ord'))
        {
            $strs = Input::get('adv');
            $ord = Input::get('ord');
            if(sizeof($strs) == sizeof($ord))
            {
                foreach($strs as $i=>$str)
                {
                    $advSearch[$ord[$i]] = $str;
                }
            }
        }

        $failed = DB::table('failed_transactions')
            ->leftJoin('follow_up_carts', 'failed_transactions.tid', '=', 'follow_up_carts.cid')
            ->leftJoin('events_related_tickets', 'follow_up_carts.pid', '=', 'events_related_tickets.product_id')
            ->leftJoin('web_user as buyer', 'follow_up_carts.email', '=', 'buyer.email')
            ->leftJoin('events', 'events_related_tickets.event_id', '=', 'events.id')
            ->leftJoin('web_user as seller', 'events_related_tickets.user_id', '=', 'seller.user_id')
            ->orderBy('follow_up_carts.created_at', 'DESC')
            ->whereBetween('follow_up_carts.created_at', [$fromDObj->format('Y-m-d H:i:s'), $toDObj->format('Y-m-d H:i:s')]);


        if(array_key_exists('bfn', $advSearch)) {
            $failed = $failed->where('buyer.firstname', 'like', $advSearch['bfn'] . '%');
        }

        if(array_key_exists('bln', $advSearch))
        {
            $failed = $failed->where('buyer.lastname', 'like', $advSearch['bln'].'%');
        }

        if(array_key_exists('bemail', $advSearch))
        {
            $failed = $failed->where('buyer.email', 'like', $advSearch['bemail'].'%');
        }

        if(array_key_exists('sfn', $advSearch)) {
            $failed = $failed->where('seller.firstname', 'like', $advSearch['sfn'] . '%');
        }

        if(array_key_exists('sln', $advSearch))
        {
            $failed = $failed->where('seller.lastname', 'like', $advSearch['sln'].'%');
        }

        if(array_key_exists('semail', $advSearch))
        {
            $failed = $failed->where('seller.email', 'like', $advSearch['semail'].'%');
        }

        $failed = $failed->select('buyer.firstname','buyer.lastname', 'buyer.user_id as buyer_id', 'seller.user_id as seller_id' ,'seller.firstname as sfirstname','seller.lastname as slastname','events.title as title', 'failed_transactions.tid as tid' ,'events.datetime as event_date', 'events_related_tickets.price as price', 'follow_up_carts.created_at as created_at')
            ->get();



        View::share('failed', $failed);

        return View::make('CustomerServices::backend.partials.failed-timeline');
    }
}