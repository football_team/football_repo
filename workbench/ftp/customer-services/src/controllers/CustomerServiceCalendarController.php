<?php

/**
 * Created by PhpStorm.
 * User: baglad
 * Date: 23/06/2016
 * Time: 10:43
 */
class CustomerServiceCalendarController extends BaseControllerCS
{
    public function index()
    {
        $user = Sentry::getUser();

        View::share('menu', 'calendar');
        $dt = new DateTime();
        $dt = $dt->format('Y-m-d');
        View::share('today', $dt);


        $cusr = DB::table('calendar_users')
                    ->where('user_id', $user->id)
                    ->first();

        if(!empty($cusr))
        {
            View::share('user_colour', $cusr->colour);
        }
        else
        {
            $colour = '#'.str_pad(dechex(mt_rand(0, 0xDDDDDD)), 6, '0', STR_PAD_LEFT);
            DB::table('calendar_users')
                    ->insert([
                        'user_id'=>$user->id,
                        'colour'=> $colour
                    ]);

            View::share('user_colour', $colour);
        }

        return View::make('CustomerServices::backend.calendar');
    }

    public function getEvents()
    {
        $input = Input::all();

        if((array_key_exists('start',$input))&&(array_key_exists('end', $input)))
        {
            if(($input['start']!= '')&&($input['end']!=''))
            {
                $start = DateTime::createFromFormat('Y-m-d', $input['start']);
                $end = DateTime::createFromFormat('Y-m-d', $input['end']);

                if($start && $end)
                {
                    $res = DB::table('calendar')
                            ->leftJoin('calendar_users', 'calendar.user_id', '=' ,'calendar_users.user_id')
                            ->whereBetween('calendar.start', array($start->format('Y-m-d H:i:s'), $end->format('Y-m-d H:i:s')))
                            ->orWhereBetween('calendar.end', array($start->format('Y-m-d H:i:s'), $end->format('Y-m-d H:i:s')))
                            ->select('calendar.id','calendar.start', 'calendar.end', 'calendar.title', 'calendar_users.colour as color')
                            ->get();

                    return Response::json($res,200);
                }
            }
        }
        return Response::json('Error', 404);
    }

    public function addEvent()
    {
        $input = Input::all();
        $user = Sentry::getUser();

        if((array_key_exists('title', $input)) && (array_key_exists('start', $input)) && (array_key_exists('end', $input)))
        {
            $title = $input['title'];
            $start = $input['start'];
            $end = $input['end'];

            $start = DateTime::createFromFormat('Y-m-d H:i:s', $start);
            $end = DateTime::createFromFormat('Y-m-d H:i:s', $end);


            if($start && $end)
            {
                $start->modify('-1 hour - 6 minutes');
                $end->modify('-1 hour - 6 minutes');
                $id = DB::table('calendar')
                    ->insertGetId([
                        'user_id'=>$user->id,
                        'title'=>$title,
                        'start'=>$start->format('Y-m-d H:i:s'),
                        'end' => $end->format('Y-m-d H:i:s')
                    ]);

                return Response::json($id, 200);
            }


        }
        return Response::json('Error', 404);
    }

    public function deleteEvent()
    {
        if(Input::has('id'))
        {
            $id = Input::get('id');

            if($id != '')
            {
                DB::table('calendar')
                        ->where('id', $id)
                        ->delete();

                return Response::json("Success", 200);
            }
        }
        return Response::json('fail', 404);
    }

    public function moveEvent()
    {
        $input = Input::all();

        if((array_key_exists('id', $input))&&(array_key_exists('start', $input))&&(array_key_exists('end', $input)))
        {
            $start = DateTime::createFromFormat('Y-m-d H:i:s', $input['start']);
            $end = DateTime::createFromFormat('Y-m-d H:i:s', $input['end']);

            if($start&&$end)
            {
                $start->modify('-1 hour -6 minutes');
                $end->modify('-1 hour -6 minutes');
                DB::table('calendar')
                    ->where('id', $input['id'])
                    ->update([
                        'start'=>$start->format('Y-m-d H:i:s'),
                        'end'=>$end->format('Y-m-d H:i:s')
                    ]);

                return Response::json("Success",200);
            }

            return Response::json("Fail", 404);
        }
    }

    public function updateTitle(){
        $input = Input::all();

        if((array_key_exists('id', $input))&&(array_key_exists('title', $input)))
        {
            DB::table('calendar')
                    ->where('id', $input['id'])
                    ->update([
                        'title'=>$input['title']
                    ]);

            return Response::json("Success",200);
        }
        return Response::json('fail',404);
    }
}