<?php

/**
 * Created by PhpStorm.
 * User: baglad
 * Date: 21/06/2016
 * Time: 13:05
 * THIS IS ALL SHIT NEED TO REMAKE
 */
class CustomerServiceNotificationController extends BaseControllerCS
{

    public function firstRun()
    {

        DB::table('notifications')
                ->delete();

        DB::table('user_notifications')
                ->delete();

        $dt = new DateTime();

        $sales = DB::table('events_ticket_buy')
            ->get();

        $failed = DB::table('failed_transactions')
            ->get();

        $abandoned = DB::table('follow_up_carts')
            ->get();

        foreach($sales as $sale)
        {
            DB::table('notifications')
                    ->insert([
                        'type'=>'order',
                        'created_at'=>$dt->format('Y-m-d H:i:s'),
                        'rel_id'=>$sale->order_id
                    ]);

        }

        foreach($failed as $fail)
        {
            DB::table('notifications')
                ->insert([
                    'type'=>'failed',
                    'created_at'=>$dt->format('Y-m-d H:i:s'),
                    'rel_id'=>$fail->id
                ]);
        }

        foreach($abandoned as $cart)
        {
            DB::table('notifications')
                ->insert([
                    'type'=>'abandoned',
                    'created_at'=>$dt->format('Y-m-d H:i:s'),
                    'rel_id'=>$cart->id
                ]);
        }

        return Response::json("Successful init");
    }

    public function updateNotificationDatabase()
    {
        $abandoned = DB::table('follow_up_carts')->whereNotIn('id', function($q){
            $q->select('rel_id')
                ->where('type', 'abandoned')
                ->distinct()
                ->from('notifications');
        })->get();


        $orders = DB::table('events_ticket_buy')->whereNotIn('order_id', function($q){
            $q->select('rel_id')
                ->where('type', 'order')
                ->distinct()
                ->from('notifications');
        })->get();


        $failed = DB::table('failed_transactions')->whereNotIn('id', function($q){
            $q->select('rel_id')
                ->where('type', 'failed')
                ->distinct()
                ->from('notifications');
        })->get();



        $dt = new DateTime();

        foreach($orders as $sale)
        {
            DB::table('notifications')
                ->insert([
                    'type'=>'order',
                    'created_at'=>$dt->format('Y-m-d H:i:s'),
                    'rel_id'=>$sale->order_id
                ]);

        }

        foreach($failed as $fail)
        {
            DB::table('notifications')
                ->insert([
                    'type'=>'failed',
                    'created_at'=>$dt->format('Y-m-d H:i:s'),
                    'rel_id'=>$fail->id
                ]);
        }

        foreach($abandoned as $cart)
        {
            DB::table('notifications')
                ->insert([
                    'type'=>'abandoned',
                    'created_at'=>$dt->format('Y-m-d H:i:s'),
                    'rel_id'=>$cart->id
                ]);
        }
    }

    public function getUserNotificationsPopup()
    {
        $this->updateNotificationDatabase();

        $user = Sentry::getUser();
        $dt = new DateTime();

        $orders = DB::table('notifications')
            ->whereNotIn('id', function($q) use ($user)
            {
                $q->select('notification_id')
                    ->where('user_id', $user->id)
                    ->where('popup', 1)
                    ->distinct()
                    ->from('user_notifications');
            })
            ->where('type', 'order')
            ->get();

        $failed = DB::table('notifications')
            ->whereNotIn('id', function($q) use ($user)
            {
                $q->select('notification_id')
                    ->where('user_id', $user->id)
                    ->where('popup', 1)
                    ->distinct()
                    ->from('user_notifications');
            })
            ->where('type', 'failed')
            ->get();

        $abandoned = DB::table('notifications')
            ->whereNotIn('id', function($q) use ($user)
            {
                $q->select('notification_id')
                    ->where('user_id', $user->id)
                    ->where('popup', 1)
                    ->distinct()
                    ->from('user_notifications');
            })
            ->where('type', 'abandoned')
            ->get();


        $retArray = array();

        $retArray['orders']=sizeof($orders);
        $retArray['failed']=sizeof($failed);
        $retArray['abandoned']=sizeof($abandoned);

        foreach($orders as $order)
        {
            $rs = DB::table('user_notifications')
                ->where('notification_id',$order->id)
                ->where('user_id',$user->id)
                ->first();

            if(empty($rs))
            {
                DB::table('user_notifications')
                    ->insert([
                        'notification_id'=>$order->id,
                        'user_id'=>$user->id,
                        'created_at'=>$dt->format('Y-m-d H:i:s'),
                        'popup'=>1
                    ]);
            }
            else
            {
                DB::table('user_notifications')
                    ->where('notification_id',$order->id)
                    ->where('user_id', $user->id)
                    ->update(['popup'=>1]);
            }


        }

        foreach($failed as $fail)
        {

            $rs = DB::table('user_notifications')
                ->where('notification_id',$fail->id)
                ->where('user_id',$user->id)
                ->first();

            if(empty($rs))
            {
                DB::table('user_notifications')
                    ->insert([
                        'notification_id'=>$fail->id,
                        'user_id'=>$user->id,
                        'created_at'=>$dt->format('Y-m-d H:i:s'),
                        'popup'=>1
                    ]);
            }
            else
            {
                DB::table('user_notifications')
                    ->where('notification_id',$fail->id)
                    ->where('user_id', $user->id)
                    ->update(['popup'=>1]);
            }
        }

        foreach($abandoned as $cart)
        {
            $rs = DB::table('user_notifications')
                ->where('notification_id',$cart->id)
                ->where('user_id',$user->id)
                ->first();

            if(empty($rs))
            {
                DB::table('user_notifications')
                    ->insert([
                        'notification_id'=>$cart->id,
                        'user_id'=>$user->id,
                        'created_at'=>$dt->format('Y-m-d H:i:s'),
                        'popup'=>1
                    ]);
            }
            else
            {
                DB::table('user_notifications')
                    ->where('notification_id',$cart->id)
                    ->where('user_id', $user->id)
                    ->update(['popup'=>1]);
            }
        }


        return Response::json($retArray,200);
    }

    public function getAlertsBox()
    {
        $this->updateNotificationDatabase();

        $user = Sentry::getUser();
        $dt = new DateTime();

        $orders = DB::table('notifications')
            ->whereNotIn('id', function($q) use ($user)
            {
                $q->select('notification_id')
                    ->where('user_id', $user->id)
                    ->where('notified', 1)
                    ->distinct()
                    ->from('user_notifications');
            })
            ->where('type', 'order')
            ->get();

        $failed = DB::table('notifications')
            ->whereNotIn('id', function($q) use ($user)
            {
                $q->select('notification_id')
                    ->where('user_id', $user->id)
                    ->where('notified', 1)
                    ->distinct()
                    ->from('user_notifications');
            })
            ->where('type', 'failed')
            ->get();

        $abandoned = DB::table('notifications')
            ->whereNotIn('id', function($q) use ($user)
            {
                $q->select('notification_id')
                    ->where('user_id', $user->id)
                    ->where('notified', 1)
                    ->distinct()
                    ->from('user_notifications');
            })
            ->where('type', 'abandoned')
            ->get();

        $retArray = array();
        $retArray['orders']=sizeof($orders);
        $retArray['failed']=sizeof($failed);
        $retArray['abandoned']=sizeof($abandoned);

        return Response::json($retArray,200);

    }
}