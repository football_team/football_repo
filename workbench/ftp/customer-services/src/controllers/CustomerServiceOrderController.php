<?php

/**
 * Created by PhpStorm.
 * User: baglad
 * Date: 21/06/2016
 * Time: 15:25
 */
class CustomerServiceOrderController extends BaseControllerCS
{
    public function index()
    {
        View::share('menu', 'orders');

        $dNowObj = new DateTime();
        $dThenObj = new DateTime();
        $dThenObj = $dThenObj->modify('-1 week');
        $dNowObj = $dNowObj->modify('+1 day');

        $dString = $dThenObj->format('d/m/Y')." - ".$dNowObj->format('d/m/Y');
        View::share('dateRange', $dString);

        $user = Sentry::getUser();

        $abandoned = DB::table('user_notifications')
            ->whereIn('notification_id', function($q)
            {
                $q->select('id')
                    ->where('type', "order")
                    ->distinct()
                    ->from('notifications');
            })
            ->where('user_id', $user->id)
            ->update(['notified'=>1]);

        return View::make('CustomerServices::backend.orders');
    }

    public function getTimeline()
    {
        //I need to select all of these things according to a date range
        $input = Input::all();

        if(!Input::has('range'))
        {
            $adt = new DateTime();
            $range = "01/01/1990 - ".$adt->format('d/m/Y');

        }
        else
        {
            $range = Input::get('range');
            if($range == '')
            {
                $adt = new DateTime();
                $range = "01/01/1990 - ".$adt->format('d/m/Y');
            }
        }

        $advSearch = array();

        if(Input::has('adv')&&Input::has('ord'))
        {
            $strs = Input::get('adv');
            $ord = Input::get('ord');
            if(sizeof($strs) == sizeof($ord))
            {
                foreach($strs as $i=>$str)
                {
                    $advSearch[$ord[$i]] = $str;
                }
            }
        }

        $dArray = explode('-', $range);

        if(count($dArray)!=2)
        {
            return Response::json("Invalid date range.", 404);
        }

        $from = trim($dArray[0]);
        $to = trim($dArray[1]);

        $fromDObj = \DateTime::createFromFormat('d/m/Y', $from);
        $toDObj = \DateTime::createFromFormat('d/m/Y', $to);

        if((!$fromDObj) && (!$toDObj))
        {
            return Response::json("Invalid dates entered.",404);
        }



        $sales = DB::table('events_ticket_buy')
            ->leftJoin('events_related_tickets', 'events_ticket_buy.product_id', '=', 'events_related_tickets.product_id')
            ->leftJoin('events', 'events_related_tickets.event_id', '=', 'events.id')
            ->leftJoin('web_user as buyer', 'events_ticket_buy.buyer_id', '=', 'buyer.user_id')
            ->leftJoin('web_user as seller', 'events_related_tickets.user_id', '=', 'seller.user_id');

        if(array_key_exists('sOn', $advSearch))
        {
            $sales = $sales->where('events_ticket_buy.order_id', 'like', $advSearch['sOn'] . '%');
        }

        if(array_key_exists('bfn', $advSearch)) {
            $sales = $sales->where('buyer.firstname', 'like', $advSearch['bfn'] . '%');
        }

        if(array_key_exists('bln', $advSearch))
        {
            $sales = $sales->where('buyer.lastname', 'like', $advSearch['bln'].'%');
        }

        if(array_key_exists('bemail', $advSearch))
        {
            $sales = $sales->where('buyer.email', 'like', $advSearch['bemail'].'%');
        }

        if(array_key_exists('sfn', $advSearch)) {
            $sales = $sales->where('seller.firstname', 'like', $advSearch['sfn'] . '%');
        }

        if(array_key_exists('sln', $advSearch))
        {
            $sales = $sales->where('seller.lastname', 'like', $advSearch['sln'].'%');
        }

        if(array_key_exists('semail', $advSearch))
        {
            $sales = $sales->where('seller.email', 'like', $advSearch['semail'].'%');
        }

        $sales = $sales->whereBetween('events_ticket_buy.created_at', [$fromDObj->format('Y-m-d H:i:s'), $toDObj->format('Y-m-d H:i:s')])
            ->select('events.title as title', 'events.datetime as event_date', 'events_ticket_buy.qty as qty', 'events_ticket_buy.amount as amount',
                'buyer.firstname as buyer_firstname', 'buyer.user_id as buyer_id' ,'buyer.lastname as buyer_lastname','buyer.email as buyer_email', 'seller.user_id as seller_id' ,'seller.firstname as seller_firstname', 'seller.lastname as seller_lastname' ,'events_ticket_buy.created_at as created_at',
                'events_ticket_buy.order_id as order_id')
            ->orderBy('events_ticket_buy.created_at', 'DESC')
            ->get();


        foreach($sales as $sale)
        {
            $oId = $sale->order_id;
            $sale_notes = DB::table('order_notes')
                ->where('order_id', $oId)
                ->get();

            if(!empty($sale_notes))
            {
                if(sizeof($sale_notes > 1))
                {
                    usort($sale_notes, array($this, "strToDateCompare"));
                }
                $sale->notes = $sale_notes;
            }
        }

        View::share('sales', $sales);

        return View::make('CustomerServices::backend.partials.order-timeline');
    }

    public function getSalesGraphData(){
        $input = Input::all();

        if(array_key_exists('daterange', $input))
        {
            $dr = $input['daterange'];

            if($dr != '')
            {
                $dArray = explode('-', $dr);

                if(count($dArray)!=2)
                {
                    return Response::json("Invalid date range.", 404);
                }

                $from = trim($dArray[0]);
                $to = trim($dArray[1]);

                $fromDObj = \DateTime::createFromFormat('d/m/Y', $from);
                $toDObj = \DateTime::createFromFormat('d/m/Y', $to);

                if((!$fromDObj) && (!$toDObj))
                {
                    return Response::json("Invalid dates entered.",404);
                }

            }
        }

        return Response::json("Requires date range",404);
    }

}