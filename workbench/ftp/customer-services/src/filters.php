<?php
/**
 * Created by PhpStorm.
 * User: baglad
 * Date: 06/06/2016
 * Time: 14:24
 */
Route::filter('auth.cservices', function () {
    if (!Sentry::check()) {
        return Redirect::route('cservices.login');
    }
    else
    {
        $user = Sentry::getUser();
        $user->getGroups();

        $flag = 0;
        foreach($user->groups as $group)
        {
            if($group->name == 'cservice')
            {
                $flag = 1;
                break;
            }
        }
        if(!$flag)
        {
            return Redirect::route('cservices.login');
        }
    }
});