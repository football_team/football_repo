<?php
/**
 * Created by PhpStorm.
 * User: baglad
 * Date: 06/06/2016
 * Time: 14:25
 */

Route::get('/customer-service/login', array('as'=>'cservices.login', 'uses'=>'CustomerServiceAuthController@login'));
Route::post('/customer-service/login', array('uses'=>'CustomerServiceAuthController@postLogin', 'before'=>'csrf'));

Route::group(array('prefix' => '/customer-service', 'before' => array('auth.cservices')), function (){
    Route::post('/logout', array('uses'=>'CustomerServiceAuthController@logout', 'before'=>'csrf'));

    Route::get('/', array('as' => 'cservice.dashboard', 'uses'=>'CustomerServiceDashboardController@index'));

    Route::post('/customers/list', array('uses'=>'CustomerServiceDashboardController@customerList'));
    Route::get('/customers/{id}', array('uses'=>'CustomerServiceDashboardController@showCustomer'))->where('id', '[0-9]+');
    Route::any('/customers/activity-feed/{id}', array('uses'=>'CustomerServiceDashboardController@getTimeline'))->where('id', '[0-9]+');

    Route::post('/customers/addNote/{id}', array('uses'=>'CustomerServiceDashboardController@addNote', 'before'=>'csrf'))->where('id', '[0-9]+');
    Route::post('/customers/deleteNote/{id}', array('uses'=>'CustomerServiceDashboardController@deleteNote', 'before'=>'csrf'))->where('id', '[0-9]+');
    Route::post('/customers/editNote/{id}', array('uses'=>'CustomerServiceDashboardController@editNote', 'before'=>'csrf'))->where('id', '[0-9]+');
    Route::post('/customers/getNote/{id}', array('uses'=>'CustomerServiceDashboardController@getNote', 'before'=>'csrf'))->where('id', '[0-9]+');

    Route::post('/customer/view-order-notes/{id}', array('uses'=>'CustomerServiceDashboardController@getOrderNotes', 'before'=>'csrf'))->where('id', '[0-9]+');
    Route::post('/customers/update-order-note/{id}', array('uses'=>'CustomerServiceDashboardController@saveOrderNote', 'before'=>'csrf'))->where('id','[0-9]+');
    Route::post('/customers/save-new-order-note/{id}', array('uses'=>'CustomerServiceDashboardController@addOrderNote', 'before'=>'csrf'))->where('id','[0-9]+');
    Route::post('/customers/delete-order-note/{id}', array('uses'=>'CustomerServiceDashboardController@deleteOrderNote', 'before'=>'csrf'))->where('id','[0-9]+');

    Route::post('/customers/change-type/{id}', array('uses'=>'CustomerServiceDashboardController@changeWebUserType', 'before'=>'csrf'))->where('id', '[0-9]+');

    Route::get('/customers/getFullEmail/{id}', array('uses'=>'CustomerServiceDashboardController@getFullEmail'))->where('id', '[0-9]+');
    Route::post('/customers/getFullOrder/{id}', array('uses'=>'CustomerServiceDashboardController@getFullOrder'))->where('id', '[0-9]+');

    Route::post('/customers/release-failed/{tid}', array('uses'=>'CustomerServiceDashboardController@makeFailedTransaction'))->where('id','[0-9]+');

    //Route::any('/notification-centre/first-run', array('uses' => 'CustomerServiceNotificationController@firstRun'));

    Route::post('/notification-centre/get', array('uses'=>'CustomerServiceNotificationController@getUserNotificationsPopup'));
    Route::post('/notification-centre/getBox', array('uses'=>'CustomerServiceNotificationController@getAlertsBox'));

    Route::get('/orders', array('uses'=>'CustomerServiceOrderController@index'));
    Route::post('/orders/activity-feed', array('uses'=>'CustomerServiceOrderController@getTimeline'));

    Route::get('/failed-transactions', array('uses'=>'CustomerServiceFailedController@index'));
    Route::post('/failed-transactions/activity-feed', array('uses'=>'CustomerServiceFailedController@getTimeline'));

    Route::get('/abandoned-carts', array('uses'=>'CustomerServiceAbandonedController@index'));
    Route::post('/abandoned-carts/activity-feed', array('uses'=>'CustomerServiceAbandonedController@getTimeline'));

    Route::get('/calendar', 'CustomerServiceCalendarController@index');
    Route::post('/calendar/getEvents', array('uses'=>'CustomerServiceCalendarController@getEvents', 'before'=>'csrf'));
    Route::post('/calendar/addEvent', array('uses'=>'CustomerServiceCalendarController@addEvent', 'before'=>'csrf'));
    Route::post('/calendar/deleteEvent', array('uses'=>'CustomerServiceCalendarController@deleteEvent', 'before'=>'csrf'));
    Route::post('/calendar/moveEvent', array('uses'=>'CustomerServiceCalendarController@moveEvent', 'before'=>'csrf'));
    Route::post('/calendar/updateTitle', array('uses'=>'CustomerServiceCalendarController@updateTitle', 'before'=>'csrf'));
});