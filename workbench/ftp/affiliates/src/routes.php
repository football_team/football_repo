<?php
/**
 * Created by PhpStorm.
 * User: baglad
 * Date: 18/03/2016
 * Time: 16:07
 */

Route::get('/affiliates', 'AffiliateRegistrationController@index');

Route::post('/affiliates/signup', 'AffiliateRegistrationController@signUp');

Route::get('/affiliates/verify', 'AffiliateRegistrationController@verify');

Route::post('/affiliates/verify-account', 'AffiliateRegistrationController@verifyDomain');

Route::get('/affiliates/login', array('as'=>'affiliate.login','uses'=>'AffiliateAuthController@login'));

Route::post('/affiliates/login', array('uses'=>'AffiliateAuthController@postLogin', 'before'=>'csrf'));

Route::post('/affiliates/logout', array('uses'=>'AffiliateAuthController@postLogout', 'before'=>'csrf'));

Route::any('/affiliates/productfeed.csv', array('uses' => 'AffiliateProductFeedController@gerneralFeed'));
Route::any('/affiliates/feed/{af_code}', array('uses' => 'AffiliateProductFeedController@getFeed'));
Route::any('/affiliates/feedredweb/{af_code}', array('uses' => 'AffiliateProductFeedController@getFeedRedWeb'));

//Obfuscated route for JSON api to access sales feed. Needs to be secure, verified by affiliate contact email and af_code

Route::any('/affiliates/sale-feed/{af_code}', array('uses'=>'AffiliateProductFeedController@getSales'));

Route::group(array('prefix' => '/affiliates/dashboard', 'before' => array('auth.affiliate')), function ()
{
    Route::get('/', array('as' => 'affiliates.dashboard', 'uses'=>'AffiliateDashboardController@index'));

    Route::get('/profile', array('uses'=>'AffiliateAccountController@index'));
    Route::post('/profile/update-contact-name', array('uses'=>'AffiliateAccountController@updateContactName', 'before'=>'csrf'));
    Route::post('/profile/update-password', array('uses'=>'AffiliateAccountController@updatePassword', 'before'=>'csrf'));
    Route::post('/profile/update-bank', array('uses'=>'AffiliateAccountController@updateBank', 'before'=>'csrf'));

    Route::get('/finances/balance', array('as' => 'affiliates.dashboard.finances.balance', 'uses'=>'AffiliateDashboardController@financesBalance'));
    Route::post('/finances/balance/new-withdrawal-request', array('uses'=>'AffiliateWithdrawalController@addNewWithdrawal', 'before'=>'csrf'));
    Route::post('/finances/balance/update-withdrawal-request', array('uses'=>'AffiliateWithdrawalController@updateWithdrawal', 'before'=>'csrf'));
    Route::post('/finances/balance/cancel-withdrawal-request', array('uses'=>'AffiliateWithdrawalController@cancelWithdrawal', 'before'=>'csrf'));

    Route::get('/finances/payment-history', array('as' => 'affiliates.dashboard.finances.payment-history', 'uses'=>'AffiliateDashboardController@financesPaymentHistory'));
    Route::post('/finances/payment-history/dt', array('uses'=>'WidgetController@paymentHistoryDataTable'));

    Route::get('/product-feed', array('uses'=>'AffiliateDashboardController@productFeed'));


    Route::post('/widgets/dash-data-range', array('uses'=>'WidgetController@getDashDateRangeSales'));
    Route::post('/widgets/dash-data-range-footer', array('uses'=>'WidgetController@getDashDateRangeFooter'));
    Route::post('/widgets/dash-daily-range', array('uses'=>'WidgetController@getDashDailyChartSales'));
    Route::post('/widgets/dash-recent-orders', array('uses'=>'WidgetController@getDashRecentOrders'));
    Route::post('/widgets/finance-balance-timeline', array('uses'=>'WidgetController@getFinanceBalanceTimeline'));

    Route::get('/sales', array('uses'=>'AffiliateDashboardController@sales'));
    Route::post('/sales/dt', array('uses'=>'WidgetController@affiliateSalesDT'));
    Route::post('/sales/totals', array('uses'=>'WidgetController@affiliateSalesTotals'));

});



