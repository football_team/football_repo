<?php
/**
 * Created by PhpStorm.
 * User: baglad
 * Date: 29/03/2016
 * Time: 11:47
 */

class HeaderComposer{

    public function compose($view)
    {
        $user = Sentry::getUser();

        $property = AffiliateProperty::where('user_id', $user->id)->first();

        $view->with('property',$property);
    }
}