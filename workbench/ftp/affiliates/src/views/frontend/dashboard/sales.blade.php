@extends(Template::name('Affiliates::frontend.dashboard._layout.layout'))

@section('page_title')
    {{trans('homepage.affilSales')}}
@stop

@section('page_sdesc')

@stop

@section('page_content')
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">{{trans('homepage.affilTotals')}}</h3>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <table id="dt_basic2" class="table table-bordered table-hover" width="100%">
                                <thead>
                                <tr>
                                    <th>{{trans('homepage.affilTotalNumberOfSales')}}</th>
                                    <th>{{trans('homepage.affilTotalSaleValue')}}</th>
                                    <th>{{trans('homepage.affilACP')}}</th>
                                    <th>{{trans('homepage.affilTCE')}}</th>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">{{trans('homepage.affilSalesHistory')}}</h3>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <table id="dt_basic" class="table table-bordered table-hover" width="100%">
                                <thead>
                                <tr>
                                    <th>{{trans('homepage.affilOrderID')}}</th>
                                    <th>{{trans('homepage.affilProductInformation')}}</th>
                                    <th>{{trans('homepage.affilDate')}}</th>
                                    <th>{{trans('homepage.affilTotalSalePrice')}}</th>
                                    <th>{{trans('homepage.affilCommissionPercentage')}}</th>
                                    <th>{{trans('homepage.affilCommissionAmount')}}</th>
                                    <th>URL</th>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('style')
    <link rel="stylesheet" href="/assets/adminlte/plugins/datatables/dataTables.bootstrap.css">
    <style>
        .wrap-words {
            overflow-wrap: break-word;
            word-wrap: break-word;
            -ms-word-break: break-all;
            word-break: break-all;
            word-break: break-word;
            -ms-hyphens: auto;
            -moz-hyphens: auto;
            -webkit-hyphens: auto;
            hyphens: auto;
        }

        .hidden-tcell{
            display:block;
            max-width:400px;
        }
    </style>
@stop

@section('script')
    <script src="/assets/adminlte/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="/assets/adminlte/plugins/datatables/dataTables.bootstrap.min.js"></script>

    <script>
        $('#dt_basic').dataTable({
            "aoColumnDefs": [
                { "sClass": "wrap-words", "aTargets": [ 6 ] }
            ],
            paging: true,
            lengthChange: true,
            searching: false,
            ordering: false,
            serverSide: true,
            ajax: {
                url: "/affiliates/dashboard/sales/dt",
                type: 'POST'
            },
            order: [[0, 'ASC']],
            "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
                $('.delete-node', nRow).remove();
            }
        });

        $('#dt_basic2').dataTable({
            paging: false,
            lengthChange: false,
            searching: false,
            ordering: false,
            serverSide: true,
            ajax: {
                url: "/affiliates/dashboard/sales/totals",
                type: 'POST'
            },
            order: [[0, 'ASC']],
            "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
                $('.delete-node', nRow).remove();
            }
        });
    </script>
@stop
