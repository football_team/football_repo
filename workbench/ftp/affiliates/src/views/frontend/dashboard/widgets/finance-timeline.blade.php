<ul class="timeline">
    @foreach($timeline_elements as $element)
        <li class="time-label">
            <span class="bg-red @if($element[0]=="Sales"){{"bg-green"}} @else {{"bg-red"}} @endif">
                {{$element[1]}}
            </span>
        </li>

        <li>
            <!-- timeline icon -->
            <i class="fa @if($element[0]=="Sales"){{"fa-caret-up bg-green"}} @else {{"fa-caret-down bg-red"}} @endif"></i>
            <div class="timeline-item">
                <h3 class="timeline-header">@if($element[0]=="Sales"){{"Monthly Sales"}} @else {{"Payment"}} @endif</h3>

                <div class="timeline-body">
                    @if($element[0]=="Sales")
                        {{$element[2]}} {{trans('homepage.affilSalesEarningC')}} {{$element[3]}} {{trans('homepage.affilIYB')}}
                    @else
                        {{trans('homepage.affilPMF')}} £{{$element[2]}} {{trans('homepage.affilOOYB')}}
                    @endif
                </div>
            </div>
        </li>
    @endforeach
</ul>