@extends(Template::name('Affiliates::frontend.dashboard._layout.layout'))

@section('page_title')
{{trans('homepage.affilProductFeed')}}
@stop

@section('page_sdesc')

@stop

@section('page_content')
    <!--
    //Need to basically have all the options available for the product feed.
    //Describe the params available
    //Describe the format the feed outputs in
    //Give a link they can use to generate the feed

    //Basically this is all documentation and a couple of links

    //GOGO
    -->

    <div class="row">
        <div class="col-md-6">
            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">{{trans('homepage.affilGettingStartedGuide')}}</h3>
                        </div>
                        <div class="box-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <p>
                                        {{trans('homepage.affilPFG1')}}
                                    </p>
                                    <p>
                                        {{trans('homepage.affilPFG2')}}
                                    </p>
                                    <p>
                                        {{trans('homepage.affilPFG3')}}
                                    </p>
                                    <p>
                                        {{$domain}}affiliates/feed/YOUR AFFILIATE REFERENCE CODE
                                    </p>
                                    <p>
                                        {{trans('homepage.affilYRC')}} <b>{{$ref_code}}</b>
                                    </p>
                                    <p>
                                        {{trans('homepage.affilPFG4')}}
                                    </p>
                                    <p>
                                        <a href="{{$domain}}affiliates/feed/{{$ref_code}}">{{$domain}}affiliates/feed/{{$ref_code}}</a>
                                    </p>
                                    <p>
                                        {{trans('homepage.affilPFG5')}}
                                    </p>
                                    <p>
                                    {{trans('homepage.affilAdditionalParameters')}}
                                        <ul>
                                            <li>
                                                {{trans('homepage.affilPFG6')}}<br>
                                                {{trans('homepage.affilPFG7')}}
                                            </li>
                                            <li>
                                                {{trans('homepage.affilPFG8')}}
                                            </li>
                                            <li>
                                                isCSV : Default FALSE. Setting this to TRUE (case sensitive) returns the feed in a CSV format.
                                            </li>
                                        </ul>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">

                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">{{trans('homepage.affilGFU')}}</h3>
                        </div>
                        <div class="box-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <table class="table table-bordered">
                                        <tbody>
                                            <tr>
                                                <td>{{trans('homepage.affilGPBE')}}</td>
                                                <td>
                                                    <select name="gbe" id="gbe">
                                                        <option value="1">{{trans('homepage.Yes')}}</option>
                                                        <option value="0">{{trans('homepage.No')}}</option>
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>{{trans('homepage.affilOPBD')}}</td>
                                                <td>
                                                    <select name="obd" id="obd">
                                                        <option value="ASC">{{trans('homepage.affilAscending')}}</option>
                                                        <option value="DESC">{{trans('homepage.affilDescending')}}</option>
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>CSV</td>
                                                <td>
                                                    <select name="icsv" id="icsv">
                                                        <option value="FALSE" selected>FALSE</option>
                                                        <option value="TRUE">TRUE</option>
                                                    </select>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <div id="url-area">

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@stop

@section('style')

@stop

@section('script')
    <script>
        $(function(){
           genUrl();
        });

        $('#gbe').change(function(e){
            genUrl();
        });

        $('#obd').change(function(e){
            genUrl();
        });

        $('#icsv').change(function(e){
            genUrl();
        });

        function genUrl(){
            var str = "https://{{$url}}/affiliates/feed/{{$ref_code}}";

            var gbe = $('#gbe').val();
            var obd = $('#obd').val();
            var icsv = $('#icsv').val();

            str += "?group_by_event="+gbe+"&order_dir="+obd+"&isCSV="+icsv;

            var aRef = '<a href="'+str+'">'+str+'</a>';

            console.log(aRef);
            $('#url-area').html(aRef);
        }
    </script>
@stop
