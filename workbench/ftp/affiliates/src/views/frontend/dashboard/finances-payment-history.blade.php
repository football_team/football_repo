@extends(Template::name('Affiliates::frontend.dashboard._layout.layout'))

@section('page_title')
    {{trans('homepage.affilFinances')}}
@stop

@section('page_sdesc')
    {{trans('homepage.affilPaymentHistory')}}
@stop

@section('page_content')
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">{{trans('homepage.affilPaymentHistory')}}</h3>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <table id="dt_basic" class="table table-bordered table-hover" width="100%">
                                <thead>
                                <tr>
                                    <th>{{trans('homepage.affilID')}}</th>
                                    <th>{{trans('homepage.affilRequestedFor')}}</th>
                                    <th>{{trans('homepage.affilAmount')}} £</th>
                                    <th>{{trans('homepage.affilStatus')}}</th>
                                    <th>{{trans('homepage.affilActionedOn')}}</th>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('style')
    <link rel="stylesheet" href="/assets/adminlte/plugins/datatables/dataTables.bootstrap.css">
@stop

@section('script')
    <script src="/assets/adminlte/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="/assets/adminlte/plugins/datatables/dataTables.bootstrap.min.js"></script>

    <script>
        $('#dt_basic').dataTable({
            paging: true,
            lengthChange: true,
            searching: false,
            ordering: false,
            serverSide: true,
            ajax: {
                url: "/affiliates/dashboard/finances/payment-history/dt",
                type: 'POST'
            },
            order: [[0, 'ASC']],
            "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
                $('.delete-node', nRow).remove();
            }
        });
    </script>
@stop
