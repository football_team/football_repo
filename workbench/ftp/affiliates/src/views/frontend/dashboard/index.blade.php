@extends(Template::name('Affiliates::frontend.dashboard._layout.layout'))

@section('page_title')
    {{trans('homepage.affilAffiliateDashboard')}}
@stop

@section('page_sdesc')
    {{trans('homepage.affilOverview')}}
@stop

@section('page_content')
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">{{trans('homepage.affilMonthlyRecapReport')}}</h3>
                    <input type="text" class="form-control pull-right" value="{{$sales_range}}" id="reservation">
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <p class="text-center">
                                <strong id="dRangeTitle">
                                    {{trans('homepage.affilSalesC')}} {{$sales_range}}
                                </strong>
                            </p>

                            <div class="chart" id="chart">
                                <!-- Sales Chart Canvas -->

                            </div>
                            <!-- /.chart-responsive -->
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- ./box-body -->
                <div class="box-footer">
                    <div class="row">
                        <div class="col-sm-1 col-xs-0"></div>
                        <div class="col-sm-2 col-xs-6">
                            <div class="description-block border-right">
                                <h5 class="description-header" id="sTotalFig"></h5>
                                <span class="description-text">{{trans('homepage.affilSalesTotal')}}</span>
                            </div>
                            <!-- /.description-block -->
                        </div>
                        <!-- /.col -->
                        <div class="col-sm-2 col-xs-6">
                            <div class="description-block border-right">
                                <h5 class="description-header" id="comTotalFig"></h5>
                                <span class="description-text">{{trans('homepage.affilSalesCommission')}}</span>
                            </div>
                            <!-- /.description-block -->
                        </div>
                        <!-- /.col -->
                        <div class="col-sm-2 col-xs-6">
                            <div class="description-block border-right">
                                <h5 class="description-header" id="avgCom"></h5>
                                <span class="description-text">{{trans('homepage.affilAvgCom')}}</span>
                            </div>
                            <!-- /.description-block -->
                        </div>
                        <!-- /.col -->
                        <div class="col-sm-2 col-xs-6">
                            <div class="description-block">
                                <h5 class="description-header" id="curCom"></h5>
                                <span class="description-text">{{trans('homepage.affilCurCom')}}</span>
                            </div>
                            <!-- /.description-block -->
                        </div>
                        <div class="col-sm-2 col-xs-6">
                            <div class="description-block">
                                <h5 class="description-header" id="clicksTotal"></h5>
                                <span class="description-text">{{trans('homepage.affilNumberOfReferrals')}}</span>
                            </div>
                            <!-- /.description-block -->
                        </div>
                        <div class="col-sm-1 col-xs-0"></div>
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.box-footer -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">
                        Daily Report - {{$today}}
                    </h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="chart" id="chart-daily">
                                <!-- Sales Chart Canvas -->
                            </div>
                            <!-- /.chart-responsive -->
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </div>
            </div>
        </div>
    </div>
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">{{trans('homepage.affilLatestOrders')}}</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="table-responsive">
                <table class="table no-margin">
                    <thead>
                        <tr>
                            <th>{{trans('homepage.affilOrderID')}}</th>
                            <th>{{trans('homepage.affilItem')}}</th>
                            <th>{{trans('homepage.affilDate')}}</th>
                            <th>{{trans('homepage.affilTotalSalePrice')}}</th>
                            <th>{{trans('homepage.affilComPerc')}}</th>
                            <th>{{trans('homepage.affilComVal')}}</th>
                        </tr>
                    </thead>
                    <tbody id="recent_orders">

                    </tbody>
                </table>
            </div>
            <!-- /.table-responsive -->
        </div>
        <!-- /.box-body -->
        <div class="box-footer clearfix">
            <a href="#" id="recent_refresh" class="btn btn-sm btn-default btn-flat pull-left">{{trans('homepage.affilRefresh')}}</a>
            <a href="/affiliates/dashboard/sales" class="btn btn-sm btn-default btn-flat pull-right">{{trans('homepage.affilViewAllOrders')}}</a>
        </div>
        <!-- /.box-footer -->
    </div>
@stop

@section('style')

@stop

@section('script')
<script>
    $(function() {
        $('#reservation').daterangepicker({
            format: 'DD/MM/YYYY'
        });

        $('#reservation').change(function(e){
            //Get the title
            $('#dRangeTitle').html("Sales: "+$(this).val());
            //Get the chart data
            getDateRangeChart($(this).val());
        });

        $('body').on('click', '#recent_refresh', function(e){
            e.preventDefault();
            getRecentOrders();
        });

        getDateRangeChart("{{$sales_range}}");
        getRecentOrders();
        getDailyChart();

        function getDateRangeChart($range){
            $('#chart').html('<canvas id="salesChart" style="height: 180px;"></canvas>');

            // Get context with jQuery - using jQuery's .get() method.
            var salesChartCanvas = $("#salesChart").get(0).getContext("2d");
            // This will get the first returned node in the jQuery collection.
            var salesChart = new Chart(salesChartCanvas);

            $.ajax({
                url:'/affiliates/dashboard/widgets/dash-data-range',
                type:'POST',
                data:{
                    'range':$range
                },
                success:function(response){
                    var labels = [];
                    var dataset = {
                        label: "Sales Number",
                        fillColor: "#00b040",
                        strokeColor: "#00b040",
                        pointColor: "#00b040",
                        pointStrokeColor: "#c1c7d1",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgb(220,220,220)",
                        data: []
                    };
                    jQuery.each(response, function(){
                        labels[labels.length] = this.title;
                        dataset.data[dataset.data.length] = this.ttl;
                    });
                    var salesChartData = {
                        labels: labels,
                        datasets: [
                            dataset
                        ]
                    };

                    var salesChartOptions = {
                        //Boolean - If we should show the scale at all
                        showScale: true,
                        //Boolean - Whether grid lines are shown across the chart
                        scaleShowGridLines: false,
                        //String - Colour of the grid lines
                        scaleGridLineColor: "rgba(0,0,0,.05)",
                        //Number - Width of the grid lines
                        scaleGridLineWidth: 1,
                        //Boolean - Whether to show horizontal lines (except X axis)
                        scaleShowHorizontalLines: true,
                        //Boolean - Whether to show vertical lines (except Y axis)
                        scaleShowVerticalLines: true,
                        //Boolean - Whether the line is curved between points
                        bezierCurve: true,
                        //Number - Tension of the bezier curve between points
                        bezierCurveTension: 0.3,
                        //Boolean - Whether to show a dot for each point
                        pointDot: true,
                        //Number - Radius of each point dot in pixels
                        pointDotRadius: 4,
                        //Number - Pixel width of point dot stroke
                        pointDotStrokeWidth: 1,
                        //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
                        pointHitDetectionRadius: 20,
                        //Boolean - Whether to show a stroke for datasets
                        datasetStroke: true,
                        //Number - Pixel width of dataset stroke
                        datasetStrokeWidth: 2,
                        //Boolean - Whether to fill the dataset with a color
                        datasetFill: true,
                        //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
                        maintainAspectRatio: true,
                        //Boolean - whether to make the chart responsive to window resizing
                        responsive: true
                    };

                    //Create the line chart
                    salesChart.Line(salesChartData, salesChartOptions);
                },
                error:function(response){
                    console.log("Error posting for data range graph");
                }
            });
            $.ajax({
                url:"/" +
                "affiliates/dashboard/widgets/dash-data-range-footer",
                type:"POST",
                data:{
                    'range':$range
                },
                success:function(response){
                    $('#sTotalFig').html("£"+response.sttl);
                    $('#comTotalFig').html("£"+response.scom);
                    $('#avgCom').html(response.avgcom);
                    $('#curCom').html(response.curcom);
                    $('#clicksTotal').html(response.clicks);
                },
                error:function(response){
                    console.log("Could not get footer data.");
                }
            });
        }

        function getDailyChart($range){
            $('#chart-daily').html('<canvas id="salesChartDaily" style="height: 180px;"></canvas>');

            // Get context with jQuery - using jQuery's .get() method.
            var salesChartCanvas = $("#salesChartDaily").get(0).getContext("2d");
            // This will get the first returned node in the jQuery collection.
            var salesChart = new Chart(salesChartCanvas);

            $.ajax({
                url:'/affiliates/dashboard/widgets/dash-daily-range',
                type:'POST',
                data:{
                    'range':$range
                },
                success:function(response){
                    var labels = [];
                    var dataset = {
                        label: "Sales Number",
                        fillColor: "#00b040",
                        strokeColor: "#00b040",
                        pointColor: "#00b040",
                        pointStrokeColor: "#c1c7d1",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgb(220,220,220)",
                        data: []
                    };
                    jQuery.each(response, function(){

                        labels[labels.length] = this.hourLZ;
                        dataset.data[dataset.data.length] = this.ttl;
                    });

                    var salesChartData = {
                        labels: labels,
                        datasets: [
                            dataset
                        ]
                    };

                    var salesChartOptions = {
                        //Boolean - If we should show the scale at all
                        showScale: true,
                        //Boolean - Whether grid lines are shown across the chart
                        scaleShowGridLines: false,
                        //String - Colour of the grid lines
                        scaleGridLineColor: "rgba(0,0,0,.05)",
                        //Number - Width of the grid lines
                        scaleGridLineWidth: 1,
                        //Boolean - Whether to show horizontal lines (except X axis)
                        scaleShowHorizontalLines: true,
                        //Boolean - Whether to show vertical lines (except Y axis)
                        scaleShowVerticalLines: true,
                        //Boolean - Whether the line is curved between points
                        bezierCurve: true,
                        //Number - Tension of the bezier curve between points
                        bezierCurveTension: 0.3,
                        //Boolean - Whether to show a dot for each point
                        pointDot: true,
                        //Number - Radius of each point dot in pixels
                        pointDotRadius: 4,
                        //Number - Pixel width of point dot stroke
                        pointDotStrokeWidth: 1,
                        //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
                        pointHitDetectionRadius: 20,
                        //Boolean - Whether to show a stroke for datasets
                        datasetStroke: true,
                        //Number - Pixel width of dataset stroke
                        datasetStrokeWidth: 2,
                        //Boolean - Whether to fill the dataset with a color
                        datasetFill: true,
                        //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
                        maintainAspectRatio: true,
                        //Boolean - whether to make the chart responsive to window resizing
                        responsive: true
                    };

                    //Create the line chart
                    salesChart.Line(salesChartData, salesChartOptions);
                },
                error:function(response){
                    console.log("Error posting for data range graph");
                }
            });
        }

        function getRecentOrders()
        {
            $.ajax({
                url:"/affiliates/dashboard/widgets/dash-recent-orders",
                type:"POST",
                success:function(response){
                    var appStr = "";

                    jQuery.each(response, function(){

                        appStr+="<tr>";
                        appStr+="<td>";
                            appStr+=this.order_id;
                        appStr+="</td>";
                        appStr+="<td>";
                            appStr+=this.title+" "+this.datetime;
                        appStr+="</td>";
                        appStr+="<td>";
                            appStr+='<span class="label label-success">'+this.created_at+'</span>';
                        appStr+="</td>";
                        appStr+="<td>";
                            appStr+="£"+this.amount;
                        appStr+="</td>";
                        appStr+="<td>";
                            appStr+=this.commission_perc+"%";
                        appStr+="</td>";
                        appStr+="<td>";
                            appStr+="£"+this.com_amount;
                        appStr+="</td>";
                        appStr+="</tr>";
                    });

                    $('#recent_orders').html(appStr);
                },
                error:function(response){
                }
            })
        }
    });



</script>
@stop
