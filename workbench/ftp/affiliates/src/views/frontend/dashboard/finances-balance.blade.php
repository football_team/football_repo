@extends(Template::name('Affiliates::frontend.dashboard._layout.layout'))

@section('page_title')
    {{trans('homepage.affilFinances')}}
@stop

@section('page_sdesc')
    {{trans('homepage.affilMyBalance')}}
@stop

@section('page_content')
    <div class="row">
        <div class="col-md-6">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-default">
                        <div class="box-header with-border">
                            <i class="fa fa-area-chart"></i>
                            <h3 class="box-title">{{trans('homepage.affilBalanceSummary')}}</h3>
                        </div>
                        <div class="box-body">
                            <div class="callout callout-success">
                                <h4>
                                    {{trans('homepage.affilYourBalanceIs')}}: £{{$balance}}
                                </h4>
                            </div>
                            <div class="info-box bg-aqua">
                                <span class="info-box-icon"><i class="fa @if($balance < $min_withdrawal){{"fa-lock"}}@else{{"fa-unlock"}}@endif"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">{{trans('homepage.affilProgressTillPayout')}}</span>
                                    <span class="info-box-number">£{{$balance}} / £{{$min_withdrawal}}</span>

                                    <div class="progress">
                                        <div class="progress-bar" style="width: {{($balance/$min_withdrawal)*100}}%"></div>
                                    </div>
                                    <span class="progress-description">
                                        {{($balance/$min_withdrawal)*100}}{{trans('homepage.affilPTWM')}}
                                    </span>
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-default">
                        <div class="box-header with-border">
                            <i class="fa fa-clock-o"></i>
                            <h3 class="box-title">{{trans('homepage.affilPaymentDates')}}</h3>
                        </div>
                        <div class="box-body">
                            <div class="row">
                                <div class="col-xs-6 col-md-4 text-center">
                                    <input type="text" class="knob" value="{{$dToday}}" data-max="{{$nDTotal}}" data-thickness=".1" data-width="90" data-height="90" data-fgColor="#3c8dbc" data-readonly="true">

                                    <div class="knob-label">{{trans('homepage.affilDaysTillPayout')}}</div>
                                </div>
                                <div class="col-md-8">
                                    <h4>{{trans('homepage.affilPayoutDate')}}</h4>
                                    <p style = "font-size:30px;"><i class="fa fa-calendar"></i>&nbsp; {{$payoutDate}}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @if($allowed)
                @if($withdrawal)
                    <div class="row">
                        <div class="col-md-12">
                                <div class="box box-default">
                                    <div class="box-header with-border">
                                        <i class="fa fa-credit-card"></i>
                                        <h3 class="box-title">{{trans('homepage.affilActiveWithdrawalRequest')}}</h3>
                                    </div>
                                    <div class="box-body">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <form id="updateForm">
                                                    <table class="table table-bordered">
                                                        <tbody>
                                                        <tr>
                                                            <th>#</th>
                                                            <th>{{trans('homepage.affilStatus')}}</th>
                                                            <th>{{trans('homepage.affilWithdrawalAmount')}} £</th>
                                                            <th>{{trans('homepage.affilQuickUpdate')}}</th>
                                                            <th>{{trans('homepage.affilCancel')}}</th>
                                                        </tr>
                                                        <tr>
                                                            <td>{{$withdrawal->id}}</td>
                                                            <td>{{$withdrawal->status}}</td>
                                                            <td>
                                                                <input type="text" class="form-control" name="wAmount" id="wAmount" value="{{$withdrawal->amount}}">
                                                            </td>
                                                            <td>
                                                                <button type="submit" class="btn btn-info">{{trans('homepage.affilSubmit')}}</button>
                                                            </td>
                                                            <td>
                                                                <button id="cancel-withdrawal" data-id="{{$withdrawal->id}}" class="btn btn-default">{{trans('homepage.affilCancel')}}</button>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        </div>
                    </div>
                @elseif($balance>=$min_withdrawal)
                    <div class="row">
                        <div class="col-md-12">
                            <div class="box box-default">
                                <div class="box-header with-border">
                                    <i class="fa fa-credit-card"></i>
                                    <h3 class="box-title">{{trans('homepage.affilCreateWithdrawalRequest')}}</h3>
                                </div>
                                <div class="box-body">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <form class="form-horizontal" id="createWR">
                                                <div class="box-body">
                                                    <div class="form-group">
                                                        <label for="nw_amount" class="col-sm-2 control-label">{{trans('homepage.affilAmount')}} £: </label>
                                                        <div class="col-sm-10">
                                                            <input class="form-control" id="nw_amount" type="text">
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- /.box-body -->
                                                <div class="box-footer">
                                                    <button type="submit" class="btn btn-info pull-right">{{trans('homepage.affilSubmit')}}</button>
                                                </div>
                                                <!-- /.box-footer -->
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
            @else
                <div class="row">
                    <div class="col-md-12">
                        <div class="box box-default">
                            <div class="box-header with-border">
                                <i class="fa fa-credit-card"></i>
                                <h3 class="box-title">{{trans('homepage.affilCreateWithdrawalRequest')}}</h3>
                            </div>
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-xs-12">
                                        {{trans('homepage.affilPUYBD')}} <a href="/affiliates/dashboard/profile">{{trans('homepage.affilmyAccount')}}</a> {{trans('homepage.affilSBPAWR')}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-default">
                        <div class="box-header with-border">
                            <i class="fa fa-credit-card"></i>
                            <h3 class="box-title">{{trans('homepage.affilCurrentCommissionRate')}}</h3>
                        </div>
                        <div class="box-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    {{trans('homepage.affilRateName')}}: {{$rateName}}<br>
                                    {{trans('homepage.affilCurrentCommission')}}: {{$curCom}}%
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6" id="timeline-area">

        </div>
    </div>

@stop

@section('style')

@stop

@section('script')
    <script src="/assets/adminlte/plugins/knob/jquery.knob.js"></script>
    <script>
        $(function(){
            $(".knob").knob({
                /*change : function (value) {
                 //console.log("change : " + value);
                 },
                 release : function (value) {
                 console.log("release : " + value);
                 },
                 cancel : function () {
                 console.log("cancel : " + this.value);
                 },*/
                draw: function () {
                    // "tron" case


                        var a = this.angle(this.cv)  // Angle
                                , sa = this.startAngle          // Previous start angle
                                , sat = this.startAngle         // Start angle
                                , ea                            // Previous end angle
                                , eat = sat + a                 // End angle
                                , r = true;

                        this.g.lineWidth = this.lineWidth;

                        this.o.cursor
                        && (sat = eat - 0.3)
                        && (eat = eat + 0.3);

                        if (this.o.displayPrevious) {
                            ea = this.startAngle + this.angle(this.value);
                            this.o.cursor
                            && (sa = ea - 0.3)
                            && (ea = ea + 0.3);
                            this.g.beginPath();
                            this.g.strokeStyle = this.previousColor;
                            this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, sa, ea, false);
                            this.g.stroke();
                        }

                        this.g.beginPath();
                        this.g.strokeStyle = r ? this.o.fgColor : this.fgColor;
                        this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, sat, eat, false);
                        this.g.stroke();

                        this.g.lineWidth = 2;
                        this.g.beginPath();
                        this.g.strokeStyle = this.o.fgColor;
                        this.g.arc(this.xy, this.xy, this.radius - this.lineWidth + 1 + this.lineWidth * 2 / 3, 0, 2 * Math.PI, false);
                        this.g.stroke();

                        return false;

                }
            });


            $('#createWR').submit(function(e){
                e.preventDefault();

                var amount = $('#nw_amount').val();

                amount = parseFloat(amount);

                if(isNaN(amount))
                {
                    alert("Value entered is not a number.");
                    return false;
                }
                else if(amount < {{$min_withdrawal}})
                {
                    alert("Value entered must be greater than £{{$min_withdrawal}}");

                }
                else if(amount > {{$balance}})
                {
                    alert("Value must be less than or equal to your balance with us: £{{$balance}}.");
                }
                else
                {
                    $.ajax({
                        url:"/affiliates/dashboard/finances/balance/new-withdrawal-request",
                        type:"post",
                        data:{
                            amount:amount
                        },
                        success:function(response){
                            location.reload();
                        },
                        error:function(response){
                            alert(response.responseJSON);
                        }
                    });
                }

            });

            $('#updateForm').submit(function(e){
                e.preventDefault();

                var amount = $('#wAmount').val();

                amount = parseFloat(amount);

                if(isNaN(amount))
                {
                    alert("Value entered is not a number.");
                    return false;
                }
                else if(amount < {{$min_withdrawal}})
                {
                    alert("Value entered must be greater than £{{$min_withdrawal}}");

                }
                else if(amount > {{$balance}})
                {
                    alert("Value must be less than or equal to your balance with us: £{{$balance}}.");
                }
                else
                {
                    $.ajax({
                        url:"/affiliates/dashboard/finances/balance/update-withdrawal-request",
                        type:"post",
                        data:{
                            amount:amount
                        },
                        success:function(response){
                            location.reload();
                        },
                        error:function(response){
                            alert(response.responseJSON);
                        }
                    });
                }

            });

            $.ajax({
               url:"/affiliates/dashboard/widgets/finance-balance-timeline",
                type:"POST",
                success:function(response){
                    $('#timeline-area').html(response);
                },
                error:function(response){
                    console.log(response);
                }
            });

            $('body').on('click', '#cancel-withdrawal', function(e){
                e.preventDefault();
                var id = $(this).data('id');
                $.ajax({
                    url:"/affiliates/dashboard/finances/balance/cancel-withdrawal-request",
                    type:"POST",
                    data:{
                        id:id
                    },
                    success:function(response){
                        location.reload();
                    },
                    error:function(response){
                        alert(response.responseJSON);
                    }
                })
            })
        });
    </script>

@stop
