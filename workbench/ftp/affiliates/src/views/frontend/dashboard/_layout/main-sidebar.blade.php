<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <!-- Optionally, you can add icons to the links -->
            <li @if($menu == "dashboard"){{'class="active"'}} @endif>
                <a href="/affiliates/dashboard"><i class="fa fa-dashboard"></i><span>{{trans('homepage.affilOverview')}}</span></a>
            </li>

            <li class="treeview @if($menu == "finances"){{'active'}} @endif">
                <a href="#"><i class="fa fa-area-chart"></i> <span>{{trans('homepage.affilFinances')}}</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="/affiliates/dashboard/finances/balance">{{trans('homepage.affilMyBalance')}}</a></li>
                    <li><a href="/affiliates/dashboard/finances/payment-history">{{trans('homepage.affilPaymentHistory')}}</a></li>
                </ul>
            </li>

            <li @if($menu == "sales"){{'class="active"'}} @endif>
                <a href="/affiliates/dashboard/sales"><i class="fa fa-cart-plus"></i><span>{{trans('homepage.affilSales')}}</span></a>
            </li>

            <li @if($menu == "feed"){{'class="active"'}} @endif>
                <a href="/affiliates/dashboard/product-feed"><i class="fa fa-feed"></i><span>{{trans('homepage.affilProductFeed')}}</span></a>
            </li>
        </ul>
        <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>