<footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">

    </div>
    <!-- Default to the left -->
    <strong>{{trans('homepage.affilCopyright')}} &copy; 2016 <a href="/">Football Ticket Pad</a>.</strong> {{trans('homepage.affilAllRightsReserved')}}
</footer>