<header class="main-header">

    <!-- Logo -->
    <a href="/affiliates/dashboard" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini">FTP</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg">Football Ticket Pad</span>
    </a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">{{trans('homepage.affiltoggleNavigation')}}</span>
        </a>
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <!-- User Account Menu -->
                <li class="dropdown user user-menu">
                    <!-- Menu Toggle Button -->
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <!-- hidden-xs hides the username on small devices so only the image appears. -->
                        <i class="fa fa-user"></i><span>{{trans('homepage.affilmyAccount')}}</span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="/affiliates/dashboard/profile" class="btn btn-default btn-flat">{{trans('homepage.affilProfile')}}</a>
                            </div>
                            <div class="pull-right">
                                <a href="#" id="signout-btn" class="btn btn-default btn-flat">{{trans('homepage.affilSignOut')}}</a>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>