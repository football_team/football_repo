<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            @yield('page_title')
            <small>@yield('page_sdesc')</small>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        @yield('page_content')
        <!-- Your Page Content Here -->
    </section>
    <!-- /.content -->
</div>