<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <link rel="stylesheet" href="/assets/adminlte/bootstrap/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="/assets/adminlte/dist/css/AdminLTE.min.css" type="text/css">
    <link rel="stylesheet" href="/assets/adminlte/dist/css/skins/skin-red-light.min.css" type="text/css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="/assets/adminlte/dist/css/skins/_all-skins.min.css">
    <link rel="stylesheet" href="/assets/adminlte/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
    <link rel="stylesheet" href="/assets/adminlte/plugins/daterangepicker/daterangepicker-bs3.css">

    @yield('style')
</head>
<body class="hold-transition skin-blue-light sidebar-mini">
    <div class="wrapper" style="overflow:visible;">
        @include('Affiliates::frontend.dashboard._layout.header')
        @include('Affiliates::frontend.dashboard._layout.main-sidebar')
        @include('Affiliates::frontend.dashboard._layout.content-wrapper')
        @include('Affiliates::frontend.dashboard._layout.footer')
        @include('Affiliates::frontend.dashboard._layout.control-sidebar')
    </div>
</body>

<script src="/assets/adminlte/plugins/jQuery/jQuery-2.2.0.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="/assets/adminlte/bootstrap/js/bootstrap.min.js"></script>
<!-- AdminLTE App -->
<script src="/assets/adminlte/dist/js/app.min.js"></script>
<!-- Sparkline -->
<script src="/assets/adminlte/plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="/assets/adminlte/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="/assets/adminlte/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- SlimScroll 1.3.0 -->
<script src="/assets/adminlte/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- ChartJS 1.0.1 -->
<script src="/assets/adminlte/plugins/chartjs/Chart.min.js"></script>
<!-- Data Range Picker -->
<script src="/assets/adminlte/plugins/daterangepicker/moment.min.js"></script>
<script src="/assets/adminlte/plugins/daterangepicker/daterangepicker.js"></script>



@yield('script')

<script>
    $(function(){
       $('body').on('click', '#signout-btn', function(e){
           e.preventDefault();
           $.ajax({
               url: "/affiliates/logout",
               type: 'POST',
               success:function(response){
                    window.location='/affiliates/login';
               },
               error:function(response){
                    alert("fail");
               }
           })
       });
    });
</script>

<script>$.ajaxSetup({headers:{'csrftoken':'{{csrf_token()}}'}});</script>
</body>
</html>