@extends(Template::name('Affiliates::frontend.dashboard._layout.layout'))

@section('page_title')
    {{trans('homepage.affilProfile')}}
@stop

@section('page_sdesc')

@stop

@section('page_content')
    <div class="row">
        <div class="col-md-6">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-default">
                        <div class="box-header with-border">
                            <i class=""></i>
                            <h3 class="box-title">{{trans('homepage.affilUpdateContactName')}}</h3>
                        </div>
                        <div class="box-body">
                            <form class="form-horizontal" id="updateContact">
                                <div class="box-body">
                                    <div class="form-group">
                                        <label for="cname" class="col-sm-4 control-label">{{trans('homepage.affilCName')}}</label>
                                        <div class="col-sm-8">
                                            <input class="form-control" name="cname" id="contact_name" value="{{$property->contact_name}}" type="text">
                                        </div>
                                    </div>
                                </div>
                                <!-- /.box-body -->
                                <div class="box-footer">
                                    <button type="submit" class="btn btn-info pull-right">{{trans('homepage.affilSubmit')}}</button>
                                </div>
                                <!-- /.box-footer -->
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-default">
                        <div class="box-header with-border">
                            <i class=""></i>
                            <h3 class="box-title">{{trans('homepage.affilUpdatePassword')}}</h3>
                        </div>
                        <div class="box-body">
                            <form class="form-horizontal" id="updatePass">
                                <div class="box-body">
                                    <div class="form-group">
                                        <label for="p1" class="col-sm-4 control-label">{{trans('homepage.affilPassword')}} </label>
                                        <div class="col-sm-8">
                                            <input class="form-control" name="p1" id="p1" value="" type="password">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="p2" class="col-sm-4 control-label">{{trans('homepage.affilConfirmC')}}</label>
                                        <div class="col-sm-8">
                                            <input class="form-control" name="p2" id="p2" value="" type="password">
                                        </div>
                                    </div>
                                </div>
                                <!-- /.box-body -->
                                <div class="box-footer">
                                    <button type="submit" class="btn btn-info pull-right">{{trans('homepage.affilSubmit')}}</button>
                                </div>
                                <!-- /.box-footer -->
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-default">
                        <div class="box-header with-border">
                            <i class=""></i>
                            <h3 class="box-title">{{trans('homepage.affilUPD')}}</h3>
                        </div>
                        <div class="box-body">
                            <form class="form-horizontal" id="updateBank">
                                <div class="box-body">
                                    <div class="form-group">
                                        <label for="p1" class="col-sm-4 control-label">Account Holder Name: </label>
                                        <div class="col-sm-8">
                                            <input class="form-control" name="account_holder_name" id="account_holder_name" value="{{$property->account_holder_name}}" type="text">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="p1" class="col-sm-4 control-label">{{trans('homepage.affilAccountNumber')}} </label>
                                        <div class="col-sm-8">
                                            <input class="form-control" name="account_number" id="account_number" value="{{$property->account_number}}" type="text">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="p2" class="col-sm-4 control-label">{{trans('homepage.affilSortCode')}} </label>
                                        <div class="col-sm-8">
                                            <input class="form-control" name="sort_code" id="sort_code" value="{{$property->sort_code}}" type="text">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="p2" class="col-sm-4 control-label">IBAN</label>
                                        <div class="col-sm-8">
                                            <input class="form-control" name="iban" id="iban" value="{{$property->IBAN}}" type="text">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="p2" class="col-sm-4 control-label">SWIFT</label>
                                        <div class="col-sm-8">
                                            <input class="form-control" name="swift" id="swift" value="{{$property->SWIFT}}" type="text">
                                        </div>
                                    </div>
                                </div>
                                <!-- /.box-body -->
                                <div class="box-footer">
                                    <button type="submit" class="btn btn-info pull-right">{{trans('homepage.affilSubmit')}}</button>
                                </div>
                                <!-- /.box-footer -->
                            </form>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        </div>

    </div>
@stop

@section('style')

@stop

@section('script')
    <script>
        $(function(){
            $('#updateContact').submit(function(e){
                e.preventDefault();

                var name = $('#contact_name').val();
                if(name != "")
                {
                    $.ajax({
                        url:"/affiliates/dashboard/profile/update-contact-name",
                        type:"POST",
                        data:{
                            name:name
                        },
                        success:function(response){
                            location.reload();
                        },
                        error:function(response){
                            console.log(response);
                        }
                    })
                }
                else{
                    alert("Name must not be blank.");
                }

            });

            $('#updatePass').submit(function(e){
                e.preventDefault();

                var p1 = $('#p1').val();
                var p2 = $('#p2').val();

                if(p1 == p2)
                {
                    if(p1 != "" && p1.length >= 8)
                    {
                        p3 = p1.trim();

                        if(p3.length==p1.length)
                        {
                            $.ajax({
                                url:"/affiliates/dashboard/profile/update-password",
                                type:"POST",
                                data:{
                                    p1:p1,
                                    p2:p2
                                },
                                success:function(response){
                                    alert("Password updated.");
                                    location.reload();
                                },
                                error:function(response){
                                    console.log(response);
                                }
                            })
                        }
                        else
                        {
                            alert('password contained leading or trailing spaces.');
                        }
                    }
                    else
                    {
                        alert("password must be at least 8 characters long.");
                    }
                }
                else
                {
                    alert("Passwords do not match.");
                }
            });

            $('#updateBank').submit(function(e){
                e.preventDefault();

                var account_name = $('#account_holder_name').val();
                var account_number = $('#account_number').val();
                var sort_code = $('#sort_code').val();
                var iban = $('#iban').val();
                var swift = $('#swift').val();

                $.ajax({
                   url:"/affiliates/dashboard/profile/update-bank" ,
                    type:"POST",
                    data:{
                        account_holder_name:account_name,
                        account_number:account_number,
                        sort_code:sort_code,
                        iban:iban,
                        swift:swift
                    },
                    success:function(response){
                        alert("Bank details updated");
                        location.reload();
                    },
                    error:function(response){
                        console.log(response);
                        alert(response.responseJSON);
                    }
                });
            });
        });
    </script>
@stop
