@extends(Template::name('Affiliates::frontend.layout'))
@section('content')
    <div class="contains">
        <div class="shade">
            <br><br><br>
            <div class="row midthis">
                <div class="col-md-3 col-sm-1"></div>
                <div class="col-md-6 col-sm-10" style="text-align:center;">
                    <h1 style="color:#fff; font-size:50px;">{{trans('homepage.affilLoginTitle')}}</h1>
                </div>
                <div class="col-md-3 col-sm-1"></div>
            </div>
            <br>
            <div class="action row">
                <form id="regForm" action="/affiliates/signup" class="fields" method="POST">
                    <div class="row">
                        <div class="col-md-3 col-sm-1"></div>
                        <div class="col-md-6 col-sm-10">
                            <input type="text" name="email" placeholder="{{trans('homepage.Email')}}" required>
                        </div>
                        <div class="col-md-3 col-sm-1"></div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 col-sm-1"></div>
                        <div class="col-md-6 col-sm-10">
                            <input type="password" name="password" placeholder="{{trans('homepage.Password')}}" required>
                        </div>
                        <div class="col-md-3 col-sm-1"></div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 col-sm-1"></div>
                        <div class="col-md-6 col-sm-10">
                            <label for="tnc" style="color:#fff">{{trans('homepage.affilRememberMe')}}</label>
                            <input type="checkbox" name="remember_me"><input type="submit" value="{{trans('homepage.affilLogin')}}" style="float:right;">
                        </div>
                        <div class="col-md-3 col-sm-1"></div>
                    </div>
                </form>
            </div>
            <br>
            <div class="row">
                <div class="col-md-3 col-sm-1"></div>
                <div class="col-md-6 col-sm-10">
                    <div id="errors" style="color:red;">

                    </div>
                </div>
                <div class="col-md-3 col-sm-1"></div>
            </div>
        </div>
    </div>


    <script>
        $(document).ready(function(){
            $('#regForm').submit(function(e){
                e.preventDefault();
                var formData = $(this).serialize();

                $.ajax({
                    url:"/affiliates/login",
                    type:"POST",
                    data:formData,
                    success:function(response){
                        //Redirect to dashboard
                        window.location="/affiliates/dashboard";
                    },
                    error:function(response){
                        $('#errors').html(response.responseText);
                    }
                });
            });
        })
    </script>
@stop

@section('style')
    <style>
        body,html{
            height:100vh;
        }
        body{
            background-image:url("/assets/images/afillogin.jpg");
            background-size:cover;
            background-repeat:no-repeat;
            padding-top:0px;
        }
        .shade{
            background-color:rgba(0,0,0,0.6);
            height:100vh;
        }
        .midthis{
            margin-top:20vh;
        }
        .row{
            margin-left:auto !important;
            margin-right:auto !important;
        }
        .fields{
            margin-left:auto !important;
            margin-right:auto !important;
            float: none !important;
        }
        input[type="text"], input[type="password"]{
            border-radius:3px !important;
        }
    </style>
@stop
