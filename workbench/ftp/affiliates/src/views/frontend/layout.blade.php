<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8" />
    <!-- Set the viewport width to device width for mobile -->
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    {{
        Assets::setStyles(
        [
            'foundation'         => 'css/foundation.min.css',
            'main'               => 'css/main.css?v=2',
            'jquery-ui'          => 'css/jquery-ui/jquery-ui.min.css'
        ], false, true);
    }}
            <!-- Included CSS Files (Compressed) -->
    {{ Assets::dumpStyles(Config::get('bondcms.assets_debug')) }}

    @yield('style')

    <link href='//fonts.googleapis.com/css?family=Open+Sans+Condensed:300|Playfair+Display:400italic' rel='stylesheet' type='text/css' />
    <!-- IE Fix for HTML5 Tags -->
    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
</head>

<body class="">
@yield('content')
        <!-- ######################## Scripts ######################## -->


{{
    Assets::setScripts(
    [
        'modernize'       => 'js/modernizr.foundation.js',
        'jquery'          => 'js/jquery.2.0.3.js',
        'foundation'      => 'js/foundation.min.js',
        'foundation54'    => 'js/foundation-5.4/foundation.min.js',
        'jquery-ui'       => 'js/jquery-ui.min.js',
        'app'             => 'js/app.js',
        'stackable'       => 'js/stack/stacktable.js'

    ], false, true);
}}


{{ Assets::dumpScripts(Config::get('bondcms.assets_debug')) }}

@yield('script')
<script type="text/javascript">
    //<![CDATA[
    //    $('ul#menu-header').nav-bar();
    //]]>

    $('.ticket-listing .responsivetable').stacktable();

    $('.collapsible_content .responsivetable').stacktable();

    Foundation.global.namespace = '';
</script>

{{ Assets::dumpOnPageScripts() }}


        <!--Start of Zopim Live Chat Script-->
<!-- Start of ticketpad Zendesk Widget script -->
<script>/*<![CDATA[*/window.zEmbed||function(e,t){var n,o,d,i,s,a=[],r=document.createElement("iframe");window.zEmbed=function(){a.push(arguments)},window.zE=window.zE||window.zEmbed,r.src="javascript:false",r.title="",r.role="presentation",(r.frameElement||r).style.cssText="display: none",d=document.getElementsByTagName("script"),d=d[d.length-1],d.parentNode.insertBefore(r,d),i=r.contentWindow,s=i.document;try{o=s}catch(e){n=document.domain,r.src='javascript:var d=document.open();d.domain="'+n+'";void(0);',o=s}o.open()._l=function(){var o=this.createElement("script");n&&(this.domain=n),o.id="js-iframe-async",o.src=e,this.t=+new Date,this.zendeskHost=t,this.zEQueue=a,this.body.appendChild(o)},o.write('<body onload="document._l();">'),o.close()}("https://assets.zendesk.com/embeddable_framework/main.js","ticketpad.zendesk.com");
    /*]]>*/</script>
<!-- End of ticketpad Zendesk Widget script -->
<script>
    $.ajaxSetup({ headers: { 'csrftoken' : '{{ csrf_token() }}' } });
</script>

</body>
</html>