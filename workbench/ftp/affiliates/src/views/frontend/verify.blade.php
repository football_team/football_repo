@extends(Template::name('Affiliates::frontend.layout'))
@section('content')
    <div class="contains">
        <div class="shade">
            <br><br><br>
            <div class="row midthis">
                <div class="col-md-3 col-sm-1"></div>
                <div class="col-md-6 col-sm-10" style="text-align:center;">
                    <h1 style="color:#fff; font-size:50px;">{{trans('homepage.affilVerifyYourDomain')}}</h1>
                </div>
                <div class="col-md-3 col-sm-1"></div>
            </div>
            <br>
            <div class="action row">
                <form id="verForm" action="/affiliates/verify-account" class="fields" method="POST">
                    <div class="row">
                        <div class="col-md-3 col-sm-1"></div>
                        <div class="col-md-6 col-sm-10">
                            <div class="row">
                                <div class="col-xs-4" style="padding:0px;">
                                    <div class="row">
                                        <select name="protocol" id="" style="border-top-right-radius:0 !important; border-bottom-right-radius:0 !important;" required>
                                            <option value="http">http://</option>
                                            <option value="https">https://</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-xs-8" style="padding-right:0px;">
                                    <div class="row">
                                        <input type="text" name="url" placeholder="{{trans('homepage.affilCSU')}}" style="border-top-left-radius:0 !important; border-bottom-left-radius:0 !important;" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-1"></div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 col-sm-1"></div>
                        <div class="col-md-6 col-sm-10">
                            <input type="submit" value="{{trans('homepage.affilSubmit')}}" style="float:right;">
                        </div>
                        <div class="col-md-3 col-sm-1"></div>
                    </div>
                </form>
            </div>
            <br>
            <div class="row">
                <div class="col-md-3 col-sm-1"></div>
                <div class="col-md-6 col-sm-10">
                    <div id="errors" style="color:red;">

                    </div>
                </div>
                <div class="col-md-3 col-sm-1"></div>
            </div>
        </div>
    </div>


    <script>
        $(document).ready(function(){
            $('#verForm').submit(function(e){
                e.preventDefault();
                var formData = $(this).serialize();

                $.ajax({
                    url:"/affiliates/verify-account",
                    type:"POST",
                    data:formData,
                    success:function(response){
                        $('.action').html('<div class="col-md-3 col-sm-1"></div><div class="col-md-6 col-sm-10" style="color:#fff; text-align:center;">{{trans("homepage.affilregSuccess2")}}</div><div class="col-md-3 col-sm-1"></div>');
                        $('#errors').html("");
                    },
                    error:function(response){
                        $('#errors').html(response.responseText);
                    }
                });
            });
        })
    </script>
@stop

@section('style')
    <style>
        body,html{
            height:100vh;
        }
        body{
            background-image:url("/assets/images/regverify.jpg");
            background-size:cover;
            background-repeat:no-repeat;
            padding-top:0px;
        }
        .shade{
            background-color:rgba(0,0,0,0.6);
            height:100vh;
        }
        .midthis{
            margin-top:20vh;
        }
        .row{
            margin-left:auto !important;
            margin-right:auto !important;
        }
        .fields{
            margin-left:auto !important;
            margin-right:auto !important;
            float: none !important;
        }
        input[type="text"], input[type="password"], .fields select{
            border-radius:3px !important;
        }
    </style>
@stop
