@extends('emails/_layout/layout')
@section('content')
        <!-- body -->
<table class="body-wrap">
    <tr>
        <td></td>
        <td class="container" bgcolor="#FFFFFF">

            <!-- content -->
            <div class="content">
                <a href="https:///www.footballticketpad.com/"><img src="{{ Assets::Path('images/email-logo.png') }}" alt="Football Ticket Padl" /></a>

                <h2>{{trans('homepage.affilWDCE1')}}</h2>
                <p>{{trans('homepage.affilWDEC2')}} {{$contact_name}},</p>

                <p>{{trans('homepage.affilWDEC3')}}</p>
                <p>{{trans('homepage.affilWDEC4')}}</p>

                <p>{{trans('homepage.affilWDEC5')}} {{$reqId}}</p>
                <p>W{{trans('homepage.affilWDEC6')}} {{$amount}}</p>

                <p>{{trans('homepage.affilWDEC7')}} <strong>Football Ticket Pad</strong></p>
            </div>
        </td>
        <td></td>
    </tr>
</table>
<!-- /body -->

@stop