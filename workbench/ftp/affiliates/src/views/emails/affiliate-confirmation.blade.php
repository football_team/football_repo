@extends('emails/_layout/layout')
@section('content')
        <!-- body -->
<table class="body-wrap">
    <tr>
        <td></td>
        <td class="container" bgcolor="#FFFFFF">

            <!-- content -->
            <div class="content">

                <a href="https:///www.footballticketpad.com/"><img src="{{ Assets::Path('images/email-logo.png') }}" alt="Football Ticket Pad" /></a>

                <h2>{{trans('homepage.affilACE1')}}</h2>
                <p>{{trans('homepage.affilACE2')}} {{$contact_name}},</p>

                <p>{{trans('homepage.affilACE3')}}</p>

                <p>{{trans('homepage.affilACE4')}}</p>

                <p>
                    <a href="http://www.footballticketpad.com/affiliates/login">http:///www.footballticketpad.com/affiliates/login</a>
                </p>
                <p>
                    {{trans('homepage.affilACE5')}} {{$email}}<br>
                    {{trans('homepage.affilACE6')}} {{$pass}}
                </p>

                <p>
                    {{trans('homepage.affilACE7')}}
                </p>

                <p>
                    {{trans('homepage.affilACE8')}}
                </p>

                <p>{{trans('homepage.affilACE9')}} <strong>Football Ticket Pad</strong></p>
            </div>
            <!-- /content -->

        </td>
        <td></td>
    </tr>
</table>
<!-- /body -->

@stop