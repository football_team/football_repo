@extends('emails/_layout/layout')
@section('content')
        <!-- body -->
<table class="body-wrap">
    <tr>
        <td></td>
        <td class="container" bgcolor="#FFFFFF">

            <!-- content -->
            <div class="content">
                <a href="https:///www.footballticketpad.com/"><img src="{{ Assets::Path('images/email-logo.png') }}" alt="Football Ticket Pad" /></a>

                <h2>{{trans('homepage.affilWDE1')}}</h2>
                <p>{{trans('homepage.affilWDE2')}} {{$contact_name}},</p>

                <p>{{trans('homepage.affilWDE3')}}</p>

                <p>{{trans('homepage.affilWDE4')}}</p>

                <p>
                    {{trans('homepage.affilWDE5')}} {{$amount}}<br>
                    {{trans('homepage.affilWDE6')}} {{$balance}}
                </p>

                <p>{{trans('homepage.affilWDE7')}} <strong>Football Ticket Pad</strong></p>
            </div>
        </td>
        <td></td>
    </tr>
</table>
<!-- /body -->

@stop