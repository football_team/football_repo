<?php

class AffiliateSale extends Eloquent {

    public $table = 'affiliate_sales';
    public $fillable = ['affiliate_id','order_id', 'commission_perc', 'fees_shown'];
    public $timestamps = false;

    public function __construct() {
        parent::__construct();
    }

    public static function getSalesInDateRange($af_id, $date_from, $date_to)
    {
        $res = DB::table('affiliate_sales')
                ->leftJoin('events_ticket_buy as etb', 'affiliate_sales.order_id', '=', 'etb.order_id')
                ->where('affiliate_sales.affiliate_id', $af_id)
                ->where('etb.created_at', '>=', $date_from)
                ->where('etb.created_at', '<', $date_to)
                ->get();

        return $res;
    }

    public static function dashSalesDateRangeWidget($af_id, $date_from, $date_to)
    {
        //So get sales numbers grouped by month
        //Get totals grouped by month
        $res = DB::table('affiliate_sales')
            ->leftJoin('events_ticket_buy as etb', 'affiliate_sales.order_id', '=', 'etb.order_id')
            ->where('affiliate_sales.affiliate_id', $af_id)
            ->where('etb.created_at', '>=', $date_from)
            ->where('etb.created_at', '<', $date_to)
            ->selectRaw('COUNT(*) as ttl, SUM(amount) as val, YEAR(etb.created_at) as year, MONTH(etb.created_at) as month')
            ->groupBy(DB::raw('YEAR(etb.created_at), MONTH(etb.created_at)'))
            ->get();

        return $res;
    }

    public static function getDashDataRangeFooter($af_id, $date_from, $date_to)
    {
        $res = DB::table('affiliate_sales')
            ->leftJoin('events_ticket_buy as etb', 'affiliate_sales.order_id', '=', 'etb.order_id')
            ->where('affiliate_sales.affiliate_id', $af_id)
            ->where('etb.created_at', '>=', $date_from)
            ->where('etb.created_at', '<', $date_to)
            ->selectRaw('SUM(etb.amount) as sttl, SUM(etb.amount*(affiliate_sales.commission_perc/100)) as scom, AVG(affiliate_sales.commission_perc) as avgcom')
            ->first();

        $clicks = DB::table('referral_hits')
                    ->where('affiliate_id', $af_id)
                    ->where('created_at', '>=', $date_from)
                    ->where('created_at', '<', $date_to)
                    ->selectRaw('COUNT(*) as counted')
                    ->first();

        if($res->sttl == null)
        {
            $res->sttl = 0.00;
        }

        if($res->avgcom == null)
        {
            $res->avgcom = 0;
        }

        if($res->scom == null)
        {
            $res->scom = 0;
        }

        if($clicks)
        {
            $res->clicks = $clicks->counted;
        }
        else
        {
            $res->clicks = 0;
        }

        $res->sttl = number_format($res->sttl,2, '.','');
        $res->avgcom = number_format($res->avgcom,2, '.','');
        $res->scom = number_format($res->scom,2, '.','');
        $prop = AffiliateProperty::where('id', $af_id)->first();
        $res->curcom = $prop->getCurrentRate();

        return $res;
    }

    public static function getDashDailySales($af_id, $today)
    {
        $res = DB::table('affiliate_sales')
            ->leftJoin('events_ticket_buy as etb', 'affiliate_sales.order_id', '=', 'etb.order_id')
            ->where('affiliate_sales.affiliate_id', $af_id)
            ->whereRaw('DATE(etb.created_at) = "' .$today.'"')
            ->selectRaw('COUNT(*) as ttl, SUM(amount) as val, HOUR(etb.created_at) as hour, DATE_FORMAT(etb.created_at, "%H") as hourLZ')
            ->groupBy(DB::raw('HOUR(etb.created_at)'))
            ->get();

        Log::info(var_export($res, true));

        Log::info($af_id);
        Log::info($today);
        return $res;
    }

    public static function getFinanceTimelineData($af_id)
    {
        $res = DB::table('affiliate_sales')
            ->leftJoin('events_ticket_buy as etb', 'affiliate_sales.order_id', '=', 'etb.order_id')
            ->where('affiliate_sales.affiliate_id', $af_id)
            ->selectRaw('COUNT(*) as ttl, YEAR(etb.created_at) as year, MONTH(etb.created_at) as month, ROUND(SUM(etb.amount*(affiliate_sales.commission_perc/100)),2) as scom')
            ->groupBy(DB::raw('YEAR(etb.created_at), MONTH(etb.created_at)'))
            ->orderBy('etb.created_at', 'ASC')
            ->get();

        return $res;
    }

    public static function getRecentOrders($af_id)
    {
        $res = DB::table('affiliate_sales')
            ->leftJoin('events_ticket_buy as etb', 'affiliate_sales.order_id', '=', 'etb.order_id')
            ->leftJoin('events_related_tickets as ert', 'etb.product_id', '=', 'ert.product_id')
            ->leftJoin('events', 'ert.event_id', '=', 'events.id')
            ->where('affiliate_sales.affiliate_id', $af_id)
            ->orderBy('affiliate_sales.order_id', 'DESC')
            ->selectRaw(
                'affiliate_sales.order_id,
                events.title,
                events.datetime,
                etb.created_at,
                etb.amount,
                affiliate_sales.commission_perc,
                etb.amount*(affiliate_sales.commission_perc/100) as com_amount'
            )
            ->take(10)
            ->get();

        foreach($res as $r)
        {

            $r->amount = number_format($r->amount, 2, '.','');
            $r->com_amount = number_format($r->com_amount,2,'.','');
        }



        return $res;
    }

    public static function getAllOrders($af_id, $take, $skip)
    {
        $res = DB::table('affiliate_sales')
            ->leftJoin('events_ticket_buy as etb', 'affiliate_sales.order_id', '=', 'etb.order_id')
            ->leftJoin('events_related_tickets as ert', 'etb.product_id', '=', 'ert.product_id')
            ->leftJoin('events', 'ert.event_id', '=', 'events.id')
            ->where('affiliate_sales.affiliate_id', $af_id)
            ->orderBy('affiliate_sales.order_id', 'DESC')
            ->selectRaw(
                'affiliate_sales.order_id as order_id,
                events.title as title,
                events.datetime as datetime,
                etb.created_at as created_at,
                etb.amount as amount,
                affiliate_sales.commission_perc as commission_perc,
                etb.amount*(affiliate_sales.commission_perc/100) as com_amount,
                etb.referral_url as referral_url'
            )
            ->take($take)
            ->skip($skip)
            ->get();

        return $res;
    }

    public static function getOrderCount($af_id)
    {
        $res = DB::table('affiliate_sales')
            ->where('affiliate_id', $af_id)
            ->selectRaw('
                    COUNT(*) as count
                ')
            ->first();

        return $res;
    }

    public static function getTotalsForSales($af_id)
    {
        $res = DB::table('affiliate_sales')
            ->leftJoin('events_ticket_buy as etb', 'affiliate_sales.order_id', '=', 'etb.order_id')
            ->where('affiliate_sales.affiliate_id',$af_id)
            ->selectRaw('
                    SUM(etb.amount) as ttl,
                    COUNT(*) as num_sales,
                    AVG(affiliate_sales.commission_perc) as average_com,
                    SUM(etb.amount*(affiliate_sales.commission_perc/100)) as commission_total
                ')
            ->first();

        return $res;
    }
}
