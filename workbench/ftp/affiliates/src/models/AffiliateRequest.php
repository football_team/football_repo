<?php

class AffiliateRequest extends Eloquent {

    public $table = 'affiliate_requests';
    public $fillable = ['email', 'company_url', 'contact_name','company_name'];
    public $timestamps = false;

    public function __construct() {
        parent::__construct();
    }

    public function approveRequest(){

    }

    public function denyRequest(){

    }

    private function generateVarCode(){

    }

}
