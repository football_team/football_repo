<?php
/**
 * Created by PhpStorm.
 * User: baglad
 * Date: 31/03/2016
 * Time: 11:44
 */
class AffiliateConfig extends Eloquent
{

    public $table = 'affiliate_config';
    public $fillable = ['key', 'value'];
    public $timestamps = false;

    public function __construct() {
        parent::__construct();
    }
}