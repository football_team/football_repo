<?php

class AffiliateWithdrawal extends Eloquent {

    public $table = 'affiliate_withdrawal_requests';
    public $fillable = ['affiliate_id', 'amount', 'requested_for', 'status'];
    public $timestamps = true;

    public function __construct() {
        parent::__construct();
    }

    public static function getTotals()
    {

        $totalWithdrawal = DB::table('affiliate_withdrawal_requests')
                    ->select(DB::raw('
                        COUNT(*) as numberPending,
                        COALESCE(SUM(amount),0) as amountPending
                    '))
                    ->where('status', "pending")
                    ->first();
        return $totalWithdrawal;
    }

    public static function getPaid($take, $skip)
    {
        $res = DB::table('affiliate_withdrawal_requests as awr')
                ->leftJoin('affiliate_properties as ap', 'awr.affiliate_id', '=', 'ap.id')
                ->where('status', 'paid')
                ->select('awr.id as id',
                    'ap.company_name as company_name',
                    'awr.amount as amount',
                    'awr.updated_at as updated_at',
                    'awr.requested_for as requested_for',
                    'ap.IBAN as IBAN',
                    'ap.SWIFT as SWIFT',
                    'ap.account_holder_name',
                    'ap.sort_code',
                    'ap.account_number')
                ->orderBy('updated_at', 'DESC')
                ->take($take)
                ->skip($skip)
                ->get();

        return $res;
    }
    public static function getCancelled($take, $skip)
    {
        $res = DB::table('affiliate_withdrawal_requests as awr')
            ->leftJoin('affiliate_properties as ap', 'awr.affiliate_id', '=', 'ap.id')
            ->where('status', 'cancelled')
            ->select('awr.id as id',
                'ap.company_name as company_name',
                'awr.amount as amount',
                'awr.updated_at as updated_at',
                'awr.requested_for as requested_for')
            ->orderBy('updated_at', 'DESC')
            ->take($take)
            ->skip($skip)
            ->get();

        return $res;
    }
}