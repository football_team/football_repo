<?php
/**
 * Created by PhpStorm.
 * User: baglad
 * Date: 31/03/2016
 * Time: 17:06
 */
class AffiliatePaymentHistory extends Eloquent {
    public $table = 'affiliate_payment';
    public $fillable = ['affiliate_id', 'amount', 'withdrawal_request_id', 'sort_code', 'account_number', 'IBAN'];
    public $timestamps = true;
}