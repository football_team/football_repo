<?php

/**
 * Created by PhpStorm.
 * User: baglad
 * Date: 29/03/2016
 * Time: 11:53
 */

class AffiliateProperty extends Eloquent {

    public $table = 'affiliate_properties';
    public $fillable = ['company_url', 'contact_name','company_name', 'ver_code', 'ref_code', 'user_id', 'sort_code', 'account_number', 'IBAN', 'account_holder_name', 'SWIFT', 'show_fees'];
    public $timestamps = false;

    public function __construct() {
        parent::__construct();
    }


    public static function genRefCode()
    {
        $flag = 0;
        while(!$flag)
        {
            $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
            $pass = array(); //remember to declare $pass as an array
            $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
            for ($i = 0; $i < 10; $i++) {
                $n = rand(0, $alphaLength);
                $pass[] = $alphabet[$n];
            }
            $pass = implode($pass); //turn the array into a string

            $p = AffiliateProperty::where('ref_code', $pass)->first();

            if(!$p)
            {
                $flag = 1;
                return $pass;
            }
        }
    }

    public function getCurrentRate()
    {
        $thisMonth = new DateTime();

        $sales = DB::table('affiliate_sales')
                    ->leftJoin('events_ticket_buy', 'affiliate_sales.order_id', '=', 'events_ticket_buy.order_id')
                    ->select(DB::raw('COUNT(*) as ttl'))
                    ->whereRaw('MONTH(events_ticket_buy.created_at) = '.$thisMonth->format('m').' and YEAR(events_ticket_buy.created_at) = '.$thisMonth->format('Y'))
                    ->where('affiliate_sales.affiliate_id', $this->id)
                    ->first();


        $rate = DB::table('affiliate_payment_rate')
                        ->leftJoin('affiliate_rate', 'affiliate_payment_rate.rate_id', '=', 'affiliate_rate.rate_id')
                        ->where('affiliate_rate.affiliate_id', $this->id)
                        ->where('affiliate_payment_rate.sale_milestone', '<=', $sales->ttl)
                        ->orderBy('affiliate_payment_rate.sale_milestone', 'DESC')
                        ->first();

        if($rate)
        {
            return $rate->rate_perc;
        }

        return $rate;
    }
}
