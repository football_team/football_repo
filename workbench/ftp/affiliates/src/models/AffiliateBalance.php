<?php
/**
 * Created by PhpStorm.
 * User: baglad
 * Date: 31/03/2016
 * Time: 11:07
 */

class AffiliateBalance extends Eloquent {
    public $table = 'affiliate_balance';
    public $fillable = ['affiliate_id', 'balance'];
    public $timestamps = false;

    public function __construct() {
        parent::__construct();
    }

    public static function getTotals()
    {
        $totals = DB::table('affiliate_balance')
            ->select(DB::raw('
                SUM(balance) as total
            '))
            ->first();

        return $totals;
    }
}