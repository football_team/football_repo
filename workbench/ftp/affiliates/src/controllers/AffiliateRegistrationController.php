<?php
/**
 * Created by PhpStorm.
 * User: baglad
 * Date: 18/03/2016
 * Time: 16:08
 */
class AffiliateRegistrationController extends BaseControllerAF {
    public function index() {
        return View::make('Affiliates::frontend.register');
    }

    public function signUp()
    {
        $input = Input::all();

        if(isset($input['url']) && isset($input['protocol']))
        {
            $ptcol = "";
            switch($input['protocol'])
            {
                case 'http':{
                    $ptcol = "http://";
                    break;
                }
                case 'https':{
                    $ptcol = "https://";
                    break;
                }
                default:{
                    return Response::json('Invalid protocol', 404);
                }


            }

            $input['url'] = $ptcol.$input['url'];
        }
        else
        {
            return Response::json('Company site URL is required.', 404);
        }


        $rules = array(
          'email' => 'required|email',
            'url' => 'required|url',
            'company_name' => 'required',
            'contact_name' => 'required',
            'tnc' => 'required'
        );

        $validator = Validator::make(
            $input,
            $rules
        );

        if(!$validator->fails())
        {
            foreach($input as $val)
            {
                $val = strip_tags($val);
            }

            try
            {
                $user = Sentry::findUserByLogin($input['email']);
                if($user)
                {
                    return Response::json("This email address is already associated with an account.", 404);
                }
            }
            catch(Cartalyst\Sentry\Users\UserNotFoundException $ex)
            {
                //As you were....
            }



            $exR = AffiliateRequest::where('company_url', $input['url'])->where('is_rejected', 0)->first();
            if(!$exR)
            {
                $req = new AffiliateRequest;
                $req->email = $input['email'];
                $req->company_url = $input['url'];
                $req->contact_name = $input['contact_name'];
                $req->company_name = $input['company_name'];
                $req->save();

                /*todo Email primary mail account with affiliate request notification
                 *
                 *
                 */
                return Response::json("Success", 200);
            }
            else
            {
                return Response::json("This domain already has an active affiliate request associated with it.", 404);
            }

        }
        else
        {
            $eString = "";
            $messages =$validator->messages();

            foreach($messages->all() as $message)
            {
                $eString .= $message."<br>";
            }
            return Response::json($eString, 404);
        }
    }

    public function verify()
    {
        return View::make('Affiliates::frontend.verify');
    }

    public function skipVerifyDomain($id)
    {

            $request = AffiliateRequest::where('id', $id)->where('is_rejected', 0)->where('is_approved',0)->where('ver_code', '!=', "")->first();
            if($request)
            {

                $pwGen = $this->randomPassword();

                $group = 0;
                try
                {
                    $group = Sentry::findGroupByName('Affiliate');
                }
                catch(Cartalyst\Sentry\Groups\GroupNotFoundException $ex)
                {
                    return Response::json("Could not find user group.", 404);
                }
                catch(Exception $ex)
                {
                    return Response::json("An error occured", 404);
                }

                $user = 0;

                try
                {
                    $user = Sentry::createUser(array(
                        'email'    => $request->email,
                        'password' => $pwGen,
                        'activated'  => 1,
                    ));


                    $user->addGroup($group);

                    //$user = Sentry::findUserByLogin($request->email);
                    //$user->delete();
                }
                catch(Cartalyst\Sentry\Users\UserExistsException $ex)
                {
                    return Response::json("Username already exists.", 404);
                }
                catch(Exception $ex)
                {
                    return Response::json("An error occured.", 404);
                }

                if(!$user)
                {
                    return Response::json("An error occured.", 404);
                }
                //Add property

                $af_property = new AffiliateProperty();

                $af_property->company_url = $request->company_url;
                $af_property->contact_name = $request->contact_name;
                $af_property->company_name = $request->company_name;
                $af_property->ver_code = $request->ver_code;
                $af_property->ref_code = AffiliateProperty::genRefCode();
                $af_property->user_id = $user->id;
                $af_property->agent_id = $request->agent_id;
                $af_property->save();

                $ab = new AffiliateBalance();
                $ab->affiliate_id = $af_property->id;
                $ab->balance = 0;
                $ab->save();

                $rate = AffiliateConfig::where('key', 'default_rate')->first();
                $rate = $rate->value;

                DB::table('affiliate_rate')
                    ->insert([
                        'affiliate_id'=>$af_property->id,
                        'rate_id' => intval($rate)
                    ]);

                //Email the user.

                $emailTemplate = 'Affiliates::emails.affiliate-confirmation';

                $r2 = $request->toArray();

                $r2['pass'] = $pwGen;

                $request->is_approved = 1;
                $request->save();

                Mail::send($emailTemplate, $r2, function ($message) use($request) {
                    $from = Config::get('mail.from');
                    $message->from($from['address'], $from['name']);
                    $email = Options::getOption('contact_email') == ''? 'support@footballticketpad.com': Options::getOption('contact_email');

                    $message->to($request->email, $request->contact_name )
                        ->bcc($email, 'Football Ticket Pad')
                        ->subject("Affiliate Account Activated");
                });

                return Response::json("Added User.", 200);

            }
            else
            {
                $request = AffiliateRequest::where('id', $id)->where('is_approved', 1)->first();
                if($request)
                {
                    return Response::json("This request has already been approved.", 404);
                }
                return Response::json("Cannot find request to verify.", 404);
            }
    }

    public function verifyDomain()
    {
        $input = Input::all();

        if(isset($input['url']) && isset($input['protocol']))
        {
            $ptcol = "";
            switch($input['protocol'])
            {
                case 'http':{
                    $ptcol = "http://";
                    break;
                }
                case 'https':{
                    $ptcol = "https://";
                    break;
                }
                default:{
                    return Response::json('Invalid protocol', 404);
                }


            }

            $input['url'] = $ptcol.$input['url'];
        }
        else
        {
            return Response::json('Company site URL is required.', 404);
        }


        $rules = array(
            'url' => 'required|url'
        );

        $validator = Validator::make(
            $input,
            $rules
        );

        if(!$validator->fails())
        {
            $request = AffiliateRequest::where('company_url', $input['url'])->where('is_rejected', 0)->where('is_approved',0)->where('ver_code', '!=', "")->first();
            if($request)
            {
                $tag = $this->getMetaTag($request->company_url);
                if($tag && $tag !='')
                {
                    if($tag == $request->ver_code)
                    {
                        $pwGen = $this->randomPassword();

                        $group = 0;
                        try
                        {
                            $group = Sentry::findGroupByName('Affiliate');
                        }
                        catch(Cartalyst\Sentry\Groups\GroupNotFoundException $ex)
                        {
                            return Response::json("Could not find user group.", 404);
                        }
                        catch(Exception $ex)
                        {
                            return Response::json("An error occured", 404);
                        }

                        $user = 0;

                        try
                        {
                            $user = Sentry::createUser(array(
                                'email'    => $request->email,
                                'password' => $pwGen,
                                'activated'  => 1,
                            ));


                            $user->addGroup($group);

                            //$user = Sentry::findUserByLogin($request->email);
                            //$user->delete();
                        }
                        catch(Cartalyst\Sentry\Users\UserExistsException $ex)
                        {
                            return Response::json("Username already exists.", 404);
                        }
                        catch(Exception $ex)
                        {
                            return Response::json("An error occured.", 404);
                        }

                        if(!$user)
                        {
                            return Response::json("An error occured.", 404);
                        }
                        //Add property

                        $af_property = new AffiliateProperty();

                        $af_property->company_url = $request->company_url;
                        $af_property->contact_name = $request->contact_name;
                        $af_property->company_name = $request->company_name;
                        $af_property->ver_code = $request->ver_code;
                        $af_property->ref_code = AffiliateProperty::genRefCode();
                        $af_property->user_id = $user->id;
                        $af_property->agent_id = $request->agent_id;
                        $af_property->save();

                        $ab = new AffiliateBalance();
                        $ab->affiliate_id = $af_property->id;
                        $ab->balance = 0;
                        $ab->save();

                        $rate = AffiliateConfig::where('key', 'default_rate')->first();
                        $rate = $rate->value;

                        DB::table('affiliate_rate')
                                    ->insert([
                                        'affiliate_id'=>$af_property->id,
                                        'rate_id' => intval($rate)
                                    ]);

                        //Email the user.

                        $emailTemplate = 'Affiliates::emails.affiliate-confirmation';

                        $r2 = $request->toArray();

                        $r2['pass'] = $pwGen;

                        $request->is_approved = 1;
                        $request->save();

                        Mail::send($emailTemplate, $r2, function ($message) use($request) {
                            $from = Config::get('mail.from');
                            $message->from($from['address'], $from['name']);
                            $email = Options::getOption('contact_email') == ''? 'birkystomper99@gmail.com': Options::getOption('contact_email');

                            $message->to($request->email, $request->contact_name )
                                ->bcc($email, 'Football Ticket Pad')
                                ->subject("Affiliate Account Activated");
                        });

                        return Response::json("Added User.", 200);
                    }
                    else
                    {
                        return Response::json("Tag on site does not match tag provided.", 404);
                    }
                }
                else
                {
                    return Response::json("Tag not found on site.", 404);
                }

            }
            else
            {
                $request = AffiliateRequest::where('company_url', $input['url'])->where('is_approved', 1)->first();
                if($request)
                {
                    return Response::json("This request has already been approved.", 404);
                }
                return Response::json("Cannot find request to verify.", 404);
            }
        }
        else
        {
            $eString = "";
            $messages =$validator->messages();

            foreach($messages->all() as $message)
            {
                $eString .= $message."<br>";
            }
            return Response::json($eString, 404);
        }
    }

    private function getMetaTag($url)
    {
        $html = $this->file_get_contents_curl($url);
        $doc = new DOMDocument();
        @$doc->loadHTML($html);

        $metas = $doc->getElementsByTagName('meta');


        for ($i = 0; $i < $metas->length; $i++)
        {
            $meta = $metas->item($i);
            if($meta->getAttribute('name') == 'af_network'){
                $tag = $meta->getAttribute('content');
                return $tag;
            }
        }

        return false;
    }

    private function file_get_contents_curl($url)
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

        $data = curl_exec($ch);
        curl_close($ch);

        return $data;
    }

    private function randomPassword() {
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 10; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }
}

