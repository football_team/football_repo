<?php

/**
 * Created by PhpStorm.
 * User: baglad
 * Date: 31/03/2016
 * Time: 15:49
 */
class AffiliateWithdrawalController extends BaseControllerAF
{
    public function addNewWithdrawal()
    {
        $user = Sentry::getUser();
        $property = AffiliateProperty::where('user_id', $user->id)->first();

        $today = new \DateTime;

        $payout_day = AffiliateConfig::where('key','payout_day')->first();
        $payout_day = $payout_day->value;

        $payoutDate = new \DateTime;

        $todays_num = $today->format('d');


        if(!$property)
        {
            return Response::json("Property not found.",404);
        }

        if(!$payout_day)
        {
            return Response::json("System Error.",404);
        }

        if($todays_num <= $payout_day)
        {
            $payoutDate->setDate($today->format('Y'), $today->format('m'), $payout_day);
        }
        else
        {
            $payoutDate->setDate($today->format('Y'), $today->format('m'), $payout_day);
            $payoutDate->modify("+1 month");
        }

        $intoDBDate = $payoutDate->format('Y-m-d');

        if(!Input::has('amount'))
        {
            return Response::json("Requires amount input.",404);
        }

        $amount = Input::get('amount');

        $amount = floatval($amount);
        if(!$amount)
        {
            return Response::json('Amount must be a number',404);
        }

        $minWithdrawal = AffiliateConfig::where('key', 'min_withdrawal')->first();

        $minWithdrawal = floatval($minWithdrawal->value);

        if($amount < $minWithdrawal)
        {
            return Response::json("Amount must be above minimum withdrawal.",404);
        }

        $balance = AffiliateBalance::where('affiliate_id', $property->id)->first();
        $balance = $balance->balance;

        if($amount > $balance)
        {
            return Response::json("Amount must be less than or equal to affiliate balance.", 404);
        }

        $withdrawal = AffiliateWithdrawal::where('affiliate_id', $property->id)->where('requested_for', $intoDBDate)->where('status','pending')->first();

        if($withdrawal)
        {
            return Response::json("A withdrawal request already exists for this account.",404);
        }

        $newWithdrawal = new AffiliateWithdrawal;

        $newWithdrawal->amount = $amount;
        $newWithdrawal->requested_for = $intoDBDate;
        $newWithdrawal->affiliate_id = $property->id;

        $newWithdrawal->save();

        return Response::json("Withdrawal request submitted.", 200);
    }

    public function updateWithdrawal()
    {
        $user = Sentry::getUser();
        $property = AffiliateProperty::where('user_id', $user->id)->first();

        $today = new \DateTime;

        $payout_day = AffiliateConfig::where('key','payout_day')->first();
        $payout_day = $payout_day->value;

        $payoutDate = new \DateTime;

        $todays_num = $today->format('d');


        if(!$property)
        {
            return Response::json("Property not found.",404);
        }

        if(!$payout_day)
        {
            return Response::json("System Error.",404);
        }

        if($todays_num <= $payout_day)
        {
            $payoutDate->setDate($today->format('Y'), $today->format('m'), $payout_day);
        }
        else
        {
            $payoutDate->setDate($today->format('Y'), $today->format('m'), $payout_day);
            $payoutDate->modify("+1 month");
        }

        $intoDBDate = $payoutDate->format('Y-m-d');

        if(!Input::has('amount'))
        {
            return Response::json("Requires amount input.",404);
        }

        $amount = Input::get('amount');

        $amount = floatval($amount);
        if(!$amount)
        {
            return Response::json('Amount must be a number',404);
        }

        $minWithdrawal = AffiliateConfig::where('key', 'min_withdrawal')->first();

        $minWithdrawal = floatval($minWithdrawal->value);

        if($amount < $minWithdrawal)
        {
            return Response::json("Amount must be above minimum withdrawal.",404);
        }

        $balance = AffiliateBalance::where('affiliate_id', $property->id)->first();
        $balance = $balance->balance;

        if($amount > $balance)
        {
            return Response::json("Amount must be less than or equal to affiliate balance.", 404);
        }

        $withdrawal = AffiliateWithdrawal::where('affiliate_id', $property->id)->where('requested_for', $intoDBDate)->where('status', 'pending')->first();

        if(!$withdrawal)
        {
            return Response::json("Specified withdrawal not found!",404);
        }

        $withdrawal->amount = $amount;
        $withdrawal->save();

        return Response::json("Withdrawal request updated.", 200);
    }

    public function cancelWithdrawal(){
        if(Input::has('id'))
        {
            $id = Input::get('id');

            $user = Sentry::getUser();
            $property = AffiliateProperty::where('user_id', $user->id)->first();

            $withdrawal = AffiliateWithdrawal::where('affiliate_id', $property->id)->where('status', 'pending')->first();

            if($withdrawal)
            {
                $withdrawal->delete();
                return Response::json("Requested withdrawal was deleted", 200);
            }
            else
            {
                return Response::json('Could not find a pending request with the provided information', 404);
            }
        }
        else
        {
            return Response::json('Required a withdrawal id', 404);
        }
    }
}