<?php

/**
 * Created by PhpStorm.
 * User: baglad
 * Date: 01/04/2016
 * Time: 11:35
 */
class AffiliateAccountController extends BaseControllerAF
{
    public function index()
    {
        $user = Sentry::getUser();
        $property = AffiliateProperty::where('user_id', $user->id)->first();

        View::share('menu', 'profile');
        View::share('property', $property);

        return View::make('Affiliates::frontend.dashboard.profile');
    }

    public function updateBank()
    {
        $user = Sentry::getUser();
        $property = AffiliateProperty::where('user_id',$user->id)->first();

        if(Input::has('iban') && Input::has('sort_code') && Input::has('account_number') && Input::has('account_holder_name') && Input::has('swift'))
        {
            $iban = Input::get('iban');
            $sort_code = Input::get('sort_code');
            $account_number = Input::get('account_number');
            $account_holder = Input::get('account_holder_name');
            $swift = Input::get('swift');

            if((strlen($sort_code) <= 10) && (strlen($account_number)<=15) && (strlen($iban)<=34) && (strlen($swift) <= 11))
            {
                $property->sort_code = strip_tags($sort_code);
                $property->account_number = strip_tags($account_number);
                $property->IBAN = strip_tags($iban);
                $property->account_holder_name = strip_tags($account_holder);
                $property->SWIFT = strip_tags($swift);

                $property->save();

                return Response::json('success', 200);
            }
            else
            {
                return Response::json('Sort code must be <= 10 characters, account number must be <= 15 characters, IBAN must be 34 or fewer characters and SWIFT must be 11 or fewer characters.', 404);
            }
        }
        else
        {
            return Response::json("All fields required", 404);
        }

    }

    public function updatePassword()
    {
        $user = Sentry::getUser();
        $property = AffiliateProperty::where('user_id',$user->id)->first();

        if(Input::has('p1') && Input::has('p2'))
        {
            $p1 = Input::get('p1');
            $p2 = Input::get('p2');

            if($p1 == $p2)
            {
                if(strlen($p1)>=8)
                {
                    $user->password = $p1;
                    $user->save();

                    return Response::json('Success', 200);
                }
                else
                {
                    return Response::json('Must be at least 8 characters long.');
                }
            }
            else
            {
                return Response::json('Password 1 and 2 required.', 404);
            }
        }
        else
        {
            return Response::json('Password and confirmation required.', 404);
        }
    }

    public function updateContactName()
    {
        $user = Sentry::getUser();
        $property = AffiliateProperty::where('user_id',$user->id)->first();

        if(Input::has('name'))
        {
            $name = Input::get('name');

            $name = strip_tags($name);

            $property->contact_name = $name;
            $property->save();

            return Response::json('Success', 200);
        }
        else
        {
            return Response::json('Name required', 404);
        }
    }

    public function getRate($id)
    {
        $ap = AffiliateProperty::where('id', $id)->first();

        return Response::json($ap->getCurrentRate());
    }

}