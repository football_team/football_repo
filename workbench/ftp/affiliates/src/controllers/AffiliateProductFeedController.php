<?php

/**
 * Created by PhpStorm.
 * User: baglad
 * Date: 05/04/2016
 * Time: 09:59
 */
class AffiliateProductFeedController extends BaseControllerAF
{

    public function gerneralFeed()
    {
        $groupByEvent = 1;
        $orderDir = "ASC";

        if(Input::has('group_by_event'))
        {
            $groupByEvent = intval(Input::get('group_by_event'));
        }

        if(Input::has('order_dir'))
        {
            $orderDir = Input::get('order_dir');
        }


        $orderDir = strtoupper($orderDir);

        if(!in_array($orderDir, ['ASC', 'DESC']))
        {
            return Response::json("Parameter order_dir must equal ASC or DESC once the parameter is made all upper case on our side.",404);
        }

        $results = "";
        if($groupByEvent == 1) {
            $results = DB::table('events_related_tickets as ert')
                ->leftJoin('events as e', 'ert.event_id', '=' , 'e.id')
                ->where('ert.ticket_status', 'active')
                ->where('ert.available_qty', '>', 0)
                ->whereRaw('ert.price = (SELECT MIN(price) FROM events_related_tickets where event_id = e.id and ticket_status = "active" and available_qty > 0)')
                ->groupBy('e.id')
                ->whereRaw('e.datetime > NOW()')
                ->orderBy('e.datetime', $orderDir)
                ->get();
        }
        else
        {
            $results = DB::table('events as e')
                ->leftJoin('events_related_tickets as ert', 'e.id', '=', 'ert.event_id')
                ->whereRaw('e.datetime > NOW()')
                ->where('ert.ticket_status', 'active')
                ->where('ert.available_qty', '>', 0)
                ->orderBy('e.datetime', $orderDir)
                ->get();
        }
        $feed = $this->gerneralGenFeed($results);

        $response = Response::make($feed, 200);
        return $response;
    }

    private function gerneralGenFeed($results)
    {

        $xmlString = '';
        $lcl = App::getLocale();
        $locale = DB::table('locales')
            ->where('lang', $lcl)
            ->first();

        $baseurl = $locale->url;

        $bf = 1 + (intval(Options::getOption('booking_fees'))/100);

        foreach($results as $result)
        {
            $url = FootBallEvent::getUrl($result);
            $tickInfo = json_decode($result->ticket);
            if($tickInfo)
            {
                $ttype = $tickInfo->ticketInformation->ticket_type;
                $block = $tickInfo->ticketInformation->loc_block;

                $row = $tickInfo->ticketInformation->loc_row;
                $typeTicket = DB::table('events_ticket_type')
                    ->where('id', $ttype)
                    ->first();

                $dt = \DateTime::createFromFormat('Y-m-d H:i:s', $result->datetime);

                $typeTicket = $typeTicket->title;

                $xmlString .= "".$result->title." ". $dt->format('Y-m-d') ."|";
                $xmlString .= ""."GBP£|";
                $xmlString .= "".number_format($result->price*$bf, 2, '.', ',').'|';
                $xmlString .="|||||";
                $xmlString .= "".$result->title." ". $dt->format('Y-m-d') ."|";
                $xmlString .= "".$url."|";
                $xmlString .= ""."|||||||";
                $xmlString .= "\n";
            }
        }
        $xmlString .= '';

        return $xmlString;
    }

    public function getFeed($af_code)
    {
        $groupByEvent = 1;
        $orderDir = "ASC";
        $isCSV = false;

        if(Input::has('group_by_event'))
        {
            $groupByEvent = intval(Input::get('group_by_event'));
        }

        if(Input::has('order_dir'))
        {
            $orderDir = Input::get('order_dir');
        }

        if(Input::has('isCSV'))
        {
            $i = Input::get('isCSV');
            if($i == 'TRUE')
            {
                $isCSV = true;
            }
        }

        $af = AffiliateProperty::where('ref_code', $af_code)->first();

        if(!$af)
        {
            return Response::json("Affiliate feed code not recognised.", 404);
        }

        $orderDir = strtoupper($orderDir);

        if(!in_array($orderDir, ['ASC', 'DESC']))
        {
            return Response::json("Parameter order_dir must equal ASC or DESC once the parameter is made all upper case on our side.",404);
        }

        $results = "";
        if($groupByEvent == 1) {
            $results = DB::table('events_related_tickets as ert')
                            ->leftJoin('events as e', 'ert.event_id', '=' , 'e.id')
                            ->where('ert.ticket_status', 'active')
                            ->where('ert.available_qty', '>', 0)
                            ->whereRaw('ert.price = (SELECT MIN(price) FROM events_related_tickets where event_id = e.id and ticket_status = "active" and available_qty > 0)')
                            ->groupBy('e.id')
                            ->whereRaw('e.datetime > NOW()')
                            ->orderBy('e.datetime', $orderDir)
                            ->get();
        }
        else
        {
            $results = DB::table('events as e')
                ->leftJoin('events_related_tickets as ert', 'e.id', '=', 'ert.event_id')
                ->whereRaw('e.datetime > NOW()')
                ->where('ert.ticket_status', 'active')
                ->where('ert.available_qty', '>', 0)
                ->orderBy('e.datetime', $orderDir)
                ->get();
        }

        $feed = '';

        if($isCSV)
        {
            $feed = $this->genFeedCSV($results, $af_code);
            $response = Response::make($feed, 200);
            $response->header('Content-Type', "text/csv");
            return $response;
        }
        else
        {
            $feed = $this->genFeedXML($results, $af_code);
            $response = Response::make($feed, 200);
            $response->header('Content-Type', "text/xml");
            return $response;
        }
    }

    private function genFeedXML($results, $af_code)
    {

        $xmlString = '<?xml version="1.0" encoding="UTF-8"?><productFeed version="1.0">';
        $bc = DB::table('locales')
                ->where('conversion', 1.00)
                ->first();

        $lcl = App::getLocale();
        $locale = DB::table('locales')
                    ->where('lang', $lcl)
                    ->first();

        $bf = 1 + (intval(Options::getOption('booking_fees'))/100);
        $af = AffiliateProperty::where('ref_code', $af_code)->first();

        $baseurl = $locale->url;
        foreach($results as $result)
        {

            $stad = false;

            $event_stad = DB::table('event_stadium')
                            ->where('event_id', $result->event_id)
                            ->first();

            if($event_stad)
            {
                $stad = DB::table('stadium')
                            ->where('id', $event_stad->stadium_id)
                            ->first();
            }
            else
            {
                $team = false;
                if($result->event_in_home)
                {
                    $team = $result->home_team_id;
                }
                else
                {
                    $team = $result->away_team_id;
                }

                $team_stad = DB::table('team_stadium')
                                ->where('team_id', $team)
                                ->first();

                if($team_stad)
                {
                    $stad = DB::table('stadium')
                                ->where('id', $team_stad->stadium_id)
                                ->first();
                }
            }



            $url = FootBallEvent::getUrl($result);
            $tickInfo = json_decode($result->ticket);
            if($tickInfo)
            {
                $ttype = $tickInfo->ticketInformation->ticket_type;
                $block = $tickInfo->ticketInformation->loc_block;

                $row = $tickInfo->ticketInformation->loc_row;
                $typeTicket = DB::table('events_ticket_type')
                    ->where('id', $ttype)
                    ->first();

                $dt = \DateTime::createFromFormat('Y-m-d H:i:s', $result->datetime);

                $typeTicket = $typeTicket->title;

                $xmlString .= '<product>';
                $xmlString .= "<name>".$result->title." ". $dt->format('Y-m-d') ."</name>";
                $xmlString .= "<productURL>".$url."?refCode=".$af_code."</productURL>";
                if($af->show_fees)
                {
                    $xmlString .= "<price>".number_format(($result->price*$bf)*$locale->conversion, 2, '.', ',')."</price>";
                }
                else
                {
                    $xmlString .= "<price>".number_format($result->price*$locale->conversion, 2, '.', ',')."</price>";
                }
                $xmlString .= "<baseCurrency>".$locale->currency."</baseCurrency>";
                $xmlString .= "<date>".$dt->format('Y-m-d')."</date>";
                $xmlString .= "<seat-location>".$typeTicket."</seat-location>";
                $xmlString .= "<block>".$block."</block>";
                $xmlString .= "<row>".$row."</row>";
                $xmlString .= "<eventname>".$result->title."</eventname>";
                if($stad)
                {
                    $country = DB::table('country_list')
                                ->select(DB::raw('`Common Name` as name'))
                                ->where('Sort Order', $stad->country_id)
                                ->first();
                    $xmlString.='<event-location>';
                    if(($stad->title) && ($stad->title != ''))
                    {
                        $xmlString.='<title>';
                        $xmlString.= $stad->title;
                        $xmlString.='</title>';
                    }

                        if(($stad->al1) && ($stad->al1 != ''))
                        {
                            $xmlString.='<line1>';
                            $xmlString.= $stad->al1;
                            $xmlString.='</line1>';
                        }

                        if(($stad->al2) && ($stad->al2 != ''))
                        {
                            $xmlString.='<line2>';
                            $xmlString.= $stad->al2;
                            $xmlString.='</line2>';
                        }

                        if(($stad->postcode) && ($stad->postcode != ''))
                        {
                            $xmlString.='<postcode>';
                            $xmlString.= $stad->postcode;
                            $xmlString.='</postcode>';
                        }

                        if($country)
                        {
                            $xmlString.='<country>';
                            $xmlString.= $country->name;
                            $xmlString.='</country>';
                        }

                    $xmlString.='</event-location>';
                }
                $xmlString .= "</product>";
            }
        }
        $xmlString .= '</productFeed>';

        $xmlString = str_replace('&', '&amp;', $xmlString);

        return $xmlString;
    }

    public function genFeedCSV($results, $af_code)
    {
        $csvString = '';
        $bc = DB::table('locales')
            ->where('conversion', 1.00)
            ->first();

        $lcl = App::getLocale();
        $locale = DB::table('locales')
            ->where('lang', $lcl)
            ->first();

        $bf = 1 + (intval(Options::getOption('booking_fees'))/100);
        $af = AffiliateProperty::where('ref_code', $af_code)->first();

        $baseurl = $locale->url;

        foreach($results as $result)
        {

            $stad = false;

            $event_stad = DB::table('event_stadium')
                ->where('event_id', $result->event_id)
                ->first();

            if($event_stad)
            {
                $stad = DB::table('stadium')
                    ->where('id', $event_stad->stadium_id)
                    ->first();
            }
            else
            {
                $team = false;
                if($result->event_in_home)
                {
                    $team = $result->home_team_id;
                }
                else
                {
                    $team = $result->away_team_id;
                }

                $team_stad = DB::table('team_stadium')
                    ->where('team_id', $team)
                    ->first();

                if($team_stad)
                {
                    $stad = DB::table('stadium')
                        ->where('id', $team_stad->stadium_id)
                        ->first();
                }
            }



            $url = FootBallEvent::getUrl($result);
            $tickInfo = json_decode($result->ticket);
            if($tickInfo)
            {
                $ttype = $tickInfo->ticketInformation->ticket_type;
                $block = $tickInfo->ticketInformation->loc_block;

                $row = $tickInfo->ticketInformation->loc_row;
                $typeTicket = DB::table('events_ticket_type')
                    ->where('id', $ttype)
                    ->first();

                $dt = \DateTime::createFromFormat('Y-m-d H:i:s', $result->datetime);

                $typeTicket = $typeTicket->title;

                $csvString .= '"'.$result->title." ". $dt->format('Y-m-d') .'",';
                $csvString .= '"'.$url."?refCode=".$af_code.'",';
                if($af->show_fees)
                {
                    $csvString .= '"'.number_format(($result->price*$bf)*$locale->conversion, 2, '.', ',').'",';
                }
                else
                {
                    $csvString .= '"'.number_format($result->price*$locale->conversion, 2, '.', ',').'",';
                }
                $csvString .= '"'.$locale->currency.'",';
                $csvString .= '"'.$dt->format('Y-m-d').'",';
                $csvString .= '"'.$typeTicket.'",';
                $csvString .= '"'.$block.'",';
                $csvString .= '"'.$row.'",';
                $csvString .= '"'.$result->title.'",';
                if($stad)
                {
                    $country = DB::table('country_list')
                        ->select(DB::raw('`Common Name` as name'))
                        ->where('Sort Order', $stad->country_id)
                        ->first();

                    if(($stad->title) && ($stad->title != ''))
                    {
                        $csvString.='"';
                        $csvString.= $stad->title;
                        $csvString.='",';
                    }
                    else
                    {
                        $csvString.=',';
                    }

                    if(($stad->al1) && ($stad->al1 != ''))
                    {
                        $csvString.='"';
                        $csvString.= $stad->al1;
                        $csvString.='",';
                    }
                    else
                    {
                        $csvString.=',';
                    }

                    if(($stad->al2) && ($stad->al2 != ''))
                    {
                        $csvString.='"';
                        $csvString.= $stad->al2;
                        $csvString.='",';
                    }
                    else
                    {
                        $csvString.=',';
                    }

                    if(($stad->postcode) && ($stad->postcode != ''))
                    {
                        $csvString.='"';
                        $csvString.= $stad->postcode;
                        $csvString.='",';
                    }
                    else
                    {
                        $csvString.=',';
                    }

                    if($country)
                    {
                        $csvString.='"';
                        $csvString.= $country->name;
                        $csvString.='"';
                    }
                }
                else
                {
                    $csvString.=',,,,';
                }
                $csvString .= "\n";
            }
        }

        $csvString = str_replace('&', '&amp;', $csvString);

        return $csvString;
    }


    public function getSales($af_code){
        $af = AffiliateProperty::where('ref_code', $af_code)->first();
        if($af) {
            if(Input::has('email')) {
                $em = Input::get('email');
                $user = User::where('email', $em)->first();

                if(($user) && ($af->user_id == $user->id)) {
                    //This works.
                    $orderDir = 'DESC';

                    if(Input::has('order_dir')) {
                        $oD = Input::get('order_dir');

                        switch($oD){
                            case 'ASC':
                                $orderDir = 'ASC';
                                break;
                            case 'asc':
                                $orderDir = 'ASC';
                                break;
                            case 'DESC':
                                $orderDir = 'DESC';
                                break;
                            case 'desc':
                                $orderDir = 'DESC';
                                break;

                            default: return Response::json('Invalid order, must be ASC, asc, DESC or desc');
                                break;
                        }
                    }

                    $oId = false;
                    $dt = new DateTime();
                    $df = new DateTime();
                    $df->modify('-1 week');

                    if(Input::has('order_id')) {
                        $oId = Input::get('order_id');
                    }

                    if(Input::has('date_from') && Input::has('date_to')) {
                        $oID = false;
                        $dts = Input::get('date_to');
                        $dfs = Input::get('date_from');

                        try {
                            $dt = DateTime::createFromFormat('d-m-Y', $dts);
                            $df = DateTime::createFromFormat('d-m-Y', $dfs);
                        }
                        catch(Exception $e) {
                            return Response::json('Dates must be in format dd-mm-yyyy',404);
                        }

                        if(!$dt || !$df) {
                            return Response::json('Dates must be in format dd-mm-yyyy',404);
                        }

                        if($dt <= $df)
                        {
                            return Response::json('date_from must be and earlier date than date_to',404);
                        }
                    }

                    $rFeed = false;

                    if($oId){
                        $rFeed = $this->getSpecificOrder($af->id, $oId);

                        if(!$rFeed)
                        {
                            return Response::json('Could not locate this order.',200);
                        }
                    }
                    else {
                        $rFeed = $this->getSalesInRange($af->id, $df, $dt, $orderDir);
                        if ((!$rFeed) || (!count($rFeed))) {
                            return Response::json("No results to show", 200);
                        }
                    }

                    return Response::json($rFeed,200);
                }
            }
        }

        return Response::json("Failed verification",404);
    }

    private function getSpecificOrder($ap_id, $order_id){
        $sale = DB::table('affiliate_sales')
                    ->leftJoin('events_ticket_buy', 'affiliate_sales.order_id', '=', 'events_ticket_buy.order_id')
                    ->leftJoin('events_related_tickets', 'events_ticket_buy.product_id', '=', 'events_related_tickets.product_id')
                    ->leftJoin('events', 'events_related_tickets.event_id', '=', 'events.id')
                    ->where('affiliate_sales.order_id', $order_id)
                    ->where('affiliate_sales.affiliate_id', $ap_id)
                    ->select('affiliate_sales.order_id', DB::raw('FORMAT(events_ticket_buy.amount,2) as sale_total'),
                        'affiliate_sales.commission_perc', 'events_ticket_buy.created_at as sale_datetime', 'events.title',
                        'events.datetime as event_datetime',
                        DB::raw('FORMAT(((events_ticket_buy.amount/100)*affiliate_sales.commission_perc),2) as commission_value'),
                        'affiliate_sales.rid as sid')
                    ->first();

        return $sale;
    }

    private function getSalesInRange($ap_id, $date_from, $date_to, $order){
        $sales = DB::table('affiliate_sales')
            ->leftJoin('events_ticket_buy', 'affiliate_sales.order_id', '=', 'events_ticket_buy.order_id')
            ->leftJoin('events_related_tickets', 'events_ticket_buy.product_id', '=', 'events_related_tickets.product_id')
            ->leftJoin('events', 'events_related_tickets.event_id', '=', 'events.id')
            ->where('events_ticket_buy.created_at', '>=', $date_from->format('Y-m-d'))
            ->where('events_ticket_buy.created_at', '<', $date_to->format('Y-m-d'))
            ->orderBy('events_ticket_buy.created_at', $order)
            ->where('affiliate_sales.affiliate_id', $ap_id)
            ->select('affiliate_sales.order_id',  DB::raw('FORMAT(events_ticket_buy.amount,2) as sale_total'),
                'affiliate_sales.commission_perc', 'events_ticket_buy.created_at as sale_datetime', 'events.title',
                'events.datetime as event_datetime',
                DB::raw('FORMAT(((events_ticket_buy.amount/100)*affiliate_sales.commission_perc),2) as commission_value'),
                'affiliate_sales.rid')
            ->get();

        return $sales;
    }

    // Custom Get Feed Added for RedWeb - 29-11-2017
    public function getFeedRedWeb($af_code)
    {
        $groupByEvent = 1;
        $orderDir = "ASC";
        $isCSV = false;

        if(Input::has('group_by_event'))
        {
            $groupByEvent = intval(Input::get('group_by_event'));
        }

        if(Input::has('order_dir'))
        {
            $orderDir = Input::get('order_dir');
        }

        if(Input::has('isCSV'))
        {
            $i = Input::get('isCSV');
            if($i == 'TRUE')
            {
                $isCSV = true;
            }
        }

        $af = AffiliateProperty::where('ref_code', $af_code)->first();

        if(!$af)
        {
            return Response::json("Affiliate feed code not recognised.", 404);
        }

        $orderDir = strtoupper($orderDir);

        if(!in_array($orderDir, ['ASC', 'DESC']))
        {
            return Response::json("Parameter order_dir must equal ASC or DESC once the parameter is made all upper case on our side.",404);
        }

        $results = "";
        if($groupByEvent == 1) {
            $results = DB::table('events_related_tickets as ert')
                            ->select(DB::raw('ert.*, e.*, homeft.title as homeTeam, awayft.title as awayTeam'))
                            ->leftJoin('events as e', 'ert.event_id', '=' , 'e.id')
                            ->leftJoin('football_ticket as homeft', 'e.home_team_id', '=' , 'homeft.id')
                            ->leftJoin('football_ticket as awayft', 'e.away_team_id', '=' , 'awayft.id')
                            ->where('ert.ticket_status', 'active')
                            ->where('ert.available_qty', '>', 0)
                            ->whereRaw('ert.price = (SELECT MIN(price) FROM events_related_tickets where event_id = e.id and ticket_status = "active" and available_qty > 0)')
                            ->groupBy('e.id')
                            ->whereRaw('e.datetime > NOW()')
                            ->orderBy('e.datetime', $orderDir)
                            ->get();
        }
        else
        {
            $results = DB::table('events as e')
                ->select(DB::raw('ert.*, e.*, homeft.title as homeTeam, awayft.title as awayTeam'))
                ->leftJoin('events_related_tickets as ert', 'e.id', '=', 'ert.event_id')
                ->leftJoin('football_ticket as homeft', 'e.home_team_id', '=' , 'homeft.id')
                ->leftJoin('football_ticket as awayft', 'e.away_team_id', '=' , 'awayft.id')
                ->whereRaw('e.datetime > NOW()')
                ->where('ert.ticket_status', 'active')
                ->where('ert.available_qty', '>', 0)
                ->orderBy('e.datetime', $orderDir)
                ->get();
        }

        $feed = '';

        if($isCSV)
        {
            $feed = $this->genFeedCSVRedWeb($results, $af_code);
            $response = Response::make($feed, 200);
            $response->header('Content-Type', "text/csv");
            return $response;
        }
        else
        {
            $feed = $this->genFeedXMLRedWeb($results, $af_code);
            $response = Response::make($feed, 200);
            $response->header('Content-Type', "text/xml");
            return $response;
        }
    }

    private function genFeedXMLRedWeb($results, $af_code)
    {

        $xmlString = '<?xml version="1.0" encoding="UTF-8"?><productFeed version="1.0">';
        $bc = DB::table('locales')
                ->where('conversion', 1.00)
                ->first();

        $lcl = App::getLocale();
        $locale = DB::table('locales')
                    ->where('lang', $lcl)
                    ->first();

        $bf = 1 + (intval(Options::getOption('booking_fees'))/100);
        $af = AffiliateProperty::where('ref_code', $af_code)->first();

        $baseurl = $locale->url;
        foreach($results as $result)
        {

            $stad = false;

            $event_stad = DB::table('event_stadium')
                            ->where('event_id', $result->event_id)
                            ->first();

            if($event_stad)
            {
                $stad = DB::table('stadium')
                            ->where('id', $event_stad->stadium_id)
                            ->first();
            }
            else
            {
                $team = false;
                if($result->event_in_home)
                {
                    $team = $result->home_team_id;
                }
                else
                {
                    $team = $result->away_team_id;
                }

                $team_stad = DB::table('team_stadium')
                                ->where('team_id', $team)
                                ->first();

                if($team_stad)
                {
                    $stad = DB::table('stadium')
                                ->where('id', $team_stad->stadium_id)
                                ->first();
                }
            }



            $url = FootBallEvent::getUrl($result);
            $tickInfo = json_decode($result->ticket);
            if($tickInfo)
            {
                $ttype = $tickInfo->ticketInformation->ticket_type;
                $block = $tickInfo->ticketInformation->loc_block;

                $row = $tickInfo->ticketInformation->loc_row;
                $typeTicket = DB::table('events_ticket_type')
                    ->where('id', $ttype)
                    ->first();

                $dt = \DateTime::createFromFormat('Y-m-d H:i:s', $result->datetime);

                $typeTicket = $typeTicket->title;

                $xmlString .= '<product>';
                $xmlString .= "<name>".$result->title." ". $dt->format('Y-m-d') ."</name>";
                $xmlString .= "<productURL>".$url."?refCode=".$af_code."</productURL>";
                if($af->show_fees)
                {
                    $xmlString .= "<price>".number_format(($result->price*$bf)*$locale->conversion, 2, '.', ',')."</price>";
                }
                else
                {
                    $xmlString .= "<price>".number_format($result->price*$locale->conversion, 2, '.', ',')."</price>";
                }
                $xmlString .= "<baseCurrency>".$locale->currency."</baseCurrency>";
                $xmlString .= "<date>".$dt->format('Y-m-d')."</date>";
                $xmlString .= "<seat-location>".$typeTicket."</seat-location>";
                $xmlString .= "<block>".$block."</block>";
                $xmlString .= "<row>".$row."</row>";
                $xmlString .= "<eventname>".$result->title."</eventname>";
                $xmlString .= "<hometeam>".$result->homeTeam."</hometeam>";
                $xmlString .= "<awayteam>".$result->awayTeam."</awayteam>";
                if($stad)
                {
                    $country = DB::table('country_list')
                                ->select(DB::raw('`Common Name` as name'))
                                ->where('Sort Order', $stad->country_id)
                                ->first();
                    $xmlString.='<event-location>';
                    if(($stad->title) && ($stad->title != ''))
                    {
                        $xmlString.='<title>';
                        $xmlString.= $stad->title;
                        $xmlString.='</title>';
                    }

                        if(($stad->al1) && ($stad->al1 != ''))
                        {
                            $xmlString.='<line1>';
                            $xmlString.= $stad->al1;
                            $xmlString.='</line1>';
                        }

                        if(($stad->al2) && ($stad->al2 != ''))
                        {
                            $xmlString.='<line2>';
                            $xmlString.= $stad->al2;
                            $xmlString.='</line2>';
                        }

                        if(($stad->postcode) && ($stad->postcode != ''))
                        {
                            $xmlString.='<postcode>';
                            $xmlString.= $stad->postcode;
                            $xmlString.='</postcode>';
                        }

                        if($country)
                        {
                            $xmlString.='<country>';
                            $xmlString.= $country->name;
                            $xmlString.='</country>';
                        }

                    $xmlString.='</event-location>';
                }
                $xmlString .= "</product>";
            }
        }
        $xmlString .= '</productFeed>';

        $xmlString = str_replace('&', '&amp;', $xmlString);

        return $xmlString;
    }

    public function genFeedCSVRedWeb($results, $af_code)
    {
        $csvString = '';
        $csvString = "event-id,name,productURL,price,baseCurrency,date,seat-location,block,row,eventname,hometeam,awayteam,event-location-title,event-location-line1,event-location-2,event-location-postcode,event-location-country\n";
        $bc = DB::table('locales')
            ->where('conversion', 1.00)
            ->first();

        $lcl = App::getLocale();
        $locale = DB::table('locales')
            ->where('lang', $lcl)
            ->first();

        $bf = 1 + (intval(Options::getOption('booking_fees'))/100);
        $af = AffiliateProperty::where('ref_code', $af_code)->first();

        $baseurl = $locale->url;

        foreach($results as $result)
        {

            $stad = false;

            $event_stad = DB::table('event_stadium')
                ->where('event_id', $result->event_id)
                ->first();

            if($event_stad)
            {
                $stad = DB::table('stadium')
                    ->where('id', $event_stad->stadium_id)
                    ->first();
            }
            else
            {
                $team = false;
                if($result->event_in_home)
                {
                    $team = $result->home_team_id;
                }
                else
                {
                    $team = $result->away_team_id;
                }

                $team_stad = DB::table('team_stadium')
                    ->where('team_id', $team)
                    ->first();

                if($team_stad)
                {
                    $stad = DB::table('stadium')
                        ->where('id', $team_stad->stadium_id)
                        ->first();
                }
            }



            $url = FootBallEvent::getUrl($result);
            $tickInfo = json_decode($result->ticket);
            if($tickInfo)
            {
                $ttype = $tickInfo->ticketInformation->ticket_type;
                $block = $tickInfo->ticketInformation->loc_block;

                $row = $tickInfo->ticketInformation->loc_row;
                $typeTicket = DB::table('events_ticket_type')
                    ->where('id', $ttype)
                    ->first();

                $dt = \DateTime::createFromFormat('Y-m-d H:i:s', $result->datetime);

                $typeTicket = $typeTicket->title;

                $csvString .= '"'.$result->event_id.'",';
                $csvString .= '"'.$result->title." ". $dt->format('Y-m-d') .'",';
                $csvString .= '"'.$url."?refCode=".$af_code.'",';
                if($af->show_fees)
                {
                    $csvString .= '"'.number_format(($result->price*$bf)*$locale->conversion, 2, '.', ',').'",';
                }
                else
                {
                    $csvString .= '"'.number_format($result->price*$locale->conversion, 2, '.', ',').'",';
                }
                $csvString .= '"'.$locale->currency.'",';
                $csvString .= '"'.$dt->format('Y-m-d').'",';
                $csvString .= '"'.$typeTicket.'",';
                $csvString .= '"'.$block.'",';
                $csvString .= '"'.$row.'",';
                $csvString .= '"'.$result->title.'",';
                $csvString .= '"'.$result->homeTeam.'",';
                $csvString .= '"'.$result->awayTeam.'",';
                if($stad)
                {
                    $country = DB::table('country_list')
                        ->select(DB::raw('`Common Name` as name'))
                        ->where('Sort Order', $stad->country_id)
                        ->first();

                    if(($stad->title) && ($stad->title != ''))
                    {
                        $csvString.='"';
                        $csvString.= $stad->title;
                        $csvString.='",';
                    }
                    else
                    {
                        $csvString.=',';
                    }

                    if(($stad->al1) && ($stad->al1 != ''))
                    {
                        $csvString.='"';
                        $csvString.= $stad->al1;
                        $csvString.='",';
                    }
                    else
                    {
                        $csvString.=',';
                    }

                    if(($stad->al2) && ($stad->al2 != ''))
                    {
                        $csvString.='"';
                        $csvString.= $stad->al2;
                        $csvString.='",';
                    }
                    else
                    {
                        $csvString.=',';
                    }

                    if(($stad->postcode) && ($stad->postcode != ''))
                    {
                        $csvString.='"';
                        $csvString.= $stad->postcode;
                        $csvString.='",';
                    }
                    else
                    {
                        $csvString.=',';
                    }

                    if($country)
                    {
                        $csvString.='"';
                        $csvString.= $country->name;
                        $csvString.='"';
                    }
                }
                else
                {
                    $csvString.=',,,,';
                }
                $csvString .= "\n";
            }
        }

        $csvString = str_replace('&', '&amp;', $csvString);

        return $csvString;
    }
}