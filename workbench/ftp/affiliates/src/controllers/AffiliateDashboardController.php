<?php

/**
 * Created by PhpStorm.
 * User: baglad
 * Date: 24/03/2016
 * Time: 15:04
 */
class AffiliateDashboardController extends BaseControllerAF
{
    public function index()
    {
        View::share('menu', "dashboard");

        $today = new \DateTime;
        $tString = $today->format('d/m/Y');
        $from = $today->sub(new DateInterval('P6M'));
        $fString = $from->format('d/m/Y');

        $conStr = $fString.' - '.$tString;
        View::share('sales_range', $conStr);
        View::share('today', $tString);

        return View::make('Affiliates::frontend.dashboard.index');
    }

    public function financesBalance()
    {
        View::share('menu', "finances");

        $user = Sentry::getUser();

        $today = new \DateTime;
        $tString = $today->format('d/m/Y');
        $from = $today->sub(new DateInterval('P6M'));
        $fString = $from->format('d/m/Y');


        $conStr = $fString.' - '.$tString;
        View::share('date_range', $conStr);

        $property = AffiliateProperty::where('user_id', $user->id)->first();
        $balance = AffiliateBalance::where('affiliate_id', $property->id)->first();

        $acc_name = $property->account_holder_name;
        $sort_code = $property->sort_code;
        $acc_number = $property->account_number;
        $iban = $property->IBAN;
        $swift = $property->SWIFT;

        $allowed = 1;

        if($acc_name == "" || $sort_code == "" || $acc_number == "" || $iban == "" || $swift == "")
        {
            $allowed = 0;
        }

        View::share('allowed', $allowed);

        View::share('balance', $balance->balance);

        $min = AffiliateConfig::where('key', 'min_withdrawal')->first();
        $min = floatval($min->value);
        View::share('min_withdrawal', $min);

        $today = new \DateTime;
        $number_of_days = cal_days_in_month(CAL_GREGORIAN, $today->format('m'), $today->format('Y'));
        $todays_num = $today->format('d');

        $payout_date = AffiliateConfig::where('key', 'payout_day')->first();
        $payout_date = intval($payout_date->value);
        $days_till = 0;
        $payoutDate = new \DateTime;

        if($todays_num <= $payout_date)
        {
            $days_till = $payout_date - $todays_num;
            $payoutDate->setDate($today->format('Y'), $today->format('m'), $payout_date);
        }
        else
        {
            $days_till = ($number_of_days-$todays_num)+$payout_date;
            $payoutDate->setDate($today->format('Y'), $today->format('m'), $payout_date);
            $payoutDate->modify("+1 month");
        }

        $withdrawal = AffiliateWithdrawal::where('affiliate_id', $property->id)
            ->where('requested_for', $payoutDate->format('Y-m-d'))
            ->where('status', "pending")
            ->first();

        $curCom = $property->getCurrentRate();

        $rateName = DB::table('affiliate_payment_rate as apr')
                    ->leftJoin('affiliate_rate as ar', 'apr.rate_id', '=', 'ar.rate_id')
                    ->where('ar.affiliate_id', $property->id)
                    ->first();

        View::share('curCom', $curCom);
        View::share('rateName', $rateName->rate_name);

        View::share('nDTotal', $number_of_days);
        View::share('dToday', $days_till);
        View::share('payoutDate', $payoutDate->format('Y-m-d'));

        View::share('withdrawal', $withdrawal);

        return View::make('Affiliates::frontend.dashboard.finances-balance');
    }

    public function financesPaymentHistory(){
        View::share('menu', "finances");
        $user = Sentry::getUser();

        $property = AffiliateProperty::where('user_id', $user->id)->first();

        return View::make('Affiliates::frontend.dashboard.finances-payment-history');
    }

    public function productFeed(){
        View::share('menu', "product-feed");
        $user = Sentry::getUser();

        $property = AffiliateProperty::where('user_id', $user->id)->first();

        $dom = Config::get('app.url');

        View::share('domain', $dom);

        View::share('ref_code', $property->ref_code);

        $locale = App::getLocale();
        $lang = DB::table('locales')
                ->where('lang', $locale)
                ->first();

        View::share('url', $lang->url);

        return View::make('Affiliates::frontend.dashboard.product-feed');
    }

    public function sales()
    {
        View::share('menu', "sales");
        return View::make('Affiliates::frontend.dashboard.sales');
    }
}