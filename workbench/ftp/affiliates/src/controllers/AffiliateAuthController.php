<?php
/**
 * Created by PhpStorm.
 * User: baglad
 * Date: 24/03/2016
 * Time: 11:41
 */

class AffiliateAuthController extends BaseControllerAF
{

    public function login()
    {
        return View::make('Affiliates::frontend.login');
    }

    public function postLogin()
    {
        if(Input::has('email') && Input::has('password'))
        {
            $credentials = array(
                'email'    => Input::get('email'),
                'password' => Input::get('password')
            );

            $rememberMe = Input::get('rememberMe');

            try {
                if(!empty($rememberMe))
                {
                    $this->user = Sentry::authenticate($credentials, true);
                }
                else
                {
                    $this->user = Sentry::authenticate($credentials, false);
                }

                if ($this->user) {
                    $flag = 0;
                    $this->user->getGroups();
                    foreach($this->user->groups as $group)
                    {
                        if($group->name == 'Affiliate')
                        {
                            $flag = 1;
                            break;
                        }
                    }
                    if(!$flag)
                    {
                        return Response::json('This is not an affiliate account.', 404);
                    }

                    return Response::json('success', 200);
                }
            }
            catch (\Exception $e) {
                return Response::json($e->getMessage(), 404);
            }
        }
        else
        {
            return Reponse::json("An email and password are required to login.", 404);
        }
    }

    public function auth()
    {
        if (!Sentry::check()) {
            return Redirect::route('affiliate.login');
        }
        else
        {
            $user = Sentry::getUser();
            $user->getGroups();

            $flag = 0;
            foreach($user->groups as $group)
            {
                if($group->name == 'Affiliate')
                {
                    $flag = 1;
                    break;
                }
            }
            if(!$flag)
            {
                return Redirect::route('affiliate.login');
            }
        }
    }

    public function postLogout()
    {
        Sentry::logout();
        return Response::json('success', 200);
    }
}