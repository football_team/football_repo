<?php

/**
 * Created by PhpStorm.
 * User: baglad
 * Date: 29/03/2016
 * Time: 17:17
 */
class WidgetController extends BaseControllerAF
{

    public function getDashDateRangeSales()
    {
        $user = Sentry::getUser();
        if(!$user)
        {
            return Response::json("Invalid User", 404);
        }
        //Get date range
        if(!Input::has('range'))
        {
            return Response::json("Requires a date range.", 404);
        }

        $range = Input::get('range');

        $property = AffiliateProperty::where('user_id', $user->id)->first();
        //Got to validate the input, just a date range.
        $dArray = explode('-', $range);


        if(count($dArray)!=2)
        {
            return Response::json("Invalid date range.", 404);
        }

        $from = trim($dArray[0]);
        $to = trim($dArray[1]);

        $fromDObj = \DateTime::createFromFormat('d/m/Y', $from);
        $toDObj = \DateTime::createFromFormat('d/m/Y', $to);

        if((!$fromDObj) && (!$toDObj))
        {
            return Response::json("Invalid dates entered.",404);
        }

        $af_sales = AffiliateSale::dashSalesDateRangeWidget($property->id, $fromDObj->format('Y-m-d'), $toDObj->format('Y-m-d'));

        foreach($af_sales as $sale)
        {
            $sale->title = $sale->month."/".$sale->year;
        }

        return Response::json($af_sales, 200);
    }

    public function getDashDateRangeFooter()
    {
        $user = Sentry::getUser();
        if(!$user)
        {
            return Response::json("Invalid User", 404);
        }
        //Get date range
        if(!Input::has('range'))
        {
            return Response::json("Requires a date range.", 404);
        }

        $range = Input::get('range');

        $property = AffiliateProperty::where('user_id', $user->id)->first();
        //Got to validate the input, just a date range.
        $dArray = explode('-', $range);

        if(count($dArray)!=2)
        {
            return Response::json("Invalid date range.", 404);
        }

        $from = trim($dArray[0]);
        $to = trim($dArray[1]);

        $fromDObj = \DateTime::createFromFormat('d/m/Y', $from);
        $toDObj = \DateTime::createFromFormat('d/m/Y', $to);

        if((!$fromDObj) && (!$toDObj))
        {
            return Response::json("Invalid dates entered.",404);
        }

        $sales_figs = AffiliateSale::getDashDataRangeFooter($property->id, $fromDObj->format('Y-m-d'), $toDObj->format('Y-m-d'));

        return Response::json($sales_figs, 200);
    }

    public function getDashDailyChartSales()
    {
        //So, return 24 points of data obviously
        //All 24, populated of unpopulated are entered onto the dash
        $user = Sentry::getUser();
        if(!$user)
        {
            return Response::json("Invalid user", 404);
        }

        $dt = new DateTime();
        $dt = $dt->format('Y-m-d');

        $property = AffiliateProperty::where('user_id', $user->id)->first();

        $sales = AffiliateSale::getDashDailySales($property->id, $dt);

        $hoursOut = array();
        for($i = 0; $i < 24; $i++)
        {
            $hoursOut[$i] = array('ttl'=>0, 'val'=>0,'hour'=>$i, 'hourLZ'=>str_pad(strval($i),2,'0', STR_PAD_LEFT).':00');
        }

        foreach($sales as $hour)
        {
            $hoursOut[$hour->hour] = array('ttl'=>$hour->ttl, 'val'=>$hour->val, 'hour'=>$hour->hour, 'hourLZ'=>$hour->hourLZ.":00");
        }

        return Response::json($hoursOut, 200);
    }

    public function getDashRecentOrders(){
        $user = Sentry::getUser();
        if(!$user)
        {
            return Response::json("Invalid User", 404);
        }

        $property = AffiliateProperty::where('user_id', $user->id)->first();

        $recentSales = AffiliateSale::getRecentOrders($property->id);

        return Response::json($recentSales, 200);
    }


    public function getFinanceBalanceTimeline()
    {
        $user = Sentry::getUser();
        if(!$user)
        {
            return Response::json("Invalid User", 404);
        }

        $property = AffiliateProperty::where('user_id', $user->id)->first();

        //So, I need to get the payments from AffiliatePaymentHistory

        $payments = AffiliatePaymentHistory::where('affiliate_id', $property->id)->get();
        $sales = AffiliateSale::getFinanceTimelineData($property->id);


        //Create the ordered array with the sales and payments ordered by date.

        $timeline_elements = array();
        $key = new \DateTime();
        $key = $key->setDate($key->format('Y'), $key->format('m'), 1);

        for($i=0; $i<6; $i++)
        {
            $year = $key->format('Y');
            $month = $key->format('n');
            $ym = $key->format('Y-m');



            foreach($sales as $sale)
            {
                if(($sale->year == $year)&&($sale->month == $month))
                {
                    $sale_element = [];
                    $sale_element[] = 'Sales';
                    $sale_element[] = $year.'/'.$month;
                    $sale_element[] = $sale->ttl;
                    $sale_element[] = number_format($sale->scom,2,'.','');
                    $timeline_elements[] = $sale_element;

                }
            }

            foreach($payments as $payment)
            {
                $date = \DateTime::createFromFormat('Y-m-d H:i:s', $payment->created_at);
                if(($date->format('Y') == $year)&&($date->format('n') == $month))
                {

                    $payment_element = [];
                    $payment_element[] = 'Payment';
                    $payment_element[] = $year.'/'.$month;
                    $payment_element[] = $payment->amount;

                    $timeline_elements[] = $payment_element;
                }
            }
            $key->modify("-1 month");
        }

        View::share('timeline_elements', $timeline_elements);

        return View::make('Affiliates::frontend.dashboard.widgets.finance-timeline');

    }

    public function paymentHistoryDataTable()
    {
        $user = Sentry::getUser();
        if(!$user)
        {
            return Response::json("Invalid User", 404);
        }

        $property = AffiliateProperty::where('user_id', $user->id)->first();

        $start = 0;
        $limit = 10;
        if(Input::has('start'))
        {
            $start = Input::get('start');
        }
        if(Input::has('length'))
        {
            $limit = Input::get('length');
        }

        //So get the payment requests, left join the properties, get the current balance.

        $output = [];
        $total = 0;

        $withdrawals = AffiliateWithdrawal::where('affiliate_id', $property->id)
            ->orderBy('updated_at', 'DESC')
            ->take($limit)
            ->skip($start)
            ->get();

        $data = [];
        $total = 0;

        foreach($withdrawals as $withdrawal)
        {
            $temp = [];
            $temp['0'] = $withdrawal->id;
            $temp['1'] = $withdrawal->requested_for;
            $temp['2'] = $withdrawal->amount;
            $temp['3'] = $withdrawal->status;
            $temp['4'] = $withdrawal->updated_at->format('Y-m-d h:i:s');

            $data[] = $temp;
            $total++;
        }

        $results['data'] = $data;
        $results['recordsTotal'] = $total;
        $results['recordsFiltered'] = $total;
        $response = Response::make($results);
        $response->header('Content-Type', 'application/json');
        return $response;
    }

    public function affiliateSalesDT()
    {
        $user = Sentry::getUser();
        if(!$user)
        {
            return Response::json("Invalid User", 404);
        }

        $property = AffiliateProperty::where('user_id', $user->id)->first();

        $start = 0;
        $limit = 10;
        if(Input::has('start'))
        {
            $start = Input::get('start');
        }
        if(Input::has('length'))
        {
            $limit = Input::get('length');
        }

        //So get the payment requests, left join the properties, get the current balance.

        $data = [];
        $total = 0;
        $all = AffiliateSale::getOrderCount($property->id);

        $sales = AffiliateSale::getAllOrders($property->id, $limit, $start);

        foreach($sales as $sale)
        {
            $temp = [];
            $temp['0'] = $sale->order_id;
            $temp['1'] = $sale->title." ".$sale->datetime;
            $temp['2'] = $sale->created_at;
            $temp['3'] = '£'.number_format($sale->amount,2);
            $temp['4'] = $sale->commission_perc;
            $temp['5'] = '£'.number_format($sale->com_amount,2);
            if(strlen($sale->referral_url) > 40)
            {
                $sale->referral_url = '<div class="hidden-tcell">'.$sale->referral_url.'</div>';
            }
            $temp['6'] = $sale->referral_url;

            $data[] = $temp;
            $total++;
        }

        $results['data'] = $data;
        $results['recordsTotal'] = $all->count;
        $results['recordsFiltered'] = $all->count;
        $response = Response::make($results);
        $response->header('Content-Type', 'application/json');
        return $response;
    }

    public function affiliateSalesTotals()
    {
        $user = Sentry::getUser();
        if(!$user)
        {
            return Response::json("Invalid User", 404);
        }

        $property = AffiliateProperty::where('user_id', $user->id)->first();


        //So get the payment requests, left join the properties, get the current balance.
        $data = [];
        $total = 1;

        $temp = [];

        $res = AffiliateSale::getTotalsForSales($property->id);

        $temp = [];
        $temp['0'] = $res->num_sales;
        $temp['1'] = '£'.number_format($res->ttl,2);
        $temp['2'] = number_format($res->average_com,2);
        $temp['3'] = '£'.number_format($res->commission_total,2);

        $data[] = $temp;

        $results['data'] = $data;
        $results['recordsTotal'] = $total;
        $results['recordsFiltered'] = $total;
        $response = Response::make($results);
        $response->header('Content-Type', 'application/json');
        return $response;
    }
}