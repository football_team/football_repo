<?php
/**
 * Created by PhpStorm.
 * User: baglad
 * Date: 24/03/2016
 * Time: 14:50
 */

Route::filter('auth.affiliate', function () {
    if (!Sentry::check()) {
        return Redirect::route('affiliate.login');
    }
    else
    {
        $user = Sentry::getUser();
        $user->getGroups();

        $flag = 0;
        foreach($user->groups as $group)
        {
            if($group->name == 'Affiliate')
            {
                $flag = 1;
                break;
            }
        }
        if(!$flag)
        {
            return Redirect::route('affiliate.login');
        }
    }
});