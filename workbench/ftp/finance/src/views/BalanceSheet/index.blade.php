@extends('backend/_layout/layout')
@section('breadcrumb')
    <li>Home</li><li>Balance Sheet</li>
@stop

{{
    Assets::setScripts([
        'jquery.dataTables'         => 'js/plugin/datatables/jquery.dataTables.min.js',
        'dataTables.colVis'         => 'js/plugin/datatables/dataTables.colVis.min.js',
        'dataTables.tableTools'     => 'js/plugin/datatables/dataTables.tableTools.min.js',
        'dataTables.bootstrap'      => 'js/plugin/datatables/dataTables.bootstrap.min.js',
        'bootstrap-datepicker'      => 'js/plugin/bootstrap-datepicker/bootstrap-datepicker.js',
        'bootstrap-datepicker.tr'   => 'js/plugin/bootstrap-datepicker/locales/bootstrap-datepicker.tr.js',
    ], true);
}}

{{  Assets::setStyles([
    'datepicker'          => 'js/plugin/bootstrap-datepicker/datepicker.css'
], true) }}

@section('content')

    <div class="row">
        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
            <h1 class="page-title txt-color-blueDark">
                <i class="fa fa-table fa-fw "></i>
                Balance Sheet
            </h1>
        </div>
    </div>
     <!-- widget grid -->
    <section id="widget-grid" class="">
        <div class="row">
            <div class="col-sm-4 col-xs-12">
                Month Selector: <input type="text" id="datetime" value="{{$date}}">
            </div>
        </div>
        <div class="row">
            <div class="col-sm-3 col-xs-12" id="main-gooey">
                <!--
                    OK. So, this is a tough one. I need to split this in to all of the sections we're going to use.
                 -->
                <div class="row">
                    <div class="col-xs-12 hdr">Revenue</div>
                    <div class="col-xs-12 bdy" id="revenue">
                        <div class="item">
                            <div class="name">On-Site Sales: £</div>
                            <div class="val" id="on-site-val"></div>
                        </div>
                        <div class="item">
                            <div class="name">Off-Site Sales: £</div>
                            <div class="val" id="off-site-val"></div>
                        </div>
                        <div class="item total-item">
                            <div class="name">Total: £</div>
                            <div class="val"></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 hdr">Cost of Goods Sold</div>
                    <div class="col-xs-12 bdy" id="COGS">
                        <div class="item">
                            <div class="name">Broker Costs: £</div>
                            <div class="val" id="broker-costs-val"></div>
                        </div>
                        <div class="item">
                            <div class="name">Affiliate Costs: £</div>
                            <div class="val" id="affiliate-costs-val"></div>
                        </div>
                        <div class="item">
                            <div class="name">Discounts: £</div>
                            <div class="val" id="discount-costs-val"></div>
                        </div>
                        <div class="item">
                            <div class="name">Gateway Costs: £</div>
                            <div class="val" id="gateway-costs-val"></div>
                        </div>
                        <div class="item total-item">
                            <div class="name">Total: £</div>
                            <div class="val"></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 hdr">Gross Profit</div>
                    <div class="col-xs-12 bdy" id="gross-profit">
                        <div class="item total-item">
                            <div class="name">Total: £</div>
                            <div class="val"></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 hdr">Fixed Costs</div>
                    <div class="col-xs-12 bdy" id="fixed-costs">
                        <div class="item total-item">
                            <div class="name">Total: £</div>
                            <div class="val"></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 hdr">Variable Costs</div>
                    <div class="col-xs-12 bdy" id="variable-costs">
                        <div class="item total-item">
                            <div class="name">Total: £</div>
                            <div class="val"></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 hdr">Net Profit</div>
                    <div class="col-xs-12 bdy" id="net-profit">
                        <div class="item total-item">
                            <div class="name">Total: £</div>
                            <div class="val"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-9 col-xs-12">
                <div class="costs-dt">
                    <h3>All Costs</h3>
                    <table id="dt_costs" class="table table-striped table-bordered table-hover" width="100%">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Title</th>
                            <th>Cost </th>
                            <th>Type</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                    <br><br>
                    <h4>Add new cost</h4>
                    <br>
                    <div class="addNewCost">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>Title</th>
                                <th>Cost</th>
                                <th>Type</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td><input type="text" id="ncttl"></td>
                                <td><input type="text" id="ncval"></td>
                                <td>
                                    <select name="" id="nctype">
                                        <option value="variable_cost" selected>Variable Cost</option>
                                        <option value="fixed_cost">Fixed Cost</option>
                                    </select>
                                </td>
                                <td>
                                    <div class="btn btn-success" id="ncsave">Save</div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <br>
                </div>
                <div class="mp-dt">
                    <h3>This months costs</h3>
                    <div class="btn btn-success" id="addAllFC">Add Fixed Costs</div>
                    <table id="dt_recurring" class="table table-striped table-bordered table-hover" width="100%">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Title</th>
                            <th>Cost </th>
                            <th>Type</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
@stop

@section ('script')
    <style>
        #main-gooey{
            margin-top:40px;
        }

        #main-gooey .hdr{
            font-size:16px;
            margin-bottom:20px;
        }

        #main-gooey .item{
            margin-bottom:10px;
            padding-left:10px;
        }

        #main-gooey .item.total-item{
            border-top: 1px solid;
            padding-top: 5px;
            margin-bottom: 20px;
            text-align:right;
        }

        #main-gooey .item div{
            display:inline-block;
        }
    </style>

    <script>
        $(document).ready(function() {
            $('#datetime').datepicker({
                format: "yyyy-mm-dd"
            });

            $('#datetime').change(function(){
                var date = $(this).val();
                getTotals(date);

                var table = $('#dt_costs').DataTable();
                table.ajax.reload();

                var table = $('#dt_recurring').DataTable();
                table.ajax.reload();
            });

            getTotals($('#datetime').val());
        });

        function getTotals(date)
        {
            var revD = getRevenue(date);
            var cogsD = getCOGS(date);
            var fc = getFixedCosts(date);
            var vc = getVariableCosts(date);
        }

        function getRevenue(date)
        {
            $.ajax({
                url:'/admin/balance-sheet/getRevenue',
                type:"post",
                data:{
                    da:date
                },
                success:function(response){
                    $('#on-site-val').html(response.sS);
                    $('#off-site-val').html(response.oSS);
                    $('#revenue .total-item .val').html(response.sum_amount);
                    $('#revenue .total-item .val').data('rev', response.sum_amount);
                    sumGross();
                },
                error:function(response){
                    console.log(response);
                }
            });
            return true;
        }

        function getCOGS(date)
        {
            $.ajax({
                url:'/admin/balance-sheet/getCOGS',
                type:"post",
                data:{
                    da:date
                },
                success:function(response){
                    $('#broker-costs-val').html(response.brokerCosts);
                    $('#affiliate-costs-val').html(response.affiliateCosts);
                    $('#discount-costs-val').html(response.discounts);
                    $('#gateway-costs-val').html(response.gatewayCosts);
                    $('#COGS .total-item .val').html(response.total);
                    $('#COGS .total-item .val').data('cogs', response.total);
                    sumGross();
                },
                error:function(response){
                    console.log(response);
                }
            });
            return true;
        }

        function getFixedCosts(date)
        {
            $.ajax({
                url:'/admin/balance-sheet/getFixedCosts',
                type:"post",
                data:{
                    da:date
                },
                success:function(response){
                    var items = response.items;
                    var total = response.total;
                    var oStr = "";
                    $('.f-cost').remove();
                    $.each(items, function(index, val){
                        oStr += '<div class="item f-cost"><div class="name">'+val.title+': £</div> <div class="val">'+val.val+'</div></div>';
                    });

                    $('#fixed-costs').prepend(oStr);
                    $('#fixed-costs .total-item .val').html(total);
                    $('#fixed-costs .total-item .val').data('fixed-costs',total);
                },
                error:function(response){
                    console.log(response);
                }
            });


            return true;
        }

        function getVariableCosts(date)
        {
            $.ajax({
                url:'/admin/balance-sheet/getVariableCosts',
                type:"post",
                data:{
                    da:date
                },
                success:function(response){
                    var items = response.items;
                    var total = response.total;
                    var oStr = "";
                    $('.v-cost').remove();
                    $.each(items, function(index, val){
                        oStr += '<div class="item v-cost"><div class="name">'+val.title+': £</div> <div class="val">'+val.val+'</div></div>';
                    });

                    $('#variable-costs').prepend(oStr);
                    $('#variable-costs .total-item .val').html(total);
                    $('#variable-costs .total-item .val').data('variable-costs',total);
                },
                error:function(response){
                    console.log(response);
                }
            });

            return true;
        }

        function sumGross()
        {
            var revenue = $('#revenue .total-item .val').data('rev');
            var cogs = $('#COGS .total-item .val').data('cogs');

            if((typeof revenue != 'undefined') && (typeof cogs != 'undefined'))
            {
                $('#gross-profit .total-item .val').html((revenue - cogs).toFixed(2));
                $('#gross-profit .total-item .val').data('gross',(revenue - cogs).toFixed(2));
                sumNet();

            }
        }

        function sumNet()
        {
            var gp = $('#gross-profit .total-item .val').data('gross');
            var fc = $('#fixed-costs .total-item .val').data('fixed-costs');
            var vc = $('#variable-costs .total-item .val').data('variable-costs');

            var tc = fc + vc;
            var np = gp - tc;

            $('#net-profit .total-item .val').html(np.toFixed(2));
        }


        $('#dt_costs').dataTable({
            serverSide: true,
            bFilter: false, bInfo: false,
            ajax: {
                url: "/{{ Config::get('bondcms.admin_prefix') }}/balance-sheet/costsDT",
                type: 'POST'
            },

            order: [[0, 'DESC']]

        });

        $('#dt_recurring').dataTable({
            serverSide: true,
            bFilter: false, bInfo: false,
            ajax: {
                url: "/{{ Config::get('bondcms.admin_prefix') }}/balance-sheet/recurringDT",
                type: 'POST',
                data:{
                    'dt':function(){
                        return $('#datetime').val();
                    }
                }
            },

            order: [[0, 'DESC']]

        });


        $('#ncsave').click(function(){
            var ttl = $('#ncttl').val();
            var val = $('#ncval').val();
            var type = $('#nctype').val();

            $.ajax({
                url:'/admin/balance-sheet/addCost',
                type:'POST',
                data:{
                    title:ttl,
                    val:val,
                    type:type
                },
                success:function(response){
                    console.log(response);
                    var table = $('#dt_costs').DataTable();
                    table.ajax.reload();
                },
                error:function(response){
                    console.log(response);

                }
            })
        });

        $('body').on('click', '.cost-delete', function(){
            var id = $(this).data('id');

            $.ajax({
                url:'/admin/balance-sheet/removeCost/'+id,
                type:'POST',
                success:function(response){
                    console.log(response);
                    var table = $('#dt_costs').DataTable();
                    table.ajax.reload();

                    var table = $('#dt_recurring').DataTable();
                    table.ajax.reload();

                    getTotals($('#datetime').val());
                },
                error:function(response){
                    console.log(response);

                }
            })
        });

        $('body').on('click', '.cost-save', function(){
            var id = $(this).data('id');
            var ttl = $('#cost-title-'+id).val();
            var val = $('#cost-val-'+id).val();
            var type = $('#cost-type-'+id).val();

            $.ajax({
                url:'/admin/balance-sheet/updateCost/'+id,
                type:'POST',
                data:{
                    title:ttl,
                    val:val,
                    type:type
                },
                success:function(response){
                    var table = $('#dt_costs').DataTable();
                    table.ajax.reload();

                    var table = $('#dt_recurring').DataTable();
                    table.ajax.reload();

                    getTotals($('#datetime').val());
                },
                error:function(response){
                    console.log(response);
                }
            });

        });


        $('body').on('click', '.cost-add', function(){
            var ple_id = $(this).data('id');
            var date = $('#datetime').val();
            var val = $('#cost-val-'+ple_id).val();

            $.ajax({
               url:'/admin/balance-sheet/addRC',
                type:'POST',
                data:{
                    ple_id:ple_id,
                    val_modifier:val,
                    date:date
                },
                success:function(response){
                    console.log(response);

                    var table = $('#dt_costs').DataTable();
                    table.ajax.reload();

                    var table = $('#dt_recurring').DataTable();
                    table.ajax.reload();

                    getTotals($('#datetime').val());
                },
                error:function(response){
                    console.log(response);
                }
            });

        });

        $('body').on('click','.rcost-save', function(){
            var id = $(this).data('id');
            var val = $('#rcost-val-'+id).val();

            $.ajax({
                url:'/admin/balance-sheet/updateRC/'+id,
                type:'POST',
                data:{
                    val_modifier:val
                },
                success:function(response){
                    console.log(response);

                    var table = $('#dt_costs').DataTable();
                    table.ajax.reload();

                    var table = $('#dt_recurring').DataTable();
                    table.ajax.reload();

                    getTotals($('#datetime').val());
                },
                error:function(response){
                    console.log(response);
                }
            });
        });

        $('body').on('click','.rcost-delete', function(){
            var id = $(this).data('id');

            $.ajax({
                url:'/admin/balance-sheet/deleteRC/'+id,
                type:'POST',
                success:function(response){
                    console.log(response);

                    var table = $('#dt_costs').DataTable();
                    table.ajax.reload();

                    var table = $('#dt_recurring').DataTable();
                    table.ajax.reload();

                    getTotals($('#datetime').val());
                },
                error:function(response){
                    console.log(response);
                }
            });
        });

        $('#addAllFC').click(function(){
            var date = $('#datetime').val();
            $.ajax({
                url:'/admin/balance-sheet/auto-fc',
                type:'POST',
                data:{
                    date:date
                },
                success:function(response){
                    console.log(response);

                    var table = $('#dt_costs').DataTable();
                    table.ajax.reload();

                    var table = $('#dt_recurring').DataTable();
                    table.ajax.reload();

                    getTotals($('#datetime').val());
                },
                error:function(response){
                    console.log(response);
                }
            });
        });
    </script>
@stop


