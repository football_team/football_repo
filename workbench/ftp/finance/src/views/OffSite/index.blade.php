@extends('backend/_layout/layout')
@section('breadcrumb')
    <li>Home</li><li>Off Site Sales</li>
@stop

{{
    Assets::setScripts([
        'jquery.dataTables'         => 'js/plugin/datatables/jquery.dataTables.min.js',
        'dataTables.colVis'         => 'js/plugin/datatables/dataTables.colVis.min.js',
        'dataTables.tableTools'     => 'js/plugin/datatables/dataTables.tableTools.min.js',
        'dataTables.bootstrap'      => 'js/plugin/datatables/dataTables.bootstrap.min.js',
        'jquery-form'               => 'js/plugin/jquery-form/jquery-form.min.js',
        'jquery-validate'           => 'js/plugin/jquery-validate/jquery.validate.min.js',
    ], true);
}}

@section('content')
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" id="modalTitle"></h4>
                </div>
                <div class="modal-body" id="fill-this">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
            <h1 class="page-title txt-color-blueDark">
                <i class="fa fa-table fa-fw "></i>
                Off Site Sales
            </h1>
        </div>
    </div>
            <!-- widget grid -->
    <section id="widget-grid" class="">
        <!-- row -->
        <div class="row">
            <!-- NEW WIDGET START -->
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <!-- Widget ID (each widget will need unique ID)-->
                <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false">
                    <header>
                        <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                        <h2>Off Site Sales</h2>
                    </header>
                    <!-- widget div-->
                    <div>
                        <!-- widget edit box -->
                        <div class="jarviswidget-editbox">
                            <!-- This area used as dropdown edit box -->
                        </div>
                        <!-- end widget edit box -->
                        <!-- widget content -->
                        <div class="widget-body no-padding">

                            <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Event Title</th>
                                    <th>Event Date</th>
                                    <th>Ticket Type</th>
                                    <th>Ticket Receiver</th>
                                    <th>Seller Name</th>
                                    <th>Price Bought (£)</th>
                                    <th>Price Sold (£)</th>
                                    <th>Date Sold</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>

                        </div>
                        <!-- end widget content -->

                    </div>
                    <!-- end widget div -->

                </div>
                <!-- end widget -->

            </article>
            <!-- WIDGET END -->

        </div>
        <!-- end row -->
        <div class="row">
            <!-- NEW WIDGET START -->
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <!-- Widget ID (each widget will need unique ID)-->
                <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false">
                    <header>
                        <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                        <h2>Off Site Sales</h2>
                    </header>
                    <!-- widget div-->
                    <div>
                        <!-- widget edit box -->
                        <div class="jarviswidget-editbox">
                            <!-- This area used as dropdown edit box -->
                        </div>
                        <!-- end widget edit box -->
                        <!-- widget content -->
                        <div class="widget-body">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>
                                            Event Title
                                        </th>
                                        <th>
                                            Event Date
                                        </th>
                                        <th>
                                            Ticket Type
                                        </th>
                                        <th>
                                            Buyer Name
                                        </th>
                                        <th>
                                            Seller Name
                                        </th>
                                        <th>
                                            Cash Receiver Name
                                        </th>
                                        <th>
                                            Ticket Receiver Name
                                        </th>
                                        <th>
                                            Number Bought
                                        </th>
                                        <th>
                                            Price Bought (£)
                                        </th>
                                        <th>
                                            Price Sold (£)
                                        </th>
                                        <th>
                                            Date Sold
                                        </th>
                                        <th>&nbsp;</th>
                                    </tr>
                                    </thead>
                                    <tbody id="nOSSCont">
                                    <form action="/admin/off-site/new" id="nOSS">
                                    <tr>
                                        <td><input type="text" class="event_title" name="event_title" style="width:100%;"><div class="msgs"></div></td>
                                        <td><input type="text" class="event_date" name="event_date" style="width:100%;"><div class="msgs"></div></td>
                                        <td><input type="text" class="ticket_type" name="ticket_type" style="width:100%;"><div class="msgs"></div></td>
                                        <td><input type="text" class="buyer_name" name="buyer_name" style="width:100%;"><div class="msgs"></div></td>
                                        <td><input type="text" class="seller_name" name="seller_name" style="width:100%;"><div class="msgs"></div></td>
                                        <td><input type="text" class="cash_receiver_name" name="cash_receiver_name" style="width:100%;"><div class="msgs"></div></td>
                                        <td><input type="text" class="ticket_receiver_name" name="ticket_receiver_name" style="width:100%;"><div class="msgs"></div></td>
                                        <td><input type="text" class="number_bought" name="number_bought" style="width:100%;"><div class="msgs"></div></td>
                                        <td><input type="text" class="price_bought" name="price_bought" style="width:100%;"><div class="msgs"></div></td>
                                        <td><input type="text" class="price_sold" name="price_sold" style="width:100%;"><div class="msgs"></div></td>
                                        <td><input type="text" class="date_sold" name="date_sold" style="width:100%;"><div class="msgs"></div></td>
                                        <td><input type="submit" value="SUBMIT"></td>
                                    </tr>
                                    </form>
                                    </tbody>
                                </table>
                        </div>
                        <!-- end widget content -->

                    </div>
                    <!-- end widget div -->

                </div>
                <!-- end widget -->

            </article>
            <!-- WIDGET END -->

        </div>
        <!-- end row -->

    </section>
    <!-- end widget grid -->
@stop

@section ('script')
    <script>
        $(document).ready(function() {
            $('#dt_basic').dataTable({
                serverSide: true,
                ajax: {
                    url: "/{{ Config::get('bondcms.admin_prefix') }}/off-site/list.json",
                    type: 'POST'
                },
                order: [[0, 'DESC']]
            });


            $('#nOSS').ajaxForm({
                dataType: 'json',
                type:"POST",
                success: function (response) {
                    $('.msgs').html("");
                    $('.msgs').parent().find('input').val("");
                    var table = $('#dt_basic').DataTable();
                    table.ajax.reload();
                },
                error: function (response) {
                    response = response.responseJSON;
                    $('.msgs').html("");

                    $.each(response, function(key, val){
                        var instr = "";
                        $.each(val, function(key, val)
                        {
                            instr = instr+val+"<br>";
                        });
                        $('#nOSSCont .'+key).parent().find('.msgs').html(instr);
                    });
                }
            });

            $('body').on('click', '.view-full', function(e){
                e.preventDefault();
                var id = $(this).data('id');
                //post the id, get full order details, get returned partial and display a modal
                $.ajax({
                    url:"/admin/off-site/getFull/"+id,
                    type:"POST",
                    success:function(response){
                        $('#fill-this').html(response);
                        $('#myModal').modal('show');
                    },
                    error:function(response){

                    }
                })

            });

            $('body').on('click', '.delete-oss', function(){
               var id = $(this).data('id');
                $.ajax({
                    url:"/admin/off-site/delete/"+id,
                    type:"POST",
                    success:function(response){
                        var table = $('#dt_basic').DataTable();
                        table.ajax.reload();
                    },
                    error:function(response){

                    }
                })
            });

            $('body').on('click','.full-view-save', function(){
                var id = $(this).data('id');
                var inArray = $('#full-inputs input');

                var data = {};

                $.each(inArray, function(key, val){
                    var aClass = $(val).data('class');
                    var aVal = $(val).val();
                    data[aClass]=aVal;
                });

                $.ajax({
                    url:"/admin/off-site/update/"+id,
                    data:data,
                    type:"POST",
                    success:function(response){
                        var table = $('#dt_basic').DataTable();
                        table.ajax.reload();
                        $('#myModal').modal('hide');
                    },
                    error:function(response){
                        response = response.responseJSON;
                        $('.ers').html("");

                        $.each(response, function(key, val){
                            var instr = "";
                            $.each(val, function(key, val)
                            {
                                instr = instr+val+"<br>";
                            });
                            $('#full-inputs .'+key).parent().find('.ers').html(instr);
                        });
                    }
                })
            })
        })
    </script>
@stop


