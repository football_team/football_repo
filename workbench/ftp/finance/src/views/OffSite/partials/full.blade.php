<table class="table">
    <thead>
    <tr>
        <th>
            Name
        </th>
        <th>
            Value
        </th>
    </tr>
    </thead>
    <tbody id="full-inputs">
    <tr>
        <td>Event Title</td>
        <td><input type="text" data-class="event_title" class="event_title" value="{{$oSS->event_title}}"><div class="ers"></div></td>
    </tr>
    <tr>
        <td>Event Date</td>
        <td><input type="text" data-class="event_date" class="event_date" value="{{$oSS->event_date}}"><div class="ers"></div></td>
    </tr>
    <tr>
        <td>Ticket Type</td>
        <td><input type="text" data-class="ticket_type" class="ticket_type" value="{{$oSS->ticket_type}}"><div class="ers"></div></td>
    </tr>
    <tr>
        <td>Buyer Name</td>
        <td><input type="text" data-class="buyer_name" class="buyer_name" value="{{$oSS->buyer_name}}"><div class="ers"></div></td>
    </tr>
    <tr>
        <td>Seller Name</td>
        <td><input type="text" data-class="seller_name" class="seller_name" value="{{$oSS->seller_name}}"><div class="ers"></div></td>
    </tr>
    <tr>
        <td>Cash Receiver Name</td>
        <td><input type="text" data-class="cash_receiver_name" class="cash_receiver_name" value="{{$oSS->cash_receiver_name}}"><div class="ers"></div></td>
    </tr>
    <tr>
        <td>Ticket Receiver Name</td>
        <td><input type="text" data-class="ticket_receiver_name" class="ticket_receiver_name" value="{{$oSS->ticket_receiver_name}}"><div class="ers"></div></td>
    </tr>
    <tr>
        <td>Number Bought</td>
        <td><input type="text" data-class="number_bought" class="number_bought" value="{{$oSS->number_bought}}"> <div class="ers"></div></td>
    </tr>
    <tr>
        <td>Price Bought (£)</td>
        <td><input type="text" data-class="price_bought" class="price_bought" value="{{$oSS->price_bought}}"> <div class="ers"></div></td>
    </tr>
    <tr>
        <td>Price Sold (£)</td>
        <td><input type="text" data-class="price_sold" class="price_sold" value="{{$oSS->price_sold}}"> <div class="ers"></div></td>
    </tr>
    <tr>
        <td>Date Sold</td>
        <td><input type="text" data-class="date_sold" class="date_sold" value="{{$oSS->date_sold}}"> <div class="ers"></div></td>
    </tr>
    <tr>
        <td></td>
        <td><button data-id="{{$oSS->id}}" class="full-view-save btn-primary">SAVE</button></td>
    </tr>
    </tbody>
</table>


