<?php

/**
 * Created by PhpStorm.
 * User: baglad
 * Date: 09/08/2016
 * Time: 10:04
 */
class ProfitLossEntityMonthly extends Eloquent
{
    public $table = 'profit_loss_entity_monthly_profile';
    public $fillable = ['date_applicable', 'ple_id'];
    public $timestamps = false;
}