<?php

/**
 * Created by PhpStorm.
 * User: baglad
 * Date: 09/08/2016
 * Time: 10:07
 */
class ProfitLossEntity extends Eloquent
{
    public $table = 'profit_loss_entities';
    public $fillable = ['date_applicable', 'type', 'val', 'title', 'id', 'remote_id'];
    public $timestamps = false;
}