<?php

/**
 * Created by PhpStorm.
 * User: baglad
 * Date: 05/08/2016
 * Time: 11:18
 */
class OffSiteSale extends Eloquent
{
    public $table = 'off_site_sales';
    public $fillable = ['event_title', 'event_date', 'buyer_name', 'seller_name',
        'cash_receiver_name', 'ticket_receiver_name', 'ticket_type', 'number_bought' ,'price_bought',
        'price_sold', 'date_sold'];

    public $timestamps = false;
}