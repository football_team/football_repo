<?php
/**
 * Created by PhpStorm.
 * User: baglad
 * Date: 05/08/2016
 * Time: 09:48
 */

Route::group(array('prefix' => Config::get('bondcms.admin_prefix'), 'before' => array('auth.admin', 'assets_admin')), function ()
{
    Route::get('off-site', 'OffSiteSaleController@index');
    Route::post('off-site/list.json', 'OffSiteSaleController@listOffSite');
    Route::post('off-site/new', array('uses'=>'OffSiteSaleController@addOffSite', 'before'=>'csrf'));
    Route::post('off-site/getFull/{id}', array('uses'=>'OffSiteSaleController@viewOffSite', 'before'=>'csrf'))->where('id', '[0-9]+');
    Route::post('off-site/delete/{id}', array('uses'=>'OffSiteSaleController@deleteOffSite', 'before'=>'csrf'))->where('id', '[0-9]+');
    Route::post('off-site/update/{id}', array('uses'=>'OffSiteSaleController@updateOffSite','before'=>'csrf'))->where('id', '[0-9]+');

    Route::get('balance-sheet', array('uses'=>'BalanceSheetController@index'));
    Route::post('balance-sheet/totals', array('uses'=>'BalanceSheetController@getCategoryFiguresByDate'));
    Route::post('balance-sheet/getRevenue', array('uses'=>'BalanceSheetController@getRevenue'));
    Route::post('balance-sheet/getCOGS', array('uses'=>'BalanceSheetController@getCOGS'));
    Route::post('balance-sheet/getFixedCosts', array('uses'=>'BalanceSheetController@getFixedCosts'));
    Route::post('balance-sheet/getVariableCosts', array('uses'=>'BalanceSheetController@getVariableCosts'));

    Route::post('balance-sheet/addCost', array('uses'=>'BalanceSheetController@addNewCost'));
    Route::post('balance-sheet/removeCost/{id}', array('uses'=>'BalanceSheetController@removeCost', 'before'=>'csrf'))->where('id', '[0-9]+');
    Route::post('balance-sheet/updateCost/{id}', array('uses'=>'BalanceSheetController@updateCost', 'before'=>'csrf'))->where('id', '[0-9]+');

    Route::post('balance-sheet/addRC', array('uses'=>'BalanceSheetController@addRC', 'before'=>'csrf'));
    Route::post('balance-sheet/updateRC/{id}', array('uses'=>'BalanceSheetController@updateRC', 'before'=>'csrf'))->where('id', '[0-9]+');
    Route::post('balance-sheet/deleteRC/{id}', array('uses'=>'BalanceSheetController@deleteRC', 'before'=>'csrf'))->where('id', '[0-9]+');

    Route::post('balance-sheet/auto-fc', array('uses'=>'BalanceSheetController@addFixedCostsToMonth', 'before'=>'csrf'));

    Route::post('balance-sheet/costsDT', array('uses'=>'BalanceSheetController@getCostsDT'));
    Route::post('balance-sheet/recurringDT', array('uses'=>'BalanceSheetController@getRecurringDT'));
});

