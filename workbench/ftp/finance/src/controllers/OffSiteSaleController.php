<?php

/**
 * Created by PhpStorm.
 * User: baglad
 * Date: 05/08/2016
 * Time: 10:40
 */
class OffSiteSaleController extends BaseControllerF
{

    public function index()
    {
        View::share('menu', 'off-site');
        return View::make('Finance::OffSite.index');
    }

    public function listOffSite()
    {
        //populate datatable with all offsite sales
        $input = Input::all();

        $likeStr = '';
        $start = 0;
        $take = 10;

        if(array_key_exists('search', $input))
        {
            $likeStr = $input['search']['value'];
        }

        if(array_key_exists('start', $input))
        {
            $start = $input['start'];
        }

        if(array_key_exists('length', $input))
        {
            $take = $input['length'];
        }

        $oss = OffSiteSale::where('event_title', 'LIKE', '%'.$likeStr.'%')->skip($start)->take($take)->get();
        $ttl = DB::table('off_site_sales')
                ->selectRaw('COUNT(*) as ttl')
                ->first();

        $ttl = $ttl->ttl;
        $output = [];

        foreach($oss as $os)
        {
            $temp = [];

            $temp['0'] = '<a class="view-full" data-id="'.$os->id.'">'.$os->id.'</a>';
            $temp['1'] = $os->event_title;
            $temp['2'] = $os->event_date;
            $temp['3'] = $os->ticket_type;
            $temp['4'] = $os->ticket_receiver_name;
            $temp['5'] = $os->seller_name;
            $temp['6'] = $os->price_bought;
            $temp['7'] = $os->price_sold;
            $temp['8'] = $os->date_sold;
            $temp['9'] = '<a class="delete-oss" data-id="'.$os->id.'">DELETE</a>';

            $output[] = $temp;
        }

        $results['data'] = $output;
        $results['recordsTotal'] = $ttl;
        $results['recordsFiltered'] = $ttl;
        $response = Response::make($results);
        $response->header('Content-Type', 'application/json');
        return $response;
    }

    public function viewOffSite($id)
    {
        $oSS = OffSiteSale::where('id', $id)->first();

        if(!empty($oSS))
        {
            View::share('oSS', $oSS);
            return View::make('Finance::OffSite.partials.full');
        }

        return Response::json("FAIL", 404);
    }

    public function addOffSite()
    {
        $input = Input::all();

        if(array_key_exists('id', $input))
        {
            unset($input['id']);
        }

        $oss = new OffSiteSale();

        $validator = Validator::make(
            $input,
            array(
                'event_title'=>'required',
                'event_date'=>'required|date',
                'buyer_name'=>'required',
                'seller_name'=>'required',
                'cash_receiver_name'=>'required',
                'ticket_receiver_name'=>'required',
                'ticket_type'=>'required',
                'number_bought'=>'required|integer',
                'price_bought'=>'required|numeric',
                'price_sold'=>'required|numeric',
                'date_sold'=>'required|date',
            )

        );

        if(!$validator->fails())
        {
            $ed = strtotime($input['event_date']);
            $ds = strtotime($input['date_sold']);

            $dt = new DateTime();
            $dt->setTimestamp($ed);

            $ed = $dt->format('Y-m-d');

            $dt->setTimestamp($ds);
            $ds = $dt->format('Y-m-d');

            $input['event_date'] = $ed;
            $input['date_sold'] = $ds;

            $oss->fill($input);
            $oss->save();

            return Response::json("Hello", 200);
        }

        $messages = $validator->messages();
        $failed = $validator->failed();

        $return = array();

        foreach($failed as $key => $fail)
        {

            $return[$key] = array();
            foreach($fail as $k => $v)
            {
                $return[$key][] = $k;
            }
        }

        return Response::json($return, 404);
    }

    public function updateOffSite($id)
    {
        $oss = OffSiteSale::where('id', $id)->first();
        if(!empty($oss))
        {
            $input = Input::all();

            $validator = Validator::make(
                $input,
                array(
                    'event_title'=>'required',
                    'event_date'=>'required|date',
                    'buyer_name'=>'required',
                    'seller_name'=>'required',
                    'cash_receiver_name'=>'required',
                    'ticket_receiver_name'=>'required',
                    'ticket_type'=>'required',
                    'number_bought'=>'required|integer',
                    'price_bought'=>'required|numeric',
                    'price_sold'=>'required|numeric',
                    'date_sold'=>'required|date',
                )

            );


            if(!$validator->fails())
            {
                $oss->fill($input);
                $oss->save();
                return Response::json("yay",200);
            }

            $messages = $validator->messages();
            $failed = $validator->failed();

            $return = array();

            foreach($failed as $key => $fail)
            {

                $return[$key] = array();
                foreach($fail as $k => $v)
                {
                    $return[$key][] = $k;
                }
            }

            return Response::json($return, 404);

        }
        return Response::json("NOT FOUND",404);

    }

    public function deleteOffSite($id)
    {
        OffSiteSale::where('id', $id)->delete();

        return Response::json("success",200);
    }
}