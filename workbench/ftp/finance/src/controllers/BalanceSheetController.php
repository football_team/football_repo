<?php

/**
 * Created by PhpStorm.
 * User: baglad
 * Date: 08/08/2016
 * Time: 14:50
 */
class BalanceSheetController extends BaseControllerF
{
    public function index()
    {
        View::share('menu','profit-loss');
        $dt = new DateTime();
        View::share('date', $dt->format('Y-m-d'));

        return View::make('Finance::BalanceSheet.index');
    }

    public function fixedCostView()
    {
        if(Input::has('da'))
        {
            $da = DateTime::createFromFormat('Y-m-d', Input::get('da'));
            $dt = DateTime::createFromFormat('Y-m-d', Input::get('da'));
            $dt->modify('+1 month');

            $da = $da->format('Y-m');
            $dt = $dt->format('Y-m');

            if ($da)
            {
                $mp = DB::table('profit_loss_entity_monthly_profile')
                    ->leftJoin('profit_loss_entities', 'profit_loss_entity_monthly_profile.ple_id', '=', 'profit_loss_entities.id')
                    ->where('profit_loss_entity_monthly_profile.date_applicable', '>=', $da)
                    ->where('profit_loss_entity_monthly_profile.date_applicable', '<', $dt)
                    ->where('profit_loss_entities.type', 'fixed_cost')
                    ->select('val', 'val_modifier')
                    ->get();

                $mpl = DB::table('profit_loss_entity_monthly_profile')
                    ->leftJoin('profit_loss_entities', 'profit_loss_entity_monthly_profile.ple_id', '=', 'profit_loss_entities.id')
                    ->where('profit_loss_entity_monthly_profile.date_applicable', '>=', $da)
                    ->where('profit_loss_entity_monthly_profile.date_applicable', '<', $dt)
                    ->where('profit_loss_entities.type', 'fixed_cost')
                    ->select('profit_loss_entities.id')
                    ->get();

                $mpl = (array) $mpl;

                $p = DB::table('profit_loss_entities')
                        ->whereNotIn('profit_loss_entities.id', $mpl)
                        ->get();
            }

            return Response::json("fail",404);
        }

        return Response::json("fail",404);
    }

    public function getCategoryFiguresByDate()
    {
        if(Input::has('da'))
        {
            $da = DateTime::createFromFormat('Y-m-d', Input::get('da'));
            $dt = DateTime::createFromFormat('Y-m-d', Input::get('da'));
            $dt->modify('+1 month');

            $da = $da->format('Y-m');
            $dt = $dt->format('Y-m');

            if($da)
            {
                $api_path = Config::get('api.mage_soap_api_path');
                require_once("{$api_path}app/Mage.php");
                umask(0);
                Mage::app('default');

                $categories = array(
                    'GrossProfit'=>0,
                    'CostOfSales'=>0,
                    'FixedCosts'=>0,
                    'VariableCosts'=>0,
                    'NetProfit'=>0
                );

                $gp = DB::table('events_ticket_buy')
                        ->leftJoin('events_related_tickets', 'events_ticket_buy.product_id', '=','events_related_tickets.product_id')
                        ->leftJoin('events', 'events_related_tickets.event_id', '=', 'events.id')
                        ->selectRaw('SUM(events_ticket_buy.amount) as ttl, SUM(events_ticket_buy.amount * (gateway_perc/100)) as gateway_cost, SUM(events_ticket_buy.fees_amount) as fees')
                        ->where('events_ticket_buy.created_at', '>=', $da)
                        ->where('events_tiket_buy.created_at', '<', $dt)
                        ->first();

                $oss = OffSiteSale::where('date_sold', '>=', $da)
                        ->where('date_sold', '<', $dt)
                        ->selectRaw('SUM(price_sold - price_bought) as sum')
                        ->first();

                $categories['GrossProfit'] = $gp->ttl + $oss->sum;



                $brokerCosts = ($gp->ttl-$gp->fees)*8.5;

                $af_sale_totals = DB::table('affiliate_sales')
                                    ->leftJoin('events_ticket_buy', 'affiliate_sales.order_id', '=', 'events_ticket_buy.order_id')
                                    ->leftJoin('events_related_tickets', 'events_ticket_buy.product_id', '=', 'events_related_tickets.product_id')
                                    ->leftJoin('events', 'events_related_tickets.event_id', '=', 'events.id')
                                    ->where('events_ticket_buy.created_at', '>=', $da)
                                    ->where('events_ticket_buy.created_at', '<', $dt)
                                    ->selectRaw('SUM(events_ticket_buy.amount * (affiliate_sales.commission_perc / 100)) as af_cost')
                                    ->first();

                $af_sale_totals = $af_sale_totals->af_cost;

                $gateway_total = $gp->gateway_cost;

                $discount_amount = 0;

                $oIDs = DB::table('events_ticket_buy')
                    ->leftJoin('events_related_tickets', 'events_ticket_buy.product_id', '=','events_related_tickets.product_id')
                    ->leftJoin('events', 'events_related_tickets.event_id', '=', 'events.id')
                    ->where('events_ticket_buy.created_at', '>=', $da)
                    ->where('events_ticket_buy.created_at', '<', $dt)
                    ->lists('order_id');

                $order_collection = Mage::getModel('sales/order')->getCollection()->addFieldToFilter('increment_id', array('in' => $oIDs));

                foreach($order_collection as $order)
                {
                    $discount_amount += $order->discount_amount;
                }

                $discount_amount = $discount_amount * -1;

                //I need to output the costs individually so we can see them

                $cOs = $brokerCosts + $af_sale_totals + $gateway_total + $discount_amount;

                $categories['CostOfSales'] = $cOs;

                $fc = DB::table('profit_loss_entity_monthly_profile')
                        ->leftJoin('profit_loss_entities', 'profit_loss_entity_monthly_profile.ple_id', '=', 'profit_loss_entities.id')
                        ->where('profit_loss_entity_monthly_profile.date_applicable', '>=', $da)
                        ->where('profit_loss_entity_monthly_profile.date_applicable', '<', $dt)
                        ->where('profit_loss_entities.type', 'fixed_cost')
                        ->select('val', 'val_modifier')
                        ->get();

                $fcTtl = 0;
                foreach($fc as $f)
                {
                    if($f->val_modifier != null)
                    {
                        $f->val = $f->val_modifier;
                    }
                    $fcTtl+=$f->val;
                }

                $categories['FixedCosts'] = $fcTtl;

                $vc = DB::table('profit_loss_entities')
                    ->where('date_applicable', '>=', $da)
                    ->where('date_applicable', '<', $dt)
                    ->where('type', 'variable_cost')
                    ->selectRaw('SUM(val) as sum')
                    ->first();

                if($vc->sum == null)
                {
                    $vc->sum = 0;
                }

                $categories['VariableCosts']=$vc->sum;
                $categories['NetProfit'] = $categories['GrossProfit'] - ($categories['CostOfSales'] + $categories['FixedCosts'] + $categories['VariableCosts']);

                return Response::json($categories,200);
            }

            return Response::json('Invalid date format', 404);
        }

        return Response::json("Must receive date", 404);

    }

    //I want to split them into 5 different methods. The net can be worked out using javascript....YAH

    public function getRevenue()
    {
        if(Input::has('da')) {
            $da = DateTime::createFromFormat('Y-m-d', Input::get('da'));
            $dt = DateTime::createFromFormat('Y-m-d', Input::get('da'));
            $dt->modify('+1 month');

            $da = $da->format('Y-m');
            $dt = $dt->format('Y-m');

            if ($da)
            {
                $gp = DB::table('events_ticket_buy')
                    ->leftJoin('events_related_tickets', 'events_ticket_buy.product_id', '=','events_related_tickets.product_id')
                    ->leftJoin('events', 'events_related_tickets.event_id', '=', 'events.id')
                    ->selectRaw('SUM(events_ticket_buy.amount) as ttl, COUNT(*) as num_sales')
                    ->where('events_ticket_buy.created_at', '>=', $da)
                    ->where('events_ticket_buy.created_at', '<', $dt)
                    ->first();

                $oss = OffSiteSale::where('date_sold', '>=', $da)
                    ->where('date_sold', '<', $dt)
                    ->selectRaw('SUM(price_sold - price_bought) as sum, COUNT(*) as num_sales')
                    ->first();

                $ret = array();

                $ret['sS']=number_format($gp->ttl,2,'.','');
                $ret['nSS']=number_format($gp->num_sales,2,'.','');
                $ret['oSS'] = number_format($oss->sum,2,'.','');
                $ret['nOSS'] = number_format($oss->num_sales,2,'.','');
                $ret['sum_amount'] = number_format($oss->sum + $gp->ttl,2,'.','');

                return Response::json($ret,200);
            }

            return Response::json("Error",404);
        }

        return Response::json('Requires date',404);
    }

    public function getCOGS()
    {
        if(Input::has('da')) {
            $api_path = Config::get('api.mage_soap_api_path');
            require_once("{$api_path}app/Mage.php");
            umask(0);
            Mage::app('default');

            $da = DateTime::createFromFormat('Y-m-d', Input::get('da'));
            $dt = DateTime::createFromFormat('Y-m-d', Input::get('da'));
            $dt->modify('+1 month');

            $da = $da->format('Y-m');
            $dt = $dt->format('Y-m');

            if ($da)
            {
                $gp = DB::table('events_ticket_buy')
                    ->leftJoin('events_related_tickets', 'events_ticket_buy.product_id', '=','events_related_tickets.product_id')
                    ->leftJoin('events', 'events_related_tickets.event_id', '=', 'events.id')
                    ->selectRaw('SUM(events_ticket_buy.amount) as ttl, SUM(events_ticket_buy.amount * (gateway_perc/100)) as gateway_cost, SUM(events_ticket_buy.fees_amount) as fees')
                    ->where('events_ticket_buy.created_at', '>=', $da)
                    ->where('events_ticket_buy.created_at', '<', $dt)
                    ->first();


                $brokerCosts = ($gp->ttl-$gp->fees)*0.85;

                $af_sale_totals = DB::table('affiliate_sales')
                    ->leftJoin('events_ticket_buy', 'affiliate_sales.order_id', '=', 'events_ticket_buy.order_id')
                    ->leftJoin('events_related_tickets', 'events_ticket_buy.product_id', '=', 'events_related_tickets.product_id')
                    ->leftJoin('events', 'events_related_tickets.event_id', '=', 'events.id')
                    ->where('events_ticket_buy.created_at', '>=', $da)
                    ->where('events_ticket_buy.created_at', '<', $dt)
                    ->where('affiliate_sales.affiliate_id','!=', 1)
                    ->selectRaw('SUM(events_ticket_buy.amount * (affiliate_sales.commission_perc / 100)) as af_cost')
                    ->first();

                $af_sale_totals = $af_sale_totals->af_cost;

                $gateway_total = $gp->gateway_cost;

                $discount_amount = 0;

                $oIDs = DB::table('events_ticket_buy')
                    ->leftJoin('events_related_tickets', 'events_ticket_buy.product_id', '=','events_related_tickets.product_id')
                    ->leftJoin('events', 'events_related_tickets.event_id', '=', 'events.id')
                    ->where('events_ticket_buy.created_at', '>=', $da)
                    ->where('events_ticket_buy.created_at', '<', $dt)
                    ->lists('order_id');

                $order_collection = Mage::getModel('sales/order')->getCollection()->addFieldToFilter('increment_id', array('in' => $oIDs));

                foreach($order_collection as $order)
                {
                    $discount_amount += $order->discount_amount;
                }

                $discount_amount = $discount_amount * -1;

                $cOs = $brokerCosts + $af_sale_totals + $gateway_total + $discount_amount;

                $ret = array();
                $ret['brokerCosts'] = number_format($brokerCosts,2,'.','');
                $ret['affiliateCosts'] = number_format($af_sale_totals,2,'.','');
                $ret['gatewayCosts'] = number_format($gateway_total,2,'.','');
                $ret['discounts'] = number_format($discount_amount,2,'.','');
                $ret['total']= number_format($cOs,2,'.','');

                return Response::json($ret,200);
            }

            return Response::json("Error",404);
        }

        return Response::json('Requires date',404);
    }

    public function getFixedCosts()
    {
        if(Input::has('da')) {
            $da = DateTime::createFromFormat('Y-m-d', Input::get('da'));
            $dt = DateTime::createFromFormat('Y-m-d', Input::get('da'));
            $dt->modify('+1 month');

            $da = $da->format('Y-m');
            $dt = $dt->format('Y-m');

            if ($da)
            {
                $fc = DB::table('profit_loss_entity_monthly_profile')
                    ->leftJoin('profit_loss_entities', 'profit_loss_entity_monthly_profile.ple_id', '=', 'profit_loss_entities.id')
                    ->where('profit_loss_entity_monthly_profile.date_applicable', '>=', $da)
                    ->where('profit_loss_entity_monthly_profile.date_applicable', '<', $dt)
                    ->where('profit_loss_entities.type', 'fixed_cost')
                    ->select('val', 'val_modifier', 'title')
                    ->get();

                $fcTtl = 0;
                $ret = array();
                $ret['items']=array();

                foreach($fc as $f)
                {
                    if($f->val_modifier != null)
                    {
                        $f->val = $f->val_modifier;
                    }
                    $fcTtl+=$f->val;

                    $ret['items'][] = $f;
                }

                $ret['total'] = $fcTtl;

                return Response::json($ret,200);
            }

            return Response::json("Invalid date",404);
        }
        return Response::json("Requires date",404);
    }

    public function getVariableCosts()
    {
        if(Input::has('da')) {
            $da = DateTime::createFromFormat('Y-m-d', Input::get('da'));
            $dt = DateTime::createFromFormat('Y-m-d', Input::get('da'));
            $dt->modify('+1 month');

            $da = $da->format('Y-m');
            $dt = $dt->format('Y-m');

            if ($da)
            {
                $fc = DB::table('profit_loss_entity_monthly_profile')
                    ->leftJoin('profit_loss_entities', 'profit_loss_entity_monthly_profile.ple_id', '=', 'profit_loss_entities.id')
                    ->where('profit_loss_entity_monthly_profile.date_applicable', '>=', $da)
                    ->where('profit_loss_entity_monthly_profile.date_applicable', '<', $dt)
                    ->where('profit_loss_entities.type', 'variable_cost')
                    ->select('val', 'val_modifier', 'title')
                    ->get();

                $fcTtl = 0;
                $ret = array();
                $ret['items']=array();

                foreach($fc as $f)
                {
                    if($f->val_modifier != null)
                    {
                        $f->val = $f->val_modifier;
                    }
                    $fcTtl+=$f->val;

                    $ret['items'][] = $f;
                }

                $ret['total'] = $fcTtl;

                return Response::json($ret,200);
            }

            return Response::json("Invalid date",404);
        }
        return Response::json("Requires date",404);
    }


    public function addNewCost()
    {
        $input = Input::all();

        $validator = Validator::make(
            $input,
            array(
                'type' => 'required',
                'val' => 'required|numeric|min:0',
                'title'=>'required'
            )
        );

        if(!$validator->fails())
        {
            if(($input['type'] == 'fixed_cost')||($input['type']=='variable_cost'))
            {
                $insArray = array(
                    'type'=>$input['type'],
                    'val'=>$input['val'],
                    'title'=>$input['title']
                );

                DB::table('profit_loss_entities')
                    ->insert($insArray);

                return Response::json('success',200);
            }

            return Response::json("Invalid cost type", 404);
        }

        $messages = $validator->messages();
        return Response::json($messages,404);
    }

    public function removeCost($id)
    {
        //Remove cost
        //remove monthly profile
        $cost = DB::table('profit_loss_entities')
                    ->where('id', $id)
                    ->first();

        if($cost)
        {
            DB::table('profit_loss_entities')
                ->where('id', $id)
                ->delete();
        }

        $mp = DB::table('profit_loss_entity_monthly_profile')
                    ->where('ple_id', $id)
                    ->first();

        if($mp)
        {
            DB::table('profit_loss_entity_monthly_profile')
                ->where('ple_id', $id)
                ->delete();
        }

        return Response::json("Success", 200);
    }

    public function updateCost($id)
    {
        $cost = DB::table('profit_loss_entities')
                    ->where('id', $id)
                    ->first();

        if($cost)
        {
            $input = Input::all();

            $validator = Validator::make(
                $input,
                array(
                    'type' => 'required',
                    'val' => 'required|numeric|min:0',
                    'title'=>'required'
                )
            );

            if(!$validator->fails())
            {
                DB::table('profit_loss_entities')
                    ->where('id', $id)
                    ->update(
                        [
                            'type'=>$input['type'],
                            'val' => $input['val'],
                            'title'=>$input['title']
                        ]
                    );

                return Response::json('success', 200);
            }

            return Response::json('Please make sure all of the fields make sense');
        }

        return Response::json('Could not find cost',404);
    }

    public function addRC()
    {
        $input = Input::all();

        $validator = Validator::make(
            $input,
            array(
                'ple_id' => 'required',
                'val_modifier' => 'required|numeric|min:0',
                'date'=>'required'
            )
        );

        if(!$validator->fails())
        {
            $cost = DB::table('profit_loss_entities')
                        ->where('id', $input['ple_id'])
                        ->first();

            if($cost)
            {
                $dt = DateTime::createFromFormat('Y-m-d',$input['date']);

                if($dt)
                {
                    DB::table('profit_loss_entity_monthly_profile')
                        ->insert([
                            'ple_id'=>$input['ple_id'],
                            'val_modifier'=>$input['val_modifier'],
                            'date_applicable'=>$dt->format('Y-m-d')
                        ]);

                    return Response::json("Success",200);
                }

                return Response::json("Date invalid format",404);
            }

            return Response::json('cannot find cost',404);
        }

        return Response::json("Please make sure all the fields make sense.");
    }


    public function updateRC($id){
        $input = Input::all();

        $validator = Validator::make(
            $input,
            array(
                'val_modifier' => 'required|numeric|min:0',
            )
        );

        if(!$validator->fails())
        {
            $rc = DB::table('profit_loss_entity_monthly_profile')
                    ->where('id', $id)
                    ->first();

            if($rc)
            {
                $rc = DB::table('profit_loss_entity_monthly_profile')
                    ->where('id', $id)
                    ->update([
                        'val_modifier'=>$input['val_modifier']
                    ]);

                return Response::json('success',200);
            }

            return Response::json('Could not locate recurrring cost',404);
        }

        return Repsonse::json("Cost must be numeric");
    }

    public function deleteRC($id)
    {
        $rc = DB::table('profit_loss_entity_monthly_profile')
                    ->where('id', $id)
                    ->first();

        if($rc)
        {
            DB::table('profit_loss_entity_monthly_profile')
                    ->where('id',$id)
                    ->delete();

            return Response::json("Success",200);
        }

        return Response::json("Could not locate recurring cost",404);
    }

    public function getCostsDT()
    {
        $start = 0;
        $length = 10;

        if(Input::has('start'))
        {
            $start = Input::get('start');
        }

        if(Input::has('length'))
        {
            $length = Input::get('length');
        }

        $costs = DB::table('profit_loss_entities')->skip($start)->take($length)->get();

        $output = [];

        $total = DB::table('profit_loss_entities')
            ->select(DB::raw('COUNT(*) as ttl'))
            ->first();

        $total = $total->ttl;

        foreach($costs as $cost)
        {
            $temp = [];

            $temp['0'] = $cost->id;
            $temp['1'] = '<input type="text" name="" id="cost-title-'.$cost->id.'" value="'.$cost->title.'">';
            $temp['2'] = '<input type="text" name="" id="cost-val-'.$cost->id.'" value="'.$cost->val.'">';

            $temp['3'] = '<select id="cost-type-'.$cost->id.'">';
            if($cost->type == "variable_cost")
            {
                $temp['3'] .= '<option value="variable_cost" selected>Variable Cost</option>';
                $temp['3'] .= '<option value="fixed_cost">Fixed Cost</option>';
            }
            else
            {
                $temp['3'] .= '<option value="variable_cost">Variable Cost</option>';
                $temp['3'] .= '<option value="fixed_cost" selected>Fixed Cost</option>';
            }
            $temp['3'] .= "</select>";

            $temp['4'] = '<div class="btn btn-success cost-save" data-id="'.$cost->id.'">Save</div>';
            $temp['4'] .= '<div class="btn btn-danger cost-delete" data-id="'.$cost->id.'">Delete</div>';
            $temp['4'] .= '<div class="btn btn-info cost-add" data-id="'.$cost->id.'">Add to month</div>';

            $output[] = $temp;

        }

        $results['data'] = $output;
        $results['recordsTotal'] = $total;
        $results['recordsFiltered'] = $total;
        $response = Response::make($results);
        $response->header('Content-Type', 'application/json');
        return $response;
    }

    public function getRecurringDT()
    {
        $start = 0;
        $length = 10;
        $dt = new DateTime();

        if(Input::has('start'))
        {
            $start = Input::get('start');
        }

        if(Input::has('length'))
        {
            $length = Input::get('length');
        }

        if(Input::has('dt'))
        {
            $dt = Input::get('dt');

            $dt = DateTime::createFromFormat('Y-m-d', $dt);

            if(!$dt)
            {
                return Response::json("Invalid date", 404);
            }
        }

        $costs = DB::table('profit_loss_entity_monthly_profile as pm')
                        ->leftJoin('profit_loss_entities as ple', 'pm.ple_id', '=', 'ple.id')
                        ->whereRaw('DATE_FORMAT(pm.date_applicable, "%Y-%m") = "'.$dt->format('Y-m').'"')
                        ->select('pm.date_applicable as date_applicable', 'ple.val as val', 'pm.val_modifier as val_modifier', 'pm.ple_id as ple_id', 'pm.id as id', 'ple.type as type', 'ple.title as title')
                        ->orderBy('id', 'ASC')
                        ->skip($start)
                        ->take($length)
                        ->get();

        $output = [];

        $total = DB::table('profit_loss_entity_monthly_profile as pm')
            ->whereRaw('DATE_FORMAT(pm.date_applicable, "%Y-%m") = '.$dt->format('Y-m'))
            ->select(DB::raw('COUNT(*) as ttl'))
            ->first();

        $total = $total->ttl;

        foreach($costs as $cost)
        {
            $temp = [];

            $temp['0'] = $cost->id;
            $temp['1'] = $cost->title;
            if($cost->val_modifier)
            {
                $temp['2'] = '<input type="text" name="" id="rcost-val-'.$cost->id.'" value="'.$cost->val_modifier.'">';
            }
            else
            {
                $temp['2'] = '<input type="text" name="" id="rcost-val-'.$cost->id.'" value="'.$cost->val.'">';
            }

            if($cost->type == 'fixed_cost')
            {
                $temp['3'] = 'Fixed cost';
            }
            else
            {
                $temp['3'] = 'Variable cost';
            }

            $temp['4'] = '<div class="btn btn-success rcost-save" data-id="'.$cost->id.'">Save</div>';
            $temp['4'] .= '<div class="btn btn-danger rcost-delete" data-id="'.$cost->id.'">Delete</div>';
            $output[] = $temp;
        }

        $results['data'] = $output;
        $results['recordsTotal'] = $total;
        $results['recordsFiltered'] = $total;
        $response = Response::make($results);
        $response->header('Content-Type', 'application/json');
        return $response;
    }

    public function addFixedCostsToMonth()
    {
        $input = Input::all();

        if(Input::has('date'))
        {
            $dt = DateTime::createFromFormat('Y-m-d', $input['date']);

            if($dt)
            {
                $fc = DB::table('profit_loss_entities')
                        ->where('type', '=', "fixed_cost")
                        ->get();

                foreach($fc as $c)
                {
                    DB::table('profit_loss_entity_monthly_profile')
                            ->insert([
                                'ple_id'=>$c->id,
                                'val_modifier'=>$c->val,
                                'date_applicable'=>$dt->format('Y-m-d'),
                            ]);
                }

                return Response::json("success",200);
            }
            return Response::json("Invalid date format",404);
        }

        return Response::json('Requires date',404);
    }
}