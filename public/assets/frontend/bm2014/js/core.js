/**************************************************************************************
 * CSRF TOKEN
 *************************************************************************************/

$.ajaxSetup({headers: {'csrftoken': '{{ csrf_token() }}'}});

/**************************************************************************************
 *  LOCAL POP UP BOX
 *************************************************************************************/

jQuery(document).ready(function () {

    jQuery('.popupOpen').click(function () {
        jQuery('.popup').addClass('animated fadeIn');
        jQuery('.mask').addClass('animated fadeIn');
        jQuery('.mask').css('display','block');
    });

    jQuery('.popupClose').click(function () {
        jQuery('.popup').removeClass('animated fadeIn');
        jQuery('.mask').css('display','none');
        jQuery('.mask').removeClass('animated fadeIn');
    });

    jQuery('header .searcharea input').on('focus', function(){
        jQuery(this).closest('.parent').addClass('focused');
    });

    jQuery('header .searcharea input').on('focusout', function(){
        jQuery(this).closest('.parent').removeClass('focused');
    });

    jQuery(window).on('scroll',function(){
        if(document.body.scrollTop > 0)
        {
            jQuery('header').addClass('scrolled');
            jQuery('#topheader').addClass('scrolled');
        }
        else
        {
            jQuery('header').removeClass('scrolled');
            jQuery('#topheader').removeClass('scrolled');
        }
    })
});


/**************************************************************************************
 *  ZOPIM
 *
 ************************************************************************************/
/*
window.$zopim || (function (d, s) {
    var z = $zopim = function (c) {
        z._.push(c)
    }, $ = z.s = d.createElement(s), e = d.getElementsByTagName(s)[0];
    z.set = function (o) {
        z.set._.push(o)
    };
    z._ = [];
    z.set._ = [];
    $.async = !0;
    $.setAttribute('charset', 'utf-8');
    $.src = '//v2.zopim.com/?{{trans("homepage.zopim-code")}}';
    z.t = +new Date;
    $.type = 'text/javascript';
    e.parentNode.insertBefore($, e)
})(document, 'script');
*/

/**************************************************************************************
 *  YAMEX
 *************************************************************************************/

(function (d, w, c) {
    if(window.location.hostname=="www.footballticketpad.com") {
        (w[c] = w[c] || []).push(function () {
            try {
                w.yaCounter38479495 = new Ya.Metrika({
                    id: 38479495,
                    clickmap: true,
                    trackLinks: true,
                    accurateTrackBounce: true,
                    webvisor: true,
                    ecommerce: "dataLayer"
                });
            } catch (e) {
            }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () {
                n.parentNode.insertBefore(s, n);
            };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://cdn3.footballticketpad.com/frontend/bm2014/js/yandex-local.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else {
            f();
        }
    }
})(document, window, "yandex_metrika_callbacks");

/**************************************************************************************
 *  SOME STRAY CODE
 *************************************************************************************/
Foundation.global.namespace = '';

/**************************************************************************************
 *  MOBILE MENU
 *************************************************************************************/

$( document ).ready(function() {
    jQuery('.mobileMenuTopGames').click(function(){
        if( jQuery('.mobileMenuHotTickets').css('display')=='none'){
            closeAll();
            jQuery('.mobileMenuHotTickets').css('display','block').addClass('animated fadeInDown')
        }else{
            closeAll()
        }
    });

    jQuery('.mobileMenuTopTeams').click(function(){
        if( jQuery('.mobileMenuHotTeams').css('display')=='none'){
            closeAll();
            jQuery('.mobileMenuHotTeams').css('display','block').addClass('animated fadeInDown')
        }else{
            closeAll()
        }
    });

    jQuery('.mobileMenuTopLeague').click(function(){
        if( jQuery('.mobileMenuLeague').css('display')=='none'){
            closeAll();
            jQuery('.mobileMenuLeague').css('display','block').addClass('animated fadeInDown')
        }else{
            closeAll()
        }
    });

    jQuery('.mobileMenuClose').click(function(){
        jQuery('.mobileMenu').removeClass('animated fadeInLeft');
        jQuery('.mobileMenu').css('display','none');
    });

    jQuery('.menuGo').click(function(){
        jQuery('.mobileMenu').addClass('animated fadeInLeft');
        jQuery('.mobileMenu').css('display','block');
    });

    jQuery('.footermobileswitchers .mobileMenuTopTicketsLower').click(function(){
        if(jQuery('.greenlist  .mobileMenuTextLeft').css('display')=='none') {
            jQuery('.greenlist  .mobileMenuTextLeft').css('display', 'block');
        }else{
            jQuery('.greenlist  .mobileMenuTextLeft').css('display', 'none');
        }

    });

    jQuery('.footermobileswitchers .mobileMenuTopTeamsLower').click(function(){
        if(jQuery('.bluelist  .mobileMenuTextLeft').css('display')=='none') {
        jQuery('.bluelist  .mobileMenuTextLeft').css('display','block');
    }else{
        jQuery('.bluelist  .mobileMenuTextLeft').css('display', 'none');
    }

    });

    jQuery('.footermobileswitchers .mobileMenuToLeagssLower').click(function(){
        if(jQuery('.redlist  .mobileMenuTextLeft').css('display')=='none') {
        jQuery('.redlist  .mobileMenuTextLeft').css('display','block');
    }else{
    jQuery('.redlist  .mobileMenuTextLeft').css('display', 'none');
}

    })
    
});

function closeAll(){
    jQuery('.mobileMenuHotTeams').css('display', 'none').removeClass('animated fadeInDown');
    jQuery('.mobileMenuHotTeams').removeClass('animated fadeOutDown');
    jQuery('.mobileMenuHotTickets').css('display', 'none').removeClass('animated fadeInDown');
    jQuery('.mobileMenuHotTickets').removeClass('animated fadeOutDown');
    jQuery('.mobileMenuLeague').css('display', 'none').removeClass('animated fadeInDown');
    jQuery('.mobileMenuLeague').removeClass('animated fadeOutDown')
}

