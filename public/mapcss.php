<?php

use Leafo\ScssPhp\Compiler;
use Leafo\ScssPhp\Server;
use Leafo\ScssPhp\Formatter\Expanded;
use Leafo\ScssPhp\Formatter\Nested;
use Leafo\ScssPhp\Formatter\Compressed;
use Leafo\ScssPhp\Formatter\Compact;
use Leafo\ScssPhp\Formatter\Crunched;
$directory = __DIR__ . "/assets/css/scssphp/";
require_once $directory . "scss.inc.php";
Server::serveFrom(__DIR__ . "/assets/css/");
$scss = new Compiler();


/**
 * Lets Import The Imports
 */
$scss->setImportPaths($directory . "../");
$coreString = "

            \$color1: rgba(255, 211, 79, 1);
            \$color2: rgba(230, 56, 133, 1);
            \$color3: rgba(96, 202, 221, 1);
            \$color4: rgba(125, 196, 104, 1);
            \$color5: rgba(248, 142, 0, 1);
            \$color6: rgb(217, 131, 250);

            \$color_ll: rgba(227, 41, 125, 1);
            \$color_lu: rgba(99, 201, 218, 1);
            \$color_sl: rgba(126, 194, 109, 1);
            \$color_su: rgba(255, 208, 92, 1);
            \$color_aw: rgba(199, 197, 192, 1);
            \$color_other: rgba(206, 43, 43, 1);

            \$lighter: 30%;
            \$light: 20%;

            \$darker: 30%;
            \$dark: 20%;



            //////


            @mixin fill-stroke(\$color) {
    fill: \$color !important;
              stroke: \$color !important;
            }

            @mixin fill-stroke-grey(\$color) {
    fill: grayscale(\$color) !important;
              stroke: grayscale(\$color) !important;
            }

            @mixin all-on-color-events(\$color,\$cat) {
    \$catpill: \$cat + \"_tickpill\";
              \$catTrigger: \"trigger_\" + \$cat + \"\";
              \$catButton: \"\" + \$cat + \"_button\";
              .#{\$cat} {
                @include fill-stroke-grey(\$color);
              }
              .#{\$cat}.click {
                @include fill-stroke(\$color);
              }
              .#{\$cat}.hover {
                @include fill-stroke(\$color);
              }
              .#{\$cat}.toggle {
                @include fill-stroke(\$color);
              }
              .#{\$cat}.hover.toggle {
                fill: lighten(\$color, \$light) !important;
                stroke: lighten(\$color, \$light) !important;
              }
              .#{\$cat}.blockhere {
                fill: darken(\$color, 30%) !important;
                stroke: darken(\$color, 30%) !important;
              }
              .#{\$cat}.blockhere.hover {
                fill: darken(\$color, 30%) !important;
                stroke: darken(\$color, 30%) !important;
              }
              .#{\$cat}.blockhere.hover.toggle {
                fill: darken(\$color, 30%) !important;
                stroke: darken(\$color, 30%) !important;
              }
              .#{\$cat}.blockhere.toggle {
                fill: darken(\$color, 30%) !important;
                stroke: darken(\$color, 30%) !important;
              }
              .#{\$catTrigger} {
                @media (min-width: 601px) {
                  border-left: 3px solid \$color;
                }
                @media (max-width: 600px) {
                  border-left: 10px solid \$color;
                }
              }
              .#{\$catTrigger}.hover {
                background: lighten(\$color, \$lighter) !important;
              }
              .#{\$catTrigger}.hover td {
                background: lighten(\$color, \$lighter) !important;
              }
              .#{\$catpill} {
                background: lighten(\$color, \$lighter) !important;
                border-left: 3px solid \$color;
              }
              .#{\$catButton}.active {
                padding: 10px;
                color: #fff;
                background-color: \$color;
                text-align: center;
                letter-spacing: .5px;
                vertical-align: middle;
                border-radius: 4px;
                font-weight: bold;
                margin: 10px;
                border: 0;
                -webkit-box-shadow: 0px 14px 0px 14px darken(\$color, \$lighter);
                -moz-box-shadow: 0px 14px 0px 14px darken(\$color, \$lighter);
                box-shadow: 0px 7px 0px 0px darken(\$color, \$lighter);
              }
              .#{\$catButton} {
                display: block !important;
                padding: 10px;
                color: #fff;
                background-color: grayscale(\$color);
                text-align: center;
                letter-spacing: .5px;
                vertical-align: middle;
                border-radius: 4px;
                font-weight: bold;
                margin: 10px;
                border: 0;
                -webkit-box-shadow: 0px 14px 0px 14px grayscale(darken(\$color, \$lighter));
                -moz-box-shadow: 0px 14px 0px 14px grayscale(darken(\$color, \$lighter));
                box-shadow: 0px 7px 0px 0px grayscale(darken(\$color, \$lighter));
              }
            }

            ////
            .cat_1, .cat_2, .cat_3, .cat_4, .cat_vip {
              stroke-width: 2;
              transition: all 1.0s !important;
            }


            .guarenteetxt-container {
              display: none !important;
            }

            .tickpill {
              padding: 10px;
              margin-bottom: 2px;
              width: 50%;
              float: left;
            }

            .resetButton {
              background: #E3397F !important;
              border: none !important;
              padding: 11px !important;
              color: white !important;
              border-radius: 7px !important;
              font-size: 15px !important;
            }

            .mobileFilterTab {
              perspective: 1500px;
              perspective-origin: 0% 50%;
              -webkit-transition: all 2s;
              transform-style: preserve-3d;
              transform-origin: 100% 50%;
              visibility: visible;
              transform: translate3d(100%, 0, 0) rotateY(0deg);
            }

            .mobileFilterTab.open {
              transform: translate3d(0%, 0, 0) rotateY(0deg);
            }

            .closeFilter_button {
              width: 95%;
            }

            .seatzones button {
              display: none;
            }

            .seatzones button {
              float: left;
              width: 43%;
              text-align: center;
            }

            ";

$maps = MapC::get();
$colors = [
    ['col' => '#CC44AA', 'cat' => 'col-xxs']
];
foreach($maps as $map){
    if (!empty($map->color)) {
        $colors[]['col'] = $map->color;
    }

    if (!empty($map->right)) {
        $colors[]['cat'] = $map->right;
    }
}



foreach ($colors as $x) {
    if (!empty($x['col'])) {
        if (!empty($x['cat'])) {
            $coreString .= "@include all-on-color-events(" . $x['col'] . ", \$cat: '" . $x['cat'] . "');";
        }
    }
}

echo $scss->compile($coreString);