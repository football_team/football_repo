<?php
/*////////////////////////////////////////////////////////////////////////////////
 \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\  Additional Fee  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
 /////////////////////////////////////////////////////////////////////////////////
 \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ NOTICE OF LICENSE\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
 ///////                                                                   ///////
 \\\\\\\ This source file is subject to the Open Software License (OSL 3.0)\\\\\\\
 ///////   that is bundled with this package in the file LICENSE.txt.      ///////
 \\\\\\\   It is also available through the world-wide-web at this URL:    \\\\\\\
 ///////          http://opensource.org/licenses/osl-3.0.php               ///////
 \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
 ///////                      * @category   EH                            ///////
 \\\\\\\                      * @package    EH_Additionalfee                \\\\\\\
 ///////    * @author     Suneet Kumar <suneet64@gmail.com>               ///////
 \\\\\\\                                                                   \\\\\\\
 /////////////////////////////////////////////////////////////////////////////////
 \\\\\\* @copyright  Copyright 2014 © www.extensionhut.com All right reserved\\\\\
 /////////////////////////////////////////////////////////////////////////////////
 */
 
class EH_Additionalfees_Adminhtml_AdditionalfeesController extends Mage_Adminhtml_Controller_Action
{
	protected function _initAction() 
	{
		$this->loadLayout()
		->_setActiveMenu('additionalfees/items')
		->_addBreadcrumb(Mage::helper('adminhtml')->__('Items Manager'), Mage::helper('adminhtml')->__('Item Manager'));
		return $this;
	}

	public function indexAction() 
	{
		/*
		* LOAD THE PAGE LAYOUT
		*/
		$this->_initAction()
		->renderLayout();
	}
	
	public function newAction()
    {
        $this->_forward('edit');
    }
 
    public function editAction()
    {
        $id = $this->getRequest()->getParam('id', null);
        $model = Mage::getModel('additionalfees/additionalfees');
        if ($id) {
            $model->load((int) $id);
            if ($model->getId()) {
                $data = Mage::getSingleton('adminhtml/session')->getFormData(true);
                if ($data) {
                    $model->setData($data)->setId($id);
                }
            } else {
                Mage::getSingleton('adminhtml/session')->addError(Mage::helper('additionalfees')->__('Example does not exist'));
                $this->_redirect('*/*/');
            }
        }
        Mage::register('additionalfees_data', $model);
 
        $this->loadLayout();
		$this->_setActiveMenu("additionalfees/additionalfees");
        $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
		$this->_addContent($this->getLayout()->createBlock("additionalfees/adminhtml_additionalfees_edit"))->_addLeft($this->getLayout()->createBlock("additionalfees/adminhtml_additionalfees_edit_tabs"));
		$this->renderLayout();
    }
	/*
	* ACTION TO SAVE THE NUMBER OF Additional FEES CHANGES MADE BY ADMIN
	*/	
	public function saveAction() {

		$post_data=$this->getRequest()->getPost();
		//print_r($post_data);exit;
		if ($post_data) {

			try {
				if(isset($post_data['category']))
				    $post_data['category']=implode(",",$post_data['category']);
				if(isset($post_data['stores']))
				    $post_data['store_id']=implode(",",$post_data['stores']);
				
				$additionalfeesModel = Mage::getModel("additionalfees/additionalfees")
				->addData($post_data)
				->setId($this->getRequest()->getParam("id"))
				->save();

				Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Fee was successfully saved"));
				Mage::getSingleton("adminhtml/session")->setModelnameData(false);

				if ($this->getRequest()->getParam("back")) {
					$this->_redirect("*/*/edit", array("id" => $additionalfeesModel->getId()));
					return;
				}
				$this->_redirect("*/*/");
				return;
			} 
			catch (Exception $e) {
				Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
				Mage::getSingleton("adminhtml/session")->setModelnameData($this->getRequest()->getPost());
				$this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
			return;
			}

		}
		$this->_redirect("*/*/");

	}

	public function deleteAction()
	{
		if( $this->getRequest()->getParam("id") > 0 ) {
			try {
				$brandsModel = Mage::getModel("additionalfees/additionalfees");
				$brandsModel->setId($this->getRequest()->getParam("id"))->delete();
				Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Item was successfully deleted"));
				$this->_redirect("*/*/");
			} 
			catch (Exception $e) {
				Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
				$this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
			}
		}
		$this->_redirect("*/*/");
	}

	public function massDeleteAction() {
		$resource = Mage::getSingleton('core/resource');
		$write= $resource->getConnection('core_write');	  
		$additionalfeesIds = $this->getRequest()->getParams('additionalfees');
		 		
		if(!is_array($additionalfeesIds['additionalfees'])) {
				Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select item(s)'));
		} else {
		    try {
		        foreach ($additionalfeesIds['additionalfees'] as $additionalfeesId) {
		            $additionalfees = Mage::getModel('additionalfees/additionalfees')->load($additionalfeesId);			   			  
		            $additionalfees->delete();		
		        }
		        Mage::getSingleton('adminhtml/session')->addSuccess(
		            Mage::helper('adminhtml')->__(
		                'Total of %d record(s) were successfully deleted', count($additionalfeesIds['additionalfees'])
		            )
		        );
		    } catch (Exception $e) {
		        Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
		    }
		}
		$this->_redirect('*/*/index');
	    }

	protected function _sendUploadResponse($fileName, $content, $contentType='application/octet-stream')
	{
		$response = $this->getResponse();
		$response->setHeader('HTTP/1.1 200 OK','');
		$response->setHeader('Pragma', 'public', true);
		$response->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0', true);
		$response->setHeader('Content-Disposition', 'attachment; filename='.$fileName);
		$response->setHeader('Last-Modified', date('r'));
		$response->setHeader('Accept-Ranges', 'bytes');
		$response->setHeader('Content-Length', strlen($content));
		$response->setHeader('Content-type', $contentType);
		$response->setBody($content);
		$response->sendResponse();
		die;
	}

	public function callmeAction()
	{
	    $post=$this->getRequest()->getParams();		
	    $sym=Mage::app()->getLocale()->currency(Mage::app()->getStore()->getCurrentCurrencyCode())->getSymbol();
	    $oQuote = Mage::getSingleton('adminhtml/sales_order_create')->getQuote();
	    $aProductsInQuote = $oQuote->getAllItems();
	    foreach ( $aProductsInQuote as $oProductInQuote ) {
		
		$myprice=unserialize($oProductInQuote->getPaymentFee());			
		$strExtra=unserialize($oProductInQuote->getPaymentStr());		
		if($post['cval']==0)
		{
			unset($myprice[$post['cname']]);		
			unset($strExtra[$post['cname']]);
		}
		else { 
			$myval=explode("_",$post['cval']);			
			if($myval[0]==$oProductInQuote->getProductId()){ 
				/*if($oProductInQuote->getProduct_type()=="bundle") {
													
				    $item_id =  $oProductInQuote->getId()+1;
				    $bundle_item = Mage::getModel('sales/quote_item')->load($item_id);					    
				    $product_id  = $bundle_item->getProduct_id();				
				
				} else { */
				    $product_id =  $oProductInQuote->getProductId();	
				//}

				$feedata=Mage::getModel('additionalfees/additionalfees')->load($myval[1]);
				if($feedata['feetype']=='Percentage'){
					
				    $myprice['O'.$oProductInQuote->getProductId()."_".$myval[1]][]=($oProductInQuote->getPrice()*$feedata['feeamount']/100);
				    $myprice['O'.$oProductInQuote->getProductId()."_".$myval[1]][]=$feedata['flatfee'];
				    $strExtra['O'.$oProductInQuote->getProductId()."_".$myval[1]]=$feedata['title'].": ".$feedata['feeamount']."%";
				    $additionalarr['O'.$oProductInQuote->getProductId()."_".$myval[1]]=$post['cval'];							

				} else {
			
					$myprice['O'.$oProductInQuote->getProductId()."_".$myval[1]][]=$feedata['feeamount'];
					$myprice['O'.$oProductInQuote->getProductId()."_".$myval[1]][]=$feedata['flatfee'];
					$strExtra['O'.$oProductInQuote->getProductId()."_".$myval[1]]=$feedata['title'].": ".$sym.$feedata['feeamount'];
					$additionalarr['O'.$oProductInQuote->getProductId()."_".$myval[1]]=$post['cval'];					

				} 
				
			}//if				
			
		}//else 

		if($oProductInQuote->getProduct_type()=="bundle")
		{			
		      $oProductInQuote->setPaymentFee(serialize($myprice));		
		      $oProductInQuote->setPaymentStr(serialize($strExtra));
		      $oProductInQuote->setAdditionalExtrafee(serialize($additionalarr));	     
		      
		      foreach ($oProductInQuote->getChildren() as $child) {					  
			    $child->setPaymentFee(serialize($myprice));
			    $child->setPaymentStr(serialize($strExtra));			    
			    break;
		      }		
		      $oProductInQuote->calcRowTotal(); 
		      $oProductInQuote->save();			
		      

		} else {
			      
		      $oProductInQuote->setPaymentFee(serialize($myprice));		
		      $oProductInQuote->setPaymentStr(serialize($strExtra));
		      $oProductInQuote->setAdditionalExtrafee(serialize($additionalarr));	
		      $oProductInQuote->calcRowTotal();	      
		      $oProductInQuote->save();

		}
		
        	
	    }//end foreach	   
	    $oQuote->collectTotals();
	    $oQuote->save();

	}
    
}
