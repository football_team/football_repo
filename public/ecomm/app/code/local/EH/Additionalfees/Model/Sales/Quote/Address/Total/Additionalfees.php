<?php
/*////////////////////////////////////////////////////////////////////////////////
 \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\  Additional Fee  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
 /////////////////////////////////////////////////////////////////////////////////
 \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ NOTICE OF LICENSE\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
 ///////                                                                   ///////
 \\\\\\\ This source file is subject to the Open Software License (OSL 3.0)\\\\\\\
 ///////   that is bundled with this package in the file LICENSE.txt.      ///////
 \\\\\\\   It is also available through the world-wide-web at this URL:    \\\\\\\
 ///////          http://opensource.org/licenses/osl-3.0.php               ///////
 \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
 ///////                      * @category   EH                            ///////
 \\\\\\\                      * @package    EH_Additionalfee                \\\\\\\
 ///////    * @author     Suneet Kumar <suneet64@gmail.com>               ///////
 \\\\\\\                                                                   \\\\\\\
 /////////////////////////////////////////////////////////////////////////////////
 \\\\\\* @copyright  Copyright 2014 © www.extensionhut.com All right reserved\\\\\
 /////////////////////////////////////////////////////////////////////////////////
 */
 
class EH_Additionalfees_Model_Sales_Quote_Address_Total_Additionalfees extends Mage_Sales_Model_Quote_Address_Total_Abstract
{
	public function collect(Mage_Sales_Model_Quote_Address $address)
	{
		$model = Mage::getSingleton('additionalfees/additionalfees');

		/*
		* CHECK IF EH FEES IS ACTIVE
		*/
		if (Mage_Sales_Model_Quote_Address::TYPE_SHIPPING == $address->getAddressType() &&  $model->getAdditionalfees())
		{
			$amount = $model->getGiftAmount();
			$txt=$model->getGiftText();	
			$method = $address->getShippingMethod();
			if($method)
			{
				$address->setShippingAmount($address->getShippingAmount() + $amount);
				$address->setBaseShippingAmount($address->getBaseShippingAmount() + $amount);
				//$address->setGrandTotal($address->getGrandTotal() + $amount);
				$address->setShippingDescription($address->getShippingDescription().$txt);
				//$address->setBaseGrandTotal($address->getBaseGrandTotal() + $amount);
			}
		}
		return $this;
	}


	public function fetch(Mage_Sales_Model_Quote_Address $address)
	{
		return $this;
	}
}
