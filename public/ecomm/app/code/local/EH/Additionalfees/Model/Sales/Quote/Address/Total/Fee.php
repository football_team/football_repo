<?php
/*////////////////////////////////////////////////////////////////////////////////
 \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\  Additional Fee  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
 /////////////////////////////////////////////////////////////////////////////////
 \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ NOTICE OF LICENSE\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
 ///////                                                                   ///////
 \\\\\\\ This source file is subject to the Open Software License (OSL 3.0)\\\\\\\
 ///////   that is bundled with this package in the file LICENSE.txt.      ///////
 \\\\\\\   It is also available through the world-wide-web at this URL:    \\\\\\\
 ///////          http://opensource.org/licenses/osl-3.0.php               ///////
 \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
 ///////                      * @category   EH                            ///////
 \\\\\\\                      * @package    EH_Additionalfee                \\\\\\\
 ///////    * @author     Suneet Kumar <suneet64@gmail.com>               ///////
 \\\\\\\                                                                   \\\\\\\
 /////////////////////////////////////////////////////////////////////////////////
 \\\\\\* @copyright  Copyright 2014 © www.extensionhut.com All right reserved\\\\\
 /////////////////////////////////////////////////////////////////////////////////
 */
 
class EH_Additionalfees_Model_Sales_Quote_Address_Total_Fee extends Mage_Sales_Model_Quote_Address_Total_Abstract
{
	protected $_code = 'fee';

	public function collect(Mage_Sales_Model_Quote_Address $address)
	{
		$session = Mage::getSingleton('checkout/session');		
		if(count($session->getQuote()->getAllItems())==0 || $session->getQuoteId()=='')	
		{			
		      $session->setAdditionalfee();
		      $session->setBaseAdditionalfee();		    	
		      $session->setDetailAdditionalfee();		
		}	
		return $this;	      
	}
  
	public function additionalTotal(Mage_Sales_Model_Quote_Address $address)
	{

	      $address->setAdditionalfee(0);
	      $address->setBaseAdditionalfee(0);

	      //we can return if there are no items
	      $items = $address->getAllItems();
	      if (!count($items)) {
		  return $this;
	      }

	      $quote = $address->getQuote();
	      $quoteId = $quote->getId();
	      if (!is_numeric($quoteId)) {
		  return $this;
	      }
	      Mage::getSingleton('checkout/session')->setFeeList();
	      $mandatoryfee = Mage::getModel('additionalfees/additionalfees')->getOrderFee($address->getSubtotal());
	      
	      $detailsFees = Mage::getSingleton('checkout/session')->getDetailAdditionalfee();
	      
	      $taxes = null;      
	      if ($address->getAppliedTaxes()) {
		  $taxes = $address->getAppliedTaxes();
	      }
	      
	      if(!empty($mandatoryfee)) {	      
		      $fee = $mandatoryfee[0];		      
		      $balance=$fee;
		      $address->setBaseAdditionalfeeAmount($balance);	
		      $address->setAdditionalfee($balance);
		      $address->setBaseAdditionalfee($balance);     
		      
		      if ($taxes) {
			  $prices = Mage::helper('additionalfees')->getAdditionalPrice($mandatoryfee[1], $address->getSubtotal(), $address->getShippingInclTax(), $taxes);
			  $taxes = Mage::helper('additionalfees')->setAdditionalTaxes($mandatoryfee[1],$taxes);
			  $address->setAppliedTaxes($taxes);
			  $address->setAdditionalfeeExclTax($prices->getFeesExclTax());
			  $address->setBaseAdditionalfeeInclTax($prices->getBaseFeesInclTax());			    
			  $address->setDetailAdditionalfee(serialize($prices->getDetailsFees()));	
			  $quote->setDetailAdditionalfee(serialize($prices->getDetailsFees()));		   
			  
		      } else {

			  $address->setDetailAdditionalfee(serialize($mandatoryfee[1]));
			  $address->setGrandTotal($address->getGrandTotal() + $address->getAdditionalfee());
			  $address->setBaseGrandTotal($address->getBaseGrandTotal() + $address->getBaseAdditionalfee());
		      
			  $quote->setAdditionalfee($address->getAdditionalfee());
			  $quote->setBaseAdditionalfee($address->getBaseAdditionalfee());
			  $quote->setDetailAdditionalfee(serialize($mandatoryfee[1]));
			
			  $quote->setGrandTotal($quote->getGrandTotal() + $address->getAdditionalfee());
			  $quote->setBaseGrandTotal($quote->getBaseGrandTotal()+ $address->getBaseAdditionalfee());

		      }		 
		      
	      }

	      if($detailsFees!='') {	
		      
		      $orgMArr=unserialize($address->getDetailAdditionalfee());	
		      $orgOArr=unserialize($detailsFees);
		      if(!empty($orgMArr)) {				  				
			$final=array_merge($orgMArr,$orgOArr);
		      } else {
			$final=$orgOArr; 	  
		      }
		      
		      if ($taxes) {
			  $prices = Mage::helper('additionalfees')->getAdditionalPrice($final, $address->getSubtotal(), $address->getShippingInclTax(), $taxes);
			  
			  $taxes = Mage::helper('additionalfees')->setAdditionalTaxes($final,$taxes);
			  $address->setAppliedTaxes($taxes);
			  $address->setAdditionalfeeExclTax($prices->getFeesExclTax());
			  $address->setBaseAdditionalfeeInclTax($prices->getBaseFeesInclTax());
			  
			  $address->setDetailAdditionalfee(serialize($prices->getDetailsFees()));
			  $address->setBaseAdditionalfee($address->getBaseAdditionalfee()+Mage::getSingleton('checkout/session')->getBaseAdditionalfee());
			  $address->setAdditionalfee($address->getAdditionalfee()+ Mage::getSingleton('checkout/session')->getAdditionalfee());

			  $quote->setDetailAdditionalfee(serialize($prices->getDetailsFees()));

		      } else {
			  $address->setAdditionalfee($address->getAdditionalfee()+Mage::getSingleton('checkout/session')->getAdditionalfee());
			  $address->setBaseAdditionalfee($address->getBaseAdditionalfee()+Mage::getSingleton('checkout/session')->getBaseAdditionalfee());
			  $address->setDetailAdditionalfee(serialize($final));				   
			  $address->setGrandTotal($address->getGrandTotal() + Mage::getSingleton('checkout/session')->getAdditionalfee());
			  $address->setBaseGrandTotal($address->getBaseGrandTotal() + Mage::getSingleton('checkout/session')->getBaseAdditionalfee());

			  $quote->setAdditionalfee((float) $quote->getAdditionalfee()+ Mage::getSingleton('checkout/session')->getAdditionalfee());
			  $quote->setBaseAdditionalfee((float) $quote->getBaseAdditionalfee()+ Mage::getSingleton('checkout/session')->getBaseAdditionalfee());
			  $quote->setDetailAdditionalfee(serialize($final));

			  $quote->setGrandTotal($quote->getGrandTotal() + Mage::getSingleton('checkout/session')->getAdditionalfee());
			  $quote->setBaseGrandTotal($quote->getBaseGrandTotal()+ Mage::getSingleton('checkout/session')->getBaseAdditionalfee());
		      }		
		      

		      
	      }	
	      if ($taxes) 
	      {					    
		    $address->setGrandTotal($address->getGrandTotal() + $prices->getFees());
		    $address->setBaseGrandTotal($address->getBaseGrandTotal() + $prices->getBaseFeesInclTax());
	      
		    $address->setTaxAmount($address->getTaxAmount() + $prices->getFees() - $address->getAdditionalfeeExclTax());
		    $address->setBaseTaxAmount($address->getBaseTaxAmount() + $address->getBaseAdditionalfeeInclTax() -  $prices->getBaseFees());

		    $quote->setGrandTotal($quote->getGrandTotal() + $prices->getFees());
		    $quote->setBaseGrandTotal($quote->getBaseGrandTotal()+ $prices->getBaseFeesInclTax());
	      }  
	      Mage::getSingleton('checkout/session')->setFeeList($address->getDetailAdditionalfee());
	      return $this;

	}

	public function fetch(Mage_Sales_Model_Quote_Address $address)
	{
// 		$arr=unserialize($address->getDetailAdditionalfee());		  
// 		
// 		$str=Mage::helper('additionalfees')->__('Additional Fees')."<br/>"; 		
// 		foreach($arr as $key=>$val)
// 		{
// 			$str.=$val['title'].": ".Mage::helper('core')->currency($val['price'],true,false)."<br/>";	
// 		}	
		
		$amt = $address->getAdditionalfee();
		if($amt > 0) {    
		    $address->addTotal(array(
			    'code'=>$this->getCode(),
			    'strong'=> false,
			    'title'=> Mage::getStoreConfig('additionalfees_section/additionalfees_group1/title')!='' ? Mage::getStoreConfig('additionalfees_section/additionalfees_group1/title') : Mage::helper('additionalfees')->__('Additional Fees'),			
			    'value'=> $amt
		    ));
		}
		return $this;
	}		
	
}
