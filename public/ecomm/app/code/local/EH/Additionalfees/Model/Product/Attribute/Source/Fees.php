<?php  
/*////////////////////////////////////////////////////////////////////////////////
 \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\  Additional Fee  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
 /////////////////////////////////////////////////////////////////////////////////
 \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ NOTICE OF LICENSE\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
 ///////                                                                   ///////
 \\\\\\\ This source file is subject to the Open Software License (OSL 3.0)\\\\\\\
 ///////   that is bundled with this package in the file LICENSE.txt.      ///////
 \\\\\\\   It is also available through the world-wide-web at this URL:    \\\\\\\
 ///////          http://opensource.org/licenses/osl-3.0.php               ///////
 \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
 ///////                      * @category   EH                            ///////
 \\\\\\\                      * @package    EH_Additionalfee                \\\\\\\
 ///////    * @author     Suneet Kumar <suneet64@gmail.com>               ///////
 \\\\\\\                                                                   \\\\\\\
 /////////////////////////////////////////////////////////////////////////////////
 \\\\\\* @copyright  Copyright 2014 © www.extensionhut.com All right reserved\\\\\
 /////////////////////////////////////////////////////////////////////////////////
 */
 
class EH_Additionalfees_Model_Product_Attribute_Source_Fees extends Mage_Eav_Model_Entity_Attribute_Source_Abstract
{

  public function getAllOptions()
  {   
	  $fetchlist = Mage::getModel('additionalfees/additionalfees')->getFeeList('Product');  // for type product
	 
			  $arcount= count($fetchlist);
	  $crcount=1;	
	  if (!$this->_options) {	  
		  $contarray = '$this->_options = array(';
		  $valu_arr = array();
		  $j= 0;
		  $sel=0;
		// $valu_arr[$j]['value'] = "0";
		// $valu_arr[$j]['label'] = "Select";
		foreach($fetchlist as $imgkey=>$imgvals)
		  { 
			//print_r($imgvals);exit;
			$j++;
			$crcount++;
			
			
			//$valu_arr[$j]['value'] = $imgkey."~".$imgvals;
			//$valu_arr[$j]['label'] = $imgvals;
			
			$valu_arr[$j]['value'] = $imgkey;
			$valu_arr[$j]['label'] = $imgvals['title'];
			
			
		  }
		// echo "<pre>"; print_r($valu_arr);
		  //exit;
		  $this->_options =  $valu_arr;
		 
		   return $this->_options;
	 
	 }
 }
}
?>
