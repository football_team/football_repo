<?php
/*////////////////////////////////////////////////////////////////////////////////
 \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\  Additional Fee  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
 /////////////////////////////////////////////////////////////////////////////////
 \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ NOTICE OF LICENSE\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
 ///////                                                                   ///////
 \\\\\\\ This source file is subject to the Open Software License (OSL 3.0)\\\\\\\
 ///////   that is bundled with this package in the file LICENSE.txt.      ///////
 \\\\\\\   It is also available through the world-wide-web at this URL:    \\\\\\\
 ///////          http://opensource.org/licenses/osl-3.0.php               ///////
 \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
 ///////                      * @category   EH                            ///////
 \\\\\\\                      * @package    EH_Additionalfee                \\\\\\\
 ///////    * @author     Suneet Kumar <suneet64@gmail.com>               ///////
 \\\\\\\                                                                   \\\\\\\
 /////////////////////////////////////////////////////////////////////////////////
 \\\\\\* @copyright  Copyright 2014 © www.extensionhut.com All right reserved\\\\\
 /////////////////////////////////////////////////////////////////////////////////
 */
 
class EH_Additionalfees_Model_Quote_Item extends Mage_Sales_Model_Quote_Item
{
	public function calcRowTotal()
	{	
	    parent::calcRowTotal();
	  
	    $product = $this->getProduct();
	    $product->load($product->getId());	  
	    
	    $farr=unserialize($this->getPaymentFee());	
	   // echo "<pre>";print_r($product);exit;
	    $extra=0;
	    if(!empty($farr)) {
		foreach($farr as $fval)
		{
		    if($fval[1]=='Yes')
			$extra+=$fval[0];
		    else
			$extra+=($fval[0]*$this->getQty());  
		}
		
		$baseTotal = $this->getBaseRowTotal() + $extra;	    
		$total = $this->getStore()->convertPrice($baseTotal);
		$this->setRowTotal($this->getStore()->roundPrice($total));
		$this->setBaseRowTotal($this->getStore()->roundPrice($baseTotal));
		
		return $this;
	    }
	      
	}
}
?>
