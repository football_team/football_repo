<?php
/*////////////////////////////////////////////////////////////////////////////////
 \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\  Additional Fee  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
 /////////////////////////////////////////////////////////////////////////////////
 \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ NOTICE OF LICENSE\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
 ///////                                                                   ///////
 \\\\\\\ This source file is subject to the Open Software License (OSL 3.0)\\\\\\\
 ///////   that is bundled with this package in the file LICENSE.txt.      ///////
 \\\\\\\   It is also available through the world-wide-web at this URL:    \\\\\\\
 ///////          http://opensource.org/licenses/osl-3.0.php               ///////
 \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
 ///////                      * @category   EH                            ///////
 \\\\\\\                      * @package    EH_Additionalfee                \\\\\\\
 ///////    * @author     Suneet Kumar <suneet64@gmail.com>               ///////
 \\\\\\\                                                                   \\\\\\\
 /////////////////////////////////////////////////////////////////////////////////
 \\\\\\* @copyright  Copyright 2014 © www.extensionhut.com All right reserved\\\\\
 /////////////////////////////////////////////////////////////////////////////////
 */
 
class EH_Additionalfees_Model_Additionalfees extends Mage_Core_Model_Abstract
{
	protected $_checkout;
	protected $_quote;

	public function _construct()
	{
		parent::_construct();
		$this->_init('additionalfees/additionalfees');
	}

	/*
	* GET THE CHECKOUT OBJECT
	*/
	public function getCheckout()
	{
		if (empty($this->_checkout)) {
			$this->_checkout = Mage::getSingleton('checkout/session');
		}
		return $this->_checkout;
	}
	
	/*
	* SET Additional FEE ADDRESSES
	*/
	public function setAdditionalfeesAddresses($addresses = array())
	{
		$this->getCheckout()->setAdditionalfeesAddresses($addresses);
	}
	
	/*
	* LOAD THE Additional FEE FRONTEND FOR MULTISHIPPING CHECKOUT
	*/
	public function getAdditionalfeesAddresses()
	{
		return (array) $this->getCheckout()->getAdditionalfeesAddresses();
	}	
	
	/*
	* SET Additional FEE EXTENSION
	*/
	public function setAdditionalfees($flag = 1)
	{
		$this->getCheckout()->setAdditionalfees($flag);
	}

	/*
	* SET GIFT AMOUNT TO BE PAID
	*/
	public function setGiftAmount($amt =0)
	{
		$this->getCheckout()->setGiftAmount($amt);
	}
	
	/*
	* GET GIFT AMOUNT TO BE PAID
	*/
	public function getGiftAmount()
	{
		return $this->getCheckout()->getGiftAmount();
	}
	
	/*
	* GET Additional FEES
	*/
	public function getAdditionalfees()
	{
		return $this->getCheckout()->getAdditionalfees();
	}
	
	/*
	* GET QUOTE
	*/
	public function getQuote()
	{
		if (empty($this->_quote)) {
			$this->_quote = $this->getCheckout()->getQuote();
		}
		return $this->_quote;
	}
	
	/*
	* SET GIFT TEXT
	*/
	public function setGiftText($feestr)
	{
		$this->getCheckout()->setGiftText($feestr);
	}
	
	/*
	* GET GIFT TEXT
	*/
	public function getGiftText()
	{
		return $this->getCheckout()->getGiftText();
	}

	/*
	* SET Payment FEE EXTENSION
	*/
	public function setPaymentFee($fee)
	{		
        	return $this->getCheckout()->setData('paymentfee', $fee);    	
	}
	
	public function getPaymentFee()
	{
		return $this->getCheckout()->getData('paymentfee');
	}

	public function getFeeList($id,$storeId='')
	{
		if($storeId=='')
		    $storeId=Mage::app()->getStore()->getId();  
		$sym=Mage::app()->getLocale()->currency(Mage::app()->getStore()->getCurrentCurrencyCode())->getSymbol();
		$filter_a = array("finset"=>array($storeId));
		$filter_b = array("finset"=>array('0'));
	    $collectionFee=Mage::getModel('additionalfees/additionalfees')->getCollection();
	    $collectionFee->addFieldToFilter('apply_to',array('eq'=>'Product'))
		    ->addFieldToFilter('status',array('eq'=>'1'))
		    ->addFieldToFilter('store_id',array( $filter_a, $filter_b))
		    ->getSelect(); 

		foreach($collectionFee as $feeDetails)
		{			
			$struct[$feeDetails->getAdditionalfeesId()]['mandatory']=$feeDetails->getMandatory(); 	
			$struct[$feeDetails->getAdditionalfeesId()]['feetype']=$feeDetails->getFeetype();
			$struct[$feeDetails->getAdditionalfeesId()]['amt']=$feeDetails->getFeeamount();
			$struct[$feeDetails->getAdditionalfeesId()]['flatfee']=$feeDetails->getFlatfee();
			if($feeDetails->getFeetype()=='Fixed')
				$struct[$feeDetails->getAdditionalfeesId()]['title']=$feeDetails->getTitle().": ".$sym.$feeDetails->getFeeamount();
			else
				$struct[$feeDetails->getAdditionalfeesId()]['title']=$feeDetails->getTitle().": ".$feeDetails->getFeeamount()."%";		
		}		
		return $struct;
	}

	public function getFeeListAdmin($id)
	{
		$sym=Mage::app()->getLocale()->currency(Mage::app()->getStore()->getCurrentCurrencyCode())->getSymbol();
		$collectionFeeAll=Mage::getModel('additionalfees/additionalfees')->getCollection();
	    $collectionFeeAll->addFieldToFilter('apply_to',array('eq'=>'Product'))
		    ->addFieldToFilter('status',array('eq'=>'1'))
			->getSelect(); 
		foreach($collectionFeeAll as $fee)	
		{	
			$struct[$fee->getAdditionalfeesId()]['mandatory']=$fee->getMandatory(); 	
			$struct[$fee->getAdditionalfeesId()]['feetype']=$fee->getFeetype();
			$struct[$fee->getAdditionalfeesId()]['amt']=$fee->getFeeamount();
			$struct[$fee->getAdditionalfeesId()]['flatfee']=$fee->getFlatfee();
			if($fee->getFeetype()=='Fixed')
				$struct[$fee->getAdditionalfeesId()]['title']=$fee->getTitle().": ".$sym.$fee->getFeeamount();
			else
				$struct[$fee->getAdditionalfeesId()]['title']=$fee->getTitle().": ".$fee->getFeeamount()."%";
		
		}
		return $struct;

	}
	
	public function getcatFeeList($id,$storeId='')
	{
		if($storeId=='')
		    $storeId=Mage::app()->getStore()->getId();
		$sym=Mage::app()->getLocale()->currency(Mage::app()->getStore()->getCurrentCurrencyCode())->getSymbol();
		$filter_a = array("finset"=>array($storeId));
		$filter_b = array("finset"=>array('0'));
	    $collectioncatFee=Mage::getModel('additionalfees/additionalfees')->getCollection();
	    $collectioncatFee->addFieldToFilter('apply_to',array('eq'=>'Category'))
		    ->addFieldToFilter('status',array('eq'=>'1'))
		    ->addFieldToFilter('store_id',array( $filter_a, $filter_b))
		    ->getSelect(); 

		foreach($collectioncatFee as $catfee)	
		{			
			$struct[$catfee->getAdditionalfeesId()]['mandatory']=$catfee->getMandatory(); 			
			$struct[$catfee->getAdditionalfeesId()]['feetype']=$catfee->getFeetype();
			$struct[$catfee->getAdditionalfeesId()]['cat']=$catfee->getCategory();
			$struct[$catfee->getAdditionalfeesId()]['amt']=$catfee->getFeeamount();
			$struct[$catfee->getAdditionalfeesId()]['flatfee']=$catfee->getFlatfee();
			if($catfee->getFeetype()=='Fixed')
				$struct[$catfee->getAdditionalfeesId()]['title']=$catfee->getTitle().": ".$sym.$catfee->getFeeamount();
			else
				$struct[$catfee->getAdditionalfeesId()]['title']=$catfee->getTitle().": ".$catfee->getFeeamount()."%";
			
		}
		return $struct;

	}

	public function getFeeDetails($cartarr)
	{
		
		$sym=Mage::app()->getLocale()->currency(Mage::app()->getStore()->getCurrentCurrencyCode())->getSymbol();
		if(!empty($cartarr)){
			foreach($cartarr as $pid)
			{
				$productFull = Mage::getModel('catalog/product')->load($pid);
				$attributes = $productFull->getAttributes();
				$overwrite=$attributes['additional_catoverride']->getFrontend()->getValue($productFull);
				if($overwrite==Mage::helper('additionalfees')->__('No'))
				{
					$categoryIds = $productFull->getCategoryIds();
					foreach($categoryIds as $km=>$vm) {
						$parentId=Mage::getModel('catalog/category')->load($vm)->getParentId();
						if(!in_array($parentId,$categoryIds))
							array_push($categoryIds,$parentId);
					}	
					$fetchcatlist = Mage::getModel('additionalfees/additionalfees')->getcatFeeList('Category');		
					
					$k=0;		
					foreach($fetchcatlist as $key=>$val)
					{
						if($val['mandatory']=='No')	
						{	
							$catArr=array();
							$catArr=explode(",",$val['cat']);
							foreach($catArr as $catId)
							{
								if(in_array($catId,$categoryIds)){											
									$cartfees[$pid][]=$key;			
								}


							}
						}
					
			
					}
					
				}
				else
				{
					$ekarr=$productFull->getAdditionalExtrafee();  					
					$arr=Mage::getModel('additionalfees/additionalfees')->getFeeList('Product');		
					foreach($arr as $key1=>$val1)
					{
						if($val1['mandatory']=='No')	
						{
						    foreach($ekarr as $ekey) {
							if(in_array($ekey,$val1))
							{								
								$cartfees[$pid][]=$key1;	
							}
						    }
						}
						
					}	
					
				}
	


			}


		}		
		return $cartfees;	
	}//end of function
	
	public function getOptFeeDetails($pid,$sid)
	{		
		$sym=Mage::app()->getLocale()->currency(Mage::app()->getStore()->getCurrentCurrencyCode())->getSymbol();		
		$productFull = Mage::getModel('catalog/product')->load($pid);
		$attributes = $productFull->getAttributes();
		$overwrite=$attributes['additional_catoverride']->getFrontend()->getValue($productFull);			
		if($overwrite==Mage::helper('additionalfees')->__('No'))
		{
			$categoryIds = $productFull->getCategoryIds();
			foreach($categoryIds as $km=>$vm) {
				$parentId=Mage::getModel('catalog/category')->load($vm)->getParentId();
				if(!in_array($parentId,$categoryIds))
					array_push($categoryIds,$parentId);
			}	
			$fetchcatlist = Mage::getModel('additionalfees/additionalfees')->getcatFeeList('Category',$sid);		
			
			$k=0;		
			foreach($fetchcatlist as $key=>$val)
			{
				if($val['mandatory']=='No')	
				{	
					$catArr=array();
					$catArr=explode(",",$val['cat']);
					foreach($catArr as $catId)
					{
						if(in_array($catId,$categoryIds)){		
							
							$cartfees[$pid][]=$key;			
						}


					}
				}
			
	
			}
			
		}
		else
		{
			$ekarr=$productFull->getAdditionalExtrafee();			
			$arr=Mage::getModel('additionalfees/additionalfees')->getFeeList('Product',$sid);				
			foreach($arr as $key1=>$val1)
			{
				if($val1['mandatory']=='No')	
				{
				    foreach($ekarr as $ekey) {
					   if($ekey==$key1)
					   {							
						   $cartfees[$pid][]=$key1;	
					   }
				    }
				}
				
			}	
			
		}				
		return $cartfees;	
	}//end of function	
	
	public function getMandFeeDetails($pid,$sid)
	{		
		$sym=Mage::app()->getLocale()->currency(Mage::app()->getStore()->getCurrentCurrencyCode())->getSymbol();		
		$productFull = Mage::getModel('catalog/product')->load($pid);
		$attributes = $productFull->getAttributes();
		$overwrite=$attributes['additional_catoverride']->getFrontend()->getValue($productFull);
		if($overwrite==Mage::helper('additionalfees')->__('No'))
		{
			$categoryIds = $productFull->getCategoryIds();
			foreach($categoryIds as $km=>$vm) {
				$parentId=Mage::getModel('catalog/category')->load($vm)->getParentId();
				if(!in_array($parentId,$categoryIds))
					array_push($categoryIds,$parentId);
			}	
			$fetchcatlist = Mage::getModel('additionalfees/additionalfees')->getcatFeeList('Category',$sid);		
			
			$k=0;		
			foreach($fetchcatlist as $key=>$val)
			{					
				$catArr=array();
				$catArr=explode(",",$val['cat']);
				foreach($catArr as $catId)
				{
					if(in_array($catId,$categoryIds)){		
						
						$mcartfees[$pid][]=$key;			
					}
				}	
			}
			
		}
		else
		{
			$ekarr=$productFull->getAdditionalExtrafee();			
			$arr=Mage::getModel('additionalfees/additionalfees')->getFeeList('Product',$sid);					
			foreach($arr as $key1=>$val1)
			{	
			      foreach($ekarr as $ekey) {
				  		if($ekey==$key1)
				  		{								
					  		$mcartfees[$pid][]=$key1;	
				  		}
			      }		      
			}	
			
		}	
		
		return $mcartfees;	
	}//end of function

	public function isConfigurableChild($product) 
	{ 
	    $configurable_product = Mage::getModel('catalog/product_type_configurable');
	    $parentIdArray = $configurable_product->getParentIdsByChild($product->getId()); 	
	   
	    if(count($parentIdArray)>0) 
	    { 
		$father = Mage::getModel('catalog/product')->load($parentIdArray[0]); 
		$type_p = $father->getTypeId(); 
		if($type_p == 'configurable') 
		{   
			$tempattr = $father->getTypeInstance()->getConfigurableAttributes();
			$configurableSet['parent']=$parentIdArray[0];
			foreach ($tempattr as $attr)
			{    
			    $configurableSet[] = $attr->getProductAttribute()->getId();                    
			}	
			return $configurableSet;	               	
		} 
		else
		{
		    return false; 	
		}
	    } 


	    
	}
	public function getShipList()
	{		
		$filter_a = array("finset"=>array(Mage::app()->getStore()->getId()));
		$filter_b = array("finset"=>array('0'));
	    $collectionshipFee=Mage::getModel('additionalfees/additionalfees')->getCollection();
	    $collectionshipFee->addFieldToFilter('apply_to',array('eq'=>'Shipping'))
		    ->addFieldToFilter('status',array('eq'=>'1'))
		    ->addFieldToFilter('store_id',array( $filter_a, $filter_b))
		    ->getSelect();	
		
		return $collectionshipFee->getData();
	}

	public function getFeeTitle($feeid)
	{
		$sym=Mage::app()->getLocale()->currency(Mage::app()->getStore()->getCurrentCurrencyCode())->getSymbol();
		$data=Mage::getModel('additionalfees/additionalfees')->load($feeid);
		if($data['feetype']=='Percentage')
			$str=$data['title'].": ".$data['feeamount']."%";	
		else
			$str=$data['title'].": ".$sym.$data['feeamount'];	
		return $str;

	}

	public function getOrderFee($subtotal) {				
		$filter_a = array("finset"=>array(Mage::app()->getStore()->getId()));
		$filter_b = array("finset"=>array('0'));
	    $collectionman=Mage::getModel('additionalfees/additionalfees')->getCollection();
	    $collectionman->addFieldToFilter('apply_to',array('eq'=>'Order'))
		    ->addFieldToFilter('status',array('eq'=>'1'))
		    ->addFieldToFilter('mandatory',array('eq'=>'Yes'))
		    ->addFieldToFilter('store_id',array( $filter_a, $filter_b))
		    ->getSelect();
		$addfee=0;
		$details=array();		
		foreach($collectionman as $fee)
		{				
			if($fee->getFeetype()=='Fixed'){
				$price = $fee->getFeeamount();								
				$feedetail['M_'.$fee->getAdditionalfeesId()]['title']=$fee->getTitle();
				//$feedetail['M_'.$fee['additionalfees_id']]['price']=Mage::helper('core')->currency($fee['feeamount'],true,false);
			} else { 	
				$price = (($subtotal*$fee->getFeeamount())/100);
				$feedetail['M_'.$fee->getAdditionalfeesId()]['title']=$fee->getFeeamount()."% ".$fee->getTitle();
				//$feedetail['M_'.$fee['additionalfees_id']]['price']=$fee['feeamount']."%";
			}	
			$addfee+=$price;			
			$feedetail['M_'.$fee->getAdditionalfeesId()]['price']=$price;				
		}	
		$deatials[0]=$addfee;	
		$deatials[1]=$feedetail;		
		return $deatials;
		
	}	

	public function getOptionaOrderFee() 
	{
		$filter_a = array("finset"=>array(Mage::app()->getStore()->getId()));
		$filter_b = array("finset"=>array('0'));
	    $collectionopt=Mage::getModel('additionalfees/additionalfees')->getCollection();
	    $collectionopt->addFieldToFilter('apply_to',array('eq'=>'Order'))
		    ->addFieldToFilter('status',array('eq'=>'1'))
		    ->addFieldToFilter('mandatory',array('eq'=>'No'))
		    ->addFieldToFilter('store_id',array( $filter_a, $filter_b))
		    ->getSelect();	
		
		return $collectionopt->getData();	
	}	
}	
