<?php
/*////////////////////////////////////////////////////////////////////////////////
 \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\  Additional Fee  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
 /////////////////////////////////////////////////////////////////////////////////
 \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ NOTICE OF LICENSE\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
 ///////                                                                   ///////
 \\\\\\\ This source file is subject to the Open Software License (OSL 3.0)\\\\\\\
 ///////   that is bundled with this package in the file LICENSE.txt.      ///////
 \\\\\\\   It is also available through the world-wide-web at this URL:    \\\\\\\
 ///////          http://opensource.org/licenses/osl-3.0.php               ///////
 \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
 ///////                      * @category   EH                            ///////
 \\\\\\\                      * @package    EH_Additionalfee                \\\\\\\
 ///////    * @author     Suneet Kumar <suneet64@gmail.com>               ///////
 \\\\\\\                                                                   \\\\\\\
 /////////////////////////////////////////////////////////////////////////////////
 \\\\\\* @copyright  Copyright 2014 © www.extensionhut.com All right reserved\\\\\
 /////////////////////////////////////////////////////////////////////////////////
 */
 
class EH_Additionalfees_Model_Sales_Order_Creditmemo_Total_Fee extends Mage_Sales_Model_Order_Creditmemo_Total_Abstract
{
	public function collect(Mage_Sales_Model_Order_Creditmemo $creditmemo)
	{
	    $orderFee        = $creditmemo->getOrder()->getAdditionalfee();
	    $baseOrderFee    = $creditmemo->getOrder()->getBaseAdditionalfee();
	    $detailsOrderFee = $creditmemo->getOrder()->getDetailAdditionalfee();

	    $creditmemo->setAdditionalfee($orderFee);
	    $creditmemo->setBaseAdditionalfee($baseOrderFee);
	    $this->_prepareDetailsAdditionalfee($creditmemo, $detailsOrderFee);
	    //$creditmemo->setDetailAdditionalfee($detailsOrderFee);
	    $creditmemo->setGrandTotal($creditmemo->getGrandTotal() + $creditmemo->getAdditionalfee());
	    $creditmemo->setBaseGrandTotal($creditmemo->getBaseGrandTotal() + $creditmemo->getBaseAdditionalfee());

	    return $this;
	}

	private function _prepareDetailsAdditionalfee($creditmemo, $detailsFees)
	{
	    $helper = Mage::helper('additionalfees');
	    $fees = @unserialize($detailsFees);
	    if (is_array($fees) && count($fees)) {
			    $tax = null;
			    $checkTax = $helper->getFullTaxInfo($creditmemo->getOrderId());
			    if (count($checkTax)) {
				    $tax = $checkTax;
			    }
		    $prices = $helper->getAdditionalPrice($fees, $creditmemo->getSubtotal(), $creditmemo->getShippingInclTax(), $tax);
		    $detailsFees = serialize($prices->getDetailsFees());
		    $creditmemo->setAdditionalfee($prices->getFeesExclTax());
		    $creditmemo->setBaseAdditionalfee($prices->getBaseFees());
	    }
	    $creditmemo->setDetailAdditionalfee($detailsFees);

	    return $this;
	}
}
