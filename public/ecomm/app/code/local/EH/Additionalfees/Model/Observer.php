<?php
/*////////////////////////////////////////////////////////////////////////////////
 \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\  Additional Fee  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
 /////////////////////////////////////////////////////////////////////////////////
 \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ NOTICE OF LICENSE\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
 ///////                                                                   ///////
 \\\\\\\ This source file is subject to the Open Software License (OSL 3.0)\\\\\\\
 ///////   that is bundled with this package in the file LICENSE.txt.      ///////
 \\\\\\\   It is also available through the world-wide-web at this URL:    \\\\\\\
 ///////          http://opensource.org/licenses/osl-3.0.php               ///////
 \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
 ///////                      * @category   EH                            ///////
 \\\\\\\                      * @package    EH_Additionalfee                \\\\\\\
 ///////    * @author     Suneet Kumar <suneet64@gmail.com>               ///////
 \\\\\\\                                                                   \\\\\\\
 /////////////////////////////////////////////////////////////////////////////////
 \\\\\\* @copyright  Copyright 2014 © www.extensionhut.com All right reserved\\\\\
 /////////////////////////////////////////////////////////////////////////////////
 */
 
class EH_Additionalfees_Model_Observer
{

	/*
	* SET THE EH FEES OBSERVER FOR SINGLE CHECKOUT
	*/
	public function setAdditionalfees($observer)
	{  	

		$quote = $observer->getEvent()->getQuote();
		
		$request = $observer->getEvent()->getRequest();	
		$additionalfee = $request->getPost('additionalfee', 0);		
		$model1 = Mage::getSingleton('additionalfees/additionalfees');		
		$modelData = $model1->getCollection();		
		$amount = 0;
		$feestr='';
		$model1->setAdditionalfeesAddresses(array());
		foreach ($modelData as $data)
		{ 		
			if(isset($additionalfee[$data->getAdditionalfeesId()]) )
			{
				if($data->getFeetype()=='Fixed') 
				{					
					$amount = $amount + $data->getFeeamount();
					$feestr.= " + ".$data->getTitle();			
				}
				else 	
				{
					$amount = $amount + $request->getPost('percent_'.$data->getAdditionalfeesId());
					$feestr.= " + ".$data->getTitle();
				}
				
			}
			
		}	
		
		$model1->setGiftAmount($amount);
		$model1->setGiftText($feestr);
		
		if (!$quote->isVirtual() && $additionalfee){
		    $model1->setAdditionalfees(1);
		}
		else {
		    $model1->setAdditionalfees(0);
		}
		$quote->collectTotals()->save();
	}
	
	/*
	* LOAD THE Additional FEE FRONTEND FOR MULTISHIPPING CHECKOUT
	*/
	public function setAdditionalfeesMultishipping($observer)
    {
    	$quote = $observer->getEvent()->getQuote();
        $request = $observer->getEvent()->getRequest();
        $additionalfees = $request->getPost('additionalfee', array());
	$model1 = Mage::getSingleton('additionalfees/additionalfees');
        //$model = Mage::getSingleton('additionalfees/subadditionalfees');
        $modelData = $model1->getCollection();
        $amount = 0;
		$feestr='';
		foreach ($modelData as $data)
		{
			if(isset($additionalfees[$data->getAdditionalfeesId()]))
			{
				if($data->getFeetype()=='Fixed') 
				{					
					$amount = $amount + $data->getFeeamount();
					$feestr.= " + ".$data->getTitle();			
				}
				else 	
				{
					$amount = $amount + $request->getPost('percent_'.$data->getAdditionalfeesId());
					$feestr.= " + ".$data->getTitle();
				}
				
			}
		}
		$model1->setGiftAmount($amount);
		$model1->setGiftText($feestr);
		
        if (!$quote->isVirtual() && !empty($additionalfees)){
            $model1->setAdditionalfees(1);
            $model1->setAdditionalfeesAddresses(array_keys($additionalfees));
        }
        else {
            $model1->setAdditionalfees(0);
        }
        $quote->collectTotals()->save();
    }    


    public function setProductFees($observer)
    {	

	if($quote_item = $observer->getQuoteItem()->getParentItem()) {	
		//echo ">>".$quote_item->getCalculationPrice();exit;
		$product_id = $quote_item->getProductId();
		$product = $quote_item->getProduct();	
		//Mage::log($product->getTypeInstance()->prepareForCart($buyRequest, $product));
		$myprice=array();	
		$strExtra=array();						
		$productFull = Mage::getModel('catalog/product')->load($product_id);
		
		$attributes = $productFull->getAttributes();
		$overwrite=$attributes['additional_catoverride']->getFrontend()->getValue($productFull);	
		if($overwrite==Mage::helper('additionalfees')->__('No'))
		{
			$categoryIds = $product->getCategoryIds();	
			foreach($categoryIds as $km=>$vm) {
				$parentId=Mage::getModel('catalog/category')->load($vm)->getParentId();
				if(!in_array($parentId,$categoryIds))
					array_push($categoryIds,$parentId);
			}
			$fetchcatlist = Mage::getModel('additionalfees/additionalfees')->getcatFeeList('Category');		
		
			$k=0;		
			foreach($fetchcatlist as $key=>$val)
			{
				if($val['mandatory']=='Yes')	
				{	
					$catArr=array();
					$catArr=explode(",",$val['cat']);
					foreach($catArr as $catId)
					{
					
						if(in_array($catId,$categoryIds)){	
							if($val['feetype']=='Percentage')
							{	
								$myprice['M'.$product_id.'_'.$key][]=($productFull->getFinalPrice()*$val['amt'])/100;
								$myprice['M'.$product_id.'_'.$key][]=$val['flatfee'];								
								$strExtra['M'.$product_id.'_'.$key]=$val['title'];

							} else { 
								
								$myprice['M'.$product_id.'_'.$key][]=$val['amt'];
								$myprice['M'.$product_id.'_'.$key][]=$val['flatfee'];								
								$strExtra['M'.$product_id.'_'.$key]=$val['title'];								
							}
										
						}


					}
				}			
			
			} 	
			
			if($quote_item->getProductType()=='bundle') {
			    if ($quote_item->getHasChildren()) {
			      foreach ($quote_item->getChildren() as $child) {
				  //echo ($child->getProductId())."<br/>";exit;				  
				  $child->setPaymentFee(serialize($myprice));
				  $child->setPaymentStr(serialize($strExtra));				
				  break;
			      }			     
			        
			    }	    
			}
			$quote_item->setPaymentFee(serialize($myprice));
			$quote_item->setPaymentStr(serialize($strExtra));
			
		}
		else
		{					
			$ekarr=$productFull->getAdditionalExtrafee();			
			$arr=Mage::getModel('additionalfees/additionalfees')->getFeeList('Product');		
			foreach($arr as $key1=>$val1)
			{
				if($val1['mandatory']=='Yes')	
				{
					foreach($ekarr as $ekey) {
					      if($ekey==$key1)
					      {	
						      if($val1['feetype']=='Percentage')
						      {								
							      $myprice['M'.$product_id.'_'.$key1][]=($productFull->getFinalPrice()*$val1['amt'])/100;
							      $myprice['M'.$product_id.'_'.$key1][]=$val1['flatfee'];							
							      $strExtra['M'.$product_id.'_'.$key1]=$val1['title'];
							      
						      }
						      else { 
							      
							      $myprice['M'.$product_id.'_'.$key1][]=$val1['amt'];
							      $myprice['M'.$product_id.'_'.$key1][]=$val1['flatfee'];
							      $strExtra['M'.$product_id.'_'.$key1]=$val1['title'];								
						      }
					      }
					}
				}			
			}
			if($quote_item->getProductType()=='bundle') {
			    if ($quote_item->getHasChildren()) {
			      foreach ($quote_item->getChildren() as $child) {
				  //echo ($child->getProductId())."<br/>";				  
				  $child->setPaymentFee(serialize($myprice));
				  $child->setPaymentStr(serialize($strExtra));
				  break;
			      }			      
			    }	    
			}
 
			$quote_item->setPaymentFee(serialize($myprice));	
			$quote_item->setPaymentStr(serialize($strExtra));	
			
			
		}

       } else {   
		
		$quote_item = $observer->getQuoteItem();		
		$product_id = $quote_item->getProductId();
		$product = $quote_item->getProduct();		
		$values = unserialize($quote_item->getOptionByCode('info_buyRequest')->getValue());
		$parentId = $values['super_product_config']['product_id'];		
		if(isset($parentId))
		{					
			$this->setAdditionalQuoteItem($quote_item,$quote_item->getProductId(),$parentId);			
		}
		else 
		{	
			$myprice=array();
			$strExtra=array();		
			$productFull = Mage::getModel('catalog/product')->load($product_id);
			$attributes = $productFull->getAttributes();
			$overwrite=$attributes['additional_catoverride']->getFrontend()->getValue($productFull);	
			if($overwrite==Mage::helper('additionalfees')->__('No'))
			{
				$categoryIds = $product->getCategoryIds();	
				foreach($categoryIds as $km=>$vm) {
					$parentId=Mage::getModel('catalog/category')->load($vm)->getParentId();
					if(!in_array($parentId,$categoryIds))
						array_push($categoryIds,$parentId);
				}
				$fetchcatlist = Mage::getModel('additionalfees/additionalfees')->getcatFeeList('Category');		
			
				$k=0;		
				foreach($fetchcatlist as $key=>$val)
				{
					if($val['mandatory']=='Yes')	
					{	
						$catArr=array();
						$catArr=explode(",",$val['cat']);
						foreach($catArr as $catId)
						{
						
							if(in_array($catId,$categoryIds)){	
								if($val['feetype']=='Percentage')
								{	
									$myprice['M'.$product_id.'_'.$key][]=($productFull->getFinalPrice()*$val['amt'])/100;
									$myprice['M'.$product_id.'_'.$key][]=$val['flatfee'];									
									$strExtra['M'.$product_id.'_'.$key]=$val['title'];
									

								} else {
										
									$myprice['M'.$product_id.'_'.$key][]=$val['amt'];
									$myprice['M'.$product_id.'_'.$key][]=$val['flatfee'];									
									$strExtra['M'.$product_id.'_'.$key]=$val['title'];
									
								}
											
							}
						}
					}			
				
				}			
				$quote_item->setPaymentFee(serialize($myprice));
				$quote_item->setPaymentStr(serialize($strExtra));
				
			}
			else
			{	
				$ekarr=$productFull->getAdditionalExtrafee();										
				$arr=Mage::getModel('additionalfees/additionalfees')->getFeeList('Product');		
				foreach($arr as $key1=>$val1)
				{
					if($val1['mandatory']=='Yes')	
					{
						foreach($ekarr as $ekey) {
						      if($ekey==$key1)
						      {	
							      if($val1['feetype']=='Percentage')
							      {									      
								      $myprice['M'.$product_id.'_'.$key1][]=($productFull->getFinalPrice()*$val1['amt'])/100;
								      $myprice['M'.$product_id.'_'.$key1][]=$val1['flatfee'];								      
								      $strExtra['M'.$product_id.'_'.$key1]=$val1['title'];								      
							      }
							      else { 									
								      $myprice['M'.$product_id.'_'.$key1][]=$val1['amt'];
								      $myprice['M'.$product_id.'_'.$key1][]=$val1['flatfee'];								     
								      $strExtra['M'.$product_id.'_'.$key1]=$val1['title'];								      
							      }
						      }
						}
					}			
				}						
				$quote_item->setPaymentFee(serialize($myprice));	
				$quote_item->setPaymentStr(serialize($strExtra));
				
			}
		}
	
       }//for simple product	

	
    } 
	
    public function setAdditionalQuoteItem($quote_item,$product_id,$parent)
    {
			$myprice=array();
			$strExtra=array();	
			$product = $quote_item->getProduct();	
			$productFull = Mage::getModel('catalog/product')->load($parent);
			$attributes = $productFull->getAttributes();
			$overwrite=$attributes['additional_catoverride']->getFrontend()->getValue($productFull);	
			if($overwrite==Mage::helper('additionalfees')->__('No'))
			{
				$categoryIds = $product->getCategoryIds();	
				foreach($categoryIds as $km=>$vm) {
					$parentId=Mage::getModel('catalog/category')->load($vm)->getParentId();
					if(!in_array($parentId,$categoryIds))
						array_push($categoryIds,$parentId);
				}
				$fetchcatlist = Mage::getModel('additionalfees/additionalfees')->getcatFeeList('Category');		
			
				$k=0;		
				foreach($fetchcatlist as $key=>$val)
				{
					if($val['mandatory']=='Yes')	
					{	
						$catArr=array();
						$catArr=explode(",",$val['cat']);
						foreach($catArr as $catId)
						{
						
							if(in_array($catId,$categoryIds)){	
								if($val['feetype']=='Percentage')
								{	
									$myprice['M'.$product_id.'_'.$key][]=($productFull->getFinalPrice()*$val['amt'])/100;
									$myprice['M'.$product_id.'_'.$key][]=$val['flatfee'];							
									$strExtra['M'.$product_id.'_'.$key]=$val['title'];
									
		
								} else {
										
									$myprice['M'.$product_id.'_'.$key][]=$val['amt'];
									$myprice['M'.$product_id.'_'.$key][]=$val['flatfee'];							
									$strExtra['M'.$product_id.'_'.$key]=$val['title'];
									
								}
											
							}
						}
					}			
				
				}		
				$items = Mage::getSingleton('checkout/session')->getQuote()->getAllItems();
				foreach($items as $item) {
					$values1 = unserialize($item->getOptionByCode('info_buyRequest')->getValue());
					if($parent==$values1['super_product_config']['product_id'])
					{					
						$item->setPaymentFee(serialize($myprice));
						$item->setPaymentStr(serialize($strExtra));						
					}					
				}				
				
				
			}
			else
			{	
				$ekarr=$productFull->getAdditionalExtrafee();					
				$arr=Mage::getModel('additionalfees/additionalfees')->getFeeList('Product');		
				foreach($arr as $key1=>$val1)
				{
					if($val1['mandatory']=='Yes')	
					{
						foreach($ekarr as $ekey) {
						      if($ekey==$key1)
						      {	
							      if($val1['feetype']=='Percentage')
							      {	
								      
								      $myprice['M'.$product_id.'_'.$key1][]=($productFull->getFinalPrice()*$val1['amt'])/100;
								      $myprice['M'.$product_id.'_'.$key1][]=$val1['flatfee'];						    
								      $strExtra['M'.$product_id.'_'.$key1]=$val1['title'];
								      
							      }
							      else { 	
									
								      $myprice['M'.$product_id.'_'.$key1][]=$val1['amt'];
								      $myprice['M'.$product_id.'_'.$key1][]=$val1['flatfee'];						     
								      $strExtra['M'.$product_id.'_'.$key1]=$val1['title'];
								      
							      }
						      }
						}
					}			
				}	
				$items = Mage::getSingleton('checkout/session')->getQuote()->getAllItems();
				foreach($items as $item) {
					$values1 = unserialize($item->getOptionByCode('info_buyRequest')->getValue());
					if($parent==$values1['super_product_config']['product_id'])
					{		
						$item->setPaymentFee(serialize($myprice));
						$item->setPaymentStr(serialize($strExtra));						
					}
				}									
				
				
			}

    }       

    public function updatePaypalCart(Varien_Event_Observer $observer){	
        $paypalCart = $observer->getEvent()->getPaypalCart();
        $additional = $observer->getEvent()->getAdditional();
        $salesEntity = $observer->getEvent()->getSalesEntity(); 
	$feeTitle = Mage::getStoreConfig('additionalfees_section/additionalfees_group1/title');
        if (!empty($additional) && !empty($salesEntity)) {
            $items = $additional->getItems();
            $items[] = new Varien_Object(array(
                'id'     => $feeTitle!='' ? Mage::helper('additionalfees')->__($feeTitle) : Mage::helper('additionalfees')->__('Additional Fees'),
                'name'   => $feeTitle!='' ? Mage::helper('additionalfees')->__($feeTitle) : Mage::helper('additionalfees')->__('Additional Fees'),
                'qty'    => 1,
                'amount' => (float)$salesEntity->getBaseAdditionalfee(),
            ));
            $salesEntity->setBaseSubtotal($salesEntity->getBaseSubtotal()+$salesEntity->getBaseAdditionalfee());
            $salesEntity->setBaseTaxAmount($salesEntity->getBaseTaxAmount());
            $additional->setItems($items); 	
        } elseif ($paypalCart && $paypalCart->getSalesEntity()->getBaseAdditionalfee()!=0) {          
            $paypalCart->addItem($feeTitle!='' ? Mage::helper('additionalfees')->__($feeTitle) : Mage::helper('additionalfees')->__('Additional Fees'),1,(float)$paypalCart->getSalesEntity()->getBaseAdditionalfee(),'extrafee');
	    if(Mage::helper('additionalfees')->mageVersionCompare(Mage::getVersion(), '1.6.1.0', '>')) {
		if($paypalCart->isShippingAsItem()) {            
		    $paypalCart->updateTotal(Mage_Paypal_Model_Cart::TOTAL_SUBTOTAL, -1* $paypalCart->getSalesEntity()->getBaseShippingAmount());
		}
	    }
        }
  
    }

    public function UpdateAdditionalQuote(Varien_Event_Observer $observer) 
    {
	$quote = $observer->getQuote();
        foreach ($quote->getAllAddresses() as $address) {
            Mage::getModel('additionalfees/sales_quote_address_total_fee')->additionalTotal($address);
        }

    }

    public function editOrderAdmin(Varien_Event_Observer $observer)
    {
        $observer->getQuoteItem()->setPaymentFee($observer->getOrderItem()->getPaymentFee());
        $observer->getQuoteItem()->setPaymentStr($observer->getOrderItem()->getPaymentStr());
        $observer->getQuoteItem()->save(); 
    } 
  
    
}     
