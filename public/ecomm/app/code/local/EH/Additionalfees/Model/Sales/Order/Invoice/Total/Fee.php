<?php
/*////////////////////////////////////////////////////////////////////////////////
 \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\  Additional Fee  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
 /////////////////////////////////////////////////////////////////////////////////
 \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ NOTICE OF LICENSE\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
 ///////                                                                   ///////
 \\\\\\\ This source file is subject to the Open Software License (OSL 3.0)\\\\\\\
 ///////   that is bundled with this package in the file LICENSE.txt.      ///////
 \\\\\\\   It is also available through the world-wide-web at this URL:    \\\\\\\
 ///////          http://opensource.org/licenses/osl-3.0.php               ///////
 \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
 ///////                      * @category   EH                            ///////
 \\\\\\\                      * @package    EH_Additionalfee                \\\\\\\
 ///////    * @author     Suneet Kumar <suneet64@gmail.com>               ///////
 \\\\\\\                                                                   \\\\\\\
 /////////////////////////////////////////////////////////////////////////////////
 \\\\\\* @copyright  Copyright 2014 © www.extensionhut.com All right reserved\\\\\
 /////////////////////////////////////////////////////////////////////////////////
 */
 
class EH_Additionalfees_Model_Sales_Order_Invoice_Total_Fee extends Mage_Sales_Model_Order_Invoice_Total_Abstract
{
    public function collect(Mage_Sales_Model_Order_Invoice $invoice)
    {
        $orderFee        = $invoice->getOrder()->getAdditionalfee();
        $baseOrderFee    = $invoice->getOrder()->getBaseAdditionalfee();
        $detailsOrderFee = $invoice->getOrder()->getDetailAdditionalfee();

        if ($orderFee >= 0) {
            $invoice->setAdditionalfee($orderFee);
            $invoice->setBaseAdditionalfee($baseOrderFee);
	    //$invoice->setDetailAdditionalfee($detailsOrderFee);
        
	    $this->_prepareDetailsAdditionalfee($invoice, $detailsOrderFee);
            $invoice->setGrandTotal($invoice->getGrandTotal() + $invoice->getAdditionalfee());
            $invoice->setBaseGrandTotal($invoice->getBaseGrandTotal() + $invoice->getBaseAdditionalfee());
        }
        return $this;
    }

    private function _prepareDetailsAdditionalfee($invoice, $detailsFees)
    {
	$helper = Mage::helper('additionalfees');
    	$fees = @unserialize($detailsFees);
    	if (is_array($fees) && count($fees)) {
			$tax = null;
			$checkTax = $helper->getFullTaxInfo($invoice->getOrderId());
			if (count($checkTax)) {
				$tax = $checkTax;
			}
    		$prices = $helper->getAdditionalPrice($fees, $invoice->getSubtotal(), $invoice->getShippingInclTax(), $tax);
    		$detailsFees = serialize($prices->getDetailsFees());
    		$invoice->setAdditionalfee($prices->getFeesExclTax());
    		$invoice->setBaseAdditionalfee($prices->getBaseFees());
    	}
    	$invoice->setDetailAdditionalfee($detailsFees);

    	return $this;
    }
   
}
