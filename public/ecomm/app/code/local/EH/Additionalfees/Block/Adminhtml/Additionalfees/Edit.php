<?php
/*////////////////////////////////////////////////////////////////////////////////
 \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\  Additional Fee  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
 /////////////////////////////////////////////////////////////////////////////////
 \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ NOTICE OF LICENSE\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
 ///////                                                                   ///////
 \\\\\\\ This source file is subject to the Open Software License (OSL 3.0)\\\\\\\
 ///////   that is bundled with this package in the file LICENSE.txt.      ///////
 \\\\\\\   It is also available through the world-wide-web at this URL:    \\\\\\\
 ///////          http://opensource.org/licenses/osl-3.0.php               ///////
 \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
 ///////                      * @category   EH                            ///////
 \\\\\\\                      * @package    EH_Additionalfee                \\\\\\\
 ///////    * @author     Suneet Kumar <suneet64@gmail.com>               ///////
 \\\\\\\                                                                   \\\\\\\
 /////////////////////////////////////////////////////////////////////////////////
 \\\\\\* @copyright  Copyright 2014 © www.extensionhut.com All right reserved\\\\\
 /////////////////////////////////////////////////////////////////////////////////
 */
 
class EH_Additionalfees_Block_Adminhtml_Additionalfees_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
	public function __construct()
	{
		parent::__construct();

		$this->_objectId = 'id';
		$this->_blockGroup = 'additionalfees';
		$this->_controller = 'adminhtml_additionalfees';

		$this->_updateButton('save', 'label', Mage::helper('additionalfees')->__('Save Item'));
		$this->_updateButton('delete', 'label', Mage::helper('additionalfees')->__('Delete Item'));

		$this->_addButton('saveandcontinue', array(
		'label'     => Mage::helper('adminhtml')->__('Save And Continue Edit'),
		'onclick'   => 'saveAndContinueEdit()',
		'class'     => 'save',
		), -100);

		$this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('additionalfees_content') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'additionalfees_content');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'additionalfees_content');
                }
            }

            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
	}

	public function getHeaderText()
	{
		if( Mage::registry('additionalfees_data') && Mage::registry('additionalfees_data')->getId() ) {
			return Mage::helper('additionalfees')->__("Edit Fee '%s'", $this->htmlEscape(Mage::registry('additionalfees_data')->getTitle()));
		} else {
			return Mage::helper('additionalfees')->__('Add Fee');
		}
	}
}
