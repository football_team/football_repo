<?php
/*////////////////////////////////////////////////////////////////////////////////
 \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\  Additional Fee  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
 /////////////////////////////////////////////////////////////////////////////////
 \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ NOTICE OF LICENSE\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
 ///////                                                                   ///////
 \\\\\\\ This source file is subject to the Open Software License (OSL 3.0)\\\\\\\
 ///////   that is bundled with this package in the file LICENSE.txt.      ///////
 \\\\\\\   It is also available through the world-wide-web at this URL:    \\\\\\\
 ///////          http://opensource.org/licenses/osl-3.0.php               ///////
 \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
 ///////                      * @category   EH                            ///////
 \\\\\\\                      * @package    EH_Additionalfee                \\\\\\\
 ///////    * @author     Suneet Kumar <suneet64@gmail.com>               ///////
 \\\\\\\                                                                   \\\\\\\
 /////////////////////////////////////////////////////////////////////////////////
 \\\\\\* @copyright  Copyright 2014 © www.extensionhut.com All right reserved\\\\\
 /////////////////////////////////////////////////////////////////////////////////
 */
 
class EH_Additionalfees_Block_Adminhtml_Sales_Order_Creditmemo_Totals extends Mage_Adminhtml_Block_Sales_Order_Creditmemo_Totals
{
    protected function _initTotals()
    {       
        parent::_initTotals();

        $arr=unserialize($this->getSource()->getDetailAdditionalfee());
	if(Mage::getStoreConfig('additionalfees_section/additionalfees_group1/title')!='') {
	    $str=Mage::getStoreConfig('additionalfees_section/additionalfees_group1/title')."<br/>";
	}
	foreach($arr as $key=>$val)
	{
	    $str.=$val['title'].": ".$val['price']."<br/>";	
	}		 

	if ($this->getSource()->getAdditionalfee() > 0) {
		$additionalfees = new Varien_Object(array(
		    'code'      => 'additionalfees',
		    'strong'    => false,
		    'value'     => $this->getSource()->getAdditionalfee(),
		    'base_value'=> $this->getSource()->getBaseAdditionalfee(),
		    'label'     => $str,
		));

		$this->addTotalBefore($additionalfees, 'grand_total');
	}
        
        return $this;
    }
}
