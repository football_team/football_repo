<?php
/*////////////////////////////////////////////////////////////////////////////////
 \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\  Additional Fee  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
 /////////////////////////////////////////////////////////////////////////////////
 \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ NOTICE OF LICENSE\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
 ///////                                                                   ///////
 \\\\\\\ This source file is subject to the Open Software License (OSL 3.0)\\\\\\\
 ///////   that is bundled with this package in the file LICENSE.txt.      ///////
 \\\\\\\   It is also available through the world-wide-web at this URL:    \\\\\\\
 ///////          http://opensource.org/licenses/osl-3.0.php               ///////
 \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
 ///////                      * @category   EH                            ///////
 \\\\\\\                      * @package    EH_Additionalfee                \\\\\\\
 ///////    * @author     Suneet Kumar <suneet64@gmail.com>               ///////
 \\\\\\\                                                                   \\\\\\\
 /////////////////////////////////////////////////////////////////////////////////
 \\\\\\* @copyright  Copyright 2014 © www.extensionhut.com All right reserved\\\\\
 /////////////////////////////////////////////////////////////////////////////////
 */
 
class EH_Additionalfees_Block_Adminhtml_Additionalfees_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
	public function __construct()
	{
		parent::__construct();
		$this->setId('additionalfeesGrid');
		$this->setDefaultSort('additionalfees_id');
		$this->setDefaultDir('ASC');
		$this->setSaveParametersInSession(true);
	}

	protected function _prepareCollection()
	{
		$collection = Mage::getModel('additionalfees/additionalfees')->getCollection();
		$this->setCollection($collection);
		return parent::_prepareCollection();
	}

	protected function _prepareColumns()
	{
		$this->addColumn('additionalfees_id', array(
		'header'    => Mage::helper('additionalfees')->__('ID'),
		'width'     => '200px',
		'align'     =>'right',
		'width'     => '50px',
		'index'     => 'additionalfees_id',
		));
		
		$this->addColumn('additionalfees_id', array(
		'header'    => Mage::helper('additionalfees')->__('Fee Name'),
		'align'     =>'left',
		'index'     => 'title',	        
		));
		
		$this->addColumn('feetype', array(
		'header'    => Mage::helper('additionalfees')->__('Type'),
		'align'     =>'left',
		'index'     => 'feetype',	        
		));	
		
		$this->addColumn('feeamount', array(
		'header'    => Mage::helper('additionalfees')->__('Amount'),
		'align'     =>'left',
		'index'     => 'additionalfees_id',	
		'renderer'  => new EH_Additionalfees_Block_Adminhtml_Renderer_Feedetail(),      
		));		
			
		$this->addColumn('apply_to', array(
		'header'    => Mage::helper('additionalfees')->__('Applied to'),
		'width'     => '150px',
		'index'     => 'apply_to',		
		));
		
		$this->addColumn('mandatory', array(
		'header'    => Mage::helper('additionalfees')->__('Is Mandatory'),
		'width'     => '150px',
		'index'     => 'mandatory',		
		));

		$this->addColumn('flatfee', array(
		'header'    => Mage::helper('additionalfees')->__('Flat Fee'),
		'width'     => '150px',
		'index'     => 'flatfee',		
		));

		$this->addColumn('status', array(
		'header'    => Mage::helper('additionalfees')->__('Status'),
		'width'     => '150px',
		'index'     => 'status',
		'type'      => 'options',
		'options'   => array(
				      1 => 'Enabled',
				      2 => 'Disabled',
				  ),
		));

		/*$this->addColumn('tax_apply', array(
		'header'    => Mage::helper('additionalfees')->__('Tax to apply'),
		'width'     => '150px',
		'index'     => 'additionalfees_id',	
		'renderer'  => new EH_Additionalfees_Block_Adminhtml_Renderer_Taxdetail(),	
		));*/

		$this->addColumn('action',
		array(
		'header'    =>  Mage::helper('additionalfees')->__('Action'),
		'width'     => '100',
		'type'      => 'action',
		'getter'    => 'getId',
		'actions'   => array(
		array(
		'caption'   => Mage::helper('additionalfees')->__('Edit'),
		'url'       => array('base'=> '*/*/edit'),
		'field'     => 'id'
		)
		),
		'filter'    => false,
		'sortable'  => false,
		'index'     => 'stores',
		'is_system' => true,
		));

		//$this->addExportType('*/*/exportCsv', Mage::helper('additionalfees')->__('CSV'));
		//$this->addExportType('*/*/exportXml', Mage::helper('additionalfees')->__('XML'));

		return parent::_prepareColumns();
	}

	protected function _prepareMassaction()
	{
		$this->setMassactionIdField('additionalfees_id');
		$this->getMassactionBlock()->setFormFieldName('additionalfees');

		$this->getMassactionBlock()->addItem('delete', array(
		'label'    => Mage::helper('additionalfees')->__('Delete'),
		'url'      => $this->getUrl('*/*/massDelete'),
		'confirm'  => Mage::helper('additionalfees')->__('Are you sure?')
		));
		
		return $this;
	}

	public function getRowUrl($row)
	{
		return $this->getUrl('*/*/edit', array('id' => $row->getId()));
	}

}
