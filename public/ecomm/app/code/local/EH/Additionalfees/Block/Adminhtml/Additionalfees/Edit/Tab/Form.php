<?php
/*////////////////////////////////////////////////////////////////////////////////
 \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\  Additional Fee  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
 /////////////////////////////////////////////////////////////////////////////////
 \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ NOTICE OF LICENSE\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
 ///////                                                                   ///////
 \\\\\\\ This source file is subject to the Open Software License (OSL 3.0)\\\\\\\
 ///////   that is bundled with this package in the file LICENSE.txt.      ///////
 \\\\\\\   It is also available through the world-wide-web at this URL:    \\\\\\\
 ///////          http://opensource.org/licenses/osl-3.0.php               ///////
 \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
 ///////                      * @category   EH                            ///////
 \\\\\\\                      * @package    EH_Additionalfee                \\\\\\\
 ///////    * @author     Suneet Kumar <suneet64@gmail.com>               ///////
 \\\\\\\                                                                   \\\\\\\
 /////////////////////////////////////////////////////////////////////////////////
 \\\\\\* @copyright  Copyright 2014 © www.extensionhut.com All right reserved\\\\\
 /////////////////////////////////////////////////////////////////////////////////
 */
 
class EH_Additionalfees_Block_Adminhtml_Additionalfees_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
	protected function _prepareForm()
	{
		$form = new Varien_Data_Form();
		$this->setForm($form);
		$fieldset = $form->addFieldset('additionalfees_form', array('legend'=>Mage::helper('additionalfees')->__('Item information')));
		
		$fieldset->addField('title', 'text', array(
		'label'     => Mage::helper('additionalfees')->__('Fee Name'),
		'class'     => 'required-entry',
		'required'  => true,
		'name'      => 'title',
		));

// 		$fieldset->addField('filename', 'file', array(
// 		'label'     => Mage::helper('additionalfees')->__('File'),
// 		'required'  => false,
// 		'name'      => 'filename',
// 		));
		$fieldset->addField('feetype', 'select', array(
		'label'     => Mage::helper('additionalfees')->__('Type'),
		'name'      => 'feetype',
		'values'    => array(
		    array(
		    'value'     => 'Fixed',
		    'label'     => Mage::helper('additionalfees')->__('Fixed'),
		    ),

		    array(
		    'value'     => 'Percentage',
		    'label'     => Mage::helper('additionalfees')->__('Percentage'),
		    ),
		),
		));

		$fieldset->addField('feeamount', 'text', array(
		'label'     => Mage::helper('additionalfees')->__('Amount'),
		'class'     => 'required-entry',
		'required'  => true,
		'name'      => 'feeamount',
		));

		$eventElem=$fieldset->addField('apply_to', 'select', array(
		'label'     => Mage::helper('additionalfees')->__('Apply To'),
		'name'      => 'apply_to',		
		'values'    => array(
		    array(
		    'value'     => 0,
		    'label'     => Mage::helper('additionalfees')->__('Please Select'),
		    ),

		    array(
		    'value'     => 'Product',
		    'label'     => Mage::helper('additionalfees')->__('Product'),
		    ),

		    array(
		    'value'     => 'Category',
		    'label'     => Mage::helper('additionalfees')->__('Category'),
		    ),

		    array(
		    'value'     => 'Shipping',
		    'label'     => Mage::helper('additionalfees')->__('Shipping'),
		    ),

		    array(
		    'value'     => 'Order',
		    'label'     => Mage::helper('additionalfees')->__('Order'),
		    ),  
		),
		 'onchange'  => "modifyTargetElement(this);", 
		));

		  $categories = array();
		  $collection = Mage::getModel('catalog/category')->getCollection()->setOrder('sort_order', 'asc');
		  $collection = Mage::getModel('catalog/category')
				->getCollection()
				->addAttributeToSelect('*')
				->addIsActiveFilter();
		  foreach ($collection as $cat) {
		      $categories[] = (array(
			  'label' => (string) $cat->getName() , 
			  'value' => $cat->getId()
		      ));
		  }
		  $fieldset->addField('category', 'multiselect', array(
		      'name' => 'category[]' , 
		      'label' => Mage::helper('additionalfees')->__('Category') , 
		      'title' => Mage::helper('additionalfees')->__('Category') , 
		      'required' => true , 
		      'style' => 'height:100px' , 
		      'values' => $categories,
		      'note' => 'This item is visible only for category type fee',				
		      'disabled' =>true
		  ));

		$fieldset->addField('mandatory', 'select', array(
		'label'     => Mage::helper('additionalfees')->__('Is Mandatory'),
		'name'      => 'mandatory',
		'values'    => array(
		    array(
		    'value'     => 'No',
		    'label'     => Mage::helper('additionalfees')->__('No'),
		    ),

		    array(
		    'value'     => 'Yes',
		    'label'     => Mage::helper('additionalfees')->__('Yes'),
		    ),
		),
		));

		$fieldset->addField('flatfee', 'select', array(
		'label'     => Mage::helper('additionalfees')->__('Flat Fee'),
		'name'      => 'flatfee',
		'disabled' =>true,
		'note' => 'This item is visible only for product & category type fee',	
		'values'    => array(
		    array(
		    'value'     => 'No',
		    'label'     => Mage::helper('additionalfees')->__('No'),
		    ),

		    array(
		    'value'     => 'Yes',
		    'label'     => Mage::helper('additionalfees')->__('Yes'),
		    ),
		),
		));

		$fieldset->addField('status', 'select', array(
		'label'     => Mage::helper('additionalfees')->__('Status'),
		'name'      => 'status',
		'values'    => array(
		    array(
		    'value'     => 1,
		    'label'     => Mage::helper('additionalfees')->__('Enabled'),
		    ),

		    array(
		    'value'     => 2,
		    'label'     => Mage::helper('additionalfees')->__('Disabled'),
		    ),
		),
		));

// 		$fieldset->addField('content', 'editor', array(
// 		'name'      => 'content',
// 		'label'     => Mage::helper('additionalfees')->__('Content'),
// 		'title'     => Mage::helper('additionalfees')->__('Content'),
// 		'style'     => 'width:700px; height:500px;',
// 		'wysiwyg'   => false,
// 		'required'  => true,
// 		));

		$fieldset->addField('store_id', 'multiselect', array(
                'name'      => 'stores[]',
                'label'     => Mage::helper('cms')->__('Store View'),
                'title'     => Mage::helper('cms')->__('Store View'),
                'required'  => true,
                'values'    => Mage::getSingleton('adminhtml/system_store')->getStoreValuesForForm(false, true),
            ));

		$eventElem->setAfterElementHtml('<script type="text/javascript"> 			      
			function modifyTargetElement(checkboxElem){  
			    switch(checkboxElem.value) {
				case "Product":
				      document.getElementById("flatfee").disabled=false;
				      document.getElementById("category").disabled=true;
				      break;
				case "Category":
				      document.getElementById("flatfee").disabled=false;
				      document.getElementById("category").disabled=false;
				      break;
				case "Shipping":
				      document.getElementById("flatfee").disabled=true;
				      document.getElementById("category").disabled=true;
				      break;
				case "Order":
				      document.getElementById("flatfee").disabled=true;
				      document.getElementById("category").disabled=true;
				      break;				
// 				default:
// 				      document.getElementById("flatfee").disabled=true;
// 				      document.getElementById("category").disabled=true;
// 				      break;
			    }
			}
		    </script>');

		 
		if ( Mage::getSingleton('adminhtml/session')->getAdditionalfeesData() )
		{
			$form->setValues(Mage::getSingleton('adminhtml/session')->getAdditionalfeesData());
			Mage::getSingleton('adminhtml/session')->setAdditionalfeesData(null);
		} elseif ( Mage::registry('additionalfees_data') ) {
			$formData=Mage::registry('additionalfees_data')->getData();
			
			if($formData['apply_to']!='Category'){
			      $targetElement=$form->getElement("category");			  
			      $targetElement->setDisabled(true);
			      //$targetElement->setReadonly(true);			  
			} else {
			      $targetElement=$form->getElement("category");			  
			      $targetElement->setDisabled(false);	  
			}
			if($formData['apply_to']=='Category' || $formData['apply_to']=='Product')
			{
			      $targetElement=$form->getElement("flatfee");			  
			      $targetElement->setDisabled(false);
			} else {
			      $targetElement=$form->getElement("flatfee");			  
			      $targetElement->setDisabled(true);	  
			}
		      
			$form->setValues(Mage::registry('additionalfees_data')->getData());
		}
		return parent::_prepareForm();
	}
}
