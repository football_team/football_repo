<?php
/*////////////////////////////////////////////////////////////////////////////////
 \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\  Additional Fee  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
 /////////////////////////////////////////////////////////////////////////////////
 \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ NOTICE OF LICENSE\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
 ///////                                                                   ///////
 \\\\\\\ This source file is subject to the Open Software License (OSL 3.0)\\\\\\\
 ///////   that is bundled with this package in the file LICENSE.txt.      ///////
 \\\\\\\   It is also available through the world-wide-web at this URL:    \\\\\\\
 ///////          http://opensource.org/licenses/osl-3.0.php               ///////
 \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
 ///////                      * @category   EH                            ///////
 \\\\\\\                      * @package    EH_Additionalfee                \\\\\\\
 ///////    * @author     Suneet Kumar <suneet64@gmail.com>               ///////
 \\\\\\\                                                                   \\\\\\\
 /////////////////////////////////////////////////////////////////////////////////
 \\\\\\* @copyright  Copyright 2014 © www.extensionhut.com All right reserved\\\\\
 /////////////////////////////////////////////////////////////////////////////////
 */
 
$installer = $this;

$installer->startSetup();

$installer->run("

DROP TABLE IF EXISTS {$this->getTable('additionalfees')};
CREATE TABLE {$this->getTable('additionalfees')} (
  `additionalfees_id` int(11) unsigned NOT NULL auto_increment,
  `title` varchar(255) default NULL,
  `feetype` enum('Fixed','Percentage') default 'Fixed',
  `feeamount` decimal(10,2) default NULL,
  `apply_to` varchar(255) default NULL,
  `mandatory` enum('Yes','No') default NULL,
  `status` smallint(6) default '1', 
  `category` varchar(255) default NULL,
  PRIMARY KEY  (`additionalfees_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=0 ;




ALTER TABLE {$this->getTable('sales_flat_quote_item')} ADD `payment_fee` TEXT NULL ,
ADD `payment_str` TEXT NULL , ADD `additional_extrafee` TEXT NULL ;

ALTER TABLE {$this->getTable('sales_flat_order_item')} ADD `payment_fee` TEXT NULL ,
ADD `payment_str` TEXT NULL ;

ALTER TABLE {$this->getTable('sales_flat_invoice_item')} ADD `payment_fee` TEXT NULL ,
ADD `payment_str` TEXT NULL ;

ALTER TABLE {$this->getTable('sales_flat_creditmemo_item')} ADD `payment_fee` TEXT NULL ,
ADD `payment_str` TEXT NULL ;

ALTER TABLE {$this->getTable('sales_flat_shipment_item')} ADD `payment_fee` TEXT NULL ,
ADD `payment_str` TEXT NULL ;

    ");

$installer->getConnection()->addColumn($installer->getTable('additionalfees'),'flatfee', 'ENUM( "Yes", "No" ) NOT NULL DEFAULT "No" ') ;



$installer->getConnection()->addColumn($installer->getTable('sales_flat_quote'),'additionalfee', 'decimal(10,2) NULL') ;
$installer->getConnection()->addColumn($installer->getTable('sales_flat_quote'),'base_additionalfee', 'decimal(10,2) NULL') ;
$installer->getConnection()->addColumn($installer->getTable('sales_flat_quote'),'detail_additionalfee', 'TEXT NULL') ;

$installer->getConnection()->addColumn($installer->getTable('sales_flat_quote_address'),'additionalfee', 'decimal(10,2) NULL') ;
$installer->getConnection()->addColumn($installer->getTable('sales_flat_quote_address'),'base_additionalfee', 'decimal(10,2) NULL') ;
$installer->getConnection()->addColumn($installer->getTable('sales_flat_quote_address'),'detail_additionalfee', 'TEXT NULL') ;
$installer->getConnection()->addColumn($installer->getTable('sales_flat_quote_address'),'additionalfee_excl_tax', 'decimal(10,2) NULL') ;
$installer->getConnection()->addColumn($installer->getTable('sales_flat_quote_address'),'base_additionalfee_incl_tax', 'decimal(10,2) NULL') ;

$installer->getConnection()->addColumn($installer->getTable('sales_flat_order'),'additionalfee', 'decimal(10,2) NULL') ;
$installer->getConnection()->addColumn($installer->getTable('sales_flat_order'),'base_additionalfee', 'decimal(10,2) NULL') ;
$installer->getConnection()->addColumn($installer->getTable('sales_flat_order'),'detail_additionalfee', 'TEXT NULL') ;

$installer->getConnection()->addColumn($installer->getTable('sales_flat_invoice'),'additionalfee', 'decimal(10,2)  NULL') ;
$installer->getConnection()->addColumn($installer->getTable('sales_flat_invoice'),'base_additionalfee', 'decimal(10,2) NULL') ;
$installer->getConnection()->addColumn($installer->getTable('sales_flat_invoice'),'detail_additionalfee', 'TEXT NULL') ;

$installer->getConnection()->addColumn($installer->getTable('sales_flat_creditmemo'),'additionalfee', 'decimal(10,2) NULL') ;
$installer->getConnection()->addColumn($installer->getTable('sales_flat_creditmemo'),'base_additionalfee', 'decimal(10,2) NULL') ;
$installer->getConnection()->addColumn($installer->getTable('sales_flat_creditmemo'),'detail_additionalfee', 'TEXT NULL') ;

$installer->getConnection()->addColumn($installer->getTable('additionalfees'),'store_id', 'TEXT NULL') ;

$installer->endSetup(); 

$setup = new Mage_Eav_Model_Entity_Setup('core_setup');
$setup->addAttribute('catalog_product', 'additional_extrafee', array(
    'group'         => 'Additional Fee',
    'input'         => 'multiselect',
    'required'      => false,
    'type'          => 'text',
    'label'         => 'Select Additional Fee',
    'source'        => 'additionalfees/source_option',
    'backend'       => 'additionalfees/backend_option',
    'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'visible'       => 1,
    'user_defined'      => true
));
$setup->addAttribute('catalog_product', 'additional_catoverride', array(
        'group'             => 'Additional Fee',
        'type'              => 'int',
        'backend'           => '',
        'frontend'          => '',
        'label'             => 'Override Category Fees',
        'input'             => 'boolean',
        'class'             => '',
        'source'            => '',
        'global'            => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'visible'           => true,
        'required'          => false,
        'user_defined'      => true,
        'default'           => '0',
        'searchable'        => false,
        'filterable'        => false,
        'comparable'        => false, 
        'visible_on_front'  => false,
        'unique'            => false,        
        'is_configurable'   => false
    ));
