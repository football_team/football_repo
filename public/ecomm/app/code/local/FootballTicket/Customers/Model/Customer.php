<?php

class FootballTicket_Customers_Model_Customer extends Mage_Core_Model_Abstract {
    private $customerBankTable = 'ftp_customer_bank';
    private $customerCardTable = 'ftp_customer_card';

    protected $customerId = null;
    protected $customerBank = array();
    protected $customerCard = array();

    public function _construct() {
        parent::_construct();

        Mage::getSingleton('core/session', array('name'=>'frontend'));
        $session = Mage::getSingleton('customer/session', array('name'=>'frontend'));
        //$customer_data = Mage::getModel('customer/customer')->$session->id;

        if($session->isLoggedIn()){
            $customer = Mage::getSingleton('customer/session')->getCustomer();
            $this->customerId = $customer->getData('id');
            $this->getCustomerAdditionalInfo();
        }
    }

    /**
     * @param null $customerId
     */
    public function setCustomerId($customerId)
    {
        $this->customerId = $customerId;
    }

    /**
     * @return array
     */
    public function getCustomerBank()
    {
        return $this->customerBank;
    }

    /**
     * @return array
     */
    public function getCustomerCard()
    {
        return $this->customerCard;
    }

    public function getCustomerAdditionalInfo() {

    }

    public function saveCustomerBankInfo(array $data=array()) {
        if(empty($data)) {
            return;
        }
        //TODO: check encrypted id
        if( isset($data['id'])) {
            //TODO: Update Customer Bank info
        } else {
            //TODO: Insert Customer Card info
        }
    }

    public function saveCustomerCardInfo(array $data=array()) {
        if(empty($data)) {
            return;
        }

        //TODO: check encrypted id
        if( isset($data['id'])) {
            //TODO: Update Customer Bank info
        } else {
            //TODO: Insert Customer Card info
        }
    }


}