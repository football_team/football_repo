<?php
require_once(Mage::getModuleDir('controllers','FootballTicket_TicketCatalog').DS.'BaseController.php');

class FootballTicket_Customers_ProductController extends FootballTicket_TicketCatalog_BaseController
{
    public function indexAction ()
    {
        $param = $this->getRequest()->getParams();
        $productModel = Mage::getModel('catalog/product');
        $product = $productModel->load($param['product_id']);

        if(isset($param['qty'])) {
            // qty update
            $product->setStockData(array(
                'qty' => $param['qty']
            ));
        } else if($param['price']) {
            $product->setPrice($param['price']);
        }

        $product->save();
        $output = array('success');
        $response = Mage::getModel('TicketCatalog/response')->setTotalPages(1);
        $response->setOutputContent($output);
        $this->dispatchJSON($response);
    }


}