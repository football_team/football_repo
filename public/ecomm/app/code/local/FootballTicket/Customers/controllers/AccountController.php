<?php
require_once(Mage::getModuleDir('controllers','FootballTicket_TicketCatalog').DS.'BaseController.php');

class FootballTicket_Customers_AccountController extends FootballTicket_TicketCatalog_BaseController
{
    public function indexAction ()
    {

    }

    public function passwordchangeAction() {
        $passwordLength = '8';
        $input = Mage::app()->getFrontController()->getRequest()->getParams();
        $customer_email = $input['email'];
        $response = Mage::getModel('TicketCatalog/response')->setTotalPages(1);
        try {
            $customer = Mage::getModel("customer/customer");
            $customer->setWebsiteId(Mage::app()->getWebsite()->getId());
            $customer->loadByEmail($customer_email);
            $customer->setPassword($customer->generatePassword($passwordLength))->save();
            $customer->sendNewAccountEmail();

            $response->setOutputContent('success');
            $this->dispatchJSON($response);

        } catch(Exception $e) {
            $response->setOutputContent($e->getMessage());
            $this->dispatchJSON($response);
        }

    }

    public function send_confirmation_emailAction() {
        $input = Mage::app()->getFrontController()->getRequest()->getParams();
        $customer_email = $input['email'];
        $response = Mage::getModel('TicketCatalog/response')->setTotalPages(1);
        try {
            $customer = Mage::getModel("customer/customer");
            $customer->setWebsiteId(Mage::app()->getWebsite()->getId());
            $customer->loadByEmail($customer_email);
            $storeId = $customer->getSendemailStoreId();
            $customer->sendNewAccountEmail('registered', '', $storeId);

            $response->setOutputContent('success');
            $this->dispatchJSON($response);

        } catch(Exception $e) {
            $response->setOutputContent($e->getMessage());
            $this->dispatchJSON($response);
        }

    }

}