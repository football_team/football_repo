<?php
require_once(Mage::getModuleDir('controllers','FootballTicket_TicketCatalog').DS.'BaseController.php');

class FootballTicket_Customers_IndexController extends FootballTicket_TicketCatalog_BaseController
{
    public function indexAction ()
    {
        try {
            $output = array();
            $customers = mage::getModel('customer/customer')->getCollection()
                ->addAttributeToSelect('*')
                ->addAttributeToSort('email', 'ASC');
            foreach($customers as $customer) {
                $data = $customer->getData();
                unset($data['password_hash']);
                $output[] = $data;
            }
            $response = Mage::getModel('TicketCatalog/response')
                ->setOutputContent($output);

            $this->dispatchJSON($response);
        } catch (Exception $e) {
            $this->dispatchError($e->getMessage(),$e->getCode());
        }
    }

    public function infoAction() {
        try {
            $output = array();
            Mage::getSingleton('core/session', array('name'=>'frontend'));
            $session = Mage::getSingleton('customer/session', array('name'=>'frontend'));
            //$customer_data = Mage::getModel('customer/customer')->$session->id;

            if(!$session->isLoggedIn()){
                throw new Exception('Customer not found!', 400);
            }

            $customer = Mage::getSingleton('customer/session')->getCustomer();
            $customerId = $customer->getData('id');
            // load customer
            $customer = Mage::getModel('customer/customer')->load($customerId);

            $output = $customer->getData();
            foreach ($customer->getAddresses() as $address)
            {
                $output['address'][] = $address->toArray();
            }


            $response = Mage::getModel('TicketCatalog/response')
                ->setOutputContent($output)
                ->setTotalPages(1);
            $this->dispatchJSON($response);
        } catch ( Exception $e) {
            $this->dispatchError($e->getMessage(),$e->getCode());
        }
    }

    public function loginAction()
    {

        $response = Mage::getModel('TicketCatalog/response')->setTotalPages(1);
        $session = Mage::getSingleton('customer/session');

        if ($session->isLoggedIn()) {
            $response->setOutputContent(array('message'=>'success'));
            $this->dispatchJSON($response);
            return $this;
        }

        try {
            if ($this->getRequest()->isPost())
            {
                $login_data = $this->getRequest()->getPost('login');
                if (empty($login_data['username']) || empty($login_data['password'])) {
                    throw new Exception ('Login and password are required.', 400);
                }
                else
                {
                    try
                    {
                        $session->login($login_data['username'], $login_data['password']);
                        $response->setOutputContent(array('message'=>'success'));
                        $this->dispatchJSON($response);
                    }
                    catch (Mage_Core_Exception $e)
                    {
                        switch ($e->getCode()) {
                            case Mage_Customer_Model_Customer::EXCEPTION_EMAIL_NOT_CONFIRMED:
                                throw new Exception ( 'Email is not confirmed. <a href="#">Resend confirmation email.</a>', 400);
                                break;
                            default:
                                throw new Exception ($e->getMessage(), 400);
                        }
                    }
                }
            } else {
                throw new Exception ('Login get request is not allowed', 400);
            }

        } catch (Exception $e) {
            $this->dispatchError($e->getMessage(),$e->getCode());
        }
    }

    public function bankAction() {
        $type = strtolower($this->getRequest()->getPost('type'));
        $customerId = $this->getRequest()->getPost('customer_id');
        $response = Mage::getModel('TicketCatalog/response')->setTotalPages(1);
        $tableFtpCustomerAccountBank = Mage::getSingleton( 'core/resource' )->getTableName( 'ftp_customer_bank' );
        try{
            switch($type) {
                case "get":
                    $read = Mage::getSingleton( 'core/resource' )->getConnection( 'core_read' );

                    $query = "SELECT * FROM " . $tableFtpCustomerAccountBank . "
                              WHERE  active = 1 AND entity_id = :customerId
                              LIMIT 1
                              ";
                    $binds = array('customerId' => $customerId);
                    $result = $read->query( $query, $binds );
                    $row = $result->fetch();
                    if($row) {
                        $response->setOutputContent($row);
                        $this->dispatchJSON($response);
                    } else {
                        throw new Exception ('Not exists', 404);
                    }
                    break;
                case "set":
                    $write = Mage::getSingleton( 'core/resource' )->getConnection( 'core_write' );

                    $updated_at = date('Y-m-d H:i:s');
                    $query = " SET  account_holder = :account_holder,
                                    iban = :iban,
                                    bic_swift= :bic_swift,
                                    paypal_email= :paypal_email,
                                    active = 1,
                                    entity_id = :customerId,
                                    updated_at = '$updated_at' ";
                    $binds = array(
                        'account_holder' => $this->getRequest()->getPost('account_holder'),
                        'iban'           => $this->getRequest()->getPost('iban'),
                        'bic_swift'      => $this->getRequest()->getPost('bic_swift'),
                        'paypal_email'   => $this->getRequest()->getPost('paypal_email'),
                        'customerId'     => $customerId
                    );

                    $id = $this->getRequest()->getPost('id');


                    if(empty($id)) {
                        $query = "INSERT INTO  {$tableFtpCustomerAccountBank} {$query}";
                    } else {
                        $query = "UPDATE {$tableFtpCustomerAccountBank} {$query}
                                  WHERE id = :id";

                        $binds['id'] = (int) $id;
                    }

                    $write->query( $query, $binds );
                    $lastInsertedId = $write->lastInsertId();

                    if($lastInsertedId) {
                        $binds['id'] = $lastInsertedId;
                    }
                    $response->setOutputContent($binds);
                    $this->dispatchJSON($response);
                    break;
                default:
                    throw new Exception ('Bad Request', 400);
            }
        } catch (Exception $e) {
            $this->dispatchError($e->getMessage(),$e->getCode());
        }

    }


    public function cardAction() {
        $type = strtolower($this->getRequest()->getPost('type'));
        $customerId = $this->getRequest()->getPost('customer_id');
        $response = Mage::getModel('TicketCatalog/response')->setTotalPages(1);
        $tableFtpCustomerAccountBank = Mage::getSingleton( 'core/resource' )->getTableName( 'ftp_customer_card' );
        try{
            switch($type) {
                case "get":
                    $read = Mage::getSingleton( 'core/resource' )->getConnection( 'core_read' );
                    $return = $this->getRequest()->getPost('return');

                    if($return == 'all') {
                        $query = "SELECT * FROM " . $tableFtpCustomerAccountBank . "
                              WHERE  active = 1 AND entity_id = :customerId";
                        $binds = array('customerId' => $customerId);
                        $result = $read->query( $query, $binds );
                        $cards = $result->fetchAll();
                        if($cards) {
                            $response->setOutputContent($cards);
                            $this->dispatchJSON($response);
                        } else {
                            throw new Exception ('Not exists', 404);
                        }
                    } else {
                        $query = "SELECT * FROM " . $tableFtpCustomerAccountBank . "
                              WHERE  active = 1 AND entity_id = :customerId
                              LIMIT 1";
                        $binds = array('customerId' => $customerId);
                        $result = $read->query( $query, $binds );
                        $row = $result->fetch();
                        if($row) {
                            $response->setOutputContent($row);
                            $this->dispatchJSON($response);
                        } else {
                            throw new Exception ('Not exists', 404);
                        }
                    }
                    break;
                case "set":
                    $write = Mage::getSingleton( 'core/resource' )->getConnection( 'core_write' );

                    $updated_at = date('Y-m-d H:i:s');

                    $query = " SET  account_holder = :account_holder,
                                    card_type = :card_type,
                                    card_number= :card_number,
                                    expire_date= :expire_date,
                                    start_date= :start_date,
                                    active = 1,
                                    entity_id = :customerId,
                                    updated_at = '$updated_at'
                                    ";
                    $binds = array(
                        'account_holder'   => $this->getRequest()->getPost('account_holder'),
                        'card_type'        => $this->getRequest()->getPost('card_type'),
                        'card_number'      => $this->getRequest()->getPost('card_number'),
                        'expire_date'      => $this->getRequest()->getPost('expire_date'),
                        'start_date'       => $this->getRequest()->getPost('start_date'),
                        'customerId'       => $customerId
                    );

                    $id = $this->getRequest()->getPost('id');


                    if(empty($id)) {
                        $query = "INSERT INTO {$tableFtpCustomerAccountBank} {$query}";
                    } else {
                        $query = "UPDATE {$tableFtpCustomerAccountBank} {$query}
                                  WHERE id = :id";

                        $binds['id'] = $id;
                    }

                    $write->query( $query, $binds );
                    $lastInsertedId = $write->lastInsertId();

                    if($lastInsertedId) {
                        $binds['id'] = $lastInsertedId;
                    }
                    $response->setOutputContent($binds);
                    $this->dispatchJSON($response);
                    break;
                default:
                    throw new Exception ('Bad Request', 400);
            }
        } catch (Exception $e) {
            $this->dispatchError($e->getMessage(),$e->getCode());
        }

    }

    public function personalAction() {
        $output = array();
        $type = strtolower($this->getRequest()->getPost('type'));
        $customerId = $this->getRequest()->getPost('customer_id');

        $customer = Mage::getModel("customer/customer");
        $customer = $customer->load($customerId);
        $customerArray = $customer->getData();

        $output['firstname'] = $customerArray['firstname'];
        $output['lastname'] = $customerArray['lastname'];
        $output['email'] = $customerArray['email'];

        $tableFtpCustomerAccountBank = Mage::getSingleton( 'core/resource' )->getTableName( 'ftp_customer' );
        $response = Mage::getModel('TicketCatalog/response')->setTotalPages(1);
        $subscriber = Mage::getModel('newsletter/subscriber')->loadByEmail($output['email']);
        if($subscriber->getId())
        {
            //$subscriber->setData('subscriber_status', Mage_Newsletter_Model_Subscriber::STATUS_UNSUBSCRIBED);
            //$subscriber->save();
            if($this->getRequest()->getPost('email') != '') {
                if($this->getRequest()->getPost('subscribed')) {
                    $subscriber->setData('subscriber_status', Mage_Newsletter_Model_Subscriber::STATUS_SUBSCRIBED);
                } else {
                    $subscriber->setData('subscriber_status', Mage_Newsletter_Model_Subscriber::STATUS_UNSUBSCRIBED);
                }
                $subscriber->save();
            }


            $output['subscribed'] = $subscriber->getData('subscriber_status') == Mage_Newsletter_Model_Subscriber::STATUS_SUBSCRIBED;
        }


        try{
            switch($type) {
                case "get":


                    $read = Mage::getSingleton( 'core/resource' )->getConnection( 'core_read' );

                    $query = "SELECT id, phone FROM " . $tableFtpCustomerAccountBank . "
                              WHERE  entity_id = :customerId
                              LIMIT 1";
                    $binds = array('customerId' => $customerId);
                    $result = $read->query( $query, $binds );
                    $row = $result->fetch();


                    if($row) {
                        $output = $output+$row;
                        $response->setOutputContent($output);
                        $this->dispatchJSON($response);
                    } else {
                        $response->setOutputContent($output);
                        $this->dispatchJSON($response);
                    }
                    return;
                    break;
                case "set":
                    $write = Mage::getSingleton( 'core/resource' )->getConnection( 'core_write' );
                    $output['firstname'] = $this->getRequest()->getPost('firstname');
                    $output['lastname'] = $this->getRequest()->getPost('lastname');
                    $output['email'] = $this->getRequest()->getPost('email');

                    if(isset($output['firstname'])) {
                        $customer->setData('firstname', $output['firstname']);
                    }

                    if(isset($output['firstname'])) {
                        $customer->setData('lastname', $output['lastname']);
                    }

                    if(isset($output['email'])) {
                        $customer->setData('email', $output['email']);
                    }



                    $customer->save();


                    $query = " SET  phone = :phone,
                                    entity_id = :customerId
                                    ";
                    $binds = array(
                        'phone'             => $this->getRequest()->getPost('phone'),
                        'customerId'        => $customerId
                    );

                    $id = $this->getRequest()->getPost('id');

                    if(empty($id)) {
                        $query = "INSERT INTO {$tableFtpCustomerAccountBank} {$query}";

                    } else {
                        $query = "UPDATE {$tableFtpCustomerAccountBank} {$query}
                                  WHERE id = :id";

                        $binds['id'] = $id;
                    }

                    $write->query( $query, $binds );
                    $lastInsertedId = $write->lastInsertId();

                    if($lastInsertedId) {
                        $output['id'] = $lastInsertedId;
                     }


                    $response->setOutputContent($output);
                    $this->dispatchJSON($response);
                    break;
                default:
                    throw new Exception ('Bad Request', 400);
            }
        } catch (Exception $e) {
            $this->dispatchError($e->getMessage(),$e->getCode());
        }

    }


    public function addressAction() {
        $type = strtolower($this->getRequest()->getPost('type'));
        $addressType = strtolower($this->getRequest()->getPost('address_type'));
        $customerId = $this->getRequest()->getPost('customer_id');
        Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
        $response = Mage::getModel('TicketCatalog/response')->setTotalPages(1);
        $customer = Mage::getModel('customer/customer')->load($customerId);

        try{
            if($addressType == 'shipping') {
                $billingaddress = Mage::getModel('customer/address')->load($customer->default_shipping);
            } else {
                $billingaddress = Mage::getModel('customer/address')->load($customer->default_billing);
            }

            switch($type) {
                case "get":
                    $output = $billingaddress->getData();
                    if($output) {
                        $response->setOutputContent($output);
                        $this->dispatchJSON($response);
                    } else {
                        throw new Exception ('Not exists', 404);
                    }
                    break;
                case "set":
                    $addData = $billingaddress->getData();

                    if($billingaddress && isset($addData['street'])) {
                        $billingaddress->setData('firstname', $this->getRequest()->getPost('firstname'));
                        $billingaddress->setData('lastname', $this->getRequest()->getPost('lastname'));
                        $billingaddress->setData('company', $this->getRequest()->getPost('company'));
                        $billingaddress->setData('vat_id', $this->getRequest()->getPost('vat_id'));
                        $billingaddress->setData('street', $this->getRequest()->getPost('street'));
                        $billingaddress->setData('city', $this->getRequest()->getPost('city'));
                        $billingaddress->setData('country_id', $this->getRequest()->getPost('country_id'));
                        $billingaddress->setData('postcode', $this->getRequest()->getPost('postcode'));
                        $billingaddress->setData('telephone',$this->getRequest()->getPost('telephone'));
                        $billingaddress->save();
                    } else {
                        $firstName = $this->getRequest()->getPost('firstname') != ''? $this->getRequest()->getPost('firstname'):  $customer->getFirstname();
                        $lastName = $this->getRequest()->getPost('lastname') != ''? $this->getRequest()->getPost('lastname'):  $customer->getLastname();

                        try {
                            $addressData =  array (
                                'prefix'              => '',
                                'firstname'           => $firstName,
                                'middlename'          => '',
                                'lastname'            => $lastName,
                                'suffix'              => '',
                                'company'             => $this->getRequest()->getPost('company'),
                                'street'              => $this->getRequest()->getPost('street'),
                                'city'                => $this->getRequest()->getPost('city'),
                                'country_id'          => $this->getRequest()->getPost('country_id'),
                                'region'              => '',
                                'region_id'           => 0,
                                'postcode'            => $this->getRequest()->getPost('postcode'),
                                'telephone'           => $this->getRequest()->getPost('telephone') != ''? $this->getRequest()->getPost('telephone') : '000000000' ,
                                'fax'                 => '0'
                            );

                            if($addressType == 'shipping') {
                                $addressData['is_default_billing'] = '0';
                                $addressData['is_default_shipping'] = '1';
                            } else {
                                $addressData['is_default_billing'] = '1';
                                $addressData['is_default_shipping'] = '0';
                                $addressData['vat_id'] = $this->getRequest()->getPost('vat_id');
                            }

                            $address   = Mage::getModel('customer/address');
                            $address->addData($addressData);
                            $address->setCustomerId($customerId)->setSaveInAddressBook('1');
                            $address->save();
                            if($addressType == 'shipping') {
                                $billingaddress = Mage::getModel('customer/address')->load($customer->default_shipping);
                            } else {
                                $billingaddress = Mage::getModel('customer/address')->load($customer->default_billing);
                            }

                        } catch(Exception $e) {
                            throw new Exception($e->getMessage(), 400);
                        }

                    }

                    $output = $billingaddress->getData();
                    $response->setOutputContent($output);
                    $this->dispatchJSON($response);
                    break;
                default:
                    throw new Exception ('Bad Request', 400);
            }
        } catch (Exception $e) {
            $this->dispatchError($e->getMessage(),$e->getCode());
        }

    }
}