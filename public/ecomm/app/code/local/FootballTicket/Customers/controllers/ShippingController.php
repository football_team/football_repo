<?php
require_once(Mage::getModuleDir('controllers','FootballTicket_TicketCatalog').DS.'BaseController.php');

class FootballTicket_Customers_ShippingController extends FootballTicket_TicketCatalog_BaseController
{
    public function indexAction ()
    {
        $methods = Mage::getSingleton('shipping/config')->getActiveCarriers();
        //var_dump($methods);
        $options = array();
        $response = Mage::getModel('TicketCatalog/response')->setTotalPages(1);
        foreach($methods as $_code => $_method)
        {
            //var_dump($_method);
            if(!$_title = Mage::getStoreConfig("carriers/$_code/title"))
                $_title = $_code;

            $_price = Mage::getStoreConfig("carriers/$_code/price");
            $options[] = array('value' => $_code, 'label' => $_title, 'price'=>$_price );
        }

        $response->setOutputContent($options);
        $this->dispatchJSON($response);
    }


}