<?php

class FootballTicket_TicketCatalog_BaseController extends Mage_Core_Controller_Front_Action {

    protected function dispatchJSON(FootballTicket_TicketCatalog_Model_Response $response, $code = 200 ) {
        $this->getResponse()->clearHeaders()
            ->setHeader('Content-type','application/json',true)
            ->setHeader("status",$code,true);

        $this->getResponse()->setBody(json_encode($response->formatResponse()));
    }

    protected function dispatchError($message = '', $code = 400) {
        $this->getResponse()->clearHeaders()
            ->setHeader('Content-type','application/json',true)
            ->setHeader($_SERVER["SERVER_PROTOCOL"]." $code", $code,true)
            ->setHeader("status",$code,true);

        $this->getResponse()->setBody(json_encode(
            array(
                'error'     => $message,
                'code'      => $code
            )
        ));
    }
}