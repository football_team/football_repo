<?php

$installer = $this;

$installer->startSetup();

$installer->run("
-- DROP TABLE IF EXISTS {$this->getTable('TicketCatalog/tournament')};
CREATE TABLE {$this->getTable('TicketCatalog/tournament')} (
  `entity_id` int(11) unsigned NOT NULL auto_increment,
  `tournament_id` int(11),
  `season_id` int(11),
  `team_id` int(11),
  `comment` text NOT NULL default '',
  `status` smallint(6) NOT NULL default '0',
  `created_time` datetime NULL,
  `update_time` datetime NULL,
  PRIMARY KEY (`entity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;  ");

$installer->run("
-- DROP TABLE IF EXISTS {$this->getTable('TicketCatalog/country')};
CREATE TABLE {$this->getTable('TicketCatalog/country')} (
  `entity_id` int(11) unsigned NOT NULL auto_increment,
  `tournament_id` int(11),
  `country_id` int(11),
  `season_id` int(11),
  `comment` text NOT NULL default '',
  `status` smallint(6) NOT NULL default '0',
  `created_time` datetime NULL,
  `update_time` datetime NULL,
  PRIMARY KEY (`entity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;  ");

$installer->run("
-- DROP TABLE IF EXISTS {$this->getTable('TicketCatalog/team')};
CREATE TABLE {$this->getTable('TicketCatalog/team')} (
  `entity_id` int(11) unsigned NOT NULL auto_increment,
  `team_id` int(11),
  `country_id` int(11),
  `season_id` int(11),
  `comment` text NOT NULL default '',
  `status` smallint(6) NOT NULL default '0',
  `created_time` datetime NULL,
  `update_time` datetime NULL,
  PRIMARY KEY (`entity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;  ");

$installer->endSetup();