<?php

class FootballTicket_TicketCatalog_Model_Response extends Mage_Core_Model_Abstract
{
    protected $outputContent;
    protected $totalRow = 0;
    protected $currentPage = 1;
    protected $numPages = 1;
    protected $totalPages = 1;

    public function _construct() {
        return $this;
    }

    /**
     * @param int $totalPages
     */
    public function setTotalPages($totalPages)
    {
        $this->totalPages = $totalPages;
        return $this;
    }

    /**
     * @param int $numPages
     */
    public function setNumPages($numPages)
    {
        $this->numPages = $numPages;
        return $this;
    }

    /**
     * @param int $currentPage
     */
    public function setCurrentPage($currentPage)
    {
        $this->currentPage = $currentPage;
        return $this;
    }

    /**
     * @param int $totalRow
     */
    public function setTotalRow($totalRow)
    {
        $this->totalRow = $totalRow;
        return $this;
    }

    /**
     * @param $content
     */
    public function setOutputContent($content) {
        $this->outputContent = $content;
        return $this;
    }

    public function formatResponse() {
        $output = array(
            'totalResult'   => 0,
            'currentPage'   => 0,
            'numPages'      => 0,
            'totalPages'    => 0,
            'data'          => array()
        );

        if($this->totalRow == 0 || empty($this->totalRow)) {
            $this->totalRow = count($this->outputContent);
        }

        if($this->totalRow == 0 || empty($this->totalRow)) {
            throw new Exception('Not found', 404);
        }

        $output['totalResult'] = $this->totalRow;
        $output['data'] = $this->outputContent;

        return $output;
    }
}