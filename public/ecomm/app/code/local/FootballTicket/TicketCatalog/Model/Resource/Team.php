<?php
class FootballTicket_TicketCatalog_Model_Resource_Team extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function _construct()
    {
        $this->_init('TicketCatalog/team', 'entity_id');
    }
}