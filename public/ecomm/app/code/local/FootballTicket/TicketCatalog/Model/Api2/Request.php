<?php

/**
 * Class FootballTicket_TicketCatalog_Model_Api2_Request
 */
class FootballTicket_TicketCatalog_Model_Api2_Request extends  Mage_Api2_Model_Request {

    /**
     * Retrieve accept types understandable by requester in a form of array sorted by quality descending
     *
     * @return array
     */
    public function getAcceptTypes()
    {
        $qualityToTypes = array();
        $orderedTypes   = array();

        foreach (preg_split('/,\s*/', $this->getHeader('Accept')) as $definition) {
            $typeWithQ = explode(';', $definition);
            $mimeType  = trim(array_shift($typeWithQ));

            // check MIME type validity
            if (!preg_match('~^([0-9a-z*+\-]+)(?:/([0-9a-z*+\-\.]+))?$~i', $mimeType)) {
                continue;
            }
            $quality = '1.0'; // default value for quality

            if ($typeWithQ) {
                $qAndValue = explode('=', $typeWithQ[0]);

                if (2 == count($qAndValue)) {
                    $quality = $qAndValue[1];
                }
            }
            $qualityToTypes[$quality][$mimeType] = true;
        }
        krsort($qualityToTypes);

        foreach ($qualityToTypes as $typeList) {
            $orderedTypes += $typeList;
        }

        /*
         * Stackoverflow fixes:
         * http://stackoverflow.com/questions/14066963/how-to-get-response-of-rest-api-in-json-format-by-default-in-magento
         */
        unset($orderedTypes);
        $orderedTypes=Array
        ("application/json" => 1);

        return array_keys($orderedTypes);
    }

}