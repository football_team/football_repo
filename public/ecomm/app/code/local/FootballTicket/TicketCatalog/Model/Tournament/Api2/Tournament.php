<?php

/**
 * Class FootballTicket_TicketCatalog_Model_Catalog_Api2_Category
 */
class FootballTicket_TicketCatalog_Model_Tournament_Api2_Tournament extends FootballTicket_TicketCatalog_Model_Api2_Resource
{
    /**
     * @return array
     */
    protected function _retrieve()
    {
        //todo
    }

    /**
     * @return string|void
     */
    protected function _create()
    {
        //todo
    }

    /**
     * @return array
     */
    protected function _retrieveCollection()
    {
        try{
            $output = [];
            /* @var FootballTicket_TicketCatalog_Model_Resource_Tournament_Collection $tournaments */
            $tournaments = Mage::getModel('TicketCatalog/tournament')
                ->getCollection();
            $tournaments = $this->_applyFilterOnCollection( $tournaments );
            $tournamentsArray = $tournaments->load()->toArray();
            $temp =  isset($tournamentsArray['items'])?  $tournamentsArray['items'] : array();

            //load tournament category
            $tournament = Mage::getModel('catalog/category')->load($this->getRequest()->getParam('tournament_id'));
            $output['tournament_id'] = $tournament->getId();
            $output['name'] = $tournament->getName();
            $preOutput = [];

            foreach($temp as $item) {
                if(isset($preOutput[$item['season_id']])) {
                    $preOutput[$item['season_id']]['items'][] = $item;
                } else {
                    $season= Mage::getModel('catalog/category')->load($item['season_id']);
                    $preOutput[$item['season_id']]['items'][] = $item;
                    $preOutput[$item['season_id']]['name'] = $season->getName();
                }
            }

            $output['teams'] = $preOutput;

            $this->_renderOutPut($output);

            $_product = Mage::getModel('catalog/product')->load(5);
            $_product->setWebsiteIds(array('1'));

            print_r($_product->debug());
            Mage::app()->setCurrentStore(Mage::getModel('core/store')->load(Mage_Core_Model_App::ADMIN_STORE_ID));
            $_product->save();
            die();

        } catch (Exception $e) {
            $this->_critical($e->getMessage(), 500);
        }
    }

    /**
     * Product update only available for admin
     *
     * @param array $data
     */
    protected function _update(array $data)
    {
        $this->_critical(self::RESOURCE_METHOD_NOT_ALLOWED);
    }

    /**
     * Product delete only available for admin
     */
    protected function _delete()
    {
        $this->_critical(self::RESOURCE_METHOD_NOT_ALLOWED);
    }


    /**
     * @param FootballTicket_TicketCatalog_Model_Resource_Tournament_Collection $collection
     * @return FootballTicket_TicketCatalog_Model_Resource_Tournament_Collection
     */

    private function _applyFilterOnCollection(FootballTicket_TicketCatalog_Model_Resource_Tournament_Collection $collection) {
        $params = $this->getRequest()->getParams();
        $collection->addFieldToFilter('tournament_id', $params['tournament_id']);
        $collection->addFieldToFilter('season_id', $params['season_id']);
        return $collection;
    }

    protected function _renderOutPut($data) {
        $output = $data;
        if(is_array($data)) {
            $output = json_encode($data);
        }

        header('Content-Type: application/json');
       // $this->_render($data);
        echo $output;
    }

}