<?php

class FootballTicket_TicketCatalog_Model_Catalog_Product_Api extends  Mage_Catalog_Model_Product_Api {

    public function items($filters = null, $store = null)
    {
        $collection = Mage::getModel('catalog/product')->getCollection()
            ->addStoreFilter($this->_getStoreId($store))
            ->addAttributeToSelect('name')
            ->addAttributeToSelect('price');


        /** @var $apiHelper Mage_Api_Helper_Data */
        $apiHelper = Mage::helper('api');
        $filters   = $apiHelper->parseFilters($filters, $this->_filtersMap);
        try {
            foreach ($filters as $field => $value) {
                $collection->addFieldToFilter($field, $value);
            }
        } catch (Mage_Core_Exception $e) {
            $this->_fault('filters_invalid', $e->getMessage());
        }
        $result = array();
        foreach ($collection as $product) {
            $result[] = array(
                'product_id'   => $product->getId(),
                'sku'          => $product->getSku(),
                'name'         => $product->getName(),
                'set'          => $product->getAttributeSetId(),
                'type'         => $product->getTypeId(),
                'category_ids' => $product->getCategoryIds(),
                'website_ids'  => $product->getWebsiteIds(),
                'price'        => $product->getPrice()
            );
        }
        return $result;
    }

}