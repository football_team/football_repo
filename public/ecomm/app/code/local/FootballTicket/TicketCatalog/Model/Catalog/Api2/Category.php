<?php

/**
 * Class FootballTicket_TicketCatalog_Model_Catalog_Api2_Category
 */
class FootballTicket_TicketCatalog_Model_Catalog_Api2_Category extends FootballTicket_TicketCatalog_Model_Api2_Resource
{
    /**
     * @return array
     */
    protected function _retrieve()
    {
        $id = $this->getRequest()->getParam('id');

        if(empty($id)) {
            $this->_critical(self::RESOURCE_REQUEST_DATA_INVALID);
        }
        $category = Mage::getModel('catalog/category')->load($id);
        $output = $category->toArray();
        if(!isset($output['entity_id'])) {
            $this->_critical(self::RESOURCE_NOT_FOUND);
        } else {
            return $output;
        }
    }

    /**
     * @return string|void
     */
    protected function _create()
    {
        $this->_critical(self::RESOURCE_METHOD_NOT_ALLOWED);
    }

    /**
     * @return array
     */
    protected function _retrieveCollection()
    {
        $categories = Mage::getModel('catalog/category')
            ->getCollection()
            ->addAttributeToSelect(array('entity_id', 'name'))
            ->addIsActiveFilter();

        $output = $this->_applyFilterOnCollection($categories)
                       ->load()
                       ->toArray();

        if(empty($output)) {
            $this->_critical(self::RESOURCE_NOT_FOUND);
            return;
        }

        return array_values($output);
    }

    /**
     * Product update only available for admin
     *
     * @param array $data
     */
    protected function _update(array $data)
    {
        $this->_critical(self::RESOURCE_METHOD_NOT_ALLOWED);
    }

    /**
     * Product delete only available for admin
     */
    protected function _delete()
    {
        $this->_critical(self::RESOURCE_METHOD_NOT_ALLOWED);
    }


    /**
     * @param Mage_Catalog_Model_Resource_Category_Collection $categories
     * @return Mage_Catalog_Model_Resource_Category_Collection
     */
    private function _applyFilterOnCollection(Mage_Catalog_Model_Resource_Category_Collection $categories) {
        $params = $this->getRequest()->getParams();
        // by category id
        if( isset($params['id'])) {
            $categories->addFieldToFilter("parent_id",array("eq"=>$params['id']));
        }

        //sort category by name
        $defaultOrderBy = "name";
        $defaultOrderDir = "ASC";

        if(isset($params['order_by']) && !empty($params['order_by'])) {
            $defaultOrderBy = $params['sortby'];
        }
        if(isset($params['direction']) && !empty($params['direction'])) {
            $defaultOrderDir = strtoupper($params['direction']);
        }

        $categories->addOrder($defaultOrderBy,$defaultOrderDir);

        if(isset($params['q']) && !empty($params['q'])) {
            $categories->addAttributeToFilter('name', array("like" => "%{$params['q']}%"));
        }

        return $categories;
    }

}