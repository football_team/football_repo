<?php
class FootballTicket_TicketCatalog_Model_Resource_Tournament_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    public function _construct()
    {
        $this->_init('TicketCatalog/tournament');
    }
}