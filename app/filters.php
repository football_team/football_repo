<?php


/*
|--------------------------------------------------------------------------
| Application & Route Filters
|--------------------------------------------------------------------------
|
| Below you will find the "before" and "after" events for the application
| which may be used to do any work before or after a request into your
| application. Here you may also register your custom route filters.
|
*/
/*
App::before(function ($request) {
    if (!Request::secure() && !App::environment('local', 'stage')) {
        return Redirect::secure(Request::path(), 301);
    }
});
App::before(function ($request) {
    if (!App::environment('local', 'stage')) {
        //Add the 'www.' to all requests
        if (substr($request->header('host'), 0, 4) != 'www.') {
            $request->headers->set('host', 'www.' . $request->header('host'));
            return Redirect::to($request->path(), 301);
        }
    }

});
*/
App::before(function ($request) {
    $path = $request->path();
    if(Cache::has('req_'.$path))
    {
        $req = Cache::get('req_'.$path);
    }
    else
    {
        $rdr = \DB::table('rdr')
                    ->where('from', $path)
                    ->first();

        if($rdr)
        {
            $req = ['rdr'=>true, 'to'=>$rdr->to, 'from'=>$rdr->from, 'code'=>$rdr->code];
            Cache::put('req_'.$path, $req, 60);
        }
        else
        {
            $req = ['rdr'=>false];
            Cache::put('req_'.$path, $req, 60);
        }
    }
    if($req['rdr'])
    {
        return Redirect::to($req['to'], $req['code']);
    }
});

App::before(function ($request) {
    if (!Session::has('locale_set')) {
        $lang = DB::table('locales')
            ->where('url', '=', $request->header('host'))
            ->first();
        if (!$lang) {
            Session::put('locale_set', 'en_gb');
        } else {
            Session::put('locale_set', $lang->lang);
        }
    }
    App::setLocale(Session::get('locale_set'));
});

App::before(function ($request) {
    $firstCookie = Cookie::get('visited', 'unknown');
    if ($firstCookie == "unknown") {
        Session::put('showBanner', 0);
        \Cookie::queue(\Cookie::forever('locla_2312', 1));
    } else {
        Session::put('showBanner', 0);
    }
});

/*
 * Lets PAss some menu information over
 */

App::before(function ($request) {
    /**
     * hotTicketsMobileMenu
     */
    $cachName = "hotTicketsMobileMenu";
    if (Cache::has($cachName)) {
        $hotTicketsCache = Cache::get($cachName);
        View::share("hotTicketsMobileMenu", $hotTicketsCache);
    }else{
    $hotTickets = FootBallEvent::where('widget_display', '=', '1')
        ->orderBy('widget_order', 'ASC')
        ->whereRaw('datetime >= NOW()')
        ->take(10)
        ->get();
        Cache::add($cachName, $hotTickets, 3600);
        View::share("hotTicketsMobileMenu", $hotTickets);
    }


    /**
     * featureClubs
     */
    $cachName = "hotTeamsMobileMenux";
    if (!Cache::has($cachName)) {
    $featureClub = FootballTickets::where('type', '=', 'club')
        ->leftJoin('football_ticket_meta', function ($join) {
            $join->on('football_ticket.id', '=', 'football_ticket_meta.football_ticket_id')
                ->where('football_ticket_meta.key','=', 'club_logo');
        })->where('widget', '=', '1')
        ->orderBy('widget_order', 'ASC')
        ->groupBy('football_ticket.id')
        ->take(10)
        ->get();
        Cache::add($cachName, $featureClub, 3600);
        View::share("featureClubMenu", $featureClub);
    }else{
        $featureClub = Cache::get($cachName);
        View::share("featureClubMenu", $featureClub);
    }


    /**
     * Featured tornements
     */
    $cachName = "featureTournaments";
    if (!Cache::has($cachName)) {
    $featureTournaments = FootballTickets::where('type', '=', 'league')
        ->leftJoin('football_ticket_meta', function ($join) {
            $join->on('football_ticket.id', '=', 'football_ticket_meta.football_ticket_id')
                ->where('football_ticket_meta.key','=', 'club_logo');
        })->where('widget', '=', '1')
        ->orderBy('widget_order', 'ASC')
        ->groupBy('football_ticket.id')
        ->take(10)
        ->get();
        Cache::add($cachName, $featureTournaments, 3600);
        View::share("featureTournaments", $featureTournaments);
    }else{
        $featureTournaments = Cache::get($cachName);
        View::share("featureTournaments", $featureTournaments);
    }


});

App::before(function($request){
    if(!Session::has('referer_url'))
    {
        $referer = Request::server('HTTP_REFERER');
        if($referer == '')
        {
            $referer = 'direct';
        }

        Session::put('referer_url', $referer);
    }
});




App::before(function ($request) {

    $cookieName = "set_visit_locale";
    View::share('locale_logo', 'ticketpad-on-black-small.png');
    if (!isset($_COOKIE['setshow'])) {
        setcookie('setshow', '1');
        if (!empty($set_counter_visit) && $set_counter_visit == "block") {
            /** GET USERS IP */
            $set_counter_visit = "none";
            View::share("set_counter_visit", $set_counter_visit);

        } else if (!empty($set_counter_visit) && $set_counter_visit == "none") {

            /** EMPTY CONDITION TO STOP ELSE CONDITION OCCURING FALSLY */

        } else {

            /** GET USERS IP */
            if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
                $ip = $_SERVER['HTTP_CLIENT_IP'];
            } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
                $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            } else {
                $ip = $_SERVER['REMOTE_ADDR'];
            }

            /** CONVERT IP TO ADDRESS */
            if (!empty($ip)) {
                $details = json_decode(file_get_contents("http://freegeoip.net/json/{$ip}"));


                /** SET ADDRESS TO COOKIE */
                if (!empty($details->country)) {
                    View::share($cookieName, $details->country);
                }

                /** LETS CHECK THE SITE THERE ON */
                $domain = $_SERVER['HTTP_HOST'];


                $arrayOfLocals = [
                    ['url' => 'www.fbtp.com',
                        "flag" => "United_Kingdom.jpg",
                        "text" => trans('homepage.viewThisPage')." United_Kingdom ".trans('homepage.site'),
                        "woops" => "Woop's You might not be on the best version of the site",
                        "view" => "View this page on our English site",
                        "local" => "GBx",
                        "logo" => 'ticketpad-on-black-small.png'
                    ],
                    ['url' => 'staging.footballticketpad.com',
                        "flag" => "United_Kingdom.jpg",
                        "text" => trans('homepage.viewThisPage')." United_Kingdom ".trans('homepage.site'),
                        "woops" => "Woop's You might not be on the best version of the site",
                        "view" => "View this page on our English site",
                        "local" => "GB",
                        "logo" => 'ticketpad-on-black-small.png'
                    ],
                    ['url' => 'www.ticketpad.net',
                        "flag" => "United_States.jpg",
                        "text" => trans('homepage.viewThisPage')." United_States ".trans('homepage.site'),
                        "woops" => "Woop's You might not be on the best version of the site",
                        "view" => "Continue on Ticket Pad US ",
                        "local" => "US",
                        "logo" => 'ticketpad-on-black-small.png'
                    ],
                    ['url' => 'www.ticketpad.fr',
                        "flag" => "France.jpg",
                        "text" => trans('homepage.viewThisPage')." France ".trans('homepage.site'),  "woops" => "Woop's You might not be on the best version of the site",
                        "woops" => "Oups, vous n'êtes peut-être pas sur la meilleure version du site",
                        "view" => "Rester sur le site Ticket Pad Français",
                        "local" => "FR",
                        "logo" => 'ticketpad-on-black-small.png'
                    ],
                    ['url' => 'www.ticketpad.es',
                        "flag" => "Spain.jpg",
                        "text" => trans('homepage.viewThisPage')." Spain ".trans('homepage.site'),
                        "woops" => "Whoops, No podría estar en la mejor versión del sitio",
                        "view" => "Continuar en Ticket Pad España",
                        "local" => "ES",
                        "logo" => 'ticketpad-on-black-small.png'
                    ],
                    ['url' => 'www.ticketpad.ru',
                        "flag" => "Russia.jpg",
                        "text" => trans('homepage.viewThisPage')." Russia ".trans('homepage.site'),
                        "woops" => "Ой, скорее всего Вы не на той версии сайта",
                        "view" => 'Продолжить Ticketpad Россия',
                        "local" => "RU",
                        "logo" => 'ticketpad-on-black-small.png'
                    ],
                    ['url' => 'www.ticketpad.com.hk',
                        "flag" => "Hong_Kong.jpg",
                        "text" => trans('homepage.viewThisPage')." Hong_Kong ".trans('homepage.site'),
                        "woops" => "Woop's You might not be on the best version of the site",
                        "view" => "Continue on Ticket Pad Hong Kong",
                        "local" => "HK",
                        "logo" => 'ticketpad-on-black-small.png'
                    ],
                    ['url' => 'www.ticketpad.co.za',
                        "flag" => "South_Africa.jpg",
                        "text" => trans('homepage.viewThisPage')." South_Africa ".trans('homepage.site'),
                        "woops" => "Woop's You might not be on the best version of the site",
                        "view" => "Continue on Ticket Pad South Africa ",
                        "local" => "ZA",
                        "logo" => 'ticketpad-on-black-small.png'
                    ],
                    ['url' => 'www.ticketpad.co.nz',
                        "flag" => "New_Zealand.jpg",
                        "text" => trans('homepage.viewThisPage')." New_Zealand ".trans('homepage.site'),
                        "woops" => "Woop's You might not be on the best version of the site",
                        "view" => "Continue on Ticket Pad New Zealand ",
                        "local" => "NZ",
                        "logo" => 'ticketpad-on-black-small.png'
                    ],
                    ['url' => 'www.ticketpad.be',
                        "flag" => "Belgium.jpg",
                        "text" => trans('homepage.viewThisPage')." Belgium ".trans('homepage.site'),
                        "woops" => "Oups, vous n'êtes peut-être pas sur la meilleure version du site",
                        "view" => "Rester sur le site Ticket Pad Belgique",
                        "local" => "BE",
                        "logo" => 'ticketpad-on-black-small.png'
                    ],
                    ['url' => 'www.ticketpad.by',
                        "flag" => "Belarus.jpg",
                        "text" => trans('homepage.viewThisPage')." Belarus ".trans('homepage.site'),
                        "woops" => "Ой, скорее всего Вы не на лучшей версии сайта",
                        "view" => "Продолжить Ticketpad Беларусь",
                        "local" => "BY",
                        "logo" => 'ticketpad-on-black-small.png'
                    ],

                    ['url' => 'www.ticketpad.com.ua',
                        "flag" => "Ukraine.jpg",
                        "text" => trans('homepage.viewThisPage')." Ukraine ".trans('homepage.site'),
                        "woops" => "Ой, скорее всего Вы не на лучшей версии сайта",
                        "view" => "Продолжить Ticketpad Украина ",
                        "local" => "UA",
                        "logo" => 'ticketpad-on-black-small.png'
                    ],
                    ['url' => 'www.ticketpad.ec',
                        "flag" => "Ecuador.jpg",
                        "text" => trans('homepage.viewThisPage')." Ecuador ".trans('homepage.site'),
                        "woops" => "Continuar en Ticket Pad Ecuador",
                        "view" => "Continuar en Ticket Pad Ecuador",
                        "local" => "EC",
                        "logo" => 'ticketpad-on-black-small.png'
                    ],
                    ['url' => 'www.ticketpad.cl',
                        "flag" => "Chile.jpg",
                        "text" => trans('homepage.viewThisPage')." Chile ".trans('homepage.site'),
                        "woops" => "Whoops, No podría estar en la mejor versión del sitio",
                        "view" => "Continuar en Ticket Pad Chile",
                        "local" => "CL",
                        "logo" => 'ticketpad-on-black-small.png'
                    ],
                    ['url' => 'www.ticketpad.com.ar',
                        "flag" => "Argentina.jpg",
                        "text" => trans('homepage.viewThisPage')." Argentina ".trans('homepage.site'),
                        "woops" => "Whoops, No podría estar en la mejor versión del sitio",
                        "view" => "Continuar en Ticket Pad Argentina ",
                        "local" => "AR",
                        "logo" => 'ticketpad-on-black-small.png'
                    ],
                    ['url' => 'www.ticketpad.co.ve',
                        "flag" => "Venezuela.jpg",
                        "text" => trans('homepage.viewThisPage')." Venezuela ".trans('homepage.site'),
                        "woops" => "Whoops, No podría estar en la mejor versión del sitio",
                        "view" => "Continuar en Ticket Pad Venezuela",
                        "local" => "VE",
                        "logo" => 'ticketpad-on-black-small.png'
                    ],
                    ['url' => 'www.ticketpad.pe',
                        "flag" => "Peru.jpg",
                        "text" => trans('homepage.viewThisPage')." Peru ".trans('homepage.site'),
                        "woops" => "Whoops, No podría estar en la mejor versión del sitio",
                        "view" => "Continuar en Ticket Pad Perú",
                        "local" => "PE",
                        "logo" => 'ticketpad-on-black-small.png'
                    ],
                    ['url' => 'www.ticketpad.mx',
                        "flag" => "Mexico.jpg",
                        "text" => trans('homepage.viewThisPage')." Mexico ".trans('homepage.site'),
                        "woops" => "Whoops, No podría estar en la mejor versión del sitio",
                        "view" => "Continuar en Ticket Pad México ",
                        "local" => "MX",
                        "logo" => 'ticketpad-on-black-small.png'
                    ],
                    ['url' => 'www.ticketpad.co',
                        "flag" => "Colombia.jpg",
                        "text" => trans('homepage.viewThisPage')." Colombia ".trans('homepage.site'),
                        "woops" => "Whoops, No podría estar en la mejor versión del sitio",
                        "view" => "Continuar en Ticket Pad Colombia",
                        "local" => "ZA",
                        "logo" => 'ticketpad-on-black-small.png'
                    ],
                    ['url' => 'www.ticketpad.ca',
                        "flag" => "Canada.jpg",
                        "text" => trans('homepage.viewThisPage')." Canada ".trans('homepage.site'),
                        "woops" => "Woop's You might not be on the best version of the site",
                        "view" => "Continue on Ticket Pad Canada",
                        "local" => "CO",
                        "logo" => 'ticketpad-on-black-small.png'
                    ],
                    ['url' => 'www.ticketpad.lu',
                        "flag" => "Luxembourg.jpg",
                        "text" => trans('homepage.viewThisPage')." Luxembourg ".trans('homepage.site'),
                        "woops" => "Oups, vous n'êtes peut-être pas sur la meilleure version du site",
                        "view" => "Rester sur le site Ticket Pad Luxembourg",
                        "local" => "LU",
                        "logo" => 'ticketpad-on-black-small.png'
                    ],
                    ['url' => 'www.ticketpad.ch',
                        "flag" => "Switzerland.jpg",
                        "text" => trans('homepage.viewThisPage')." Switzerland ".trans('homepage.site'),
                        "woops" => "Oups, vous n'êtes peut-être pas sur la meilleure version du site",
                        "view" => "Rester sur le site Ticket Pad Suisse",
                        "local" => "CH",
                        "logo" => 'ticketpad-on-black-small.png'
                    ],
                    ['url' => 'www.ticketpad.it',
                        "flag" => "Italy.jpg",
                        "text" => trans('homepage.viewThisPage')." Italia ".trans('homepage.site'),
                        "woops" => "",
                        "view" => "",
                        "local" => "IT",
                        "logo" => 'ticketpad-on-black-small.png'
                    ],
                ];
                //$details->country != $x['local']
                //$domain == $x['url']
                setcookie('domain',$domain);
                setcookie('ccode',$details->country_code);
                foreach ($arrayOfLocals as $x) {

                    setcookie('1',1);
                    if (!empty($details->country_code)) {
                        setcookie('2',2);
                        if ( $details->country_code == $x['local'] && $domain != $x['url']) {
                            setcookie('3',3);
                            View::share('locale_url', $x['url']);
                            View::share('locale_flag', $x['flag']);
                            View::share('locale_text', $x['text']);
                            View::share('locale_woops', $x['woops']);
                            View::share('locale_view', $x['view']);
                            View::share('locale_logo', $x['logo']);

                        }
                    }
                }


            }
            View::share("set_counter_visit", "block");

        }
    }
});

/*

App::after(function ($request, $response) {
    //
});*/

/*
|--------------------------------------------------------------------------
| Authentication Filters
|--------------------------------------------------------------------------
|
| The following filters are used to verify that the user of the current
| session is logged into this application. The "basic" filter easily
| integrates HTTP Basic authentication for quick, simple checking.
|
*/

/*
Route::filter('auth', function () {

    if (Auth::guest()) return Redirect::guest('login');
});
*/

// Sentry User Auth Filter
Route::filter('auth.admin', function () {
    if (!Sentry::check()) {
        return Redirect::route('admin.login');
    } else {

        $user = Sentry::getUser();
        $user->getGroups();

        $flag = 0;
        foreach ($user->groups as $group) {
            if ($group->name == 'Admin') {
                $flag = 1;
                break;
            }
        }
        if (!$flag) {
            return Redirect::route('admin.login');
        }
    }
});

Route::filter('auth.basic', function () {
    return Auth::basic();
});

Route::filter('assets_admin', function () {
    Assets::setStyles([
        'bootstrap' => "css/bootstrap.min.css",
        'font-awesome' => "css/font-awesome.min.css",
        'smartadmin-production' => "css/smartadmin-production.min.css",
        'smartadmin-skins' => "css/smartadmin-skins.min.css",
        'demo' => "css/demo.min.css",
        'bond' => "css/bond-admin-style.css"

    ], true);

    Assets::setScripts([
        'pace' => "js/plugin/pace/pace.min.js",
        'jquery' => "js/libs/jquery-2.0.2.min.js",
        'jquery-ui' => "js/libs/jquery-ui-1.10.3.min.js",
        'bootstrap' => "js/bootstrap/bootstrap.min.js",
        'SmartNotification' => "js/notification/SmartNotification.min.js",
        'jarvis.widget' => "js/smartwidgets/jarvis.widget.min.js",
        'easy-pie-chart' => "js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js",
        'sparkline' => "js/plugin/sparkline/jquery.sparkline.min.js",
        'validate' => "js/plugin/jquery-validate/jquery.validate.min.js",
        'maskedinput' => "js/plugin/masked-input/jquery.maskedinput.min.js",
        'select2' => "js/plugin/select2/select2.min.js",
        'bootstrap-slider' => "js/plugin/bootstrap-slider/bootstrap-slider.min.js",
        'mb.browser' => "js/plugin/msie-fix/jquery.mb.browser.min.js",
        'fastclick' => "js/plugin/fastclick/fastclick.min.js",
        'demo' => "js/demo.min.js",
        'app' => "js/app.min.js"
    ], true);
});

/*
|--------------------------------------------------------------------------
| Guest Filter
|--------------------------------------------------------------------------
|
| The "guest" filter is the counterpart of the authentication filters as
| it simply checks that the current user is not logged in. A redirect
| response will be issued if they are, which you may freely change.
|
*/

Route::filter('guest', function () {

    if (Auth::check()) return Redirect::to('/');
});

/*
|--------------------------------------------------------------------------
| CSRF Protection Filter
|--------------------------------------------------------------------------
|
| The CSRF filter is responsible for protecting your application against
| cross-site request forgery attacks. If this special token in a user
| session does not match the one given in this request, we'll bail.
|
*/
Route::filter('csrf', function () {
    if (Request::ajax()) {
        if (Session::token() !== Request::header('csrftoken')) {
            // Change this to return something your JavaScript can read...
            throw new Illuminate\Session\TokenMismatchException;
        }
    } elseif (Session::token() !== Input::get('_token')) {
        throw new Illuminate\Session\TokenMismatchException;
    }
});

/*
|--------------------------------------------------------------------------
| Route Cache Filter
|--------------------------------------------------------------------------
*/
Route::filter('cache.fetch', 'Bond\Filters\CacheFilter@fetch');
Route::filter('cache.put', 'Bond\Filters\CacheFilter@put');


/*
|--------------------------------------------------------------------------
| Football ticket customer account filter
|--------------------------------------------------------------------------
 */

Route::filter('customer.account', function () {
    $api_path = Config::get('api.mage_soap_api_path');
    require_once("{$api_path}app/Mage.php");
    umask(0);
    Mage::app('default');

    Mage::getSingleton('core/session', array('name' => 'frontend'));
    $session = Mage::getSingleton('customer/session', array('name' => 'frontend'));

    if (!$session->isLoggedIn()) {
        Session::put('customer', null);
        return Redirect::to('/login');
    } else {
        $customer = Mage::getSingleton('customer/session')->getCustomer()->getData();
        $SesCustomer = Session::get('customer');

        if ($customer['entity_id'] != $SesCustomer['entity_id']) {
            Session::put('customer', null);
            return Redirect::to('/');
        }
    }
});

/*
|--------------------------------------------------------------------------
| Affiliate Reference Capture Filter
|--------------------------------------------------------------------------
 */

Route::filter('assoc.affil', function () {
    if (Input::has('refCode')) {
        if (!Session::has('refCode')) {
            $code = Input::get('refCode');
            $af = AffiliateProperty::where('ref_code', $code)->first();
            if ($af) {
                Session::put('refCode', $code);

                $rid = null;

                if(Input::has('sid'))
                {
                    $rid = Input::get('sid');

                    if(strlen($rid) < 25)
                    {
                        Session::put('rid', $rid);
                    }
                }

                $dt = new DateTime();
                $path = Request::url();
                DB::table('referral_hits')
                    ->insert([
                        'created_at'=>$dt->format('Y-m-d H:i:s'),
                        'affiliate_id'=>$af->id,
                        'url'=>$path,
                        'rid'=>$rid
                    ]);
            }
        }
    }
});
