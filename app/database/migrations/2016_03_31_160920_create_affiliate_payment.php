<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAffiliatePayment extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('affiliate_payment', function (Blueprint $table){
			$table->increments('id');
			$table->integer('affiliate_id');
			$table->integer('withdrawal_request_id');
			$table->integer('sort_code');
			$table->integer('account_number');
			$table->string('IBAN');
			$table->datetime('created_at');
			$table->datetime('updated_at');
			$table->decimal('amount',8,2);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
