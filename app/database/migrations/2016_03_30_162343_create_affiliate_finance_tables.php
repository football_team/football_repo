<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAffiliateFinanceTables extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('affiliate_balance', function (Blueprint $table){
			$table->increments('id');
			$table->integer('affiliate_id');
			$table->double('balance', 7,2);
		});

		Schema::create('affiliate_withdrawal_requests', function (Blueprint $table){
			$table->increments('id');
			$table->integer('affiliate_id');
			$table->double('amount', 7,2);
			$table->date('requested_for');
			$table->dateTime('created_at');
			$table->dateTime('updated_at');
			$table->enum('status', array('pending', 'cancelled', 'paid'))->default('pending');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
