<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAffiliateRates extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//So I need
		//Rate id
		//Number of sales req for rate
		//Rate %
		Schema::create('affiliate_payment_rate', function (Blueprint $table){
			$table->increments('id');
			$table->integer('rate_id');
			$table->integer('sale_milestone');
			$table->integer('rate_perc');
			$table->string('rate_time_period');
			$table->string('rate_name');
		});

		Schema::create('affiliate_rate', function (Blueprint $table){
			$table->increments('id');
			$table->integer('affiliate_id');
			$table->integer('rate_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
