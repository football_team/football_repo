<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAffiliateTables extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		
		Schema::create('affiliate_properties', function (Blueprint $table) {
			$table->increments('id');
			$table->string('company_name');
			$table->string('company_url');
			$table->string('contact_name');
			$table->string('ver_code');
			$table->string('ref_code');
			$table->string('user_id');
			$table->string('sort_code', 6)->default("");
			$table->string('account_number', 8)->default("");
			$table->string('IBAN', 34)->default("");
			$table->string('account_holder_name')->default("");
		});

		Schema::create('affiliate_sales', function (Blueprint $table){
			$table->increments('id');
			$table->integer('order_id');
			$table->integer('affiliate_id');
			$table->integer('commission_perc');
		});

		Schema::create('affiliate_requests', function (Blueprint $table){
			$table->increments('id');
			$table->string('email');
			$table->string('company_url');
			$table->string('ver_code');
			$table->string('contact_name');
			$table->boolean('is_approved')->default(0);
			$table->integer('approver_id');
			$table->string('company_name');
			$table->boolean('is_rejected')->default(0);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
