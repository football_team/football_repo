<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticleFbtAssocTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('article_fbt_assoc', function (Blueprint $table){
			$table->increments('id');
			$table->integer('articles_id');
			$table->integer('fbt_id');
			$table->enum('type', array('club', 'league'));
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
