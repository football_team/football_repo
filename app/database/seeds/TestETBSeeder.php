<?php

use Faker\Factory as Faker;

class TestETBSeeder extends Seeder {

    public function run() {
        $faker = Faker::create();

        DB::table('events_ticket_buy_test')->truncate();
        DB::table('affiliate_sales')->truncate();

        foreach(range(1, 500) as $index) {
            $buyer = rand(8,10);
            $event_id = rand(77, 124);
            $product_id = rand(27, 32);
            $qty = rand(1,4);
            $amount = $faker->numberBetween(10, 400);
            $perc = rand(10,20);

            $cu = $faker->dateTimeThisYear('now');


            DB::table('events_ticket_buy_test')
                ->insert([
                    'order_id' => $index,
                    'buyer_id' => $buyer,
                    'event_id' => $event_id,
                    'product_id' => $product_id,
                    'qty' => $qty,
                    'amount' => $amount,
                    'listed_amount' => $amount,
                    'commission' => 10,
                    'commission_amount' => 0,
                    'fees_amount' => 0,
                    'delivery_status' => $faker->randomElement(['pending','in-transit','delivered','cancelled']),
                    'payment_status' => 'paid',
                    'updated_at' => $cu,
                    'created_at' => $cu,
                    'seller_amount_paid' => $amount,
                    'seller_payment_status' => 'paid',
                    'tracked' => 1
                ]);

            DB::table('affiliate_sales')
                ->insert([
                   'order_id' => $index,
                    'affiliate_id' => 1,
                    'commission_perc' => $perc
                ]);
        }
    }
}