<?php
/**
 * Created by PhpStorm.
 * User: baglad
 * Date: 12/04/2016
 * Time: 12:55
 */
class AffiliateConfigSeeder extends Seeder {

    /**
     * Run the database seeding.
     *
     * @return void
     */
    public function run() {

        DB::table('affiliate_config')->truncate();

        DB::table('affiliate_config')
            ->insert([
                'key'=>'min_withdrawal',
                'value'=>'1000'
            ]);

        DB::table('affiliate_config')
            ->insert([
                'key'=>'payout_day',
                'value' => '5'
            ]);

        DB::table('affiliate_config')
            ->insert([
                'key' => 'default_rate',
                'value' => '1'
            ]);
    }
}

