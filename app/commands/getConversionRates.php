<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class getConversionRates extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'getConversion:rates';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'get conversion rates from api';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$json = "http://apilayer.net/api/live?access_key=121ac40727aec5a5b518dc873d32ef1f&source=USD&format=1";
		$jsonfile = file_get_contents($json);
		$rates = json_decode($jsonfile);
		$gbp = $rates->quotes->USDGBP;

		$multi = 1/$gbp;
		$newList = array();

		foreach($rates->quotes as $quoteName=>$quote)
		{
			$quote = $quote*$multi;
			$quote = number_format($quote, 3, '.', '');
			$curName = substr($quoteName, 3);
			$newList[$curName] = $quote;
		}
		$newList['GBP'] = 1;

		$allUsed = DB::table('locales')
					->select('currency')
					->distinct()
					->get();

		Log::info("Currency Conversion CRON");
		foreach($allUsed as $aCur)
		{
			$rate = $newList[$aCur->currency];
			if($rate)
			{	
				DB::table('locales')
						->where('currency', $aCur->currency)
						->update(['conversion' => $rate]);

				Log::info('Set '.$aCur->currency.' equals '. $rate .' GBP.');
			}
		}

		return true;
		
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			array('example', InputArgument::OPTIONAL, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}