<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class updateSalePrice extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'updateSale:price';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'update sale price';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
        $orderCollection = FootballTicketBuy::all();
        foreach($orderCollection as $o) {
            $order = TicketSoap::process('sales_order.info', $o->order_id);
            echo $o->order_id.":";
            $o->listed_amount = isset($order['items'][0]['price'])?$order['items'][0]['price'] : $order['base_subtotal']/$order['total_qty_ordered'];
            echo $o->listed_amount."\n";
            $o->save();
        }


	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			array('example', InputArgument::OPTIONAL, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}
