<?php namespace App\Controllers\Admin;

use BaseController;
use Facebook\WebDriver\WebDriverKeys;
use LogEvent;
use Net_SSH2;
use Sentry;
use Redirect;
use Translate;
use View;
use Input;
use Pages;
use Template;
use DB;
use PagesMeta;
use Validator;
use Response;
use Request;
use Notification;
use Bond\Traits\GridPagination;
use Bond\Repositories\Page\PageRepository as Page;
use Bond\Exceptions\Validation\ValidationException;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\WebDriverBy;

class AutomatedController extends BaseController
{


    public function checkAllHomepages($os, $browser)
    {
        $web_driver = RemoteWebDriver::create(
            "https://mark1171:ZpoXXfxqVETXS8wLxguq@hub-cloud.browserstack.com/wd/hub",
            array("platform" => $os, "browserName" => $browser)
        );

        $arrayUrls = [
            "https://www.footballticketpad.com/",
            "https://www.ticketpad.net/",
            "https://www.ticketpad.ru/",
            "https://www.ticketpad.es/",
            "https://www.ticketpad.fr/",
            "https://www.ticketpad.co.nz/",
            "https://www.ticketpad.co.az/",
            "https://www.ticketpad.com.hk/",
            "https://www.ticketpad.ie/",
            "https://www.ticketpad.ch/",
            "https://www.ticketpad.be/",
            "https://www.ticketpad.by/",
            "https://www.ticketpad.com.ua/",
            "https://www.ticketpad.com.ar/",
            "https://www.ticketpad.co.ve/",
            "https://www.ticketpad.pe/",
            "https://www.ticketpad.mx/",
            "https://www.ticketpad.co/",
            "https://www.ticketpad.ca/",
            "https://www.ticketpad.lu/",
            "https://www.ticketpad.pt/"
        ];

        foreach ($arrayUrls as $item) {

            $web_driver->get($item);

            print $web_driver->getTitle();
        }


        $web_driver->quit();

    }


    public function buyTest($os, $browser)
    {
        $web_driver = RemoteWebDriver::create(
            "https://mark1171:ZpoXXfxqVETXS8wLxguq@hub-cloud.browserstack.com/wd/hub",
            array("platform" => $os, "browserName" => $browser)
        );
        $web_driver->get("https://www.footballticketpad.com/checkout/15731");


        $element = $web_driver->findElement(WebDriverBy::name("number_of_ticket"));
        $element->sendKeys("2");

        $element = $web_driver->findElement(WebDriverBy::name("password"));
        $element->sendKeys("123123123");

        $element = $web_driver->findElement(WebDriverBy::name("password_confirmation"));
        $element->sendKeys("123123123");

        $element = $web_driver->findElement(WebDriverBy::name("first_name"));
        $element->sendKeys("Monkey");

        $element = $web_driver->findElement(WebDriverBy::name("last_name"));
        $element->sendKeys("Spanner");


        $element = $web_driver->findElement(WebDriverBy::name("shipping_country"));
        $element->sendKeys("United Kingdom");

        $element = $web_driver->findElement(WebDriverBy::name("other_country"));
        $element->sendKeys("UK - Mon – Fri Special Free");

        $element = $web_driver->findElement(WebDriverBy::name("country_code"));
        $element->sendKeys("0044");
        $element = $web_driver->findElement(WebDriverBy::name("contact_no"));
        $element->sendKeys("007759273459");
        $element = $web_driver->findElement(WebDriverBy::name("street"));
        $element->sendKeys("This road");
        $element = $web_driver->findElement(WebDriverBy::name("postcode"));
        $element->sendKeys("L34EG");

        $element = $web_driver->findElement(WebDriverBy::name("city"));
        $element->sendKeys("Liverpool");

        $randx = rand(100000, 900000);
        $rands = "mark+" . $randx . "@arcade247.co.uk";


        $element = $web_driver->findElement(WebDriverBy::name("email"));
        $element->sendKeys($rands);


        $element = $web_driver->findElement(WebDriverBy::name("email_confirmation"));
        $element->sendKeys($rands);

        $element = $web_driver->findElement(WebDriverBy::name("ship-same"));
        $element->click();
        $element = $web_driver->findElement(WebDriverBy::name("terms_n_conditions"));
        $element->click();
        $element->submit();

        /**
         *
         *  GateWay settings
         *
         */
        $web_driver->manage()->timeouts()->implicitlyWait(20);
        $my_frame = $web_driver->findElement(WebDriverBy::cssSelector('body > div:nth-child(5) > div > iframe'));

        $web_driver->switchTo()->frame($my_frame);


        $element = $web_driver->findElement(WebDriverBy::cssSelector("#address1"));
        $element->sendKeys("61 armstrong quey");

        $element = $web_driver->findElement(WebDriverBy::cssSelector("#city"));
        $element->sendKeys("liverpool");

        /// $element = $web_driver->findElement(WebDriverBy::cssSelector("#dk_container_country"));
        // $element->sendKeys("united kingdom");

        $element = $web_driver->findElement(WebDriverBy::cssSelector("#first_name"));
        $element->sendKeys("mark");

        $element = $web_driver->findElement(WebDriverBy::cssSelector("#last_name"));
        $element->sendKeys("jones");

        $element = $web_driver->findElement(WebDriverBy::cssSelector("#phone1"));
        $element->sendKeys("07759273459");

        $element = $web_driver->findElement(WebDriverBy::cssSelector("#zip"));
        $element->sendKeys("l34eg");


        $element = $web_driver->findElement(WebDriverBy::cssSelector("#cc_name_on_card"));
        $element->sendKeys("stephen michael broad");

        $element = $web_driver->findElement(WebDriverBy::cssSelector("#cc_card_number"));
        $element->sendKeys("5460431001830784");

        $element = $web_driver->findElement(WebDriverBy::cssSelector("#dk_container_cc_exp_month > a"));
        $element->click();
        $element->sendKeys("12");
        $element->sendKeys(WebDriverKeys::ENTER);

        $element = $web_driver->findElement(WebDriverBy::cssSelector("#dk_container_cc_exp_year > a"));
        $element->click();
        $element->sendKeys("17");
        $element->sendKeys(WebDriverKeys::ENTER);

        $element = $web_driver->findElement(WebDriverBy::cssSelector("#cc_cvv2"));
        $element->sendKeys("549");

//mar
        $element = $web_driver->findElement(WebDriverBy::cssSelector("#continueButton"));
        $element->click();

        $output = $this->buildTables();
        return $this->buildTables();

    }


    public function runTest($os, $browser)
    {
        $web_driver = RemoteWebDriver::create(
            "https://mark1171:ZpoXXfxqVETXS8wLxguq@hub-cloud.browserstack.com/wd/hub",
            array("platform" => $os, "browserName" => $browser)
        );
        $web_driver->get("https://www.footballticketpad.com/");


        $element = $web_driver->findElement(WebDriverBy::cssSelector("#upcoming_event_1 > div.btnsgroup > a.btn.pinkbtn"));
        if ($element) {
            $element->click();
        }

        $element = $web_driver->findElement(WebDriverBy::cssSelector("#buy-ticket > table.responsive.desktop-version > tbody > tr:nth-child(1) > td:nth-child(5) > a"));
        if ($element) {
            $element->click();
        }


        $element = $web_driver->findElement(WebDriverBy::name("number_of_ticket"));
        $element->sendKeys("2");

        $element = $web_driver->findElement(WebDriverBy::name("password"));
        $element->sendKeys("123123123");

        $element = $web_driver->findElement(WebDriverBy::name("password_confirmation"));
        $element->sendKeys("123123123");

        $element = $web_driver->findElement(WebDriverBy::name("first_name"));
        $element->sendKeys("Monkey");

        $element = $web_driver->findElement(WebDriverBy::name("last_name"));
        $element->sendKeys("Spanner");


        $element = $web_driver->findElement(WebDriverBy::name("shipping_country"));
        $element->sendKeys("United Kingdom");

        $element = $web_driver->findElement(WebDriverBy::name("other_country"));
        $element->sendKeys("UK - Mon – Fri Special Free");

        $element = $web_driver->findElement(WebDriverBy::name("country_code"));
        $element->sendKeys("0044");
        $element = $web_driver->findElement(WebDriverBy::name("contact_no"));
        $element->sendKeys("007759273459");
        $element = $web_driver->findElement(WebDriverBy::name("street"));
        $element->sendKeys("This road");
        $element = $web_driver->findElement(WebDriverBy::name("postcode"));
        $element->sendKeys("L34EG");

        $element = $web_driver->findElement(WebDriverBy::name("city"));
        $element->sendKeys("Liverpool");

        $randx = rand(100000, 900000);
        $rands = "mark+" . $randx . "@arcade247.co.uk";


        $element = $web_driver->findElement(WebDriverBy::name("email"));
        $element->sendKeys($rands);


        $element = $web_driver->findElement(WebDriverBy::name("email_confirmation"));
        $element->sendKeys($rands);

        $element = $web_driver->findElement(WebDriverBy::name("ship-same"));
        $element->click();
        $element = $web_driver->findElement(WebDriverBy::name("terms_n_conditions"));
        $element->click();
        $element->submit();


        $output = $this->buildTables();
        return $this->buildTables();

    }

    public function testlist()
    {
        return View::make('backend.automation.running');
    }

    public function viewTest()
    {
        $output = $this->buildTables();
        return View::make('backend.automation.view')->with('events',$output);
    }


    public function buildTables()
    {
        $url = "https://www.browserstack.com/automate/builds/ddcb38bfdd079f8a69c550d1f2d7f2ada0b0f714/sessions.json";
        $username = "mark1171";
        $password = "ZpoXXfxqVETXS8wLxguq";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        $outputArray = curl_exec($ch);
        $outputArray = json_decode($outputArray);
        curl_close($ch);
        $output = [];
        $counter = 1;
        foreach ($outputArray as $item) {
            if (!empty($item->automation_session->video_url)) {
                $output[$counter]['video'] = $item->automation_session->video_url;
            } else {
                $output[$counter]['video'] = "";
            }
            $output[$counter]['status'] = $item->automation_session->status;
            $output[$counter]['os_version'] = $item->automation_session->os_version;
            $output[$counter]['browser'] = $item->automation_session->browser;
            $output[$counter]['browser_version'] = $item->automation_session->browser_version;
            $output[$counter]['os'] = $item->automation_session->os;
            $output[$counter]['reason'] = $item->automation_session->reason;
            $counter++;
        }

        return $output;
    }
}
