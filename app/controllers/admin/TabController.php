<?php namespace App\Controllers\Admin;

use BaseController;
use Redirect;
use View;
use Input;
use Response;
use Tag;
use Str;
use Notification;
use Bond\Exceptions\Validation\ValidationException;

class TabController extends BaseController {


    public function __construct() {
        View::share('active', 'club/tabs');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {

        return View::make('backend.club.tabs')
            ->with('menu', 'club/tabs');
    }

    public function listjson()
    {
        $locale = \App::getLocale();
        $in = Input::all();

        $rs = \DB::table('football_ticket')
                ->where('type', 'club')
                ->select('id', 'title')
                ->skip($in['start'])
                ->take($in['length'])
                ->where('title', 'like', '%'.$in['search']['value'].'%')
                ->get();
        $out = array();

        foreach($rs as $r)
        {
            $temp = array();
            $temp[0] = '<a href="/admin/club/tabs/edit/'.$r->id.'" >'.$r->id.'</a>';
            $temp[1] = $r->title;
            $out[] = $temp;
        }
        $results = array();
        $results["data"] = $out;
        $response = Response::make($results);
        $response->header('Content-Type', 'application/json');
        return $response;
    }

    public function team($id) {
        $fbt = \DB::table('football_ticket')
                    ->where('id', $id)
                    ->first();

        View::share('club', $fbt->title);
        View::share('clubID', $fbt->id);

        return View::make('backend.club.club_tabs')
            ->with('menu', 'club/tabs');
    } 

    public function getTeamTabs($id)
    {
        $locale = \App::getLocale();

        $rs = \DB::table('fbt_tabs')
                ->where('fbt_id', $id)
                ->where('locale', $locale)
                ->get();

        $out = array();

        foreach($rs as $r)
        {
            $temp = array();
            $temp[0] = '<a href="/admin/club/tabs/edittab/'.$r->id.'">'.$r->id.'</a>';
            $temp[1] = $r->title;
            $temp[2] = $r->locale;
            $temp[3] = $r->enabled ? "Enabled" : "Disabled";
            $temp[4] = $r->order_shown;
            $temp[5] = '<a href="/admin/club/tabs/delete/'.$r->id.'?to='.$id.'" class="delete-node">Delete</a>';
            $out[] = $temp;
        }
        $results = array();
        $results["data"] = $out;
        $response = Response::make($results);
        $response->header('Content-Type', 'application/json');
        return $response;
    }

    public function addTeamTab($id)
    {
        View::share('id', $id);
        return View::make('backend.club.add_tab')
            ->with('menu', 'club/tabs');
    }

    public function addTab($id)
    {
        $input = Input::all();
        $locale = \App::getLocale();
        $input['order_shown'] = intval($input['order_shown']);
        $nl = 0;
        if(array_key_exists('new_layout',$input))
        {
            $nl = intval($input['new_layout']);
        }

        if(! isset($input['enabled']))
        {
            $input['enabled']=0;
        }
        $rs = \DB::table('fbt_tabs')
                ->insert([
                    'fbt_id' => $id,
                    'title' => $input['title'],
                    'content' => $input['content_page'],
                    'enabled' => $input['enabled'],
                    'locale' => $locale,
                    'order_shown' => $input['order_shown'],
                    'new_layout'=>$nl,
                    'icon_image'=>$input['icon_image']
                ]);

        return Response::json("/admin/club/tabs/edit/".$id, 200);
    }

    public function deleteTab($id)
    {
        $rs = \DB::table('fbt_tabs')
                    ->where('id', $id)
                    ->delete();
        if(Input::has('to'))
        {
            $to = Input::get('to');
            return Redirect::to('/admin/club/tabs/edit/'.$to);
        }
        return Redirect::to('/admin/club/tabs');
    }

    public function editTab($id)
    {
        $tab = \DB::table('fbt_tabs')
            ->where('id', $id)
            ->first();

        View::share('tab', $tab);
        View::share('id', $id);
        return View::make('backend.club.edit_tab')
            ->with('menu', 'club/tabs');
    }

    public function saveTab($id)
    {
        $input = Input::all();
        $input['order_shown'] = intval($input['order_shown']);
        $nl = 0;
        if(array_key_exists('new_layout',$input))
        {
            $nl = intval($input['new_layout']);
        }
        if(! isset($input['enabled']))
        {
            $input['enabled']=0;
        }
        $rs = \DB::table('fbt_tabs')
                    ->where('id', $id)
                    ->update([
                        'title' => $input['title'],
                        'content' => $input['content_page'],
                        'enabled' => $input['enabled'],
                        'order_shown' => $input['order_shown'],
                        'new_layout'=>$nl,
                        'icon_image'=>$input['icon_image'],
                    ]);

        return Response::json("/admin/club/tabs/edittab/".$id, 200);
    }
}
