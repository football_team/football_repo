<?php namespace App\Controllers\Admin;

use BaseController;
use Redirect;
use Sentry;
use View;
use Input;
use Validator;
use User;
use Notification;
use DB;

class UserController extends BaseController {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {

        $users = User::orderBy('created_at', 'DESC')
            ->paginate(10);

        return View::make('backend.user.index', compact('users'))->with('active', 'user')
            ->with('menu', 'users');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {

        return View::make('backend.user.create')
            ->with('menu', 'users/new');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store() {

        $formData = array(
            'first-name'       => Input::get('first_name'),
            'last-name'        => Input::get('last_name'),
            'email'            => Input::get('email'),
            'password'         => Input::get('password'),
            'confirm-password' => Input::get('confirm_password')
        );

        $rules = array(
            'first-name'       => 'required|min:3',
            'last-name'        => 'required|min:3',
            'email'            => 'required|email|unique:users,email',
            'password'         => 'required|min:4',
            'confirm-password' => 'required|same:password'
        );

        $validation = Validator::make($formData, $rules);

        if ($validation->fails()) {

            return Redirect::action('App\Controllers\Admin\UserController@create')->withErrors($validation)->withInput();
        }

        $user = Sentry::createUser(array(
            'email'      => $formData['email'],
            'password'   => $formData['password'],
            'first_name' => $formData['first-name'],
            'last_name'  => $formData['last-name'],
            'activated'  => 1,
        ));

        $adminGroup = Sentry::findGroupById(1);
        $user->addGroup($adminGroup);

        Notification::success('User was successfully added');

        return Redirect::action('App\Controllers\Admin\UserController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id) {

        $user = Sentry::findUserById($id);
        return View::make('backend.user.show', compact('user'))
            ->with('active', 'user')
            ->with('menu', 'user');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id) {

        $user = Sentry::findUserById($id);

        $user->getGroups();

        $flag = 0;
        foreach($user->groups as $group)
        {
            if($group->name == 'cservice')
            {
                $flag = 1;
                break;
            }
        }

        View::share('is_cs', $flag);

        return View::make('backend.user.edit', compact('user'))->with('active', 'user')
            ->with('menu', 'users/edit');;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id) {

        $formData = array(
            'first-name' => Input::get('first_name'),
            'last-name'  => Input::get('last_name'),
            'email'      => Input::get('email'),
        );

        $user = Sentry::findUserById($id);
        $user->email = $formData['email'];
        $user->first_name = $formData['first-name'];
        $user->last_name = $formData['last-name'];
        $user->save();

        Notification::success('User was successfully updated');

        return Redirect::action('App\Controllers\Admin\UserController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id) {

        $user = Sentry::findUserById($id);
        $user->delete();

        Notification::success('User was successfully deleted');
        return Redirect::action('App\Controllers\Admin\UserController@index');
    }

    public function confirmDestroy($id) {

        $user = User::find($id);
        return View::make('backend.user.confirm-destroy', compact('user'))
            ->with('active', 'user')
            ->with('menu', 'user/edit');
    }
    public function removeByID($id){

        $disable = DB::table('events_related_tickets')->where('product_id','=',$id)->update(['ticket_status' => 'deleted']);
        return $this->getSellers();

    }

    public function getBrokers(){


        return $this->getTicketsBySellerType('broker');
    }
    public function getCustomers(){
        return $this->getTicketsBySellerType('customer');
    }
    public function getSellers(){

        return $this->getTicketsBySellerType('seller');

    }
    public function getTicketsBySellerType($tpe){

        $data = DB::table('events_related_tickets')
            ->join('web_user', 'events_related_tickets.user_id', '=', 'web_user.user_id')
            ->join('events', 'events_related_tickets.event_id', '=', 'events.id')
            ->where('web_user.seller_type','=',$tpe)
            ->where('events_related_tickets.ticket_status','!=','deleted')
            ->paginate(25);

        foreach($data as $item){

            $ticketFormId = json_decode($item->ticket);
            $ticketTypeId = $ticketFormId->ticketInformation->ticket_type;
            $ticketFormId = $ticketFormId->ticketInformation->form_of_ticket;
            $formitem = DB::table('events_form_of_ticket')->where('id','=',$ticketFormId)->first();
            $ticketType = DB::table('events_ticket_type')->where('id','=',$ticketTypeId)->first();
            if (!empty($formitem->title)) {
                $item->ticket = $formitem->title;
            }else{
                $item->ticket = "NA";
            }


            if (!empty($ticketType)) {
                $item->ticket_type = $ticketType->title;
            }else{
                $item->ticket_type = "NA";
            }


        }
        return View::make('backend.sellers.seller')->with('seller',$data);

    }

    public function addUserToCSGroup($id)
    {
        try{
            $group = Sentry::findGroupByName('cservice');
            $user = Sentry::findUserById($id);
            $user->addGroup($group);
            $user->save();
            return \Response::json("Success", 200);
        }
        catch(Exception $e)
        {
            //as you were
        }

        return \Response::json("Error", 404);
    }

    public function removeUserFromCSGroup($id)
    {
        try{
            $group = Sentry::findGroupByName('cservice');
            $user = Sentry::findUserById($id);
            $user->removeGroup($group);
            $user->save();
            return \Response::json("Success", 200);
        }
        catch(Exception $e)
        {
            //as you were
        }

        return \Response::json("Error", 404);
    }
}
