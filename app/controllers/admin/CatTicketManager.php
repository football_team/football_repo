<?php namespace App\Controllers\Admin;

use BaseController;
use EventsrelatedTickets;
use LogEvent;
use Sentry;
use Redirect;
use Translate;
use View;
use Input;
use Pages;
use Template;
use DB;
use PagesMeta;
use Validator;
use Cache;
use Response;
use Request;
use Notification;
use Bond\Traits\GridPagination;
use Bond\Repositories\Page\PageRepository as Page;
use Bond\Exceptions\Validation\ValidationException;

class CatTicketManager extends BaseController
{

    /**
     * Returns the view Showing all of the Tickets yet to be tested
     * for the correct ticket type]
     *
     * @return array $arrayFullTickets
     */

    public function __construct()
    {
    }


    /**
     * @return mixed
     */
    public function listAll()
    {
        /** @var object $objectTickets */
        $objectTickets = EventsrelatedTickets::where('ticket_status', '=', 'active')->where('reviewed', '=', '0')->orderBy('id', 'DESC')->limit(50)->get();

        /** @var TYPE_NAME $countOfObjectsToGets */
        $countOfObjectsToGets = EventsrelatedTickets::where('ticket_status', '=', 'active')->where('reviewed', '=', '0')->count();

        /** @var array $arrayFullTickets */
        $arrayFullTickets = [];

        /** @var int $counter */
        $counter = 0;

        if (!empty($objectTickets)) {
            foreach ($objectTickets as $rowKey => $rowItem) {

                //Increment the counter
                $counter++;

                /** @var array $rowTicketData */
                $rowItem->ticket = json_decode($rowItem->ticket);
                $arrayFullTickets[$counter][$rowKey] = $rowItem;
            }
        }


        /** @var object $ticketTypes */
        $ticketTypes = DB::table('events_ticket_type')->orderBy('id', 'DESC')->get();

        /** @var TYPE_NAME $arrayTicketTypes */
        $arrayTicketTypes = [];

        foreach ($ticketTypes as $rowKey => $rowValue) {

            $arrayTicketTypes[$rowValue->id] = $rowValue;

        }


        /** @var TYPE_NAME $objectUsers */
        $objectUsers = DB::table('web_user')->orderBy('id', 'DESC')->get();


        /** @var TYPE_NAME $arrayUsers */
        $arrayUsers = [];

        foreach ($objectUsers as $rowKey => $rowValue) {

            $arrayUsers[$rowValue->user_id] = $rowValue;

        }

        /** @var TYPE_NAME $objectUsers */
        $objectClubs = DB::table('event_stadium')->get();


        /** @var TYPE_NAME $arrayUsers */
        $arrayClub = [];

        foreach ($objectClubs as $rowKey => $rowValue) {

            $arrayClub[$rowValue->event_id] = $rowValue;

        }

        /** @var TYPE_NAME $objectUsers */
        $objectStadium = DB::table('stadium')->get();


        /** @var TYPE_NAME $arrayUsers */
        $arrayStadium = [];

        foreach ($objectStadium as $rowKey => $rowValue) {

            $arrayStadium[$rowValue->id] = $rowValue;

        }


        /** @var object $events */
        $events = DB::table('events')->get();

        if (!Cache::has('cacheKeyEvents')) {

            /** @var array $arrayEvents */
            $arrayEvents = [];

            foreach ($events as $rowKey => $rowValue) {

                $arrayEvents[$rowValue->id] = $rowValue;

            }

            Cache::put('cacheKeyEvents', $arrayEvents, '600');

        }else{

            /** @var array $arrayEvents */
            $arrayEvents = Cache::get('cacheKeyEvents');

        }

        //return the data in a view
        if (!empty($arrayFullTickets)) {
            View::share('listTickets', $arrayFullTickets);
            View::share('ticketTypes', $arrayTicketTypes);
            View::share('web_user', $arrayUsers);
            View::share('clubs', $arrayClub);
            View::share('stadiums', $arrayStadium);
            View::share('events', $arrayEvents);
            View::share('count', $countOfObjectsToGets);
            return View::make('backend.catmanage.show');
        }
    }


    public function saveItem($id, $tickettype)
    {

        /** @var object $objectTickets */
        $objectTickets = EventsrelatedTickets::where('id', '=', $id)->first();
        $data = json_decode($objectTickets->ticket);
        $data->ticketInformation->ticket_type = $tickettype;
        $newData = json_encode($data);
        $objectTickets->ticket = $newData;
        $objectTickets->reviewed = 1;
        print_r($objectTickets->ticket);
        echo $objectTickets->save();
    }
}