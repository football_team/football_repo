<?php namespace App\Controllers\Admin;

use BaseController;
use LogEvent;
use Sentry;
use Redirect;
use Translate;
use View;
use Input;
use Pages;
use Template;
use DB;
use PagesMeta;
use Validator;
use Response;
use Request;
use Notification;
use Bond\Traits\GridPagination;
use Bond\Repositories\Page\PageRepository as Page;
use Bond\Exceptions\Validation\ValidationException;

class TranslateController extends BaseController
{
    public function translatesearch($search, $langx)
    {
        $search = "%" . $search . "%";
        $transalate = Translate::where('node', 'like', $search)->get();

        $allTranslations = [];

        foreach ($transalate as $translate) {

            $transalateThis = DB::table('trans_text')
                ->where('node_id', '=', $translate->id)
                ->get();

            $allTranslations[$translate->id]['node'] = $translate->node;

            foreach ($transalateThis as $lang) {
                $allTranslations[$translate->id][$lang->lang] = $lang->text;
            }

        }
        $langx = $this->getLangDetails($langx);
        return View::make("backend.lang.show")->with('transalate', $allTranslations)->with('links', $search)->with('lang', $langx);

    }

    public function translate($langx)
    {
        $transalate = Translate::paginate(15);
        $allTranslations = [];
        foreach ($transalate as $translate) {
            $transalateThis = DB::table('trans_text')
                ->where('node_id', '=', $translate->id)
                ->get();
            $allTranslations[$translate->id]['node'] = $translate->node;
            foreach ($transalateThis as $lang) {
                $allTranslations[$translate->id][$lang->lang] = $lang->text;
            }
        }
        $langx = $this->getLangDetails($langx);
        return View::make("backend.lang.show")->with('transalate', $allTranslations)->with('links', $transalate->links())->with('lang', $langx);
    }

    public function transSave($lang)
    {

        $lang = $this->getLangDetails($lang);
        if (!empty($_REQUEST['node'])) {
            $node = $_REQUEST['node'];
        }
        if (!empty($_REQUEST['EN'])) {
            $EN = $_REQUEST['EN'];
        } else {
            $EN = "";
        }

        if (!empty($_REQUEST[$lang['slug']])) {
            $langText = $_REQUEST[$lang['slug']];
        } else {
            $langText = "";
        }

        /**
         * Get the nodes ID
         */
        $logevent = new LogEventController();

        if (!empty($node)) {
            $transalateThis = DB::table('trans')
                ->whereRaw("BINARY `node` = '{$node}'")->first();
            if (!empty($transalateThis->id)) {
                $transalteENG = DB::table('trans_text')
                    ->whereRaw("BINARY `node_id` = '{$node}'")->first();
                $this->tranlateThisAll($EN, $langText, $lang, $transalateThis->id);
                $logevent->logTranslator("Update", "translated " . $node . " To <b>" . $langText . "</b> in Language " . $lang['slug'] . " ");
            } else {
                DB::table('trans')
                    ->insert(array('node' => $node));
                $transalateThis = DB::table('trans')
                    ->whereRaw("BINARY `node` = '{$node}'")->first();
                $this->tranlateThisAll($EN, $langText, $lang, $transalateThis->id);
                $logevent->logTranslator("New", "Created new Translation " . $node . " in Language " . $lang['slug'] . " ");
            }
        }
        if (!empty($_REQUEST['page'])) {
            $string = '/admin/translate/' . $lang['slug'] . '/?page=' . $_REQUEST['page'];
            return Redirect::to($string);
        } else {
            return Redirect::to('/admin/translate/' . $lang['slug']);
        }


    }


    public function tranlateThisAll($EN, $langText, $lang, $id)
    {

        if ($EN != null) {
            $this->translaterow($EN, "EN", $id);
        }
        $this->translaterow($langText, $lang['slug'], $id);

    }

    public function translaterow($text, $lang, $id)
    {
        $isrow = DB::table('trans_text')
            ->where('node_id', $id)
            ->where('lang', $lang)->first();

        if (!empty($isrow)) {
            $xrs = DB::table('trans_text')
                ->where('node_id', $id)
                ->where('lang', $lang)
                ->update(array('text' => $text));
        } else {
            DB::table('trans_text')
                ->insert(array('text' => $text, 'lang' => $lang, 'node_id' => $id));
        }

    }

    public function getLangDetails($lang)
    {
        $langArray = [
            'sp' => ['full' => 'SP', 'slug' => 'sp'],
            'ru' => ['full' => 'Russian', 'slug' => 'ru'],
            'es' => ['full' => 'ES', 'slug' => 'es'],
            'fr' => ['full' => 'FR', 'slug' => 'fr'],
            'it' => ['full' => 'IT', 'slug' => 'it'],
            'is' => ['full' => 'IS', 'slug' => 'is'],
            'sv' => ['full' => 'SV', 'slug' => 'sv'],
            'fi' => ['full' => 'FI', 'slug' => 'fi'],
            'dk' => ['full' => 'DK', 'slug' => 'dk'],
            'no' => ['full' => 'NO', 'slug' => 'no'],
            'de' => ['full' => 'DE', 'slug' => 'de'],
            'nl' => ['full' => 'NL', 'slug' => 'nl'],
            'tr' => ['full' => 'TR', 'slug' => 'tr'],
            'kr' => ['full' => 'KR', 'slug' => 'kr'],
            'cz' => ['full' => 'CZ', 'slug' => 'cz'],
            'pt' => ['full' => 'PT', 'slug' => 'pt'],
            'th' => ['full' => 'TH', 'slug' => 'th'],
            'ja' => ['full' => 'JA', 'slug' => 'ja'],
            'cn' => ['full' => 'CN', 'slug' => 'cn'],
        ];
        return $langArray[$lang];

    }

    public function importCSV()
    {

        $fulLoc = storage_path('cz-trans.csv');
        $language = "cz";

        // UTF
        // $csv = array_map('str_getcsv', explode("\r", file_get_contents($fulLoc) ) );

        // ASCII
        $csv = array_map('str_getcsv', file($fulLoc));

        // var_dump($csv);
        // die();

        foreach($csv as $entry){
            $trans = DB::table('trans')
                    ->whereRaw("BINARY `node` = '".$entry[0]."'")
                    ->first();

            if(empty($trans))
            {
                DB::table('trans')
                    ->insert([
                        'node'=>$entry[0]
                    ]);

                $trans = DB::table('trans')
                    ->whereRaw("BINARY `node` = '".$entry[0]."'")
                    ->first();
            }
            else
            {
                \Log::info("Log made");
            }

            //So need to update english trans
            $tt = DB::table('trans_text')
                    ->where('node_id', $trans->id)
                    ->where('lang', 'EN')
                    ->first();

            if($tt)
            {
                DB::table('trans_text')
                    ->where('node_id', $trans->id)
                    ->where('lang', 'EN')
                    ->update([
                        'text'=>$entry[1]
                    ]);

                \Log::info("Update EN");
            }
            else
            {
                DB::table('trans_text')
                    ->insert([
                        'node_id'=>$trans->id,
                        'lang'=>'EN',
                        'text'=>$entry[1]
                    ]);

                \Log::info("Insert EN");
            }

            $tt2 = DB::table('trans_text')
                ->where('node_id', $trans->id)
                ->where('lang', $language)
                ->first();

            if($tt2)
            {
                DB::table('trans_text')
                    ->where('node_id', $trans->id)
                    ->where('lang', $language)
                    ->update([
                        'text'=>$entry[2]
                    ]);

                \Log::info("Update ".$language);
            }
            else
            {
                DB::table('trans_text')
                    ->insert([
                        'node_id'=>$trans->id,
                        'lang'=>$language,
                        'text'=>$entry[2]
                    ]);

                \Log::info("Insert ".$language);
            }
        }

        echo "done";
    }

}