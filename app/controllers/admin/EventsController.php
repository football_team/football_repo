<?php namespace App\Controllers\Admin;

use BaseController;
use LocalLang;
use Map;
use Redirect;
use Request;
use View;
use Input;
use Validator;
use Response;
use Str;
use Cache;
use DB;
use Notification;
use FootballTickets;
use Stadium;
use Bond\Repositories\Events\EventsRepository AS Events;
use Bond\Exceptions\Validation\ValidationException;
use FootBallEvent;
use Bond\Traits\GridPagination;
use EventData;

class EventsController extends BaseController
{
    use GridPagination;
    protected $events;

    public function __construct(Events $events)
    {

        View::share('active', 'modules');
        $this->events = $events;
    }

    public function postFix()
    {
        $eventsEffected = EventData::where('migrate', '=', '1')->get();;
        $count = 0;
        echo "<table style='border:1px solid;'>";
        foreach ($eventsEffected as $item) {
            echo "<tr style='border:1px solid; background:red; color:white;'><td style='border:1px solid;'>" . $item->id . "</td><td style='border:1px solid;'>" . $item->title . "</td><td></td><td  style='border:1px solid;'>" . $item->magrate_id . "</td><td></td></tr>";

            $translationArray = [];

            $a_trans = LocalLang::where('id', '=', "event_meta_desc_" . $item->id)->where('locale',"!=",'en_gb')->where('locale',"!=",'rus')->where('locale',"!=",'ru_by')->where('locale',"!=",'fr_fr')->where('locale',"!=",'ru_ua')->get();
            foreach ($a_trans as $a) {
                $translationArray[$a->locale]['origin'] = $a->lstring;
            }

            $b_trans = LocalLang::where('id', '=', "event_meta_desc_" . $item->magrate_id)->where('locale',"!=",'en_gb')->where('locale',"!=",'rus')->where('locale',"!=",'ru_by')->where('locale',"!=",'ru_ua')->where('locale',"!=",'fr_fr')->get();
            foreach ($b_trans as $b) {
                $translationArray[$b->locale]['new'] = $b->lstring;
            }

            foreach ($translationArray as $y => $c) {

                if (!empty($c['origin'])) {


                if (empty($c['new'])) {
                    $c['new'] = "";
                }

                    if($c['origin']==$c['new']){
                        $classString="style='background: green !important; width:50px;'";
                    }else{
                        $classString="style='background: orange !important; width:50px;'";
                    }

                echo "<tr style='border:1px solid;'><td style='border:1px solid;'>" . $y . "</td><td style='border:1px solid;'>" . $c['origin'] . "</td><td><a href='/admin/migration/dofix/" . $item->magrate_id . "/" . $y . "/" . $item->id . "'>Migrate</a></td><td style='border:1px solid;'>" . $c['new'] . "</td><td ".$classString."></td></tr>";
                }
            }


        }
        echo "</table>";
        echo $count;

    }


    public function doFix($new, $lang, $old)
    {

        $doaction = LocalLang::where('id', '=', 'event_meta_desc_' . $new)
            ->where('locale', '=', $lang)->first();


        $oldData = LocalLang::where('id', '=', 'event_meta_desc_' . $old)
            ->where('locale', '=', $lang)->first();
        if ($doaction === null) {

            $saveNew = new LocalLang();
            $newid = 'event_meta_title_' . $new;
            $newstring = "".$oldData->lstring;

            $saveNew->id = $newid;
            $saveNew->locale = $lang;
            $saveNew->lstring = $newstring;

            $saveNew->save();

        } else {
            LocalLang::where('id', '=', 'event_meta_desc_' . $new)->where('locale', '=', $lang)->update(['lstring' => $oldData->lstring]);
        }


        return $this->postFix();
    }


    public function index()
    {
        $events = $this->events->paginate(null, true);
        return View::make('backend.events.index', compact('events'))
            ->with('menu', 'events');
    }

    public function teamFilterEents($lang, $order, $dir, $year, $tournament_id, $team, $dataType)
    {
        if ($order == "date") {
            $searchorder = "datetime";
        } elseif ($order == "alp") {
            $searchorder = "title";
        } elseif ($order == "hteam") {
            $searchorder = "home_team_id";
        } elseif ($order == "ateam") {
            $searchorder = "away_team_id";
        } else {
            $searchorder = "datetime";
        }

        if ($dir == 1) {
            $direction = "DESC";
        } else {

            $direction = "ASC";
        }

        if ($tournament_id == "all" && $team == "all" && $year == "all") {
            $events = EventData::orderBy($searchorder, $direction)->paginate(25);
        } elseif ($tournament_id != "all" && $team == "all" && $year == "all") {
            $events = EventData::where('tournament_id', '=', $tournament_id)->orderBy($searchorder, $direction)->paginate(25);
        } elseif ($tournament_id == "all" && $team == "all" && $year != "all") {
            $events = EventData::where('season_id', '=', $year)->orderBy($searchorder, $direction)->paginate(25);
        } elseif ($tournament_id != "all" && $team == "all" && $year != "all") {
            $events = EventData::where('tournament_id', '=', $tournament_id)->where('season_id', '=', $year)->orderBy($searchorder, $direction)->paginate(25);
        } elseif ($tournament_id != "all" && $team != "all" && $year == "all") {
            $events = EventData::where('tournament_id', '=', $tournament_id)->where('home_team_id', '=', $team)->orWhere('away_team_id', '=', $team)->where('tournament_id', '=', $tournament_id)->orderBy($searchorder, $direction)->paginate(25);
        } elseif ($tournament_id != "all" && $team != "all" && $year != "all") {
            $events = EventData::where('season_id', '=', $year)->where('tournament_id', '=', $tournament_id)->where('home_team_id', '=', $team)->orWhere('away_team_id', '=', $team)->where('season_id', '=', $year)->where('tournament_id', '=', $tournament_id)->orderBy($searchorder, $direction)->paginate(25);
        } elseif ($tournament_id == "all" && $team != "all" && $year == "all") {
            $events = EventData::where('home_team_id', '=', $team)->orWhere('away_team_id', '=', $team)->orderBy($searchorder, $direction)->paginate(25);
        } elseif ($tournament_id == "all" && $team != "all" && $year != "all") {
            $events = EventData::where('home_team_id', '=', $team)->where('season_id', '=', $year)->orWhere('away_team_id', '=', $team)->where('season_id', '=', $year)->orderBy($searchorder, $direction)->paginate(25);
        } else {
            $events = EventData::orderBy($searchorder, $direction)->paginate(25);
        }


        $langClean = $this->cleanLang($lang);


        if ($langClean != false) {

            foreach ($events as $event) {

                /**
                 *  Grab the translated meta_description
                 */
                $translatedMeta = DB::table('locale_text')->where('id', '=', "event_meta_desc_" . $event->id)->where('locale', '=', $langClean)->first();
                if (!empty($translatedMeta->lstring)) {
                    $event->meta_description = $translatedMeta->lstring;
                } else {
                    $event->meta_description = "";
                }


                /**
                 *  Grab the translated meta_title
                 */
                $translatedTitle = DB::table('locale_text')->where('id', '=', "event_meta_title_" . $event->id)->where('locale', '=', $langClean)->first();
                if (!empty($translatedTitle->lstring)) {
                    $event->meta_title = $translatedTitle->lstring;
                } else {
                    $event->meta_title = "";
                }


                /**
                 *  Grab the translated content
                 *
                 */
                $translatedContent = DB::table('locale_text')->where('id', '=', "event_content_" . $event->id)->where('locale', '=', $langClean)->first();
                if (!empty($translatedContent->lstring)) {
                    $event->content = $translatedContent->lstring;
                } else {
                    $event->content = "";
                }


                /**
                 *  Grab the translated Title
                 */
                $translatedTitle = DB::table('locale_text')->where('id', '=', "event_title_" . $event->id)->where('locale', '=', $langClean)->first();
                if (!empty($translatedTitle->lstring)) {
                    $event->transtitle = $translatedTitle->lstring;
                } else {
                    $event->transtitle = "  ";
                }

            }

        }

        $Allclubs = DB::table('football_ticket')->where('type', '=', "club")->orderBy('title','ASC')->get();
        return View::make('backend.events.eventPremMeta', compact('events'))
            ->with('menu', 'events')->with('season_id', 362)->with('dataType', $dataType)->with('teamid', $team)->with('season', $tournament_id)->with('year', $year)->with('order', $order)->with('dir', $dir)->with('lang', $lang)->with('Allclubs',$Allclubs);

    }

    public function teamFilterEentsSearch($lang, $order, $dir, $year, $tournament_id, $team, $dataType, $search)
    {


        if ($order == "date") {
            $searchorder = "datetime";
        } elseif ($order == "alp") {
            $searchorder = "title";
        } elseif ($order == "hteam") {
            $searchorder = "home_team_id";
        } elseif ($order == "ateam") {
            $searchorder = "away_team_id";
        } else {
            $searchorder = "datetime";
        }

        if ($dir == 1) {
            $direction = "DESC";
        } else {

            $direction = "ASC";
        }


        if ($tournament_id == "all" && $team == "all" && $year == "all") {
            $events = EventData::orderBy($searchorder, $direction)->where('title', 'like', "%" . $search . "%")->paginate(25);
        } elseif ($tournament_id != "all" && $team == "all" && $year == "all") {
            $events = EventData::where('tournament_id', '=', $tournament_id)->orderBy($searchorder, $direction)->where('title', 'like', "%" . $search . "%")->paginate(25);
        } elseif ($tournament_id == "all" && $team == "all" && $year != "all") {
            $events = EventData::where('season_id', '=', $year)->orderBy($searchorder, $direction)->where('title', 'like', "%" . $search . "%")->paginate(25);
        } elseif ($tournament_id != "all" && $team == "all" && $year != "all") {
            $events = EventData::where('tournament_id', '=', $tournament_id)->where('title', 'like', "%" . $search . "%")->where('season_id', '=', $year)->orderBy($searchorder, $direction)->paginate(25);
        } elseif ($tournament_id != "all" && $team != "all" && $year == "all") {
            $events = EventData::where('tournament_id', '=', $tournament_id)->where('title', 'like', "%" . $search . "%")->where('home_team_id', '=', $team)->orWhere('away_team_id', '=', $team)->where('title', 'like', "%" . $search . "%")->where('tournament_id', '=', $tournament_id)->orderBy($searchorder, $direction)->paginate(25);
        } elseif ($tournament_id != "all" && $team != "all" && $year != "all") {
            $events = EventData::where('season_id', '=', $year)->where('tournament_id', '=', $tournament_id)->where('title', 'like', "%" . $search . "%")->where('home_team_id', '=', $team)->orWhere('away_team_id', '=', $team)->where('title', 'like', "%" . $search . "%")->where('season_id', '=', $year)->where('tournament_id', '=', $tournament_id)->orderBy($searchorder, $direction)->paginate(25);
        } elseif ($tournament_id == "all" && $team != "all" && $year == "all") {
            $events = EventData::where('home_team_id', '=', $team)->orWhere('away_team_id', '=', $team)->where('title', 'like', "%" . $search . "%")->orderBy($searchorder, $direction)->paginate(25);
        } elseif ($tournament_id == "all" && $team != "all" && $year != "all") {
            $events = EventData::where('home_team_id', '=', $team)->where('season_id', '=', $year)->where('title', 'like', "%" . $search . "%")->orWhere('away_team_id', '=', $team)->where('title', 'like', "%" . $search . "%")->where('season_id', '=', $year)->orderBy($searchorder, $direction)->paginate(25);
        } else {
            $events = EventData::orderBy($searchorder, $direction)->where('title', 'like', "%" . $search . "%")->paginate(25);
        }


        $langClean = $this->cleanLang($lang);
        if ($langClean != false) {

            foreach ($events as $event) {

                /**
                 *  Grab the translated meta_description
                 */
                $translatedMeta = DB::table('locale_text')->where('id', '=', "event_meta_desc_" . $event->id)->where('locale', '=', $langClean)->first();
                if (!empty($translatedMeta->lstring)) {
                    $event->meta_description = $translatedMeta->lstring;
                } else {
                    $event->meta_description = "";
                }


                /**
                 *  Grab the translated meta_title
                 */
                $translatedTitle = DB::table('locale_text')->where('id', '=', "event_meta_title_" . $event->id)->where('locale', '=', $langClean)->first();
                if (!empty($translatedTitle->lstring)) {
                    $event->meta_title = $translatedTitle->lstring;
                } else {
                    $event->meta_title = "";
                }


                /**
                 *  Grab the translated content
                 *
                 */
                $translatedContent = DB::table('locale_text')->where('id', '=', "event_content_" . $event->id)->where('locale', '=', $langClean)->first();
                if (!empty($translatedContent->lstring)) {
                    $event->content = $translatedContent->lstring;
                } else {
                    $event->content = "";
                }


                /**
                 *  Grab the translated Title
                 */
                $translatedTitle = DB::table('locale_text')->where('id', '=', "event_title_" . $event->id)->where('locale', '=', $langClean)->first();
                if (!empty($translatedTitle->lstring)) {
                    $event->transtitle = $translatedTitle->lstring;
                } else {
                    $event->transtitle = "  ";
                }

            }

        }


        $Allclubs = DB::table('football_ticket')->where('type', '=', "club")->orderBy('title', 'ASC')->get();
        return View::make('backend.events.eventPremMeta', compact('events'))
            ->with('menu', 'events')->with('season_id', 362)->with('dataType', $dataType)->with('teamid', $team)->with('season', $tournament_id)->with('year', $year)->with('order', $order)->with('dir', $dir)->with('lang', $lang)->with('Allclubs', $Allclubs);


    }

    public function eventsMetaPrem2016($dataType)
    {

        $events = EventData::where('season_id', '=', 362)->orderBy('datetime', 'desc')->paginate(25);
        return View::make('backend.events.eventPremMeta', compact('events'))
            ->with('menu', 'events')->with('season_id', 362)->with('dataType', $dataType);

    }

    public function eventsMetaPrem2016Search($dataType, $search)
    {

        $events = EventData::where('season_id', '=', 362)->orderBy('datetime', 'desc')->where('title', 'like', "%" . $search . "%")->paginate(25);
        return View::make('backend.events.eventPremMeta', compact('events'))
            ->with('menu', 'events')->with('season_id', 362)->with('dataType', $dataType);

    }

    /**
     *  Allocates a new Kick of date to the event
     * Used i the migration Tools
     */
    public function saveNewDate($datatype)
    {

        $eventid = Input::get('eventid');
        $date = Input::get('date');;
        $lang = Input::get('lang');
        $event = EventData::where('id', '=', $eventid)->first();
        if ($lang == "uk") {
            $this->saveEventData($datatype, $event, $date);
        } else {

            /**Билеты Челси - Сандерленд | Билеты на футбол | Ticket Pad
             * Lets SAve to the language string
             */
            $langClean = $this->cleanLang($lang);

            if ($datatype == "datetime") {
                $event->datetime = $date;
            } elseif ($datatype == "meta_title") {
                return $this->saveLocalLang($eventid, $langClean, $date, $datatype);
            } elseif ($datatype == "meta_description") {
                $datatype = "meta_desc";
                return $this->saveLocalLang($eventid, $langClean, $date, $datatype);
            } elseif ($datatype == "content") {
                $this->saveLocalLang($eventid, $langClean, $date, $datatype);
                return $event->content = $date;
            } elseif ($datatype == "title") {
                return $this->saveLocalLang($eventid, $langClean, $date, $datatype);
                $event->title = $date;
            }

        }

        return $event;

    }

    /**
     * @param $lang
     * @return bool|string
     */
    public function cleanLang($lang)
    {
        switch ($lang) {
            case "us": $langClean = "en_us"; break;
            case "ru": $langClean = "rus";   break;
            case "es": $langClean = "es_es"; break;
            case "fr": $langClean = "fr_fr"; break;
            case "nz": $langClean = "en_nz"; break;
            case "sa": $langClean = "en_sa"; break;
            case "hk": $langClean = "en_hk"; break;
            case "ie": $langClean = "en_ie"; break;
            case "ch": $langClean = "fr_ch"; break;
            case "be": $langClean = "fr_be"; break;
            case "by": $langClean = "ru_by"; break;
            case "ua": $langClean = "ru_ua"; break;
            case "ec": $langClean = "sp_ec"; break;
            case "cl": $langClean = "sp_cl"; break;
            case "ar": $langClean = "sp_ar"; break;
            case "ve": $langClean = "sp_ve"; break;
            case "pe": $langClean = "sp_pe"; break;
            case "cl": $langClean = "sp_cl"; break;
            case "ar": $langClean = "sp_ar"; break;
            case "ve": $langClean = "sp_ve"; break;
            case "pe": $langClean = "sp_pe"; break;
            case "me": $langClean = "sp_mx"; break;
            case "co": $langClean = "sp_co"; break;
            case "ca": $langClean = "en_Ca"; break;
            case "lu": $langClean = "fr_lu"; break;
            case "pt": $langClean = "prt_pt"; break;
            case "it": $langClean = 'it_it'; break;
            case "is": $langClean = 'is_is'; break;
            case "sv": $langClean = 'sv_se'; break;
            case "fi": $langClean = 'fi_fi'; break;
            case "dk": $langClean = 'dk_dk'; break;
            case "cn": $langClean = 'zh_cn'; break;
            case "no": $langClean = 'no_no'; break;
            case "pl": $langClean = 'pl_pl'; break;
            case "ja": $langClean = 'ja_ja'; break;
            case "de": $langClean = 'de_de'; break;
            case "nl": $langClean = 'nl_nl'; break;
            case "tr": $langClean = 'tr_tr'; break;
            case "kr": $langClean = 'kr_kr'; break;
            case "cz": $langClean = 'cz_cz'; break;
            case "th": $langClean = 'th_th'; break;
            default: $langClean = false; break;
        }
        return $langClean;
    }

    /**
     * @param $datatype
     * @param $event
     * @param $date
     */
    public function saveEventData($datatype, $event, $date)
    {

        if ($datatype == "datetime") {
            $event->datetime = $date;
        } elseif ($datatype == "meta_title") {
            $event->meta_title = $date;
        } elseif ($datatype == "meta_description") {
            $event->meta_description = $date;
        } elseif ($datatype == "content") {
            $event->content = $date;
        } elseif ($datatype == "title") {
            $event->title = $date;
        }
        $event->save();
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function jsonList()
    {
        if (!Request::ajax()) {
            $response = Response::make(array(
                    'draw' => 1,
                    'recordsTotal' => 0,
                    'recordsFiltered' => 0,
                    'data' => []
                )
            );

            $response->header('Content-Type', 'application/json');
            return $response;
        }

        $columns = array(
            0 => 'id',
            1 => 'title',
            2 => 'slug',
            3 => 'season_id',
            4 => 'created_at'
        );
        $response = Response::make($this->getGridData('events', $columns));
        $response->header('Content-Type', 'application/json');
        return $response;
    }

    public function create()
    {

        View::share('season', FootballTickets::getDataForOptions('season'));
        View::share('tournaments', FootballTickets::getDataForOptions('league'));
        View::share('club', FootballTickets::getDataForOptions('club'));
        View::share('countries', FootballTickets::getDataForOptions('contry'));
        View::share('events', null);

        View::share('stadiums', Stadium::all());

        //get ticket type
        $results = DB::table('events_ticket_type')
            ->select('id', 'title')
            ->where('default', '=', '1')
            ->get();
        $ticketType = array();
        foreach ($results as $result) {
            $ticketType[$result->id] = $result->title;
        }

        View::share('defaultTicketType', $ticketType);

        //get ticket type
        $results = DB::table('events_ticket_restrictions')
            ->select('id', 'title')
            ->where('default', '=', '1')
            ->get();
        $ticketRestriction = array();
        foreach ($results as $result) {
            $ticketRestriction[$result->id] = $result->title;
        }

        View::share('defaultTicketRestriction', $ticketRestriction);

        //get ticket type
        $results = DB::table('events_form_of_ticket')
            ->select('id', 'title')
            ->where('default', '=', '1')
            ->get();
        $ticketFormType = array();
        foreach ($results as $result) {
            $ticketFormType[$result->id] = $result->title;
        }

        $ttypes = \TicketType::where('status', 1)->orderBy('sort_order','ASC')->get();
        View::share('ttypes',$ttypes);

        View::share('defaultTicketFormType', $ticketFormType);

        return View::make('backend.events.create')
            ->with('menu', 'events/events');;
    }


    /**
     * @param $idx : id string of the event data to save
     * @param $langClean : The clean anme of the lang,  such as en_gb
     * @param $data : The information to be saved
     */
    public function saveLocalLang($eventid, $langClean, $data, $localNaming)
    {

        /**
         * INSERT TRANSLATED DATA
         */

        $idString = 'event_' . $localNaming . '_' . $eventid;
        $lang = LocalLang::where('id', '=', $idString)->where('locale', '=', $langClean)->first();
        if (($lang === null)) {
            $langc = new LocalLang();
            $langc->lstring = $data;
            $langc->locale = $langClean;
            $langc->id = $idString;
            $langc->save();
        } else {
            $lang = LocalLang::where('id', '=', $idString)->where('locale', '=', $langClean)->update(['lstring' => $data]);
            dd($lang);
        }

        /**
         * IF theres no title insert blank one
         */

        $idString = 'event_title_' . $eventid;
        $lang = LocalLang::where('id', '=', $idString)->where('locale', '=', $langClean)->first();
        if (($lang === null)) {
            $langc = new LocalLang();
            $langc->lstring = "";
            $langc->locale = $langClean;
            $langc->id = $idString;
            $langc->save();
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        try {
            $id = $this->events->create(Input::all());
            $input = Input::all();
            $stad = $input['stadium'];
            if ($stad && $stad != "na") {
                DB::table('event_stadium')
                    ->insert(['event_id' => $id, 'stadium_id' => $stad]);
            }
            unset($input['stadium']);

            Notification::success('Events was successfully added');

            return Redirect::route('admin.events.index');
        } catch (ValidationException $e) {
            return Redirect::back()->withInput()->withErrors($e->getErrors());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {

        $events = $this->events->find($id);
        return View::make('backend.events.show', compact('events'))
            ->with('menu', 'events/show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {

        View::share('season', FootballTickets::getDataForOptions('season'));
        View::share('tournaments', FootballTickets::getDataForOptions('league'));
        View::share('club', FootballTickets::getDataForOptions('club'));
        View::share('countries', FootballTickets:: getDataForOptions('country'));

        $ss = DB::table('event_stadium')->where('event_id', $id)->first();

        $stadiums = Stadium::all();

        View::share('stadiums', $stadiums);

        if ($ss) {
            View::share('curStad', $ss->stadium_id);
        }

        $events = $this->events->find($id);

        $listOfSvgMaps = Map::get();

        $logevent = new LogEventController();
        $logevent->logEvents("Edit", "Started Editing '" . $events->title . "'");

        $ttypes = \TicketType::where('status', 1)->orderBy('sort_order', 'ASC')->get();
        if(!empty($events->ticket_type_ids)&&($events->ticket_type_ids != ''))
        {
            $ArrTypes = explode(',', $events->ticket_type_ids);
            $types = new \TicketType();
            $ttypesSel = $types->ticketByIds($ArrTypes);

            $tArray = array();
            foreach($ttypesSel as $ttype)
            {
                $tArray[] = $ttype->id;
            }

            foreach($ttypes as $ttype)
            {
                if(in_array($ttype->id, $tArray))
                {
                    $ttype->selected = true;
                }
                else
                {
                    $ttype->selected = false;
                }
            }


        }
        View::share('ttypes', $ttypes);

        View::share('events', $events);
        return View::make('backend.events.edit')
            ->with('menu', 'events/edit')->with('listOfMaps', $listOfSvgMaps);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        $input = Input::all();
        if(!array_key_exists('is_tbc', $input))
        {
            $input['is_tbc'] = false;
        }
        try {

            $this->events->update($id, $input);
            if (Input::has('update-buyers-event-change')) {
                $this->notifyBuyersDatetimeChange($id);
            }

            if(Input::has('is_tbc') && Input::has('is_tbc_email'))
            {
                $this->notifyBuyersDatetimeTBC($id);
            }

            $stad = $input['stadium'];
            if ($stad) {
                $res = DB::table('event_stadium')
                    ->where('event_id', $id)
                    ->first();

                if ($stad != "na") {
                    if ($res) {
                        DB::table('event_stadium')
                            ->where('event_id', $id)
                            ->update(['stadium_id' => $stad]);
                    } else {
                        DB::table('event_stadium')
                            ->insert(['event_id' => $id, 'stadium_id' => $stad]);
                    }
                } else if ($res) {
                    DB::table('event_stadium')
                        ->where('event_id', $id)
                        ->delete();
                }

            }
            unset($input['stadium']);

            $events = $this->events->find($id);
            $logevent = new LogEventController();
            $logevent->logEvents("Updated", "Saved changes too '" . $events->title . "'");

            Notification::success('Events was successfully updated');
            return Redirect::route('admin.events.index');
        } catch (ValidationException $e) {
            return Redirect::back()->withInput()->withErrors($e->getErrors());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {

        $this->events->destroy($id);
        Notification::success('Events was successfully deleted');
        return Redirect::action('App\Controllers\Admin\EventsController@index');
    }

    public function confirmDestroy($id)
    {

        $events = $this->events->find($id);
        return View::make('backend.events.confirm-destroy', compact('events'))
            ->with('menu', 'events/edit');
    }

    public function togglePublish($id)
    {

        return $this->news->togglePublish($id);
    }

    public function widget()
    {
        $hotTickets = FootBallEvent::where('widget_display', '=', '1')
            ->orderBy('widget_order', 'ASC')
            ->whereRaw('datetime >= NOW()')
            ->where('is_tbc', false)
            ->get();

        $hotTicketsId = array();

        foreach ($hotTickets as $t) {
            $hotTicketsId[] = $t->id;
        }

        $games = array();

        if (!empty($hotTicketsId)) {
            $gameObj = FootBallEvent::whereRaw('datetime >= NOW()')
                ->orderBy('title', 'ASC')
                ->whereNotIn('id', $hotTicketsId);
        } else {
            $gameObj = FootBallEvent::whereRaw('datetime >= NOW()')
                ->orderBy('title', 'ASC');
        }

        $games = $gameObj->get();

        View::share('tickets', $hotTickets);
        View::share('games', $games);

        return View::make('backend.events.widget')
            ->with('menu', 'events/widget');
    }

    public function widgetUpdate()
    {
        $tickets = Input::get('tickets');
        $updateIds = array();
        if (is_array($tickets)) {
            foreach ($tickets as $ticket) {
                $id = (int)$ticket['id'];
                $order = (int)$ticket['index'];
                $updateIds[] = $id;
                DB::table('events')
                    ->where('id', $id)
                    ->update(array('widget_display' => 1, 'widget_order' => $order));
            }

            DB::table('events')->whereNotIn('id', $updateIds)->update(array('widget_display' => 0, 'widget_order' => 0));
        }
        echo json_encode(array('success'));
    }

    public function topTeamWidget()
    {
        $topTeams = FootballTickets::where('widget', '=', '1')
            ->orderBy('widget_order', 'ASC')
            ->whereRaw('type= "club"')
            ->get();

        $topTeamsId = array();

        foreach ($topTeamsId as $t) {
            $hotTicketsId[] = $t->id;
        }

        $teams = array();

        if (!empty($hotTicketsId)) {
            $gameObj = FootballTickets::orderBy('title', 'ASC')->where('type', '=', 'club')
                ->whereNotIn('id', $hotTicketsId);
        } else {
            $gameObj = FootballTickets::orderBy('title', 'ASC')->where('type', '=', 'club');
        }


        $teams = $gameObj->get();

        View::share('topTeams', $topTeams);
        View::share('teams', $teams);

        return View::make('backend.events.topteam-widget')
            ->with('menu', 'club/widget');
    }

    public function topTournaments()
    {
        $topTeams = FootballTickets::where('widget', '=', '1')
            ->orderBy('widget_order', 'ASC')
            ->whereRaw('is_published = 1 AND type= "league"')
            ->get();

        $topTeamsId = array();

        foreach ($topTeamsId as $t) {
            $hotTicketsId[] = $t->id;
        }

        $teams = array();

        if (!empty($hotTicketsId)) {
            $gameObj = FootballTickets::orderBy('title', 'ASC')->where('type', '=', 'league')
                ->whereNotIn('id', $hotTicketsId);
        } else {
            $gameObj = FootballTickets::orderBy('title', 'ASC')->where('type', '=', 'league');
        }


        $teams = $gameObj->get();

        View::share('topTournaments', $topTeams);
        View::share('tournament', $teams);

        return View::make('backend.events.tournaments-widget')
            ->with('menu', 'tournament/widget');
    }

    public function topTournamentsUpdate()
    {
        $tickets = Input::get('tickets');
        $type = Input::get('type');
        $updateIds = array();
        if (is_array($tickets)) {
            foreach ($tickets as $ticket) {
                $id = (int)$ticket['id'];
                $order = (int)$ticket['index'];
                $updateIds[] = $id;
                DB::table('football_ticket')
                    ->where('id', $id)
                    ->update(array('widget' => 1, 'widget_order' => $order));
            }

            DB::table('football_ticket')->whereNotIn('id', $updateIds)->where('type', '=', 'league')->update(array('widget' => 0, 'widget_order' => 0));
        }
        echo json_encode(array('success'));
    }

    public function topTeamWidgetUpdate()
    {
        $tickets = Input::get('tickets');
        $type = Input::get('type');
        $updateIds = array();
        if (is_array($tickets)) {
            foreach ($tickets as $ticket) {
                $id = (int)$ticket['id'];
                $order = (int)$ticket['index'];
                $updateIds[] = $id;
                DB::table('football_ticket')
                    ->where('id', $id)
                    ->update(array('widget' => 1, 'widget_order' => $order));
            }

            DB::table('football_ticket')->whereNotIn('id', $updateIds)->where('type', '=', 'club')->update(array('widget' => 0, 'widget_order' => 0));
        }
        echo json_encode(array('success'));
    }


    public function upComing()
    {
        $hotTickets = FootBallEvent::where('home_widget', '=', '1')
            ->orderBy('home_widget_order', 'ASC')
            ->where('is_tbc', false)
            ->whereRaw('datetime >= NOW()')
            ->get();

        $hotTicketsId = array();

        foreach ($hotTickets as $t) {
            $hotTicketsId[] = $t->id;
        }

        $games = array();

        if (!empty($hotTicketsId)) {
            $gameObj = FootBallEvent::whereRaw('datetime >= NOW()')
                ->orderBy('title', 'ASC')
                ->whereNotIn('id', $hotTicketsId);
        } else {
            $gameObj = FootBallEvent::whereRaw('datetime >= NOW()')
                ->orderBy('title', 'ASC');
        }

        $games = $gameObj->get();

        View::share('upcomings', $hotTickets);
        View::share('events', $games);

        return View::make('backend.events.upcoming-widget')
            ->with('menu', 'events/upcoming');
    }

    public function upComingWidgetUpdate()
    {
        $tickets = Input::get('tickets');
        $updateIds = array();
        if (is_array($tickets)) {
            foreach ($tickets as $ticket) {
                $id = (int)$ticket['id'];
                $order = (int)$ticket['index'];
                $updateIds[] = $id;
                DB::table('events')
                    ->where('id', $id)
                    ->update(array('home_widget' => 1, 'home_widget_order' => $order));
            }

            DB::table('events')->whereNotIn('id', $updateIds)->update(array('home_widget' => 0, 'home_widget_order' => 0));
        }
        echo json_encode(array('success'));
    }

    private function notifyBuyersDatetimeChange($event_id)
    {
        $results = \DB::table('web_user')
            ->leftJoin('events_ticket_buy as etb', 'web_user.user_id', '=', 'etb.buyer_id')
            ->leftJoin('events_related_tickets as ert', 'etb.product_id', '=', 'ert.product_id')
            ->where('ert.event_id', $event_id)
            ->where('etb.created_at', '>', '2015/05/31 11:59:59')
            ->select('web_user.*')
            ->get();

        $event = FootBallEvent::where('id', $event_id)->first();

        foreach ($results as $result) {
            $data = [];
            $data['email'] = $result->email;
            $data['name'] = $result->firstname . ' ' . $result->lastname;
            $data['newDate'] = $event->datetime;
            $data['eventTitle'] = $event->title;

            \Mail::send('emails.change-of-date', $data, function ($message) use ($result) {
                $message->from('info@footballticketpad.com', 'Football Ticket Pad');
                $message->to($result->email, $result->firstname . ' ' . $result->lastname)->subject("Change of Event Date Notification.");
            });
        }

        return true;
    }

    private function notifyBuyersDatetimeTBC($event_id)
    {
        $results = \DB::table('web_user')
            ->leftJoin('events_ticket_buy as etb', 'web_user.user_id', '=', 'etb.buyer_id')
            ->leftJoin('events_related_tickets as ert', 'etb.product_id', '=', 'ert.product_id')
            ->where('ert.event_id', $event_id)
            ->where('etb.created_at', '>', '2015/05/31 11:59:59')
            ->select('web_user.*')
            ->get();

        $event = FootBallEvent::where('id', $event_id)->first();

        foreach ($results as $result) {
            $data = [];
            $data['email'] = $result->email;
            $data['name'] = $result->firstname . ' ' . $result->lastname;
            $data['newDate'] = $event->datetime;
            $data['eventTitle'] = $event->title;

            \Mail::send('emails.tbc-date-inform', $data, function ($message) use ($result) {
                $message->from('info@footballticketpad.com', 'Football Ticket Pad');
                $message->to($result->email, $result->firstname . ' ' . $result->lastname)->subject("Change of Event Date Notification.");
            });
        }

        return true;
    }

    public function MigrateToFast($id)
    {
        return $this->MigrateTo($id, true);
    }

    public function emptyCache(){
        Cache::flush();
    }

    /**
     * @param $id
     * @return int
     *
     * Migrates $id by replicating the event
     * Translates Old year strings into new years
     * Migrates translations
     *
     */
    public function MigrateTo($id, $skip = false)
    {

        $newEvent = EventData::find($id)->replicate();
        $newEvent->season_id = 438;
        $newEvent->is_published = 0;


        /**
         * Lets Help the Migration by Auto changing the Years
         */

        //meta_content

        $newEvent->content = str_replace("2017", "2018", $newEvent->content);
        $newEvent->content = str_replace("2016", "2017", $newEvent->content);
        //meta_title
        $newEvent->meta_title = str_replace("2017", "2018", $newEvent->meta_title);
        $newEvent->meta_title = str_replace("2016", "2017", $newEvent->meta_title);
        $newEvent->meta_title = $newEvent->meta_title . "-tickets";
        //meta_description
        $newEvent->meta_description = str_replace("2017", "2018", $newEvent->meta_description);
        $newEvent->meta_description = str_replace("2016", "2017", $newEvent->meta_description);
        //meta_content
        $newEvent->meta_description = str_replace("2017", "2018", $newEvent->meta_description);
        $newEvent->meta_description = str_replace("2016", "2017", $newEvent->meta_description);
        $newEvent->ticket_type_ids = "";

        /**
         * Lets Log the Migrations
         *
         */
        $logger = new LogEventController;
        if (!empty($newEvent->title)) {
            $logger->logMigration("Migration", "Migrated <b>" . $newEvent->title . "</b> To <b>2017-18</b> Season");
        }


        /**
         * Clean up by unpublishing and renaming the Old event
         */

        $newEvent->save();
        $navigateTo = $newEvent->id;

        $oldEvent = EventData::find($id);
        $oldEvent->is_published = 0;
        $oldEvent->title = "2016_" . $oldEvent->title;
        $oldEvent->slug = "2016_" . $oldEvent->slug;
        $oldEvent->migrate = 1;
        $oldEvent->magrate_id = $newEvent->id;
        $oldEvent->save();

        /**
         * Lets Now Translate the Migration Text
         */

        if (isset($oldEvent->id)) {
            if (isset($newEvent->id)) {
                $this->MigrateTranslate($oldEvent->id, $newEvent->id);
            }
        }

        /**
         * Finish up by associating the stadium
         */
        $ex_stad = DB::table('event_stadium')
            ->where('event_id', $id)
            ->first();
        if (!empty($ex_stad)) {
            DB::table('event_stadium')
                ->insert([
                    'event_id' => $newEvent->id,
                    'stadium_id' => $ex_stad->stadium_id
                ]);

        }

        if ($skip == false) {
            return $this->edit($newEvent->id);

        } else {
            return Redirect::back()->with('message', 'Operation Successful !');
        }


    }

    /**
     * @param $id
     * @param $newId
     * @return bool
     *
     * Migrates All translations to the new Event ID
     *
     */
    public function MigrateTranslate($id, $newId)
    {

        if (!empty($id)) {
            $likeSearch = "event%" . $id;
            $allTranslatsions = DB::table('locale_text')->where('id', 'like', $likeSearch)->get();
            foreach ($allTranslatsions as $e) {

                if (!empty($e->locale)) {

                    /**
                     * Change the associated id
                     */

                    if (!empty($e->id) && !empty($newId)) {
                        $newLocale = str_replace($id, $newId, $e->id);
                    }

                    /**
                     * Fix the dates in each Translated String
                     */

                    if (!empty($e->lstring)) {
                        $e->lstring = str_replace("2016", "2017", $e->lstring);
                        $e->lstring = str_replace("2015", "2016", $e->lstring);
                    }

                    /**
                     * Save it as a new Local String
                     */

                    if (!empty($newLocale) && !empty($e->locale) && !empty($e->lstring)) {
                        DB::table('locale_text')->insert([
                            'id' => $newLocale,
                            'locale' => $e->locale,
                            'lstring' => $e->lstring
                        ]);
                    }

                }
            }
        } else {
            return false;
        }

    }

    public function migrateList($id)
    {
        $events = EventData::where('season_id', '=', $id)->paginate(25);
        return View::make('backend.events.migrate', compact('events'))
            ->with('menu', 'events')->with('season_id', $id);
    }

    public function jsonMigrateList($id)
    {
        if (!Request::ajax()) {
            $response = Response::make(array(
                    'draw' => 1,
                    'recordsTotal' => 0,
                    'recordsFiltered' => 0,
                    'data' => []
                )
            );

            $response->header('Content-Type', 'application/json');
            return $response;
        }

        $columns = array(
            0 => 'id',
            1 => 'title',
            2 => 'slug',
            3 => 'season_id',
        );
        $response = Response::make($this->getGridData('events', $columns, null, $id, 'eventsMigrate'));
        $response->header('Content-Type', 'application/json');
        return $response;
    }

    public function checkGamesListTeam($id){

        $teams = [
            ["name" => "Manchester United", "id" => "13"],
            ["name" => "Chelsea", "id" => "14"],
            ["name" => "Arsenal", "id" => "15"],
            ["name" => "Burnley", "id" => "17"],
            ["name" => "Crystal Palace", "id" => "18"],
            ["name" => "Everton", "id" => "19"],
            ["name" => "Hull City", "id" => "20"],
            ["name" => "Leicester City", "id" => "21"],
            ["name" => "Liverpool", "id" => "22"],
            ["name" => "Manchester City", "id" => "23"],
            ["name" => "Southampton", "id" => "25"],
            ["name" => "sunderland", "id" => "26"],
            ["name" => "Stoke", "id" => "27"],
            ["name" => "Swansea City", "id" => "28"],
            ["name" => "Tottenham", "id" => "29"],
            ["name" => "West brom", "id" => "30"],
            ["name" => "West Ham United", "id" => "31"],
            ["name" => "middlesbougha", "id" => "326"],
            ["name" => "bormough", "id" => "176"],
            ["name" => "Watford", "id" => "175"],
        ];
        $content = "";
        foreach($teams as $a){
            $team13 = EventData::where('home_team_id','=',$id)->where('tournament_id','=','7')->where('season_id','=','362')->where('away_team_id','=',$a['id'])->first();
            if($team13 === null){
                $content .= "Vs ".$a['name']." <div style='color : red !important;'> NO GAME</div><br>";


            }else{
                $content .= "Vs ".$a['name']." <div style='color : green !important;'>  GAME</div><br>";
            }
        }
        return View::make('backend.log.blank')
            ->with('content',$content);


    }

    public function checkGamesList(){

        $teams = [
            ["name" => "Manchester United", "id" => "13"],
            ["name" => "Chelsea", "id" => "14"],
            ["name" => "Arsenal", "id" => "15"],
            ["name" => "Burnley", "id" => "17"],
            ["name" => "Crystal Palace", "id" => "18"],
            ["name" => "Everton", "id" => "19"],
            ["name" => "Hull City", "id" => "20"],
            ["name" => "Leicester City", "id" => "21"],
            ["name" => "Liverpool", "id" => "22"],
            ["name" => "Manchester City", "id" => "23"],
            ["name" => "Southampton", "id" => "25"],
            ["name" => "sunderland", "id" => "26"],
            ["name" => "Stoke", "id" => "27"],
            ["name" => "Swansea City", "id" => "28"],
            ["name" => "Tottenham", "id" => "29"],
            ["name" => "West brom", "id" => "30"],
            ["name" => "West Ham United", "id" => "31"],
            ["name" => "middlesbougha", "id" => "326"],
            ["name" => "bormough", "id" => "176"],
            ["name" => "Watford", "id" => "175"],
        ];
        $content = "";
        foreach($teams as $a){
            $content .= "<a href='/admin/hiddentools/checkgames/".$a['id']."'>".$a['name']."</a><br><br>";
        }


        return View::make('backend.log.blank')
            ->with('content',$content);

    }
}
