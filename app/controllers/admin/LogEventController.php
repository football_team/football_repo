<?php namespace App\Controllers\Admin;

use BaseController;
use LogEvent;
use Sentry;
use Redirect;
use Translate;
use User;
use View;
use Input;
use Pages;
use Template;
use DB;
use PagesMeta;
use Validator;
use Response;
use Request;
use Notification;
use Bond\Traits\GridPagination;
use Bond\Repositories\Page\PageRepository as Page;
use Bond\Exceptions\Validation\ValidationException;

class LogEventController extends BaseController
{

    /**
     * @return mixed
     */
    public function translatorLog()
    {
        return $this->buildView('Translator', "Translator Log");
    }

    public function eventlog()
    {
        return $this->buildView('Events', "Event Log");
    }

    public function migrateLog(){

        return $this->buildView('MIGRATION', "Migration Log");

    }

    public function buildView($modual, $title)
    {

        $translatroLof = LogEvent::where('modual', '=', $modual)
            ->orderBy('id', 'desc')
            ->paginate(15);
        $transdata = [];
        $i = 0;
        foreach ($translatroLof as $key => $row) {
            $i++;
            $transdata[$i] = $row;
            $id = $row->user;
            if (!empty($id) || $id = 0) {
                $userx = Sentry::findUserById($id);
                $transdata[$i]['user'] = $userx->first_name;

            }else{
                $transdata[$i]['user'] = "SU";
            }

        }

        return View::make("backend.logview.show")->with('transalate', $translatroLof)->with('h1', $title);

    }

    /**
     * @param $action
     * @param $note
     *
     * LOG THE TRANSLATION CHANGES
     *
     */
    public function logTranslator($action, $note)
    {
        $model = "Translator";
        if (!empty($action)) {
            if (!empty($note)) {
                $this->genericLog($action, $note, $model);
            }
        }
    }


    /**
     * @param $action
     * @param $note
     *
     * LOG EVENT CHANGES
     *
     */
    public function logEvents($action, $note)
    {

        $model = "Events";
        if (!empty($action)) {
            if (!empty($note)) {
                $this->genericLog($action, $note, $model);
            }
        }
    }


    /**
     * @param $action
     * @param $note
     *
     * LOG MIGRATION CHANGES
     *
     */
    public function logMigration($action, $note)
    {

        $model = "MIGRATION";
        if (!empty($action)) {
            if (!empty($note)) {
                $this->genericLog($action, $note, $model);
            }
        }
    }

    /**
     * @param $action
     * @param $note
     * @param $model
     *
     * LOG Generic CHANGES
     *
     */
    public function genericLog($action, $note, $model)
    {

        if (!empty($action) && !empty($note)) {
            $user = Sentry::getUser();
            if (!empty($user)) {
                $userid = $user->id;
            } else {
                $userid = 0;
            }

            $log = new LogEvent;
            $log->modual = $model;
            $log->user = $userid;
            $log->action = $action;

            $log->note = $note;
            $log->save();
        }

    }

}