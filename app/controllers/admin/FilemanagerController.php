<?php namespace App\Controllers\Admin;

use BaseController;
use Bond\Filemanager\Filemanager;
use Redirect;
use Sentry;
use View;
use Input;
use Validator;
use User;
use Notification;

class FilemanagerController extends BaseController
{

    public function index()
    {
        $data = array('admin_url' => "");
        return View::make('backend.plugins.filemanager', $data);
    }

    public function showInAdmin()
    {
        return View::make('backend.plugins.in-admin')->with('menu', 'filemanager');
    }

    public function listFiles()
    {

    }

    public function getinfo()
    {

        $fm = new Filemanager();


        switch ($_REQUEST['mode']) {

            case 'getinfo':

                if ($fm->getvar('path')) {
                    $response = $fm->getinfo();
                }
                break;

            case 'getfolder':

                if ($fm->getvar('path')) {
                    $response = $fm->getfolder();
                }
                break;

            case 'rename':

                if ($fm->getvar('old') && $fm->getvar('new')) {
                    $response = $fm->rename();
                }
                break;

            case 'delete':

                if ($fm->getvar('path')) {
                    $response = $fm->delete();
                }
                break;

            case 'addfolder':

                if ($fm->getvar('path') && $fm->getvar('name')) {
                    $response = $fm->addfolder();
                }
                break;

            case 'download':
                if ($fm->getvar('path')) {
                    $fm->download();
                }
                break;

            case 'preview':
                return $fm->getvar('path');
                break;

            case 'maxuploadfilesize':
                $fm->getMaxUploadFileSize();
                break;

            case 'add':
                if ($fm->postvar('currentpath')) {
                     echo $fm->add();
                }
                break;
            default:

                $fm->error($fm->lang('MODE_ERROR'));
                break;

        }

        return $response;
    }

}