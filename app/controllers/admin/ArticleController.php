<?php namespace App\Controllers\Admin;

use BaseController;
use Redirect;
use View;
use Input;
use Response;
use Tag;
use Str;
use Notification;
use Bond\Repositories\Article\ArticleRepository as Article;
use Bond\Repositories\Category\CategoryRepository as Category;
use Bond\Exceptions\Validation\ValidationException;

class ArticleController extends BaseController {

    protected $article;
    protected $category;

    public function __construct(Article $article, Category $category) {

        View::share('active', 'blog');
        $this->article = $article;
        $this->category = $category;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {

        $articles = $this->article->paginate(null, true);
        return View::make('backend.article.index', compact('articles'))
            ->with('menu', 'article');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {

        $categories = $this->category->lists();

        $teams = \FootballTickets::where('type', 'club')->get();
        View::share('teams', $teams);

        $leagues = \FootballTickets::where('type', 'league')->get();
        View::share('leagues', $leagues);

        $events = \FootBallEvent::whereRaw('datetime >='. \Carbon\Carbon::today()->toDateString())->get();
        View::share('events', $events);

        return View::make('backend.article.create', compact('categories'))
            ->with('menu', 'article/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store() {

        try {
            $article = $this->article->create(Input::all());

            if(Input::has('clubtags'))
            {
                $clubtags = Input::get('clubtags');
                foreach($clubtags as $clubname)
                {
                    $club = \FootballTickets::where('type','club')->where('title', $clubname)->first();
                    if($club)
                    {
                        \DB::table('article_fbt_assoc')
                            ->insert([
                                'articles_id'=>$article->id,
                                'fbt_id'=>$club->id,
                                'type'=>'club'
                            ]);
                    }
                }
            }

            if(Input::has('leaguetags'))
            {
                $leaguetags = Input::get('leaguetags');
                foreach($leaguetags as $leaguename)
                {
                    $league = \FootballTickets::where('type','league')->where('title', $leaguename)->first();
                    if($league)
                    {
                        \DB::table('article_fbt_assoc')
                            ->insert([
                                'articles_id'=>$article->id,
                                'fbt_id'=>$league->id,
                                'type'=>'league'
                            ]);
                    }

                }
            }

            if(Input::has('eventtags'))
            {
                $eventtags = Input::get('eventtags');
                foreach($eventtags as $eventname)
                {
                    $event = \FootBallEvent::where('title', $eventname)->first();
                    if($event)
                    {
                        \DB::table('article_event_assoc')
                            ->insert([
                                'articles_id'=>$article->id,
                                'event_id'=>$event->id
                            ]);
                    }

                }
            }

            Notification::success('Article was successfully added');
            return Redirect::route('admin.article.index');
        } catch (ValidationException $e) {
            return Redirect::back()->withInput()->withErrors($e->getErrors());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id) {

        $article = $this->article->find($id);
        return View::make('backend.article.show', compact('article'))
            ;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id) {

        $article = $this->article->find($id);
        $tags = null;

        foreach ($article->tags as $tag) {
            $tags .= ',' . $tag->name;
        }

        $tags = substr($tags, 1);
        $categories = $this->category->lists();

        $fbt_ids = \DB::table('article_fbt_assoc')->where('articles_id', $id)->select('fbt_id')->get();

        $fbt_id_array = [];
        foreach($fbt_ids as $fbt)
        {
            $fbt_id_array[] = $fbt->fbt_id;
        }

        $event_ids = \DB::table('article_event_assoc')->where('articles_id', $id)->select('event_id')->get();
        $event_id_array = [];
        foreach($event_ids as $event)
        {
            $event_id_array[] = $event->event_id;
        }

        $takenClubs = [];
        $takenLeague = [];
        $takenEvents = [];
        $clubs = [];
        $leagues = [];
        $events = [];

        if(!empty($fbt_id_array))
        {
            $takenClubs = \FootballTickets::whereIn('id', $fbt_id_array)->where('type', 'club')->get();
            $takenLeague = \FootballTickets::whereIn('id',$fbt_id_array)->where('type','league')->get();
            $clubs = \FootballTickets::where('type','club')->whereNotIn('id', $fbt_id_array)->get();
            $leagues = \FootballTickets::where('type', 'league')->whereNotIn('id', $fbt_id_array)->get();
        }
        else
        {
            $clubs = \FootballTickets::where('type','club')->get();
            $leagues = \FootballTickets::where('type', 'league')->get();
        }
        if(!empty($event_id_array))
        {
            $takenEvents = \DB::table('events')->whereRaw('datetime >= '. \Carbon\Carbon::today()->toDateString())->whereIn('id',$event_id_array)->get();
            $events = \DB::table('events')->whereRaw('datetime >='. \Carbon\Carbon::today()->toDateString())->whereNotIn('id',$event_id_array)->get();
        }
        else
        {
            $events = \DB::table('events')->whereRaw('datetime >='. \Carbon\Carbon::today()->toDateString())->get();
        }

        View::share('teams', $clubs);
        View::share('leagues', $leagues);
        View::share('events', $events);

        View::share('takenTeams', $takenClubs);
        View::share('takenLeagues', $takenLeague);
        View::share('takenEvents', $takenEvents);

        return View::make('backend.article.edit', compact('article', 'tags', 'categories'))
            ->with('menu', 'article/edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id) {

        try {
            $article=$this->article->update($id, Input::all());

            \DB::table('article_fbt_assoc')
                    ->where('articles_id', $id)
                    ->delete();

            \DB::table('article_event_assoc')
                    ->where('articles_id', $id)
                    ->delete();
            if(Input::has('clubtags'))
            {
                $clubtags = Input::get('clubtags');

                foreach($clubtags as $clubname)
                {
                    $club = \FootballTickets::where('type','club')->where('title', $clubname)->first();
                    if($club)
                    {
                        $res = \DB::table('article_fbt_assoc')->where('articles_id', $id)->where('fbt_id',$club->id);
                        \DB::table('article_fbt_assoc')
                            ->insert([
                                'articles_id'=>$article->id,
                                'fbt_id'=>$club->id,
                                'type'=>'club'
                            ]);
                    }
                }
            }

            if(Input::has('leaguetags'))
            {
                $leaguetags = Input::get('leaguetags');
                foreach($leaguetags as $leaguename)
                {
                    $league = \FootballTickets::where('type','league')->where('title', $leaguename)->first();
                    if($league)
                    {
                        \DB::table('article_fbt_assoc')
                            ->insert([
                                'articles_id'=>$article->id,
                                'fbt_id'=>$league->id,
                                'type'=>'league'
                            ]);
                    }

                }
            }

            if(Input::has('eventtags'))
            {
                $eventtags = Input::get('eventtags');
                foreach($eventtags as $eventname)
                {
                    $event = \FootBallEvent::where('title', $eventname)->first();
                    if($event)
                    {
                        \DB::table('article_event_assoc')
                            ->insert([
                                'articles_id'=>$article->id,
                                'event_id'=>$event->id
                            ]);
                    }

                }
            }

            Notification::success('Article was successfully updated');
            return Redirect::route('admin.article.index');
        } catch (ValidationException $e) {

            return Redirect::back()->withInput()->withErrors($e->getErrors());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id) {

        $this->article->destroy($id);

        \DB::table('article_fbt_assoc')
            ->where('articles_id', $id)
            ->delete();

        \DB::table('article_event_assoc')
            ->where('articles_id', $id)
            ->delete();

        Notification::success('Article was successfully deleted');
        return Redirect::action('App\Controllers\Admin\ArticleController@index');
    }

    public function confirmDestroy($id) {

        $article = $this->article->find($id);
        return View::make('backend.article.confirm-destroy', compact('article'))
            ->with('menu', 'article/edit');
    }

    public function togglePublish($id) {

        return $this->article->togglePublish($id);
    }
}
