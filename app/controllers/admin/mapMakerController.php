<?php

namespace App\Controllers\Admin;


use BaseController;
use Map;
use File;
use MapC;
use MapMeta;
use MapModel;
use Sentry;
use Redirect;
use View;
use Input;
use Template;
use DB;
use Validator;
use Response;
use Request;
use Notification;
use Leafo\ScssPhp\Compiler;
use Leafo\ScssPhp\Server;
use Leafo\ScssPhp\Formatter\Expanded;
use Leafo\ScssPhp\Formatter\Nested;
use Leafo\ScssPhp\Formatter\Compressed;
use Leafo\ScssPhp\Formatter\Compact;
use Leafo\ScssPhp\Formatter\Crunched;
use Cache;

class MapMakerController extends BaseController
{

    /**
     * @return View
     *
     * Note: Returns a List of all of the Maps
     * Currently Made in the CMS
     *
     */
    public function selectMap()
    {
        $allMaps = Map::get();
        return View::make("backend.map.show")->with('maps', $allMaps);

    }

    /**
     * @return View
     *
     * Note : Allows The user to make a new map from an SVG
     *
     */
    public function createMap()
    {

        return View::make("backend.map.create");

    }

    /**
     * @return View
     *
     * Note : Saves the map based on user input
     *
     */
    public function saveMap()
    {

        $data = Input::all();
        $newMap = Map::create($data);
        $this->mapCompile($newMap->id);
        return $this->selectMap();

    }

    /**
     * @param $id
     * @return View
     *
     * Note : Turns a map on or off to be selected from the drop down list
     *
     */
    public function enableMap($id)
    {

        $map = Map::find($id);
        $map->status = "1";
        $map->save();

        return $this->selectMap();
    }


    /**
     * @return mixed
     *
     * Note : Shows the list so you can remove a map
     *
     */
    public function removeList()
    {

        $allMaps = Map::get();
        return View::make("backend.map.removeShow")->with('maps', $allMaps);


    }

    public function removeThis($id)
    {

        /**  Get Associated Map Meta **/
        $MapObject = MapModel::where('map_id', '=', $id)->get();
        if (!empty($MapObject)) {
            foreach ($MapObject as $x) {

                $currentMapObjectMeta = MapMeta::where('map_object_id', '=', $x->id)->get();

                if (!empty($currentMapObjectMeta)) {
                    foreach ($currentMapObjectMeta as $y) {

                        $y->delete();

                    }
                }

                $x->delete();

            }
        }

        $map = Map::where('id', '=', $id)->first();

        if (!empty($map)) {
            $map->delete();
        }

        return $this->selectMap();

    }

    /**
     * @param $id
     * @return View
     *
     * Note : Disables a Map in the backend
     *
     */
    public function disableMap($id)
    {

        $map = Map::find($id);
        $map->status = "0";
        $map->save();

        return $this->selectMap();
    }

    public function killblock($id)
    {

        $mapobject = MapModel::find($id);
        $mapobject->status = 0;
        $mapobject->save();
        return 1;

    }

    public function source($id)
    {


        $map = Map::find($id);

        /**
         * Lets Compile the SVG Settings
         */
        $attributes = MapModel::where('map_id', '=', $id)->where('node', '=', '@attributes')->where('status', '=', '1')->first();
        $attributesData = MapMeta::where('map_object_id', '=', $attributes->id)->get();
        $attributeData = "";
        foreach ($attributesData as $x) {
            $attributeData .= " " . $x->key . "='" . $x->value . "' ";
        }


        /**
         * Lets now Compile the SVG
         */
        if (!empty($attributeData)) {
            $composedMap = "<svg style='width:100%; height:100% !important;' " . $attributeData . ">";
        } else {
            $composedMap = "<svg>";
        }

        $mapObjects = MapModel::where('map_id', '=', $id)->where('node', '!=', '@attributes')->where('status', '=', '1')->orderBy('id', 'desc')->get();

        $inputArray = array_merge([''], array_merge(range('0', '9'), range('A', 'Z')));

        $pulldataToStart = "";
        $pushDataToEnd = "";
        $textString = "";
        $textStringFlag = false;

        foreach ($mapObjects as $y => $x) {


            $poss = false;
            $currentlLoop = "";
            $currentlLoop .= "<" . $x->node;
            $mapMeta = MapMeta::where('map_object_id', '=', $x->id)->get();
            foreach ($mapMeta as $p => $b) {
                $currentlLoop .= " " . $b->key . "='" . $b->value . "'";
                if ($b->key == "class") {
                    $currentlLoop .= " data-item-id='" . $b->id . "' ";
                    $currentlLoop .= " core-item-id='" . $x->id . "' ";
                }
                if (($b->key == "fill" && $b->value == "#FFFFFF")) {
                    $poss = true;
                }


            }
            $currentlLoop .= " />\n";

            if ($poss == true) {
                $pulldataToStart .= $currentlLoop;
            } else {
                if ($textStringFlag == false) {
                    $pushDataToEnd .= $currentlLoop;
                } else {
                    $textString .= $currentlLoop;
                }
            }

        }

        /**
         * Ok finaly,  I know I know WERE DOING LOTs
         * Lets Build the list of *OTHER* Categorys...
         */

        $listOfOther = MapC::select('id', 'left', 'right')->groupBy('right')->get();


        $composedMap .= $pushDataToEnd . $pulldataToStart . $textString;
        $composedMap .= "</svg>";
       return $composedMap;

    }

    public function mapBuilder($id)
    {


        $map = Map::find($id);

        /**
         * Lets Compile the SVG Settings
         */
        $attributes = MapModel::where('map_id', '=', $id)->where('node', '=', '@attributes')->where('status', '=', '1')->first();
        $attributesData = MapMeta::where('map_object_id', '=', $attributes->id)->get();
        $attributeData = "";
        foreach ($attributesData as $x) {
            $attributeData .= " " . $x->key . "='" . $x->value . "' ";
        }


        /**
         * Lets now Compile the SVG
         */
        if (!empty($attributeData)) {
            $composedMap = "<svg style='width:100%; height:100% !important;' " . $attributeData . ">";
        } else {
            $composedMap = "<svg>";
        }

        $mapObjects = MapModel::where('map_id', '=', $id)->where('node', '!=', '@attributes')->where('status', '=', '1')->orderBy('id', 'desc')->get();

        $inputArray = array_merge([''], array_merge(range('0', '9'), range('A', 'Z')));

        $pulldataToStart = "";
        $pushDataToEnd = "";
        $textString = "";
        $textStringFlag = false;

        foreach ($mapObjects as $y => $x) {


            $afterLoopText = '';
            $poss = false;
            $currentlLoop = "";
            $currentlLoop .= "<" . $x->node;
            $mapMeta = MapMeta::where('map_object_id', '=', $x->id)->get();
            foreach ($mapMeta as $p => $b) {
                $currentlLoop .= " " . $b->key . "='" . $b->value . "'";
                if ($b->key == "class") {
                    $currentlLoop .= " data-item-id='" . $b->id . "' ";
                    $currentlLoop .= " core-item-id='" . $x->id . "' ";
                }
                if (($b->key == "fill" && $b->value == "#FFFFFF")) {
                    $poss = true;
                }

                if($x->node =="text"){
                    if ($b->key == "text") {
                        $afterLoopText = $b->value;
                    }
                }else{
                    $afterLoopText = "";
                }

            }


            if($afterLoopText != ""){
                $currentlLoop .= " >";
                $poss = true;
                $currentlLoop .= $afterLoopText."</text>";
            }else {
                $currentlLoop .= " />";
            }

            $currentlLoop .= "\n";
            if ($poss == true) {
                $pulldataToStart .= $currentlLoop;
            } else {
                if ($textStringFlag == false) {
                    $pushDataToEnd .= $currentlLoop;
                } else {
                    $textString .= $currentlLoop;
                }
            }

        }

        /**
         * Ok finaly,  I know I know WERE DOING LOTs
         * Lets Build the list of *OTHER* Categorys...
         */

        $listOfOther = MapC::select('id', 'left', 'right')->groupBy('right')->get();


        $composedMap .= $pushDataToEnd . $pulldataToStart . $textString;
        $composedMap .= "</svg>";
        return View::make("backend.map.builder")->with('map', $map)->with('buildpam', $composedMap)->with('seats', $inputArray)->with('other', $listOfOther)->with('id',$id );

    }

    public function savetext($t,$x,$y,$s,$r,$id)
    {
        $t = preg_replace("/[^A-Za-z0-9 ]/", "", $t);
        $x = (int) $x;
        $y =(int) $y;
        $s = preg_replace("/[^0-9 ]/", "", $s);
        $r = preg_replace("/[^0-9 ]/", "", $r);
        $id = preg_replace("/[^0-9 ]/", "", $id);

        $model = new MapModel;
        $model->node = 'text';
        $model->map_id = $id;
        $model->save();
        $modelid = $model->id;

        $class = new MapMeta();
        $class->map_object_id = $modelid;
        $class->key = "style";
        $class->value = "-webkit-transform: rotate(".$r."deg); -moz-transform: rotate(".$r."deg); -o-transform: rotate(".$r."deg); -ms-transform: rotate(".$r."deg); transform: rotate(".$r."deg);";
        $class->save();

        $class = new MapMeta();
        $class->map_object_id = $modelid;
        $class->key = "x";
        $class->value = $x;
        $class->save();

        $class = new MapMeta();
        $class->map_object_id = $modelid;
        $class->key = "class";
        $class->value = 'maptext';
        $class->save();

        $class = new MapMeta();
        $class->map_object_id = $modelid;
        $class->key = "y";
        $class->value = $y;
        $class->save();

        $class = new MapMeta();
        $class->map_object_id = $modelid;
        $class->key = "font-size";
        $class->value = $s;
        $class->save();


        $class = new MapMeta();
        $class->map_object_id = $modelid;
        $class->key = "text";
        $class->value = $t;
        $class->save();
    }

    public function savepitch($w,$h,$x,$y,$id){

        $model = new MapModel;
        $model->node = 'image';
        $model->map_id = $id;
        $model->save();
        $modelid = $model->id;

        $class = new MapMeta();
        $class->map_object_id = $modelid;
        $class->key = "class";
        $class->value = "pitchimg";
        $class->save();

        $width = new MapMeta();
        $width->map_object_id = $modelid;
        $width->key = "width";
        $width->value = $w;
        $width->save();

        $height = new MapMeta();
        $height->map_object_id = $modelid;
        $height->key = "height";
        $height->value = $h;
        $height->save();

        $xval = new MapMeta();
        $xval->map_object_id = $modelid;
        $xval->key = "x";
        $xval->value = $x;
        $xval->save();

        $yval = new MapMeta();
        $yval->map_object_id = $modelid;
        $yval->key = "y";
        $yval->value = $y;
        $yval->save();


        $img = new MapMeta();
        $img->map_object_id = $modelid;
        $img->key = "xlink:href";
        $img->value = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAu4AAAH/CAMAAAA/oahbAAAAM1BMVEX///+4zpT////t8uTJ2q7b5sn6+/i80ZrR37rk7NbF16jA1KHx9erX48L2+PHf6dDo790ODiEaAAAAAXRSTlO/Gzh2ewAAEFdJREFUeNrs0sFJAwEARUHdjQEl9t+u4CHnd1rCz0wN83HA29CdN6I7b+S/++PkQvfjuJ9c7PHsfvvkQudxnJ9c7KZ7pPsA3SvdB+he6T5A90r3AbpXug/QvdJ9gO6V7gN0r3QfoHul+wDdK90H6F7pPkD3SvcBule6D9C90n2A7pXuA3SvdB+ge6X7AN0r3QfoXuk+QPdK9wG6V7oP0L3SfYDule4DdK90H6B7pfsA3SvdB+he6T5A90r3AbpXug/QvdJ9gO6V7gN0r3QfoHul+wDdK90H6F7pPkD3SvcBule6D9C90n2A7pXuA3SvdB+ge6X7AN0r3QfoXuk+QPdK9wG6V7oP0L3SfYDule4DdK90H6B7pfsA3SvdB+he6T5A90r3AbpXug/QvdJ9gO6V7gN0r3QfoHul+wDdK90H6F7pPkD3SvcBule6D9C90n2A7pXuA3SvdB+ge6X7AN0r3QfoXuk+QPdK9wG6V7oP0L3SfYDule4DdK90H6B7pfsA3SvdB+he6T5A90r37Of2qn6f3b+/XtUf+3VshEAUgFBQvdHI/uu1ABIyuT/7algCjhziydyv13SP13Tv54Hh/qdwb8L9LO7fa7IP7rfrmn2C81f1wv124Y477gPh3oc77hnuuK9qwr0Pd9wz3HFf1YR7H+64Z7jjvqoJ9z7ccc9wx31VE+59uOOe4Y77qibc+3DHPcMd91VNuPfhjnuGO+6rmnDvwx33DHfcVzXh3oc77hnuuK9qwr0Pd9wz3HFf1YR7H+64Z7jjvqoJ9z7ccc9wx31VE+59uOOe4Y77qibc+3DHPcMd91VNuPfhjnuGO+6rmnDvwx33DHfcVzXh3oc77hnuuK9qwr0Pd9wz3HFf1YR7H+64Z7jjvqoJ9z7ccc9wx31VE+59uOOe4Y77qibc+3DHPcMd91VNuPfhjnuGO+6rmnDvwx33DHfcVzXh3oc77hnuuK9qwr0Pd9wz3HFf1YR7H+64Z7jjvqoJ9z7ccc9wx31VE+59uOOe4Y77qibc+3DHPcMd91VNuPfhjnuGO+6rmnDvwx33DHfcVzXh3oc77hnuuK9qwr0Pd9wz3HFf1YR7H+64Z7jjvqoJ9z7ccc9wx31VE+59uOOe4Y77qibc+3DHPcP9x84d7rYOwmAYVrCBAKHN/V/tOSVKNima5jXJFpvv/T2tqvSUGkoL7nfVBO7ywB3c94E7uN9VE7jLA3dw3wfu4H5XTeAuD9zBfR+4g/tlmiIfqm7ckz9SGbbAHdwv487uYI374XjYAndwB3dwB3dwB3d7Xcw9nTG78y2fHrgr7FLuhzwwuIsD97/3AO46A3dwB3cTgTu4vypc/UwU3Fogmn3lMpgK3Lvnnmsi93WUah6sBO5dc8+VnCQyQh7c++U+psdn0eT9yP+bnJuYefSeyH30SOOgPnDvlDunsEGePZcvtqqF/by9KEK66W29O3gA99sW/eNjzS7fnsyUMW1/7+OgOHDvj3tObmmqWXwQmevklpLiMR7ce+POtK7T5Yfn7mV9TyC1Mw2498V9xZ74rY+ZOOkGD+49cc8L9uDL25+qFh8W8CpHGnDvh3tMyxRT46FLBLEuM01SuGkF926419BW9iq5MyP9V9oC9064Z2pCfTzlilj0QeVEA+59cK/u1VxOuxFZZvdK2QIP7j1wL9SGdj71AjC3EZ5U3ZkE9w64j23yeMaT77vHZ5uPNF2lAXf73J+7pV3CXb7APwc1gbt17pHa1B4v+TZTbBM8qTmSBHfj3PO021FKucv3wJOWExpwt809h9d4nYcD3EWPoMQ7uJvm3japU7z0q9ll0rNhBXfL3EfB2C7gLhrgVXgHd8Pcm/b0Cz+8kbR4B3e73Pfa5dyNegd3s9xX7W9xN+od3K1yzwLtMu5y77c/nwH3f+zdQZKDMAxE0VJCMGAG5/6nnZK4A/7pau2zcb0ISbZBlPuxlvbHuMf+C/NIc9fk3m7tD3K/vcP3V81dkns7X4n2Se71w9fJ9m7uktz3kvcw9/qP7UEOc1fk/pd1RY+HuUdf6Rc+zF2Q+3Gv2uPcY6GPZ8xdj3u7KslO4F6PlQtcvpu7HveRB2ViCvf4sq97mLsc960GgpO4txW9u2ruatwL3BKTuMeCnr6buxr3UeXENO4xyOWMuYtxX6pZnMi9ni7UN6aauxj3s2rnidyrdziDGeauxT2tvWMq93hzu1Vzl+JelUSfzL1zu1Vzl+L+yTWf/tXs8QIujbnLcW9r5tXp3NtKTe/mrsT9Tu7TuceHmt7NXYj7nVUB3LHp3dyFuN9JFcAdm97NXYj7lTkVwT3T+xW8MHcd7ltt3yO4x2DO3s1dh/uZM3cI917bXbgwdxnuRx1zh3CPL/Jek7nLcK/6AcN9Qx6MNHcZ7tUdYrhX3xy0MHcV7pVOQdyRzaq5q3Dfs1gGcT+IL50xdxXua54yB3FHVjPmLsI9l2uguA/grSZzF+GeuA4U9wM4mzF3Ee5nzmVQ3GPlXeIzdw3uPRtDGPe9dnlRYe4a3Lcc+8G4b7xRpLlrcB+ZSWHcO694N3cN7lW6w7jHhSvezV2Dex4Pw3HPY2KBCnOX4F5rjeP+wU3ezV2C+5bLhOO+4HpVc5fgnnm04bg33I1Vc5fg/s4qGcf9n71723EUBoIAujaJbbBJ8v9fu9tNZp9RJuCiXP0+I4FOrKJ9C3BbmsSdgnuLcQLkPqG1ZsSdgrs1ZgC5w7VmxJ2Bu4dkQO72SQF1upK4M3C/AXOH6kSKOwv3GyD3m7iL+7tA2+4bd9LGu7gzcL8Dc0d5R+JOxb0Ccq/iLu5W3+ceALkHcRd3K3HfWeKO8XjiLu7ivrPEfWeJO8bjibu4i/vOEvedJe4Yj4fGXY1IcR+IO+o0k2ZVxf1fjbKIQGtmxD1oidjuEneEx0PjjroAGOrYPHFn4I68vSMglbgzcAfevId1o4G4U3DH3ZqtkwjEfashDt7Aup5J3Cm4+zchHHe4WSZx5+COe2geVB9S3Dm44x6JCnXuhrhzcIc98PoRoErcObi3GBMc92S/QagSdw7uqJfVrAGqxJ2DO+pVZEuAKnHn4F4tvINxh5tTFXcW7h7ewbjjRXdxZ+GOeUk81N4Ocefhbq/rCcX9ibb6V9x5uFt0eEBxx7tVVdx5uM+WZoC4L3htSHHn4V4szQBxR8wy4k7D3dJMAuKeALOMuPNwt+G0wHAvgH0ZcSfibmF5guE+xZiwVkOKOxV339JUQbhXuI1M4k7GvRgxEO4z3noZcefibp3uWCG4V7jr4cWdjvvd3jgEd7gLVcWdj3u2XmQG4J4T3D4mcafjvg3vANzvkF1IcSfjnpOl9+7cawQd3MWdirsPq3N37jPq4C7uXNx9eL915n4DbcuIOxt37723ztwbaFtG3Om4u7W1K/cVc0JV3Bm522tLtSP3miBXy4g7JXdfGDl15D4hbusQd1buOZm3btxX3O9UcSfkHorHmU7cF4sycJuYxJ2Xu8eZ1ol7Q44y4s7IPT9s22oX7jN0lBF3Ru6+rymWDtxLBO7KiDsp97Aau+V07kvCnWASd17uHipSPpl7TdjBXdxZuedmn6v5VO65AU+nijsz95CTez+Ru2tvyMFd3Gm5bzl6Po+756cHuHZxZ+W+tWfms7i79gR49oC4j8E9FPf+GXdS7eLOy/1D786dVDs49z/i/mvvLR/OPbeLaBd3Zu4feXfupNrFnZp7KMklHsp9SZfRLu7c3N8Wy4Hci3f4r6Fd3Mm5h6XZK3wexv0ZLzC7JO6jcA95cpD1EO61RfyVA+I+EPf3AJzKAdxLivCrwsR9MO5vla/8Ze75Zf/2cZHYLu6jcA/VA01av8p93X5EV4nt4j4Md9vwYTUtX+O+bL8gzKMgxd1qYO7G02quX+Fe52g14Z45IO5hZO4/4SPd86+553u64tAu7iNxD3mOO8A79x3Y4/NSqV3cR+P+P9GkuX7Mvc4b9tfVcoy4D8c9hNsUvabyEffy8+fQ5w38Ze9uViqGgQAKQ/5o0ibt+z+t1epNYVDHW5GJPWfTTZcfIQmBgftbcD+BT2v5IfeyprGxw/1+3He22R21tai5l7W5ozzkNgbud+X+etr8sJvyNH/LfZ5ycketDnhAhfu9ue+Fh2CXthjmT7jPIW79xzzSgwG4w/3clJvreR/jFPYW55b9M8XoF9drebxrdrjD/Vyp3ilKWx19XYc73N/J56/M+/xPqMMd7n2PXuPmfeoLuvdbrGHgWxi4w12Rtz2RwDL3HJ6uPrjn+HQN7nD/K+7XOrhfDO5whzvc4f7LFX+pRbGZ0XQ6bMEd7laH6YSDu91pP3AfL7jDHe4Ggrs+uMNdBne4W9UEd31wh7sM7nC3qgnu+uAOdxnc4W5VE9z1wR3uMrjD3aomuOuDO9xlcIe7VU1w1wd3uMvgDnermuCuD+5wl8Ed7lY1wV0f3OEugzvcrWqCuz64w10Gd7hb1QR3fXCHuwzucLeqCe764A53GdzhblUT3PXBHe4yuMPdqia464M73GVwf2GXDgkAAAAQAP1/7QCL0QAb0P11k+473XVvuuv+ukn3ne66N911f92k+0533Zvuur9u0n2nu+5Nd91fN+m+0133prvur5t03+mue9Nd99dNuu90173prvvrJt13uuvedNf9dZPuO911b7rr/rpJ953uujfddX/dpPtOd92b7rq/btJ9p7vuTXfdXzfpvtNd96a77q+bdN/prnvTXffXTbrvdNe96a776ybdd7rr3nTX/XWT7jvddW+66/66Sfed7ro33XV/3aT7Tnfdm+66v27Sfae77k133V83hR17x2koCqIgKH4BILP/5eIEkpe0HF0fVe9gpApmBvce7rhfwx33UzXh3sMd92u4436qJtx7uON+DXfcT9WEew933K/hjvupmnDv4Y77NdxxP1UT7j3ccb+GO+6nasK9hzvu13DH/VRNuPdw77Pd3o/s9s/98+PIfnB/ut5eT+7O/ehwf7Jwfzzcny7cHw/3p+vr/dT+dvdjj4t73y+DLXM/t+M/M6vhXsJ9JNxLuI+Eewn3kXAv4T4S7iXcR8K9hPtIuJdwHwn3Eu4j4V7CfSTcS7iPhHsJ95FwL+E+Eu4l3EfCvYT7SLiXcB8J9xLuI+Fewn0k3Eu4j4R7CfeRcC/hPhLuJdxHwr2E+0i4l3AfCfcS7iPhXsJ9JNxLuI+Eewn3kXAv4T4S7iXcR8K9hPtIuJdwHwn3Eu4j4V7CfSTcS7iPhHsJ95FwL+E+Eu4l3EfCvYT7SLiXcB8J9xLuI+Fewn0k3Eu4j4R7CfeRcC/hPhLuJdxHwr2E+0i4l3AfCfcS7iPhXsJ9JNxLuI+Eewn3kXAv4T4S7iXcR8K9hPtIuJdwHwn3Eu4j4V7CfSTcS7iPhHsJ95FwL+E+Eu4l3H/bpWMCAGAYCEL1r7oWbv6ABkboXug+QvdC9xG6F7qP0L3QfYTuhe4jdC90H6F7ofsI3QvdR+he6D5C90L3EboXuo/QvdB9hO6F7iN0L3QfoXuh+wjdC91H6F7oPkL3QvcRuhe6j9A96np3uEJ3DtGdQ3TnkPcBpQ1zpEjytzkAAAAASUVORK5CYII=";
        $img->save();


    }

    public function mapBuilderFrontEnd($id)
    {

        if (Cache::has("map_build_" . $id)) {

            return Cache::get("map_build_" . $id);

        } else {


            $map = Map::find($id);

            /**
             * Lets Compile the SVG Settings
             */
            if (!empty($id)) {
                $attributes = MapModel::where('map_id', '=', $id)->where('node', '=', '@attributes')->where('status', '=', '1')->first();
                $attributesData = MapMeta::where('map_object_id', '=', $attributes->id)->get();
                $attributeData = "";
                if (!empty($attributesData)) {
                    foreach ($attributesData as $x) {
                        $attributeData .= " " . $x->key . "='" . $x->value . "' ";
                    }
                }
            }


            /**
             * Lets now Compile the SVG
             */
            if (!empty($attributeData)) {
                $composedMap = "<svg " . $attributeData . ">";
            } else {
                $composedMap = "<svg>";
            }

            $mapObjects = MapModel::where('map_id', '=', $id)->where('node', '!=', '@attributes')->where('status', '=', '1')->orderBy('id', 'desc')->get();

            $inputArray = array_merge([''], array_merge(range('0', '9'), range('A', 'Z')));

            $pulldataToStart = "";
            $pushDataToEnd = "";

            foreach ($mapObjects as $y => $x) {


                $poss = false;
                $afterLoopText= "";
                $currentlLoop = "";
                $currentlLoop .= "<" . $x->node;
                $mapMeta = MapMeta::where('map_object_id', '=', $x->id)->get();
                foreach ($mapMeta as $p => $b) {
                    $currentlLoop .= " " . $b->key . "='" . $b->value . "'";
                    if ($b->key == "class") {
                        $currentlLoop .= " data-item-id='" . $b->id . "' ";
                    }
                    if ($b->key == "fill" && $b->value == "#FFFFFF") {
                        $poss = true;
                    }

                    if($x->node =="text"){
                        if ($b->key == "text") {
                            $afterLoopText = $b->value;
                        }
                    }else{
                        $afterLoopText = "";
                    }


                }
                if($afterLoopText != ""){
                    $currentlLoop .= " >";
                    $poss = true;
                    $currentlLoop .= $afterLoopText."</text>";
                }else {
                    $currentlLoop .= " />";
                }

                if ($poss == true) {
                    $pulldataToStart .= $currentlLoop;
                } else {
                    $pushDataToEnd .= $currentlLoop;
                }

            }



            /**
             * Ok finaly,  I know I know WERE DOING LOTs
             * Lets Build the list of *OTHER* Categorys...
             */

            $listOfOther = MapC::select('id', 'left', 'right', 'frontend')->groupBy('right')->get();


            $composedMap .= $pushDataToEnd . $pulldataToStart;
            $composedMap .= "</svg>";
            Cache::add("map_build_" . $id, $composedMap, 2000);
            return $composedMap;

        }
    }

    public function mapCompareEdit($id)
    {

        $compared = MapC::find($id);
        return View::make("backend.map.createcomp")->with('maps', $compared);
    }

    public function mapCompare()
    {

        $compared = MapC::get();
        return View::make("backend.map.showcomp")->with('maps', $compared);


    }

    public function mapCompareNew()
    {
        $listOfNames = DB::table('events_ticket_type')->get();
        return View::make("backend.map.createnewcomp")->with('list', $listOfNames);
    }

    public function mapCompareSaveNew()
    {


        $input = Input::all();
        $mapc = new MapC;
        $mapc->left = $input['left'];
        $mapc->right = $input['right'];
        $mapc->frontend = $input['frontend'];
        $mapc->color = $input['color'];
        $mapc->save();
        return $this->mapCompare();

    }

    public function style()
    {

        header("Content-type: text/css");

        $directory = __DIR__ . "/../../../public/assets/css/scssphp/";
        require_once $directory . "scss.inc.php";
        $scss = new Compiler();

        $scss->setImportPaths($directory . "../");
        $coreString = "

            \$color1: rgba(255, 211, 79, 1);
            \$color2: rgba(230, 56, 133, 1);
            \$color3: rgba(96, 202, 221, 1);
            \$color4: rgba(125, 196, 104, 1);
            \$color5: rgba(248, 142, 0, 1);
            \$color6: rgb(217, 131, 250);

            \$color_ll: rgba(227, 41, 125, 1);
            \$color_lu: rgba(99, 201, 218, 1);
            \$color_sl: rgba(126, 194, 109, 1);
            \$color_su: rgba(255, 208, 92, 1);
            \$color_aw: rgba(199, 197, 192, 1);
            \$color_other: rgba(206, 43, 43, 1);

            \$lighter: 30%;
            \$light: 20%;

            \$darker: 30%;
            \$dark: 20%;



            //////


            @mixin fill-stroke(\$color) {
    fill: \$color !important;
              stroke: \$color !important;
            }

            @mixin fill-stroke-grey(\$color) {
    fill: grayscale(\$color) !important;
              stroke: grayscale(\$color) !important;
            }

            @mixin all-on-color-events(\$color,\$cat) {
    \$catpill: \$cat + \"_tickpill\";
              \$catTrigger: \"trigger_\" + \$cat + \"\";
              \$catButton: \"\" + \$cat + \"_button\";
              .#{\$cat} {
                @include fill-stroke-grey(\$color);
              }
              .#{\$cat}.click {
                @include fill-stroke(\$color);
              }
              .#{\$cat}.hover {
                @include fill-stroke(\$color);
              }
              .#{\$cat}.toggle {
                @include fill-stroke(\$color);
              }
              .#{\$cat}.hover.toggle {
                fill: lighten(\$color, \$light) !important;
                stroke: lighten(\$color, \$light) !important;
              }
              .#{\$cat}.blockhere {
                fill: darken(\$color, 30%) !important;
                stroke: darken(\$color, 30%) !important;
              }
              .#{\$cat}.blockhere.hover {
                fill: darken(\$color, 30%) !important;
                stroke: darken(\$color, 30%) !important;
              }
              .#{\$cat}.blockhere.hover.toggle {
                fill: darken(\$color, 30%) !important;
                stroke: darken(\$color, 30%) !important;
              }
              .#{\$cat}.blockhere.toggle {
                fill: darken(\$color, 30%) !important;
                stroke: darken(\$color, 30%) !important;
              }
              .#{\$catTrigger} {
                @media (min-width: 601px) {
                  border-left: 3px solid \$color;
                }
                @media (max-width: 600px) {
                  border-left: 10px solid \$color;
                }
              }
              .#{\$catTrigger}.hover {
                background: lighten(\$color, \$lighter) !important;
              }
              .#{\$catTrigger}.hover td {
                background: lighten(\$color, \$lighter) !important;
              }
              .#{\$catpill} {
                background: lighten(\$color, \$lighter) !important;
                border-left: 3px solid \$color;
              }
              .#{\$catButton}.active {
                padding: 10px;
                color: #fff;
                background-color: \$color;
                text-align: center;
                letter-spacing: .5px;
                vertical-align: middle;
                border-radius: 4px;
                font-weight: bold;
                margin: 10px;
                border: 0;
                -webkit-box-shadow: 0px 14px 0px 14px darken(\$color, \$lighter);
                -moz-box-shadow: 0px 14px 0px 14px darken(\$color, \$lighter);
                box-shadow: 0px 7px 0px 0px darken(\$color, \$lighter);
              }
              .#{\$catButton} {
                display: block !important;
                padding: 10px;
                color: #fff;
                background-color: grayscale(\$color);
                text-align: center;
                letter-spacing: .5px;
                vertical-align: middle;
                border-radius: 4px;
                font-weight: bold;
                margin: 10px;
                border: 0;
                -webkit-box-shadow: 0px 14px 0px 14px grayscale(darken(\$color, \$lighter));
                -moz-box-shadow: 0px 14px 0px 14px grayscale(darken(\$color, \$lighter));
                box-shadow: 0px 7px 0px 0px grayscale(darken(\$color, \$lighter));
              }
            }

            ////
            .cat_1, .cat_2, .cat_3, .cat_4, .cat_vip {
              stroke-width: 2;
              transition: all 1.0s !important;
            }

           .guarenteetxt-container {
              display: none !important;
            }

            .tickpill {
              padding: 10px;
              margin-bottom: 2px;
              width: 50%;
              float: left;
            }

            .resetButton {
              background: #E3397F !important;
              border: none !important;
              padding: 11px !important;
              color: white !important;
              border-radius: 7px !important;
              font-size: 15px !important;
            }

            .mobileFilterTab {
              perspective: 1500px;
              perspective-origin: 0% 50%;
              -webkit-transition: all 2s;
              transform-style: preserve-3d;
              transform-origin: 100% 50%;
              visibility: visible;
              transform: translate3d(100%, 0, 0) rotateY(0deg);
            }

            .mobileFilterTab.open {
              transform: translate3d(0%, 0, 0) rotateY(0deg);
            }

            .closeFilter_button {
              width: 95%;
            }

            .seatzones button {
              display: none;
            }

            .seatzones button {
              float: left;
              width: 43%;
              text-align: center;
            }

            ";


        $maps = MapC::all();

        $colors = [
            ['col' => '#CC44AA', 'cat' => 'col-xxs']
        ];

        $i = 0;
        foreach ($maps as $map) {
            if (!empty($map->color)) {
                $colors[$i]['col'] = "#" . $map->color;
            }

            if (!empty($map->right)) {
                $colors[$i]['cat'] = $map->right;
            }

            $i++;
        }


        foreach ($colors as $x) {
            if (!empty($x['col'])) {
                if (!empty($x['cat'])) {
                    $coreString .= "@include all-on-color-events(" . $x['col'] . ", \$cat: '" . $x['cat'] . "');";
                }
            }
        }


        $contents = $scss->compile($coreString);
        $response = Response::make($contents);
        $response->header('Content-Type', 'text/css');
        return $response;
    }

    public function mapCompareSaveThis()
    {

        $input = Input::all();
        if (!empty($input['id'])) {
            $row = MapC::find($input['id']);

            if (!empty($input['left'])) {
                $row->left = $input['left'];
            }

            if (!empty($input['right'])) {
                $row->right = $input['right'];
            }

            if (!empty($input['frontend'])) {
                $row->frontend = $input['frontend'];
            }
            if (!empty($input['color'])) {
                $row->color = $input['color'];
            }

            $row->save();

        }

        return $this->mapCompare();

    }

    /**
     * @param $id
     *
     * Note : Builds the SVG into a Database SVG
     *
     */
    public function mapCompile($id)
    {

        $mapDataFromID = Map::find($id);
        $filepath = public_path() . "/assets/maps/" . $mapDataFromID->src;
        unset($mapDataFromID);

        if (file_exists($filepath)) {


            $mapData = File::get($filepath);
            \Log::info("Got here");
            $xml = simplexml_load_string($mapData);
            $json = json_encode($xml);
            $array = json_decode($json, TRUE);
            \Log::info(var_export($array,true));

            foreach ($array as $y => $x) {
                if ($y == "@attributes") {
                    $this->handelAttribute($x, $id, '@attributes');
                }

                if ($y == "rect") {
                    foreach ($x as $a) {
                        $this->handel_inner($a, $id, 'rect');
                    }
                }

                if ($y == "path") {
                    foreach ($x as $a) {
                        $this->handel_inner($a, $id, 'path');
                    }
                }
                if ($y == "polygon") {
                    foreach ($x as $a) {
                        $this->handel_inner($a, $id, 'polygon');
                    }
                }
                if ($y == "text") {
                    foreach ($x as $a) {
                        $this->handel_inner($a, $id, 'text');
                    }
                }
                if ($y == "circle") {
                    foreach ($x as $a) {
                        $this->handel_inner($a, $id, 'circle');
                    }
                }
                if ($y == "image") {
                    foreach ($x as $a) {
                        $this->handel_inner($a, $id, 'image');
                    }
                }
            }
        } else {
            dd('Yolo');
        }

    }

    /**
     * @param $id
     * @return json
     *
     * Handles the AJAX saving fot the Map builder
     *
     */
    public function saveBlock($id)
    {
        $input = Input::all();
        $class = $input['class'];
        $blocxk = $input['block'];
        $imgx = $input['img'];

        $meta = MapMeta::find($id);
        $meta->value = $class;
        $meta->save();


        /**
         * Lets Set The Block
         */
        if (!empty($blocxk)) {
            $block = MapMeta::where('map_object_id', '=', $meta->map_object_id)->where('key', '=', 'block')->first();
            if (!empty($block)) {
                $block->value = $blocxk;
                $block->save();
            } else {
                $block = new MapMeta;
                $block->map_object_id = $meta->map_object_id;
                $block->key = 'block';
                $block->value = $blocxk;
                $block->save();

            }
        }

        /**
         * Lets Set the img
         */
        if (!empty($imgx)) {
            $img = MapMeta::where('map_object_id', '=', $meta->map_object_id)->where('key', '=', 'view')->first();

            if (!empty($img)) {
                $img->value = $imgx;
                $img->save();
            } else {
                $img = new MapMeta;
                $img->map_object_id = $meta->map_object_id;
                $img->key = 'view';
                $img->value = $imgx;
                $img->save();

            }
        }

        return $meta;
    }

    /**
     * @param $item
     * @param $id
     * @param $type
     *
     * Note : Handels the incompileSasssertion of each Row Item into the database
     * This dose not handle the insertion of the @attribute data for the SVG Elevment
     *
     */
    public function handel_inner($item, $id, $type)
    {


        if (!empty($item) && (is_array($item) || is_object($item))) {
            foreach ($item as $a) {


                $y = new MapModel;
                $y->node = $type;
                $y->map_id = $id;
                $y->save();
                $newItemId = $y->id;

                $class = false;
                if (!empty($a)) {
                    foreach ((array)$a as $z => $x) {

                        $a = new MapMeta();
                        $a->map_object_id = $newItemId;
                        $a->key = $z;
                        if ($a->key == "class") {
                            $class == true;
                        }
                        $a->value = $x;
                        $a->save();
                    }
                }

                if ($class == false) {
                    $a = new MapMeta();
                    $a->map_object_id = $newItemId;
                    $a->key = "class";
                    $a->value = "unset";
                    $a->save();
                }

            }

        }
    }

    /**
     * @param $item
     * @param $id
     * @param $type
     *
     * Note : Handels the insertion of the @attribute data of the SVG element
     *v
     */
    public function handelAttribute($item, $id, $type)
    {

        if (!empty($item)) {

            $y = new MapModel;
            $y->node = $type;
            $y->map_id = $id;
            $y->save();
            $newItemId = $y->id;

            foreach ($item as $z => $x) {

                $a = new MapMeta();
                $a->map_object_id = $newItemId;
                $a->key = $z;
                $a->value = $x;
                $a->save();

            }


        }

    }


    public function fetchCompare($id, $naming)
    {
        $naming = str_replace("%2F", "/", $naming);
        $cachName = "o_" . $id . $naming;
        if (Cache::has($cachName)) {
            return Cache::get($cachName);
        } else {
            $mapx = Mapc::where('left', '=', $naming)->first();
            if (!empty($mapx->right)) {
                return $this->compileBlocks(
                    $id,
                    $mapx->right,
                    $cachName
                );
            }

        }
    }


    public function compileBlocks($id, $cat, $cachName)
    {

        $dataPool = [];
        $mapComponents = MapModel::where('map_id', '=', $id)->get();

        foreach ($mapComponents as $a) {

            $mapMeta = MapMeta::where('map_object_id', '=', $a->id)->get();

            $first = 0;
            foreach ($mapMeta as $x) {

                if ($x->key == "class" && $first == 0) {

                    $dataPool[$a->id]['class'] = $x->value;
                    $first++;
                }

                if ($x->key == "block") {

                    $dataPool[$a->id]['block'] = $x->value;

                }

            }


        }
        $cleanData = [];

        foreach ($dataPool as $x => $y) {

            if (!empty($y['class'])) {
                if (!empty($y['block'])) {
                    $cleanData[$y['class']][] = $y['block'];
                }
            }

        }

        if (!empty($cleanData[$cat])) {
            Cache::add($cachName, $cleanData[$cat], 2000);
            return ($cleanData[$cat]);
        }

    }


    public function deprechiated___compileBlocks($id, $cat, $cachName)
    {
        $dataPool = [];

        if (!empty($id)) {
            $maps = Map::find($id);
        }

        if (!empty($id)) {
            $mapComponents = MapModel::where('map_id', '=', $id)->get();
        }

        $cleanData = [];

        if (!empty($mapComponents)) {

            foreach ($mapComponents as $a) {

                $mapMeta = MapMeta::where('map_object_id', '=', $a->id)->where('key', '=', "class")->orWhere('key', '=', "block")->where('map_object_id', '=', $a->id)->get();

                $first = 0;
                if (!empty($mapMeta)) {
                    foreach ($mapMeta as $x) {
                        $class = null;
                        $block = null;

                        if (!empty($x->key)) {
                            if ($x->key == "class" && $first == 0 && $x->value == $cat) {
                                if (!empty($x->value) && $x->value != "") {
                                    $class = $x->value;
                                }
                                $first++;
                            }
                        }


                        if (!empty($x->key)) {
                            if ($x->key == "block") {
                                if (!empty($x->value) && $x->value != "") {
                                    $block = $x->value;
                                }
                            }
                        }


                        if (!empty($block)) {
                            $cleanData[$class][] = $block;
                        }
                    }
                }
            }
        }
        Cache::add($cachName, json_encode($cleanData), 2000);
        return json_encode($cleanData);

    }

}