<?php

use Bond\Repositories\Article\ArticleRepository as Article;
use Bond\Repositories\Category\CategoryRepository as Category;
use Bond\Repositories\Tag\TagRepository as Tag;

class ArticleController extends BaseController {

    protected $article;

    public function __construct(Article $article, Tag $tag, Category $category) {

        $this->article = $article;
        $this->tag = $tag;
        $this->category = $category;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        View::share('body_class', 'articles news');
        $news = $this->article->paginate();
        foreach($news as $new)
        {
            $new->lText = $new->getLocaleText();
        }
        return View::make(Template::name('frontend.%s.article.index'), compact('news'));
    }

    /**
     * @param $id
     * @return \Illuminate\View\View
     */
    public function show($id, $slug = null) {
        $article = $this->article->find($id);
        $lText = $article->getLocaleText();
        View::composer(Template::name('frontend.%s._layout.layout'), function ($view) use ($article, $lText) {
            $view->with('meta_title', $lText['meta_title']);
            $view->with('meta_keywords', $lText['meta_keywords']);
            $view->with('meta_description', $lText['meta_description']);
        });
        View::share('body_class', 'articles');
        $categories = $this->category->all();
        $tags = $this->tag->all();
        return View::make(Template::name('frontend.%s.article.show'), compact('article', 'lText' , 'categories', 'tags'));
    }


    public function showNews($slug = null) {
        $article = $this->article->findBySlug($slug);
        $lText = $article->getLocaleText();
        View::composer(Template::name('frontend.%s._layout.layout'), function ($view) use ($article, $lText) {
            $view->with('meta_title', $lText['meta_title']);
            $view->with('meta_keywords', $lText['meta_keywords']);
            $view->with('meta_description', $lText['meta_description']);
        });

        View::share('body_class', 'articles');
        $categories = $this->category->all();
        $tags = $this->tag->all();

        $clubs = [];
        $events = [];
        $leagues = [];

        $clubs = DB::table('article_fbt_assoc as afa')
                    ->where('afa.articles_id', $article->id)
                    ->where('afa.type', 'club')
                    ->leftJoin('football_ticket', 'afa.fbt_id', '=', 'football_ticket.id')
                    ->get();

        $events = DB::table('article_event_assoc as aea')
                    ->where('aea.articles_id', $article->id)
                    ->leftJoin('events', 'aea.event_id', '=', 'events.id')
                    ->get();

        foreach($events as $event)
        {
            $event->url = FootBallEvent::getUrl($event);
        }

        $leagues = DB::table('article_fbt_assoc as afa')
                    ->where('afa.articles_id', $article->id)
                    ->where('afa.type', 'league')
                    ->leftJoin('football_ticket', 'afa.fbt_id', '=', 'football_ticket.id')
                    ->get();

        View::share('taggedClubs',$clubs);
        View::share('taggedEvents',$events);
        View::share('taggedLeagues',$leagues);

        return View::make(Template::name('frontend.%s.article.show'), compact('article', 'lText' ,'categories', 'tags'));
    }
}
