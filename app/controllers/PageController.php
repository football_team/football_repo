<?php

use Bond\Repositories\Page\PageRepository as Page;
use Template;

class PageController extends BaseController {

    protected $page;

    public function __construct(Page $page) {

        $this->page = $page;
    }

    /**
     * Display page
     * @param $id
     * @return \Illuminate\View\View
     */
    public function show($id) {
        $locale = \App::getLocale();
        $template = 'frontend.%s.';
        $node = $this->page->find($id);

        if($this->page->checkTrans($locale, $node->id))
        {
            $lText = $this->page->getLText($node->id, $locale);
            $node->title = $lText['pt']->lstring;
            $node->content = $lText['pc']->lstring;
            $node->meta_content = $lText['pmc']->lstring;
            $node->meta_title = $lText['pmt']->lstring;
            $node->meta_description = $lText['pmd']->lstring;
        }
        $template = $template.Template::getTemplate(PagesMeta::getMetaKey([
                'page_id' => $id,
                'meta_keyword' => 'template'
            ])->meta_content);
        return View::make(Template::name($template), compact('node'));
    }
}
