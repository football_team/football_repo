<?php

use Bond\Services\Mailer;

class TicketRequestController extends BaseController {
	public function post()
	{
		$passedData = Input::all();
		
	    $rules = array(
	        'name'             => 'required|min:3',                       // just a normal required validation
	        'email'            => 'required|email',
	        'phone'			   => 'required',   // required and must be unique in the ducks table
	        'numTickets'       => 'required|integer',
	        'reqTogether'      => 'required|boolean',
	        'eventId'          => 'required|integer|exists:events,id',
	        'side'			   => 'required',
			'captcha'          => 'required|captcha'
	    );

	    $validator = Validator::make($passedData, $rules);

	    if ($validator->fails()) 
	    {
	        $messages = $validator->messages()->toArray();
			$messages['captcha_image'] = HTML::image(Captcha::img(), 'Captcha image');
	        $response = Response::make(json_encode(array('result' => $messages)), 400);
	        $response->header('Content-Type', 'application/json');
	        return $response;
	    }
	    else
	    {
	    	
	        $result = DB::table('events')->where('id', $passedData['eventId'])->first();
	        $passedData['title'] = $result->title;
	        $passedData['date'] = $result->datetime;
	        
	        if(Input::has('additionalInfo'))
	        {
	        	$passedData['additionalInfo'] = strip_tags($passedData['additionalInfo']);
	        }

	        $mailer = new Mailer;
       		$mailer->send('emails.contact-form.ticket-request', trans('homepage.contact-email'), 'Ticket request', $passedData);

	        $response = Response::make(json_encode(array('result' => 'success')), 200);
	        $response->header('Content-Type', 'application/json');
	        return $response;	        
	    }
	}
}