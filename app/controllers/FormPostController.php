<?php

use Bond\Services\Mailer;

class FormPostController extends BaseController {

    protected $formPost;

    public function __construct(FormPost $formPost) {

        $this->formPost = $formPost;
    }

    public function getContact() {

        return View::make(Template::name('frontend.%s.contact.form'));
    }

    public function postContact() {

        $formData = array(
            'sender_name_surname' => Input::get('sender_name_surname'),
            'sender_email'        => Input::get('sender_email'),
            'sender_phone_number' => Input::get('sender_phone_number'),
            'subject'             => '',
            'post'                => Input::get('message'),
            'captcha'               => Input::get('captcha')
        );

        $rules = array(
            'sender_name_surname' => 'required',
            'sender_email'        => 'required|email',
            'sender_phone_number' => 'required',
            //'subject'             => 'required',
            'post'                => 'required',
            'captcha'             => 'required|captcha'
        );

        $validation = Validator::make($formData, $rules);

        if ($validation->fails()) {
            $errors = $validation->messages()->toArray();;
            $errors['captcha_image'] = HTML::image(Captcha::img(), 'Captcha image');

            $response = Response::make(json_encode(array('result' => $errors )), 400);
            $response->header('Content-Type', 'application/json');
            return $response;
        }

        $formData['sender_message'] = $formData['post'];

        Mail::send('emails.contact-form.form', $formData, function ($message) {
            $message->from(Input::get('sender_email'), Input::get('sender_name_surname'));
            $email = trans('homepage.contact-email');
            $message->to($email, 'Football Ticket Pad')->subject("Web Enquiry");
        });


        /*
        $mailer = new Mailer;
        $mailer->send('emails.contact-form.form', 'admin@bondmedia.co.uk', Input::get('subject'), $formData);
        */

        $formPost = new FormPost();
        $formPost->sender_name_surname = $formData['sender_name_surname'];
        $formPost->sender_email = $formData['sender_email'];
        $formPost->sender_phone_number = $formData['sender_phone_number'];
        $formPost->subject = $formData['subject'];
        $formPost->message = $formData['post'];
        $formPost->save();

        $response = Response::make(json_encode(array('result' => 'success')), 200);
        $response->header('Content-Type', 'application/json');
        return $response;
    }

    public function postCorporateForm() {

        Log::info("Got here.");
        $formData = array(
            'name'              => Input::get('name'),
            'number_of_tickets' => Input::get('number_of_tickets'),
            'game'              => Input::get('game'),
            'date_of_match'     => Input::get('date_of_match'),
            'company'           => Input::get('company'),
            'budget'            => Input::get('budget'),
            'email'             => Input::get('email'),
            'tel'               => Input::get('tel'),
            'captcha'           => Input::get('captcha')
        );

        $rules = array(
            'name'                      => 'required',
            'email'                     => 'required|email',
            'game'                      => 'required',
            'date_of_match'             => 'required',
            'company'                   => 'required',
            'number_of_tickets'         => 'required',
            'captcha'                   => 'required|captcha'
        );

        $validation = Validator::make($formData, $rules);

        if ($validation->fails()) {
            $errors = $validation->messages()->toArray();;
            $errors['captcha_image'] = HTML::image(Captcha::img(), 'Captcha image');

            $response = Response::make(json_encode(array('result' => $errors )), 400);
            $response->header('Content-Type', 'application/json');
            return $response;
        }



        Mail::send('footballticket::email.corporate', $formData, function ($message) {
            $message->from(Input::get('email'), Input::get('name'));
            $email = trans('homepage.contact-email');
            $message->to($email, 'Football Ticket Pad')->subject("Corporate enquiry");
        });


        $response = Response::make(json_encode(array('result' => 'success')), 200);
        $response->header('Content-Type', 'application/json');
        return $response;
    }

}
