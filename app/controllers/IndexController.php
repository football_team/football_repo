<?php
use TransactPRO\Gate\GateClient;
use Bond\Repositories\FootballTicket\FootballTicketRepository as FootballTicket;
use Bond\Repositories\Page\PageRepository as PageRepo;

class IndexController extends BaseController {

    /*
    |--------------------------------------------------------------------------
    | Default Home Controller
    |--------------------------------------------------------------------------
    |
    | You may wish to use controllers instead of, or in addition to, Closure
    | based routes. That's great! Here is an example controller method to
    | get you started. To route to this controller, just add the route:
    |
    |	Route::get('/', 'HomeController@showWelcome');
    |
    */

    public function showPage($url) {
        $locale = \App::getLocale();
        $node = Pages::parseUri($url);
        if(!$node) {
            App::abort(404);
        }
        $template = 'frontend.%s.';
        $pagesMeta = new PagesMeta();

        $pr = new PageRepo($node);

        $template = $template.Template::getTemplate(@$pagesMeta->getMetaKey([
                'page_id' => $node->id,
                'meta_keyword' => 'template'
            ])->meta_content);

        View::share('body_class',Template::getBodyClass($node, Template::getTemplate(@$pagesMeta->getMetaKey([
            'page_id' => $node->id,
            'meta_keyword' => 'template'
        ])->meta_content)));

        View::share('feature_image',@$pagesMeta->getMetaKey([
            'page_id' => $node->id,
            'meta_keyword' => 'feature_image'
        ])->meta_content);

        if($pr->checkTrans($locale, $node->id))
        {
            $lText = $pr->getLText($node->id, $locale);
            $node->title = $lText['pt']->lstring;
            $node->content = $lText['pc']->lstring;
            $node->meta_keywords = $lText['pmc']->lstring;
            $node->meta_title = $lText['pmt']->lstring;
            $node->meta_description = $lText['pmd']->lstring;
        }


        return View::make(Template::name($template), compact('node'));
    }


    public function test() {
        
    }

    public function generateXMLFeed()
    {
        $date = Date('Y-m-d H:i:s');
        $return = array();
        $xmlString = '<?xml version="1.0" encoding="UTF-8"?><productFeed version="2.0">';
        $tickets = DB::table('ticket_view')
                        ->where('ticket_status', 'Active')
                        ->where('price', '>', '1')
                        ->where('event_date', '>', $date)
                        ->leftJoin('events', 'ticket_view.event_id', '=', 'events.id')
                        ->orderBy('event_date', 'asc')
                        ->orderBy('price', 'desc')
                        ->get();

        foreach($tickets as $ticket)
        {
            $url = FootBallEvent::getUrl($ticket);
            $tickInfo = json_decode($ticket->ticket_info);
            $ttype = $tickInfo->ticketInformation->ticket_type;
            $block = $tickInfo->ticketInformation->loc_block;

            $row = $tickInfo->ticketInformation->loc_row;
            $typeTicket = DB::table('events_ticket_type')
                            ->where('id', $ttype)
                            ->first();

            $typeTicket = $typeTicket->title;
            
            $xmlString .= '<product id="'.$ticket->product_id.'">';
            $xmlString .= "<name>".$ticket->title." ". $ticket->event_date ." Tickets</name>";
            $xmlString .= "<productURL>".$url."</productURL>";
            $xmlString .= "<imageURL>https://www.footballticketpad.com/logobbg.gif</imageURL>";
            $xmlString .= "<price>".number_format($ticket->price * (1+(intval(Options::getOption('booking_fees'))/100)), 2)."</price>";
            $xmlString .= "<description>". $typeTicket ." tickets for ".$ticket->title. " on the ". $ticket->event_date .". Block: ".$block." Row: ".$row."</description>";
            $xmlString .= "<categories>";
            $xmlString .= '<category name="Tickets" tdCategoryId="1813"/>';
            $xmlString .= "</categories>";
            $xmlString .= "<shortDescription>". $typeTicket ." tickets for ".$ticket->title. " on the ". $ticket->event_date .". Block: ".$block." Row: ".$row."</shortDescription>";
            $xmlString .= "<availability>Now</availability>";
            $xmlString .= "<shippingCost>Free</shippingCost>";
            $xmlString .= "<deliveryTime>3-4 days</deliveryTime>";
            $xmlString .= "<date>".$ticket->event_date."</date>";
            $xmlString .= "<location>".$typeTicket."</location>";
            $xmlString .= "<block>".$block."</block>";
            $xmlString .= "<row>".$row."</row>";
            $xmlString .= "<eventname>".$ticket->title."</eventname>";
            $xmlString .= "</product>";
        } 
        $xmlString .= '</productFeed>';

        $response = Response::make($xmlString, 200);
        $response->header('Content-Type', "text/xml");
        return $response;

    }

    public function ifttArticles()
    {
        $articles = Article::where('created_at', '>', '2016-04-12 00:00:00')
                    ->orderBy('created_at','DESC')
                    ->get();

        $outstr='<?xml version="1.0" encoding="UTF-8"?>';
        $outstr.='<rss version="2.0">';
        $outstr.='<channel>';
        $outstr.='<title>Football Ticket Pad News</title>';
        $outstr.='<link>https://www.footballticketpad.com</link>';
        $outstr.='<description>The Worlds Leading Football Ticket Platform</description>';
        foreach($articles as $article)
        {
            $dt = DateTime::createFromFormat('Y-m-d H:i:s', $article->created_at);
            $outstr.='<item>';
            $outstr.='<guid>https://www.footballticketpad.com/news/'.$article->slug.'</guid>';
            $outstr.='<title>'.$article->title.'</title>';
            $outstr.='<description>'.$article->excerpt.'</description>';
            $outstr.='<link>https://www.footballticketpad.com/news/'.$article->slug.'</link>';
            $outstr.='<pubDate>'.$dt->format('D, d M y H:i:s O').'</pubDate>';
            $outstr.='</item>';
        }

        $outstr.='</channel>';
        $outstr.='</rss>';

        $response = Response::make($outstr, 200);
        $response->header('Content-Type', "text/xml");
        return $response;
    }

    public function importLangFile(){
        $file = storage_path('detransupl.txt');
        $content = File::get($file);

        $split = explode('|', $content);
        $n = sizeof($split);

        for($i = 0; $i<$n-1; $i+=2)
        {
            $key = trim($split[$i]);
            $en = utf8_encode(trim($split[$i]));
            $val = utf8_encode(trim($split[$i+1]));

            \Log::info($key.'=>'. $en . '=>' .$val);

            $trans = DB::table('trans')
                        ->where('node', $key)
                        ->first();

            if($trans)
            {
                Log::info($trans->id);
                $at = DB::table('trans_text')
                        ->where('node_id', $trans->id)
                        ->where('lang', 'de')
                        ->first();

                if($at)
                {
                    DB::table('trans_text')
                            ->where('id', $at->id)
                            ->update(['text'=>$val]);
                }
                else
                {
                    DB::table('trans_text')
                            ->insert([
                               'node_id'=>$trans->id,
                                'lang'=>'de',
                                'text'=>$val
                            ]);
                }

            }


        }

        return Response::json('success',200);
    }

    public function getLangUploadRoute(){
        return View::make("frontend.bm2014.langupload");
    }
}
