<?php
use Bond\Repositories\Page\PageRepository as Page;
class HomeController extends BaseController
{

    protected $slider;

    public function __construct(Slider $slider)
    {

        $this->slider = $slider;
    }

    public function index()
    {
        $redis = LRedis::connection();
        $locale = App::getLocale();

        if($redis->exists('homeSliderImages'))
        {
            $images = json_decode($redis->get('homeSliderImages'));
        }
        else
        {
            $slider = $this->slider->with('images')->get()->first();
            if (isset($slider)) {
                $images = $slider->images;
                LRedis::set('homeSliderImages', json_encode($images), 'ex', 300);
            }
        }


        $node = Pages::parseUri('home');
        if ($node) {

            $page = new Page($node);

            if($page->checkTrans($locale, $node->id))
            {
                $lText = $page->getLText($node->id, $locale);
                $node->title = $lText['pt']->lstring;
                $node->content = $lText['pc']->lstring;
                $node->meta_content = $lText['pmc']->lstring;
                $node->meta_title = $lText['pmt']->lstring;
                $node->meta_description = $lText['pmd']->lstring;
            }

            $template  = 'frontend.%s.';
            $pagesMeta = new PagesMeta();
            $thist = Template::getTemplate(@$pagesMeta->getMetaKey([
                'page_id'      => $node->id,
                'meta_keyword' => 'template'
            ])->meta_content);

            $template = $template . $thist;

            view::share('meta_title', $node->meta_title);
            View::share('meta_description', $node->meta_description);

            View::share('body_class', Template::getBodyClass($node, $thist));
        } else {
            $template = 'frontend.%s.';
            $template = $template . Template::getTemplate('home');
            View::share('body_class', 'home');
        }

        if($redis->exists($locale.'_home_matches'))
        {
            $upcomingMatches = json_decode(LRedis::get($locale.'_home_matches'));
        }
        else
        {
            $upcomingMatches = array();

            $results = DB::table('events')
                ->select('events.*')
                ->where('home_widget', '=', '1')
                ->whereRaw('events.datetime >= NOW() ' )
                ->groupBy('events.id')
                ->orderBy('home_widget_order', 'ASC')
                ->get();

            foreach($results as $result) {
                $result->homeTeamClubLog = FootballTicketMeta::getTicketMeta($result->home_team_id, 'club_logo');
                $result->awayTeamClubLog = FootballTicketMeta::getTicketMeta($result->away_team_id, 'club_logo');

                $trans = DB::table('locale_text')
                    ->where('id', 'event_title_'.$result->id)
                    ->where('locale', $locale)
                    ->first();

                if($trans)
                {
                    if(trim($trans->lstring) != '')
                    {
                        $result->title = $trans->lstring;
                    }
                }

                $upcomingMatches[] = $result;
            }

            $redis->set($locale.'_home_matches', json_encode($upcomingMatches), 'ex', 86400);
        }

        View::share('upcomingMatches', $upcomingMatches);

        return View::make(Template::name($template), compact('images', 'node'));
    }
}
