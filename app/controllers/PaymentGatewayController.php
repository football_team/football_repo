<?php
use TransactPRO\Gate\GateClient;
use Bond\Services\Mailer;

class PaymentGatewayController extends BaseController
{

    public function gatewayReturn()
    {
        $response = Input::all();

        if (Options::getOption('payment_gateway') == 'tp') {
            $merchantTransactionId = Input::get('merchant_transaction_id');

            $transaction = Transaction::getTransaction($merchantTransactionId);

            try {

                if(!$transaction) {
                    throw new Exception('Transaction already completed.');
                }

                $paymentInfo = json_decode($transaction->order, true);

                Log::info(var_export($paymentInfo,true));


                if (empty($merchantTransactionId)) {
                    return Redirect::to('/');
                }

                if (!isset($paymentInfo['shoppingCartId'])) {
                    return Redirect::to('/');
                }

                if ($transaction->status == 1) {
                    return Redirect::to('/');
                }

                $shoppingCartId = $paymentInfo['shoppingCartId'];
                $orderInfo      = $paymentInfo['order'];

                $auth = Config::get('transactpro.auth');
                $gateClient = new GateClient($auth);

                $response = $gateClient->statusRequest(array(
                    'request_type' => 'transaction_status',
                    'init_transaction_id' => $paymentInfo['transactionId'],
                    'f_extended' => '6'
                ));

                if ($response->isSuccessful() && preg_match('/Status:Success/', $response->getResponseContent())) {

                    //Log::info("Product ID" . $orderInfo['product_id']);
                    $bf = intval(Options::getOption('booking_fees'));
                    FootBallEvent::updateMagentoDescription($orderInfo['product_id']);

                    $orderNumber = TicketSoap::process("cart.order", array($shoppingCartId, null, null));
                    $orderInfo['order_id'] = $orderNumber;
                    $buyModel = new FootballTicketBuy();
                    $order = TicketSoap::process('sales_order.info', $orderNumber);
                    $orderInfo['amount'] = $order['grand_total'];
                    $orderInfo['payment_status'] = 'Pending';
                    $orderInfo['listed_amount'] = isset($order['items'][0]['price']) ? $order['items'][0]['price'] : $order['base_subtotal'] / $order['total_qty_ordered'];
                    $buyModel->fill($orderInfo);
                    $buyModel->fees_amount = ($orderInfo['listed_amount'] * ($bf/100))*$order['total_qty_ordered'];
                    $buyModel->save();
                    View::share('orderInfo', $orderInfo);
                    TicketSoap::process('sales_order.addComment', array('orderIncrementId' => $orderNumber, 'status' => 'processing', 'comment' => 'Payment has been successful'));

                    $response = $gateClient->statusRequest(array(
                        'request_type' => 'transaction_status',
                        'init_transaction_id' => $paymentInfo['transactionId'],
                        'f_extended' => '5'
                    ));

                    if((array_key_exists('af_id', $paymentInfo))&&(array_key_exists('com_perc', $paymentInfo)))
                    {
                        Log::info("AFFILIATE SALE");
                        $af_id = $paymentInfo['af_id'];
                        $perc = intval($paymentInfo['com_perc']);
                        $rid = null;
                        if(array_key_exists('rid', $paymentInfo))
                        {
                            $rid = $paymentInfo['rid'];
                        }

                        $af = AffiliateProperty::where('id', $af_id)->first();
                        if($af)
                        {
                            $aid = null;
                            $aip = null;
                            $fees_shown = $af->show_fees;

                            if($af->agent_id)
                            {
                                $ag = Agent::where('id', $af->agent_id)->where('status', 'active')->first();

                                if($ag)
                                {
                                    $aid = $ag->id;
                                    $aip = $ag->agent_perc;
                                }
                            }

                            DB::table('affiliate_sales')
                                ->insert([
                                    'order_id'=>$orderNumber,
                                    'affiliate_id'=>$af_id,
                                    'commission_perc'=>$perc,
                                    'agent_id'=>$aid,
                                    'agent_perc'=>$aip,
                                    'rid'=>$rid,
                                    'fees_shown'=>$fees_shown
                                ]);

                            $balance = DB::table('affiliate_balance')
                                ->where('affiliate_id', $af_id)
                                ->first();

                            $balance = $balance->balance;

                            $balance = $balance + ($orderInfo['amount']*($perc/100));
                            $balance = number_format($balance,2,'.','');
                            $balance = floatval($balance);

                            DB::table('affiliate_balance')
                                ->where('affiliate_id', $af_id)
                                ->update(['balance' => $balance]);
                        }


                    }

                    if((array_key_exists('agent_discount',$paymentInfo['order']))&&($paymentInfo['order']['agent_discount'] != false))
                    {
                        $agent_id = $paymentInfo['order']['agent_discount']['agent_id'];
                        $agentAmt = $paymentInfo['order']['agent_discount']['amt'];
                        $rId = $paymentInfo['order']['agent_discount']['rId'];

                        $agent = Agent::where('id', $agent_id)->first();
                        $ae = 0;
                        $ad = 0;
                        if($agent)
                        {
                            $ad = $agent->agent_perc;
                            $ae = number_format(($orderInfo['amount']/100)*$ad, 2, '.','');
                        }

                        DB::table('agent_sale')
                                ->insert([
                                    'order_id'=>$orderNumber,
                                    'agent_id'=>$agent_id,
                                    'discount_amount'=>number_format($agentAmt,2,'.',''),
                                    'agent_earning'=>$ae,
                                    'agent_perc'=>$ad,
                                ]);

                        $api_path = Config::get('api.mage_soap_api_path');
                        require_once("{$api_path}app/Mage.php");
                        umask(0);
                        Mage::app('default');

                        $rulemodel = Mage::getModel('salesrule/rule')->load($rId);
                        $rulemodel->delete();
                    }

                    TicketSoap::process('sales_order.addComment', array('orderIncrementId' => $orderNumber, 'status' => 'processing', 'comment' => 'transaction id#' . $paymentInfo['transactionId']));
                    TicketSoap::process('sales_order.addComment', array('orderIncrementId' => $orderNumber, 'status' => 'processing', 'comment' => $response->getResponseContent()));

                    RelatedTicket::updateTicket($orderInfo['product_id'], $orderInfo['qty']);
                    $node = new stdClass();
                    $node->title = 'Order Success Confirmation';
                    View::share('body_class', 'checkout success');

                    //clean session
                    Session::put('mid_' . $merchantTransactionId, null);

                    //if order success create automated invoice
                    // Create new invoice
                    $newInvoiceId = TicketSoap::process('sales_order_invoice.create', array($orderNumber, array(), 'Invoice Created', true, true));
                    $invoice = TicketSoap::process('sales_order_invoice.info', $newInvoiceId);

                    $this->sendEmailConfirmationToSeller($orderNumber);
                    $transaction->status = 1;
                    $transaction->save();

                    $this->removeFromAbandoned($shoppingCartId);
                    Log::info(var_export($orderInfo,true));

                    return View::make(Template::name('frontend.%s.checkout.success'), compact('node', 'orderNumber', 'orderInfo'));
                } else {
                    throw new Exception('Declined by your bank. Please contact your card issuer to try to resolve the issue and try again.');
                }
            } catch (Exception $e) {
                $node = new stdClass();
                $node->title = 'Error';
                $node->content = $e->getMessage();
                View::share('body_class', 'checkout success');
                //clean session
                Session::put('mid_' . $merchantTransactionId, null);

                if(isset($merchantTransactionId) && !empty($merchantTransactionId))
                {
                    $this->recordFailedTransaction($merchantTransactionId);
                }


                Log::info($e->getMessage());

                return View::make(Template::name('frontend.%s.checkout.failure'), compact('node'));
            }

        }
        else if (Options::getOption('payment_gateway') == 'g2s') {
            Log::info('response-function: '.var_export($response, true));

            if(empty($response)) {
                ?>
                <script>
                    window.parent.backToCheckout();
                </script>
                <?php
                die();
            }
            $unique_id = $response['merchant_unique_id'];
            $transaction = Transaction::getTransaction($unique_id);


            try {

                if(!$transaction) {
                    throw new Exception('Transaction already completed.');
                }
                $paymentInfo = json_decode($transaction->order, true);
                $shoppingCartId = $paymentInfo['shoppingCartId'];
                $orderInfo      = $paymentInfo['order'];

                if ($transaction->status == 1) {
                    throw new Exception('Transaction already completed.');
                }

                $post_data['0']     = Config::get('gate2shop.secret_string');
                $post_data['1']     = $response['totalAmount'];
                $post_data['2']     = Config::get('gate2shop.currency');
                $post_data['3']     = $response['responseTimeStamp'];
                $post_data['4']     = $response['PPP_TransactionID'];
                $post_data['5']     = $response['Status'];
                $post_data['6']     = $response['productId'];


                $JoinedInfo = join('', array_values($post_data));
                $checksum = md5( $JoinedInfo);


                if($checksum == $response['advanceResponseChecksum'] && $response['Status'] == 'APPROVED') {
                    $transaction->status = 1;
                    $transaction->save();

                    $bf = intval(Options::getOption('booking_fees'));

                    FootBallEvent::updateMagentoDescription($orderInfo['product_id']);
                    $orderNumber = TicketSoap::process("cart.order", array($shoppingCartId, null, null));
                    $orderInfo['order_id'] = $orderNumber;
                    $buyModel = new FootballTicketBuy();
                    $order = TicketSoap::process('sales_order.info', $orderNumber);
                    $orderInfo['amount'] = $order['grand_total'];
                    $orderInfo['payment_status'] = 'Pending';
                    $orderInfo['listed_amount'] = isset($order['items'][0]['price']) ? $order['items'][0]['price'] : $order['base_subtotal'] / $order['total_qty_ordered'];
                    $buyModel->fill($orderInfo);
                    $buyModel->fees_amount = ($orderInfo['listed_amount'] * ($bf/100))*$order['total_qty_ordered'];

                    $rt = RelatedTicket::where('product_id', $orderInfo['product_id'])->first();
                    $event = false;
                    if(!empty($rt))
                    {
                        $event = FootBallEvent::where('id', $rt->event_id)->first();
                    }

                    if($event)
                    {

                        if($event->sniper_active)
                        {
                            $deferred_order = new DeferredOrder();
                            $deferred_order->fill($orderInfo);
                            $deferred_order->save();
                            Log::info("AND HERE IS THE DEFERED BLOCK");
                        }
                        else
                        {
                            Log::info("SORRY DEFERRED HAHA");
                            $buyModel->save();
                        }
                    }
                    else
                    {
                        Log::info("NOT DEFERED");
                        $buyModel->save();
                    }

                    View::share('orderInfo', $orderInfo);
                    TicketSoap::process('sales_order.addComment', array('orderIncrementId' => $orderNumber, 'status' => 'processing', 'comment' => 'Payment has been successful'));

                    if((array_key_exists('af_id', $paymentInfo))&&(array_key_exists('com_perc', $paymentInfo)))
                    {
                        $af_id = $paymentInfo['af_id'];
                        $perc = intval($paymentInfo['com_perc']);

                        DB::table('affiliate_sales')
                            ->insert([
                                'order_id'=>$orderNumber,
                                'affiliate_id'=>$af_id,
                                'commission_perc'=>$perc
                            ]);

                        $balance = DB::table('affiliate_balance')
                            ->where('affiliate_id', $af_id)
                            ->first();

                        $balance = $balance->balance;
                        $balance = $balance + ($orderInfo['amount']*($perc/100));
                        $balance = number_format($balance,2);
                        $balance = floatval($balance);

                        DB::table('affiliate_balance')
                            ->where('affiliate_id', $af_id)
                            ->update(['balance' => $balance]);
                    }

                    $paymentInfo['order_id'] = $orderNumber;
                    $transaction->order = json_encode($paymentInfo);
                    $transaction->save();

                    TicketSoap::process('sales_order.addComment', array('orderIncrementId' => $orderNumber, 'status' => 'processing', 'comment' => 'transaction id#' . $response['PPP_TransactionID']));
                    TicketSoap::process('sales_order.addComment', array('orderIncrementId' => $orderNumber, 'status' => 'processing', 'comment' => json_encode($response)));

                    if($event && $event->sniper_active)
                    {
                        //Email us over the deferred order, this is a function that I can test anyway.
                        $this->sendDeferredSaleEmail($orderNumber);

                    }
                    else
                    {
                        RelatedTicket::updateTicket($orderInfo['product_id'], $orderInfo['qty']);
                        $this->sendEmailConfirmationToSeller($orderNumber);
                    }

                    $node = new stdClass();
                    $node->title = 'Order Success Confirmation';
                    View::share('body_class', 'checkout success');

                    //clean session
                    Session::put('mid_' . $shoppingCartId, null);

                    //if order success create automated invoice
                    // Create new invoice
                    $newInvoiceId   = TicketSoap::process('sales_order_invoice.create', array($orderNumber, array(), 'Invoice Created', true, true));
                    $invoice        = TicketSoap::process('sales_order_invoice.info', $newInvoiceId);



                    //echo 'Payment Success';
                    //Log::info("This would cause the information to be registered twice if both were called." . $orderNumber);
                    $this->removeFromAbandoned($shoppingCartId);
                    return View::make(Template::name('frontend.%s.checkout.success'), compact('node', 'orderNumber', 'orderInfo'));
                    die();

                } else {
                    throw new Exception('Invalid Order');
                }
            } catch (Exception $e) {
                $node = new stdClass();
                $node->title = 'Error';
                $node->content = $e->getMessage();
                Log::info($e->getMessage());
                View::share('body_class', 'checkout success');
                //clean session
                Session::put('mid_' . $shoppingCartId, null);                //THROWN ERRORS ARE WHAMMED INTO HERE SO USE THIS TO RECORD FAILED TRANSACTIONS

                if(isset($unique_id) && !empty($unique_id))
                {
                    $this->recordFailedTransaction($unique_id);
                }

                return View::make(Template::name('frontend.%s.checkout.failure'), compact('node'));
            }

        } else {
            die('Unable process your order. Payment gateway is not selected.');
        }
    }

    public function thanks(){
        if(Session::has('orderInfo'))
        {
            $orderInfo = Session::pull('orderInfo');
            /*
            if(Session::has('customer'))
            {

                $customer = Session::get('customer');
                $lastOrder = DB::table('events_ticket_buy')
                    ->where('buyer_id', $customer['entity_id'])
                    ->orderBy('created_at', 'desc')
                    ->first();

                if($lastOrder)
                {
                    if(!$lastOrder->tracked)
                    {
                        Log::info('Order has been tracked.');
                        $lastOrder = DB::table('events_ticket_buy')
                                        ->where('id', $lastOrder->id)
                                        ->update(array('tracked'=>"1"));
                */
                        //Log::info("It is pretty silly if it makes it to both of these..");
                        return View::make(Template::name('frontend.%s.checkout.success'), compact('node', 'orderInfo'));
                  //  }
                //}
            //}
        }
        //Log::info('Order has not been tracked.');
        return Redirect::to('/');
    }

    public function success() {
        // Testing page
        // $node = new stdClass();
        // $node->title = 'Order Success Confirmation';
        // View::share('body_class', 'checkout success');
        // $orderInfo['order_id'] = "1233456";
        // return View::make(Template::name('frontend.%s.checkout.ezpay-success'), compact('node', 'orderNumber', 'orderInfo'));
        // die();
        // Testing page

        if(Session::has('ezpayMerchantId')){
            $unique_id = Session::get('ezpayMerchantId');
            $transaction = Transaction::getTransaction($unique_id, '1');
            $paymentInfo    = json_decode($transaction->order, true);
            $shoppingCartId = $paymentInfo['shoppingCartId'];
            $orderInfo      = $paymentInfo['order'];
            $bf = intval(Options::getOption('booking_fees'));
            FootBallEvent::updateMagentoDescription($orderInfo['product_id']);
            $orderNumber = TicketSoap::process("cart.order", array($shoppingCartId, null, null));
            $orderInfo['order_id'] = $orderNumber;
            $buyModel = new FootballTicketBuy();
            $order = TicketSoap::process('sales_order.info', $orderNumber);
            $orderInfo['amount'] = $order['grand_total'];
            $orderInfo['payment_status'] = 'Pending';
            $orderInfo['listed_amount'] = isset($order['items'][0]['price']) ? $order['items'][0]['price'] : $order['base_subtotal'] / $order['total_qty_ordered'];
            $buyModel->fill($orderInfo);
            $buyModel->fees_amount = ($orderInfo['listed_amount'] * ($bf/100))*$order['total_qty_ordered'];
            $buyModel->save();
            View::share('orderInfo', $orderInfo);
            TicketSoap::process('sales_order.addComment', array('orderIncrementId' => $orderNumber, 'status' => 'processing', 'comment' => 'Payment has been successful - cartID: '.$shoppingCartId));

            $paymentInfo['order_id'] = $orderNumber;
            $transaction->order = json_encode($paymentInfo);
            $transaction->save();
            $arr = Session::get('respezpay');
            $myarr = json_decode($arr,true);
            Session::forget('respezpay');
            function flatten($array, $prefix = '') {
                    $result = array();
                    foreach($array as $key=>$value) {
                            if(is_array($value)) {
                                $result = $result + flatten($value, $key );
                            }
                            else {
                                $result[$key] = $value;
                            }
                    }
                    return $result;
            }
            $data = flatten($myarr);
            $string = '';
            foreach($data as $key => $val){
                $string  = $string .$key.':'.$val;
                if($key != 'Terminal'){
                    $string  = $string .'~';
                }
            }
            TicketSoap::process('sales_order.addComment', array('orderIncrementId' => $orderNumber, 'status' => 'processing', 'comment' => $string));

            RelatedTicket::updateTicket($orderInfo['product_id'], $orderInfo['qty']);
            $node = new stdClass();
            $node->title = 'Order Success Confirmation';
            View::share('body_class', 'checkout success');

            //clean session
            Session::put('mid_' . $shoppingCartId, null);

            //if order success create automated invoice
            // Create new invoice
            $newInvoiceId   = TicketSoap::process('sales_order_invoice.create', array($orderNumber, array(), 'Invoice Created', true, true));
            $invoice        = TicketSoap::process('sales_order_invoice.info', $newInvoiceId);

            $this->sendEmailConfirmationToSeller($orderNumber);
            Session::forget('ezpayMerchantId');
            //echo 'Payment Success';
            //Log::info("It is crazy if it gets to both of these." . $orderNumber);
            return View::make(Template::name('frontend.%s.checkout.ezpay-success'), compact('node', 'orderNumber', 'orderInfo'));
            die();
        }
        else{
            $response = Input::all();
                    Log::info('success-function: '.var_export($response, true));
                    $unique_id = $response['merchant_unique_id'];
                    $transaction = Transaction::getTransaction($unique_id, '1');
                    if(!$transaction)
                    {
                        Log::info("There is no associated transaction. Investigate this.");
                    }
                    $paymentInfo = json_decode($transaction->order, true);

                    Log::info('success-function-payment-info: '.var_export($paymentInfo, true));

                    if(isset($paymentInfo['order_id']) || (isset($transaction->status) && $transaction->status  == '1')) {
                        $orderNumber = isset($paymentInfo['order_id']) ? $paymentInfo['order_id'] : null;
                        $orderRefNumber = $unique_id;
                        $dectrans = json_decode($transaction);
                        $orderInfo['order_id'] = $dectrans->id;
                        $orderInfo['product_id']=$paymentInfo['product_id'];
                        $node = new stdClass();
                        $node->title = 'Order Success Confirmation';
                        View::share('body_class', 'checkout success');
                        $orderInfo['amount'] = $paymentInfo['post']['total_amount'];
                        $orderInfo['productUnits']=$paymentInfo['post']['item_quantity_1'];
                        $orderInfo['productCost']=$paymentInfo['post']['item_amount_1'];
                        View::share('orderInfo', $orderInfo);
                        //Log::info("Control Flow 1");
                        //return View::make(Template::name('frontend.%s.checkout.success'), compact('node', 'orderNumber', 'orderRefNumber' ,'orderInfo'));
                        return Redirect::to('/checkout/thank-you')->with('orderInfo', $orderInfo);
                    } else if (Options::getOption('payment_gateway') == 'g2s') {
                        $response = Input::all();
                        if(empty($response)) {
                            ?>
                            <script>
                                window.parent.backToCheckout();
                            </script>
                            <?php
                            die();
                        }

                        $unique_id = $response['merchant_unique_id'];
                        $transaction = Transaction::getTransaction($unique_id);


                        try {
                            //Log::info("Control flow 2");
                            if(!$transaction) {
                                throw new Exception('Transaction already completed.');
                            }
                            $paymentInfo    = json_decode($transaction->order, true);
                            $shoppingCartId = $paymentInfo['shoppingCartId'];
                            $orderInfo      = $paymentInfo['order'];

                            if ($transaction->status == 1) {
                                throw new Exception('Transaction already completed.');
                            }

                            $post_data['0']     = Config::get('gate2shop.secret_string');
                            $post_data['1']     = $response['totalAmount'];
                            $post_data['2']     = Config::get('gate2shop.currency');
                            $post_data['3']     = $response['responseTimeStamp'];
                            $post_data['4']     = $response['PPP_TransactionID'];
                            $post_data['5']     = $response['Status'];
                            $post_data['6']     = $response['productId'];


                            $JoinedInfo = join('', array_values($post_data));
                            $checksum = md5( $JoinedInfo);


                            if($checksum == $response['advanceResponseChecksum'] && $response['Status'] == 'APPROVED') {
                                //Log::info("Control flow 3");
                                $transaction->status = 1;
                                $transaction->save();

                                $bf = intval(Options::getOption('booking_fees'));


                                FootBallEvent::updateMagentoDescription($orderInfo['product_id']);
                                $orderNumber = TicketSoap::process("cart.order", array($shoppingCartId, null, null));
                                $orderInfo['order_id'] = $orderNumber;
                                $buyModel = new FootballTicketBuy();
                                $order = TicketSoap::process('sales_order.info', $orderNumber);
                                $orderInfo['amount'] = $order['grand_total'];
                                $orderInfo['payment_status'] = 'Pending';
                                $orderInfo['listed_amount'] = isset($order['items'][0]['price']) ? $order['items'][0]['price'] : $order['base_subtotal'] / $order['total_qty_ordered'];
                                $buyModel->fill($orderInfo);
                                $buyModel->fees_amount = ($orderInfo['listed_amount'] * ($bf/100))*$order['total_qty_ordered'];
                                $buyModel->save();
                                View::share('orderInfo', $orderInfo);
                                TicketSoap::process('sales_order.addComment', array('orderIncrementId' => $orderNumber, 'status' => 'processing', 'comment' => 'Payment has been successful'));

                                $paymentInfo['order_id'] = $orderNumber;
                                $transaction->order = json_encode($paymentInfo);
                                $transaction->save();

                                TicketSoap::process('sales_order.addComment', array('orderIncrementId' => $orderNumber, 'status' => 'processing', 'comment' => 'transaction id#' . $response['PPP_TransactionID']));
                                TicketSoap::process('sales_order.addComment', array('orderIncrementId' => $orderNumber, 'status' => 'processing', 'comment' => json_encode($response)));

                                RelatedTicket::updateTicket($orderInfo['product_id'], $orderInfo['qty']);
                                $node = new stdClass();
                                $node->title = 'Order Success Confirmation';
                                View::share('body_class', 'checkout success');

                                //clean session
                                Session::put('mid_' . $shoppingCartId, null);

                                //if order success create automated invoice
                                // Create new invoice
                                $newInvoiceId   = TicketSoap::process('sales_order_invoice.create', array($orderNumber, array(), 'Invoice Created', true, true));
                                $invoice        = TicketSoap::process('sales_order_invoice.info', $newInvoiceId);

                                $this->sendEmailConfirmationToSeller($orderNumber);

                                //echo 'Payment Success';
                                //Log::info("It is crazy if it gets to both of these." . $orderNumber);
                                return View::make(Template::name('frontend.%s.checkout.success'), compact('node', 'orderNumber', 'orderInfo'));
                                die();

                            } else {
                                throw new Exception('Invalid Order');
                            }
                        } catch (Exception $e) {
                            $node = new stdClass();
                            $node->title = 'Error';
                            $node->content = $e->getMessage();
                            View::share('body_class', 'checkout success');
                            //clean session
                            return View::make(Template::name('frontend.%s.checkout.failure'), compact('node'));
                        }
                    }
        }
    }

    public function error() {
        Log::info("I AM IN HERE");
        $node = new stdClass();
        $node->title = 'Error';
        $node->content = 'Unable to process your order';
        View::share('body_class', 'checkout success');
        $input = Input::all();
        Log::info(var_export($input, true));
        return View::make(Template::name('frontend.%s.checkout.failure'), compact('node'));
    }

    public function pending() {
        $node = new stdClass();
        $node->title = 'Order Pending';
        $node->content = 'Your order is pending';
        View::share('body_class', 'checkout success');
        return View::make(Template::name('frontend.%s.checkout.failure'), compact('node'));
    }

    public function gatewayPaymentRequest()
    {
        $auth = Config::get('transactpro.auth');
        $gateClient = new GateClient($auth);

        //userip: if local database or stage should provided white label ip address
        $userIP = App::environment('local', 'stage') ? '81.137.255.206' : $_SERVER['REMOTE_ADDR'];
        $country = App::environment('local', 'stage') ? 'LV' : Input::get('country');


        $response = $gateClient->initDms(array(
            'rs' => Config::get('transactpro.rs'),
            'merchant_transaction_id' => microtime(true),
            'user_ip' => $userIP,
            'description' => Input::get('product_title'),
            'amount' => Input::get('amount'),
            'currency' => Config::get('transactpro.currency'),
            'name_on_card' => Input::get('name_on_card'),
            'street' => Input::get('street'),
            'zip' => Input::get('zip'),
            'city' => Input::get('city'),
            'country' => $country,
            'state' => Input::get('state'),
            'email' => Input::get('email'),
            'phone' => Input::get('phone'),
            'merchant_site_url' => Input::get('merchant_site_url'),
            'card_bin' => substr('card_number', 0, 6),
            'bin_name' => Input::get('name_on_card'),
            'bin_phone' => Input::get('phone')
        ));
    }

    public function codPayment()
    {
        $merchantTransactionId = Input::get('merchant_transaction_id');
        $paymentInfo = json_decode(Session::get('mid_' . $merchantTransactionId), true);


        if (empty($merchantTransactionId)) {
            return Redirect::to('/');
        }

        if (!isset($paymentInfo['shoppingCartId'])) {
            return Redirect::to('/');
        }

        $shoppingCartId = $paymentInfo['shoppingCartId'];
        $orderInfo = $paymentInfo['order'];


        try {

            //Log::info("Product ID" . $orderInfo['product_id']);
            FootBallEvent::updateMagentoDescription($orderInfo['product_id']);

            $orderNumber = TicketSoap::process("cart.order", array($shoppingCartId, null, null));
            $orderInfo['order_id'] = $orderNumber;
            $buyModel = new FootballTicketBuy();
            $order = TicketSoap::process('sales_order.info', $orderNumber);

            //Log::info('Order: ' . var_export($order, true));

            $orderInfo['amount'] = $order['grand_total'];
            $orderInfo['payment_status'] = 'Pending';
            $orderInfo['listed_amount'] = isset($order['items'][0]['price']) ? $order['items'][0]['price'] : $order['base_subtotal'] / $order['total_qty_ordered'];
            $buyModel->fill($orderInfo);
            $buyModel->save();
            View::share('orderInfo', $orderInfo);
            //TicketSoap::process('sales_order.addComment', array('orderIncrementId' => $orderNumber, 'status' => 'processing', 'comment'=>'Payment has been successful'));

            RelatedTicket::updateTicket($orderInfo['product_id'], $orderInfo['qty']);
            $node = new stdClass();
            $node->title = 'Order Success Confirmation';
            View::share('body_class', 'checkout success');

            //clean session
            Session::put('mid_' . $merchantTransactionId, null);

            //if order success create automated invoice
            // Create new invoice
            //$newInvoiceId = TicketSoap::process('sales_order_invoice.create', array($orderNumber, array(),'Invoice Created', true, true ));
            //$invoice = TicketSoap::process('sales_order_invoice.info', $newInvoiceId);

            $this->sendEmailConfirmationToSeller($orderNumber);

            return View::make(Template::name('frontend.%s.checkout.success'), compact('node', 'orderNumber'));
        } catch (Exception $e) {
            $node = new stdClass();
            $node->title = 'Error';
            $node->content = $e->getMessage();
            View::share('body_class', 'checkout success');
            //clean session
            Session::put('mid_' . $merchantTransactionId, null);

            return View::make(Template::name('frontend.%s.checkout.failure'), compact('node'));
        }
    }

    public function ezPayError() {
        $node = new stdClass();
        $node->title = 'Error';
        $node->content = Session::get("ezPayError");
        // $node->content = "Error: Unable to process transaction.";
        View::share('body_class', 'checkout success');
        //clean session
        return View::make(Template::name('frontend.%s.checkout.ezpay-failure'), compact('node'));
    }

    //private functions
    private function sendDeferredSaleEmail($orderId)
    {
        Log::info("INSIDE DEFERRED SALES EMAIL");
        $deferred_order = DeferredOrder::where('order_id', $orderId)->first();

        //So, need to get the
            //Existing listing data
            //Event data
            //

        $product = RelatedTicket::where('product_id', $deferred_order->product_id)->first();
        $event = FootBallEvent::where('id', $product->event_id)->first();

        Log::info("ASSUMES event product deferred order");
        $data = array();
        $data['order'] = $deferred_order;
        $data['listing'] = $product;
        $data['event'] = $event;
        $data['tInfo'] = json_decode($product->ticket);


        $form = $data['tInfo']['ticketInformation']['form_of_ticket'];
        $ttype = $data['tInfo']['ticketInformation']['ticket_type'];

        $form = DB::table('events_form_of_ticket')->where('id', $form)->first();
        $ttype = DB::table('events_ticket_type')->where('id', $ttype)->first();

        $form = $form->title;
        $ttype = $ttype->title;

        $data['form_of_ticket']=$form;
        $data['ttype']=$ttype;
        Log::info("SET ALL DATA");
        Mail::send('footballticket::email.deferred-order', $data, function ($message){
            $from = Config::get('mail.from');
            $from['address'] = trans('homepage.contact-email');
            $message->from($from['address'], $from['name']);
            $email = Options::getOption('contact_email') == '' ? 'hello@bondmedia.co.uk' : Options::getOption('contact_email');

            $message->to('info@footballticketpad.com', 'Ticketpad')
                ->subject("Deferred Order Alert");
        });
        Log::info("SENT MAIL");
    }

    private function sendEmailConfirmationToSeller($orderId = '')
    {
        try {
            FootballTicketBuy::createSaleView();

            $orderDetails = DB::table('sales')->where('order_id', '=', $orderId)->first();

            $event = FootBallEvent::find($orderDetails->event_id);
            $eventTicket = RelatedTicket::where('product_id', '=', $orderDetails->product_id)->first();
            $ticket = RelatedTicket::getTicketByTicketId($orderDetails->product_id);
            $order = TicketSoap::process('sales_order.info', $orderId);

            $etb = FootballTicketBuy::where('order_id', $orderId)->first();

            $data = array();
            $data['game'] = $event->title . " - " . date('l, d F Y, h:ia', strtotime($event->datetime)) . ", " . $event->event_location;
            $data['event_id'] = $event->id;
            $data['order_id'] = $orderId;
            $data['total_amount'] = $eventTicket->price * $orderDetails->qty;
            $data['commission'] = ($data['total_amount'] * (empty($eventTicket->selling_commission_percentage) ? 15 : (int)$eventTicket->selling_commission_percentage) / 100);
            $data['total_amount_after_commission'] = $data['total_amount'] - $data['commission'];
            $data['seller_name'] = $orderDetails->seller_firstname . " " . $orderDetails->seller_lastname;
            $data['qty'] = $orderDetails->qty;
            $data['site'] = $etb->site_sold;

            $ticketInfo = json_decode($eventTicket->ticket, true);

            $data['formOfTicket'] = @$ticket->formOfTicket['title'];
            $data['location'] = @$ticket->ticketType['title'];
            $temp = array();
            if (isset($ticketInfo['ticketInformation']['loc_block']) && !empty($ticketInfo['ticketInformation']['loc_block'])) {
                $temp[] = 'Block: ' . $ticketInfo['ticketInformation']['loc_block'];
            }

            if (isset($ticketInfo['ticketInformation']['loc_row']) && !empty($ticketInfo['ticketInformation']['loc_row'])) {
                $temp[] = 'Row: ' . $ticketInfo['ticketInformation']['loc_row'];
            }

            $data['location'] = $data['location'] . ', ' . implode(', ', $temp);

            $temp = array();

            $defaultRestriction = [];
            foreach ($event->getSelectedRestrictions() as $val) {

                $defaultRestriction[] = array(
                    'id' => $val->id,
                    'title' => $val->title
                );
            }

            foreach ($defaultRestriction as $r):
                if (in_array($r['id'], $ticket->info['restrictions'])):
                    $temp[] = $r['title'];
                endif;
            endforeach;

            if ($ticket->buyerNot != ''):
                $temp[] = '<br /> Note: ' . $ticket->buyerNot;
            endif;

            $data['restrictions'] = implode(', ', $temp);

            $sm = $order['shipping_method'];
            $rateName = explode('_', $sm);
            $rateName = $rateName[0];
            $ships = DB::table('shipping_method_assoc')
                ->where('name', '=', $rateName)
                ->first();
            $data['shipping_method']=$ships->description;

           Mail::send('footballticket::email.sold-ticket-confirmation-to-seller', $data, function ($message) use ($orderDetails, $orderId) {
               $from = Config::get('mail.from');
                $from['address'] = trans('homepage.contact-email');
                $message->from($from['address'], $from['name']);
                $email = Options::getOption('contact_email') == '' ? 'hello@bondmedia.co.uk' : Options::getOption('contact_email');

                $message->to($orderDetails->seller_email, $orderDetails->seller_firstname . " " . $orderDetails->seller_lastname)
                    ->bcc($email, 'Football Ticket Pad')
                    ->subject("Confirmation of ticket sale: New Order # {$orderId}");
            });


        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function testMail()
    {
        $this->sendEmailConfirmationToSeller('100000061');
    }

    public function removeFromAbandoned($cid)
    {
        DB::table('follow_up_carts')
            ->where('cid', $cid)
            ->delete();

        return true;
    }

    public function recordFailedTransaction($tid)
    {
        DB::table('failed_transactions')
            ->insert(['tid'=>$tid]);

        return true;
    }
}
