<?php

class ImageController  extends BaseController
{
    public static function getSideBarImage($type, $id, $url)
    {
        $ctype = '';
        $url = substr($url, 1);
        if($type == 'club')
        {
            $ctype == 'club';
        }
        else if ($type == 'league')
        {
            $ctype == 'league';
        }

        if(File::exists($url))
        {

            $iname = $type.'-'.$id.'.png';
            if(File::exists('uploads/resized-logs/'.$iname))
            {
                return '/uploads/resized-logs/'.$iname;
            }
            else
            {
                $image = Image::make($url)->resize(33, 33);;
                $image->save('uploads/resized-logs/'.$iname);
                return '/uploads/resized-logs/'.$iname;
            }

        }
        return false;
    }
}