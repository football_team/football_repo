<?php
$translations = DB::table('trans_text')
    ->where('lang', '=', 'es')
    ->leftJoin('trans', 'trans_text.node_id', '=', 'trans.id')
    ->remember(120)
    ->get();
$fullTranslateList = [];
foreach ($translations as $singleTranslate) {
    if($singleTranslate->text !="") {
        $fullTranslateList[$singleTranslate->node] = "" . $singleTranslate->text;
    }else{
        $fullTranslateList[$singleTranslate->node] = "";
    }
}

$fullTranslateList['viewThisPage'] = 'Continuar en Ticket Pad España';
$fullTranslateList['woops'] = 'Whoops, No podría estar en la mejor versión del sitio';
$fullTranslateList['close'] = 'Cerrar';
$fullTranslateList['notCorrect'] = 'No correcto, Haz Clic aqui';
$fullTranslateList['langiosTwoThree'] = 'ES ES';
$fullTranslateList['language'] = 'Idioma';
$fullTranslateList['currency'] = 'Moneda';
$fullTranslateList['currentCountry'] = 'Peru';
$fullTranslateList['country'] = 'Pais';
$fullTranslateList['pleaseSelectYourLocale'] = 'Por Favor, Elegir su lugar';
$fullTranslateList['ggtrackingcode']='UA-72169155-1';
$fullTranslateList['currencyInUse']='S/.';


$fullTranslateList['countrycodefefault'] = 'ES';
return $fullTranslateList;
