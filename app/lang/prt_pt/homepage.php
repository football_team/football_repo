<?php
$lang = "lang_PT";
if (Cache::has($lang)) {
	return Cache::get($lang);

} else {
	$translations = DB::table('trans_text')
		->where('lang', '=', 'pt')
		->leftJoin('trans', 'trans_text.node_id', '=', 'trans.id')
		->remember(120)
		->get();

	$fullTranslateList = array(
	'gglSiteVer' => 'Bh0xvoTTl7Vls0HgLx1A5BYVB3uezPG94Q47ooeHrJ8',
	'ggtrackingcode' => 'UA-57128660-1',
	'tnumber' => '+44 (0)20 79936171',
	'zopim-code' => '2q52Bs9lVyDi0nlODLQrUfMbUJepajol',
	'contact-email' => 'info@footballticketpad.com',
	'currencyInUse'=>'&pound;',
	'login' => 'LOGIN',
	'logout' => 'LOGOUT',
	'News' => 'News',
	'FAQS' => 'FAQS',
	'FAQ' => 'FAQ',
	'Contact Us' => 'Contact Us',
	'About Us' => 'About Us',
	'Corporate' => 'Corporate',
	'rLogin' => 'Login',
	'slogan' => 'The Home Of Football Tickets',
	'search_example' => 'e.g. Champions League Final',
	'hot_tickets_header' => 'Hot Tickets',
	'top_teams_header' => 'Top Teams',
	'tournaments_header' => 'Tournaments',
	'view' => 'view',
	'buy' => 'buy',
	'sell' => 'sell',
	'BUY' => 'BUY',
	'SELL' => 'SELL',
	'up and coming matches' => 'UP &amp; COMING MATCHES',
	'Call us' => 'Call us',
	'never miss out' => 'NEVER MISS OUT',
	'enter email address' =>'Enter Email Address',
	'Email address' => 'Email address',
	'go' => 'GO',
	'subscribe now' => 'SUBSCRIBE NOW',
	'Worldwide Delivery' => 'Worldwide Delivery.',
	'fast secure' => 'Fast &amp; secure.',
	'Latest News' => 'Latest News',
	'fbLikeFooter' => 'Like us on Facebook',
	'termsCondFoot' => 'Terms and Conditions',
	'Privacy policy' => 'Privacy policy',
	'Privacy Policy' => 'Privacy Policy',

	'Top matches' => 'Top matches',
	'Show more' => 'Show More',
	'Hide' => 'Hide',
	'Search by Team' => 'Search by Team',
	'Upcoming Matches' => 'Upcoming Matches',

	'Buy Tickets' => 'Buy Tickets',
	'Sell Tickets' => 'Sell Tickets',
	'Buy' => 'Buy',
	'BUY TICKETS' => 'BUY TICKETS',
	'SELL TICKETS' => 'SELL TICKETS',
	'Filter By' => 'Filter By',
	'Select Type Of Tickets' => 'Select Type Of Tickets',
	'Select Number Of Tickets' => 'Select Number Of Tickets',
	'Location' => 'Location',
	'Available' => 'Available',
	'Price per ticket' => 'Price per Ticket',
	'Group Purchase' => 'Group Purchase',
	'Tickets for' => 'Tickets for',
	'tttc' => 'Ticket Type (Ticket Category)',
	'Tickets' => 'Tickets',

	'buy_corp_text' => "are available on group purchase through our corporate sales department. Please fill in the form below and a member of our VIP Sales Team will contact you with a quote!",

	'Your Name' => 'Your Name',
	'Company Name' => 'Company Name',
	'Your Email' => 'Your Email',
	'Telephone-mobile' => 'Telephone/mobile',
	'Message' => 'Message',
	'Submit' => 'Submit',
	'Other Note' => 'Other Note',

	'YOUR NAME' => 'YOUR NAME',
	'YOUR EMAIL' => 'YOUR EMAIL',
	'TorMob' => 'TEL/MOBILE NUMBER',
	'MESSAGE' => 'MESSAGE',
	'capEC' => 'Enter code here',
	'SUBMIT' => 'SUBMIT',

	'SUBMIT ORDER' => 'SUBMIT ORDER',


	'contactMSUB' => 'Thank you, we will read and reply to your message as soon as possible.',
	'captchaError' => 'Incorrect captcha entered. Please try again.',
	'COMPANY' => 'COMPANY',
	'GAME' => 'GAME',
	'DATE OF MATCH' => 'DATE OF MATCH',
	'MAXIMUM BUDGET' => 'MAXIMUM BUDGET',

    'gttc1' => 'Safe, Simple and Secure',
    'gttc2' => 'Matchday Tickets',
    'gttc3' => 'Fast / Flexible Delivery',
    'gttc4' => 'UK Customer Service',
    'gttc5' => 'Experienced Customer Support',

    'Checkout' => 'Checkout',
    'DescTickets' => 'Description of Tickets',
    'Event' => 'Event',
    'Ticket Type' => 'Ticket Type',
    'Form of Ticket' => 'Form of Ticket',
    'Restriction' => 'Restriction',
    'Number of Tickets' => 'Number of Tickets',
    'NUMBER OF TICKETS' => 'NUMBER OF TICKETS',
    'Shipping' => 'Shipping',
    'Delivery Country' => 'Delivery Country',
    'Country Code' => 'Country Code',
    'Delivery Option' => 'Delivery Option',
    'delGuarantee' => 'Delivery of tickets is guaranteed before the event.',

    'fSelect' => '- SELECT -',
    'noSOAvail' => '- NO SHIPPING OPTIONS AVAILABLE -',

    'internationalDelInfo' => "Due to the proximity in which clubs dispatch tickets, if you are travelling abroad to attend a game we highly recommend that tickets are delivered locally.",

    'Buyer Details' => 'Buyer Details',
    'Buyer Notes' => 'Buyer Notes',
	'ifRegClickHere' => 'If you have already registered then please',
	'to login' => 'to login.',
	'click here' => 'click here',
	'Personal Details' => 'Personal Details',
	'fname' => 'First Name',
	'sname' => 'Second Name',
	'emailAndContact' => 'Email address &amp; contact info',
	'Email' => 'Email',
	'Confirm Email' => 'Confirm Email',
	'Mobile Number' => 'Mobile Number',
	'subForPromDis' => 'Subscribe to newsletter offering promotions and discount codes',
	'Security' => 'Security',
	'Password' => 'Password',
	'PASSWORD' => 'PASSWORD',
	'Confirm Password' => 'Confirm Password',
	'passWarningLN' => '(The password may only contain letters and numbers.)',
	'passWarningChars' => 'Your password needs to be between 8 and 16 characters',
	'passWarningDNM' => 'Password does not match.',
	'Shipping Address' => 'Shipping Address',
	'Address' => 'Address',
	'Postcode' => 'Postcode',
	'City' => 'City',
	'Reservation Name' => 'Reservation Name',
	'Hotel Name' => 'Hotel Name',
	'Checkin Date' => 'Check-in Date',
	'Checkout Date' => 'Check-out Date',
	'bsAreSame' => 'Tick if billing address and shipping address are the same ',

	'canHD' => 'Can Hand Deliver',
	'sellCanHD' => 'Can you provide hand delivery for this event?',
	'compare' => 'compare',
    'copy' => 'copy',
    'trash' => 'trash',
    'accQTYsbE' => 'Quantity should be an even number',

    'update' => 'update',
    'accSelAG' => 'Select a game',
    'accPTAGNH' => 'Please type a game name here',

	'Billing address' => 'Billing address',
	'Billing Address' => 'Billing Address',
	'ifmp' => 'I forgot my password',
	'Order Summary' => 'Order Summary',
	'ticketMulti' => 'ticket(s)',
	'Delivery fee' => 'Delivery fee',
	'BookingFeePerc' => 'Booking fee',
	'CCCPerc' => 'Credit Card Charge',
	'Final price' => 'Final Price*',
	'fpf' => '*Final price with handling fee already included',
	'tncAgree' => 'I agree to the',
	'terms and conditions' => 'terms and conditions',
	'Submit Order' => 'Submit Order',

	'wbfu0' => 'Why buy from us?',
	'wbfu1' => 'We implement the latest payment gateway solution to ensure the security of your transactions.',
    'wbfu2' => 'We provide unrivalled customer support, available around the clock at your beck and call from within the UK',
    'wbfu3' => 'You can take advantage of our last minute sales programme and even book on match day',
    'wbfu4' => 'We screen our network of ticket brokers to ensure that your tickets are guaranteed to be genuine',
    'wbfu5' => 'We are the only 5 star ticket agency online dedicated to the beautiful game',

    'tbNoneAvail' => "There are no tickets currently listed for this event. We can however source these tickets for you! Please fill in the request form below or even better give us a call on +34 9182952.",

	'trf1' => 'Your telephone number',
	'trf2' => 'Desired number of tickets',
	'trf3' => 'Seated Together',
	'trf4' => 'No preference',
	'trf5' => 'Home side',
	'trf6' => 'Message',
	'trf7' => 'Other events for',
	'Payment' => 'Payment',
	'side' => 'side',

	'Confirmation' => 'Confirmation',
	'Purchase confirmation' => 'Purchase Confirmation',
	'pc1' => 'Congratulations! Your purchase was successful.',
	'pc2' => 'Your transaction number is',
	'pc3' => 'A confirmation email has been sent to you. Please read this for full details of your incoming tickets.',
	'pc4' => 'Tell all of your friends that you are going to this event!',
	'Success' => 'Success',

	'Error' => 'Error',
	'checkoutError' => 'Unable to place order.',
	'Game' => 'Game',
	'Date' => 'Date',
	'PriceFrom' => 'Price From',

	'cUs1' => 'Your Name',
	'cUs2' => 'Your Email',
	'cUs3' => 'Tel or Mobile',
	'cUs4' => 'Message',
	'cUs5' => 'Enter Code Here',
	'cUsE1' => 'This field is required.',
	'cUsE2' => 'Please enter valid captcha',

	'Company' => 'Company',
	'corpNoT' => 'Number of tickets',
	'corpDoM' => 'Date of match',
	'Maximum Budget' => 'Maximum Budget',

	'Categories' => 'Categories',
	'Events' => 'Events',
	'noCatFound' => 'No categories found',
	'No tickets found' => 'No tickets found',
	'noEvFound' => 'No events found',
	'Search Results' => 'Search Results',
	'See tickets' => 'See tickets',

	'Listing' => 'Listing',
	'Listing ID' => 'Listing ID',
	'QTY' => 'QTY',
	'Sold' => 'Sold',
	'Split' => 'Split',

	'Purchases' => 'Purchases',
	'Sales' => 'Sales',
	'accInfo' => 'Account Information',
	'Addresses' => 'Addresses',
	'accAddNewBilling' => 'Add New Billing Address',
	'Add Address' => 'Add Address',

	'accToS' => 'Tickets on sale',
	'Sold Tickets' => 'Sold Tickets',
	'Filter by Event' => 'Filter by Event',
	'List Tickets' => 'List Tickets',
	'accPT' => 'Purchased Tickets',
	'accTransDate' => 'Transaction Date',
	'Price' => 'Price',
	'TransNumber' => 'Transaction Number',
	'TsOID' => 'Transaction / Order ID',
	'DelStatus' => 'Delivery Status',
	'Delivery Address' => 'Delivery Address',
	'accVMDA' => 'View/modify delivery address',
	'accFname' => 'Firstname',
	'accSname' => 'Surname',
	'Country' => 'Country',
	'Save' => 'Save',
	'purTickNotFound' => 'Could not find any purchased tickets. If this is a mistake then please contact us immediatly.',
	'saleTickNotFound' => 'Could not find any sold tickets.',
	'accSF1' => 'Show only future events',
	'accSF2' => 'Show only unpaid tickets',
	'Sale Date' => 'Sale Date',
	'delStatus' => 'Delivery Status',
	'View Address' => 'View Address',
	'Ship Ticket' => 'Ship Ticket',
	'cftp' => 'Contact Football Ticket Pad',
	'accDeets' => 'Account Details',
	'pDeets' => 'Personal Details',
	'RN' => 'Receive Newsletter',
	'Yes' => 'Yes',
	'No' => 'No',
	'Mobile' => 'Mobile',
	'Bank' => 'Bank',
	'Bank Account' => 'Bank Account',
	'Card Details' => 'Card Details',
	'accAddModify' => 'add/modify',
	'accChangePwd' => 'Change your password',
	'accChangeInfo' => 'Change information',
	'accPwdChange' => 'Password Change',
	'accUnChEmail' => 'You are un-able to change your email, if you wish to use a new email you must open another account',
	'accPM' => 'Payment method',
	'Account holder' => 'Account holder',
	'Card type' => 'Card type',
	'accIBAN' => 'IBAN (contact bank)',
	'accBorSw' => 'BIC or SWIFT (contact bank)',
	'PayPal' => 'PayPal',
	'accBanSw' => 'Bank swift',
	'accBnIB' => 'Bank IBAN',
	'accPPE' => 'Paypal Email',
	'accBD' => 'Billing Details',
	'accVAT' => 'VAT Registration Number',
	'Company Name' => 'Company Name',
	'Not set' => 'Not Set',
	'Modify' => 'Modify',
	'Addresses' => 'Addresses',
	'accMCADA' => 'Main Collection and Delivery Address',
	'accYSAAMCP' => 'Your shipping address and main collection point',
	'Change Address' => 'Change Address',
	'accRMTC' => 'Royal Mail Tracking Code',
	'Tracking Code' => 'Tracking Code',
	'Block' => 'Block',
	'Row' => 'Row',
	'accTTBP' => 'Total to be paid',
	'accTransID' => 'Transaction ID',
	'accPaySt' => 'Payment Status',
	'accDelSt' => 'Delivery Status',
	'See Tracking' => 'See Tracking',
	'accBlocknRow' => 'Block &amp; row',
	'accMEarnings' => 'My Earnings',
	'fgPwd' => 'Forgot Password',
	'rsPwd' => 'Reset Password',
	'Web Price' => 'Web Price',
	'accRtoS' => 'Ready to Ship',
	'Qty' => 'Qty',
	'Register' => 'Register',
	'REGISTER' => 'REGISTER',
	'whyReg' => "Ticket Pad is the leading marketplace for buying and selling football tickets for sought after matches across the World. It’s easy to use, safe and free, so join us today",
	'subToNewsletter' => 'Subscribe to newsletter offering promotions and discount codes',
	'pwdReset' => 'Password Reset',
	'pwResetLink' => "To reset your password, :linktext. If you did not request a password reset, you can safely ignore this email - nothing will be changed.",
	'pwResetLink1' => 'To reset your password,',
	'pwResetLink2' => 'click here.',
	'pwResetLink3' => 'If you did not request a password reset, you can safely ignore this email - nothing will be changed.',
	'pointYourBrowser' => 'Or point your browser to this address:',
	'emailTY' => 'Thank you,',
	'fbtpAT' => 'FootballTicketPad Admin Team',
	'Dear' => 'Dear',
	'ythbd' => 'Your ticket has been dispatched.',
	'chtcyis' => 'Click here to check your item status',
    'tvalofpch' => 'To view a list of FAQs please click here',
    'buyDesc1' => 'Buy matchday tickets to watch',
    'buyDesc2' => 'in the',
    'buyDesc3' => 'Buying tickets with <a href="/about">Football Ticket Pad</a> is safe, simple and secure. We have earned a 5 star Trust Pilot rating for customer service and support. The ticket options available for this fixture are listed below. Prices shown are subject to a 10% booking fee. Select the tickets you would like to buy and you will be taken to a secure checkout page where you can complete your purchase online.',
    'buyDesc4' => 'If you have a question or need help with anything at all, <a href="/contact-us">get in touch</a> with our UK based team. Call, email, or use the chat box in the bottom right of this page. And don\'t forget to take a look at our regularly updated <a href="/faq">FAQs</a>. <b>Enjoy the match!</b>',
    'accPFIRF' => 'Please fill in all required fields.',
    'hdAgreed' => 'You have agreed to offer hand delivery for this ticket.',
    'invalidSellPreff' => 'Invalid sell preference selected.',
    'to' => 'to',
    'buyReqSent' => 'Your request has been submitted',
    'buyReqNotSent' => 'Unable to submit request. Please make sure you filled in all of the available fields.',
    'cantLeaveOne' => 'Cannot leave one ticket.',
    'emExists' => 'This email address is already registered',
    'unableToProcess' => 'Sorry, we are unable to process your request at this time. Please leave us a message detailing the steps you took or try again later.',
    'This field is required' => 'This field is required.',
    'passMinLen' => 'Your password is too short, it must be between 8 and 16 characters long.',
    'passMaxLen' => 'Your password is too long, it must be between 8 and 16 characters long.',
    'min2chars' => 'This field must be at least 2 characters long.',
    'emailMismatch' => 'The email addresses entered do not match.',
    'passMismatch' => 'The passwords entered do not match.',
    'mustAcceptTerms' => 'You must accept our terms and conditions in order to use our services.',
    'onlyNumeric' => 'This field only accepts numeric characters (digits).',
    'mustSellDelivery' => 'You must select a delivery method.',
    'Top matches' => 'Top matches',
    'Unknown error' => 'Unknown error',
    'Shipping Method' => 'Shipping Method',
    'Team Fact File' => 'Team Fact File',
    'Nickname' => 'Nickname',
    'Founded' => 'Founded',
    'Rivals' => 'Rivals',
    'Record Goal Scorer' => 'Record Goal Scorer',
    'Record Signing' => 'Record Signing',
    'fftp' => '&copy; Football Ticket Pad',
    'fftptac' => 'Terms &amp; conditions',
    'ftpwad' => 'This page was updated at',
    'Read More' => 'Read More',
    'Posted on' => 'Posted on',
    'Your Tickets' => 'Your Tickets',
    'Your Personal Details' => 'Your Personal Details',
    'Protection Guarantee' => 'Protection Guarantee',
    'Guarantee' => 'Guarantee',
    'sell1long1' => 'The tickets must be adjoining seats, if they are not then please list them seperately',
    'sell1long2' => 'Indicate if you have normal tickets (paper or etickets) or season passes',
    'sell1long3' => 'It’s important that you provide a detailed description of your tickets as it increases your chance of selling them',
    'sell1as' => 'Do the tickets have any restrictions?',
    'Other information' => 'Other information',
    'sell1long4' => 'Indicate any kind of restriction that appears on your ticket for example it is a discounted ticket, restricted view etc',
    'sell1long5' => 'Ticket Pad does not allow additional information that includes unhelpful descriptions such as "best seats" or "good view"',
    'Sale Definition' => 'Sale Definition',
    'sell1howmuch' => 'How much do you want to charge for each ticket?',
    'sell1howsell' => 'How do you want to sell them?',
    'All together' => 'All together',
    'AvoidLeaving1' => 'Avoid leaving 1 ticket',
    'In pairs' => 'In pairs',
    'Price Review' => 'Price Review',
    'sell1LF' => 'Your tickets will be listed for',
    'Maximum Price' => 'Maximum Price',
    'Minimum Price' => 'Minimum Price',
    'Average Price' => 'Average Price',
    'sell1CSF' => 'They are currently being sold for',
    'CONTINUE' => 'CONTINUE',
    'Enter price' => 'Enter price',
    'sell1long6' => 'Please make sure that the price is a valid number without currency symbols.',
    'Registered User' => 'Registered User',
    'Not Registered User' => 'Not Registered User',
    'Method of Collection' => 'Method of Collection',
    'sell2long1' => 'Indicate the desired account that you want to receive your payment into once you have sold your tickets.',
    'sell3long1' => 'We protect you so that the transactions are',
    '100secure' => '100% secure',
    'sell3long2' => 'Are the tickets ready to be sent?',
    'sell3long3' => 'If you already have the tickets, you will increase your chances of selling them by checking the "yes I have them" this will make your listing stand out from the rest.',
    'yIHT' => 'Yes, I have them (if selected you will have 3 working days to send them)',
    'nIDHT' => 'No, I don\'t have them',
    'I accept the' => 'I accept the',
    'Terms and Conditions' => 'Terms and Conditions',
    'and' => 'and',
    'PUBLISH' => 'PUBLISH',
    'BACK' => 'BACK',
    'sell3long4' => 'Thank you for listing your tickets with Football Ticket Pad. Your tickets are now available for football fans all over the world to see and buy.',
    'sell3long5' => 'Your listing ID is:',
    'sell3long6' => 'Please ensure that your listing is kept up to date at all times. You can edit the listing in the',
    'sell3macc' => '"My Account"',
    'sell3long7' => 'section of our website under the heading “Listings.” For answers to any questions you may have, please visit our',
    'section.' => 'section.',
    'Ticket Confirmation' => 'Ticket Confirmation',
    'Form Post' => 'form Post',
    'Name:' => 'Name:',
    'Email:' => 'Email:',
    'Company:' => 'Company:',
    'Phone Number:' => 'Phone Number:',
    'Number of Tickets:' => 'Number of Tickets:',
    'Game:' => 'Game:',
    'Date:' => 'Max Budget',
    'Message:' => 'Message:',
    'SACFor' => 'Shipping Address Change For',
    'Address:' => 'Address:',
    'tickList1' => 'Ticket Listing Confirmation',
    'tickList2' => 'Hi',
    'tickList3' => 'Thank you very much for you listing the tickets for sale for the',
    'tickList4' => 'game taking place on',
    'tickList5' => '. Please find below details of the ticket(s).',
    'tickList6' => 'Form of Ticket:',
    'tickList7' => 'Location:',
    'tickList8' => 'Restrictions:',
    'tickList9' => 'No restrictions specified.',
    'tickList10' => 'Commission breakdown:',
    'tickList11' => 'Number of Ticket Listed:',
    'tickList12' => 'Selling Price (Per Ticket):',
    'tickList13' => 'Total Selling Price:',
    'tickList14' => 'Selling Fees',
    'tickList15' => 'Net Payment:',
    'tickList16' => 'If you have any questions about your account or any other matter, please feel free to contact us at info@footballticketpad.com',
    'tickList17' => 'To view a list of FAQs please click',
    'tickList18' => 'here',
    'tickList19' => 'Thank you again, Football Ticket Pad',
    'weirdrusto' => '',
    'frticbefore' => '',
	'England' => 'England',
	'Wales' => 'Wales',
	'Reset Filter Button' => 'Reset Filter',
	'Italy' => 'Italy',
	'Sweden' => 'Sweden',
	'VS' => 'vs',
	'Russia' => 'Russia',
	'Belgium' => 'Belgium',
	'Republic of Ireland'=> 'Republic of Ireland',
	'France' => 'France',
	'Romania' => 'Romania',
	'Switzerland' => 'Switzerland',
	'Slovakia' => 'Slovakia',
	'Germany' => 'Germany',
	'Northern Ireland' => 'Northern Ireland',
	'Poland' => 'Poland',
	'Ukraine' => 'Ukraine',
	'Croatia' => 'Croatia',
	'Czech Republic' => 'Czech Republic',
	'mapsmayvairy' => 'Maps may vary from official categories.',
	'Spain'=>'Spain',
	'Turkey'=>'Turkey',
	'Iceland' => 'Iceland',
	'Hungary' => 'Hungary',
	'Arsenal'=>'Arsenal',
	'Burnley'=>'Burnley',
	'affilFinances' => 'Finances',
	'affilPaymentHistory'=>'Payment History',
	'affilID' => 'ID',
	'affilRequestedFor'=>'Requested For',
	'affilAmount' => 'Amount',
	'affilStatus' => 'Status',
	'affilActionedOn' => 'Actioned On',
	'affilMyBalance' => 'My Balance',
	'affilBalanceSummary' => 'Balance - Summary',
	'affilYourBalanceIs' => 'Your balance is',
	'affilProgressTillPayout' => 'Progress till payout',
	'affilPTWM' => '% Through withdrawal minimum',
	'affilPaymentDates' => 'Payment Dates',
	'affilActiveWithdrawalRequest' => 'Active Withdrawal Request',
	'affilWithdrawalAmount'=> 'Withdrawal Amount',
	'affilQuickUpdate' => 'Quick Update',
	'affilCancel' => 'Cancel',
	'affilSubmit'=>'Submit',
	'affilCreateWithdrawalRequest'=>'Create Withdrawal Request',
	'affilPUYBD'=>'Please update your bank account details in the',
	'affilmyAccount'=>'my account',
	'affilSBPAWR'=>'section before placing a withdrawal request.',
	'affilCurrentCommissionRate'=>'Current Commission Rate',
	'affilRateName'=>'Rate Name',
	'affilCurrentCommission' => 'Current Commission',
	'affilAffiliateDashboard' => 'Affiliate Dashboard',
	'affilOverview' => 'Overview',
	'affilMonthlyRecapReport' => 'Monthly Recap Report',
	'affilSalesC' => 'Sales:',
	'affilSalesTotal' => 'Sales Total',
	'affilSalesCommission' => 'Sales Commission',
	'affilAvgCom' => 'Average Commission %',
	'affilCurCom' => 'Current Commission %',
	'affilLatestOrders' => 'Latest Orders',
	'affilOrderID' => 'Order ID',
	'affilItem' => 'Item',
	'affilDate' => 'Date',
	'affilTotalSalePrice' => 'Total Sale Price',
	'affilComPerc' => 'Commission %',
	'affilComVal' => 'Commission Value',
	'affilRefresh' => 'Refresh',
	'affilViewAllOrders' => 'View All Orders',
	'affilProductFeed' => 'Product Feed',
	'affilGettingStartedGuide' => 'Getting Started Guide',
	'affilPFG1' => "On this page you will find forms that will generate URL's with which you can access your product feed.",
	'affilPFG2' => 'The product feed is an XML data feed that contains all of the information required for you to become a reseller by providing events and associated tickets and prices',
	'affilPFG3' => "The URL's take the following structure:",
	'affilYRC' => 'Your reference code is:',
	'affilPFG4' => 'Visiting the following link will take you to your default product feed, fine for most purposes:',
	'affilPFG5' => 'Requests to this URL can be made using either the POST or GET method with additional parameters being handled correctly in both instances.',
	'affilAdditionalParameters' => 'Additional Parameters',
	'affilPFG6' => "group_by_event : Default enabled. This will return all events with active listings along with just the cheapest listing available for that event. When disabled this will return many instances of each event with listing objects attatched.<br>",
	'affilPFG7' => 'Set this to 0 to disable.',
	'affilPFG8' => 'order_dir : Default ASC. Events are ordered by their event date in either ascending (ASC) or descending (DESC) order.',
	'affilGFU' => 'Generate Feed URL',
	'affilGPBE' => 'Group products by event?',
	'affilOPBD' => 'Order products by date:',
	'affilAscending' => 'Ascending',
	'affilDescending' => 'Descending',
	'affilProfile' => 'Profile',
	'affilUpdateContactName' => 'Update Contact Name',
	'affilCName' => 'Contact Name:',
	'affilUpdatePassword' => 'Update Password',
	'affilPassword' => 'Password:',
	'affilConfirmC' => 'Confirm:',
	'affilUPD' => 'Update Payment Details',
	'affilAccountNumber' => 'Account Number:',
	'affilSortCode' => 'Sort Code:',
	'affilIBS' => 'IBAN/BIC/SWIFT',
	'affilSales' => 'Sales',
	'affilTotals' => 'Totals',
	'affilTotalNumberOfSales' => 'Total Number of Sales',
	'affilTotalSaleValue' => 'Total Sale Value',
	'affilACP' => 'Average Commission Percentage',
	'affilTCE' => 'Total commission earned',
	'affilSalesHistory' => 'Sales History',
	'affilProductInformation' => 'Product Information',
	'affilCommissionPercentage' => 'Commission Percentage',
	'affilCommissionAmount' => 'Commission Amount',
	'affilBecomeAPartner' => 'Become a Partner',
	'affilYEA' => 'Your Email Address',
	'affilCSU' => 'Company Site URL',
	'affilCompany Name' => 'Company Name',
	'affilYourName' => 'Your Name',
	'affilRegSuccess' => 'Thank you, we are reviewing your request and will email you with further instructions.',
	'affilLoginTitle' => 'Login to Affiliate Portal',
	'affilEmail' => 'Email',
	'affilPasswordC' => 'Password:',
	'affilLogin' => 'Login',
	'affilRememberMe' => 'Remember Me',
	'affilVerifyYourDomain' => 'Verify Your Domain',
	'affilCompanySiteURL' => 'Company Site URL',
	'affilRegSuccess2' => 'Thank you, a final email has been sent to you with your login credentials.',
	'affiltoggleNavigation' => 'Toggle Navigation',
	'affilSignOut' => 'Sign Out',
	'affilCopyright' => 'Copyright',
	'affilAllRightsReserved' => 'All rights reserved.',
	'affilSalesEarningC' => 'sales earning:',
	'affilIYB' => 'into your balance',
	'affilPMF' => 'Payment made for:',
	'affilOOYB' => 'out of your balance.',
	'affilWDE1' => 'Your withdrawal request has been completed!',
	'affilWDE2' => 'Hello',
	'affilWDE3' => 'We have just processed your withdrawal request and your funds have been transferred into the bank account associated with your affiliate account.',
	'affilWDE4' => 'Please be aware, transfers may take up to 3 working days to appear into your account.',
	'affilWDE5' => 'Amount Transferred:',
	'affilWDE6' => 'Your Current Balance:',
	'affilWDE7' => 'Thank you,',
	'affilWDCE1' => 'Your withdrawal request has been cancelled.',
	'affilWDCE2' => 'Hello',
	'affilWDCE3' => 'We have just processed your withdrawal request and unfortunately it has been cancelled.',
	'affilWDCE4' => 'For more information please feel free to contact us and we will be happy to inform you of the process that led to this decision.',
	'affilWDCE5' => 'Withdrawal Request ID:',
	'affilWDCE6' => 'Withdrawal Amount:',
	'affilWDCE7' => 'Thank you,',
	'affilACE1' => 'Your affiliate account is ready!',
	'affilACE2' => 'Hello',
	'affilACE3' => "Your site has been verified and you are ready to log in to your account. Here's how to access and secure your account.",
	'affilACE4' => '1 - Go to the following URL and log in using the details below',
	'affilACE5' => 'Email:',
	'affilACE6' => 'Temporary Password:',
	'affilACE7' => '2 - Change your password to something a little more personal and secure in the "My Account" section of the affiliate dashboard',
	'affilACE8' => '3 - Follow our step by step guide to implement our product feed and to receive an introduction to our affiliate dashboard',
	'affilACE9' => 'Thank you,',
	'affilARA1' => 'Your affiliate request has been approved',
	'affilARA2' => 'Hello',
	'affilARA3' => 'Your request to be an affiliate partner with us has been approved. Below you will find instructions detailing how to activate your account.',
	'affilARA4' => '1 - Enter the following code snippet into the head tag of your home page',
	'affilARA5' => 'This should be done by your web developer. This is required so that we can verify your ownership of the site.',
	'affilARA6' => '2 - Visit the following url to activate your account',
	'affilARA7' => '3 - Feel free to remove the code snippet from your page at this point',
	'affilARA8' => 'Thank you,',
	'affilARC1' => 'Your affiliate request has been cancelled',
	'affilARC2' => 'Hello',
	'affilARC3' => 'We are sorry to inform you that your inactive affiliate account has been cancelled.',
	'affilARC4' => 'If you would still like to be an affiliate then please start the registration process again and make sure that your account is activated within a reasonable time frame.',
	'affilARC5' => 'Thank you,',
	'affilARD1' => 'Your affiliate request has not been approved',
	'affilARD2' => 'Hello',
	'affilARD3' => 'We are sorry to inform you that your affiliate request has not been approved at this time.',
	'affilARD4' => 'Thank you,',
	'affilDaysTillPayout' => 'Days Till Payout',
	'affilAmountC' => 'Amount:',
	'reservation_name' => 'Reservation Name',
	'affilNumberOfReferrals' => 'Number of Referrals',
	'become-a-partner' => 'Become a Partner',
	'currencyAfter' => '',
	'affiliate-dashboard' => 'Affiliate Dashboard',
	'reset-filter' => 'Reset Filter',
	'apply-a-filter' => 'Apply a Filter',
	'Premier League' => 'Premier League',
	'Bundesliga' => 'Bundesliga',
	'La Liga' => 'La Liga',
	'Serie A' => 'Serie A',
	'viewThisPage' => 'Ver está pagina en nuestro',
	'woops' => 'Whoops, No podría estar en la mejor versión del sitio',
	'close' => 'Cerrar',
	'notCorrect' => 'No correcto, Haz Clic aqui',
	'langiosTwoThree' => 'ES EC',
	'language' => 'Idioma',
	'currency' => 'Moneda',
	'currentCountry' => 'Spain',
	'country' => 'Pais',
	'pleaseSelectYourLocale' => 'Por Favor, Elegir su lugar',
	'Club Bio'=>'Bio Del Club',
	'View From Seat'=>'Vista desde el asiento',
	'faq-category-1' => '',
	'faq-category-2' => '',
	'countrycodefefault' => 'CH'
);
	foreach ($translations as $singleTranslate) {
		if ($singleTranslate->text != "") {
			$fullTranslateList[$singleTranslate->node] = "" . $singleTranslate->text;
		} else {
			$fullTranslateList[$singleTranslate->node] = "";
		}
	}

	// $fullTranslateList['langiosTwoThree'] = 'JA JA';

	Cache::add($lang,$fullTranslateList,2000);
	return $fullTranslateList;
}
