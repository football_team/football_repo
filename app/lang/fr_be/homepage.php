<?php
$translations = DB::table('trans_text')
	->where('lang', '=', 'fr')
	->leftJoin('trans', 'trans_text.node_id', '=', 'trans.id')
	->remember(120)
	->get();
$fullTranslateList = [];
foreach ($translations as $singleTranslate) {
	if($singleTranslate->text !="") {
		$fullTranslateList[$singleTranslate->node] = "" . $singleTranslate->text;
	}else{
		$fullTranslateList[$singleTranslate->node] = "";
	}
}

$fullTranslateList['viewThisPage'] = 'Rester sur le site Ticket Pad Belgique';
$fullTranslateList['woops'] = 'Oups, vous n\'êtes peut-être pas sur la meilleure version du site';
$fullTranslateList['close'] = 'Fermer';
$fullTranslateList['notCorrect'] = 'Incorrect ? Cliquez ici';
$fullTranslateList['langiosTwoThree'] = 'FR BE';
$fullTranslateList['language'] = 'Langue ';
$fullTranslateList['currency'] = 'Devise ';
$fullTranslateList['currentCountry'] = 'Belgium';
$fullTranslateList['country'] = 'Pays';
$fullTranslateList['pleaseSelectYourLocale'] = 'S\'il vous plais choisissez votre locale';
$fullTranslateList['ggtrackingcode']='UA-72160356-1';

return $fullTranslateList;
