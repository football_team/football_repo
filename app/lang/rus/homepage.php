<?php
$translations = DB::table('trans_text')
	->where('lang', '=', 'ru')
	->leftJoin('trans', 'trans_text.node_id', '=', 'trans.id')
	->remember(120)
	->get();
$fullTranslateList = [];
foreach ($translations as $singleTranslate) {
	if($singleTranslate->text !="") {
		$fullTranslateList[$singleTranslate->node] = "" . $singleTranslate->text;
	}else{
		$fullTranslateList[$singleTranslate->node] = "";
	}
}
$fullTranslateList['viewThisPage'] = 'Продолжить Ticketpad Россия ';
$fullTranslateList['woops'] = 'Ой, скорее всего Вы не на лучшей версии сайта';
$fullTranslateList['close'] = 'закрыть';
$fullTranslateList['notCorrect'] = 'Неверно? Нажмите здесь';
$fullTranslateList['langiosTwoThree'] = 'RU';
$fullTranslateList['language'] = 'Язык ';
$fullTranslateList['currency'] = 'Валюта ';
$fullTranslateList['currentCountry'] = 'Russia';
$fullTranslateList['country'] = 'Страна';
$fullTranslateList['pleaseSelectYourLocale'] = 'Пожалуйста, выберите Ваш домен';

$fullTranslateList['currencyInUse'] ='';
$fullTranslateList['countrycodefefault'] = 'RU';
$fullTranslateList['ggtrackingcode']='UA-70049536-1';
return $fullTranslateList;
