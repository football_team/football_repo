<?php
$translations = DB::table('trans_text')
	->where('lang', '=', 'ru')
	->leftJoin('trans', 'trans_text.node_id', '=', 'trans.id')
	->remember(120)
	->get();
$fullTranslateList = [];
foreach ($translations as $singleTranslate) {
	if($singleTranslate->text !="") {
		$fullTranslateList[$singleTranslate->node] = "" . $singleTranslate->text;
	}else{
		$fullTranslateList[$singleTranslate->node] = "";
	}
}
$fullTranslateList['viewThisPage'] = 'Продолжить Ticketpad Беларусь';
$fullTranslateList['woops'] = 'Ой, скорее всего Вы не на лучшей версии сайта';
$fullTranslateList['close'] = 'закрыть';
$fullTranslateList['notCorrect'] = 'Не верно? Нажмите здесь';
$fullTranslateList['langiosTwoThree'] = 'RU BY';
$fullTranslateList['language'] = 'Язык ';
$fullTranslateList['currency'] = 'Валюта ';
$fullTranslateList['currentCountry'] = 'Belarus';
$fullTranslateList['country'] = 'Страна';
$fullTranslateList['pleaseSelectYourLocale'] = 'Пожалуйста, выберете Ваш домен';
$fullTranslateList['currencyInUse'] ='';
$fullTranslateList['countrycodefefault'] = 'RU';
$fullTranslateList['ggtrackingcode']='UA-72187655-1';
return $fullTranslateList;

