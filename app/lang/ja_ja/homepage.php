<?php
$lang = "lang_JA";
if (Cache::has($lang)) {
	return Cache::get($lang);

} else {
	$translations = DB::table('trans_text')
		->where('lang', '=', 'ja')
		->leftJoin('trans', 'trans_text.node_id', '=', 'trans.id')
		->remember(120)
		->get();
	$fullTranslateList = [];
	foreach ($translations as $singleTranslate) {
		if ($singleTranslate->text != "") {
			$fullTranslateList[$singleTranslate->node] = "" . $singleTranslate->text;
		} else {
			$fullTranslateList[$singleTranslate->node] = "";
		}
	}

	// $fullTranslateList['langiosTwoThree'] = 'JA JA';
	$fullTranslateList['currentCountry'] = 'JA';

	Cache::add($lang,$fullTranslateList,2000);
	return $fullTranslateList;
}
