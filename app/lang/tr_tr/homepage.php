<?php
$lang = "lang_TR";
if (Cache::has($lang)) {

    return Cache::get($lang);

} else {
    $translations = DB::table('trans_text')
        ->where('lang', '=', 'tr')
        ->leftJoin('trans', 'trans_text.node_id', '=', 'trans.id')
        ->remember(120)
        ->get();
    $fullTranslateList = [];
    foreach ($translations as $singleTranslate) {
        if ($singleTranslate->text != "") {
            $fullTranslateList[$singleTranslate->node] = "" . $singleTranslate->text;
        } else {
            $fullTranslateList[$singleTranslate->node] = "";
        }
    }

    // $fullTranslateList['langiosTwoThree'] = 'TR TR';
    $fullTranslateList['currentCountry'] = 'TR';

    Cache::add($lang,$fullTranslateList,2000);
    return $fullTranslateList;
}
