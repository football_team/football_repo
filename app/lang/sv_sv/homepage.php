<?php
$lang = "lang_SV";
if (Cache::has($lang)) {

	return Cache::get($lang);

} else {
	$translations = DB::table('trans_text')
		->where('lang', '=', 'SV')
		->leftJoin('trans', 'trans_text.node_id', '=', 'trans.id')
		->remember(120)
		->get();
	$fullTranslateList = [];
	foreach ($translations as $singleTranslate) {
		if ($singleTranslate->text != "") {
			$fullTranslateList[$singleTranslate->node] = "" . $singleTranslate->text;
		} else {
			$fullTranslateList[$singleTranslate->node] = "";
		}
	}

	$fullTranslateList['viewThisPage'] = '';
	$fullTranslateList['woops'] = '';
	$fullTranslateList['close'] = '';
	$fullTranslateList['notCorrect'] = '';
	$fullTranslateList['langiosTwoThree'] = 'SV SV';
	$fullTranslateList['language'] = '';
	$fullTranslateList['currency'] = '';
	$fullTranslateList['currentCountry'] = 'SV';
	$fullTranslateList['country'] = '';
	$fullTranslateList['pleaseSelectYourLocale'] = '';

	Cache::add($lang,$fullTranslateList,2000);
	return $fullTranslateList;
}
