<?php
$translations = DB::table('trans_text')
	->where('lang', '=', 'fr')
	->leftJoin('trans', 'trans_text.node_id', '=', 'trans.id')
	->remember(120)
	->get();
$fullTranslateList = [];
foreach ($translations as $singleTranslate) {
	if($singleTranslate->text !="") {
		$fullTranslateList[$singleTranslate->node] = "" . $singleTranslate->text;
	}else{
		$fullTranslateList[$singleTranslate->node] = "";
	}
}
$fullTranslateList['viewThisPage'] = 'Rester sur le site Ticket Pad Luxembourg';
$fullTranslateList['woops'] = 'Oups, vous n\'êtes peut-être pas sur la meilleure version du site';
$fullTranslateList['close'] = 'Fermer';
$fullTranslateList['notCorrect'] = 'Incorrect ? Cliquez ici';
$fullTranslateList['langiosTwoThree'] = 'FR LU';
$fullTranslateList['language'] = 'Langue ';
$fullTranslateList['currency'] = 'Devise ';
$fullTranslateList['currentCountry'] = 'Luxembourg';
$fullTranslateList['country'] = 'Pays';
$fullTranslateList['pleaseSelectYourLocale'] = 'S\'il vous plais choisissez votre locale';
$fullTranslateList['countrycodefefault'] = 'FR';
$fullTranslateList['ggtrackingcode']='UA-72164957-1';
return $fullTranslateList;
