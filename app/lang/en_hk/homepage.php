<?php

$lang = "lang_HK2";
if (Cache::has($lang)) {

	return Cache::get($lang);

} else {
	$translations = DB::table('trans_text')
		->where('lang', '=', 'EN')
		->leftJoin('trans', 'trans_text.node_id', '=', 'trans.id')
		->remember(120)
		->get();
	$fullTranslateList = [];
	foreach ($translations as $singleTranslate) {
		if ($singleTranslate->text != "") {
			$fullTranslateList[$singleTranslate->node] = "" . $singleTranslate->text;
		} else {
			$fullTranslateList[$singleTranslate->node] = "";
		}
	}

	$fullTranslateList['viewThisPage'] = 'Continue on Ticket Pad Hong Kong';
	$fullTranslateList['woops'] = 'Woop\'s You might not be on the best version on the site';
	$fullTranslateList['close'] = 'close';
	$fullTranslateList['notCorrect'] = 'Not correct?  Click here';
	$fullTranslateList['langiosTwoThree'] = 'ENG HK';
	$fullTranslateList['language'] = 'Language ';
	$fullTranslateList['currency'] = 'Currency ';
	$fullTranslateList['currentCountry'] = 'HongKong ';
	$fullTranslateList['country'] = 'Country';
	$fullTranslateList['pleaseSelectYourLocale'] = 'Please Select Your Locale';

	$fullTranslateList['gglSiteVer'] =  'Bh0xvoTTl7Vls0HgLx1A5BYVB3uezPG94Q47ooeHrJ8';
	$fullTranslateList['ggtrackingcode'] = 'UA-72174046-1';
	$fullTranslateList['tnumber'] = '+44 (0)20 79936171';
	$fullTranslateList['zopim-code'] = '2q52Bs9lVyDi0nlODLQrUfMbUJepajol ';
	$fullTranslateList['contact-email'] = 'Ireland ';
	$fullTranslateList['currencyInUse'] = 'HK&#36;';
	$fullTranslateList['contact-email'] = 'info@footballticketpad.com ';
	$fullTranslateList['Email'] = 'Email ';
	$fullTranslateList['Email'] = 'EMAIL ';
	$fullTranslateList['Telephone'] = 'Telephone ';

	$fullTranslateList['countrycodefefault'] = 'HK';
	Cache::add($lang,$fullTranslateList,2000);
	return $fullTranslateList;
}