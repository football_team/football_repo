@if (!isset($type))
	{{$type=''}}
@endif
@if(empty($menu))
	{{$menu=''}}
@endif
<nav >
	<ul >
		<li class="{{ $menu == 'dashboard'?'active': '' }}" >
			<a href="/{{ Config::get('bondcms.admin_prefix') }}" title="Dashboard" ><i class="fa fa-lg fa-fw fa-home" ></i >
				<span class="menu-item-parent" >Dashboard</span ></a >
		</li >
		@yield('nev_menu_after_dashboard')
		<li class="{{ in_array($menu, array('article', 'article/create', 'article/edit'))? 'active':'' }}" >
			<a href="#" ><i class="fa fa-lg fa-fw fa fa-align-left" ></i >
				<span class="menu-item-parent" >Article</span ></a >
			<ul >
				<li class="{{ $menu == 'article'?'active': '' }}" >
					<a href="/{{ Config::get('bondcms.admin_prefix') }}/article" >View Articles</a >
				</li >
				@if($menu == 'article/edit')
					<li class="{{ $menu == 'article/edit'?'active': '' }}" >
						<a href="/{{ Config::get('bondcms.admin_prefix') }}/article/edit" >Edit</a >
					</li >

				@else
					<li class="{{ $menu == 'article/create'?'active': '' }}" >
						<a href="/{{ Config::get('bondcms.admin_prefix') }}/article/create" >Add New</a >
					</li >
				@endif
			</ul >
		</li >
		<li class="{{ $menu == 'affiliates/config'?'active': '' }}" >
			<a href="#" ><i class="fa fa-lg fa-fw fa-list-ul" ></i > <span class="menu-item-parent" >Automated Tests</span ></a >
			<ul >
				<li ><a href="/admin/automate/view/checkout" >View Checkout Tests</a ></li >
				<li ><a href="/admin/automate/testlist" >Run Checkout Test</a ></li >
			</ul>
		</li >
		<li class="" >
			<a href="#" ><i class="fa fa-lg fa-map-marker " ></i >
				<span class="menu-item-parent" >Map Builder</span ></a >
			<ul >
				<li >
					<a href="#" title="Dashboard" >BUILDER TOOLS</a >
					<ul >
						<li ><a href="/admin/map/select" title="Dashboard" >LIST ALL</a ></li >
						<li ><a href="/admin/map/new/create" title="Dashboard" >CREATE NEW</a ></li >
						<li ><a href="/admin/map/remove" title="Dashboard" >REMOVE MAP</a ></li >
					</ul >
				</li >
				<li >
					<a href="#" title="Dashboard" >CAT TOOLS</a >
					<ul >
						<li ><a href="/admin/map/compare/" title="Dashboard" >LIST ALL</a ></li >
						<li ><a href="/admin/map/compare/new/" title="Dashboard" >CREATE NEW</a ></li >
					</ul >
				</li >
			</ul >
		</li >
		<li class="" >
			<a href="#" ><i class="fa fa-cog fa-spin " ></i > <span class="menu-item-parent" >Translator</span ></a >
			<ul >
				<li ><a href="/admin/translate/es" title="Dashboard" >Spanish</a ></li >
				<li ><a href="/admin/translate/ru" title="Dashboard" >Russian</a ></li >
				<li ><a href="/admin/translate/fr" title="Dashboard" >French</a ></li >
				<li ><a href="/admin/translate/it" title="Dashboard" >Italian</a ></li >
				<li ><a href="/admin/translate/is" title="Dashboard" >Icelandic</a ></li >
				<li ><a href="/admin/translate/sv" title="Dashboard" >Swedish</a ></li >
				<li ><a href="/admin/translate/fi" title="Dashboard" >Finnish</a ></li >
				<li ><a href="/admin/translate/dk" title="Dashboard" >Danish</a ></li >
				<li ><a href="/admin/translate/no" title="Dashboard" >Norwegian</a ></li >
				<li ><a href="/admin/translate/de" title="Dashboard" >German</a ></li >
				<li ><a href="/admin/translate/nl" title="Dashboard" >Dutch</a ></li >
				<li ><a href="/admin/translate/tr" title="Dashboard" >Turkish</a ></li >
				<li ><a href="/admin/translate/kr" title="Dashboard" >Korean</a ></li >
				<li ><a href="/admin/translate/cz" title="Dashboard" >Czech</a ></li >
				<li ><a href="/admin/translate/th" title="Dashboard" >Thai</a ></li >
				<li ><a href="/admin/translate/pt" title="Dashboard" >Portugese</a ></li >
				<li ><a href="/admin/translate/ja" title="Dashboard" >Japanese</a ></li >
				<li ><a href="/admin/translate/cn" title="Dashboard" >Chinese</a ></li >
			</ul >
		</li >
		<li class="" >
			<a href="#" ><i class="fa fa-list" ></i > <span class="menu-item-parent" >Event Log</span ></a >
			<ul >
				<li >
					<a href="/admin/log/translate" title="Dashboard" >Translator</a ></li >
				<li >
					<a href="/admin/log/events" title="Dashboard" >Events</a ></li >
				<li >
					<a href="/admin/log/migration" title="Dashboard" >Migration Log</a ></li >
				<li ><a href="/admin/log" title="Dashboard" >Error</a ></li >
			</ul >
		</li >
		<li class="{{ in_array($menu, array('redirector'))? 'active':'' }}" >
			<a href="/admin/redirector" ><i class="fa fa-cog fa-spin " ></i > <span class="menu-item-parent" >Redirector</span ></a >
		</li >
		@yield('nev_menu_after_article')
		<li class="{{ in_array($menu, array('category', 'category/create', 'category/edit'))? 'active':'' }}" >
			<a href="#" ><i class="fa fa-lg fa-fw fa-tag" ></i > <span class="menu-item-parent" >Category</span ></a >
			<ul >
				<li class="{{ $menu == 'category'?'active': '' }}" >
					<a href="/{{ Config::get('bondcms.admin_prefix') }}/category" >View Categories</a >
				</li >
				@if($menu == 'category/edit')
					<li class="{{ $menu == 'category/edit'?'active': '' }}" >
						<a href="/{{ Config::get('bondcms.admin_prefix') }}/category/edit" >Edit</a >
					</li >

				@else
					<li class="{{ $menu == 'category/create'?'active': '' }}" >
						<a href="/{{ Config::get('bondcms.admin_prefix') }}/category/create" >Add New</a >
					</li >
				@endif
			</ul >
		</li >
		@yield('nev_menu_after_category')
		<li class="{{ in_array($menu, array('pages', 'pages/new', 'pages/edit'))? 'active':'' }}" >
			<a href="#" ><i class="fa fa-lg fa-fw fa-pencil" ></i > <span class="menu-item-parent" >Pages</span ></a >
			<ul >
				<li class="{{ $menu == 'pages'?'active': '' }}" >
					<a href="/{{ Config::get('bondcms.admin_prefix') }}/pages" >View Pages</a >
				</li >
				@if($menu == 'pages/edit')
					<li class="{{ $menu == 'pages/edit'?'active': '' }}" >
						<a href="/{{ Config::get('bondcms.admin_prefix') }}/pages/edit" >Edit</a >
					</li >

				@else
					<li class="{{ $menu == 'pages/new'?'active': '' }}" >
						<a href="/{{ Config::get('bondcms.admin_prefix') }}/pages/new" >Add New</a >
					</li >
				@endif
			</ul >
		</li >
		<!-- @yield('nev_menu_after_pages')
			<li class="{{ in_array($menu, array('news', 'news/new', 'news/edit'))? 'active':'' }}">
            <a href="#"><i class="fa fa-lg fa-fw fa-desktop"></i> <span class="menu-item-parent">News</span></a>
            <ul>
                <li class="{{ $menu == 'news'?'active': '' }}">
                    <a href="/{{ Config::get('bondcms.admin_prefix') }}/news">View News</a>
                </li>
                @if($menu == 'news/edit')
			<li class="{{ $menu == 'news/edit'?'active': '' }}" >
                    <a href="/{{ Config::get('bondcms.admin_prefix') }}/news/edit">Edit</a>
                </li>

                @else
			<li class="{{ $menu == 'news/new'?'active': '' }}" >
                    <a href="/{{ Config::get('bondcms.admin_prefix') }}/news/create">Add New</a>
                </li>
                @endif
			</ul>
		</li>

		-->
		<li class="{{ in_array($menu, array('faq', 'faq/new', 'faq/edit'))? 'active':'' }}" >
			<a href="#" ><i class="fa fa-lg fa-fw fa-question" ></i > <span class="menu-item-parent" >Faq</span ></a >
			<ul >
				<li class="{{ $menu == 'faq'?'active': '' }}" >
					<a href="/{{ Config::get('bondcms.admin_prefix') }}/faq" > View Faq</a >
				</li >
				@if($menu == 'faq/edit')
					<li class="{{ $menu == 'faq/edit'?'active': '' }}" >
						<a href="/{{ Config::get('bondcms.admin_prefix') }}/faq/edit" >Edit</a >
					</li >

				@else
					<li class="{{ $menu == 'faq/new'?'active': '' }}" >
						<a href="/{{ Config::get('bondcms.admin_prefix') }}/faq/create" >Add New</a >
					</li >
				@endif
			</ul >
		</li >
		@yield('nev_menu_after_news')
		<li class="{{ in_array($menu, array('media', 'media/new'))? 'active':'' }}" >
			<a href="#" ><i class="fa fa-lg fa-fw fa-picture-o" ></i >
				<span class="menu-item-parent" >Media</span ></a >
			<ul >
				<li class="{{ $menu == 'media'?'active': '' }}" >
					<a href="/{{ Config::get('bondcms.admin_prefix') }}/media" >View Media</a >
				</li >
				<li class="{{ $menu == 'media/list'?'active': '' }}" >
					<a href="/{{ Config::get('bondcms.admin_prefix') }}/media/list" >List</a >
				</li >
				@if($menu == 'media/edit')
					<li class="{{ $menu == 'media/edit'?'active': '' }}" >
						<a href="/{{ Config::get('bondcms.admin_prefix') }}/media/edit" >Edit</a >
					</li >
				@endif
			</ul >
		</li >
		@yield('nev_menu_after_media')
		<li class="{{ in_array($menu, array('appearance', 'menu', 'appearance/themes', 'appearance/header'))? 'active':'' }}" >
			<a href="#" ><i class="fa fa-lg fa-fw fa-desktop" ></i > <span class="menu-item-parent" >Appearance</span ></a >
			<ul >
				<li class="{{ $menu == 'menu'?'active': '' }}" >
					<a href="/{{ Config::get('bondcms.admin_prefix') }}/menu" >Menu</a >
				</li >
				<!--                <li class="{{ $menu == 'appearance/themes'?'active': '' }}" >-->
				<!--                    <a href="/{{ Config::get('bondcms.admin_prefix') }}/appearance/themes">Themes</a>-->
				<!--                </li>-->
				<!---->
				<!--                <li class="{{ $menu == 'appearance/header'?'active': '' }}" >-->
				<!--                    <a href="/{{ Config::get('bondcms.admin_prefix') }}/appearance/header">Header</a>-->
				<!--                </li>-->
			</ul >
		</li >
		@yield('nev_menu_after_appearance')
		<li class="{{ $type != 'block' && in_array($menu, array('slider', 'slider/new', 'slider/edit'))? 'active':'' }}" >
			<a href="#" ><i class="fa fa-lg fa-fw fa-th-large" ></i >
				<span class="menu-item-parent" >Slider</span ></a >
			<ul >
				<li class="{{ $type != 'block' && $menu == 'slider'?'active': '' }}" >
					<a href="/{{ Config::get('bondcms.admin_prefix') }}/slider" >View Slider</a >
				</li >
				@if($menu == 'gallery/edit')
					<li class="{{ $type != 'block' &&$menu == 'slider/edit'?'active': '' }}" >
						<a href="/{{ Config::get('bondcms.admin_prefix') }}/slider/edit" >Edit</a >
					</li >

				@else
					<li class="{{ $type != 'block' &&$menu == 'slider/new'?'active': '' }}" >
						<a href="/{{ Config::get('bondcms.admin_prefix') }}/slider/create" >Add New</a >
					</li >
				@endif
			</ul >
		</li >
		<li class="{{ $type == 'block' && in_array($menu, array('slider', 'slider/new', 'slider/edit'))? 'active':'' }}" >
			<a href="#" ><i class="fa fa-lg fa-fw fa-cubes" ></i >
				<span class="menu-item-parent" >BlockPress</span ></a >
			<ul >
				<li class="{{ $type == 'block' && $menu == 'slider'?'active': '' }}" >
					<a href="/{{ Config::get('bondcms.admin_prefix') }}/slider?type=block" >View Blocks</a >
				</li >
				@if($menu == 'gallery/edit')
					<li class="{{ $type == 'block' && $menu == 'slider/edit'?'active': '' }}" >
						<a href="/{{ Config::get('bondcms.admin_prefix') }}/slider/edit?type=block" >Edit Block</a >
					</li >

				@else
					<li class="{{ $type == 'block' && $menu == 'slider/new'?'active': '' }}" >
						<a href="/{{ Config::get('bondcms.admin_prefix') }}/slider/create?type=block" >New Block</a >
					</li >
				@endif
			</ul >
		</li >
		@yield('nev_menu_after_slider')
		<li class="{{ in_array($menu, array('filemanager'))? 'active':'' }}" >
			<a href="#" ><i class="fa fa-lg fa-fw fa-files-o" ></i >
				<span class="menu-item-parent" >File Manager</span ></a >
			<ul >
				<li class="{{ $menu == 'filemanager'?'active': '' }}" >
					<a href="/{{ Config::get('bondcms.admin_prefix') }}/filemanager/show-admin" >Show</a >
				</li >
			</ul >
		</li >
		@yield('nev_menu_after_filemanager')
		<li class="{{ in_array($menu, array('users', 'users/new', 'users/edit'))? 'active':'' }}" >
			<a href="#" ><i class="fa fa-lg fa-fw fa-user" ></i > <span class="menu-item-parent" >Users</span ></a >
			<ul >
				<li class="{{ $menu == 'users'?'active': '' }}" >
					<a href="/{{ Config::get('bondcms.admin_prefix') }}/user" >Users</a >
				</li >
				@if($menu != 'users/edit')
					<li class="{{ $menu == 'users/new'?'active': '' }}" >
						<a href="/{{ Config::get('bondcms.admin_prefix') }}/user/create" >New User</a >
					</li >
				@else
					<li class="{{ $menu == 'users/edit'?'active': '' }}" >
						<a href="/{{ Config::get('bondcms.admin_prefix') }}/#" >Edit User</a >
					</li >
				@endif
			</ul >
		</li >
		@yield('nev_menu_after_user')
		<li class="{{ in_array($menu, array('settings', 'settings/general', 'settings/website'))? 'active':'' }}" >
			<a href="#" ><i class="fa fa-lg fa-fw fa-cog" ></i > <span class="menu-item-parent" >Settings</span ></a >
			<ul >
				<li class="{{ $menu == 'settings/general'?'active': '' }}" >
					<a href="/{{ Config::get('bondcms.admin_prefix') }}/settings/general" >General</a >
				</li >
				<li class="{{ $menu == 'settings/website'?'active': '' }}" >
					<a href="/{{ Config::get('bondcms.admin_prefix') }}/settings/website" >Website</a >
				</li >
			</ul >
		</li >
		@yield('nev_menu_end')
		<li class="{{ in_array($menu, array('events', 'events/new', 'events/edit'))? 'active':'' }}" >
			<a href="#" ><i class="fa fa-lg fa-fw fa-shield" ></i > <span class="menu-item-parent" >Events</span ></a >
			<ul >
				<li class="{{ $menu == 'events'?'active': '' }}" >
					<a href="/admin/prem/2016/filter/uk/date/1/all/all/all/list" >View Events</a >
				</li >
				<li class="" >
					<a href="/admin/prem/2016/filter/uk/date/1/362/all/all/list" >View 2016 Events</a >
				</li >

				<li class="{{ $menu == 'events/migrate'?'active': '' }}" >
					<a href="/admin/prem/2016/filter/uk/date/1/50/7/all/list/" >Migrate 2015->2016</a >
				</li >
				@if($menu == 'events/edit')
					<li class="{{ $menu == 'events/edit'?'active': '' }}" >
						<a href="/{{ Config::get('bondcms.admin_prefix') }}/events/edit" >Edit</a >
					</li >

				@else
					<li class="{{ $menu == 'events/new'?'active': '' }}" >
						<a href="/{{ Config::get('bondcms.admin_prefix') }}/events/create" >Add New</a >
					</li >
				@endif
				<li class="{{ $menu == 'events/widget'?'active': '' }}" >
					<a href="/{{ Config::get('bondcms.admin_prefix') }}/widget/events" >Hot Ticket Widget</a >
				</li >
				<li class="{{ $menu == 'widget/upcoming'?'active': '' }}" >
					<a href="/{{ Config::get('bondcms.admin_prefix') }}/widget/upcoming" >Upcoming events (Home
																						  Page)</a >
				</li >
			</ul >
		</li >
		<li class="{{ in_array($menu, array('country', 'country/new', 'country/edit'))? 'active':'' }}" >
			<a href="#" ><i class="fa fa-lg fa-fw fa-flag" ></i > <span class="menu-item-parent" >Country</span ></a >
			<ul >
				<li class="{{ $menu == 'country'?'active': '' }}" >
					<a href="/{{ Config::get('bondcms.admin_prefix') }}/footballticket?action_type=country" >View
																											 Countries</a >
				</li >
				@if($menu == 'country/edit')
					<li class="{{ $menu == 'country/edit'?'active': '' }}" >
						<a href="/{{ Config::get('bondcms.admin_prefix') }}/footballticket/edit/?action_type=country" >Edit</a >
					</li >

				@else
					<li class="{{ $menu == 'country/new'?'active': '' }}" >
						<a href="/{{ Config::get('bondcms.admin_prefix') }}/footballticket/create/?action_type=country" >Add
																														 New</a >
					</li >
				@endif
			</ul >
		</li >
		<li class="{{ in_array($menu, array('club', 'club/new', 'club/edit', 'club/tabs'))? 'active':'' }}" >
			<a href="#" ><i class="fa fa-lg fa-fw fa-life-ring" ></i > <span class="menu-item-parent" >Club</span ></a >
			<ul >
				<li class="{{ $menu == 'club'?'active': '' }}" >
					<a href="/{{ Config::get('bondcms.admin_prefix') }}/footballticket?action_type=club" >View
																										  Clubs</a >
				</li >
				@if($menu == 'club/edit')
					<li class="{{ $menu == 'club/edit'?'active': '' }}" >
						<a href="/{{ Config::get('bondcms.admin_prefix') }}/footballticket/edit?action_type=club" >Edit</a >
					</li >

				@else
					<li class="{{ $menu == 'club/new'?'active': '' }}" >
						<a href="/{{ Config::get('bondcms.admin_prefix') }}/footballticket/create?action_type=club" >Add
																													 New</a >
					</li >
				@endif
				<li class="{{ $menu == 'club/widget'?'active': '' }}" >
					<a href="/{{ Config::get('bondcms.admin_prefix') }}/widget/topteam" >Top Team Widget</a >
				</li >
				<li class="{{ $menu == 'club/tabs'?'active': '' }}" >
					<a href="/{{ Config::get('bondcms.admin_prefix') }}/club/tabs/" >Club Tabs</a >
				</li >
			</ul >
		</li >
		<li class="{{ in_array($menu, array('league', 'league/new', 'league/edit'))? 'active':'' }}" >
			<a href="#" ><i class="fa fa-lg fa-fw fa-align-justify" ></i >
				<span class="menu-item-parent" >League</span ></a >
			<ul >
				<li class="{{ $menu == 'league'?'active': '' }}" >
					<a href="/{{ Config::get('bondcms.admin_prefix') }}/footballticket?action_type=league" >View
																											Leagues</a >
				</li >
				@if($menu == 'league/edit')
					<li class="{{ $menu == 'league/edit'?'active': '' }}" >
						<a href="/{{ Config::get('bondcms.admin_prefix') }}/footballticket/edit/?action_type=league" >Edit</a >
					</li >

				@else
					<li class="{{ $menu == 'league/new'?'active': '' }}" >
						<a href="/{{ Config::get('bondcms.admin_prefix') }}/footballticket/create/?action_type=league" >Add
																														New</a >
					</li >
				@endif
				<li class="{{ $menu == 'league/widget'?'active': '' }}" >
					<a href="/{{ Config::get('bondcms.admin_prefix') }}/widget/toptournament" >Top Tournament</a >
				</li >
			</ul >
		</li >
		<li class="{{ in_array($menu, array('stadium', 'stadium/create', 'stadium/edit'))? 'active':'' }}" >
			<a href="#" ><i class="fa fa-lg fa-fw fa-align-justify" ></i >
				<span class="menu-item-parent" >Stadiums</span ></a >
			<ul >
				<li class="{{ $menu == 'stadium'?'active': '' }}" >
					<a href="/{{ Config::get('bondcms.admin_prefix') }}/stadiums" >View Stadiums</a >
				</li >
				@if($menu == 'stadium/edit')
					<li class="{{ $menu == 'stadium/edit'?'active': '' }}" >
						<a href="/{{ Config::get('bondcms.admin_prefix') }}/stadiums/edit" >Edit Stadium</a >
					</li >

				@else
					<li class="{{ $menu == 'stadium/create'?'active': '' }}" >
						<a href="/{{ Config::get('bondcms.admin_prefix') }}/stadiums/create" >Add New Stadium</a >
					</li >
				@endif
			</ul >
		</li >
		<li class="{{ in_array($menu, array('season', 'season/new', 'season/edit'))? 'active':'' }}" >
			<a href="#" ><i class="fa fa-lg fa-fw fa-list-ul" ></i > <span class="menu-item-parent" >Season</span ></a >
			<ul >
				<li class="{{ $menu == 'season'?'active': '' }}" >
					<a href="/{{ Config::get('bondcms.admin_prefix') }}/footballticket?action_type=season" >View
																											Seasons</a >
				</li >
				@if($menu == 'league/edit')
					<li class="{{ $menu == 'season/edit'?'active': '' }}" >
						<a href="/{{ Config::get('bondcms.admin_prefix') }}/footballticket/edit?action_type=season" >Edit</a >
					</li >

				@else
					<li class="{{ $menu == 'season/new'?'active': '' }}" >
						<a href="/{{ Config::get('bondcms.admin_prefix') }}/footballticket/create?action_type=season" >Add
																													   New</a >
					</li >
				@endif
			</ul >
		</li >
		<li class="{{ in_array($menu, array('customer', 'customer/all', 'customer/view'))? 'active':'' }}" >
			<a href="#" ><i class="fa fa-lg fa-fw fa-list-ul" ></i >
				<span class="menu-item-parent" >Customers</span ></a >
			<ul >
				<li class="{{ $menu == 'customer'?'active': '' }}" >
					<a href="/{{ Config::get('bondcms.admin_prefix') }}/customer" >Customers</a >
				</li >
			</ul >
		</li >
		<li class="{{ in_array($menu, array('tickets', 'tickets/all', 'tickets/view', 'sold-tickets', 'e-tickets', 'deferred-orders', 'ticket-types'))? 'active':'' }}" >
			<a href="#" ><i class="fa fa-lg fa-fw fa-list-ul" ></i >
				<span class="menu-item-parent" >Tickets</span ></a >
			<ul >
				<li class="{{ $menu == 'tickets'?'active': '' }}" >
					<a href="/{{ Config::get('bondcms.admin_prefix') }}/tickets" >Tickets</a >
				</li >

				<li class="{{ $menu == ''?'active': '' }}" >
					<a href="/admin/lists/list/sellers" >Sellers Tickets</a >
				</li >

				<li class="{{ $menu == ''?'active': '' }}" >
					<a href="/admin/catmanager/" >Ticket Cat Manager</a >
				</li >


				<li class="{{ $menu == ''?'active': '' }}" >
					<a href="/admin/lists/list/brokers" >Brokers Tickets</a >
				</li >


				<li class="{{ $menu == ''?'active': '' }}" >
					<a href="/admin/lists/list/cust" >Customer Tickets</a >
				</li >

				<li class="{{ $menu == 'sold-tickets'?'active': '' }}" >
					<a href="/{{ Config::get('bondcms.admin_prefix') }}/sold-tickets" >Sold Tickets</a >
				</li >
				<li class="{{ $menu == 'deferred-orders'?'active': '' }}" >
					<a href="/{{ Config::get('bondcms.admin_prefix') }}/tickets/deferred-orders" >Deferred Orders</a >
				</li >
				<li class="{{ $menu == 'e-tickets'?'active': '' }}" >
					<a href="/{{ Config::get('bondcms.admin_prefix') }}/sold-tickets/etickets" >E-Tickets</a >
				</li >
				<li class="{{ $menu == 'ticket-types'?'active': '' }}" >
					<a href="/{{ Config::get('bondcms.admin_prefix') }}/ticket-types" >Ticket Types</a >
				</li >
			</ul >
		</li >
		<li class="{{ in_array($menu, array('finances', 'futures', 'balance', 'off-site', 'history'))? 'active':'' }}" >
			<a href="#" ><i class="fa fa-lg fa-fw fa-list-ul" ></i >
				<span class="menu-item-parent" >Finances</span ></a >
			<ul >
				<li class="{{ $menu == 'finances'?'active': '' }}" >
					<a href="/{{ Config::get('bondcms.admin_prefix') }}/finances" >Dashboard</a >
				</li >
				<li class="{{ $menu == 'history'?'active': '' }}" >
					<a href="/{{ Config::get('bondcms.admin_prefix') }}/finances/payment-history" >Payment History</a >
				</li >
				<li class="{{ $menu == 'futures'?'active': '' }}" >
					<a href="/{{ Config::get('bondcms.admin_prefix') }}/finances-futures" >Futures</a >
				</li >
				<li class="{{ $menu == 'profit-loss'?'active': '' }}" >
					<a href="/{{ Config::get('bondcms.admin_prefix') }}/balance-sheet" >Profit Loss</a >
				</li >
				<li class="{{ $menu == 'off-site'?'active': '' }}" >
					<a href="/{{ Config::get('bondcms.admin_prefix') }}/off-site" >Off Site Sales</a >
				</li >
			</ul >
		</li >
		<li class="{{ in_array($menu, array('locales', 'check-lang'))? 'active':'' }}" >
			<a href="#" ><i class="fa fa-lg fa-fw fa-list-ul" ></i >
				<span class="menu-item-parent" >Locales</span ></a >
			<ul >
				<li class="{{ $menu == 'locales'?'active': '' }}" >
					<a href="/{{ Config::get('bondcms.admin_prefix') }}/locales" >Locales</a >
				</li >
				<li class="{{ $menu == 'check-lang'?'active': '' }}" >
					<a href="/{{ Config::get('bondcms.admin_prefix') }}/language-tester" >Language Tester</a >
				</li >
			</ul >
		</li >
		<li class="{{ in_array($menu, array('affiliates/requests','affiliates/properties', 'affiliates/payment-requests', 'affiliates/closed-requests', 'affiliates/config', 'affiliates/sales', 'affiliates/referrals'))? 'active':'' }}" >
			<a href="#" ><i class="fa fa-lg fa-fw fa-list-ul" ></i > <span class="menu-item-parent" >Affiliates</span ></a >
			<ul >
				<li class="{{ $menu == 'affiliates/sales'?'active': '' }}" >
					<a href="/{{ Config::get('bondcms.admin_prefix') }}/affiliates/sales" >Affiliate Sales</a >
				</li >
				<li class="{{ $menu == 'affiliates/referrals'?'active': '' }}" >
					<a href="/{{ Config::get('bondcms.admin_prefix') }}/affiliates/referrals" >Affiliate Referrals</a >
				</li >
				<li class="{{ $menu == 'affiliates/properties'?'active': '' }}" >
					<a href="/{{ Config::get('bondcms.admin_prefix') }}/affiliates/properties" >Affiliate Properties</a >
				</li >
				<li class="{{ $menu == 'affiliates/requests'?'active': '' }}" >
					<a href="/{{ Config::get('bondcms.admin_prefix') }}/affiliates/requests" >Affiliate Requests</a >
				</li >
				<li class="{{ $menu == 'affiliates/payment-requests'?'active': '' }}" >
					<a href="/{{ Config::get('bondcms.admin_prefix') }}/affiliates/payment-requests" >Affiliate Payment Requests</a >
				</li >
				<li class="{{ $menu == 'affiliates/closed-requests'?'active': '' }}" >
					<a href="/{{ Config::get('bondcms.admin_prefix') }}/affiliates/closed-payment-requests" >Closed Payment Requests</a >
				</li >
				<li class="{{ $menu == 'affiliates/config'?'active': '' }}" >
					<a href="/{{ Config::get('bondcms.admin_prefix') }}/affiliates/config" >Config</a >
				</li >
			</ul >
		</li >

		<li class="{{ in_array($menu, array('agents/index','agents/sales'))? 'active':'' }}" >
			<a href="#" ><i class="fa fa-lg fa-fw fa-list-ul" ></i > <span class="menu-item-parent" >Agents</span ></a >
			<ul >
				<li class="{{ $menu == 'agents/index'?'active': '' }}" >
					<a href="/{{ Config::get('bondcms.admin_prefix') }}/agents/" >Agents</a >
				</li >
				<li class="{{ $menu == 'agents/sales'?'active': '' }}" >
					<a href="/{{ Config::get('bondcms.admin_prefix') }}/agents/sales" >Agent Sales</a >
				</li >
			</ul >
		</li >
	</ul >
</nav >