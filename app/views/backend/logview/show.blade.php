@extends('backend._layout.layout')
@section('content')

<h1>{{$h1}}</h1>
    <table class="table table-striped table-bordered table-hover dataTable no-footer">
        <thead >
        <th>
            ID
        </th>
        <th>
            Modual
        </th>
        <th>
            User
        </th>
        <th>
            Action
        </th>
        <th>
            Note
        </th>
        <th>
            Time
        </th>
        </thead >
        <tbody >
        @foreach($transalate as $row)
            <tr >
                <td >
                    {{$row['id']}}
                </td >
                <td >
                    {{$row['modual']}}
                </td >
                <td >
                    {{$row['user']}}
                </td >
                <td >
                    {{$row['action']}}
                </td >
                <td >
                    {{$row['note']}}
                </td >
                <td >
                    {{$row['time']}}
                </td >
            </tr >
        @endforeach
        </tbody >
    </table >


@stop
