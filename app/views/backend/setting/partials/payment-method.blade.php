<div class="control-group {{ $errors->has('payment_gateway') ? 'has-error' : '' }}">
    <label class="control-label" for="title">Payment Gateway ( Website Checkout )</label>
    <div class="controls">
        <select name="payment_gateway">
            <option value=""> - Select Payment Method - </option>
            <option value="tp" {{ @$payment_gateway == 'tp'? 'selected': ''}}> Transact pro </option>
            <option value="g2s" {{ @$payment_gateway == 'g2s'? 'selected': ''}}> Gate2Shop </option>
            <option value="ezpay" {{ @$payment_gateway == 'ezpay'? 'selected': ''}}> EZpay </option>
        </select>
        @if ($errors->first('payment_gateway'))
        <span class="help-block">{{ $errors->first('payment_gateway') }}</span>
        @endif
    </div>
</div>
<br>
