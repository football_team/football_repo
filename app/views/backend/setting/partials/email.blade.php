<!--
'contact_email',
'corporate_enquiry',
'ticket_listing_cc',
'ticket_sold_cc',
'ticket_shipped_cc'
-->
<div class="control-group {{ $errors->has('contact_email') ? 'has-error' : '' }}">
    <label class="control-label" for="title">Contact Email</label>

    <div class="controls">
        @if (isset($contact_email))
        {{ Form::text('contact_email', $contact_email, array('class'=>'form-control', 'id' => 'contact_email', 'placeholder'=>'Contact Email', 'value'=>Input::old('contact_email'))) }}
        @else
        {{ Form::text('contact_email', '' , array('class'=>'form-control', 'id' => 'contact_email', 'placeholder'=>'Contact Email', 'value'=>Input::old('contact_email'))) }}
        @endif
        @if ($errors->first('contact_email'))
        <span class="help-block">{{ $errors->first('contact_email') }}</span>
        @endif
    </div>
</div>
<br>


<!-- Basic Seller Limit  -->
<div class="control-group {{ $errors->has('corporate_enquiry') ? 'has-error' : '' }}">
    <label class="control-label" for="title">Corporate Enquiry</label>

    <div class="controls">
        @if (isset($corporate_enquiry))
        {{ Form::text('corporate_enquiry', $corporate_enquiry, array('class'=>'form-control', 'id' => 'corporate_enquiry', 'placeholder'=>'Corporate Enquiry', 'value'=>Input::old('corporate_enquiry'))) }}
        @else
        {{ Form::text('corporate_enquiry', '' , array('class'=>'form-control', 'id' => 'seller_limit', 'placeholder'=>'Corporate Enquiry', 'value'=>Input::old('corporate_enquiry'))) }}
        @endif
        @if ($errors->first('corporate_enquiry'))
        <span class="help-block">{{ $errors->first('corporate_enquiry') }}</span>
        @endif
    </div>
</div>
<br>


<!-- Basic Seller Limit  -->
<div class="control-group {{ $errors->has('ticket_listing_cc') ? 'has-error' : '' }}">
    <label class="control-label" for="title">Ticket Listing (CC) </label>

    <div class="controls">
        @if (isset($ticket_listing_cc))
        {{ Form::text('ticket_listing_cc', $ticket_listing_cc, array('class'=>'form-control', 'id' => 'ticket_listing_cc', 'placeholder'=>'Ticket Listing (CC)', 'value'=>Input::old('ticket_listing_cc'))) }}
        @else
        {{ Form::text('ticket_listing_cc', '' , array('class'=>'form-control', 'id' => 'ticket_listing_cc', 'placeholder'=>'Ticket Listing (CC)', 'value'=>Input::old('ticket_listing_cc'))) }}
        @endif
        @if ($errors->first('ticket_listing_cc'))
        <span class="help-block">{{ $errors->first('ticket_listing_cc') }}</span>
        @endif
    </div>
</div>
<br>

<!-- Ticket Sold (CC)  -->
<div class="control-group {{ $errors->has('ticket_sold_cc') ? 'has-error' : '' }}">
    <label class="control-label" for="title">Ticket Sold (CC)</label>

    <div class="controls">
        @if (isset($ticket_sold_cc))
        {{ Form::text('ticket_sold_cc', $ticket_sold_cc, array('class'=>'form-control', 'id' => 'ticket_sold_cc', 'placeholder'=>'Ticket Sold (CC)', 'value'=>Input::old('ticket_sold_cc'))) }}
        @else
        {{ Form::text('ticket_sold_cc', '' , array('class'=>'form-control', 'id' => 'ticket_sold_cc', 'placeholder'=>'Ticket Sold (CC)', 'value'=>Input::old('ticket_sold_cc'))) }}
        @endif
        @if ($errors->first('ticket_sold_cc'))
        <span class="help-block">{{ $errors->first('ticket_sold_cc') }}</span>
        @endif
    </div>
</div>
<br>

<!-- ticket_shipped_cc  -->
<div class="control-group {{ $errors->has('ticket_shipped_cc') ? 'has-error' : '' }}">
    <label class="control-label" for="title">Ticket Shipped (CC)</label>

    <div class="controls">
        @if (isset($ticket_shipped_cc))
        {{ Form::text('ticket_shipped_cc', $ticket_shipped_cc, array('class'=>'form-control', 'id' => 'ticket_shipped_cc', 'placeholder'=>'Ticket Shipped (CC)', 'value'=>Input::old('ticket_shipped_cc'))) }}
        @else
        {{ Form::text('ticket_shipped_cc', '' , array('class'=>'form-control', 'id' => 'ticket_shipped_cc', 'placeholder'=>'Ticket Shipped (CC)', 'value'=>Input::old('ticket_shipped_cc'))) }}
        @endif
        @if ($errors->first('ticket_shipped_cc'))
        <span class="help-block">{{ $errors->first('ticket_shipped_cc') }}</span>
        @endif
    </div>
</div>
<br>