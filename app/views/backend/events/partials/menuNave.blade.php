<div class="col-md-12 btn-group"
     role="group"
     aria-label="Basic example">
    <a href="/admin/prem/2016/filter/{{$lang}}/{{$order}}/{{$dir}}/{{$year}}/{{$season}}/{{$teamid}}/list"
       class="btn btn-danger">
        List
    </a>
    <a href="/admin/prem/2016/filter/{{$lang}}/{{$order}}/{{$dir}}/{{$year}}/{{$season}}/{{$teamid}}/meta_title"
       class="btn btn-danger">
        Meta Data
    </a>
    <a href="/admin/prem/2016/filter/{{$lang}}/{{$order}}/{{$dir}}/{{$year}}/{{$season}}/{{$teamid}}/datetime"
       class="btn btn-danger">
        Dates
    </a>
    <a href="/admin/prem/2016/filter/{{$lang}}/{{$order}}/{{$dir}}/{{$year}}/{{$season}}/{{$teamid}}/meta_description"
       class="btn btn-danger">
        Meta Description
    </a>
    <a href="/admin/prem/2016/filter/{{$lang}}/{{$order}}/{{$dir}}/{{$year}}/{{$season}}/{{$teamid}}/content"
       class="btn btn-danger">
        Content
    </a>
    <a href="/admin/prem/2016/filter/{{$lang}}/{{$order}}/{{$dir}}/{{$year}}/{{$season}}/{{$teamid}}/title"
       class="btn btn-danger">
        Title
    </a>
</div>

<div class="col-md-12 btn-group"
     role="group"
     aria-label="Basic example">
    <div class="btn-group" role="group">
        <button type="button " class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                aria-expanded="false">
            Filter Team {{$teamid}}
            <span class="caret"></span>
        </button>
        <ul class="dropdown-menu" style="width:50vw; overflow-y:scroll; height:100%;">

            @if(!empty($Allclubs))
                @foreach($Allclubs as $club)
                    <li style="width:25%; flOAT:LEFT;"><a
                                href="/admin/prem/2016/filter/{{$lang}}/{{$order}}/{{$dir}}/{{$year}}/{{$season}}/{{$club->id}}/{{$dataType}}/">{{$club->title}}</a>
                    </li>
                @endforeach
            @endif
        </ul>

    </div>
    <div class="btn-group" role="group">
        <button type="button " class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                aria-expanded="false">
            Filter League {{$season}}
            <span class="caret"></span>
        </button>
        <ul class="dropdown-menu" style="overflow-y:scroll; height:100%;">
            <li>
                <a href="/admin/prem/2016/filter/{{$lang}}/{{$order}}/{{$dir}}/{{$year}}/all/{{$teamid}}/{{$dataType}}/">All</a>
            </li>
            <li><a href="/admin/prem/2016/filter/{{$lang}}/{{$order}}/{{$dir}}/{{$year}}/7/{{$teamid}}/{{$dataType}}/">
                    Premier League</a></li>
            <li><a href="/admin/prem/2016/filter/{{$lang}}/{{$order}}/{{$dir}}/{{$year}}/12/{{$teamid}}/{{$dataType}}/">UEFA</a>
            </li>
            <li><a href="/admin/prem/2016/filter/{{$lang}}/{{$order}}/{{$dir}}/{{$year}}/41/{{$teamid}}/{{$dataType}}/">La
                    Liga</a></li>
            <li><a href="/admin/prem/2016/filter/{{$lang}}/{{$order}}/{{$dir}}/{{$year}}/42/{{$teamid}}/{{$dataType}}/">Bundesliga</a>
            </li>
            <li><a href="/admin/prem/2016/filter/{{$lang}}/{{$order}}/{{$dir}}/{{$year}}/45/{{$teamid}}/{{$dataType}}/">Europa
                    League</a></li>
            <li><a href="/admin/prem/2016/filter/{{$lang}}/{{$order}}/{{$dir}}/{{$year}}/46/{{$teamid}}/{{$dataType}}/">FA</a>
            </li>
        </ul>
    </div>
    <div class="btn-group" role="group">
        <button type="button " class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                aria-expanded="false">
            Filter Season {{$year}}
            <span class="caret"></span>
        </button>
        <ul class="dropdown-menu" style="overflow-y:scroll; height:100%;">
            <li>
                <a href="/admin/prem/2016/filter/{{$lang}}/{{$order}}/{{$dir}}/362/{{$season}}/{{$teamid}}/{{$dataType}}/">2016-2017</a>
            </li>
            <li>
                <a href="/admin/prem/2016/filter/{{$lang}}/{{$order}}/{{$dir}}/50/{{$season}}/{{$teamid}}/{{$dataType}}/">2015-2016</a>
            </li>
            <li>
                <a href="/admin/prem/2016/filter/{{$lang}}/{{$order}}/{{$dir}}/8/{{$season}}/{{$teamid}}/{{$dataType}}/">2014</a>
            </li>
        </ul>
    </div>
    <div class="btn-group" role="group">
        <button type="button " class="btn btn-warning dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                aria-expanded="false">
            Order By {{$order}}
            <span class="caret"></span>
        </button>
        <ul class="dropdown-menu" style="overflow-y:scroll; height:100%;">
            <li>
                <a href="/admin/prem/2016/filter/{{$lang}}/date/{{$dir}}/{{$year}}/{{$season}}/{{$teamid}}/{{$dataType}}/">Kick
                    Off</a></li>
            <li>
                <a href="/admin/prem/2016/filter/{{$lang}}/alp/{{$dir}}/{{$year}}/{{$season}}/{{$teamid}}/{{$dataType}}/">Alphabetic</a>
            </li>
            <li>
                <a href="/admin/prem/2016/filter/{{$lang}}/hteam/{{$dir}}/{{$year}}/{{$season}}/{{$teamid}}/{{$dataType}}/">Home
                    Team</a></li>
            <li>
                <a href="/admin/prem/2016/filter/{{$lang}}/ateam/{{$dir}}/{{$year}}/{{$season}}/{{$teamid}}/{{$dataType}}/">Away
                    Team</a></li>
        </ul>
    </div>

    <div class="btn-group" role="group">
        <button type="button " class="btn btn-warning dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                aria-expanded="false">
            Direction {{$dir}}
            <span class="caret"></span>
        </button>
        <ul class="dropdown-menu" style="overflow-y:scroll; height:100%;">
            <li>
                <a href="/admin/prem/2016/filter/{{$lang}}/{{$order}}/1/{{$year}}/{{$season}}/{{$teamid}}/{{$dataType}}/">DESC</a>
            </li>
            <li>
                <a href="/admin/prem/2016/filter/{{$lang}}/{{$order}}/0/{{$year}}/{{$season}}/{{$teamid}}/{{$dataType}}/">ASC</a>
            </li>
        </ul>
    </div>


    <div class="btn-group" role="group">
        <button type="button " class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                aria-expanded="false">
            Language {{$lang}}
            <span class="caret"></span>
        </button>
        <ul class="dropdown-menu" style="overflow-y:scroll; height:100%;">
            <li><a href="/admin/prem/2016/filter/uk/{{$order}}/1/{{$year}}/{{$season}}/{{$teamid}}/{{$dataType}}/">English
                    (UK)</a></li>
            <li><a href="/admin/prem/2016/filter/us/{{$order}}/1/{{$year}}/{{$season}}/{{$teamid}}/{{$dataType}}/">English
                    (US)</a></li>
            <li><a href="/admin/prem/2016/filter/ru/{{$order}}/1/{{$year}}/{{$season}}/{{$teamid}}/{{$dataType}}/">Russian
                    (RU)</a></li>
            <li><a href="/admin/prem/2016/filter/es/{{$order}}/1/{{$year}}/{{$season}}/{{$teamid}}/{{$dataType}}/">Spanish
                    (ES)</a></li>
            <li><a href="/admin/prem/2016/filter/fr/{{$order}}/1/{{$year}}/{{$season}}/{{$teamid}}/{{$dataType}}/">French
                    (FR)</a></li>
            <li><a href="/admin/prem/2016/filter/nz/{{$order}}/1/{{$year}}/{{$season}}/{{$teamid}}/{{$dataType}}/">English
                    (NZ)</a></li>
            <li><a href="/admin/prem/2016/filter/sa/{{$order}}/1/{{$year}}/{{$season}}/{{$teamid}}/{{$dataType}}/">English
                    (SA)</a></li>
            <li><a href="/admin/prem/2016/filter/hk/{{$order}}/1/{{$year}}/{{$season}}/{{$teamid}}/{{$dataType}}/">English
                    (HK)</a></li>
            <li><a href="/admin/prem/2016/filter/ie/{{$order}}/1/{{$year}}/{{$season}}/{{$teamid}}/{{$dataType}}/">English
                    (IE)</a></li>
            <li><a href="/admin/prem/2016/filter/ch/{{$order}}/1/{{$year}}/{{$season}}/{{$teamid}}/{{$dataType}}/">French
                    (CH)</a></li>
            <li><a href="/admin/prem/2016/filter/be/{{$order}}/1/{{$year}}/{{$season}}/{{$teamid}}/{{$dataType}}/">French
                    (BE)</a></li>
            <li><a href="/admin/prem/2016/filter/by/{{$order}}/1/{{$year}}/{{$season}}/{{$teamid}}/{{$dataType}}/">Russian
                    (BY)</a></li>
            <li><a href="/admin/prem/2016/filter/ua/{{$order}}/1/{{$year}}/{{$season}}/{{$teamid}}/{{$dataType}}/">Russian
                    (UA)</a></li>
            <li><a href="/admin/prem/2016/filter/ec/{{$order}}/1/{{$year}}/{{$season}}/{{$teamid}}/{{$dataType}}/">Spanish
                    (EC)</a></li>
            <li><a href="/admin/prem/2016/filter/cl/{{$order}}/1/{{$year}}/{{$season}}/{{$teamid}}/{{$dataType}}/">Spanish
                    (CL)</a></li>
            <li><a href="/admin/prem/2016/filter/ar/{{$order}}/1/{{$year}}/{{$season}}/{{$teamid}}/{{$dataType}}/">Spanish
                    (AR)</a></li>
            <li><a href="/admin/prem/2016/filter/ve/{{$order}}/1/{{$year}}/{{$season}}/{{$teamid}}/{{$dataType}}/">Spanish
                    (VE)</a></li>
            <li><a href="/admin/prem/2016/filter/pe/{{$order}}/1/{{$year}}/{{$season}}/{{$teamid}}/{{$dataType}}/">Spanish
                    (PE)</a></li>
            <li><a href="/admin/prem/2016/filter/mx/{{$order}}/1/{{$year}}/{{$season}}/{{$teamid}}/{{$dataType}}/">Spanish
                    (MX)</a></li>
            <li><a href="/admin/prem/2016/filter/co/{{$order}}/1/{{$year}}/{{$season}}/{{$teamid}}/{{$dataType}}/">Spanish
                    (CO)</a></li>
            <li><a href="/admin/prem/2016/filter/ca/{{$order}}/1/{{$year}}/{{$season}}/{{$teamid}}/{{$dataType}}/">English
                    (CA)</a></li>
            <li><a href="/admin/prem/2016/filter/lu/{{$order}}/1/{{$year}}/{{$season}}/{{$teamid}}/{{$dataType}}/">French
                    (LU)</a></li>
            <li><a href="/admin/prem/2016/filter/pt/{{$order}}/1/{{$year}}/{{$season}}/{{$teamid}}/{{$dataType}}/">Portugese
                    (PT)</a></li>
            <li><a href="/admin/prem/2016/filter/it/{{$order}}/1/{{$year}}/{{$season}}/{{$teamid}}/{{$dataType}}/">Italian (IT)
                    </a></li>
            <li><a href="/admin/prem/2016/filter/is/{{$order}}/1/{{$year}}/{{$season}}/{{$teamid}}/{{$dataType}}/">Icelandic (IS)
                </a></li>
            <li><a href="/admin/prem/2016/filter/sv/{{$order}}/1/{{$year}}/{{$season}}/{{$teamid}}/{{$dataType}}/">Swedish (SE)
                </a></li>
            <li><a href="/admin/prem/2016/filter/fi/{{$order}}/1/{{$year}}/{{$season}}/{{$teamid}}/{{$dataType}}/">Finnish (FI)
                </a></li>
            <li><a href="/admin/prem/2016/filter/dk/{{$order}}/1/{{$year}}/{{$season}}/{{$teamid}}/{{$dataType}}/">Danish (DK)
                </a></li>
            <li><a href="/admin/prem/2016/filter/no/{{$order}}/1/{{$year}}/{{$season}}/{{$teamid}}/{{$dataType}}/">Norwegian (NO)
                </a></li>
            <li><a href="/admin/prem/2016/filter/ja/{{$order}}/1/{{$year}}/{{$season}}/{{$teamid}}/{{$dataType}}/">Japanese (JA)
                </a></li>
            <li><a href="/admin/prem/2016/filter/pl/{{$order}}/1/{{$year}}/{{$season}}/{{$teamid}}/{{$dataType}}/">Polish (PL)
                </a></li>
            <li><a href="/admin/prem/2016/filter/cn/{{$order}}/1/{{$year}}/{{$season}}/{{$teamid}}/{{$dataType}}/">Chinese (CN)
                </a></li>
            <li><a href="/admin/prem/2016/filter/de/{{$order}}/1/{{$year}}/{{$season}}/{{$teamid}}/{{$dataType}}/">German (DE)
                </a></li>
            <li><a href="/admin/prem/2016/filter/nl/{{$order}}/1/{{$year}}/{{$season}}/{{$teamid}}/{{$dataType}}/">Dutch (NL)
                </a></li>
            <li><a href="/admin/prem/2016/filter/tr/{{$order}}/1/{{$year}}/{{$season}}/{{$teamid}}/{{$dataType}}/">Turkish (TR)
                </a></li>
            <li><a href="/admin/prem/2016/filter/kr/{{$order}}/1/{{$year}}/{{$season}}/{{$teamid}}/{{$dataType}}/">Korean (KR)
                </a></li>
            <li><a href="/admin/prem/2016/filter/cz/{{$order}}/1/{{$year}}/{{$season}}/{{$teamid}}/{{$dataType}}/">Czech (CZ)
                </a></li>
            <li><a href="/admin/prem/2016/filter/th/{{$order}}/1/{{$year}}/{{$season}}/{{$teamid}}/{{$dataType}}/">Thai (TH)
                </a></li>
        </ul>
    </div>

</div>

<br><br>