@extends('backend/_layout/layout')
@section('breadcrumb')
	<li >Home</li >
	<li >Events</li >
@stop

{{
    Assets::setScripts([
        'jquery.dataTables'         => 'js/plugin/datatables/jquery.dataTables.min.js',
        'dataTables.colVis'         => 'js/plugin/datatables/dataTables.colVis.min.js',
        'dataTables.tableTools'     => 'js/plugin/datatables/dataTables.tableTools.min.js',
        'dataTables.bootstrap'      => 'js/plugin/datatables/dataTables.bootstrap.min.js',
    ], true);
}}

@section('script')
	@parent
	<script type="text/javascript"
			src="/assets/backend/js/plugin/jquery-datetime/jquery.datetimepicker.js?1.0" ></script >
	<link rel="stylesheet"
		  href="/assets/backend/js/plugin/jquery-datetime/jquery.datetimepicker.css?1.0" >
	<script >
		jQuery(document).ready(function () {
			jQuery('.dateToShow').datetimepicker();
		});
	</script >
	<script src="/assets/js/form.js"></script>
@stop

@section('content')

	<div class="row" >
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
			<h1 class="page-title txt-color-blueDark" >
				<i class="fa fa-table fa-fw " ></i >
				Events | Teams : {{$teamid}} | Year : {{$year}} | League : {{$season}} | Data : {{$dataType}}| Lang : {{$lang}}<br >
			</h1 >
		</div >
	</div >


	{{ Notification::showAll() }}
		<!-- widget grid -->

	<section id="widget-grid"
			 class="" >
		<!-- row -->
		<div class="row" >
			@include('backend.events.partials.menuNave')
				<!-- NEW WIDGET START -->
			<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
				<!-- Widget ID (each widget will need unique ID)-->
				<div class="jarviswidget jarviswidget-color-darken"
					 id="wid-id-0"
					 data-widget-editbutton="false" >
					<header >
						<span class="widget-icon" > <i class="fa fa-table" ></i > </span >
						<h2 >Available Pages</h2 >
					</header >
					<!-- widget div-->
					<div >
						<!-- widget edit box -->
						<div class="jarviswidget-editbox" >
							<!-- This area used as dropdown edit box -->
						</div >
						<!-- end widget edit box -->
						<!-- widget content -->
						<form class="searchform"
							  style="margin-bottom: 20px;"
							  data-go="/admin/prem/2016/filter/{{$lang}}/{{$order}}/{{$dir}}/{{$year}}/{{$season}}/{{$teamid}}/{{$dataType}}/"
							  action="/admin/prem/2016/filter/{{$lang}}/{{$order}}/{{$dir}}/{{$year}}/{{$season}}/{{$teamid}}/{{$dataType}}/" >
							<input
								type="text"
								class="typedsearch"
								name="search"
								onchange="jQuery('.searchform').attr('action',jQuery('.searchform').attr('data-go')+jQuery('.typedsearch').val());
								" >
							<button >Search</button >
						</form >
						<div class="widget-body no-padding" >
							<table id="dt_basic"
								   class="table table-striped table-bordered table-hover"
								   width="100%" >
								<thead width="100%" >
									<tr >
										<th style="width:30% !important;" >
											<i class="fa fa-fw fa-user text-muted hidden-md hidden-sm hidden-xs" ></i >
											Title
										</th >
										<th style="width:30% !important;" >
											{{$dataType}}
										</th >
									</tr >
								</thead >
								<tbody width="100%" >
									@foreach($events as $a)
										<tr >
											<td width="10%" >{{$a->title}}</td >
											<td width="40%" >
												<form class="@if(empty($counter))ajaxform_{{$counter=1}}@else ajaxform_{{++$counter}}@endif" action="/admin/datetime/save/{{$dataType}}/" >
													<input
														value="{{$a->id}}"
														name="eventid"
														type="hidden" >
													<input
														value="{{$lang}}"
														name="lang"
														type="hidden" >
													<fieldset class="form-group" >
														@if($dataType=="meta_title")
															<input

																class="form-control"
																value="{{$a->meta_title}}"
																style="
															    float: left;
   		 														width: 70%;
   		 														"
																name="date" >
															<div
																onClick="jQuery('.ajaxform_{{$counter}}').ajaxSubmit({success: function(){ jQuery('.ajaxform_{{$counter}} .changecolor').css('background','#7EC26D').css('border','#7EC26D').text('saved');   }});"
																href=""
																class="btn btn-danger changecolor"
																style="padding: 6px;"
																value="Save" >Submit
															</div >
															<a href="/admin/events/edit/{{$a->id}}"
															   class="btn btn-danger" >Edit</a >


														@elseif($dataType=="title")
															<input
																class="form-control"
																@if(!empty($a->transtitle)))
																value="{{$a->transtitle}}"
																@else value="{{$a->title}}"
																@endif
																style="
															    float: left;
   		 														width: 70%;
   		 														"
																name="date" >
															<div
																onClick="jQuery('.ajaxform_{{$counter}}').ajaxSubmit({success: function(){ jQuery('.ajaxform_{{$counter}} .changecolor').css('background','#7EC26D').css('border','#7EC26D').text('saved');   }});"
															class="btn btn-danger changecolor"
																style="padding: 6px;"
																value="Save" >Submit
															</div >
															<a href="/admin/events/edit/{{$a->id}}"
															   class="btn btn-danger " >Edit</a >
														@elseif($dataType=="datetime")
															<input
																class="dateToShow form-control"
																value="{{$a->datetime}}"
																style="
															    float: left;
   		 														width: 70%;
   		 														"
																name="date" >
															<div
																onClick="jQuery('.ajaxform_{{$counter}}').ajaxSubmit({success: function(){ jQuery('.ajaxform_{{$counter}} .changecolor').css('background','#7EC26D').css('border','#7EC26D').text('saved');   }});"
																class="btn btn-danger changecolor"
																style="padding: 6px;"
																value="Save" >Submit
															</div >
															<a href="/admin/events/edit/{{$a->id}}"
															   class="btn btn-danger" >Edit</a >
														@elseif($dataType=="meta_description")
															<textarea
																class="form-control"
																value=""
																style="
															    float: left;
   		 														width: 70%;
    															height: 100px;
   		 														"
																name="date" >{{$a->meta_description}}</textarea >
															<div
																onClick="jQuery('.ajaxform_{{$counter}}').ajaxSubmit({success: function(){ jQuery('.ajaxform_{{$counter}} .changecolor').css('background','#7EC26D').css('border','#7EC26D').text('saved');   }});"
																class="btn btn-danger changecolor"
																style="padding: 6px;"
																value="Save" >Submit
															</div >
															<a href="/admin/events/edit/{{$a->id}}"
															   class="btn btn-danger" >Edit</a >
														@elseif($dataType=="content")
															<textarea
																class="form-control col-md-12"
																value=""
																style="
															    float: left;
    															height: 100px;
   		 														"
																name="date" >{{$a->content}}</textarea >
															<div class="col-md-12 btn-group"
																 role="group"
																 aria-label="Basic example" >
																<div
																	onClick="jQuery('.ajaxform_{{$counter}}').ajaxSubmit({success: function(){ jQuery('.ajaxform_{{$counter}} .changecolor').css('background','#7EC26D').css('border','#7EC26D').text('saved');   }});"
																	type="submit"
																	class="btn btn-danger col-md-6 changecolor"
																	value="Save" >Save
																</div >
																<a href="/admin/events/edit/{{$a->id}}"
																   class="btn btn-danger  col-md-6" >Full Edit</a >
															</div >

														@else
															<a href="/admin/events/edit/{{$a->id}}"
															   class="btn btn-danger" >Edit</a >
															@if($a->season_id==362 && $a->migrate==0)
																<a href='/admin/migrate/2017/{{$a->id}}'
																   class='btn btn-success' >Migrate</a >
															@endif
															@if($a->season_id==362 && $a->migrate==0)
																<a href='/admin/migrate/fast/2017/{{$a->id}}'
																   class='btn btn-warning' >Fast Migrate</a >
															@endif
														@endif
														<span class="input-group-btn" >

															</span >
													</fieldset >
												</form >
											</td >
										</tr >
									@endforeach
								</tbody >
							</table >
							{{ $events->links() }}
						</div >
						<!-- end widget content -->
					</div >
					<!-- end widget div -->
				</div >
				<!-- end widget -->
			</article >
			<!-- WIDGET END -->
		</div >
		<!-- end row -->
		<!-- end row -->
	</section >
	<!-- end widget grid -->

@stop

@section ('script')

@stop


