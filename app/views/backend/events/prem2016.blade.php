@extends('backend/_layout/layout')
@section('breadcrumb')
	<li >Home</li >
	<li >Events</li >
@stop

{{
    Assets::setScripts([
        'jquery.dataTables'         => 'js/plugin/datatables/jquery.dataTables.min.js',
        'dataTables.colVis'         => 'js/plugin/datatables/dataTables.colVis.min.js',
        'dataTables.tableTools'     => 'js/plugin/datatables/dataTables.tableTools.min.js',
        'dataTables.bootstrap'      => 'js/plugin/datatables/dataTables.bootstrap.min.js',
    ], true);
}}

@section('script')
	@parent

	<script type="text/javascript"
			src="/assets/backend/js/plugin/jquery-datetime/jquery.datetimepicker.js?1.0" ></script >
	<link rel="stylesheet"
		  href="/assets/backend/js/plugin/jquery-datetime/jquery.datetimepicker.css?1.0" >
	<script >
		jQuery(document).ready(function () {
			jQuery('.dateToShow').datetimepicker();
		});
	</script >
	<script src="/assets/js/form.js"></script>
@stop

@section('content')

	<div class="row" >
		<div class="col-xs-12 col-sm-7 col-md-7 col-lg-4" >
			<h1 class="page-title txt-color-blueDark" >
				<i class="fa fa-table fa-fw " ></i >
				Events
			</h1 >
		</div >
	</div >



	{{ Notification::showAll() }}
		<!-- widget grid -->

	<section id="widget-grid"
			 class="" >
		<!-- row -->
		<div class="row" >




			@include('backend.events.partials.menuNave')

			<!-- NEW WIDGET START -->
			<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
				<!-- Widget ID (each widget will need unique ID)-->
				<div class="jarviswidget jarviswidget-color-darken"
					 id="wid-id-0"
					 data-widget-editbutton="false" >
					<header >
						<span class="widget-icon" > <i class="fa fa-table" ></i > </span >
						<h2 >Available Pages</h2 >
					</header >
					<!-- widget div-->
					<div >
						<!-- widget edit box -->
						<div class="jarviswidget-editbox" >
							<!-- This area used as dropdown edit box -->
						</div >
						<!-- end widget edit box -->
						<!-- widget content -->
						<form class="searchform"
							  style="margin-bottom: 20px;"
							  data-go="/admin/prem/2016/events/"
							  action="/prem/2016/events/" >
							<input
								type="text"
								class="typedsearch"
								name="search"
								onchange="jQuery('.searchform').attr('action',jQuery('.searchform').attr('data-go')+jQuery('.typedsearch').val());
								" >
							<button >Search</button >
						</form >
						<div class="widget-body no-padding" >
							<table id="dt_basic"
								   class="table table-striped table-bordered table-hover"
								   width="100%" >
								<thead width="100%" >
									<tr >
										<th width="20%" >
											<i class="fa fa-fw fa-user text-muted hidden-md hidden-sm hidden-xs" ></i >
											Title
										</th >
										<th style="width:20%;" >Action</th >
									</tr >
								</thead >
								<tbody width="100%" >
									@foreach($events as $a)
										<tr >
											<td width="20%" >{{$a->title}}</td >
											<td width="20%" >
												<form action="/admin/datetime/save" >
													<input
														value="{{$a->id}}"
														name="eventid"
														type="hidden" >
													<fieldset class="form-group" >
														<input
															class="dateToShow form-control"
															value="{{$a->datetime}}"
															style="
															    float: left;
   		 														width: 70%;
   		 														"
															name="date" >
														<span class="input-group-btn" >
														<button
															type="submit"
															href=""
															class="btn btn-danger"
															style="padding: 6px;"
															value="Save" >Submit
														</button >
															</span >
													</fieldset >
												</form >
											</td >
											<td width="20%" >
												<a
													class="btn btn-danger"
													href="/admin/events/edit/{{$a->id}}"
													style="
												" >edit
												</a >
											</td >
										</tr >
									@endforeach
								</tbody >
							</table >
							{{ $events->links() }}
						</div >
						<!-- end widget content -->
					</div >
					<!-- end widget div -->
				</div >
				<!-- end widget -->
			</article >
			<!-- WIDGET END -->
		</div >
		<!-- end row -->
		<!-- end row -->
	</section >
	<!-- end widget grid -->

@stop

@section ('script')

@stop


