@extends('backend._layout.layout')
@section('script')
    @parent()
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jscolor/2.0.4/jscolor.min.js"></script>
    @stop
@section('content')
    <h1 > New Class Naming </h1 >

    @include('backend.map.internnav')
    {{ Form::open(array('url' => '/admin/map/compare/new/save/')) }}

    {{Form::token();}}

    <b >What Class Do you want to refer To</b ><br >
    <select name="left" id="left" >
        @foreach($list as $item)
            <option value="{{str_replace(" ","",strtolower($item->title))}}">{{$item->title}}</option>
        @endforeach
    </select >
    <br ><br >

    <b >How Do you want this to read on the map</b ><br >
    <select name="frontend" id="frontend" >
        @foreach($list as $item)
            <option value="{{$item->title}}">{{$item->title}}</option>
        @endforeach
    </select >
    <br ><br >
    <b >Give this a "CAT_" name</b ><br >
    {{Form::text('right');}}<br ><br >


    <b >Set The Color</b ><br >
    {{Form::text('color','ab2567', array('class'=>'jscolor'))}}<br ><br >

    {{Form::submit('Click Me!');}}

    {{ Form::close() }}

@stop
