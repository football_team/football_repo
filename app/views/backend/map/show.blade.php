@extends('backend._layout.layout')
@section('content')

    <h1 > Map Maker </h1 >


    @include('backend.map.internnav')

    <table class="table table-striped table-bordered table-hover dataTable no-footer" >
        <thead >
        <th >
            ID
        </th >
        <th >
            Name
        </th >
        <th >
            SRC
        </th >
        <th >
            Status
        </th >
        <th >
            Action
        </th >
        </thead >
        <tbody >
        @foreach($maps as $map)
            <tr >
                <td >
                    {{$map->id}}
                </td >
                <td >
                    {{$map->name}}
                </td >
                <td >
                    {{$map->src}}
                </td >
                <td >
                    {{$map->status}}
                </td >
                <td >
                    @if($map->status==0)
                        <a href="/admin/map/enable/{{$map->id}}" style="padding:10px; background: #7EC26D; border:0px; -webkit-border-radius:  5px;-moz-border-radius:  5px;border-radius: 5px; color:white;" >Enable</a >@endif
                    @if($map->status==1)
                        <a href="/admin/map/disable/{{$map->id}}" style="padding:10px; background: #E3297D; border:0px; -webkit-border-radius:  5px;-moz-border-radius:  5px;border-radius: 5px; color:white;" >Disable</a >@endif

                        <a href="/admin/map/builder/{{$map->id}}" style="padding:10px; background: #63C9DA; border:0px; -webkit-border-radius:  5px;-moz-border-radius:  5px;border-radius: 5px; color:white;" >Map Builder</a >


                </td >
            </tr >
        @endforeach
        </tbody >
    </table >


@stop

