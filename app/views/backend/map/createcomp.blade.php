@extends('backend._layout.layout')
@section('script')
    @parent()
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jscolor/2.0.4/jscolor.min.js"></script>
@stop
@section('content')

    <h1 > Edit Naming list </h1 >

    @include('backend.map.internnav')
@if(!empty($maps))
    {{ Form::open(array('url' => '/admin/map/compare/save/'.$maps->id)) }}

    {{Form::token();}}
    {{Form::text('id',$maps->id);}}<br ><br >

    <b >What Do you want this to read as on the maps</b ><br >
    {{Form::text('frontend',$maps->frontend);}}<br ><br >

    <b >Give it a "CAT_" name</b ><br >
    {{Form::text('right',$maps->right);}}<br ><br >

    <b >Set The Color</b ><br >
    {{Form::text('color',$maps->color,array('class'=>"jscolor"));}}<br ><br >

    {{Form::submit('Click Me!');}}

    {{ Form::close() }}




@endif
@stop
