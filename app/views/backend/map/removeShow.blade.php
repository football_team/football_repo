@extends('backend._layout.layout')
@section('content')

    <h1 >Delete Maps </h1 >
    <p>Note : This is almost always a Stupid Idea,  Check twice before doing anything here.</p>

    @include('backend.map.internnav')

    <table class="table table-striped table-bordered table-hover dataTable no-footer" >
        <thead >
        <th >
            ID
        </th >
        <th >
            Name
        </th >
        <th >
            SRC
        </th >
        <th >
            Status
        </th >
        <th >
            Action
        </th >
        </thead >
        <tbody >
        @foreach($maps as $map)
            <tr >
                <td >
                    {{$map->id}}
                </td >
                <td >
                    {{$map->name}}
                </td >
                <td >
                    {{$map->src}}
                </td >
                <td >
                    {{$map->status}}
                </td >
                <td >
                   <a style="background: red; color: white; padding: 5px; width:100%;" href="/admin/map/remove/{{$map->id}}">Remove This Map</a>
                </td >
            </tr >
        @endforeach
        </tbody >
    </table >


@stop

