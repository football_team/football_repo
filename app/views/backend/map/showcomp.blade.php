@extends('backend._layout.layout')
@section('content')

    <h1 > Map Category Manager </h1 >

    @include('backend.map.internnav')
    <table class="table table-striped table-bordered table-hover dataTable no-footer" >
        <thead >
        <th >
            ID
        </th >
        <th >
            BackendName
        </th >
        <th >
            Cat NAme
        </th >
        <th >
            Front End name
        </th >
        <th >
           Edit
        </th >
        </thead >
        <tbody >
        @foreach($maps as $map)
            <tr >
                <td >
                    {{$map->id}}
                </td >
                <td >
                    {{$map->left}}
                </td >
                <td >
                    {{$map->right}}
                </td >
                <td >
                    {{$map->frontend}}
                </td >
                <td >
                   <div style="width:30px; height:30px; background:#{{$map->color}};"></div>
                </td >
                <td >
                    <a href="/admin/map/compare/{{$map->id}}">Edit</a>
                </td >
            </tr >
        @endforeach
        </tbody >
    </table >


@stop

