@extends('backend._layout.layout')
@section('content')

    <h1 > Create New Map </h1 >

    @include('backend.map.internnav')
    {{ Form::open(array('url' => '/admin/map/new/save')) }}

        {{Form::token();}}

        <b >Map Name</b ><br>
        {{Form::text('name');}}<br><br>

        <b >Map File Name</b ><br>
        {{Form::text('src');}}<br><br>


        <b >Map Status</b ><br>
        {{Form::radio('Enabled', '1', true);}} Enabled<br>
        {{Form::radio('Disabled', '0', false);}} Disabled<br><br>

        {{Form::submit('Click Me!');}}

    {{ Form::close() }}


@stop
