@extends('backend/_layout/layout')
@section('content')
	<div id="content" >
		<div class="row" >
			<div class="col-xs-12 col-sm-7 col-md-7 col-lg-4" >
				<h1 class="page-title txt-color-blueDark" >
					<i class="fa fa-table fa-fw " ></i >
					Customers
				</h1 >
			</div >
		</div >
		<!-- widget grid -->
		<section id="widget-grid"
				 class="" >
			<!-- row -->
			<div class="row" >
				<!-- NEW WIDGET START -->
				<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12 sortable-grid ui-sortable" >
					<div class="jarviswidget jarviswidget-color-darken jarviswidget-sortable"
						 id="wid-id-0"
						 data-widget-editbutton="false"
						 role="widget" >
						<header role="heading" >

							<h2 >Customers</h2 >
							<span class="jarviswidget-loader" ><i class="fa fa-refresh fa-spin" ></i ></span ></header >
						<div role="content" >
							<div class="jarviswidget-editbox" >
							</div >
							<div class="widget-body no-padding" >
								<div id="dt_basic_wrapper"
									 class="dataTables_wrapper form-inline no-footer" >
									<div class="dt-toolbar" >

										<div class="col-xs-12 col-sm-6" >

									</div >
									<table id="dt_basic"
										   class="table table-striped table-bordered table-hover dataTable no-footer"
										   width="100%"
										   role="grid"
										   aria-describedby="dt_basic_info"
										   style="width: 100%;" >
										<thead >
										<tr>											<th>id</th>

											<th>First Name</th><th>Last Name</th>
											<th>Price</th>											<th>Qty</th>

											<th>Status</th>
											<th>Game</th>
											</tr >
										</thead >
										<tbody >
											@foreach($seller as $row)

												<tr>													<td>{{$row->product_id}}</td>

													<td>{{$row->firstname}}</td>													<td>{{$row->lastname}}</td>

													<td>{{$row->price}}</td>
													<td>{{$row->available_qty}}</td>
													<td>{{$row->ticket_status}}</td>
													<td>{{$row->title}}</td>
													<td>{{$row->ticket}}</td>
													<td>{{$row->ticket_type}}</td>
													<td><a href="/admin/disable/ticket/{{$row->product_id}}">Disable</a></td>
												</tr>

											@endforeach()
										</tbody >
									</table >
										{{$seller->links()}}
								</div >
							</div >
						</div >
					</div >
				</article >
			</div >
		</section >
	</div >
@stop
