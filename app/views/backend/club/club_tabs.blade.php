@extends('backend/_layout/layout')
@section('breadcrumb')
<li>Home</li><li>Club</li>
@stop

{{
    Assets::setScripts([
        'jquery.dataTables'         => 'js/plugin/datatables/jquery.dataTables.min.js',
        'dataTables.colVis'         => 'js/plugin/datatables/dataTables.colVis.min.js',
        'dataTables.tableTools'     => 'js/plugin/datatables/dataTables.tableTools.min.js',
        'dataTables.bootstrap'      => 'js/plugin/datatables/dataTables.bootstrap.min.js',
    ], true);
}}

@section('content')

<div class="row">
    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
        <h1 class="page-title txt-color-blueDark">
            <i class="fa fa-table fa-fw "></i>
            {{$club}} Tabs
        </h1>
    </div>

</div>
{{ Notification::showAll() }}
<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- row -->
    <div class="row">

        <!-- NEW WIDGET START -->
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget jarviswidget-color-darken" id="wid-id-1" data-widget-editbutton="false">
                <header>
                    <span class="widget-icon"> <i class="fa fa-table"></i> </span>

                    <h2>Tabs</h2>

                </header>

                <!-- widget div-->
                <div>

                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox">
                        <!-- This area used as dropdown edit box -->
                    </div>
                    <div class="btn-toolbar" style="margin-bottom:20px; text-align:right;">
                         <a href="/admin/club/tabs/club/{{$clubID}}/add" class="btn btn-primary">Add New Tab</a>
                    </div>
                    <!-- end widget edit box -->

                    <!-- widget content -->
                    <div class="widget-body no-padding">

                        <table id="dt_basic1" class="table table-striped table-bordered table-hover" width="100%">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th><i class="fa fa-fw fa-user text-muted hidden-md hidden-sm hidden-xs"></i>Title</th>
                                <th><i class="fa fa-fw fa-user text-muted hidden-md hidden-sm hidden-xs"></i>Locale</th>
                                <th><i class="fa fa-fw fa-user text-muted hidden-md hidden-sm hidden-xs"></i>Enabled</th>
                                <th><i class="fa fa-fw fa-user text-muted hidden-md hidden-sm hidden-xs"></i>Order</th>
                                <th><i class="fa fa-fw fa-user text-muted hidden-md hidden-sm hidden-xs"></i>Edit</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>

                    </div>
                    <!-- end widget content -->

                </div>
                <!-- end widget div -->

            </div>
            <!-- end widget -->

        </article>
        <!-- WIDGET END -->

    </div>

    <!-- end row -->

    <!-- end row -->

</section>
<!-- end widget grid -->

@stop

@section ('script')
<script>
    $(document).ready(function() {
        $('#dt_basic1').dataTable({
            serverSide: true,
            ajax: {
                url: "/{{ Config::get('bondcms.admin_prefix') }}/club/tabs/club/{{$clubID}}/list.json",
                type: 'POST'
            },

            order: [[0, 'DESC']]

        });
    })
</script>
@stop


