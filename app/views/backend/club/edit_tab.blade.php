@extends('backend/_layout/layout')

@section('style')
    <style>
        .container {top: 0px;}
    </style>
@stop

{{
Assets::setStyles([
        'bootstrap-datepicker'          => 'js/plugin/jquery-datetime/jquery.datetimepicker.css',
        'bootstrap-tagsinput'           => 'js/plugin/bootstrap-tags/bootstrap-tagsinput.css'
    ], true);
}}


{{
Assets::setScripts([
        'bootstrap-tagsinput'       => 'js/plugin/bootstrap-tags/bootstrap-tagsinput.min.js',
        'slug'                      => 'js/plugin/jquery-slug/jquery.slug.js'
    ], true);
}}
@section('content')
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <h1 class="page-title txt-color-blueDark"><!-- PAGE HEADER --><i class="fa-fw fa fa-pencil-square-o"></i> Club <span>> Add Tab </span></h1>
    </div>
</div>

<section id="widget-grid" class="admin-page">
    <div class="row">
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="alert alert-danger hidden-lg hidden-md hidden-sm">
                <b>Please note:</b>
                This plugin is non-responsive
            </div>

            <div class="jarviswidget jarviswidget-sortable" id="wid-id-0" data-widget-togglebutton="false" data-widget-editbutton="false" data-widget-fullscreenbutton="false" data-widget-colorbutton="false" data-widget-deletebutton="false" role="widget" style="">
            <!-- widget options:
            usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

            data-widget-colorbutton="false"
            data-widget-editbutton="false"
            data-widget-togglebutton="false"
            data-widget-deletebutton="false"
            data-widget-fullscreenbutton="false"
            data-widget-custombutton="false"
            data-widget-collapsed="true"
            data-widget-sortable="false"

            -->
            <header role="heading">
                <span class="widget-icon"> <i class="fa fa-file-image-o txt-color-darken"></i> </span>
                <h2 class="hidden-xs hidden-sm">Edit Tab </h2>
                <span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span>
            </header>

            <!-- widget div-->
            <div role="content">
                <!-- widget edit box -->
                <div class="widget-body">
                    <div id="myTabContent" class="tab-content">
                        <form method="POST" id = "addTabForm">
                        <div class="tab-pane fade active in" id="s1">
                            <!-- Title -->
                            <div class="control-group {{ $errors->has('title') ? 'has-error' : '' }}">
                                <label class="control-label" for="title">Title</label>

                                <div class="controls">
                                    <input type="text" class="form-control" id="title" name="title" value="{{$tab->title}}">
                                </div>
                            </div>
                            <br>
                            <!-- Content -->
                            <div class="control-group {{ $errors->has('content') ? 'has-error' : '' }}">
                                <label class="control-label" for="content_page">Content</label>

                                <div class="controls">
                                    <textarea class="form-control" id="content_page" name="content_page">{{$tab->content}}</textarea>
                                </div>
                            </div>
                            <br />
                            <!-- Published -->
                            <div class="control-group {{ $errors->has('is_published') ? 'has-error' : '' }}">
                                <div class="controls">
                                    <label class="checkbox"><input type="checkbox" name="enabled" value="1" @if($tab->enabled) checked @endif> Enabled?</label>
                                </div>
                            </div>
                            <br>
                            <div class="control-group {{ $errors->has('title') ? 'has-error' : '' }}">
                                <label class="control-label" for="title">Order</label>

                                <div class="controls">
                                    <input type="text" class="form-control" id="title" name="order_shown" value="{{$tab->order_shown}}">
                                </div>
                            </div>
                            <br>
                            <div class="control-group {{ $errors->has('title') ? 'has-error' : '' }}">
                                <label class="control-label" for="title">New Layout?</label>

                                <div class="controls">
                                    <input type="checkbox" class="form-control" id="new_layout" name="new_layout" value="1" @if($tab->new_layout) checked @endif>
                                </div>
                            </div>
                            <br>
                            <br/>
                            <div class="control-group {{ $errors->has('icon_image') ? 'has-error' : '' }}">
                                <label class="control-label" for="title">Icon Image</label>

                                <div class="controls icon-image-container">

                                    @if (isset($tab->icon_image) && $tab->icon_image != '' )
                                        <img class="icon-img-preview" src="{{$tab->icon_image}}" style="max-width:300px; max-height:200px;" />
                                        <hr class='image-preview-div' style='margin-top: 10px;' />
                                        <input name="icon_image" value="{{$tab->icon_image}}" type="hidden" id="icon_image"/>
                                        {{ Form::button('Icon Image',  array('class'=>'btn btn-info', 'id'=>'icon_image_btn')) }}
                                        {{ Form::button('Remove',  array('class'=>'btn btn-info', 'id'=>'remove_icon_image_btn', 'style'=>"display: block")) }}
                                    @else
                                        <input name="icon_image" value="" type="hidden" id="icon_image"/>
                                        {{ Form::button('Icon Image',  array('class'=>'btn btn-info', 'id'=>'icon_image_btn')) }}
                                        {{ Form::button('Remove',  array('class'=>'btn btn-info', 'id'=>'remove_icon_image_btn', 'style'=>"display: none")) }}
                                    @endif

                                    @if ($errors->first('icon_image'))
                                        <span class="help-block">{{ $errors->first('icon_image') }}</span>
                                    @endif
                                </div>
                            </div>
                            <br><br>
                        </div>

                        <div class="tab-pane fade" id="s5">

                        </div>
                        <div class="">
                            <!-- Form actions -->
                            <input type="submit" class="btn btn-success">
                        </div>
                        </form>
                    </div>
                </div>
            </div> <!-- end widget div -->
            </div>
        </article>
    </div>
</section>
@stop
{{
    Assets::setScripts([
        'bootstrap-datepicker'         => 'js/plugin/jquery-datetime/jquery.datetimepicker.js'
    ], true);
}}

@section('script')
@parent
@include('backend.partials.ckeditor')

<script>
    $('#addTabForm').submit(function(e){
        var url = "/admin/club/tabs/save/{{$id}}";

        $('#content_page').each(function () {
           var $textarea = $(this);
           $textarea.val(CKEDITOR.instances[$textarea.attr('name')].getData());
        });

        $.ajax({
            type: "POST",
            url:url,
            data: $('#addTabForm').serialize(),
            success: function(data)
            {
                window.location.replace(data);
            }
        });
        e.preventDefault();
    });
</script>

<script type="text/javascript">
    window.AppFileManager = {};


    function SetUrl(p,w,h) {
        AppFileManager.setImage(p,w,h);
    }

    (function ($, f) {
        f.fileManagerWindow =null;
        f.elem = null;
        f.intervalId = null;
        f.type = '';
        f.openFileManager = function () {
            if ( f.fileManagerWindow !== null) {
                alert("You already have opened window, p[lease close that before open another one");
                return null;
            }
            this.fileManagerWindow = window.open("/admin/filemanager/show?CKEditorFuncNum=1&langCode=en", "_blank", "toolbar=yes, scrollbars=yes, resizable=yes, top=100, left=500, width=900, height=600");
            this.intervalId = window.setInterval(this.checkwindow, 500);
        };
        f.closeFileManager = function () {

            this.fileManagerWindow.close();
            this.fileManagerWindow = null;
        };
        f.setImage= function(p,w,h) {
            this.closeFileManager();
            window.clearInterval(f.intervalId);
            if(p) {
                var pathExtract = /^[a-z]+:\/\/\/?[^\/]+(\/[^?]*)/i;
                var imageUrl = (pathExtract.exec(p))[1];
                if(imageUrl && this.type == 'icon') {
                    $('.icon-image-container').find('.icon-img-preview').remove();
                    $('.icon-image-container').find('.image-preview-div').remove();
                    var image = $('<img class="icon-img-preview" src="'+imageUrl+'" style="max-width:300px; max-height:200px;" />');
                    this.elem.prepend("<hr class='image-preview-div' style='margin-top: 10px;' />");
                    this.elem.prepend(image);
                    this.elem.find('#icon_image').val(imageUrl);
                    this.elem.find('#remove_icon_image_btn').css({display: 'block'});
                    return;
                }
            }
        }

        f.checkwindow =  function () {
            if (f.fileManagerWindow  && f.fileManagerWindow  == null) {
                window.clearInterval(f.intervalId);
            }

            try {
                if(!f.fileManagerWindow.top) {
                    window.clearInterval(f.intervalId);
                    f.fileManagerWindow = null;
                }
            } catch (err) {
                window.clearInterval(f.intervalId);
                f.fileManagerWindow = null;
            }

        }


        $.fn.fileManager = function (option) {
            f.elem = $(this).closest(option.container);
            f.openFileManager();
        }

        $.fn.removeFeatureImage = function (option) {
            var elem = $(this).closest(option.container);
            elem.find(option.img_pre).remove();
            elem.find(option.img_prev_div).remove();
            elem.find(option.feature_img).val('');
            elem.find(option.remove).css({display: 'none'});
        }
    })(jQuery, AppFileManager)

    $(function() {

        $('#icon_image_btn').click(function () {
            AppFileManager.type = 'icon';
            $(this).fileManager({
                container: '.icon-image-container'
            });
        });

        $('#remove_icon_image_btn').click(function () {
            $(this).removeFeatureImage({
                container: '.icon-image-container',
                img_pre: ".icon-img-preview",
                img_prev_div: ".image-preview-div",
                feature_img: '#icon_image',
                remove: '#remove_icon_image_btn'
            });
        });
    });


</script>

@stop
