@extends('backend/_layout/layout')
@section('breadcrumb')
@section('content')
	<h1 >Automated Test</h1 >

	<div class="dropdown" >
		<button class="btn btn-default dropdown-toggle"
				type="button"
				id="dropdownMenu1"
				data-toggle="dropdown"
				aria-haspopup="true"
				aria-expanded="true" >
			Checkout
			<span class="caret" ></span >
		</button >
		<ul class="dropdown-menu"
			aria-labelledby="dropdownMenu1" >
			<li ><a href="/admin/automate/run/checkout/XP/chrome"
					class="btn btn-warning" >
					
					<h2 > NORMAL TEST</h2 >
				</a ></li >
			<li ><a href="/admin/automate/run/checkout/WIN8/firefox"
					class="btn btn-primary" >
					WIN 8 | FF</a ></li >
			<li ><a href="/admin/automate/run/checkout/WIN8/chrome"
					class="btn btn-primary" >
												WIN
												8
												|
												Chrome</a ></li >
			<li ><a href="/admin/automate/run/checkout/WIN8/internet explorer"
					class="btn btn-primary" >
					
												WIN
												8
												|
												IE</a ></li >
			<li ><a href="/admin/automate/run/checkout/MAC/firefox"
					class="btn btn-danger" >
					 MAC | FF</a ></li >
			<li ><a href="/admin/automate/run/checkout/MAC/chrome"
					class="btn btn-danger" >
					
												 MAC | Chrome</a ></li >
			<li ><a href="/admin/automate/run/checkout/XP/firefox"
					class="btn btn-warning" >
					XP | FF</a ></li >
			<li ><a href="/admin/automate/run/checkout/XP/chrome"
					class="btn btn-warning" >
					
												XP
												|
												Chrome</a ></li >
			<li ><a href="/admin/automate/run/checkout/XP/internet explorer"
					class="btn btn-warning" >
					
												WIN
												8
												|
												IE</a ></li >
			<li ><a href="/admin/automate/run/checkout/ANDROID/android"
					class="btn btn-success" >
					ANDROID | ANDROID</a ></li >
			<li ><a href="/admin/automate/run/checkout/ANDROID/chrome"
					class="btn btn-success" >
					
												ANDROID
												|
												Chrome</a ></li >
			<li ><a href="/admin/automate/run/checkout/ios/iPad"
					class="btn btn-info" >
					 ios | iPad</a ></li >
			<li ><a href="/admin/automate/run/checkout/ios/iPhone"
					class="btn btn-info" >
					
												 ios | iPhone</a ></li >
		</ul >
	</div >
	
	<div class="dropdown" >
		<button class="btn btn-default dropdown-toggle"
				type="button"
				id="dropdownMenu1"
				data-toggle="dropdown"
				aria-haspopup="true"
				aria-expanded="true" >
			HOMEPAGE TEST
			<span class="caret" ></span >
		</button >
		<ul class="dropdown-menu"
			aria-labelledby="dropdownMenu1" >
			<li ><a href="/admin/automate/run/home/XP/chrome"
					class="btn btn-warning" >
					
					<h2 > NORMAL TEST</h2 >
				</a ></li >
			<li ><a href="/admin/automate/run/home/WIN8/firefox"
					class="btn btn-primary" >
					WIN 8 | FF</a ></li >
			<li ><a href="/admin/automate/run/home/WIN8/chrome"
					class="btn btn-primary" >
					WIN
					8
					|
					Chrome</a ></li >
			<li ><a href="/admin/automate/run/home/WIN8/internet explorer"
					class="btn btn-primary" >
					
					WIN
					8
					|
					IE</a ></li >
			<li ><a href="/admin/automate/run/home/MAC/firefox"
					class="btn btn-danger" >
					MAC | FF</a ></li >
			<li ><a href="/admin/automate/run/home/MAC/chrome"
					class="btn btn-danger" >
					
					MAC | Chrome</a ></li >
			<li ><a href="/admin/automate/run/home/XP/firefox"
					class="btn btn-warning" >
					XP | FF</a ></li >
			<li ><a href="/admin/automate/run/home/XP/chrome"
					class="btn btn-warning" >
					
					XP
					|
					Chrome</a ></li >
			<li ><a href="/admin/automate/run/home/XP/internet explorer"
					class="btn btn-warning" >
					
					WIN
					8
					|
					IE</a ></li >
			<li ><a href="/admin/automate/run/home/ANDROID/android"
					class="btn btn-success" >
					ANDROID | ANDROID</a ></li >
			<li ><a href="/admin/automate/run/home/ANDROID/chrome"
					class="btn btn-success" >
					
					ANDROID
					|
					Chrome</a ></li >
			<li ><a href="/admin/automate/run/home/ios/iPad"
					class="btn btn-info" >
					ios | iPad</a ></li >
			<li ><a href="/admin/automate/run/home/ios/iPhone"
					class="btn btn-info" >
					
					ios | iPhone</a ></li >
		</ul >
	</div >


@stop

