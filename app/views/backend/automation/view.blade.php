@extends('backend/_layout/layout')
@section('breadcrumb')
	<li >Home</li >
	<li >Events</li >
	@stop
	@section('content')
	{{ Notification::showAll() }}
		<!-- widget grid -->

	<section id="widget-grid"
			 class="" >
		<!-- row -->
		<div class="row" >
			<!-- NEW WIDGET START -->
			<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
				<!-- Widget ID (each widget will need unique ID)-->
				<div class="jarviswidget jarviswidget-color-darken"
					 id="wid-id-0"
					 data-widget-editbutton="false" >
					<div >
						<!-- widget edit box -->
						<div class="jarviswidget-editbox" >
							<!-- This area used as dropdown edit box -->
						</div >
						<!-- end widget edit box -->
						<!-- widget content -->
						<div class="widget-body no-padding" >
							<table id="dt_basic"
								   class="table table-striped table-bordered table-hover"
								   width="100%" >
								<thead width="100%" >
									<tr >
										<th style="width:30% !important;" >
											<i class="fa fa-fw fa-user text-muted hidden-md hidden-sm hidden-xs" ></i >
											Title
										</th >
									</tr >
								</thead >
								<tbody width="100%" >
									@if(!empty($events)) @foreach($events as $a)
										<tr >
											<td >
												{{$a['status']}}
											</td >
											<td >
												{{$a['reason']}}
											</td >
											<td >
												{{$a['os']}}
											</td >
											<td >
												{{$a['os_version']}}
											</td >
											<td >
												{{$a['browser']}}
											</td >
											<td >
												{{$a['browser_version']}}
											</td >
											<td >
												<video width="320" height="240" controls>
													<source src="{{$a['video']}}" type="video/mp4">
													Your browser does not support the video tag.
												</video>
											</td >
										</tr >
									@endforeach @endif
								</tbody >
							</table >
						</div >
					</div >
				</div >
			</article >
		</div >
	</section >
@stop

