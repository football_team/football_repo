@extends('backend/_layout/layout')
@section('content')
{{ HTML::style('assets/bootstrap/css/bootstrap-tagsinput.css') }}

    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <h1 class="page-title txt-color-blueDark"><!-- PAGE HEADER --><i class="fa-fw fa fa-pencil-square-o"></i> Article <span>>
                                New Article </span></h1>
        </div>
    </div>

    <section id="widget-grid" class="admin-page">

    <!-- row -->
    <div class="row">

    <!-- NEW WIDGET START -->
    <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

    <div class="alert alert-danger hidden-lg hidden-md hidden-sm">
        <b>Please note:</b>
        This plugin is non-responsive
    </div>

    <!-- Widget ID (each widget will need unique ID)-->

    <div class="jarviswidget jarviswidget-sortable" id="wid-id-0" data-widget-togglebutton="false" data-widget-editbutton="false" data-widget-fullscreenbutton="false" data-widget-colorbutton="false" data-widget-deletebutton="false" role="widget" style="">
    <!-- widget options:
    usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

    data-widget-colorbutton="false"
    data-widget-editbutton="false"
    data-widget-togglebutton="false"
    data-widget-deletebutton="false"
    data-widget-fullscreenbutton="false"
    data-widget-custombutton="false"
    data-widget-collapsed="true"
    data-widget-sortable="false"

    -->
    <header role="heading">
        <span class="widget-icon"> <i class="fa fa-file-image-o txt-color-darken"></i> </span>
        <h2 class="hidden-xs hidden-sm">New Article</h2>

        <ul class="nav nav-tabs pull-right in" id="myTab">
            <li class="active">
                <a data-toggle="tab" href="#s1"><i class="fa fa-crop text-success"></i> <span class="hidden-mobile hidden-tablet">Content</span></a>
            </li>
            <li>
                <a data-toggle="tab" href="#s2"><i class="fa fa-crop text-primary"></i> <span class="hidden-mobile hidden-tablet">SEO</span></a>
            </li>
            <li>
                <a data-toggle="tab" href="#s3"><i class="fa fa-crop text-primary"></i> <span class="hidden-mobile hidden-tablet">Tags</span></a>
            </li>
        </ul>
        <span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span>
    </header>

    <div role="content">
        <div class="widget-body">
            <div id="myTabContent" class="tab-content">
            {{ Form::open(array('action' => 'App\Controllers\Admin\ArticleController@store', 'id'=>'addArticleForm')) }}
            <div class="tab-pane fade active in" id="s1">
            <!-- Title -->
            <div class="control-group {{ $errors->has('title') ? 'has-error' : '' }}">
                <label class="control-label" for="title">Title</label>

                <div class="controls">
                    {{ Form::text('title', null, array('class'=>'form-control', 'id' => 'title', 'placeholder'=>'Title', 'value'=>Input::old('title'))) }}
                    @if ($errors->first('title'))
                    <span class="help-block">{{ $errors->first('title') }}</span>
                    @endif
                </div>
            </div>
            <br>

            <div class="control-group {{ $errors->has('slug') ? 'has-error' : '' }}">
                <label class="control-label" for="title">Slug</label>

                <div class="controls">
                    <div class="input-group">
                        <span class="input-group-addon">/</span>
                        {{ Form::text('slug', null, array('class'=>'form-control slug', 'id' => 'slug', 'placeholder'=>'Slug', 'value'=>Input::old('slug'))) }}
                    </div>
                    @if ($errors->first('slug'))
                    <span class="help-block">{{ $errors->first('slug') }}</span>
                    @endif
                </div>
            </div>
            <br>

            <div class="control-group {{ $errors->has('category') ? 'error' : '' }}">
                <label class="control-label" for="title">Category</label>

                <div class="controls">
                    {{ Form::select('category', $categories, null, array('class' => 'form-control', 'value'=>Input::old('category'))) }}
                    @if ($errors->first('category'))
                    <span class="help-block">{{ $errors->first('category') }}</span>
                    @endif
                </div>
            </div>
            <br>

            <!-- Content -->
            <div class="control-group {{ $errors->has('content') ? 'has-error' : '' }}">
                <label class="control-label" for="title">Content</label>

                <div class="controls">
                    {{ Form::textarea('content', null, array('class'=>'form-control', 'id' => 'content_page', 'placeholder'=>'Content', 'value'=>Input::old('content'))) }}
                    @if ($errors->first('content'))
                    <span class="help-block">{{ $errors->first('content') }}</span>
                    @endif
                </div>
            </div>
            <br>

            <div class="control-group {{ $errors->has('is_published') ? 'has-error' : '' }}">

                <div class="controls">
                    <label class="checkbox">{{ Form::checkbox('is_published', 'is_published') }} Publish ?</label>
                    @if ($errors->first('is_published'))
                    <span class="help-block">{{ $errors->first('is_published') }}</span>
                    @endif
                </div>
            </div>
                @include('backend.article.partials.articles')
        </div>

            <div class="tab-pane fade" id="s2">
                <div class="row">
                    <div class="col-md-12">
                        <!-- Meta Title -->
                        <div class="control-group {{ $errors->has('meta_title') ? 'has-error' : '' }}">
                            <label class="control-label" for="title">Meta Title</label>

                            <div class="controls">
                                {{ Form::text('meta_title', null, array('class'=>'form-control', 'id' => 'meta_title', 'placeholder'=>'Meta Title', 'value'=>Input::old('meta_title'))) }}
                                @if ($errors->first('meta_description'))
                                <span class="help-block">{{ $errors->first('meta_title') }}</span>
                                @endif
                            </div>
                        </div>
                        <br>
                        <!-- Meta Title -->


                        <div class="control-group {{ $errors->has('meta_description') ? 'has-error' : '' }}">
                            <label class="control-label" for="title">Meta Description</label>

                            <div class="controls">
                                {{ Form::text('meta_description', null, array('class'=>'form-control', 'id' => 'meta_description', 'placeholder'=>'Meta Description', 'value'=>Input::old('meta_description'))) }}
                                @if ($errors->first('meta_description'))
                                <span class="help-block">{{ $errors->first('meta_description') }}</span>
                                @endif
                            </div>
                        </div>
                        <br>


                        <!-- Meta Keywords -->
                        <div class="control-group {{ $errors->has('meta_keywords') ? 'has-error' : '' }}">
                            <label class="control-label" for="title">Meta Keywords</label>

                            <div class="controls">
                                {{ Form::textarea('meta_keywords', null, array('class'=>'form-control', 'id' => 'meta_keywords', 'placeholder'=>'Meta Keywords', 'value'=>Input::old('meta_keywords'))) }}
                                @if ($errors->first('meta_keywords'))
                                <span class="help-block">{{ $errors->first('meta_keywords') }}</span>
                                @endif
                            </div>
                        </div>
                        <br>
                    </div>
                </div>
            </div>

            <div class="tab-pane fade" id="s3">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-xs-12">
                                <h2><u>Event Tags</u></h2>
                                <div class="tagfield col-xs-12" id="event-tag-field">

                                </div>
                                <br><br>
                                <div id="event-tag-input">
                                    <input id="event-tags" class="tag-add-input"><button class="" id="event-tag-add">Add</button>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <h2><u>Club Tags</u></h2>
                                <div class="tagfield col-xs-12" id="club-tag-field">

                                </div>
                                <br><br>
                                <div id="club-tag-input">
                                    <input id="club-tags" class="tag-add-input"><button class="" id="club-tag-add">Add</button>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <h2><u>League Tags</u></h2>
                                <div class="tagfield col-xs-12" id="league-tag-field">

                                </div>
                                <br><br>
                                <div id="league-tag-input">
                                    <input id="league-tags" class="tag-add-input"><button class="" id="league-tag-add">Add</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br>
            {{ Form::submit('Save', array('class' => 'btn btn-success')) }}
            {{ Form::close() }}
        </div>
    </div>
</div> <!-- end widget div -->
</div>
</article>

</div>
</section>

    <style>
        .tagfield{
            background-color:rgb(230, 230, 230);
            border-color:rgb(240,240,240);
            border:1px solid;
        }
        .label{
            float:left;
            margin:3px;
        }
        .tag-add-input{
            width:500px;
        }
    </style>

@stop

@section('script')

{{ HTML::script('assets/bootstrap/js/bootstrap-tagsinput.js') }}
{{ HTML::script('assets/js/jquery.slug.js') }}

@include('backend.partials.ckeditor')

<script type="text/javascript">
    $(document).ready(function () {
        $("#title").slug();

        if ($('#tag').length != 0) {
            var elt = $('#tag');
            elt.tagsinput();
        }
    });

    $(function(){

        var availableEvents = [
            @foreach($events as $event)
            "{{$event->title}}",
            @endforeach
        ];

        var availableClubs = [
            @foreach($teams as $team)
                    "{{$team->title}}",
            @endforeach
        ];

        var availableLeagues = [
            @foreach($leagues as $league)
                    "{{$league->title}}",
            @endforeach
        ];

        $( "#event-tags" ).autocomplete({
            source: availableEvents
        });

        $( "#club-tags" ).autocomplete({
            source: availableClubs
        });

        $( "#league-tags" ).autocomplete({
            source: availableLeagues
        });

        $('#event-tag-add').click(function(e){
            e.preventDefault();
            val = $('#event-tags').val();
            if(availableEvents.indexOf(val)>-1)
            {
                $('#event-tag-field').append('<span class="label label-default event-tag" data-val="'+val+'">'+val+'</span>');
                var index = availableEvents.indexOf(val);
                availableEvents.splice(index,1);

            }
            $('#event-tags').val("");
        });

        $('body').on('click', '.event-tag' ,function(e){
            e.preventDefault();
            data = $(this).data('val');
            $(this).remove();
            availableEvents.push(data);
        });

        $('#club-tag-add').click(function(e){
            e.preventDefault();
            val = $('#club-tags').val();
            if(availableClubs.indexOf(val)>-1)
            {
                $('#club-tag-field').append('<span class="label label-default club-tag" data-val="'+val+'">'+val+'</span>');
                var index = availableClubs.indexOf(val);
                availableClubs.splice(index,1);

            }
            $('#club-tags').val("");
        });

        $('body').on('click', '.club-tag' ,function(e){
            e.preventDefault();
            data = $(this).data('val');
            $(this).remove();
            availableClubs.push(data);
        });

        $('#league-tag-add').click(function(e){
            e.preventDefault();
            val = $('#league-tags').val();
            if(availableLeagues.indexOf(val)>-1)
            {
                $('#league-tag-field').append('<span class="label label-default league-tag" data-val="'+val+'">'+val+'</span>');
                var index = availableLeagues.indexOf(val);
                availableLeagues.splice(index,1);

            }
            $('#league-tags').val("");
        });

        $('body').on('click', '.league-tag' ,function(e){
            e.preventDefault();
            data = $(this).data('val');
            $(this).remove();
            availableLeagues.push(data);
        });

        $('#addArticleForm').submit(function(e){
            var clubTitles = $('.club-tag');
            var leagueTitles = $('.league-tag');
            var eventTitles = $('.event-tag');
            var i=0;

            var ctstring = "";
            for(i=0; i<clubTitles.length;i++)
            {
                ctstring+='<input type="hidden" name="clubtags['+i+']" value="'+$(clubTitles[i]).data('val')+'">';
            }

            $(this).append(ctstring);

            var lstring = "";
            for(i=0; i<leagueTitles.length;i++)
            {
                lstring+='<input type="hidden" name="leaguetags['+i+']" value="'+$(leagueTitles[i]).data('val')+'">';
            }

            var estring = "";
            for(i=0; i<eventTitles.length;i++)
            {
                estring+='<input type="hidden" name="eventtags['+i+']" value="'+$(eventTitles[i]).data('val')+'">';
            }

            $(this).append(ctstring);
            $(this).append(lstring);
            $(this).append(estring);
        });

    });
</script>
@stop