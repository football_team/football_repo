@extends('backend._layout.layout')
@section('content')

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.css">

    @if(!empty($listTickets))

        <table class="matTable">

            <thead>
            <th class="matCard" colspan="4">Category Manager</th>
            </thead>


            <thead>
            <th class="chucknoris" colspan="7"></th>
            <th class="counter" colspan="2">{{$count}} Remaining</th>
            </thead>


            <thead>
            <th class="matHeadRow">ID</th>
            <th class="matHeadRow">Match</th>
            <th class="matHeadRow">Home Game</th>
            <th class="matHeadRow">Location</th>
            <th class="matHeadRow">Seller</th>
            <th class="matHeadRow">Qty</th>
            <th class="matHeadRow">Status</th>
            <th class="matHeadRow">Type</th>
            </thead>


            @foreach($listTickets as $rowItem)

                @foreach($rowItem as $innerRow)
                    @if(!empty($innerRow->event_id))

                        <tr class="matTableRow">
                            <td class="number">
                                @if(!empty($innerRow->id)){{$innerRow->id}}@endif
                            </td>
                            <td class="">

                                @if(!empty($events[$innerRow->ticket->ticketInformation->event_id]->title))
                                    {{$events[$innerRow->ticket->ticketInformation->event_id]->title}}
                                @endif
                            </td>
                            <td class="">

                                @if(!empty($events[$innerRow->ticket->ticketInformation->event_id]->event_in_home))
                                    @if($events[$innerRow->ticket->ticketInformation->event_id]->event_in_home ==1 )
                                        yes @endif
                                @endif
                            </td>
                            <td class="">

                                @if(!empty($events[$innerRow->ticket->ticketInformation->event_id]->event_location))
                                    {{$events[$innerRow->ticket->ticketInformation->event_id]->event_location}}
                                @endif
                            </td>

                            <td class="">
                                @if(!empty($web_user[$innerRow->user_id]->firstname))
                                    {{$innerRow->user_id}} |  {{$web_user[$innerRow->user_id]->firstname}}   {{$web_user[$innerRow->user_id]->lastname}}
                                @endif
                            </td>
                            <td class="number">
                                @if(!empty($innerRow->available_qty)){{$innerRow->available_qty}}@endif
                            </td>
                            <td>
                                @if(!empty($innerRow->ticket_status)){{$innerRow->ticket_status}}@endif
                            </td>
                            <td>
                                <select style="float:left;" class="dataSubmit-{{$innerRow->id}}">
                                    @if(!empty($ticketTypes[$innerRow->ticket->ticketInformation->ticket_type]->id))
                                        <option value="{{$ticketTypes[$innerRow->ticket->ticketInformation->ticket_type]->id}}">
                                            @if(!empty($ticketTypes[$innerRow->ticket->ticketInformation->ticket_type]->title)){{$ticketTypes[$innerRow->ticket->ticketInformation->ticket_type]->title}}@endif
                                        </option>
                                    @endif


                                    @if(!empty($events[$innerRow->ticket->ticketInformation->event_id]->event_in_home) && $events[$innerRow->ticket->ticketInformation->event_id]->event_in_home == 1)
                                        @if(!empty($clubs[$events[$innerRow->ticket->ticketInformation->event_id]->id]->stadium_id))
                                            @if(!empty($stadiums[$clubs[$events[$innerRow->ticket->ticketInformation->event_id]->id]->stadium_id]->ticket_type_ids))
                                                <optgroup label="Reconmended">
                                                    <?php $explodedData = explode(',', $stadiums[$clubs[$events[$innerRow->ticket->ticketInformation->event_id]->id]->stadium_id]->ticket_type_ids); ?>
                                                    @foreach($explodedData as $rowvalue)
                                                        @if(!empty($ticketTypes[$rowvalue]->title))
                                                            <option value="{{$ticketTypes[$rowvalue]->id}}">
                                                                {{$ticketTypes[$rowvalue]->title}}
                                                            </option>
                                                        @endif
                                                    @endforeach
                                                </optgroup>
                                            @endif
                                        @endif
                                    @endif

                                        <optgroup label="All">
                                    @foreach($ticketTypes as $row)
                                        <option value="{{$row->id}}">{{$row->title}}</option>
                                    @endforeach
                                        </optgroup>
                                </select>
                                <div>
                                    <svg onClick="save({{$innerRow->id}})" xmlns="http://www.w3.org/2000/svg"
                                         style="margin-left:10px; float:left; width:20px !important;"
                                         xmlns:xlink="http://www.w3.org/1999/xlink"
                                         version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 460.002 460.002"
                                         style="fill: rgb(0, 0, 0);" xml:space="preserve">
                                                    <path d="M427.137,0C408.93,0,51.379,0,32.865,0C14.743,0,0,14.743,0,32.865v394.272c0,18.122,14.743,32.865,32.865,32.865    c0,0,374.895,0,394.272,0c18.122,0,32.865-14.743,32.865-32.865V32.865C460.001,14.743,445.258,0,427.137,0z M245.812,30h50.995    v54.466h-50.995V30z M107.198,30h108.615v69.466c0,8.284,6.716,15,15,15h80.995c8.284,0,15-6.716,15-15V30h26.377v119.636H107.198    V30z M107.007,430.001V308.673h245.986v121.328H107.007z M430.002,427.137L430.002,427.137c-0.001,1.58-1.286,2.865-2.866,2.865    h-44.143V293.673c0-8.284-6.716-15-15-15H92.007c-8.284,0-15,6.716-15,15v136.328H32.865c-1.58,0-2.865-1.285-2.865-2.865V32.865    C30,31.285,31.285,30,32.865,30h44.333v134.636c0,8.284,6.716,15,15,15h275.986c8.284,0,15-6.716,15-15V30h43.953    c1.58,0,2.865,1.285,2.865,2.865V427.137z"
                                                          style="fill: rgb(0, 0, 0);"></path>
                                      </svg>
                                </div>
                            </td>
                            <td>
                                <div>
                                    <svg onclick="jQuery(this).parent('div').parent('td').parent('.matTableRow').addClass('animated bounceOutLeft').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', jQuery(this).css('display','none'));"
                                         fill="#8bc34a" xmlns="http://www.w3.org/2000/svg"
                                         xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" viewBox="0 0 44 44"
                                         enable-background="new 0 0 44 44">
                                        <path fill="#8bc34a"
                                              d="m22,0c-12.2,0-22,9.8-22,22s9.8,22 22,22 22-9.8 22-22-9.8-22-22-22zm12.7,15.1l0,0-16,16.6c-0.2,0.2-0.4,0.3-0.7,0.3-0.3,0-0.6-0.1-0.7-0.3l-7.8-8.4-.2-.2c-0.2-0.2-0.3-0.5-0.3-0.7s0.1-0.5 0.3-0.7l1.4-1.4c0.4-0.4 1-0.4 1.4,0l.1,.1 5.5,5.9c0.2,0.2 0.5,0.2 0.7,0l13.4-13.9h0.1c0.4-0.4 1-0.4 1.4,0l1.4,1.4c0.4,0.3 0.4,0.9 0,1.3z"></path>
                                    </svg>
                                </div>
                            </td>
                        </tr>
                    @endif

                @endforeach

            @endforeach

        </table>

    @endif


    <script>
        var counter = '{{$count}}';

        function save(id) {

            --counter;

            var dataValue = '.dataSubmit-' + id + '';
            var tickettype = jQuery(dataValue).val();

            $.ajax({
                url: "/admin/catmanagersave/" + id + "/" + tickettype + "/",
                context: document.body
            }).done(function () {
                jQuery(dataValue).parent('td').parent('.matTableRow').addClass('animated bounceOutLeft');
                setTimeout(function () {
                    jQuery('.animated').css('display', 'none');
                }, 600);

                jQuery('.counter').text(counter + 'Remaining');

            });

        }
    </script>

@stop
