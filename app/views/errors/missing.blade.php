@extends(Template::name('frontend.%s._layout.layout'))
@section('style')
{{ HTML::style('ckeditor/contents.css') }}
@stop
@section('content')

<!---------sidebar------------>
<section class="main">

    <div class="row">
        <span class="sidebar-space"></span>
        @include(Template::name('frontend.%s.sidebars.side1'))
    </div>
</section>
<!---------sidebar------------>



<section class="banner-home">


    <h1 class="page-header">    Error 404</h1>




    @if(isset($feature_image) && $feature_image != '')
    <img class="inner-banner" src="{{ $feature_image }}" alt="error 404" />
    @else
    <img class="inner-banner" src="{{ Assets::Path('images/default.jpg') }}" alt="error 404" />
    @endif



    <span class="ftp-line">
    	<span class="greenline"></span>
        <span class="yellowline"></span>
        <span class="pinkline"></span>
        <span class="blueline"></span>
    </span>


</section>




<!---------main content------------>
<div class="row">
    <div class="site-content">

        <p>
           Sorry but the page you requested could not be found Use your browsers back button to navigate to the page you have previously come from,  or alternativly use one of the links bellow.

        </p>

        <p>
            <a href="/">Home Page</a>
        </p>
        <p>
            <a href="/sitemap.html">Site Map</a>
        </p>
        <p>
            <a href="/faq">FAQ</a>
        </p>
        <p>
            <a href="/contact-us">Contact Us</a>
        </p>

    </div>
</div>
<!---------main content------------>


@stop










