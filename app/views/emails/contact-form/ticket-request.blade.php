<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>
<h2>Ticket Request for {{$title}}</h2>
<div>
    <p><b>Sender Name: </b>{{ $name }}</p>
    <p><b>Sender Email: </b>{{ $email }}</p>
    <p><b>Sender Phone number: </b>{{$phone}}</p>
    <p><b>Customer would like </b> {{$numTickets}} <b> tickets for: </b>{{ $title }}<b> on </b>{{$date}}</p>
    @if(!$reqTogether)
    	<p><b>Customer would like these tickets seated together</b></p>
    @else
    	<p><b>Customer has no preference of whether the tickets are seated together or not</b></p>
    @endif
    <p><b>{{$side}} end</b></p>
    <p><b>Additional notes:</b></p>
    <p>{{{$additionalInfo or 'None given'}}}</p>
</div>
</body>
</html>