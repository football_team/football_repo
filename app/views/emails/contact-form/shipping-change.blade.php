<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>
<h2>{{trans('homepage.SACFor')}} {{$orderId+100000000}}</h2>
<div>
    <p><b>{{trans('homepage.Name:')}} </b>{{ $fname }} {{$lname}}</p>
    <p><b>{{trans('homepage.Address:')}} </b></p><br>
    {{ $street }}<br>
    {{$city}}<br>
    {{$country}}<br>
    {{$postcode}}<br><br>
</div>
</body>
</html>