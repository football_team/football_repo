<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>
<h2>{{trans('homepage.Form Post')}}</h2>
<div>
    <p><b>{{trans('homepage.Name:')}}: </b>{{ $sender_name_surname }}</p>
    <p><b>{{trans('homepage.Email')}}: </b>{{ $sender_email }}</p>
    <p><b>{{trans('homepage.Mobile Number')}}: </b>{{ $sender_phone_number }}</p>
    <p><b>{{trans('homepage.Message')}}: </b>{{ $sender_message }}</p>
</div>
</body>
</html>
