<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>

<body bgcolor="#f6f6f6">

<style>
        * {
            margin: 0;
            padding: 0;
            font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;
            font-size: 100%;
            line-height: 1.6;
        }
        img {
            max-width: 100%;
        }
        body {
            -webkit-font-smoothing: antialiased;
            -webkit-text-size-adjust: none;
            width: 100%!important;
            height: 100%;
        }
        /* -------------------------------------
                ELEMENTS
        ------------------------------------- */
        a {
            color: #348eda;
        }
        .btn-primary {
            text-decoration: none;
            color: #FFF;
            background-color: #348eda;
            border: solid #348eda;
            border-width: 10px 20px;
            line-height: 2;
            font-weight: bold;
            margin-right: 10px;
            text-align: center;
            cursor: pointer;
            display: inline-block;
            border-radius: 25px;
        }
        .btn-secondary {
            text-decoration: none;
            color: #FFF;
            background-color: #aaa;
            border: solid #aaa;
            border-width: 10px 20px;
            line-height: 2;
            font-weight: bold;
            margin-right: 10px;
            text-align: center;
            cursor: pointer;
            display: inline-block;
            border-radius: 25px;
        }
        .last {
            margin-bottom: 0;
        }
        .first {
            margin-top: 0;
        }
        .padding {
            padding: 10px 0;
        }
        /* -------------------------------------
                BODY
        ------------------------------------- */
        table.body-wrap {
            width: 100%;
            padding: 20px;
        }
        table.body-wrap .container {
            border: 1px solid #f0f0f0;
        }
        /* -------------------------------------
                FOOTER
        ------------------------------------- */
        table.footer-wrap {
            width: 100%;
            clear: both!important;
        }
        .footer-wrap .container p {
            font-size: 12px;
            color: #666;

        }
        table.footer-wrap a {
            color: #999;
        }
        /* -------------------------------------
                TYPOGRAPHY
        ------------------------------------- */
        h1, h2, h3 {
            font-family: "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;
            line-height: 1.1;
            margin-bottom: 15px;
            color: #000;
            margin: 40px 0 10px;
            line-height: 1.2;
            font-weight: 200;
        }
        h1 {
            font-size: 36px;
        }
        h2 {
            font-size: 28px;
        }
        h3 {
            font-size: 22px;
        }
        p, ul, ol {
            margin-bottom: 10px;
            font-weight: normal;
            font-size: 14px;
        }
        ul li, ol li {
            margin-left: 5px;
            list-style-position: inside;
        }
        /* ---------------------------------------------------
                RESPONSIVENESS
                Nuke it from orbit. It's the only way to be sure.
        ------------------------------------------------------ */
        /* Set a max-width, and make it display as block so it will automatically stretch to that width, but will also shrink down on a phone or something */
        .container {
            display: block!important;
            max-width: 600px!important;
            margin: 0 auto!important; /* makes it centered */
            clear: both!important;
        }
        /* Set the padding on the td rather than the div for Outlook compatibility */
        .body-wrap .container {
            padding: 20px;
        }
        /* This should also be a block element, so that it will fill 100% of the .container */
        .content {
            max-width: 600px;
            margin: 0 auto;
            display: block;
        }
        /* Let's make sure tables in the content area are 100% wide */
        .content table {
            width: 100%;
        }
    </style>


<table class="body-wrap">
    <tr>
        <td></td>
        <td class="container" bgcolor="#FFFFFF">
        
        
         <div class="content">
         
       
       <a href="https://www.footballticketpad.com/"><img src="{{ Assets::Path('images/email-logo.png') }}" alt="Football Ticket Pad" /></a>
                
                <p><strong>{{trans('homepage.slogan')}}</strong></p>  
         
<h2>{{trans('homepage.tickList1')}}</h2>
<div>
    <p>{{trans('homepage.tickList2')}} <b>{{$seller_name}}</b></p>
    <p>{{trans('homepage.tickList3')}} {{$game}} {{trans('homepage.tickList4')}} {{date('dS F Y', strtotime($event_date))}}{{trans('homepage.tickList5')}}</p>
    <br/> 
    <p><b>{{trans('homepage.tickList6')}}</b> {{$formOfTicket}}</p>
    <p><b>{{trans('homepage.tickList7')}}</b> {{$location}}</p>
    @if(!empty($restrictions))
    <p><b>{{trans('homepage.tickList8')}}</b> {{$restrictions}}</p>
    @else
    <p><b>{{trans('homepage.tickList8')}}</b> {{trans('homepage.tickList9')}}</p>
    @endif
     <br/> 
    <p><b>{{trans('homepage.tickList10')}}</b></p>
    <p><b>{{trans('homepage.tickList11')}}</b> {{$number_of_ticket}}</p>
    <p><b>{{trans('homepage.tickList12')}}</b> {{trans('homepage.currencyInUse')}}{{@number_format($web_price, 2)}} {{trans('homepage.currencyAfter')}}</p>
    <p><b>{{trans('homepage.tickList13')}}</b> {{trans('homepage.currencyInUse')}}{{@number_format($web_price_total, 2)}} {{trans('homepage.currencyAfter')}}</p>
    <p><b>{{trans('homepage.tickList14')}} ({{@number_format($selling_fees, 2)}} %):</b> {{trans('homepage.currencyInUse')}}{{@number_format($selling_fees_amount, 2)}} {{trans('homepage.currencyAfter')}}</p>
    <p>-------------------------------------</p>

    <p>{{trans('homepage.tickList15')}} {{trans('homepage.currencyInUse')}}{{number_format($net_payment, 2)}} {{trans('homepage.currencyAfter')}}</p>
    <br/> 

    

    <p>{{trans('homepage.tickList16')}}</p>
    <p>{{trans('homepage.tickList17')}} <a href="https://www.footballticketpad.com/faq">{{trans('homepage.tickList18')}}</a></p>
</div>
<div style="text-align: center; padding:20px; background:rgba(216,216,216,1.00)"><strong>{{trans('homepage.tickList19')}}</strong></div>



</div>

		</td>
        </tr>
        </table>	



</body>
</html>
