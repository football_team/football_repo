@extends('emails/_layout/layout')
@section('content')
<!-- body -->
<table class="body-wrap">
    <tr>
        <td></td>
        <td class="container" bgcolor="#FFFFFF">

            <!-- content -->
            <div class="content">
                <table>
                    <tr>
                        <td>
                            <p>Dear {{ $receiver_name}},</p>
                            <h2>Congratulation! your ticket has been sold</h2>
                            <p>This is a really simple email template. It's sole purpose is to get you to click the button below.</p>
                            <h2>How do I use it?</h2>
                            <p>All the information you need is on GitHub.</p>
                            <table>
                                <tr>
                                    <td class="padding">
                                        <p><a href="https://github.com/leemunroe/html-email-template" class="btn-primary">View the source and instructions on GitHub</a></p>
                                    </td>
                                </tr>
                            </table>
                            <p>Feel free to use, copy, modify this email template as you wish.</p>
                            <p>Thanks, have a lovely day.</p>
                            <p><a href="http://twitter.com/leemunroe">Follow @leemunroe on Twitter</a></p>
                        </td>
                    </tr>
                </table>
            </div>
            <!-- /content -->

        </td>
        <td></td>
    </tr>
</table>
<!-- /body -->

@stop