@extends('emails/_layout/layout')
@section('content')
        <!-- body -->
<table class="body-wrap">
    <tr>
        <td></td>
        <td class="container" bgcolor="#FFFFFF">
            <div class="content">
                <a href="https://www.footballticketpad.com/"><img src="{{ Assets::Path('images/email-logo.png') }}" alt="Football Ticket Pad" /></a>
                <p><strong>The Home of Football Tickets!</strong></p>
                <h2>Hello {{$name}},</h2>

                <p>Please be aware that the date and/or time of the event: {{$eventTitle}} have changed.</p>
                <table>
                    <tr>
                        <td>
                            <p>This is a courtesy email regarding your purchase for the upcoming event: {{$eventTitle}}. The match has been rescheduled for {{$newDate}} (yyyy-mm-dd H:i:s). Please make sure to note this in your calendar and schedule accordingly.</p>
                            <p>This is common practice with footballing events and is usually caused by the demands of broadcasters.</p>
                            <p>As per our terms and conditions your order will remain valid for the rescheduled date and time.</p>
                            <p>If you can no longer attend the event then you can attempt to resell your tickets from the "My Account" section of our website.</p>
                        </td>
                    </tr>
                    <tr>
                        <td bgcolor="#D4D4D4" align="center" style="padding:20px">Thank you, <strong>Football Ticket Pad</strong></td>
                    </tr>
                </table>
            </div>
        </td>
        <td></td>
    </tr>
</table>
@stop