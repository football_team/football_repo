<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<h2>{{trans('homepage.pwdReset')}}</h2>
		<p>{{trans('homepage.pwResetLink1')}} <a href="{{ URL::to('admin') }}/{{ $userId }}/reset/{{ urlencode($resetCode) }}">{{trans('homepage.pwResetLink2')}}</a>{{trans('homepage.pwResetLink3')}}</p>
		<p>{{trans('homepage.pointYourBrowser')}} <br /> {{ URL::to('admin') }}/{{ $userId }}/reset/{{ urlencode($resetCode) }}</p>
		<p>{{trans('homepage.emailTY')}} <br />
		{{trans('homepage.fbtpAT')}}</p>
	</body>
</html>