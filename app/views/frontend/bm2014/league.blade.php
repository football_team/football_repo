@extends(Template::name('frontend.%s._layout.layout'))
@section('style')
    {{ HTML::style('ckeditor/contents.css') }}

@stop
@section('content')

    <section class="main">
        <div class="row">
            <span class="sidebar-space"></span>
            @include(Template::name('frontend.%s.sidebars.side1'))
        </div>
    </section>

    <section class="banner-home hidden-xs">
        <h1 class="page-header">
            @if(isset($node->title))
                {{ e($node->title) }}
            @endif</h1>
        @if(isset($node->feature_image) && $node->feature_image != '')
            <img class="inner-banner" src="{{ $node->feature_image }}" alt="{{$node->title}}"/>
        @else
            <img class="inner-banner" src="{{ Assets::Path('images/default.jpg') }}" alt="{{$node->title}}"/>
        @endif
        <span class="ftp-line">
            <span class="greenline"></span>
            <span class="yellowline"></span>
            <span class="pinkline"></span>
            <span class="blueline"></span>
        </span>
    </section>

    <div class="visible-xs mobileouterViewRow">
        <p class="largerCenter">
            @if(isset($node->title))
                {{ e($node->title) }}
            @endif
        </p>
    </div>

    <div class="col-md-12 visible-xs topGamesBox" >
        @if(!empty($clubs))
            @foreach($clubs as $club)

                @if(!empty($club->alt_img))
                    <div class="columns six faceDiv" style="float: left; width: 50%; padding: 0; margin-bottom:0 !important;">
                        <div class="nopadding"
                             style="width:100%; height:200px; float:left; background-size: cover; background-position: 50%; background-image:url('https://www.footballticketpad.com/{{$club->alt_img}}')">
                            <a href="/group/club/{{$club->slug}}">
                                <div class="gradiant">
                                    <p style="position: relative;top: 70%;text-align: center;color: white;font-size: 18px;">
                                        {{$club->title}} {{trans('homepage.Tickets')}}
                                    </p>
                                </div>
                            </a>
                        </div>
                    </div>
                @endif
            @endforeach
        @endif
    </div>
    <div class="col-xs-12 visible-xs"
         style="width: 100%;float: left;color: WHITE;text-align: center;font-size: 20px;background: #72bd5f;padding: 20px;"
         onclick=" $('html, body').animate({scrollTop: $('.searchbyteam').offset().top}, 2000);">
        {{trans('homepage.more')}}
    </div>

    <div class="col-md-12 visible-xs">
   @if(!empty($tickets))

            <h2 style="clear: both;
    padding: 5px;
    text-align: center;
    width: 100%;
    float: left;">{{trans('homepage.Top matches')}}</h2>
    @foreach($tickets as $ticket)
            <div class="columns six" style="float: left; width: 50% !important;padding: 0; margin-bottom:0 !important;">
                <div class="nopadding"
                     style="width:100%; height:200px; float:left; background-size: cover; background-position: 50% 50%;  background-image:url('https://www.footballticketpad.com/{{$ticket->feature_imagex}}') ">
                    <a href="{{FootBallEvent::getUrl($ticket)}}">
                        <div class="gradiant">
                            <p style="position: relative;top: 40%;text-align: center;color: white;font-size: 20px;">{{$ticket->title}}</p>
                        </div>
                    </a>
                </div>
            </div>
    @endforeach
        @endif
        </div>
    <div class="row mobileRowFlow">
        <div class="site-content mobileLowerContent" style="" >

            <div class="cleanTeamRow  hidden-xs">
            @if(!empty($clubs))
                @foreach($clubs as $club)
                    @if(!empty($club->alt_img))
                    <article class="teamRowl" style="border: 10px solid white; margin: 0 !important; position: relative">
                        <div class="panel"><a href="/group/club/{{$club->slug}}">
                                <span class="hovereffect" style="width: 100%;height: 100%;float: left;">
                    <img class="plushover" src="https://cdn3.footballticketpad.com/frontend/bm2014/images/pluspng.png" alt="plus">
                    <img class="ftplinehover" src="https://cdn3.footballticketpad.com/frontend/bm2014/images/ftpline.png" alt="line">
                    </span>
                                <img src="{{$club->alt_img}}" style="width: 100%;padding-bottom: 0px !important;"></a>
                              </div>
                        <h4 style="padding: 7px;margin: 0;text-align: center;width: 100%;"><a href="/group/club/{{$club->slug}}"> {{trans('homepage.frtickets')}} {{$club->title}} {{trans('homepage.Tickets')}}</a></h4>

                    </article>
                    @endif
                @endforeach
            @endif
            </div>

            <div class="hidden-xs" style="float: left;margin-top: 20px; width:100%;">
                @if(isset($node->content))
                    {{ Template::doShortCode($node->content ) }}
                @endif
            </div>
            @if( isset($tickets) && count($tickets) > 0)
                <div class="hidden-xs">
                <h2 style="float: left; display: inline-block;">{{trans('homepage.Top matches')}}</h2>
                <hr>
                @foreach($tickets as $ticket)
                <!--repeater-->
                    <div class="columns four topmatch">
                        <div class="ft-image-match">
                            <a href="{{FootBallEvent::getUrl($ticket)}}">
                                @if(trim($ticket->feature_image) != '')
                                    <img src="{{$ticket->feature_image}}" alt="{{$ticket->title}}"/>
                                @else
                                    <div class="default-fallback">
                                        <img src="{{ @$ticket->homeTeamClubLog->value }}"/>
                                        <span>{{trans('homepage.VS')}}</span>
                                        <img src="{{ @$ticket->awayTeamClubLog->value }}"/>
                                    </div>
                                @endif
                            </a>
                        </div>
                        <span class="ftp-line">
                            <span class="greenline"></span>
                            <span class="yellowline"></span>
                            <span class="pinkline"></span>
                            <span class="blueline"></span>
                        </span>
                        <a href="{{FootBallEvent::getUrl($ticket)}}">
                            <span class="gamematch">{{$ticket->title}}</span>
                        </a>
                        <span class="stadium"><img
                                    src="https://cdn3.footballticketpad.com/frontend/bm2014/images/stadium.png"/>{{$ticket->event_location}}</span>
                        <span class="date-time"><img
                                    src="https://cdn3.footballticketpad.com/frontend/bm2014/images/whistle.png"/>{{date('d-m-y', strtotime($ticket->datetime))}}</span>

                        <div class="btnsgroup">
                            <a href="{{ FootBallEvent::getUrl($ticket) }}"
                               class="btn pinkbtn"> {{trans('homepage.BUY')}} </a>
                            <a href="{{ '/ticket/sell/'.$ticket->id }}"
                               class="btn bluebtn"> {{trans('homepage.SELL')}} </a>
                        </div>
                    </div>
                @endforeach
                <hr>
                </div>
            @endif
            @if(!empty($clubs))
                <div class="columns six teamlistDesctop" style="padding: 0; !important;text-align: center;">
                    <h2 class="searchbyteam">{{trans('homepage.Search by Team')}}</h2>
                    <ul class="clublist columnlist columns twelve">
                        @foreach($clubs as $club)
                            <li class="columns six nopadding" style="width: 50% !important; float: left;">
                                <a href="/group/club/{{$club->slug}}">
                                    <img src="{{ @$club->clubLogo->value}}"/>
                                    <span>{{trans('homepage.frtickets')}} {{$club->title}} {{trans('homepage.Tickets')}}</span>
                                </a>
                            </li>
                        @endforeach

                    </ul>
                </div>
            @endif
            @if(!empty($upcomingEvents))
                <div class="hidden-xs columns five upcomingGamesDesctop">
                    <h2>{{trans('homepage.Upcoming Matches')}}</h2>
                    <ul class="list-upcoming-matches">
                        @foreach($upcomingEvents as $ticket)
                            <li class="columns twelve brderbtm upcominglist">
                                <span class="columns four nopadding">
                                    {{date('d-m-y', strtotime($ticket->datetime))}}
                                </span>
                                <div class="columns eight">
                                    <a href="{{ FootBallEvent::getUrl($ticket) }}">{{$ticket->title}}</a>
                                </div>
                            </li>
                        @endforeach
                    </ul>

                </div>
            @endif



        </div>
    </div>
@stop
{{ Assets::jsStart() }}
<script>
    (function ($) {
        var index = 0;
        $('.site-content > p').each(function () {
            if ($.trim($(this).html()) != '&nbsp;' && index < 2) {
                $(this).addClass('para-display');
                index++;
            } else if ($.trim($(this).html()) != '&nbsp;') {
                $(this).addClass('masked');
            }
        });

        if (index > 1) {
            $('<div class="show-all-text"><a href="#" class="down-arrow">{{trans("homepage.Show more")}}</a></div><div stye="clear: both">').insertAfter('.site-content p.masked:last');
        }

        $('body').on('click', '.show-all-text a.down-arrow', function (e) {
            e.preventDefault();
            $('.site-content p.masked').addClass('unmasked');
            $('.site-content p.unmasked').removeClass('masked');
            $(this).addClass('up-arrow');
            $(this).removeClass('down-arrow');
            $(this).html('{{trans("homepage.Hide")}}');
        });

        $('body').on('click', '.show-all-text a.up-arrow', function (e) {
            e.preventDefault();
            $('.site-content p.unmasked').addClass('masked');
            $('.site-content p.masked').removeClass('unmasked');
            $(this).addClass('down-arrow');
            $(this).removeClass('up-arrow');
            $(this).html('{{trans("homepage.Show more")}}');
            var firstPara = $('.site-content p.para-display').first();
            $('html,body').animate({scrollTop: firstPara.offset().top - 100}, 'slow');
        });

        $('.site-content').css({display: 'block'})

    })(jQuery)

</script>
{{ Assets::jsEnd() }}