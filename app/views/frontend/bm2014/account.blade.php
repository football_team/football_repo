@extends(Template::name('frontend.%s._layout.layout'))
@section('style')
{{ HTML::style('ckeditor/contents.css') }}
@stop
@section('content')

@include(Template::name('frontend.%s._layout.account-tabs'))
<!---------main content------------>
<div class="accountarea">
<div class="site-content">
<!---------faz shortcode------------>
<div class="row">
    <h2>{{trans('homepage.accToS')}}</h2>
</div>

<!---------toolbar------------>
<div class="row toolbar">

    <span class="filter-label pull-left offset-right">{{trans('homepage.Filter By')}}</span>

    <input type="text" placeholder="Filter by event" class="quickfilter  pull-left offset-right">


    <div class="pull-right">

        <a href="#"><span class="btn medium bluebtn pull-right ticket-listing">{{trans('homepage.List Tickets')}}</span> </a>

    </div>

    <span class="ftp-line">
        <span class="greenline"></span>
        <span class="yellowline"></span>
        <span class="pinkline"></span>
        <span class="blueline"></span>
    </span>

</div>
<!---------toolbar------------>

@foreach($events as $event)
<div class="row listing {{$event->slug}}"
     data-slug="{{$event->slug}}"
     data-label="{{$event->title}}"
     data-type-ids = "{{$event->ticket_type_ids}}"
     data-form-of-ticket = "{{$event->form_of_ticket_ids}}" >
    <div class="collapsible_header">

        <div class="thumb-crop pull-left">
            @if($event->feature_image != '')
            <img src="{{$event->feature_image}}" alt="{{$event->title}}"/>
            @else
            <img src="{{ Assets::Path('images/default.jpg') }}" alt="{{$event->title}}"/>
            @endif
        </div>

        <div class="pull-left gamedetail">
            <span class="game">{{$event->title}} </span>
            <span class="game-info pull-left  clearboth"> {{date('l dS F Y, h:ia', strtotime($event->datetime))}}. {{$event->event_location}}</span>
        </div>
    </div>

    <div class="collapsible_content" style="display: none" data-loaded="no" data-event-information="{{route('account.listing.event', array('id'=>$event->id))}}">

    </div>
</div>
@endforeach

<!---------faz shortcode------------>

</div>
</div>
<!---------main content------------>
<style>
    .ui-widget-content{
        background-color:#fff !important;
    }
    .ui-tabs .ui-tabs-panel{
        padding:0px !important;
    }
    .ui-widget-header{
        border: 1px solid #C2C2C2 !important;
        color: #fff;
        font-weight: bold;
        background:none;
        background-color: #57CADD !important;
    }

    .ui-state-active, .ui-widget-content .ui-state-active, .ui-widget-header .ui-state-active {
        border: 1px solid #848484;
    }

    .ui-state-active a, .ui-state-active a:link, .ui-state-active a:visited {
        color: #1c94c4;
    }

    .ui-state-hover, .ui-widget-content .ui-state-hover, .ui-widget-header .ui-state-hover, .ui-state-focus, .ui-widget-content .ui-state-focus, .ui-widget-header .ui-state-focus {
        border: 1px solid #ccc;
        background: #f6f6f6 url("images/ui-bg_glass_100_f6f6f6_1x400.png") 50% 50% repeat-x;
        font-weight: bold;
        color: #1c94c4;
    }

    .ui-state-hover a, .ui-state-hover a:hover, .ui-state-hover a:link, .ui-state-hover a:visited, .ui-state-focus a, .ui-state-focus a:hover, .ui-state-focus a:link, .ui-state-focus a:visited {
        color: #1c94c4;
        text-decoration: none;
    }

    #compare-modal table tbody tr:nth-child(2n)
    {
        border-top:none;
    }

    #compare-modal table tbody tr td{
        padding:9px 1px;
    }

    #compare-modal{
        width:320px;
        margin-left:-160px;
    }

    .ticket-row input[type="text"]{
        padding: 3px !important;
        border-radius: 0px !important;
        height: 25px !important;
        margin-bottom:0px !important;
    }

    .ticket-row select{
        height:27px  !important;
    }

    .responsivetable .has-tip{
        padding:0px !important;
        width:25px !important;
        margin-bottom:0px !important;
    }

    .ticket-price input[type="text"], .ticket-block input[type="text"]{
    }
    .min100{
        min-width:100px;
    }
    .site-content .row
    {
        width:100% !important;
    }
    .collapsible_content
    {
        padding:0px;
    }
    h2
    {
        margin-left:20px;
    }

    .stacktable.small-only{
        margin-bottom: 0px;
     }

    .stacktable.small-only .st-key{
        text-align:center;
    }

    .stacktable.small-only .st-val{
        text-align:center;
    }

    .stacktable.small-only .st-val .has-tip{
        margin-left: auto;
        margin-right: auto;
        float:initial;
        margin: 10px 30px !important;
    }

    .stacktable.small-only .st-val select{
        width:100% !important;
        background:none;
    }

    .small-only>tbody>tr:last-child{
        background:none;
    }
    .small-only>tbody>tr:last-child .st-key { display:none; }
    .small-only>tbody>tr:last-child .st-val {
        display: block;
        width: 200%;
        clear: both;
        border-bottom: 10px solid #e13d7f !important;
    }

    .reveal-modal .close-reveal-modal{
        font-size: 22px;
        font-size: 1.2rem;
        line-height: .5;
        position: absolute;
        top: 15px;
        right: 11px;
        color: #000;
        font-weight: bold;
        cursor: pointer;
        background: none;
    }

</style>

@stop
{{  Assets::setStyles([
    'stacktable'           => 'js/stacktable/stacktable.css'
], false, true) }}


{{
    Assets::setScripts(
    [
        'underscore'             => 'js/underscore.min.js',
        'stacktable'           => 'js/stacktable/stacktable.min.js'


    ], false, true);
}}

{{ Assets::jsStart() }}


<script type="text/javascript" charset="utf-8">
    (function ($) {

        var body = $('body');

        /* close modal */
        body.on('click','.close-reveal-modal', function (e) {
            e.preventDefault();
            var ElemId = $(this).closest('div.reveal-modal').attr('id');
            $('#'+ElemId).foundation('reveal', 'close');
            $('#'+ElemId).fadeOut('slow', function () {
                $('#'+ElemId).remove();
                $('.reveal-modal-bg').remove();
            });
        });

        body.on('click','.reveal-modal-bg', function (e) {
            e.preventDefault();
            var ElemId = body.find('div.reveal-modal').attr('id');
            $('#'+ElemId).foundation('reveal', 'close');
            $('#'+ElemId).fadeOut('slow', function () {
                $('#'+ElemId).remove();
                $('.reveal-modal-bg').remove();
            });
        })

    })(jQuery);
</script>


<script type="text/javascript" charset="utf-8">

(function ($) {

    var getEventTicketType = function (parentObject) {
        var _types = parentObject.data('type-ids');
        var _TypesArr = _types.split(',');
        return _TypesArr;
    }

    $(".collapsible_header").click(function () {
        var content = $(this).parent().children(".collapsible_content");
        var ticketTypes = {{json_encode($ticketType)}};
        var formOfTicket =  {{json_encode($formOfTicket)}};
        var _listing = $(this).closest('div.listing');

        content.slideToggle(500);
        content.toggleClass('active');
        if(content.data('loaded') != 'yes')
        {
            var url = content.data('event-information');
            $.ajax({
                url: url,
                success: function (response) {
                    var template = _.template($('#template-ticket-listing').html());
                    content.append(template({tickets: response, types: ticketTypes, selectedTypes: getEventTicketType(_listing), formOfTicket: formOfTicket}));
                    content.data('loaded', 'yes');

                    $('.responsivetable').cardtable({myClass:'mob-tables'});
                }
            });

        }
    });

    $("body").on('click', '.statusChange', function (e) {
        e.preventDefault();
        var row = $(this).closest('tr.ticket-row');
        var rowId = row.data('row-id');

        if(typeof rowId == 'undefined')
        {
            var classes = $(row).attr("class").split(' ');
            var splitclass = classes[classes.length - 1].split('-');
            rowId = splitclass[splitclass.length - 1];
        }

        var url = '/account/listing/event/update/'+rowId;
        var newStatus = $(this).data('current-status') == 'active'? 'paused': 'active';
        var data = {
            type: 'status',
            value: newStatus
        };
        var self = $(this);
        $.ajax({
            url: url,
            data: data,
            type: 'POST',
            success: function (response) {
                if(response == 'success') {
                    self.removeClass(self.data('current-status'));
                    self.addClass(newStatus);
                    self.data('current-status', newStatus);
                }
            }
        });
    });

    $("body").on('click', '.delete-ticket', function (e) {
        e.preventDefault();
        var row = $(this).closest('tr.ticket-row');
        var rowId = row.data('row-id');
        if(typeof rowId == 'undefined')
        {
            var classes = $(row).attr("class").split(' ');
            var splitclass = classes[classes.length - 1].split('-');
            rowId = splitclass[splitclass.length - 1];
        }
        var url = '/account/listing/event/update/'+rowId;
        var newStatus = 'deleted';
        var data = {
            type: 'status',
            value: newStatus
        };

        var smallTable = $('.mob-tables .ticket-row-'+rowId);
        var smallTable = $(smallTable[0]).closest('.small-only');

        var largeTable = $('.large-only .ticket-row-'+rowId);

        $.ajax({
            url: url,
            data: data,
            type: 'POST',
            success: function (response) {
                if(response == 'success') {

                    $(smallTable).remove();
                    $(largeTable).remove();
                }

                $('.tooltip').css({display: 'none'});
            }
        });
    });

    var cloneTicket = function (elem) {
        var parent = $(elem).closest('tr.ticket-row');
        var ticketId = parent.data('row-id');
        if(typeof ticketId == 'undefined')
        {
            var classes = $(parent).attr("class").split(' ');
            var splitclass = classes[classes.length - 1].split('-');
            ticketId = splitclass[splitclass.length - 1];
        }
        var url = '/ticket/clone/'+ticketId;
        var self = $(this);


        var _listing = $(elem).closest('div.listing');
        var content = $(elem).closest(".collapsible_content ");

        $.ajax({
            url: url,
            type: 'get',
            dataType: 'json',
            success: function (response) {
                var ticketTypes = {{json_encode($ticketType)}};
                var formOfTicket =  {{json_encode($formOfTicket) }}
                var url = content.data('event-information');
                $.ajax({
                    url: url,
                    success: function (response) {
                        content.html('');
                        var template = _.template($('#template-ticket-listing').html());
                        content.append(template({tickets: response, types: ticketTypes, selectedTypes: getEventTicketType(_listing), formOfTicket: formOfTicket}));
                        content.data('loaded', 'yes');

                        $(content).find('.responsivetable').cardtable({myClass:'mob-tables'});
                        $('.tooltip').css({display: 'none'});
                    }
                });
            },
            error: function (response) {

            }
        });
    };
    //clone ticket
    $('body').on('tap', '.clone-ticket', function () {
        cloneTicket(this);
    });
    $('body').on('click', '.clone-ticket', function () {
        cloneTicket(this);
    });

    var compareTicket = function(id)
    {
        $.ajax({
            url: '/account/listed/get-similar-listings/'+id,
            type: 'POST',
            success: function (response) {
                for(var i=0; i < response.length; i++)
                {
                    var appString = '';
                    var appStringPrice ='';
                    var appStringQty ='';

                    if(response[i].isUsers == "1")
                    {
                        appStringStart = '<tr style="background-color:#FED265 !important;">';
                        appStringPrice = '<td><input style="padding: 5px !important; border-radius: 0px !important; width: 70px; height: 30px !important;" type="text" value="'+ parseFloat(response[i].price).toFixed(2) +'" data-orig = "'+parseFloat(response[i].price).toFixed(2)+'" class="comparePriceChange" data-id="'+response[i].id+'"></td>';
                        appStringQty = '<td><input style="padding: 5px !important; border-radius: 0px !important; width: 70px; height: 30px !important;" type="text" value="'+ response[i].available_qty +'" data-pref = "'+response[i].pref.replace(/\"/g, "").trim()+'" data-orig = "'+response[i].available_qty+'" class="compareQtyChange" data-id="'+response[i].id+'"></td>';
                    }
                    else
                    {
                        appStringStart = '<tr>';
                        appStringPrice += '<td>'+ parseFloat(response[i].price).toFixed(2) +'</td>';
                        appStringQty  = '<td>'+ response[i].available_qty +'</td>';
                    }
                    appString += appStringStart;
                    appString += '<td>'+ response[i].ticket_type +'</td>';
                    appString += '<td>'+ response[i].loc_block.replace(/\"/g, "").trim() +'</td>';
                    appString += '<td>'+ response[i].loc_row.replace(/\"/g, "").trim() +'</td>';
                    appString += appStringQty;
                    appString += appStringPrice;

                    appString += '</tr>';

                    $('#compare-table-body').append(appString);
                }
            }
        });

        $.ajax({
            url: '/account/listed/get-all-listings/'+id,
            type: 'POST',
            success: function (response) {
                for(var i=0; i < response.length; i++)
                {
                    var appString = '';
                    var appStringPrice ='';
                    var appStringQty ='';

                    if(response[i].isUsers =="1")
                    {
                        appStringStart = '<tr style="background-color:#FED265 !important;">';
                        appStringPrice = '<td><input style="padding: 5px !important; border-radius: 0px !important; width: 70px; height: 30px !important;" type="text" value="'+ parseFloat(response[i].price).toFixed(2) +'" data-orig = "'+parseFloat(response[i].price).toFixed(2)+'" class="comparePriceChange" data-id="'+response[i].id+'"></td>';
                        appStringQty = '<td><input style="padding: 5px !important; border-radius: 0px !important; width: 70px; height: 30px !important;" type="text" value="'+ response[i].available_qty +'" data-pref = "'+response[i].pref.replace(/\"/g, "").trim()+'" data-orig = "'+response[i].available_qty+'" class="compareQtyChange" data-id="'+response[i].id+'"></td>';
                    }
                    else
                    {
                        appStringStart = '<tr>';
                        appStringPrice += '<td>'+ parseFloat(response[i].price).toFixed(2) +'</td>';
                        appStringQty  = '<td>'+ response[i].available_qty +'</td>';
                    }
                    appString += appStringStart;
                    appString += '<td>'+ response[i].ticket_type +'</td>';
                    appString += '<td>'+ response[i].loc_block.replace(/\"/g, "").trim() +'</td>';
                    appString += '<td>'+ response[i].loc_row.replace(/\"/g, "").trim() +'</td>';
                    appString += appStringQty;
                    appString += appStringPrice;

                    appString += '</tr>';

                    $('#compare-table-body2').append(appString);
                }
            }
        });

        var template = _.template($('#compare-modal-template').html());
        $('#compare-modal').remove();
        $('body').append(template());
        $('#tabs').tabs();
        $('#compare-modal').foundation('reveal', 'open');
        //Show the modal
    }

    $('body').on('focusout', '.comparePriceChange', function () {
        var newPrice = parseFloat($(this).val()).toFixed(2);
        var orig = parseFloat($(this).data("orig"));
        var ticketId = $(this).data("id");


        if(newPrice != orig)
        {
            updateTicket(ticketId, 'Price', newPrice,function (response) {
                compareTicket(ticketId);
                //in here, refresh the list, refresh the existing tickets in the background.
            });
        }

    });

    $('body').on('focusout', '.compareQtyChange', function () {
        var newValue = parseInt($(this).val());
        var origValue = parseInt($(this).data('orig'));
        var ticketId = $(this).data('id');
        var pref= parseInt($(this).data('pref'));

        if(newValue % 2 !=0 && pref == 3) {
            alert('Invalid qty added.');
            $(this).val(originalValue);
            return null;
        }
        if(newValue != origValue)
        {
            updateTicket(ticketId, 'Qty', newValue,function (response) {

            });
        }

    });

    $('body').on('tap', '.compare-ticket', function(){
        var parent = $(this).closest('tr.ticket-row');
        var ticketId = parent.data('row-id');
        if(typeof ticketId == 'undefined')
        {
            var classes = $(parent).attr("class").split(' ');
            var splitclass = classes[classes.length - 1].split('-');
            ticketId = splitclass[splitclass.length - 1];
        }
        compareTicket(ticketId);
    });
    $('body').on('click', '.compare-ticket', function(){
        var parent = $(this).closest('tr.ticket-row');
        var ticketId = parent.data('row-id');
        if(typeof ticketId == 'undefined')
        {
            var classes = $(parent).attr("class").split(' ');
            var splitclass = classes[classes.length - 1].split('-');
            ticketId = splitclass[splitclass.length - 1];
        }
        compareTicket(ticketId);
    });


    $('body').on('change', '.ticket-type', function () {
        var parent = $(this).closest('tr.ticket-row');
        var ticketId = parent.data('row-id');
        if(typeof ticketId == 'undefined')
        {
            var classes = $(parent).attr("class").split(' ');
            var splitclass = classes[classes.length - 1].split('-');
            ticketId = splitclass[splitclass.length - 1];
        }
        updateTicket(ticketId, 'TicketType', $(this).val(), false);
    });

    $('body').on('change', '.form-of-ticket', function () {
        var parent = $(this).closest('tr.ticket-row');
        var ticketId = parent.data('row-id');
        if(typeof ticketId == 'undefined')
        {
            var classes = $(parent).attr("class").split(' ');
            var splitclass = classes[classes.length - 1].split('-');
            ticketId = splitclass[splitclass.length - 1];
        }
        updateTicket(ticketId, 'FormOfTicket', $(this).val(), false);
    });

    $('body').on('change', '.sell-preference', function () {
        var parent = $(this).closest('tr.ticket-row');
        var ticketId = parent.data('row-id');
        if(typeof ticketId == 'undefined')
        {
            var classes = $(parent).attr("class").split(' ');
            var splitclass = classes[classes.length - 1].split('-');
            ticketId = splitclass[splitclass.length - 1];
        }
        var qty = parseInt(parent.find('.update-new-qty').val());
        var pref= parseInt($(this).val());
        if(qty % 2 !=0 && pref == 3) {
            alert("{{trans('homepage.invalidSellPreff')}}");
            $(this).val(0);
            return null;
        }

        updateTicket(ticketId, 'SellPreference', $(this).val(), false);
    });

    $('body').on('change', '.ready-to-ship', function () {
        var parent = $(this).closest('tr.ticket-row');
        var ticketId = parent.data('row-id');
        if(typeof ticketId == 'undefined')
        {
            var classes = $(parent).attr("class").split(' ');
            var splitclass = classes[classes.length - 1].split('-');
            ticketId = splitclass[splitclass.length - 1];
        }
        updateTicket(ticketId, 'ReadyToShip', $(this).val(),false);
    });

    $('body').on('change', '.hand_delivery', function () {
        var parent = $(this).closest('tr.ticket-row');
        var ticketId = parent.data('row-id');
        if(typeof ticketId == 'undefined')
        {
            var classes = $(parent).attr("class").split(' ');
            var splitclass = classes[classes.length - 1].split('-');
            ticketId = splitclass[splitclass.length - 1];
        }
        updateTicket(ticketId, 'HandDelivery', $(this).val(),false);
    });



    function updateTicket(id, type, value, callback) {
        var url = '/ticket/update/'+id;
        var data = {type: type, value: value};

        $.ajax({
            url: url,
            data: data,
            type: 'post',
            dataType: 'json',
            success: function  (response) {
                try{
                    var divstr = "ticket-row-"+response.flag;

                    var child = $('.large-only .'+divstr);

                    var aboveLarge = $(child.prev(".large-only .ticket-row"));

                    var content = $(child).closest(".collapsible_content");

                    var _listing = $(child).closest('div.listing');

                    var ticketTypes = {{json_encode($ticketType)}};
                    var formOfTicket =  {{json_encode($formOfTicket) }}

                    $(child).remove();
                    var template = _.template($('#template-ticket-listing-indiv').html());
                    if($(aboveLarge).length > 0)
                    {
                        $(template({ticket: response.tickets, types: ticketTypes, selectedTypes: getEventTicketType(_listing), formOfTicket: formOfTicket})).insertAfter($(aboveLarge));
                    }
                    else
                    {
                        var thebody = $('> table > tbody', content);
                        $(thebody).prepend(template({ticket: response.tickets, types: ticketTypes, selectedTypes: getEventTicketType(_listing), formOfTicket: formOfTicket}));
                    }


                    var child = $('.small-only .'+divstr);
                    var child = $(child[0]).closest('.small-only');

                    var aboveSmall = $(child.prev(".small-only"));

                    var allSmall = $(child).closest('.collapsible_content > .mob-tables');

                    $(child).remove();

                    var template = _.template($('#template-ticket-listing-indiv-small').html());
                    if($(aboveSmall).length > 0)
                    {
                        $(template({ticket: response.tickets, types: ticketTypes, selectedTypes: getEventTicketType(_listing), formOfTicket: formOfTicket})).insertAfter($(aboveSmall));
                    }
                    else
                    {
                        var thebody = $('> .mob-tables', content);
                        $(thebody).prepend(template({ticket: response.tickets, types: ticketTypes, selectedTypes: getEventTicketType(_listing), formOfTicket: formOfTicket}));
                    }
                    content.data('loaded', 'yes');
                }
                catch(err)
                {
                    console.log(err);
                }
                if(callback)
                {
                   callback(response);
                }
            },
            error: function (response) {
                console.log(response);
            }
        })
    }

    $('body').on('focusout', '.update-new-price', function () {
        var parent = $(this).closest('.ticket-price');
        var originalPrice = parseFloat(parent.data('price')).toFixed(2);
        var newPrice = parseFloat($(this).val()).toFixed(2);


        if(originalPrice != newPrice ) {
            var row = parent.closest('tr.ticket-row');
            var ticketId = row.data('row-id');
            if(typeof ticketId == 'undefined')
            {
                var classes = $(row).attr("class").split(' ');
                var splitclass = classes[classes.length - 1].split('-');
                ticketId = splitclass[splitclass.length - 1];
            }
            parent.find('span').html(newPrice);
            updateTicket(ticketId, 'Price', newPrice,false);
        }
    });


    $('body').on('focusout', '.update-new-qty', function () {
        var parent = $(this).closest('.ticket-qty');
        var originalValue= parseInt(parent.data('qty'));
        var newValue = parseInt($(this).val());
        var parentTr = parent.closest('tr');

        if(originalValue != newValue ) {
            var row = parent.closest('tr.ticket-row');
            var ticketId = row.data('row-id');
            if(typeof ticketId == 'undefined')
            {
                var classes = $(row).attr("class").split(' ');
                var splitclass = classes[classes.length - 1].split('-');
                ticketId = splitclass[splitclass.length - 1];
            }
            var pref= parseInt(parentTr.find('.sell-preference').val());

            if(newValue % 2 !=0 && pref == 3) {
                alert('Invalid qty added.');
                $(this).val(originalValue);
                return null;
            }

            updateTicket(ticketId, 'Qty', newValue,function (response) {
                parent.data('qty', newValue);
            });
        }
    });

    $('body').on('focusout', '.update-new-block', function () {
        var parent = $(this).closest('.ticket-block-row');
        var originalValue= parent.data('block');
        var newValue = $(this).val();

        if(originalValue != newValue ) {
            var row = parent.closest('tr.ticket-row');
            var ticketId = row.data('row-id');
            if(typeof ticketId == 'undefined')
            {
                var classes = $(row).attr("class").split(' ');
                var splitclass = classes[classes.length - 1].split('-');
                ticketId = splitclass[splitclass.length - 1];
            }

            parent.find('span.ticket-block').html(newValue);
            updateTicket(ticketId, 'BlockAndRow', {block: newValue},function (response) {

            });
        }
    });

    $('body').on('focusout', '.update-new-row', function () {
        var parent = $(this).closest('.ticket-block-row');
        var originalValue= parent.data('row');
        var newValue = $(this).val();

        if(originalValue != newValue ) {
            var row = parent.closest('tr.ticket-row');
            var ticketId = row.data('row-id');
            if(typeof ticketId == 'undefined')
            {
                var classes = $(row).attr("class").split(' ');
                var splitclass = classes[classes.length - 1].split('-');
                ticketId = splitclass[splitclass.length - 1];
            }

            parent.find('span.ticket-row').html(newValue);
            updateTicket(ticketId, 'BlockAndRow', {row: newValue},function (response) {

            });
        }
    });

    $('body').on('click', '.buyer-note-submit', function (e) {
        e.preventDefault();
        var ticketId = $('#buyer-note-id').val();
        var data = {};


        $('#buyer-note-form').find('input[type=checkbox]').each(function (){
            if($(this).prop('checked')) {
                data[$(this).val()] = $(this).val();
            }
        });

        if ($("[name=restrictions\\[11\\]]")) {
            data.others = $("[name=restrictions\\[others\\]]").val() || "";
        } else {
            data.others = '';
        }
        updateTicket(ticketId, 'BuyerNote', data ,function (response) {
            $('.close-reveal-modal').trigger('click');
        });
    });


})(jQuery)
</script>

<script type="text/x-template" id="template-ticket-listing-indiv" charset="utf-8">
    <tr class="<%= ticket.status %> ticket-row ticket-row-<%= ticket.id %>" data-row-id="<%= ticket.id %>">
        <td>
                 <span class="statusChange <%= ticket.status %>" data-current-status="<%= ticket.status %>">
                 </span>
        </td>
        <td>
            </span>
            <select class="ticket-type">
                <% _.each(types , function (type) { %>
                    <% if(selectedTypes.indexOf(""+type.id) != -1 ) {%>
                        <% if(ticket.ticket_type == type.id ){ %>
                            <option value="<%= type.id %>" selected><%= type.title %></option>
                        <% } else { %>
                            <option value="<%= type.id %>"><%= type.title %></option>
                        <% } %>
                    <% } %>
                <% }) %>
            </select>
        </td>
        <td>
            <select class="form-of-ticket">
                <% _.each(formOfTicket, function (fot) {%>

                <% if(ticket.form_of_ticket == fot.id ){ %>
                <option id="<%= fot.id %>" value="<%= fot.id %>" selected><%= fot.title %></option>
                <% } else { %>
                <option id="<%= fot.id %>" value="<%= fot.id %>" ><%= fot.title %></option>
                <% } %>
                <% }) %>
            </select>
        </td>
        <td class="ticket-qty" data-qty="<%= parseInt(ticket.number_of_ticket) %>">
            <input type="text" value="<%= parseInt(ticket.number_of_ticket) %>" class="update-new-qty" />

        </td>
        <td><%= parseInt(ticket.sold) %></td>
        <td>
            <select name="sell_preference" class="sell-preference">
                <option <%= (ticket.preference == 0)? 'selected': '' %> value="0">No preference</option>
                <option <%= (ticket.preference == 1)? 'selected': '' %> value="1">All together</option>
                <option <%= (ticket.preference == 2)? 'selected': '' %> value="2">Avoid leaving one ticket</option>
                <option <%= (ticket.preference == 3)? 'selected': '' %> value="3">In Pairs</option>
            </select>

        </td>
        <td data-block="<%= ticket.block %>" class="ticket-block-row ticket-block">
            <input type="text" value="<%= ticket.block %>" class="update-new-block" />
        </td>

        <td>{{trans('homepage.currencyInUse')}}<span><%= parseFloat(ticket.earning).toFixed(2) %>{{trans('homepage.currencyAfter')}}</span></td>

        <td data-price="<%= parseFloat(ticket.price).toFixed(2) %>" class="ticket-price">
            <input type="text" value="<%= parseFloat(ticket.price).toFixed(2) %>" class="update-new-price" />
        </td>

        <td>
            <select name="ready_to_ship" class="ready-to-ship">
                <option <%= (ticket.ready_to_ship == 0)? 'selected': '' %> value="0" >No</option>
                <option <%= (ticket.ready_to_ship == 1)? 'selected': '' %> value="1" >Yes</option>
            </select>

        </td>
        <td>
            <span style="padding-top:0px;" data-tooltip aria-haspopup="true" class="has-tip buyernote-display" title="{{trans('homepage.Buyer Notes')}}" data-buyer-note='<%= JSON.stringify(ticket.buyer_note) %>' data-default-restriction='<%= JSON.stringify(ticket.default_restriction) %>' >
                <span class="buyernotes">
                </span>
            </span>
        </td>
        <td>
            <select name="hand_delivery" class="hand_delivery">
                <option <%= (ticket.can_offer_collection == 0)? 'selected': '' %> value="0" >No</option>
                <option <%= (ticket.can_offer_collection == 1)? 'selected': '' %> value="1" >Yes</option>
            </select>

        </td>
        <td class="min100">
            <span data-tooltip aria-haspopup="true" class="has-tip" title="{{trans('homepage.compare')}}">
                <span class="compare-ticket"></span>
            </span>
            <span data-tooltip aria-haspopup="true" class="has-tip delete-ticket" title="{{trans('homepage.trash')}}">
                <span class="trash"></span>
            </span>
            <span data-tooltip aria-haspopup="true" class="has-tip" title="{{trans('homepage.copy')}}">
                <span class="add clone-ticket"></span>
            </span>
        </td>
    </tr>
</script>

<script type="text/x-template" id="template-ticket-listing-indiv-small" charset="utf-8">
    <table class="responsivetable stacktable small-only">
        <tbody>
            <tr class="<%= ticket.status %> ticket-row ticket-row-<%= ticket.id %>">
                <td class="st-key">
                    Active
                </td>
                <td class="st-val">
                    <span class="statusChange <%= ticket.status %>" data-current-status="<%= ticket.status %>"></span>
                </td>
            </tr>
            <tr class="<%= ticket.status %> ticket-row ticket-row-<%= ticket.id %>">
                <td class="st-key">
                    {{trans('homepage.tttc')}}
                </td>
                <td class="st-val">
                    <select class="ticket-type">
                        <% _.each(types , function (type) { %>
                            <% if(selectedTypes.indexOf(""+type.id) != -1 ) {%>
                                <% if(ticket.ticket_type == type.id ){ %>
                                    <option value="<%= type.id %>" selected><%= type.title %></option>
                                <% } else { %>
                                    <option value="<%= type.id %>"><%= type.title %></option>
                                <% } %>
                            <% } %>
                        <% }) %>
                    </select>
                </td>
            </tr>
            <tr class="<%= ticket.status %> ticket-row ticket-row-<%= ticket.id %>">
                <td class="st-key">
                    {{trans('homepage.Form of Ticket')}}
                </td>
                <td class="st-val">
                    <select class="form-of-ticket">
                        <% _.each(formOfTicket, function (fot) {%>

                        <% if(ticket.form_of_ticket == fot.id ){ %>
                        <option id="<%= fot.id %>" value="<%= fot.id %>" selected><%= fot.title %></option>
                        <% } else { %>
                        <option id="<%= fot.id %>" value="<%= fot.id %>" ><%= fot.title %></option>
                        <% } %>
                        <% }) %>
                    </select>
                </td>
            </tr>
            <tr class="<%= ticket.status %> ticket-row ticket-row-<%= ticket.id %>">
                <td class="st-key">
                    {{trans('homepage.QTY')}}
                </td>
                <td class="st-val ticket-qty">
                    <input type="text" value="<%= parseInt(ticket.number_of_ticket) %>" class="update-new-qty" />
                </td>
            </tr>
            <tr class="<%= ticket.status %> ticket-row ticket-row-<%= ticket.id %>">
                <td class="st-key">
                    {{trans('homepage.Sold')}}
                </td>
                <td class="st-val">
                    <%= parseInt(ticket.sold) %>
                </td>
            </tr>
            <tr class="<%= ticket.status %> ticket-row ticket-row-<%= ticket.id %>">
                <td class="st-key">
                    {{trans('homepage.Split')}}
                </td>
                <td class="st-val">
                    <select name="sell_preference" class="sell-preference">
                        <option <%= (ticket.preference == 0)? 'selected': '' %> value="0">No preference</option>
                        <option <%= (ticket.preference == 1)? 'selected': '' %> value="1">All together</option>
                        <option <%= (ticket.preference == 2)? 'selected': '' %> value="2">Avoid leaving one ticket</option>
                        <option <%= (ticket.preference == 3)? 'selected': '' %> value="3">In Pairs</option>
                    </select>
                </td>
            </tr>
            <tr class="<%= ticket.status %> ticket-row ticket-row-<%= ticket.id %>">
                <td class="st-key">
                    Block
                </td>
                <td class="st-val ticket-block">
                    <input type="text" value="<%= ticket.block %>" class="update-new-block" />
                </td>
            </tr>
            <tr class="<%= ticket.status %> ticket-row ticket-row-<%= ticket.id %>">
                <td class="st-key">
                    {{trans('homepage.Web Price')}}
                </td>
                <td class="st-val">
                    {{trans('homepage.currencyInUse')}}<span><%= parseFloat(ticket.earning).toFixed(2) %>{{trans('homepage.currencyAfter')}}</span>
                </td>
            </tr>
            <tr class="<%= ticket.status %> ticket-row ticket-row-<%= ticket.id %>">
                <td class="st-key">
                    {{trans('homepage.accMEarnings')}} ({{trans('homepage.currencyInUse')}}) {{trans('homepage.currencyAfter')}}
                </td>
                <td class="st-val ticket-price">
                    <input type="text" value="<%= parseFloat(ticket.price).toFixed(2) %>" class="update-new-price" />
                </td>
            </tr>
            <tr class="<%= ticket.status %> ticket-row ticket-row-<%= ticket.id %>">
                <td class="st-key">
                    {{trans('homepage.accRtoS')}}
                </td>
                <td class="st-val">
                    <select name="ready_to_ship" class="ready-to-ship">
                        <option <%= (ticket.ready_to_ship == 0)? 'selected': '' %> value="0" >No</option>
                        <option <%= (ticket.ready_to_ship == 1)? 'selected': '' %> value="1" >Yes</option>
                    </select>
                </td>
            </tr>
            <tr class="<%= ticket.status %> ticket-row ticket-row-<%= ticket.id %>">
                <td class="st-key">
                    {{trans('homepage.Buyer Notes')}}
                </td>
                <td class="st-val">
                    <span style="padding-top:0px;" data-tooltip aria-haspopup="true" class="has-tip buyernote-display" title="{{trans('homepage.Buyer Notes')}}" data-buyer-note='<%= JSON.stringify(ticket.buyer_note) %>' data-default-restriction='<%= JSON.stringify(ticket.default_restriction) %>' >
                        <span class="buyernotes">
                        </span>
                    </span>
                </td>
            </tr>
            <tr class="<%= ticket.status %> ticket-row ticket-row-<%= ticket.id %>">
                <td class="st-key">
                    {{trans('homepage.canHD')}}
                </td>
                <td class="st-val">
                    <select name="hand_delivery" class="hand_delivery">
                        <option <%= (ticket.can_offer_collection == 0)? 'selected': '' %> value="0" >No</option>
                        <option <%= (ticket.can_offer_collection == 1)? 'selected': '' %> value="1" >Yes</option>
                    </select>
                </td>
            </tr>
            <tr class="<%= ticket.status %> ticket-row ticket-row-<%= ticket.id %>">
                <td class="st-key">
                    &nbsp;
                </td>
                <td class="st-val">
                    <span data-tooltip aria-haspopup="true" class="has-tip" title="{{trans('homepage.compare')}}">
                        <span class="compare-ticket"></span>
                    </span>
                    <span data-tooltip aria-haspopup="true" class="has-tip delete-ticket" title="{{trans('homepage.trash')}}">
                        <span class="trash"></span>
                    </span>
                    <span data-tooltip aria-haspopup="true" class="has-tip" title="{{trans('homepage.copy')}}">
                        <span class="add clone-ticket"></span>
                    </span>
                </td>
            </tr>
        </tbody>
    </table>
</script>


<script type="text/x-template" id="template-ticket-listing" charset="utf-8">

<table class="responsivetable">
    <thead>
        <th>Active</th>
        <th>{{trans('homepage.tttc')}}</th>
        <th>{{trans('homepage.Form of Ticket')}}</th>
        <th>{{trans('homepage.QTY')}}</th>
        <th>{{trans('homepage.Sold')}}</th>
        <th>{{trans('homepage.Split')}}</th>
        <th>Block</th>
        <th>{{trans('homepage.Web Price')}}</th>
        <th>{{trans('homepage.accMEarnings')}} ({{trans('homepage.currencyInUse')}}) {{trans('homepage.currencyAfter')}}</th>
        <th>{{trans('homepage.accRtoS')}}</th>
        <th>{{trans('homepage.Buyer Notes')}}</th>
        <th>{{trans('homepage.canHD')}}</th>
        <th>&nbsp;</th>
    </thead>
    <tbody>
        <% _.each(tickets, function (ticket) {  %>
        <tr class="<%= ticket.status %> ticket-row ticket-row-<%= ticket.id %>" data-row-id="<%= ticket.id %>">
            <td>
                     <span class="statusChange <%= ticket.status %>" data-current-status="<%= ticket.status %>">
                     </span>
            </td>
            <td>
                </span>
                <select class="ticket-type">
                    <% _.each(types , function (type) { %>
                        <% if(selectedTypes.indexOf(""+type.id) != -1 ) {%>
                            <% if(ticket.ticket_type == type.id ){ %>
                                <option value="<%= type.id %>" selected><%= type.title %></option>
                            <% } else { %>
                                <option value="<%= type.id %>"><%= type.title %></option>
                            <% } %>
                        <% } %>
                    <% }) %>
                </select>
            </td>
            <td>
                <select class="form-of-ticket">
                    <% _.each(formOfTicket, function (fot) {%>

                    <% if(ticket.form_of_ticket == fot.id ){ %>
                    <option id="<%= fot.id %>" value="<%= fot.id %>" selected><%= fot.title %></option>
                    <% } else { %>
                    <option id="<%= fot.id %>" value="<%= fot.id %>" ><%= fot.title %></option>
                    <% } %>
                    <% }) %>
                </select>
            </td>
            <td class="ticket-qty" data-qty="<%= parseInt(ticket.number_of_ticket) %>">
                <input type="text" value="<%= parseInt(ticket.number_of_ticket) %>" class="update-new-qty" />

            </td>
            <td><%= parseInt(ticket.sold) %></td>
            <td>
                <select name="sell_preference" class="sell-preference">
                    <option <%= (ticket.preference == 0)? 'selected': '' %> value="0">No preference</option>
                    <option <%= (ticket.preference == 1)? 'selected': '' %> value="1">All together</option>
                    <option <%= (ticket.preference == 2)? 'selected': '' %> value="2">Avoid leaving one ticket</option>
                    <option <%= (ticket.preference == 3)? 'selected': '' %> value="3">In Pairs</option>
                </select>

            </td>
            <td data-block="<%= ticket.block %>" class="ticket-block-row ticket-block">
                <input type="text" value="<%= ticket.block %>" class="update-new-block" />
            </td>

            <td>{{trans('homepage.currencyInUse')}}<span><%= parseFloat(ticket.earning).toFixed(2) %> {{trans('homepage.currencyAfter')}}</span></td>

            <td data-price="<%= parseFloat(ticket.price).toFixed(2) %>" class="ticket-price">
                <input type="text" value="<%= parseFloat(ticket.price).toFixed(2) %>" class="update-new-price" />
            </td>

            <td>
                <select name="ready_to_ship" class="ready-to-ship">
                    <option <%= (ticket.ready_to_ship == 0)? 'selected': '' %> value="0" >No</option>
                    <option <%= (ticket.ready_to_ship == 1)? 'selected': '' %> value="1" >Yes</option>
                </select>

            </td>
            <td>
                <span style="padding-top:0px;" data-tooltip aria-haspopup="true" class="has-tip buyernote-display" title="{{trans('homepage.Buyer Notes')}}" data-buyer-note='<%= JSON.stringify(ticket.buyer_note) %>' data-default-restriction='<%= JSON.stringify(ticket.default_restriction) %>' >
                    <span class="buyernotes">
                    </span>
                </span>
            </td>
            <td>
                <select name="HandDelivery" class="hand_delivery">
                    <option <%= (ticket.can_offer_collection == 0)? 'selected': '' %> value="0" >No</option>
                    <option <%= (ticket.can_offer_collection == 1)? 'selected': '' %> value="1" >Yes</option>
                </select>

            </td>
            <td class="min100">
                <span data-tooltip aria-haspopup="true" class="has-tip" title="{{trans('homepage.compare')}}">
                    <span class="compare-ticket"></span>
                </span>
                <span data-tooltip aria-haspopup="true" class="has-tip delete-ticket" title="{{trans('homepage.trash')}}">
                    <span class="trash"></span>
                </span>
                <span data-tooltip aria-haspopup="true" class="has-tip" title="{{trans('homepage.copy')}}">
                    <span class="add clone-ticket"></span>
                </span>
            </td>
        </tr>
        <% }); %>
    </tbody>
</table>
</script>

<script type="text/x-template" id="buyer-note-template" charset="utf-8">
    <div id="buyer-note-modal" class="reveal-modal" data-reveal style="display:none">
        <form name="buyer-note" action="#" id="buyer-note-form">
        <input name="id" type="hidden" value="<%= id %>" id="buyer-note-id">
        <h2>{{trans('homepage.Buyer Notes')}}</h2>
        <p class="buyer-note">

        <ul>
            <% for(i in defaultRestriction) {  %>
            <li>
                <label>
                    <input type="checkbox" value="<%=defaultRestriction[i].id%>" name="restrictions[<%=defaultRestriction[i].id%>]" <%= defaultRestriction[i].checked %> > <%= defaultRestriction[i].title %>
                </label>
                <% if (defaultRestriction[i].id == 11) { %>
                <textarea name="restrictions[others]" rows="3" cols="50"><%= (buyerNote.others)? buyerNote.others: '' %></textarea>
                <% } %>
            </li>
            <% } %>
        </ul>

        <br/>

        </p>
        <button class="buyer-note-submit button" >{{trans('homepage.update')}}</button>
        </form>
        <a class="close-reveal-modal">X</a>
    </div>
</script>

<script type="text/x-template" id="listing-modal-template" charset="utf-8">
    <div id="listing-modal" class="reveal-modal" data-reveal style="display:none">
            <div id="project-label">{{trans('homepage.accSelAG')}}:</div>
            <!-- images/transparent_1x1.png -->
            <img id="project-icon" src="" class="ui-state-default" alt="">
            <input name="game_name" id="project" placeholder="{{trans('homepage.accPTAGNH')}}" type="text" />
            <input type="hidden" id="project-id">
            <a class="close-reveal-modal">X</a>
    </div>
</script>

<script type="text/x-template" id="compare-modal-template" charset="utf-8">
    <div id="compare-modal" class="reveal-modal" data-reveal style="display:none; border-radius:4px 4px 0 0 !important; padding:0px !important;">
        <div id="tabs" style="padding:0px !important;">
            <ul>
                <li><a href="#tabs-1">Similar Listings</a></li>
                <li><a href="#tabs-2">All Listings</a></li>
            </ul>
            <div id="tabs-1" style="height:300px; overflow-y:scroll;">
                <table style="width:100%;">
                    <thead>
                        <tr>
                            <th>
                                Ticket Type
                            </th>
                            <th>
                                Block
                            </th>
                            <th>
                                Qty
                            </th>
                            <th>
                                Price ({{trans('homepage.currencyInUse')}}{{trans('homepage.currencyAfter')}})
                            </th>
                        </tr>
                    </thead>
                    <tbody id = "compare-table-body">
                    </tbody>
                </table>
            </div>
            <div id="tabs-2" style="height:300px; overflow-y:scroll;">
                <table style="width:100%;">
                    <thead>
                        <tr>
                            <th>
                                Ticket Type
                            </th>
                            <th>
                                Block
                            </th>
                            <th>
                                Qty
                            </th>
                            <th>
                                Price ({{trans('homepage.currencyInUse')}} {{trans('homepage.currencyAfter')}})
                            </th>
                        </tr>
                    </thead>
                    <tbody id = "compare-table-body2">
                    </tbody>
                </table>
            </div>
        </div>
        <a class="close-reveal-modal">X</a>
    </div>
</script>

<script>
    $(document).ready(function () {
        Foundation.global.namespace = '';

        $('select[name="sell_preference"]').change(function () {
            var qty = parseInt($('select[name="number_of_ticket"]').val());
            var pref = parseInt($(this).val());

            if(pref === 3) {

                var mod = qty % 2;

                if( mod > 0  ) {

                    alert("{{trans('homepage.accQTYsbE')}}")
                    $(this).val('');
                }
            }
        });

       $('body').on('click', '.buyernote-display', function (e) {
           var defaultRestriction = $(this).data('default-restriction');
           var buyerNote = $(this).data('buyer-note');
           var parent = $(this).closest('tr.ticket-row');
           var ticketId = parent.data('row-id');
           if(typeof ticketId == 'undefined')
            {
                var classes = $(parent).attr("class").split(' ');
                var splitclass = classes[classes.length - 1].split('-');
                ticketId = splitclass[splitclass.length - 1];
            }
           e.preventDefault();
           var template = _.template($('#buyer-note-template').html());
           $('#buyer-note-modal').remove();
           $('body').append(template({
               defaultRestriction: defaultRestriction,
               buyerNote: buyerNote,
               id : ticketId
           }));
           $('#buyer-note-modal').foundation('reveal', 'open');
       });


        $('body').on('click','.ticket-listing',function (e) {
            e.preventDefault();
            var template = _.template($('#listing-modal-template').html());
            $('#listing-modal').remove();
            $('body').append(template());
            $('#listing-modal').foundation('reveal', 'open');

            $( "#project" ).autocomplete({
                minLength: 0,
                source: "{{route('ticket.events.autocomplete')}}",
                focus: function( event, ui ) {
                    $( "#project" ).val( ui.item.label );
                    return false;
                },
                select: function( event, ui ) {
                    $( "#project" ).val( ui.item.label );
                    $( "#project-id" ).val( ui.item.value );
                    $( "#project-description" ).html( ui.item.desc );
                    //$( "#project-icon" ).attr( "src", "images/" + ui.item.icon );
                    if(isiPhone()){
                        window.location.replace(ui.item.link);
                    } else {
                        window.location = ui.item.link;
                    }

                    return false;
                }
            })
            .autocomplete( "instance" )._renderItem = function( ul, item ) {
            return $( "<li>" )
                .append( "<a>" + item.label + "<br>" + item.desc + "</a>" )
                .appendTo( ul );
            };
       });

    });

    function isiPhone(){
        return (
            (navigator.platform.indexOf("iPhone") != -1)
                || (navigator.platform.indexOf("iPod") != -1)
                || (navigator.platform.indexOf("iPad") != -1)
            );
    }

    $('#tabs').tabs();
</script>
{{ Assets::jsEnd() }}

