@extends(Template::name('frontend.%s._layout.layout'))
@section('style')
{{ HTML::style('ckeditor/contents.css') }}
@stop
@section('content')

<!---------sidebar------------>
<section class="main">
        <div class="row">
            @include(Template::name('frontend.%s.sidebars.side1'))
        </div>
</section>

<!---------sidebar------------>
<section class="banner-home  hidden-xs">

 	<span class="club-effect">
        @if(isset($node->feature_image ))
        <img class="inner-banner" src="{{ $node->feature_image }}" alt="test" />
        @else
        <img class="inner-banner" src="{{ Assets::Path('images/default.jpg') }}" alt="test" />
        @endif
    </span>

    <!-- FTP LINE-->
    <span class="ftp-line hidden-xs">
    	<span class="greenline hidden-xs"></span>
        <span class="yellowline hidden-xs"></span>
        <span class="pinkline hidden-xs"></span>
        <span class="blueline hidden-xs"></span>
    </span>

</section>



<!---------main content------------>
<div class="row mobilenEventsStireContent">
	   <div class="teaminfo">
      		@if(isset($meta_club_logo))
            <img class="club-logo fadeIn" src="{{ $meta_club_logo }}" alt="{{trans('homepage.Buy')}} {{trans('homepage.frtickets')}} {{ e($node->title) }} {{trans('homepage.Tickets')}}" />
            @else
            <img class="club-logo fadeIn" src="" alt="{{ e($node->title) }}" />
            @endif
            <h1 class="page-header fadeIn">{{trans('homepage.frtickets')}} {{ ($node->title) }} {{trans('homepage.Tickets')}}</h1>
            <div class="team-summary">
            	<span class="small-title">
                	<strong>{{trans('homepage.Team Fact File')}}:</strong>
                </span>
                <ul>
                  <li>
                  	 <span class="labeltitle"><strong>{{trans('homepage.Nickname')}}:</strong></span>
                     <span class="data">{{ @$meta_nickname }}</span>
                  </li>	
                  <li>
                  	 <span class="labeltitle"><strong>{{trans('homepage.Founded')}}:</strong></span>
                     <span class="data">{{ @$meta_founded }}</span>
                  </li>
                  <li>
                  	 <span class="labeltitle"><strong>{{trans('homepage.Rivals')}}:</strong></span>
                     <span class="data">{{ @$meta_rivals }}</span>
                  </li>
                  <li>
                  	 <span class="labeltitle"><strong>{{trans('homepage.Record Goal Scorer')}}:</strong></span>
                     <span class="data">{{ @$meta_recorded_goal_scorer }}</span>
                  </li>
                  <li>
                  	 <span class="labeltitle"><strong>{{trans('homepage.Record Signing')}}:</strong></span>
                     <span class="data">{{ @$meta_record_signing }}</span>
                  </li>
                </ul>
                
            </div>
      </div>

    <div class="visible-xs mobileouterViewRowTeam">
        <p class="">
        @if(isset($meta_club_logo))
            <p>
                <img class="club-logo fadeIn" src="{{ $meta_club_logo }}" alt="{{trans('homepage.Buy')}} {{trans('homepage.frtickets')}} {{ e($node->title) }} {{trans('homepage.Tickets')}}" />
            </p>
        @else

        @endif
        @if(isset($node->title))
            <p class="textName">{{trans('homepage.frtickets')}} {{ e($node->title) }}  {{trans('homepage.Tickets')}}</p>
        @endif
        </p>
    </div>

    <div class="site-content ">
        <div id="tabs" class="eventstabs">
          <ul>
            <li><a href="#tabs-1">{{trans('homepage.Club Bio')}}</a></li>
            @if(isset($tabs))
              @foreach($tabs as $i => $tab)
                <li><a href="#tabs-{{$i+2}}">{{$tab->title}}</a></li>
              @endforeach
            @endif
          </ul>
          <div id="tabs-1">
            @if(isset($node->content ))
            {{ Template::doShortCode($node->content ) }}
            @endif
          </div>
          @if(isset($tabs))
            @foreach($tabs as $i => $tab)
              <div id="tabs-{{$i+2}}">
                {{$tab->content}}
              </div>
            @endforeach
          @endif
        </div>
      <br><br>
      @include('footballticket::frontend.partials.ticket-listing-new')

          <div class="teamPageMobile">
      @if(!empty($articles))
          <h3>Associated Articles</h3>
          <div class="article-container">
              @foreach($articles as $key=>$article )
                  <div class="columns twelve arCont">
                      <div class="arti @if($key%2 == 0) odd @else even @endif">
                          <div class="hdr">
                              <a href="/news/{{$article->slug}}">{{$article->title}}</a>
                          </div>
                          <div class="excerpt">
                              <p>{{$article->excerpt}}</p>
                          </div>
                          <div class="foot">
                              <a href="/news/{{$article->slug}}">View Full Article</a>
                          </div>
                      </div>
                  </div>
              @endforeach
          </div>
      @endif
          </div>

    </div>
  </div>
</div>
<!---------main content------------>

@include('frontend.bm2014.moduals_global.footersliders')

@stop    
{{ Assets::jsStart() }}
<style>
    .masked {display: none; }
    .ui-widget-content
    {
      background:none;
    }
    .ui-widget-header
    {
      border: 1px solid #57CADD;
      background: #57CADD;
    }
    .ui-state-active a, .ui-state-active a:link, .ui-state-active a:visited {
      color: #636363;
      text-decoration: none;
    }
    .ui-state-active, .ui-widget-content .ui-state-active, .ui-widget-header .ui-state-active {
      border: 1px solid #818181;
    }
    .ui-tabs-anchor
    {
      font-size:18px;
    }
    #tabs-1 a{
      color: #e13d7f;
    }
    .ui-tabs-panel
    {
      font-family: 'HelveticaNeueLTStd-Lt', sans-serif !important;
      margin-top:20px !important;
    }

    table.responsive thead tr th{
        font-size:16px;
    }

    table.responsive td{
        vertical-align: middle;
    }

    table.responsive tr:last-child{
        border-bottom:1px solid #f2f2f2;
    }

    table.responsive p{
        margin:0px;
    }
</style>
<script>
    (function ($){
        var index =0;
        $('#tabs-1 > p').each(function () {
            if($.trim($(this).html()) != '&nbsp;' && index < 1) {
                $(this).addClass('para-display');
                index++;
            } else if($.trim($(this).html()) != '&nbsp;') {
                $(this).addClass('masked');
            }
        });

        if(index >= 1) {
            $('<div class="show-all-text"><a href="#" class="down-arrow">{{trans("homepage.Show more")}}</a></div><div stye="clear: both">').insertAfter('#tabs-1 p.masked:last');
        }

        $( "#tabs" ).tabs();

        $('body').on('click', '.show-all-text a.down-arrow', function (e) {
            e.preventDefault();
            $('#tabs-1 p.masked').addClass('unmasked');
            $('#tabs-1 p.unmasked').removeClass('masked');
            $(this).addClass('up-arrow');
            $(this).removeClass('down-arrow');
            $(this).html('{{trans("homepage.Hide")}}');
        });

        $('body').on('click', '.show-all-text a.up-arrow', function (e) {
            e.preventDefault();
            $('#tabs-1 p.unmasked').addClass('masked');
            $('#tabs-1 p.masked').removeClass('unmasked');
            $(this).addClass('down-arrow');
            $(this).removeClass('up-arrow');
            $(this).html("{{trans('homepage.Show more')}}");
            var firstPara = $('#tabs-1 p.para-display').first();
            $('html,body').animate({scrollTop: firstPara.offset().top -100 }, 'slow');
        });

        $('.site-content').css({display: 'block'})
    })(jQuery)

</script>
{{ Assets::jsEnd() }}
