@extends(Template::name('frontend.%s._layout.layout'))
@section('style')
{{ HTML::style('ckeditor/contents.css') }}
@stop
@section('content')

@include(Template::name('frontend.%s._layout.account-tabs'))

<!---------main content------------>
<div class="accountarea">

    <div class="site-content">

        <!---------faz shortcode------------>

        <div class="row">
            <h2> {{trans('homepage.Purchases')}}
            </h2>
        </div>

        <!---------toolbar------------>
        <div class="row toolbar">

            <span class="filter-label pull-left offset-right">{{trans('homepage.Filter By')}}</span>

            <input type="text" placeholder="{{trans('homepage.Filter by Event')}}" class="quickfilter  pull-left offset-right">

                <span class="ftp-line">
                    <span class="greenline"></span>
                    <span class="yellowline"></span>
                    <span class="pinkline"></span>
                    <span class="blueline"></span>
                </span>

        </div>
        <!---------toolbar------------>


        <!---------purchases------------>

        <div class="row overflow-mobile">
            <table class="responsivetable nocolour">

                <thead>
                <th>{{trans('homepage.Event')}}</th>
                <th>{{trans('homepage.Ticket Type')}}</th>
                <th>{{trans('homepage.accBlocknRow')}}</th>
                <th>{{trans('homepage.Qty')}}</th>
                <th>{{trans('homepage.Price')}}</th>
                <th>{{trans('homepage.accTransDate')}}</th>
                <th>{{trans('homepage.TransNumber')}}</th>
                <th>{{trans('homepage.DelStatus')}}</th>
                <th>{{trans('homepage.Delivery Address')}}</th>
                <th>Delivery Actions</th>
                </thead>
                <tbody>

                @if(isset($purchasedTickets) && count($purchasedTickets) > 0)
                @foreach($purchasedTickets as $ticket)
                <tr class="{{$ticket->event->slug}} listing" data-slug="{{$ticket->event->slug}}" data-label="{{$ticket->event->title}}">
                    <td>
                        <div class="pull-left gamedetail nopadding">

                            <span class="game">{{ $ticket->event->title}}</span>
                            <span class="game-info pull-left  clearboth">{{date('d/m/Y, G:i', strtotime($ticket->event->datetime) ) }}</span>
                        </div>

                    </td>

                    <td>{{ $ticket->ticketType }}</td>

                    <td>{{trans('homepage.Block')}}: {{ $ticket->ticket['ticketInformation']['loc_block'] }}
                        <br/>
                        {{trans('homepage.Row')}}: {{ $ticket->ticket['ticketInformation']['loc_row'] }}
                    </td>

                    <td>{{ $ticket->qty }}</td>

                    <td>{{trans('homepage.currencyInUse')}}{{ number_format($ticket->amount, 2) }} {{trans('homepage.currencyAfter')}}</td>

                    <td>{{ date('d/m/Y', strtotime($ticket->orderDate)) }}</td>

                    <td>
                        <span class="paidID">{{$ticket->order_id}}</span>
                    </td>

                    <td>
                        {{ $ticket->delivery_status }}
                    </td>
                    <td>
                        <a href="{{route('account.orders.customer.address', array('orderId'=>$ticket->order_id))}}" class="view-customer-address">{{trans('homepage.accVMDA')}}</a>
                    </td>
                    <td>
                        @if($ticket->delivery_flag)
                            <a href="#" class="getDeliveryAction" data-id="{{$ticket->order_id}}">{{$ticket->delivery_action}}</a>
                        @else
                            {{$ticket->delivery_action}}
                        @endif
                        
                    </td>
                </tr>
                @endforeach
                @else
                <tr>
                    <td colspan="10">{{trans('homepage.purTickNotFound')}}</td>
                </tr>
                @endif


                </tbody>
            </table>
        </div>

        <!---------purchases------------>


        <!---------faz shortcode------------>


    </div>
</div>
<!---------main content------------>

@stop

{{
Assets::setScripts(
[
'underscore'             => 'js/underscore.min.js'
], false, true);
}}

{{ Assets::jsStart() }}
<script>
    (function ($) {
        $('.view-customer-address').click(function (e) {
            e.preventDefault();
            $('body').append('<div id="customer-address-template" style="display: none" class="reveal-modal" data-reveal></div>');
            var self = $(this);
            var row = $(this).closest('tr.listing');
            var event_html = row.find('.gamedetail').html()+row.find('.venue-location').html();
            // url with custom callbacks
            $('#customer-address-template').foundation('reveal', 'open', {
                url: self.attr('href'),
                success: function(data) {
                   $('.ticket-information').append( event_html);
                },
                error: function() {

                }
            });
        });

        $('.getDeliveryAction').click(function(e){
            e.preventDefault();
            var id = $(this).data('id');
            var url = '/account/purchases/get-delivery-action/'+id;
            $.ajax({
                url: url,
                type: 'POST',
                success: function (response) {
                    $('body').append('<div id="customer-address-template" style="display: none" class="reveal-modal" data-reveal></div>');
                    $('#customer-address-template').html(response);
                    $('#customer-address-template').foundation('reveal', 'open');
                },
                error: function (response) {
                    alert("An error occured");
                }
            })
        });
    })(jQuery)
</script>

<script type="text/javascript" charset="utf-8">
    (function ($) {
        var body = $('body');

        /* close modal */
        body.on('click','.close-reveal-modal', function (e) {
            e.preventDefault();
            var ElemId = $(this).closest('div.reveal-modal').attr('id');
            $('#'+ElemId).foundation('reveal', 'close');
            $('#'+ElemId).fadeOut('slow', function () {
                $('#'+ElemId).remove();
                $('.reveal-modal-bg').remove();
            });
        });

        body.on('click','.reveal-modal-bg', function (e) {
            e.preventDefault();
            var ElemId = body.find('div.reveal-modal').attr('id');
            $('#'+ElemId).foundation('reveal', 'close');
            $('#'+ElemId).fadeOut('slow', function () {
                $('#'+ElemId).remove();
                $('.reveal-modal-bg').remove();
            });
        })

    })(jQuery);
</script>
{{ Assets::jsEnd() }}