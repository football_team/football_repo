@extends(Template::name('frontend.%s._layout.layout'))
@section('style')
{{ HTML::style('ckeditor/contents.css') }}
@stop
@section('content')

<!---------sidebar------------>
<section class="main">

        <div class="row">
            @include(Template::name('frontend.%s.sidebars.side1'))
        </div>
</section>
<!---------sidebar------------>



<section class="banner-home">

    <h1 class="page-header">{{ e($node->title) }}</h1>


    @if(isset($feature_image) && $feature_image != '')
    <img class="inner-banner" src="{{ $feature_image }}" alt="{{$node->title}}" />
    @else
    <img class="inner-banner" src="{{ Assets::Path('images/default.jpg') }}" alt="{{$node->title}}" />
    @endif
    
    <span class="ftp-line">
    	<span class="greenline"></span>
        <span class="yellowline"></span>
        <span class="pinkline"></span>
        <span class="blueline"></span>
    </span>	
    
    
</section>




<!---------main content------------>
<div class="row">



     <div class="site-content">





         <div class="columns six nopadding">

             @if($node instanceof Pages)
             {{ Template::doShortCode($node->content ) }}
             @endif

         </div>





         <div class="columns six contact-form">

             <form class="enquiryform" action="{{route('corporate.contact.post')}}" method="post">
                 <input type="hidden" name="_token" value="{{csrf_token()}}">
                 <div class="row">
                     <input type="text" name="name" placeholder="{{trans('homepage.YOUR NAME')}}" required/>
                 </div>

                 <div class="row">
                     <input type="text" name="company" placeholder="{{trans('homepage.COMPANY')}}" required/>
                 </div>

                 <div class="row">
                     <input type="email" name="email" placeholder="{{trans('homepage.EMAIL')}}" required/>
                 </div>


                 <div class="row">
                     <input type="text" name="tel" placeholder="{{trans('homepage.TorMob')}}" />
                 </div>


                 <div class="row">
                     <input type="text" name="number_of_tickets" placeholder="{{trans('homepage.NUMBER OF TICKETS')}}" required/>
                 </div>



                 <div class="row">
                     <input type="text" placeholder="{{trans('homepage.GAME')}}" name="game" required/>
                 </div>


                 <div class="row">
                     <input type="text" name="date_of_match" placeholder="{{trans('homepage.DATE OF MATCH')}}" required/>
                 </div>


                 <div class="row">
                     <input type="text" name="budget" placeholder="{{trans('homepage.MAXIMUM BUDGET')}}" />
                 </div>

                 <div class="row captcha-container">

                     <div class="captcha-image pull-left columns six">
                         {{ HTML::image(Captcha::img(), 'Captcha image') }}
                     </div>
                     <div class="pull-right columns six">
                         {{ Form::text('captcha', '', array('required'=>'', 'placeholder'=>trans('homepage.capEC'))) }}
                     </div>
                 </div>

                 <div class="row">
                     <input type="button" class="btn pinkbtn pull-right contact-submit" value="{{trans('homepage.SUBMIT')}}">
                 </div>


             </form>

         </div>




                            
     </div>
</div>
<!---------main content------------>

    
@stop

{{
Assets::setScripts(
[
'underscore'             => 'js/underscore.min.js',
'jquery-validator'       => 'js/jquery-validator/jquery.validate.min.js',
'jquery-form'            => 'js/jquery-form.min.js'

], false, true);
}}


{{ Assets::jsStart() }}
<script>
    $('.contact-submit').click(function (e) {
        e.preventDefault();
        var validator = $(".enquiryform").validate();
        if (validator.form()) {
            var form = $("form.enquiryform");
            var serialiseData = form.serialize();
            var action = form.attr('action');

            $.ajax({
                url: action,
                type: 'post',
                dataType: 'json',
                data: serialiseData,
                success: function (reponse) {
                    $('.contact-form').html('<p>{{trans("homepage.contactMSUB")}}</p>');
                },
                error: function (reponse) {
                    $('.captcha-container').find('#captcha-error').remove();
                    $('.captcha-container').append('<label id="captcha-error" class="error" for="captcha">{{trans("homepage.captchaError")}}</label>');
                    $('input[name=captcha]').addClass("error");
                    var obj = $.parseJSON(reponse.responseText);
                    $('.captcha-image').html(obj.result.captcha_image);

                    console.log(reponse);
                }
            })
        }
    })

</script>

{{ Assets::jsEnd() }}