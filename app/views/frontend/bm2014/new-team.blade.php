@extends(Template::name('frontend.%s._layout.layout'))
@section('style')
    {{ HTML::style('ckeditor/contents.css') }}
@stop
@section('content')
    <section class="banner-home  hidden-xs">

 	<span class="club-effect">
        @if(isset($node->feature_image ))
            <img class="inner-banner" src="{{ $node->feature_image }}" alt="test" />
        @else
            <img class="inner-banner" src="{{ Assets::Path('images/default.jpg') }}" alt="test" />
        @endif
    </span>

        <!-- FTP LINE-->
    <span class="ftp-line hidden-xs">
    	<span class="greenline hidden-xs"></span>
        <span class="yellowline hidden-xs"></span>
        <span class="pinkline hidden-xs"></span>
        <span class="blueline hidden-xs"></span>
    </span>


        <!--
            Event title
            Event date (formatted to ISO standards)
            Team a name
            Team b name
            Locational data
                - Stadium
         -->

    </section>



    <!---------main content------------>
    <div class="row mobilenEventsStireContent">
        <div class="teaminfo">
            <!-- TrustBox widget -->
            <div class="trustpilot-widget visible-xs" style="background-color: #404040; padding: 10px 0px 16px 0;" data-locale="en-GB" data-template-id="5419b732fbfb950b10de65e5" data-businessunit-id="54f5f5ad0000ff00057dce30" data-style-height="20px" data-style-width="100%" data-theme="dark">
                <a href="https://uk.trustpilot.com/review/footballticketpad.com" target="_blank">Trustpilot</a>
            </div>
            <!-- End TrustBox widget -->
            @if(isset($meta_club_logo))
                <img class="club-logo fadeIn" src="{{ $meta_club_logo }}" alt="{{trans('homepage.Buy')}} {{trans('homepage.frtickets')}} {{ e($node->title) }} {{trans('homepage.Tickets')}}" />
            @else
                <img class="club-logo fadeIn" src="" alt="{{ e($node->title) }}" />
            @endif
            <h1 class="page-header fadeIn">{{trans('homepage.frtickets')}} {{ ($node->title) }} {{trans('homepage.Tickets')}}</h1>
            <div class="team-summary">
            <span class="small-title">
                <strong>{{trans('homepage.Team Fact File')}}:</strong>
            </span>
                <ul>
                    <li>
                        <span class="labeltitle"><strong>{{trans('homepage.Nickname')}}:</strong></span>
                        <span class="data">{{ @$meta_nickname }}</span>
                    </li>
                    <li>
                        <span class="labeltitle"><strong>{{trans('homepage.Founded')}}:</strong></span>
                        <span class="data">{{ @$meta_founded }}</span>
                    </li>
                    <li>
                        <span class="labeltitle"><strong>{{trans('homepage.Rivals')}}:</strong></span>
                        <span class="data">{{ @$meta_rivals }}</span>
                    </li>
                    <li>
                        <span class="labeltitle"><strong>{{trans('homepage.Record Goal Scorer')}}:</strong></span>
                        <span class="data">{{ @$meta_recorded_goal_scorer }}</span>
                    </li>
                    <li>
                        <span class="labeltitle"><strong>{{trans('homepage.Record Signing')}}:</strong></span>
                        <span class="data">{{ @$meta_record_signing }}</span>
                    </li>
                </ul>
            </div>
        </div>

        <div class="site-content ">
            <div class="row" style="margin-bottom:30px;">
                <div class="col-md-6 main-content hidden-xs">
                    {{ Template::doShortCode($node->content ) }}
                </div>
                <div class="col-md-6 trustfactors hidden-xs">
                    <div class="row">
                        <div class="col-sm-4 tf">
                            <img style="max-width:60px;" src="https://cdn1.footballticketpad.com/frontend/bm2014/images/tickets.svg" alt=""><br><br>
                            <div class="text-desc">
                                <p class="bolder">100% Ticket Guarantee</p>
                                <p>Your tickets are guaranteed no matter what. Never miss the chance to watch the match of a lifetime.</p>
                            </div>
                        </div>
                        <div class="col-sm-4 tf">
                            <img style="max-width:60px;" src="https://cdn1.footballticketpad.com/frontend/bm2014/images/lock.svg" alt=""><br><br>
                            <div class="text-desc">
                                <p class="bolder">Secure Checkout</p>
                                <p> We use high levels of data encryption here and do not share your data with any third party vendors!</p>
                            </div>
                        </div>
                        <div class="col-sm-4 tf">
                            <img style="max-width:60px;" src="https://cdn1.footballticketpad.com/frontend/bm2014/images/star.svg" alt=""><br><br>
                            <div class="text-desc">
                                <p class="bolder">Rated 5 stars</p>
                                <p>We are proud to offer a five star buying experience and after sales care. <br><a href="#trustrow">Read our reviews</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row hidden-sm">
                <div class="col-md-6">
                    <h2>{{$node->title}} <b>Home</b> tickets</h2>
                    @include('footballticket::frontend.partials.ticket-listing-home')
                </div>
                <div class="col-md-6">
                    <h2>{{$node->title}} <b>Away</b> tickets</h2>
                    @include('footballticket::frontend.partials.ticket-listing-away')
                </div>
            </div>
            <div class="row visible-sm" style="margin:0px;">
                <div class="col-xs-12" style="padding:0px;">
                    <h2 class="hidden-xs" style="padding:10px;">{{$node->title}} tickets</h2>
                    @include('footballticket::frontend.partials.ticket-listing-new')
                </div>
            </div>
            <div class="row trustrow hidden-xs" id="trustrow">
                <!-- TrustBox widget -->
                <div class="trustpilot-widget" data-locale="en-GB" data-template-id="53aa8912dec7e10d38f59f36" data-businessunit-id="54f5f5ad0000ff00057dce30" data-style-height="130px" data-style-width="100%" data-theme="light" data-stars="5">
                    <a href="https://uk.trustpilot.com/review/footballticketpad.com" target="_blank">Trustpilot</a>
                </div>
                <!-- End TrustBox widget -->
            </div>
            <h2 class="statttl visible-sm">Stadium & Ticket Information</h2>
            <div class="row tab-row">
                @if(isset($new_tabs))
                    @foreach($new_tabs as $tab)
                        <div class="tab">
                            <div class="hdr">
                                @if(isset($tab->icon_image) && $tab->icon_image != '')
                                    <img class="tab-img" src="{{$tab->icon_image}}" style="max-width:60px;" alt="">
                                @endif
                                <h2>{{$tab->title}}</h2>
                                <img src="https://cdn1.footballticketpad.com/frontend/bm2014/images/chevron.png" class="expnd" alt="">
                            </div>
                            <div class="bdy">{{$tab->content}}</div>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
    </div>
    <!---------main content------------>
@stop
{{ Assets::jsStart() }}
<style>
    html{font-family:sans-serif;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%}body{margin:0}article,aside,details,figcaption,figure,footer,header,hgroup,main,menu,nav,section,summary{display:block}audio,canvas,progress,video{display:inline-block;vertical-align:baseline}audio:not([controls]){display:none;height:0}[hidden],template{display:none}a{background-color:transparent}a:active,a:hover{outline:0}abbr[title]{border-bottom:1px dotted}b,strong{font-weight:bold}dfn{font-style:italic}h1{font-size:2em;margin:0.67em 0}mark{background:#ff0;color:#000}small{font-size:80%}sub,sup{font-size:75%;line-height:0;position:relative;vertical-align:baseline}sup{top:-0.5em}sub{bottom:-0.25em}img{border:0}svg:not(:root){overflow:hidden}figure{margin:1em 40px}hr{-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box;height:0}pre{overflow:auto}code,kbd,pre,samp{font-family:monospace, monospace;font-size:1em}button,input,optgroup,select,textarea{color:inherit;font:inherit;margin:0}button{overflow:visible}button,select{text-transform:none}button,html input[type="button"],input[type="reset"],input[type="submit"]{-webkit-appearance:button;cursor:pointer}button[disabled],html input[disabled]{cursor:default}button::-moz-focus-inner,input::-moz-focus-inner{border:0;padding:0}input{line-height:normal}input[type="checkbox"],input[type="radio"]{-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;padding:0}input[type="number"]::-webkit-inner-spin-button,input[type="number"]::-webkit-outer-spin-button{height:auto}input[type="search"]{-webkit-appearance:textfield;-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box}input[type="search"]::-webkit-search-cancel-button,input[type="search"]::-webkit-search-decoration{-webkit-appearance:none}fieldset{border:1px solid #c0c0c0;margin:0 2px;padding:0.35em 0.625em 0.75em}legend{border:0;padding:0}textarea{overflow:auto}optgroup{font-weight:bold}table{border-collapse:collapse;border-spacing:0}td,th{padding:0}*{-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box}*:before,*:after{-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box}html{font-size:10px;-webkit-tap-highlight-color:rgba(0,0,0,0)}body{font-family:"Helvetica Neue",Helvetica,Arial,sans-serif;font-size:14px;line-height:1.42857143;color:#333;background-color:#fff}input,button,select,textarea{font-family:inherit;font-size:inherit;line-height:inherit}a{color:#337ab7;text-decoration:none}a:hover,a:focus{color:#23527c;text-decoration:underline}a:focus{outline:5px auto -webkit-focus-ring-color;outline-offset:-2px}figure{margin:0}img{vertical-align:middle}.img-responsive{display:block;max-width:100%;height:auto}.img-rounded{border-radius:6px}.img-thumbnail{padding:4px;line-height:1.42857143;background-color:#fff;border:1px solid #ddd;border-radius:4px;-webkit-transition:all .2s ease-in-out;-o-transition:all .2s ease-in-out;transition:all .2s ease-in-out;display:inline-block;max-width:100%;height:auto}.img-circle{border-radius:50%}hr{margin-top:20px;margin-bottom:20px;border:0;border-top:1px solid #eee}.sr-only{position:absolute;width:1px;height:1px;margin:-1px;padding:0;overflow:hidden;clip:rect(0, 0, 0, 0);border:0}.sr-only-focusable:active,.sr-only-focusable:focus{position:static;width:auto;height:auto;margin:0;overflow:visible;clip:auto}[role="button"]{cursor:pointer}.container{margin-right:auto;margin-left:auto;padding-left:15px;padding-right:15px}@media (min-width:768px){.container{width:750px}}@media (min-width:992px){.container{width:970px}}@media (min-width:1200px){.container{width:1170px}}.container-fluid{margin-right:auto;margin-left:auto;padding-left:15px;padding-right:15px}.row{margin-left:-15px;margin-right:-15px}.col-xs-1, .col-sm-1, .col-md-1, .col-lg-1, .col-xs-2, .col-sm-2, .col-md-2, .col-lg-2, .col-xs-3, .col-sm-3, .col-md-3, .col-lg-3, .col-xs-4, .col-sm-4, .col-md-4, .col-lg-4, .col-xs-5, .col-sm-5, .col-md-5, .col-lg-5, .col-xs-6, .col-sm-6, .col-md-6, .col-lg-6, .col-xs-7, .col-sm-7, .col-md-7, .col-lg-7, .col-xs-8, .col-sm-8, .col-md-8, .col-lg-8, .col-xs-9, .col-sm-9, .col-md-9, .col-lg-9, .col-xs-10, .col-sm-10, .col-md-10, .col-lg-10, .col-xs-11, .col-sm-11, .col-md-11, .col-lg-11, .col-xs-12, .col-sm-12, .col-md-12, .col-lg-12{position:relative;min-height:1px;padding-left:15px;padding-right:15px}.col-xs-1, .col-xs-2, .col-xs-3, .col-xs-4, .col-xs-5, .col-xs-6, .col-xs-7, .col-xs-8, .col-xs-9, .col-xs-10, .col-xs-11, .col-xs-12{float:left}.col-xs-12{width:100%}.col-xs-11{width:91.66666667%}.col-xs-10{width:83.33333333%}.col-xs-9{width:75%}.col-xs-8{width:66.66666667%}.col-xs-7{width:58.33333333%}.col-xs-6{width:50%}.col-xs-5{width:41.66666667%}.col-xs-4{width:33.33333333%}.col-xs-3{width:25%}.col-xs-2{width:16.66666667%}.col-xs-1{width:8.33333333%}.col-xs-pull-12{right:100%}.col-xs-pull-11{right:91.66666667%}.col-xs-pull-10{right:83.33333333%}.col-xs-pull-9{right:75%}.col-xs-pull-8{right:66.66666667%}.col-xs-pull-7{right:58.33333333%}.col-xs-pull-6{right:50%}.col-xs-pull-5{right:41.66666667%}.col-xs-pull-4{right:33.33333333%}.col-xs-pull-3{right:25%}.col-xs-pull-2{right:16.66666667%}.col-xs-pull-1{right:8.33333333%}.col-xs-pull-0{right:auto}.col-xs-push-12{left:100%}.col-xs-push-11{left:91.66666667%}.col-xs-push-10{left:83.33333333%}.col-xs-push-9{left:75%}.col-xs-push-8{left:66.66666667%}.col-xs-push-7{left:58.33333333%}.col-xs-push-6{left:50%}.col-xs-push-5{left:41.66666667%}.col-xs-push-4{left:33.33333333%}.col-xs-push-3{left:25%}.col-xs-push-2{left:16.66666667%}.col-xs-push-1{left:8.33333333%}.col-xs-push-0{left:auto}.col-xs-offset-12{margin-left:100%}.col-xs-offset-11{margin-left:91.66666667%}.col-xs-offset-10{margin-left:83.33333333%}.col-xs-offset-9{margin-left:75%}.col-xs-offset-8{margin-left:66.66666667%}.col-xs-offset-7{margin-left:58.33333333%}.col-xs-offset-6{margin-left:50%}.col-xs-offset-5{margin-left:41.66666667%}.col-xs-offset-4{margin-left:33.33333333%}.col-xs-offset-3{margin-left:25%}.col-xs-offset-2{margin-left:16.66666667%}.col-xs-offset-1{margin-left:8.33333333%}.col-xs-offset-0{margin-left:0}@media (min-width:768px){.col-sm-1, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-sm-10, .col-sm-11, .col-sm-12{float:left}.col-sm-12{width:100%}.col-sm-11{width:91.66666667%}.col-sm-10{width:83.33333333%}.col-sm-9{width:75%}.col-sm-8{width:66.66666667%}.col-sm-7{width:58.33333333%}.col-sm-6{width:50%}.col-sm-5{width:41.66666667%}.col-sm-4{width:33.33333333%}.col-sm-3{width:25%}.col-sm-2{width:16.66666667%}.col-sm-1{width:8.33333333%}.col-sm-pull-12{right:100%}.col-sm-pull-11{right:91.66666667%}.col-sm-pull-10{right:83.33333333%}.col-sm-pull-9{right:75%}.col-sm-pull-8{right:66.66666667%}.col-sm-pull-7{right:58.33333333%}.col-sm-pull-6{right:50%}.col-sm-pull-5{right:41.66666667%}.col-sm-pull-4{right:33.33333333%}.col-sm-pull-3{right:25%}.col-sm-pull-2{right:16.66666667%}.col-sm-pull-1{right:8.33333333%}.col-sm-pull-0{right:auto}.col-sm-push-12{left:100%}.col-sm-push-11{left:91.66666667%}.col-sm-push-10{left:83.33333333%}.col-sm-push-9{left:75%}.col-sm-push-8{left:66.66666667%}.col-sm-push-7{left:58.33333333%}.col-sm-push-6{left:50%}.col-sm-push-5{left:41.66666667%}.col-sm-push-4{left:33.33333333%}.col-sm-push-3{left:25%}.col-sm-push-2{left:16.66666667%}.col-sm-push-1{left:8.33333333%}.col-sm-push-0{left:auto}.col-sm-offset-12{margin-left:100%}.col-sm-offset-11{margin-left:91.66666667%}.col-sm-offset-10{margin-left:83.33333333%}.col-sm-offset-9{margin-left:75%}.col-sm-offset-8{margin-left:66.66666667%}.col-sm-offset-7{margin-left:58.33333333%}.col-sm-offset-6{margin-left:50%}.col-sm-offset-5{margin-left:41.66666667%}.col-sm-offset-4{margin-left:33.33333333%}.col-sm-offset-3{margin-left:25%}.col-sm-offset-2{margin-left:16.66666667%}.col-sm-offset-1{margin-left:8.33333333%}.col-sm-offset-0{margin-left:0}}@media (min-width:992px){.col-md-1, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9, .col-md-10, .col-md-11, .col-md-12{float:left}.col-md-12{width:100%}.col-md-11{width:91.66666667%}.col-md-10{width:83.33333333%}.col-md-9{width:75%}.col-md-8{width:66.66666667%}.col-md-7{width:58.33333333%}.col-md-6{width:50%}.col-md-5{width:41.66666667%}.col-md-4{width:33.33333333%}.col-md-3{width:25%}.col-md-2{width:16.66666667%}.col-md-1{width:8.33333333%}.col-md-pull-12{right:100%}.col-md-pull-11{right:91.66666667%}.col-md-pull-10{right:83.33333333%}.col-md-pull-9{right:75%}.col-md-pull-8{right:66.66666667%}.col-md-pull-7{right:58.33333333%}.col-md-pull-6{right:50%}.col-md-pull-5{right:41.66666667%}.col-md-pull-4{right:33.33333333%}.col-md-pull-3{right:25%}.col-md-pull-2{right:16.66666667%}.col-md-pull-1{right:8.33333333%}.col-md-pull-0{right:auto}.col-md-push-12{left:100%}.col-md-push-11{left:91.66666667%}.col-md-push-10{left:83.33333333%}.col-md-push-9{left:75%}.col-md-push-8{left:66.66666667%}.col-md-push-7{left:58.33333333%}.col-md-push-6{left:50%}.col-md-push-5{left:41.66666667%}.col-md-push-4{left:33.33333333%}.col-md-push-3{left:25%}.col-md-push-2{left:16.66666667%}.col-md-push-1{left:8.33333333%}.col-md-push-0{left:auto}.col-md-offset-12{margin-left:100%}.col-md-offset-11{margin-left:91.66666667%}.col-md-offset-10{margin-left:83.33333333%}.col-md-offset-9{margin-left:75%}.col-md-offset-8{margin-left:66.66666667%}.col-md-offset-7{margin-left:58.33333333%}.col-md-offset-6{margin-left:50%}.col-md-offset-5{margin-left:41.66666667%}.col-md-offset-4{margin-left:33.33333333%}.col-md-offset-3{margin-left:25%}.col-md-offset-2{margin-left:16.66666667%}.col-md-offset-1{margin-left:8.33333333%}.col-md-offset-0{margin-left:0}}@media (min-width:1200px){.col-lg-1, .col-lg-2, .col-lg-3, .col-lg-4, .col-lg-5, .col-lg-6, .col-lg-7, .col-lg-8, .col-lg-9, .col-lg-10, .col-lg-11, .col-lg-12{float:left}.col-lg-12{width:100%}.col-lg-11{width:91.66666667%}.col-lg-10{width:83.33333333%}.col-lg-9{width:75%}.col-lg-8{width:66.66666667%}.col-lg-7{width:58.33333333%}.col-lg-6{width:50%}.col-lg-5{width:41.66666667%}.col-lg-4{width:33.33333333%}.col-lg-3{width:25%}.col-lg-2{width:16.66666667%}.col-lg-1{width:8.33333333%}.col-lg-pull-12{right:100%}.col-lg-pull-11{right:91.66666667%}.col-lg-pull-10{right:83.33333333%}.col-lg-pull-9{right:75%}.col-lg-pull-8{right:66.66666667%}.col-lg-pull-7{right:58.33333333%}.col-lg-pull-6{right:50%}.col-lg-pull-5{right:41.66666667%}.col-lg-pull-4{right:33.33333333%}.col-lg-pull-3{right:25%}.col-lg-pull-2{right:16.66666667%}.col-lg-pull-1{right:8.33333333%}.col-lg-pull-0{right:auto}.col-lg-push-12{left:100%}.col-lg-push-11{left:91.66666667%}.col-lg-push-10{left:83.33333333%}.col-lg-push-9{left:75%}.col-lg-push-8{left:66.66666667%}.col-lg-push-7{left:58.33333333%}.col-lg-push-6{left:50%}.col-lg-push-5{left:41.66666667%}.col-lg-push-4{left:33.33333333%}.col-lg-push-3{left:25%}.col-lg-push-2{left:16.66666667%}.col-lg-push-1{left:8.33333333%}.col-lg-push-0{left:auto}.col-lg-offset-12{margin-left:100%}.col-lg-offset-11{margin-left:91.66666667%}.col-lg-offset-10{margin-left:83.33333333%}.col-lg-offset-9{margin-left:75%}.col-lg-offset-8{margin-left:66.66666667%}.col-lg-offset-7{margin-left:58.33333333%}.col-lg-offset-6{margin-left:50%}.col-lg-offset-5{margin-left:41.66666667%}.col-lg-offset-4{margin-left:33.33333333%}.col-lg-offset-3{margin-left:25%}.col-lg-offset-2{margin-left:16.66666667%}.col-lg-offset-1{margin-left:8.33333333%}.col-lg-offset-0{margin-left:0}}.clearfix:before,.clearfix:after,.container:before,.container:after,.container-fluid:before,.container-fluid:after,.row:before,.row:after{content:" ";display:table}.clearfix:after,.container:after,.container-fluid:after,.row:after{clear:both}.center-block{display:block;margin-left:auto;margin-right:auto}.pull-right{float:right !important}.pull-left{float:left !important}.hide{display:none !important}.show{display:block !important}.invisible{visibility:hidden}.text-hide{font:0/0 a;color:transparent;text-shadow:none;background-color:transparent;border:0}.hidden{display:none !important}.affix{position:fixed}@-ms-viewport{width:device-width}.visible-xs,.visible-sm,.visible-md,.visible-lg{display:none !important}.visible-xs-block,.visible-xs-inline,.visible-xs-inline-block,.visible-sm-block,.visible-sm-inline,.visible-sm-inline-block,.visible-md-block,.visible-md-inline,.visible-md-inline-block,.visible-lg-block,.visible-lg-inline,.visible-lg-inline-block{display:none !important}@media (max-width:767px){.visible-xs{display:block !important}table.visible-xs{display:table !important}tr.visible-xs{display:table-row !important}th.visible-xs,td.visible-xs{display:table-cell !important}}@media (max-width:767px){.visible-xs-block{display:block !important}}@media (max-width:767px){.visible-xs-inline{display:inline !important}}@media (max-width:767px){.visible-xs-inline-block{display:inline-block !important}}@media (min-width:768px) and (max-width:991px){.visible-sm{display:block !important}table.visible-sm{display:table !important}tr.visible-sm{display:table-row !important}th.visible-sm,td.visible-sm{display:table-cell !important}}@media (min-width:768px) and (max-width:991px){.visible-sm-block{display:block !important}}@media (min-width:768px) and (max-width:991px){.visible-sm-inline{display:inline !important}}@media (min-width:768px) and (max-width:991px){.visible-sm-inline-block{display:inline-block !important}}@media (min-width:992px) and (max-width:1199px){.visible-md{display:block !important}table.visible-md{display:table !important}tr.visible-md{display:table-row !important}th.visible-md,td.visible-md{display:table-cell !important}}@media (min-width:992px) and (max-width:1199px){.visible-md-block{display:block !important}}@media (min-width:992px) and (max-width:1199px){.visible-md-inline{display:inline !important}}@media (min-width:992px) and (max-width:1199px){.visible-md-inline-block{display:inline-block !important}}@media (min-width:1200px){.visible-lg{display:block !important}table.visible-lg{display:table !important}tr.visible-lg{display:table-row !important}th.visible-lg,td.visible-lg{display:table-cell !important}}@media (min-width:1200px){.visible-lg-block{display:block !important}}@media (min-width:1200px){.visible-lg-inline{display:inline !important}}@media (min-width:1200px){.visible-lg-inline-block{display:inline-block !important}}@media (max-width:767px){.hidden-xs{display:none !important}}@media (min-width:768px) and (max-width:991px){.hidden-sm{display:none !important}}@media (min-width:992px) and (max-width:1199px){.hidden-md{display:none !important}}@media (min-width:1200px){.hidden-lg{display:none !important}}.visible-print{display:none !important}@media print{.visible-print{display:block !important}table.visible-print{display:table !important}tr.visible-print{display:table-row !important}th.visible-print,td.visible-print{display:table-cell !important}}.visible-print-block{display:none !important}@media print{.visible-print-block{display:block !important}}.visible-print-inline{display:none !important}@media print{.visible-print-inline{display:inline !important}}.visible-print-inline-block{display:none !important}@media print{.visible-print-inline-block{display:inline-block !important}}@media print{.hidden-print{display:none !important}}
    .masked {display: none; }
    .ui-widget-content
    {
        background:none;
    }
    .ui-widget-header
    {
        border: 1px solid #57CADD;
        background: #57CADD;
    }
    .ui-state-active a, .ui-state-active a:link, .ui-state-active a:visited {
        color: #636363;
        text-decoration: none;
    }
    .ui-state-active, .ui-widget-content .ui-state-active, .ui-widget-header .ui-state-active {
        border: 1px solid #818181;
    }
    .ui-tabs-anchor
    {
        font-size:18px;
    }
    #tabs-1 a{
        color: #e13d7f;
    }
    .ui-tabs-panel
    {
        font-family: 'HelveticaNeueLTStd-Lt', sans-serif !important;
        margin-top:20px !important;
    }

    .site-content{
        width:100%;
    }

    .mobilenEventsStireContent{
        margin-left: auto !important;
        margin-right: auto !important;
    }

    .teaminfo{
        left:0px !important;
        width:100%;
        display:inline !important;
    }

    .club-effect{
        height: 320px !important;
        overflow: hidden !important;
    }

    .team-summary{
        margin-top:10px !important;
    }

    .club-logo{
        margin-top:30px !important;
    }

    .teaminfo .page-header{
        margin-top: 70px;
    }

    .row .six{
        float:left;
    }

    .row .four{
        float:left;
    }

    .row .row{
        margin:0 15px;
    }

    .trustfactors{
        margin-top:20px;
        background-color: #f2f2f2;
        padding: 5px;
        border: 1px solid #e6e6e6;
    }

    .trustfactors>.row{
        text-align:center;
    }

    .trustfactors .tf{
        margin:20px 0;
        padding:0px;
        display:inline-block;
        min-height:100px;
        text-align:center;
    }

    .trustfactors img{
        height:60px;
        max-width:60px;
    }

    .trustfactors .text-desc{
        text-align:center;
        font-size:12px;
        padding-left:10px;
    }

    .trustfactors .text-desc p{
        margin:0px;
        font-size:12px;
    }

    .trustfactors .text-desc p.bolder{
        font-size:14px;
        font-weight:bold;

    }

    .row {
        margin-left: 0px;
        margin-right: 0px;
        width: 100%;
    }

    p{
        font-weight:initial;
    }

    .main-content{

    }

    .trustrow{
        margin:60px 0 50px 0 !important;
        padding:50px;
        background-color:#f2f2f2;
    }

    .tab-row{

    }
    .tab-row .tab{
        width:50%;

    }

    .tab-row .tab .hdr .tab-img{
        height:30px;
        max-width:40px;
        margin-top: -8px;
        margin-right: 4px;
    }

    .tab .hdr{
        font-size:16px;
        padding-bottom: 10px;
        padding:20px;
    }

    .tab .hdr .expnd{
        display:none;
    }

    .tab .hdr h2{
        display:inline;
        border:none!important;
        font-size:24px;
    }

    .tab .bdy{
        padding:20px;
    }

    .tab .bdy img{
        display:block;
        margin-left:auto;
        margin-right:auto;
    }

    .pinkbtn{
        background-color:#5e934f;
        width:100%;
        max-width:100px;
    }

    .pinkbtn:hover {
        background: #7ab969;
        color: #FFF;
    }

    .click-row{
        cursor:pointer;
    }

    .tt{
        color: #e13d7f;
        font-size: 14px;
    }

    table.responsive thead tr th{
        font-size:16px;
    }
    
    table.responsive td{
        vertical-align: middle;
    }

    table.responsive tr:last-child{
        border-bottom:1px solid #f2f2f2;
    }

    table.responsive p{
        margin:0px;
    }

    .statttl {
        text-align: center;
        font-size: 24px;
        margin: 70px 0 !important;
    }

    p.pinkbox::before{
        content:'';
        border-left:5px solid;
        margin-right:10px;
        margin-left:-10px;

        border-color:#e13d7f;
    }

    p.yellowbox::before{
        content:'';
        border-left:5px solid;
        margin-right:10px;
        margin-left:-10px;
        border-color:#fed265;
    }

    p.bluebox::before{
        content:'';
        border-left:5px solid;
        margin-right:10px;
        margin-left:-10px;
        border-color:#57cadd;
    }

    p.greenbox::before{
        content:'';
        border-left:5px solid;
        margin-right:10px;
        margin-left:-10px;
        border-color:#80c36f;
    }

    p.orangebox::before{
        content:'';
        border-left:5px solid #f38d2f;
        margin-right:10px;
        margin-left:-10px;
    }

    p.redbox::before{
        content:'';
        border-left:5px solid #e74c3c;
        margin-right:10px;
        margin-left:-10px;
    }




    @media only screen and (max-width: 1239px) and (min-width: 1001px)
    {
        .team-summary {
            clear: none !important;
            /* color: #fff; */
            display: inline-block ;
            float: right;
            margin-top: 0;
            width: 300px;
            top: 0;
        }

        .club-logo {
            margin-left: 10px;
            margin-top: 10px;
            width: 110px;
        }

        .teaminfo .page-header {
            font-size: 26px;
        }
    }


    @media only screen and (max-width: 1000px) and (min-width: 768px){
        .teaminfo .page-header {
            max-width: 300px !important;
            min-width: initial !important;
            padding-left:30px !important;
        }

        .club-logo{
            width: 110px!important;
        }
    }

    @media(max-width:991px){
        footer{
            margin-top:0px !important;
        }
        table.responsive{
            margin-bottom:0px;
        }

        .sidebar-mobile{
            display:none !important;
        }
        .tab-row .tab{
            width:100%;
        }

        .tab-row{
            margin:0px !important;
        }

        .tab{

        }

        .tab .hdr{
            margin-bottom: 0;

        }

        .tab-row .tab .hdr .tab-img{

        }

        .tab:nth-child(odd){
            background-color:#f2f2f2;
        }


        .tab .bdy{
            display:none;
        }

        .tab.active .bdy{
            display:block;
        }

        .tab.active .hdr{
            margin-bottom: 15px;
            border-bottom: 1px solid #d2d2d2;
        }


        .tab .hdr .expnd{
            float: right;
            display: inline-block !important;
            width: 29px;
            cursor: pointer;
            padding: 7px;
        }

        .tab.active .hdr .expnd{
            -webkit-transform: rotate(180deg);     /* Chrome and other webkit browsers */
            -moz-transform: rotate(180deg);        /* FF */
            -o-transform: rotate(180deg);          /* Opera */
            -ms-transform: rotate(180deg);         /* IE9 */
            transform: rotate(180deg);             /* W3C compliant browsers */

            /* IE8 and below */
            filter: progid:DXImageTransform.Microsoft.Matrix(M11=-1, M12=0, M21=0, M22=-1, DX=0, DY=0, SizingMethod='auto expand');
        }
    }

    @media (max-width: 767px){
        .sidebar-mobile{
            display:none !important;
        }
        .hidden-sm{
            display:none;
        }
        .visible-sm{
            display:block !important;
        }
        .club-logo {
            float: none;
            margin: auto!important;
            width: 80px;
            margin-top:30px !important;
        }

        .teaminfo{
            margin-top:-25px;
            margin-bottom:0px;
        }

        .teaminfo .page-header{
            display: inline-block;
            position: relative;
            min-width: 200px !important;
            font-size: 25px !important;
            margin-top: 15px !important;
            left: 0%;
        }

        .team-summary{
            display:none !important;
        }

        .banner-home {
            display:none;
        }

        header{
            padding:0px !important;
        }

        .ticketsearch{
            display:none;
        }

        .mobileClickZone svg{
            display:none;
        }

        .site-content{
            margin-top:0px;
        }

        table.responsive thead tr th{
            padding:4px;
            background-color:#404040 !important;
            color:#fff;
            font-size:16px;
            padding-top:16px;
            padding-bottom:16px;
        }

        .mobilenEventsStireContent{
            width:100% !important;
        }
    }

</style>
<script type="text/javascript" src="//widget.trustpilot.com/bootstrap/v5/tp.widget.sync.bootstrap.min.js" async></script>
<script src="https://unpkg.com/masonry-layout@4.1/dist/masonry.pkgd.min.js"></script>
<script>
    (function ($){
        $('.tab-row').masonry({
            // options
            itemSelector: '.tab',
            columnWidth: '.tab'
        });

        $('.sidebar-mobile').remove();

        $('.click-row').click(function(){
            var url = $(this).data('href');
            window.document.location = url;
        });


        $('.tab .hdr').click(function(){
            $(this).closest('.tab').toggleClass('active');
            $('.tab-row').masonry({
                // options
                itemSelector: '.tab',
                columnWidth: '.tab'
            });
        })
    })(jQuery);


</script>

<script type="application/ld+json">
    [
    @foreach($rich_snippets as $snippet)
        {
            "@context": "http://schema.org",
            "@type" : "Event",
            "name" : "{{$snippet['event_title']}}",
            "url" : "{{Request::root()}}{{$snippet['url']}}",
            "location": {
                "@type" : "Place",
                "name" : "{{$snippet['stadium_name']}}",
                "address" : "{{$snippet['address']}}"
            },
            "startDate": "{{$snippet['event_date']}}"
        }@if(!($snippet == end($rich_snippets))),@endif
    @endforeach
    ]
</script>
{{ Assets::jsEnd() }}
