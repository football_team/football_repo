@extends(Template::name('frontend.%s._layout.layout'))
@section('style')
	{{ HTML::style('ckeditor/contents.css') }}
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-T8Gy5hrqNKT+hzMclPo118YTQO6cYprQmhrYwIiQ/3axmI1hQomh7Ud2hPOy8SP1" crossorigin="anonymous">
	<style >
		div#stadiumpicture svg {
			width: 100% !important;
		}

		.para-display {
			display: block !important;
		}

		.masked {
			display: none !important;
		}

		.unmasked {
			display: block !important;
		}

		@media (max-width: 600px) {
			.hidden-xs {
				display: none !important;
			}

			.sidebar-mobile {
				display: none !important;

			}

			svg#Layer_1 {
				width: 100% !important;
			}

			body.buy .row {
				width: 100% !important;
			}

			.mobile-version {

				display: none !important;
			}

			.desktop-version {

				display: block !important;

				overflow: scroll !important;
				height: 400px !important;
			}

		}

		@media (min-width: 601px) {
			.hidden-md {
				display: none !important;
			}

		}

	</style >
	<link rel="stylesheet" type="text/css" href="/map/style.css">

@stop

@section('script')
	<script>
		$('document').ready(function(){
			$(window).on("scroll", function (e) {
				var sb = $('.sidebar-mobile');
				if ($(sb[0]).css("display") == "none") {
					sb[0] = $('footer');
				}
				if (($(window).width() > 767) && ($('#stadiumpicture').outerHeight() < $('#outer-container').outerHeight())) {
					var topOff = ($('#description-team').offset().top + $('#description-team').outerHeight()) - $(window).scrollTop() <= 0;
					var sidepos = parseFloat(($(sb[0]).offset().top) - 50 - ($('.stadiumpicture').outerHeight()));
					if (topOff && $(window).scrollTop() < sidepos) {
						$('.stadiumpicture').css('position', 'fixed');
						$('.stadiumpicture').css('top', '0');
						$('header').css('display', 'none');
					} else if (topOff && $(window).scrollTop() >= sidepos) {
						$('.stadiumpicture').css('position', 'fixed');
						$('.stadiumpicture').css('top', '' + ((sidepos - $(window).scrollTop())) + 'px');
						$('header').css('display', 'none');
					} else if ($(window).scrollTop() < sidepos) {
						$('.stadiumpicture').css('position', 'static');
						$('.stadiumpicture').css('top', '');
						$('header').css('display', 'block');
					}
				} else {
					$('#stadiumpicture').css('position', 'static');
					$('header').css('display', 'block');
				}
			});
		})
	</script>
@endsection

@section('content')
	<div class="mobileFilterTab" style="
    background: white;
    height: 100vh;
    width: 300px;
    position: fixed;
    top: 0;
    right: 0;
    z-index: 100000000;
    border-left: 3px solid #E3296D;
" >
		<h2>{{trans('homepage.TicketOptions')}}</h2>
		<div class="ticketCounter" >
			<p>{{trans('homepage.WWYLTS')}}</p>
			<div class="seatzones" >
			</div >
			<div class="closefilter" >
				<button class="closeFilter_button active">
					{{trans('homepage.Search')}}
				</button >
			</div >
		</div >
	</div >

	<section class="banner-home" @if($node->feature_image != '')style="background-image:url({{$node->feature_image}});" @else style="background-image:url({{ Assets::Path('images/default.jpg') }});" @endif>
    	<span class="page-header buy-detail">
			 <h1 class="gamename" style="margin-bottom:0px;" >
				 <strong ><a href='/group/club/{{$homeTeam->slug}}' style="color:#fff;" >{{trans('homepage.frticbefore')}}
						 @if(trans('homepage.'.$node->homeTeam) != "" && (trans('homepage.'.$node->homeTeam)!='homepage.'.$node->homeTeam))
							 {{trans('homepage.'.$node->homeTeam)}}
						 @else
							 {{$homeTeam->title}}
						 @endif
					 </a ></strong >
				 vs
				 <strong ><a href='/group/club/{{$awayTeam->slug}}' style="color:#fff;" >
						 @if(trans('homepage.'.$node->awayTeam) != "" && (trans('homepage.'.$node->awayTeam)!='homepage.'.$node->awayTeam))
							 {{trans('homepage.'.$node->awayTeam)}}
						 @else
							 {{$awayTeam->title}}
						 @endif
					 </a ></strong > {{trans('homepage.Tickets')}}
			 </h1 >
			 <span class="gamedetail" style="margin-top:0px;" ><b ><a href="/group/league/{{$lSlug}}" style="color:#fff;" >{{$TournamentTitle}}</a ></b ></span >
			@if(!empty($stadium))
				<span class="gamedetail">@if(!$node->is_tbc){{date('g:ia d-m-y', strtotime($node->datetime))}}@else TBC @endif</span>
				<div class="gamedetail">
						@if(!empty($stadium->title))
						{{$stadium->title}},
					@endif
					@if(!empty($stadium->al1)){{$stadium->al1}}, @endif
					@if(!empty($stadium->al2)){{$stadium->al2}}, @endif
					@if(!empty($stadium->postcode)){{$stadium->postcode}}@endif
					</div >
			@else
				<span class="gamedetail">@if(!$node->is_tbc){{date('g:ia d-m-y', strtotime($node->datetime))}}@else TBC @endif
					. {{$node->event_location}}</span>
			@endif
			<a href="/ticket/sell/{{$node->id}}" class="btn bluebtn">{{trans('homepage.SELL TICKETS')}}</a>
		</span >
		<span class="club-effect  hidden-xs" >
			@if($node->feature_image != '')
				<img class="inner-banner" src="{{$node->feature_image}}" alt="banner image" />
			@else
				<img class="inner-banner" src="{{ Assets::Path('images/default.jpg') }}" alt="banner image" />
			@endif
		</span >
	</section >
	<span class="ftp-line  hidden-xs " >
			<span class="greenline" ></span >
			<span class="yellowline" ></span >
			<span class="pinkline" ></span >
			<span class="blueline" ></span >
		</span >
	<div class="row" >
		<div class="site-content" >
			<div class="description-team hidden-xs" id="description-team" >
				<div class="description" >
					<h2 > {{trans('homepage.buyDesc1')}}
						<a href="/group/club/{{$homeTeam->slug}}" style='color:#E60073;' >@if(trans('homepage.'.$node->homeTeam) != "" && (trans('homepage.'.$node->homeTeam)!='homepage.'.$node->homeTeam))
								{{trans('homepage.'.$node->homeTeam)}}
							@else
								{{$homeTeam->title}}
							@endif</a > v
						<a href="/group/club/{{$awayTeam->slug}}" style='color:#E60073;' > @if(trans('homepage.'.$node->awayTeam) != "" && (trans('homepage.'.$node->awayTeam)!='homepage.'.$node->awayTeam))
								{{trans('homepage.'.$node->awayTeam)}}
							@else
								{{$awayTeam->title}}
							@endif</a > {{trans('homepage.buyDesc2')}}
						<a href="/group/league/{{$lSlug}}" style='color:#E60073;' >{{$TournamentTitle}}</a >.</h2 >
					{{$node->content}}
				</div >
				<div class="sharebox" >
					<!-- Go to www.addthis.com/dashboard to customize your tools -->
					<div class="addthis_sharing_toolbox" ></div >
					<!-- Go to www.addthis.com/dashboard to customize your tools -->
					<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-4f426a07524dc6b4" ></script >
				</div >
				<div class="tel-no" style="display:block !important;" ><span class="telpink" >?:</span >
					<a href="/faq" >{{trans('homepage.FAQ')}}</a ></div >
				<div class="tel-no" style="display:block !important;" ><span class="telpink" >t:</span >
					<a href="tel:{{trans('homepage.tnumber')}}" >{{trans('homepage.tnumber')}}</a ></div >
				<div class="tel-no" style="display:block !important;" ><span class="telpink" >e:</span >
					<a href="mailto:info@footballticketpad.com" >{{trans('homepage.contact-email')}}</a ></div >
				<br >
				<br >
			</div >
			<div id="outer-container" >
				<div class="row" >
					<span class="callMobile hidden-md" style="background: #63C9DA;padding: 10px;border-radius: 100px;position: fixed;top: 10px;right: 10px;z-index: 1000000;">
						<a href="tel:{{trans('homepage.tnumber')}}">
							<svg xmlns="http://www.w3.org/2000/svg" style="width:25px !important;" height="26" viewBox="0 0 26 26" >
								<path style="text-indent:0;text-align:start;line-height:normal;text-transform:none;block-progression:tb;-inkscape-font-specification:Bitstream Vera Sans" d="M 13 0.03125 C 7.7966186 0.03125 3.4115453 3.6370915 2 8.5625 C 0.73794985 9.3225167 0 10.786442 0 12.5 C 0 14.985 1.515 17 4 17 C 4.553 17 5 16.553 5 16 L 5 9 C 5 8.54 4.6734954 8.1789815 4.25 8.0625 C 5.71009 4.478577 9.0805494 1.96875 13 1.96875 C 16.91945 1.96875 20.28991 4.478577 21.75 8.0625 C 21.326505 8.1789815 21 8.54 21 9 L 21 16 C 21 16.553 21.447 17 22 17 C 22.232675 17 22.44103 16.971562 22.65625 16.9375 C 22.332832 18.803483 21.549188 20.194009 20.75 21.15625 C 20.251551 21.756394 19.740424 22.191323 19.34375 22.46875 C 19.145413 22.607464 18.991125 22.693636 18.875 22.75 C 18.816938 22.77818 18.769457 22.80392 18.75 22.8125 L 16.875 22.8125 C 16.514882 21.768341 15.368809 21 14 21 C 12.343146 21 11 22.119288 11 23.5 C 11 24.880712 12.343146 26 14 26 C 15.368809 26 16.514882 25.231659 16.875 24.1875 L 18.84375 24.1875 C 19.181782 24.1875 19.267722 24.09757 19.46875 24 C 19.669778 23.90243 19.905978 23.768786 20.15625 23.59375 C 20.656794 23.243677 21.260824 22.733106 21.84375 22.03125 C 22.898041 20.76186 23.879777 18.875966 24.125 16.34375 C 25.304123 15.561135 26 14.152115 26 12.5 C 26 10.786442 25.26205 9.3225167 24 8.5625 C 22.588455 3.6370915 18.203382 0.03125 13 0.03125 z" color="#000" overflow="visible" enable-background="accumulate" font-family="Bitstream Vera Sans" class="                                                                            undefined" ></path >
							</svg >
						</a >
					</span >
					<div class="columns five stadiumpicture" id="stadiumpicture" >
						@if(empty($svgmap))
							@if(empty($svgmapfile))
								@if($groundImage != '')
									<a href="{{$groundImage}}" > <img src="{{$groundImage}}" /> </a >
								@else
									<a href="{{ Assets::Path('images/stadiumpic.png') }}" >
										<img src="{{ Assets::Path('images/stadiumpic.png') }}" /> </a >
								@endif
							@else
								<?php  $path = __DIR__ . '/../../../public/assets/maps/' . $node->svgmap . '';
								if (file_exists($path)) {
									echo file_get_contents($path);
								} else {
									echo "<!-- broken -->";
								}

								?>
								<div class="seatpreview hidden-xs"></div>


								<div style="width: 100%; text-align: center; float: left; padding: 20px; " >{{trans('homepage.mapsmayvairy')}}</div >
							@endif

						@else
							{{$svgmap}}
							<div class="seatpreview hidden-xs"></div>
						@endif
						<div class="seatpreviewImg hidden-xs" style="display:none;"><br><br>
							<h3>Seat View</h3><img class="seatimgholder"></div>

							<!-- TrustBox script -->
							<script type="text/javascript" src="//widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js" async></script>
							<!-- End Trustbox script -->

							<!-- TrustBox widget - List -->
							<div class="trustpilot-widget" data-locale="en-GB" data-template-id="539ad60defb9600b94d7df2c" data-businessunit-id="54f5f5ad0000ff00057dce30" data-style-height="500px" data-style-width="100%" data-stars="5" data-schema-type="Organization">
								<a href="https://uk.trustpilot.com/review/footballticketpad.com" target="_blank">Trustpilot</a>
							</div>
							<!-- End TrustBox widget -->


						<ul class="guarenteetxt-container">
							<li class="guarenteetxt"> {{trans('homepage.gttc1')}}</li>
							<li class="guarenteetxt"> {{trans('homepage.gttc2')}} </li>
							<li class="guarenteetxt"> {{trans('homepage.gttc3')}} </li>
							<li class="guarenteetxt"> {{trans('homepage.gttc4')}}</li>
						</ul>
					</div >
					<div id="buy-ticket" class="columns seven " style="float:right !important;" >
			   		<span onclick="jQuery('.mobileFilterTab').attr('class','mobileFilterTab open'); jQuery('.mobileFilterTab').css('display','block')" class="callMobile hidden-md" style="background: #E32963; padding: 15px; border-radius: 5px; z-index: 1000000; width: 50%; float: right; max-width: 200px !important; " >
					 <div onclick="jQuery('.mobileFilterTab').attr('class','mobileFilterTab open'); jQuery('.mobileFilterTab').css('display','block')" style="float: left; color: white; font-weight: bold; " >
					 {{trans('homepage.apply-a-filter')}}
					 </div ><svg onclick="jQuery('.mobileFilterTab').attr('class','mobileFilterTab open'); jQuery('.mobileFilterTab').css('display','block')" xmlns="http://www.w3.org/2000/svg" style="width: 20px !important;margin-bottom: -20px;float: left;margin-top: -17px;position: absolute;right: 11px !important;" width="50" height="50" viewBox="0 0 50 50" class="fa-rotate" >
					   <path d="M50,28.794v-7.69l-7.343-1.199c-0.382-1.325-0.91-2.599-1.575-3.802l4.278-6.098l-5.438-5.436l-6.021,4.328 c-1.213-0.675-2.497-1.209-3.83-1.593l-1.28-7.306h-7.69L19.914,7.28c-1.344,0.381-2.629,0.909-3.837,1.576l-6.005-4.29L4.636,10 l4.223,6.038c-0.68,1.222-1.218,2.516-1.605,3.861L0,21.107v7.686l7.242,1.283c0.385,1.344,0.923,2.638,1.605,3.86l-4.28,5.986 L10,45.362l6.047-4.235c1.218,0.674,2.506,1.206,3.845,1.588l1.213,7.287h7.685l1.297-7.307c1.338-0.39,2.62-0.927,3.827-1.603 l6.085,4.27l5.438-5.441l-4.347-6.035c0.666-1.205,1.192-2.48,1.571-3.805L50,28.794z M25,32c-3.86,0-7-3.141-7-7c0-3.86,3.14-7,7-7 c3.859,0,7,3.14,7,7C32,28.859,28.859,32,25,32z" class="                                                                                                                                    undefined" ></path >
				   </svg ></span >

<!--

						@if($daysRemaining > 5)
							<div class="row toolbar  hidden-xs buyListingDaysLeft">
								{{trans('homepage.thereareonly')}} <span class="red"><b>{{$daysRemaining}}</b></span> {{trans('homepage.daysremaining')}}
							</div>
						@endif

						@if($daysRemaining < 5 && $daysRemaining > 1)
							<div class="row toolbar  hidden-xs buyListingDaysLeft">
								<span class="red"><b> <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{trans('homepage.eventin')}} {{$daysRemaining}}  {{trans('homepage.days')}}
										</b></span>
							</div>
						@endif

						@if($daysRemaining == 1)
							<div class="row toolbar  hidden-xs buyListingDaysLeft" style="border: 3px solid #e13d7f">
								<span class="red"><b> <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{trans('homepage.eventtomrowo')}}</b></span>
							</div>
						@endif

						@if($daysRemaining == 0)
							<div class="row toolbar  hidden-xs buyListingDaysLeft" style="border: 3px solid #e13d7f">
								<span class="red"><b> <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{trans('homepage.eventtoday')}} <i><i>{{trans('homepage.orderfast')}}</i></b></span>
							</div>
						@endif
-->

						@if(!empty($tickets) && !$is_past)

							<div class="row toolbar  hidden-xs buyListingFilterBy ">
								<div class="pull-right" style="width:100%;">
									<button class="resetButton" onclick="reset()" >{{trans('homepage.reset-filter')}}</button >
									<select name="fiter_of_ticket" class="buyListingFilterButton">
										<option value="-1" >{{trans('homepage.Select Number Of Tickets')}}</option >
										<option value="1" > 1</option >
										<option value="2" > 2</option >
										<option value="3" > 3</option >
										<option value="4" > 4</option >
										<option value="5" > 5+</option >
									</select >
									<select class="ticket_type_drop_down" name="ticket_type" >
										<option value="-1" >{{trans('homepage.Select Type Of Tickets')}}</option >
									</select >
								</div >
							</div >

							<span id="mobile-only hidden-xs hidden-md" > <a href="#" data-reveal-id="GroupPurchase" class="btn yellowbtn hidden-xs hidden-md" >{{trans('homepage.Group Purchase')}} </a >  </span >
							<?php
							$arrayOfSeatsForJquery = array();
							foreach ($tickets as $ticket) {
								foreach ($ticketTypes as $ticketType) {
									if ($ticketType->id == $ticket['ticketInformation']['ticket_type']) {
										$arrayOfSeatsForJquery[$ticketType->title] = $ticketType->title;
									}
								}
							}
							?>
							<table class="responsive desktop-version" >
								<thead >
								<th >{{trans('homepage.Location')}}</th >
								<th style="width: 25%;">{{trans('homepage.Available')}}</th >
								<th >{{trans('homepage.Price per ticket')}}:</th >
								<th >
									<a href="#" data-reveal-id="GroupPurchase" class="btn yellowbtn" >{{trans('homepage.Group Purchase')}} </a >
								</th >
								</thead >
								<tbody >
								<?php

								/**           foreach($catconvarray as $b => $a){

								$value = trim($b);
								$value = str_replace(" ", "_",$value);
								$value = strtolower($value);
								echo "'".$value."' => '".$a."',<br>";
								}
								die();
								 **/

								$buildAvailabale = [];

								$keOfRow = 0;
								?>

								@foreach($tickets as $ticket)
									@foreach($ticketTypes as $ticketType)
										@if($ticketType->id == $ticket['ticketInformation']['ticket_type'])
											<?php
											$value = $ticketType->title;
											$value = trim($value);
											$value = str_replace(" ", "", $value);
											$value = strtolower($value);

											if (array_key_exists($value, $catconvarray)) {
												$finalTicketTipe = $catconvarray[$value];
												$buildAvailabale[$finalTicketTipe] = $finalTicketTipe;
											} else {
												$finalTicketTipe = " ";
											}
											?>
											<tr class="trigger_{{$finalTicketTipe}}" >
												@endif
												@endforeach
												<td data-ticket-type="{{$ticket['ticketInformation']['ticket_type']}}" data-type="@foreach($ticketTypes as $ticketType)
												@if($ticketType->id == $ticket['ticketInformation']['ticket_type'])
												{{$ticketType->title}}
												@endif
												@endforeach" class="td-ticket-type" >
													@foreach($ticketTypes as $ticketType)
														@if($ticketType->id == $ticket['ticketInformation']['ticket_type'])
															{{$ticketType->title}}
														@endif
													@endforeach
													<span class="block-row" data-block-row="@if(!empty($ticket['ticketInformation']['loc_block'])){{$ticket['ticketInformation']['loc_block']}}@endif" >
												@if(!empty($ticket['ticketInformation']['loc_block']))
															{{trans('homepage.block')}}: {{$ticket['ticketInformation']['loc_block']}}
															<br />
														@endif
														@if(!empty($ticket['ticketInformation']['loc_row']))
														{{trans('homepage.row')}}: {{$ticket['ticketInformation']['loc_row']}}
														@endif
											</span >
												</td >
												<td data-number-of-ticket="{{$ticket['ticketInformation']['number_of_ticket']}}" class="td-num-of-ticket" >
													@if($ticket['available_qty'] <=3)<span
															class="red">{{trans('homepage.only')}} @endif
														{{$ticket['available_qty']}} @if($ticket['available_qty'] >1) {{trans('homepage.ticketMulti')}} @else {{trans('homepage.ticketMulti')}} @endif {{trans('homepage.Available')}} @if($ticket['available_qty'] <=3) </span> @endif

													@if($ticket['onlyTicket']==1)
														<div class=" blue " style="font-size: 9px; color: #039eea; font-weight: 700;">
															{{trans('homepage.lastticketinsection')}}
														</div>
													@endif
												</td >

												<td class="">
													<div class="buyListingTicketPrice green bgcolor visible-xs"
														 style=" padding:3px !important;">
														<a href="/checkout/{{$ticket['product_id']}}"
														   style="color: white !important;"
														   class="visible-xs"> {{trans('homepage.currencyInUse')}}{{$ticket['ticketInformation']['price']}} {{trans('homepage.currencyAfter')}}
														</a>


													</div>
													 <span CLASS="hidden-xs"
														   style="width:100%; float:left;">{{trans('homepage.currencyInUse')}}{{$ticket['ticketInformation']['price']}} {{trans('homepage.currencyAfter')}}
														</span>
													<!-- BEST BUY -->
													@if($ticket['bestbuy'] == 2)
														<div class="buyListingBestBuy yellow bgcolor">
															{{trans('homepage.Todays Deal')}}
														</div>
													@endif



												<!-- READY -->
													@if($ticket['note']->info['restrictions'])
														<span style="float:left;" data-tooltip aria-haspopup="true" class="has-tip"
														title="@foreach($defaultRestriction as $r)
																	@if(in_array($r['id'], $ticket['note']->info['restrictions']))
																		@if($r['id'] == 11 && $ticket['note']->buyerNot != '')
																			{{trans('homepage.Other Note')}}:
																			{{addslashes ($ticket['note']->buyerNot) }}
																		@else
																			{{addslashes ($r['title'])}},
																		@endif
																	@endif
																@endforeach
																">
														<i class="fa fa-commenting-o buyListingReadyToShip green bgcolor" aria-hidden="true"  onClick="jQuery('.hiddenMobileExtra').css('display','none'); jQuery('.@if(!empty($ticket['product_id']))ticketidget-{{$ticket['product_id']}}@endif').css('display','block');"></i>
														</span>
													@endif
												</td >
												<td class="hidden-xs">
													@if(!empty($ticket['note']->ticket_status))
														@if($ticket['note']->ticket_status == "active")
															<a href="/checkout/{{$ticket['product_id']}}"
															   class="buyListingButton bgcolor red">{{trans('homepage.BUY')}} </a>
														@else
															{{trans('homepage.sold')}}
														@endif
													@else

													@endif
												</td >

												<div class="hiddenMobileExtra @if(!empty($ticket['product_id'])) ticketidget-{{$ticket['product_id']}} @endif" style="display:none;">

													@if(!empty($ticket['note']->buyerNot) || !empty($ticket['note']->info['restrictions']))
														<b>{{trans('homepage.buyernotes')}}</b><br>
													@endif

													@if(!empty($ticket['note']->info['restrictions']))
														@foreach($defaultRestriction as $r)
															@if(in_array($r['id'],$ticket['note']->info['restrictions']))
																{{addslashes ($r['title'])}},
															@endif
														@endforeach
														<br>
													@endif

													@if(!empty($ticket['note']->buyerNot))
														{{$ticket['note']->buyerNot}}
													@endif
												</div>
											</tr >

											@endforeach
								</tbody >
							</table >
						@elseif($is_past)
							<div class="""row">
							<br ><br >
							<b >{{trans('homepage.played')}}</b >
							<br ><br ><br >
					</div >
					@else
						<div class="""row">
						<br ><br >
						<b >{{trans('homepage.tbNoneAvail')}}</b >
						<br ><br ><br >
						<div id="my-response" style="font-size:18px; padding:10px;" ></div >
				</div >
				<form class="enquiryform" action="/request-tickets" method="post" >
					<input type="hidden" name="eventId" value="{{$node->id}}" />
					<div class="row" >
						<input type="text" placeholder="{{trans('homepage.cUs1')}}" name="name" required />
					</div >
					<div class="row" >
						<input type="email" placeholder="{{trans('homepage.cUs2')}}" name="email" required />
					</div >
					<div class="row" >
						<input type="text" placeholder="{{trans('homepage.cUs3')}}" name="phone" required />
					</div >
					<div class="row" >
						<input type="text" placeholder="{{trans('homepage.trf2')}}" name="numTickets" required />
					</div >
					<div class="row" >
						<select name="reqTogether" class="styled-select" >
							<option value="0" >{{trans('homepage.trf3')}}</option >
							<option value="1" >{{trans('homepage.trf4')}}</option >
						</select >
					</div >
					<div class="row" >
						<select name="side" class="styled-select" >
							<option value="{{$node->homeTeam}}" >{{$node->homeTeam}} {{trans('homepage.side')}}</option >
							<option value="{{$node->awayTeam}}" >{{$node->awayTeam}} {{trans('homepage.side')}}</option >
						</select >
					</div >
					<div class="row" >
						<textarea placeholder="{{trans('homepage.trf6')}}" name="additionalInfo" ></textarea >
					</div >
					<div class="row captcha-container">

						<div class="captcha-image pull-left columns six">
							{{ HTML::image(Captcha::img(), 'Captcha image') }}
						</div>
						<div class="pull-right columns six">
							{{ Form::text('captcha', '', array('required'=>'', 'placeholder'=>trans('homepage.capEC'))) }}
						</div>
					</div>
					<div class="row" >
						<input type="submit" class="btn pinkbtn pull-right contact-submit" value="{{trans('homepage.Submit')}}" >
					</div >
				</form >
				<br ><br ><br >
				@endif
				@if(isset($homeTeamEvents) && count($homeTeamEvents) > 0 && isset($homeTeam->title))
					<div class="otherEvents" >
						<H2 >{{trans('homepage.trf7')}} <strong >{{$homeTeam->title}}</strong ></H2 >
						<ul class="othervents" >
							@foreach($homeTeamEvents as $e)
								<li >
									<span ><a href="{{FootBallEvent::getUrl($e)}}" > {{ $e->title }} </a ></span >
							<span class="date-details" >@if(!$e->is_tbc){{date('g:ia d-m-y', strtotime($e->datetime))}}@else TBC @endif
								, {{$e->event_location}}</span >
								</li >
							@endforeach
						</ul >
					</div >
				@endif
				@if(!empty($articles))
					<div class="columns twelve hidden-xs" ><h3 >Associated Articles</h3 ></div >
					<div class="article-container  hidden-xs" >
						@foreach($articles as $key=>$article )
							<div class="columns twelve arCont  hidden-xs" style="padding-left:15px !important; padding-right:15px !important;" >
								<div class="arti @if($key%2 == 0) odd @else even @endif" >
									<div class="hdr" >
										<a href="/news/{{$article->slug}}" >{{$article->title}}</a >
									</div >
									<div class="excerpt" >
										<p >{{$article->excerpt}}</p >
									</div >
									<div class="foot" >
										<a href="/news/{{$article->slug}}" >{{trans('homepage.viewfull')}}</a >
									</div >
								</div >
							</div >
						@endforeach
					</div >
				@endif
			</div >
			{{ Assets::jsStart() }}
			<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.js" ></script >
			<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.css" >
			<script type="text/javascript" >
				function strip_tags(input, allowed) {

					allowed = (((allowed || '') + '')
						.toLowerCase()
						.match(/<[a-z][a-z0-9]*>/g) || [])
						.join(''); // making sure the allowed arg is a string containing only tags in lowercase (<a><b><c>)
					var tags = /<\/?([a-z][a-z0-9]*)\b[^>]*>/gi,
						commentsAndPhpTags = /<!--[\s\S]*?-->|<\?(?:php)?[\s\S]*?\?>/gi;
					return input.replace(commentsAndPhpTags, '')
						.replace(tags, function ($0, $1) {
							return allowed.indexOf('<' + $1.toLowerCase() + '>') > -1 ? $0 : '';
						});
				}
				(function ($) {



					var ticketType = [];
					$(document).ready(function () {

						$('.td-ticket-type').each(function () {
							var t = strip_tags($(this).data('type'));

							if (ticketType.indexOf(t) == '-1') {
								ticketType.push(t);
							}
						});

						if (ticketType.length > 0) {
							for (var i = 0; i < ticketType.length; i++) {
								var option = '<option value="' + ticketType[i] + '">' + ticketType[i] + '</option>';
								$('select[name=ticket_type]').append(option);
							}
						}


						$('select[name=fiter_of_ticket]').change(function () {
							var selectedNumberOfTicket = $(this).val();
							$('.td-num-of-ticket').each(function () {
								var t = $(this).data('number-of-ticket');
								var parentTr = $(this).closest('tr');

								if (selectedNumberOfTicket == '-1') {
									if (parentTr.data('type-filter') !== true) {
										parentTr.show();

									}
									parentTr.data('number-filter', false);

								}
								else if (parseInt(selectedNumberOfTicket) <= parseInt(t)) {
									if (parentTr.data('type-filter') !== true) {
										parentTr.show();

									}
									parentTr.data('number-filter', false);
								} else {
									parentTr.data('number-filter', true);
									parentTr.hide();
								}
							});
						});


						$('select[name=ticket_type]').change(function () {
							var selectedNumberOfTicket = $(this).val();
							$('.td-ticket-type').each(function () {
								var t = strip_tags($(this).data('type'));
								var parentTr = $(this).closest('tr');

								if (selectedNumberOfTicket == '-1') {
									parentTr.data('type-filter', false);
									if (parentTr.data('number-filter') !== true) {
										parentTr.show();
									}

								}
								else if (selectedNumberOfTicket == t) {
									parentTr.data('type-filter', false);
									if (parentTr.data('number-filter') !== true) {
										parentTr.show();
									}
								} else {
									parentTr.data('type-filter', true);
									parentTr.hide();
								}
							});
						});


						$('.buyer-note-mob').click(function (e) {
							e.preventDefault();
							var buyer_note = $(this).attr('title');
							var template = _.template($('#buyer-note-modal-template').html());
							$('#buyer-note-modal').remove();
							$('body').append(template({
								buyer_note: buyer_note
							}));

							$('#buyer-note-modal').foundation('reveal', 'open');
						});

					})
				})(jQuery);


				(function ($) {
					$.validator.setDefaults({
						success: "valid"
					});

					var form = $('form.enquiryform');
					var body = $("body");

					$(document).ready(function () {
						form.ajaxForm({
							dataType: 'json',
							beforeSubmit: function (arr, $form, options) {
								if (form.valid()) {
									body.append('<div class="ajax-loading-modal"></div>');
									body.addClass("loading");
									return true;
								} else {
									return false;
								}
							},
							success: function (response) {
								body.removeClass("loading");
								$('.ajax-loading-modal').remove();

								$('#my-response').html("{{trans('homepage.buyReqSent')}}");
								$('#my-response').css({backgroundColor: '#CFE8C8'});

								setTimeout(function () {
									$('.close-reveal-modal').trigger('click');
								}, 5000);

							},
							error: function (response) {
								body.removeClass("loading");
								$('.ajax-loading-modal').remove();

								$('#my-response').html("<p>{{trans('homepage.buyReqNotSent')}}</p>");
								$('#my-response').css({backgroundColor: 'rgb(232, 200, 200)'});

								var obj = $.parseJSON(response.responseText);
								$('.captcha-image').html(obj.result.captcha_image);

								setTimeout(function () {
									$('.close-reveal-modal').trigger('click');
								}, 5000);
							}
						});
					});

				})(jQuery)
			</script >
			<script type="text/javascript" charset="utf-8" >
				(function ($) {
					var body = $('body');

					/* close modal */
					body.on('click', '.close-reveal-modal', function (e) {
						e.preventDefault();
						var ElemId = $(this).closest('div.reveal-modal').attr('id');
						$('#' + ElemId).foundation('reveal', 'close');
						$('#' + ElemId).fadeOut('slow', function () {
							$('#' + ElemId).remove();
							$('.reveal-modal-bg').remove();
						});
					});

					body.on('click', '.reveal-modal-bg', function (e) {
						e.preventDefault();
						var ElemId = body.find('div.reveal-modal').attr('id');
						$('#' + ElemId).foundation('reveal', 'close');
						$('#' + ElemId).fadeOut('slow', function () {
							$('#' + ElemId).remove();
							$('.reveal-modal-bg').remove();
						});
					})

				})(jQuery);
			</script >
			<script >
				var firstTouch = 0;
				$(document).ready(function () {


					/**
					 *    This Block Currently Handles The on Seat View Section
					 */
					jQuery('.block-row').each(function () {
						var parent = "" + jQuery(this).parent('td').parent('tr').attr('class');
						var blockdata = "" + jQuery(this).attr('data-block-row');
						parent = parent.replace('trigger_', '').replace(' hover', '');
						if (blockdata != "" && parent.length > 1) {
							var builtBlockString = " [block='" + blockdata + "']." + parent;
							var image = decodeURIComponent(jQuery(builtBlockString).attr('view'));
							if (image.length > 10) {
								jQuery(this).append("<a  class='fancybox' rel='group'  target='_blank' href='" + image + "'>{{trans('homepage.View From Seat')}}</a>");
							}
						}

					});


					jQuery(".fancybox").fancybox();

					jQuery('.td-ticket-type').click(function () {

						var blockrow = jQuery(this).children('.block-row').attr('data-block-row');
						var parent = jQuery(this).parent('tr').attr('class').replace('trigger_', '').replace(' hover', '');

						var block = " [block='" + blockrow + "']." + parent;
						console.log(block);
						jQuery(block).attr('class', jQuery(block).attr('class') + " blockhere");
					});

					functionallOn();

					@if(!empty($buildAvailabale))
						@foreach($buildAvailabale as $value)
							@if(!empty($fullnames[$value]['name']))
							jQuery(".seatpreview").append('<div style="@if(empty($fullnames[$value]['vis']) || $fullnames[$value]['vis'] != 1) display:none; @endif" class="tickpill {{$value}}_tickpill"><input data-enable="{{$value}}" type="checkbox"> {{$fullnames[$value]['name']}}</div>');
					jQuery(".seatzones").append('<button class="{{$value}}_button">{{$fullnames[$value]['name']}}</button>');
					@endif
				@endforeach
			@endif

		jQuery('.tickpill').click(function () {
						var dataenabled = jQuery(this).children('input').attr('data-enable');
						triggerChange(dataenabled);
					});

					@if (!empty($arrayOfSeatsForJquery))
					@foreach($arrayOfSeatsForJquery as $value)
					<?php
					$value = $value;
					$value = trim($value);
					$value = str_replace(" ", "", $value);
					$value = strtolower($value);

					if (array_key_exists($value, $catconvarray)) {

					$value = $catconvarray[$value]; ?>



					/**
					 * Mouse Enters the MAP
					 */
					jQuery('.{{$value}}').mouseenter(function () {
						cleanMap();
						var thisValuex = jQuery('.{{$value}}').attr('class');
						var x = 'hover ' + thisValuex;
						jQuery('.{{$value}}').attr('class', x);
						jQuery('.trigger_{{$value}}').attr('class', 'trigger_{{$value}} hover');
					});


					/**
					 * Mouse LEAVES the MAP
					 */
					jQuery('.{{$value}}').mouseleave(function () {
						cleanMap();
						jQuery('.trigger_{{$value}}').attr('class', 'trigger_{{$value}}');

					});

					/**
					 * Mouse ENTERS the Listing
					 */
					jQuery('.trigger_{{$value}}').mouseenter(function () {
						cleanMap();
						var thisValuex = jQuery('.{{$value}}').attr('class');
						var x = 'hover ' + thisValuex;
						jQuery('.{{$value}}').attr('class', x);
						jQuery(this).attr('class', 'trigger_{{$value}} hover');

					});

					/**
					 * Mouse LEAVES the Listing
					 */
					jQuery('.trigger_{{$value}}').mouseleave(function () {
						cleanMap();
						jQuery(this).attr('class', 'trigger_{{$value}}');

					});


					jQuery('.{{$value}}').click(function () {
						reset();
						triggerChange('{{$value}}');
						var viewImg = decodeURIComponent(jQuery(this).attr('view'));
					});


					jQuery('.{{$value}}_button').click(function () {
						triggerChange('{{$value}}');
					});

					<?php  } ?>

					@endforeach
					@endif
				})
				;
			</script >
			<script >
				function triggerChange(seat) {

					if (jQuery('.' + seat + '_button').attr('class') != '' + seat + '_button active') {
						enableAllForSeat('' + seat + '');
					} else {
						disableAllForSeat('' + seat + '');
					}
				}

				function enableAllForSeat(seat) {

					if (firstTouch == 0) {
						functionalOff();
						console.log('firstTouchOff');
						firstTouch = 1;
					}

					/* Check the Tick Box */
					var Classpill = '.' + seat + '_tickpill input';
					jQuery(Classpill).attr('checked', 'checked');

					/* Enable the buttons */
					var Classbuttonx = '.' + seat + '_button';
					var buttonClass = "" + jQuery(Classbuttonx).attr('class');
					buttonClass = buttonClass.replace(' active', '');
					jQuery(Classbuttonx).attr('class', '' + buttonClass + ' active');


					/* Enable the map */
					var ClassSeat = '.' + seat + '';
					var mapClass = "" + jQuery(ClassSeat).attr('class');
					mapClass = mapClass.replace(' toggle', '');
					jQuery(ClassSeat).attr('class', '' + mapClass + ' toggle');


					checkPillFilters();
				}

				function disableAllForSeat(seat) {

					if (firstTouch == 0) {
						functionalOff();
						console.log('firstTouchOff');
						firstTouch = 1;
					}

					/* Check the Tick Box */
					var Classpill = '.' + seat + '_tickpill input';
					jQuery(Classpill).attr('checked', false);

					/* Enable the buttons */
					var Classbuttonx = '.' + seat + '_button';
					var buttonClass = "" + jQuery(Classbuttonx).attr('class');
					buttonClass = buttonClass.replace(' active', '').replace('active', '').replace('active', '');
					jQuery(Classbuttonx).attr('class', '' + buttonClass + '');


					/* Enable the map */
					var ClassSeat = '.' + seat + '';
					var mapClass = "" + jQuery(ClassSeat).attr('class');
					mapClass = mapClass.replace(' toggle', '').replace('toggle', '').replace('toggle', '');
					jQuery(ClassSeat).attr('class', '' + mapClass + '');


					checkPillFilters();
				}

				function functionallOn() {
					@foreach($fullnames as $key => $value)
					jQuery('.{{$key}}').attr('class', '{{$key}} toggle');
					@endforeach
				}
				function functionalOff() {
					@foreach($fullnames as $key => $value)
				   jQuery('.{{$key}}').attr('class', '{{$key}} ');
					@endforeach
				}
				function checkPillFilters() {
					var thisarray = [];
					jQuery('.tickpill').each(function () {
						if (jQuery(this).children('input').attr('checked') == 'checked') {
							thisarray.push(jQuery(this).children('input').attr('data-enable'));
						}
					});


					jQuery('table.responsive.desktop-version tr').attr('style', 'display:none;');
					if ((thisarray.length != 0)) {
						$.each(thisarray, function (item, ex) {
							console.log(item);
							console.log('.trigger_' + ex);
							jQuery('.trigger_' + ex).attr('style', 'display:table-row;');

						})
					} else {
						jQuery('table.responsive.desktop-version tr').attr('style', 'display:table-row;');
					}
				}
				function reset() {
					jQuery('.ticket_type_drop_down').val(1);
					jQuery('table.responsive.desktop-version tr').attr('style', 'display:table-row;');
					cleanHard();
					jQuery('.tickpill input').attr('checked', false);
				}

				function cleanMap() {
					jQuery('svg *').each(function () {
						{
							var thisValuex = jQuery(this).attr('class');
							var x = "  " + thisValuex;
							x = x.replace("hover", "");
							jQuery(this).attr('class', x);
						}

					});

				}

				function cleanHard() {
					jQuery('svg *').each(function () {
						{
							var thisValuex = jQuery(this).attr('class');
							var x = "  " + thisValuex;
							x = x.replace("hover", "");
							x = x.replace("toggle", "");
							jQuery(this).attr('class', x);
						}

					});

				}
				jQuery('.ticket_type_drop_down').change(function () {
					var list = {
						@foreach($catconvarray as $a => $b)
						'{{$a}}': '{{$b}}',
						@endforeach
					};
					cleanHard();
					var x = list[jQuery('.ticket_type_drop_down').val().toLowerCase().trim().replace(" ", "").replace(" ", "").replace(" ", "").replace(" ", "")];
					var y = x;
					if (x != "") {
						y = x;
						x = "." + x;

						var z = " " + x + "_tickpill input";

						jQuery(x).attr('class', '' + y + ' toggle');
						console.log(z);
						jQuery('.tickpill input').attr('checked', false);
						jQuery(z).attr('checked', 'checked');
					}

				});

				$(document).ready(function () {
					jQuery('.closeFilter_button').click(function () {
						jQuery('.mobileFilterTab').attr('class', 'mobileFilterTab')
						jQuery('.mobileFilterTab').css('display', 'none')
					});
				});



			</script >
			@if($stadium)
				<script type="application/ld+json" >
		{
		  "@context": "http://schema.org/",
		  "@type": "SportsEvent",
		  "name": "{{$node->title}}",
		  "startDate": "@if(!$node->is_tbc){{date('Y-m-d H:i:s', strtotime($node->datetime))}}@else TBC @endif",
		  "location": {
			"@type": "StadiumOrArena",
			"name": "@if(!empty($stadium->title)){{$stadium->title}}@endif",
			"address": "@if(!empty($stadium->title)){{$stadium->al1}}@endif
					, @if(!empty($stadium->al2)){{$stadium->al2}}@endif
					, @if(!empty($stadium->postcode)){{$stadium->postcode}}@endif
					, @if(!empty($stadium->countryname)){{$stadium->countryname}}@endif"
		  },
		  "awayTeam": {
			"@type": "SportsTeam",
			"name": "{{$node->awayTeam}}"
		  },
		  "homeTeam": {
			"@type": "SportsTeam",
			"name": "{{$node->homeTeam}}"
		  }
		}

				</script >
			@endif
			<script type="text/x-template" id="buyer-note-modal-template" charset="utf-8" >
				<div id="buyer-note-modal" class="reveal-modal" data-reveal style="display:none" >
					<div id="project-label" >{{trans('homepage.Buyer Notes')}}</div >
	<%= buyer_note %>
	<a class="close-reveal-modal">&#215;</a>
	</div>
	</script>


	{{ Assets::jsEnd() }}


	<div style="float:left;" class="trustpilot-widget mobiles-trust hidden-xs" data-locale="en-GB" data-template-id="539ad60defb9600b94d7df2c" data-businessunit-id="54f5f5ad0000ff00057dce30" data-style-height="300px" data-style-width="100%" data-stars="3,4,5"></div>



	</div>

	</div>

	</div>
	</div>


	<div id="GroupPurchase" class="reveal-modal" data-reveal>
	<a class="close-reveal-modal">&#215;</a>


	<span class="ftp-line">
	<span class="greenline"></span>
	<span class="yellowline"></span>
	<span class="pinkline"></span>
	<span class="blueline"></span>
	</span>

	<h2>{{trans('homepage.Tickets for')}} <strong>{{$node->homeTeam}}</strong> vs  <strong>{{$node->awayTeam}}</strong></h2>

	<p>
	{{trans('homepage.Tickets for')}} <strong>{{$node->homeTeam}}</strong> v <strong>{{$node->awayTeam}}</strong> {{trans('homepage.buy_corp_text')}}


	</p>


	<form class="enquiryform" action="{{route('customer.group-purchase-request')}}" method="post">
	{{Form::token();}}
	<input name="game" value="{{$node->title}}" type="hidden">
	<input name="event_id" value="{{$node->id}}" type="hidden">
	<div class="row">
	<input type="text" placeholder="{{trans('homepage.Your Name')}}" name="name" required />
	</div>

	<div class="row">
	<input type="text" placeholder="{{trans('homepage.Company Name')}}" name="company_name" required/>
	</div>


	<div class="row">
	<input type="text" placeholder="{{trans('homepage.Your Email')}}" name="email" required/>
	</div>


	<div class="row">
	<input type="text" placeholder="{{trans('homepage.Telephone-mobile')}}" name="tel" required/>
	</div>


	<div class="row">
	<textarea placeholder="{{trans('homepage.Message')}}" name="message" required></textarea>
	</div>

	<div class="row">
	{{ HTML::image(Captcha::img(), 'Captcha image') }}
	{{ Form::text('captcha', '', array('required'=>'', 'placeholder'=>trans('homepage.capEC'))) }}
	</div>


	<div class="row">
	<input type="submit" class="btn pinkbtn pull-right" value="{{trans('homepage.Submit')}}">
	</div>

	<div class="row post-after-response">
	</div>
	</form>


	</div>



	<!---------main content------------>


	@stop

	{{
	Assets::setScripts(
	[
	'serializejson'          => 'js/jquery.serializejson.min.js',
	'jquery-form'            => 'js/jquery-form.min.js',
	'jquery-validator'       => 'js/jquery-validator/jquery.validate.min.js',
	'validator-add-method'   => 'js/jquery-validator/additional-methods.min.js',
	'underscore'             => 'js/underscore.min.js'
	], false, true);
	}}

	{{ Assets::jsStart() }}
	<style>

	</style>
	<script>
	(function ($){
	var index =0;
	$('.description > p').each(function () {
	if($.trim($(this).html()) != '&nbsp;' && index < 2) {
	$(this).addClass('para-display');
	index++;
	} else if($.trim($(this).html()) != '&nbsp;') {
	$(this).addClass('masked');
	}
	});

	if(index > 1) {
	$('<div class="show-all-text"><a href="#" class="down-arrow">{{trans("homepage.Show more")}}</a></div><div stye="clear: both">').insertAfter('.site-content p.masked:last');
	}

	$('body').on('click', '.show-all-text a.down-arrow', function (e) {
	e.preventDefault();
	$('.site-content p.masked').addClass('unmasked');
	$('.site-content p.unmasked').removeClass('masked');
	$(this).addClass('up-arrow');
	$(this).removeClass('down-arrow');
	$(this).html('{{trans("homepage.Hide")}}');
	});

	$('body').on('click', '.show-all-text a.up-arrow', function (e) {
	e.preventDefault();
	$('.site-content p.unmasked').addClass('masked');
	$('.site-content p.masked').removeClass('unmasked');
	$(this).addClass('down-arrow');
	$(this).removeClass('up-arrow');
	$(this).html("{{trans('homepage.Show more')}}");
	var firstPara = $('.site-content p.para-display').first();
	$('html,body').animate({scrollTop: firstPara.offset().top -100 }, 'slow');
	});

	$('.site-content').css({display: 'block'})
	})(jQuery)

	</script>

	{{ Assets::jsEnd() }}
