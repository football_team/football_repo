@extends(Template::name('frontend.%s._layout.layout'))
@section('content')
	<style>
		.mobileClickZone{
			display:none !important;
		}

		@media (max-width: 767px) {
			header {
				background: #000 !important;
				padding-bottom: 0 !important;
			}
		}
	</style>
	<!-- ######################## Section ######################## -->
<!---------sidebar + search form-------------->
<section class="main" >
	<div class="overlay" >
		<div class="row" >
			<div class="searcharea" >
				<span class="slogan" >{{trans('homepage.slogan')}}</span >
				<form id="form-home" action="/ticket-search" method="get" >
					<input class="forminput" placeholder="{{trans('homepage.search_example');}}" name="search" >
					<input class="bigbox-search-button" type="submit" />
				</form >
			</div >
		</div >
	</div >
</section >
<!---------sidebar + search form------------>
{{ Template::doShortCode("bm-slider id=3", true) }}
<div class="row homepageFeatureForceRow">
	<section class="main" >
		<div class="overlay" >
			<div class="row" >
				@include(Template::name('frontend.%s.sidebars.side1'))
			</div >
		</div >
	</section >
	<div class="site-content" >
		<div class="hidden-xs">
		@if($node instanceof Pages)
			{{ Template::doShortCode($node->content) }}
		@endif
			</div>
		<div class="visible-xs">
			<div class="col-xs-12 homepageFeaturedImage" style="  width:100%; float:left;  background-image: url('/uploads/premier_league_homepage_640x640.jpg');background-size: cover !important;height: 270px;">
			<a href="/group/league/premier-league"><div class="gradiant">
				<p>{{trans('homepage.Premier League')}}</p>

			</div></a>
				</div>
			<div class="col-xs-12 homepageFeaturedImage" style="  width:100%; float:left;  background-image: url('/uploads/la_liga_homepage_640x640.jpg');background-size: cover !important;height: 270px;">
				<a href="/group/league/la-liga"><div class="gradiant"><p>{{trans('homepage.La Liga')}}</p>			</div></a>

			</div>
			<div class="col-xs-12 homepageFeaturedImage" style="  width:100%; float:left;  background-image: url('/uploads/bundesliga_homepage_640x640.jpg');background-size: cover !important;height: 270px;">
				<a href="/group/league/bundesliga"><div class="gradiant"><p>{{trans('homepage.Bundesliga')}}</p>			</div></a>images

			</div>
			<div class="col-xs-12 homepageFeaturedImage" style="     margin-bottom: -30px;  width:100%; float:left;  background-image: url('/uploads/serie_A_homepage_640x640.jpg');background-size: cover !important;height: 270px;">
				<a href="/group/league/serie-a"><div class="gradiant"><p>{{trans('homepage.Serie A')}}</p></div></a>

			</div>
		</div>
		<div class="upcoming-area" >
			@if( isset($upcomingMatches) && count($upcomingMatches) > 0)
				<h3 >{{trans('homepage.up and coming matches');}}</h3 >

				@foreach($upcomingMatches as $ticket)
					@if(empty($key))
						<div class="columns four topmatch" id="upcoming_event_{{ $key = 1 }}" >
					@else
						<div class="columns four topmatch" id="upcoming_event_{{ ++$key  }}" >
					@endif

					@if(($ticket->homeTeamClubLog->value != "") && ($ticket->awayTeamClubLog->value != ""))
					<div class="ft-image-match" >
						<a href="{{FootBallEvent::getUrl($ticket)}}" id="upcoming_event_link_{{ $key }}" >
							<div class="default-fallback" >
								<img src="{{ @$ticket->homeTeamClubLog->value }}" />
								<span >{{trans('homepage.VS')}}</span >
								<img src="{{ @$ticket->awayTeamClubLog->value }}" />
							</div >
						</a >
					</div >
					@elseif(trim($ticket->feature_image) != '')
					<div class="ft-image-match" style="background-image:url({{$ticket->feature_image}}); background-position:center; background-size:cover;">
						<a href="{{FootBallEvent::getUrl($ticket)}}" id="upcoming_event_link_{{ $key }}" >
						</a >
					</div >
					@else
					<div class="ft-image-match" >
						<a href="{{FootBallEvent::getUrl($ticket)}}" id="upcoming_event_link_{{ $key }}" >
							<div class="default-fallback" >
								<img src="{{ @$ticket->homeTeamClubLog->value }}" />
								<span >vs</span >
								<img src="{{ @$ticket->awayTeamClubLog->value }}" />
							</div >
						</a >
					</div>
					@endif

					 <span class="ftp-line" >
						<span class="greenline" ></span >
						<span class="yellowline" ></span >
						<span class="pinkline" ></span >
						<span class="blueline" ></span >
					</span >
					<a href="{{FootBallEvent::getUrl($ticket)}}" >
						<span class="gamematch" >{{$ticket->title}}</span >
					</a >
					<span class="stadium" ><img src="https://cdn3.footballticketpad.com/frontend/bm2014/images/stadium.png" />{{$ticket->event_location}}</span >
					<span class="date-time" ><img src="https://cdn3.footballticketpad.com/frontend/bm2014/images/whistle.png" />{{date('d-m-Y H:i', strtotime($ticket->datetime))}}</span >
					<div class="btnsgroup" >
						<a href="{{ FootBallEvent::getUrl($ticket) }}" class="btn pinkbtn" > {{trans('homepage.BUY')}} </a >
						<a href="{{ '/ticket/sell/'.$ticket->id }}" class="btn bluebtn" > {{trans('homepage.SELL')}} </a >
					</div >
				</div >
				<!--repeater--->
				@endforeach
				<hr >
			@endif
		</div >
		</div >
		</div>
</div>

@include('frontend.bm2014.moduals_global.footersliders');
@stop
