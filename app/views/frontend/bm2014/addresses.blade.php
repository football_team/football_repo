@extends(Template::name('frontend.%s._layout.layout'))
@section('style')
{{ HTML::style('ckeditor/contents.css') }}
@stop
@section('content')


@include(Template::name('frontend.%s._layout.account-tabs'))
<!---------main content------------>
<div class="accountarea">

     <div class="site-content">
         <!---------faz shortcode------------>
         <div class="row">
            <h2>{{trans('homepage.Addresses')}}</h2>
             <span class="ftp-line">
                <span class="greenline"></span>
                <span class="yellowline"></span>
                <span class="pinkline"></span>
                <span class="blueline"></span>
             </span>
         </div>
         <div class="row">

             <h3>{{trans('homepage.accMCADA')}}</h3>
             <span class="address_label">{{trans('homepage.accYSAAMCP')}}</span>

             <table class="responsivetable addresstable" >

                 <tr>
                     <td><strong>{{trans('homepage.fname')}}</strong></td>
                     <td class="firstname" data-firstname=""></td>
                 </tr>
                 <tr>
                     <td><strong>{{trans('homepage.sname')}}</strong></td>
                     <td class="lastname" data-lastname=""></td>
                 </tr>
                 <tr>
                     <td><strong>{{trans('homepage.Address')}}</strong></td>
                     <td class="street" data-street=""></td>
                 </tr>
                 <tr>
                     <td><strong>{{trans('homepage.Postcode')}}</strong></td>
                     <td class="postcode" data-postcode=""></td>
                 </tr>
                 <tr>
                     <td><strong>{{trans('homepage.City')}}</strong></td>
                     <td class="city" data-city=""></td>
                 </tr>
<!--                 <tr>-->
<!--                     <td><strong>County</strong></td>-->
<!--                     <td class="" >Mersydie</td>-->
<!--                 </tr>-->
                 <tr>
                     <td><strong>{{trans('homepage.Country')}}</strong></td>
                     <td class="country-id" data-country-id=""></td>
                 </tr>
                 <tr>
                     <td><strong>{{trans('homepage.Mobile')}}</strong></td>
                     <td class="mobile" data-mobile=""></td>
                 </tr>
             </table>

             <br />
             <div style="clear:both"></div>
             <a href="#" class="pull-left clearboth change-address">{{trans('homepage.Add Address')}}</a>

         </div>


         <!---------adresss----------->




         <!---------faz shortcode------------>


     </div>
</div>
<!---------main content------------>
@stop

{{
    Assets::setScripts(
    [
        'underscore'             => 'js/underscore.min.js'
    ], false, true);
}}

{{ Assets::jsStart() }}
<script type="text/javascript" charset="utf-8">

    (function ($) {
        var body = $('body');

        /* close modal */
        body.on('click','.close-reveal-modal', function (e) {
            e.preventDefault();
            var ElemId = $(this).closest('div.reveal-modal').attr('id');
            $('#'+ElemId).foundation('reveal', 'close');
            $('#'+ElemId).fadeOut('slow', function () {
                $('#'+ElemId).remove();
                $('.reveal-modal-bg').remove();
            });
        });

        body.on('click','.reveal-modal-bg', function (e) {
            e.preventDefault();
            var ElemId = body.find('div.reveal-modal').attr('id');
            $('#'+ElemId).foundation('reveal', 'close');
            $('#'+ElemId).fadeOut('slow', function () {
                $('#'+ElemId).remove();
                $('.reveal-modal-bg').remove();
            });
        })

    })(jQuery);
</script>

<script type="text/javascript" charset="utf-8">
    (function ($) {
        var body = $('body');
        var countries = {{ json_encode($country) }};
        Foundation.global.namespace = '';
        $('.addresstable').hide();
        $('document').ready(function () {
            /*Loading Address info */
            $.ajax({
                url: '/account/account-information/shipping',
                dataType: 'json',
                type: 'get',
                beforeSend: function () {

                },
                success: function (response) {
                    if(response.data) {
                        var data = response.data;
                        if(data.firstname) {

                            $('.firstname').data('firstname', data.firstname);
                            $('.firstname').html(data.firstname);

                            $('.lastname').data('lastname',data.lastname);
                            $('.lastname').html(data.lastname?data.lastname : "{{trans('homepage.Not set')}}");

                            $('.street').data('street', data.street);
                            $('.street').html(data.street);

                            $('.postcode').data('postcode', data.postcode);
                            $('.postcode').html(data.postcode);

                            $('.city').data('city', data.city);
                            $('.city').html(data.city);

                            $('.country-id').data('country-id', data.country_id);
                            $('.country-id').html(countries[data.country_id]);

                            $('.mobile').data('mobile', data.telephone);
                            $('.mobile').html(data.telephone);

                            $('.change-address').html("{{trans('homepage.Change Address')}}");
                            $('.addresstable').show();
                        }
                    }
                }
            }).done(function () {

            });
        });

        /* Billing Address modal open event*/
        $('.change-address').click(function (e) {
            e.preventDefault();
            var template = _.template($('#address-form-template').html());
            $('#address-form-modal').remove();
            var country_id = $('.country-id').data('country-id') != '' ? $('.country-id').data('country-id'): 'GB';



            body.append(template({
                firstname: $('.firstname').data('firstname'),
                lastname: $('.lastname').data('lastname'),
                street: $('.street').data('street'),
                country_id: country_id,
                postcode: $('.postcode').data('postcode'),
                city: $('.city').data('city'),
                mobile: $('.mobile').data('mobile'),
                countries: countries
            }));
            $('#address-form-modal').foundation('reveal', 'open');
        });

        /* Bank card save event */
        body.on('click','.billing-save', function (e) {
            e.preventDefault();
            var parent = $(this).closest('div.reveal-modal');

            // validate formt
            var isError = false;
            // validateAddressInput(
            //     [ 'firstname','lastname', 'street', 'country', 'postcode', 'city' , 'mobile']
            // );

            if(isError) {
                alert("{{trans('homepage.accPFIRF')}}");
                return false;
            }

            $.ajax({
                url: '/account/account-information/shipping',
                data: {
                    firstname: $('#firstname').val(),
                    lastname: $('#lastname').val(),
                    street: $('#street').val(),
                    country_id: $('#country').val(),
                    postcode: $('#postcode').val(),
                    city: $('#city').val(),
                    phone: $('#mobile').val()
                },
                dataType: 'json',
                type: 'post',
                success: function (response) {
                    if(response.data) {
                        var data = response.data;
                        if(data.firstname) {

                            $('.firstname').data('firstname', data.firstname);
                            $('.firstname').html(data.firstname);

                            $('.lastname').data('lastname',data.lastname);
                            $('.lastname').html(data.lastname?data.lastname : 'Not set');

                            $('.street').data('street', data.street);
                            $('.street').html(data.street);

                            $('.postcode').data('postcode', data.postcode);
                            $('.postcode').html(data.postcode);

                            $('.city').data('city', data.city);
                            $('.city').html(data.city);

                            $('.country-id').data('country-id', data.country_id);
                            $('.country-id').html(countries[data.country_id]);

                            $('.mobile').data('mobile', data.telephone);
                            $('.mobile').html(data.telephone);

                            $('.change-address').html('Change Address');
                            $('.addresstable').show();
                        }
                    }
                }

            }).done(function () {
                parent.find('.close-reveal-modal').trigger('click');
            });


        });
    })(jQuery);
</script>

<script type="text/x-template" id="address-form-template" charset="utf-8">
    <div id="address-form-modal" class="reveal-modal" data-reveal style="display:none">
        <h2>Shipping Address</h2>
        <p>
            <label for="firstname">{{trans('homepage.fname')}}</label>
            <input name="firstname" id="firstname" value="<%=firstname%>" type="text" />
        </p>
        <p>
            <label for="lastname">{{trans('homepage.sname')}}</label>
            <input name="lastname" id="lastname" value="<%=lastname%>" type="text" />
        </p>

        <p>
            <label for="street">{{trans('homepage.Address')}}</label>
            <input name="street" id="street" value="<%=street%>" type="text" />
        </p>

        <p>
            <label for="postcode">{{trans('homepage.Postcode')}}</label>
            <input name="postcode" id="postcode" value="<%=postcode%>" type="text" />
        </p>

        <p>
            <label for="city">{{trans('homepage.City')}}</label>
            <input name="city" id="city" value="<%=city%>" type="text" />
        </p>
        <p>
            <label for="country">{{trans('homepage.Country')}}</label>
            <select name="country" id="country">
                <% if(country_id == '')
                        country_id = 'GB';
                %>
                <% _.each(countries, function (val, key) { %>
                    <option value="<%=key%>" <%= key==country_id? 'selected': ''%> > <%=val%></option>
                <% }) %>
            </select>
        </p>
        <p>
            <label for="mobile">{{trans('homepage.Mobile')}}</label>
            <input name="mobile" id="mobile" value="<%=mobile%>" type="text" />
        </p>
        <p>
            <a href="#" class="button expand billing-save">{{trans('homepage.Save')}}</a>
        </p>
        <a class="close-reveal-modal">&#215;</a>
    </div>
</script>
{{ Assets::jsEnd() }}