<section class="banner-home">
    <span class="page-header buy-detail">
         <span class="gamename"> <strong>{{$node->title}}</strong> {{trans("homepage.Tickets")}}</span>
         <span class="gamedetail">{{ date('Y-m-d H:i:s ',strtotime($node->datetime)) }} {{$node->event_location}}</span>
     </span>
    <img class="inner-banner" src="/uploads/sell_tickets_page_1.jpg" alt="" />
    <span class="ftp-line">
    	<span class="greenline"></span>
        <span class="yellowline"></span>
        <span class="pinkline"></span>
        <span class="blueline"></span>
    </span>
</section>