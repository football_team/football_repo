@extends(Template::name('frontend.%s._layout.layout'))
@section('style')
{{ HTML::style('ckeditor/contents.css') }}
@stop
@section('content')


<!-- Todo: need to use partial for this ticket info display -->
@include(Template::name('frontend.%s.sell.partials.banner'))

<!---------main content------------>
<div class="row">



    <div class="site-content">




        <div class="row">
            <ul class="step-process">
                <li >
                    <span class="number">1</span>
                    <span class="selltitle">{{trans("homepage.Your Tickets")}}</span>
                </li>
                <li>
                    <span class="number">2</span>
                    <span class="selltitle">{{trans("homepage.Your Personal Details")}}</span>
                </li>
                <li class="active">
                    <span class="number">3</span>
                    <span class="selltitle">{{trans("homepage.Protection Guarantee")}}</span>
                </li>
            </ul>
        </div>


        <h3 class="sell-divider row">{{trans("homepage.Guarantee")}}</h3>

        <span class="bluehighlight row">
            <span class="guarenteeicon"></span>
            {{trans("homepage.sell3long1")}} <span class="blackhighlight">{{trans("homepage.100secure")}}</span>

        </span>

        <form name="ticket_guarantee" id="ticket-guarantee" action="/ticket/sell/agreement/{{$ticketId}}" method="post">
<!--        <div class="row selldivider credit-card-container">-->
<!---->
<!--            <p>Why do we need you credit card data?-->
<!--                <span data-tooltip aria-haspopup="true" class="has-tip whatsisthis round" title="?????">?</span>-->
<!--            </p>-->
<!---->
<!--        </div>-->




        <div class="row selldivider">

            <br/>
            <p>
                <strong>{{trans("homepage.sell3long2")}}</strong>
                <br/>
                {{trans("homepage.sell3long3")}}

            </p>


            <input type="radio" class="inline-radio" name="ticket_ready" value="1">
            <label class="inline-label" data-value="1">{{trans("homepage.yIHT")}}</label>

            <br/><br/>

            <input type="radio" class="inline-radio" name="ticket_ready" value="0" checked>
            <label class="inline-label" data-value="0">{{trans("homepage.nIDHT")}}</label>
            <br/><br/>

        </div>

        <div class="row selldivider">
            <br/><br/>
            <input type="checkbox" class="inline-radio" name="term_and_conditions" value="1">
            <label class="inline-label spacingforlabel">{{trans("homepage.I accept the")}} <a href="/terms-conditions" target="_blank">{{trans("homepage.Terms and Conditions")}}</a> {{trans("homepage.and")}} <a href="/privacy-policy" target="_blank">{{trans("homepage.Privacy Policy")}}</a>
            </label>
        </div>
        </form>
        <input type="button" class="btn bluebtn pull-right" value="PUBLISH">
        <a href="#"><span class="btn greybtn pull-right">{{trans("homepage.BACK")}}</span></a>
    </div>
</div>
@stop

{{ Assets::jsStart() }}
<script src="https://cdn2.footballticketpad.com/frontend/bm2014/js/jquery-form.min.js"></script>
<script src="https://cdn2.footballticketpad.com/frontend/bm2014/js/underscore.min.js"></script>
@include(Template::name('frontend.%s.sell.partials.customer-card'))

<script type="text/javascript" charset="utf-8">
    (function ($) {
        var body = $('body');

        /* close modal */
        body.on('click','.close-reveal-modal', function (e) {
            e.preventDefault();
            var ElemId = $(this).closest('div.reveal-modal').attr('id');
            $('#'+ElemId).foundation('reveal', 'close');
            $('#'+ElemId).fadeOut('slow', function () {
                $('#'+ElemId).remove();
                $('.reveal-modal-bg').remove();
            });
        });

        body.on('click','.reveal-modal-bg', function (e) {
            e.preventDefault();
            var ElemId = body.find('div.reveal-modal').attr('id');
            $('#'+ElemId).foundation('reveal', 'close');
            $('#'+ElemId).fadeOut('slow', function () {
                $('#'+ElemId).remove();
                $('.reveal-modal-bg').remove();
            });
        })

    })(jQuery);
</script>

<script>
    (function ($) {
        $(document).ready(function () {
            $.getAllCards();
        });

        $(document).ready(function () {

            $('input[value=PUBLISH]').click(function (e) {
                var submit = true;
                e.preventDefault();
//                if( $('.credit-card-container').find('input[type=radio]:checked').length < 1 ) {
//                    submit = false;
//                    alert('No credit card added!!!');
//                }

                if(!$('input[name=term_and_conditions]').prop('checked')) {
                    submit = false;
                    alert("{{trans('homepage.mustAcceptTerms')}}");
                }

                if (submit) {
                    $('form[name=ticket_guarantee]').submit();
                }

            });
        });
    })(jQuery)
</script>

<script>
    (function ($) {
       // wait for the DOM to be loaded
        $(document).ready(function() {
            var ticketContainer =  $('body');
            // bind 'myForm' and provide a simple callback function
            $('#ticket-guarantee').ajaxForm({

                beforeSubmit: function(arr, $form, options) {
                    $(".ajax-loading-modal").remove();
                    ticketContainer.append('<div class="ajax-loading-modal"></div>');
                    ticketContainer.addClass("loading");
                },
                success: function(response) {
                    ticketContainer.removeClass('loading');
                    if(response.message == "held")
                    {
                        var template = _.template($('#ticket-response-held').html());
                        $('.site-content').html(template());
                    }
                    else if ( response.message  ) {
                        var template = _.template($('#ticket-reponse').html());
                        $('.site-content').html(template({reference_number:response.message}));
                    }
                }

            });


            $('.inline-label').click(function (e) {
                if($(this).data('value') == 0 || $(this).data('value') == 1) {
                    var self = $(this);
                    $('input[name="ticket_ready"]').prop('checked', false);
                    $('input[name="ticket_ready"]').each(function() {
                        if($(this).val() == self.data('value')) {
                            $(this).prop('checked', true);
                        }
                    });
                }
            });

        });
    })(jQuery)
</script>

<script type="text/x-template" id="ticket-response-held">
    <div class="row">
        <h2>{{trans("homepage.Ticket Confirmation")}}</h2>
        <p>
            We have placed your tickets on hold, we will need to contact you to verify some details before we list your tickets for sale.
        </p>
    </div>
</script>

<script type="text/x-template" id="ticket-reponse">
    <div class="row">
        <h2>{{trans("homepage.Ticket Confirmation")}}</h2>
        <p>
            {{trans("homepage.sell3long4")}}
            <br />
            {{trans("homepage.sell3long5")}} <%=reference_number%>
            <br />
            {{trans("homepage.sell3long6")}} <a href="/account/listing" target="_blank"> {{trans("homepage.sell3macc")}} </a> {{trans("homepage.sell3long7")}} <a href="/faq" target="_blank"> {{trans("homepage.FAQ")}} </a> {{trans("homepage.section.")}}
            <br />
        </p>
    </div>
</script>
{{ Assets::jsEnd() }}