@extends(Template::name('frontend.%s._layout.layout'))
@section('style')
	{{ HTML::style('ckeditor/contents.css') }}
@stop
@section('content')

	@include(Template::name('frontend.%s.sell.partials.banner'))



	<form name="ticket_sell" action="" method="post" >
		<input name="event_id" type="hidden" value="{{$node->id}}" >
		<div class="row" >
			<div class="site-content" >
				<div class="row" >
					<ul class="step-process" >
						<li class="active" >
							<span class="number" >1</span >
							<span class="selltitle" >{{trans("homepage.Your Tickets")}}</span >
						</li >
						<li >
							<span class="number" >2</span >
							<span class="selltitle" >{{trans("homepage.Your Personal Details")}}</span >
						</li >
						<li >
							<span class="number" >3</span >
							<span class="selltitle" >{{trans("homepage.Protection Guarantee")}} </span >
						</li >
					</ul >
				</div >
				<h3 class="sell-divider row" >{{trans("homepage.DescTickets")}}</h3 >
				<div class="row selldivider" >
					<div class="label bluelabel" >{{trans("homepage.Event")}}</div >
					<div class="fields" >
						<strong >{{$node->title}}</strong > <br /><br />
						{{ date('Y-m-d H:i:s ',strtotime($node->datetime)) }} {{$node->event_location}}
					</div >
				</div >
				<div class="row selldivider" >
					<div class="label bluelabel" >{{trans("homepage.Number of Tickets")}}</div >
					<div class="fields" >
						<select name="number_of_ticket" required >
							<option value="" >{{trans("homepage.fSelect")}}</option >
							@for($i=1; $i<=20; $i++)
								<option value="{{$i}}" >{{$i}}</option >
							@endfor
						</select >
						<small >{{trans("homepage.sell1long1")}}</small >
					</div >
				</div >
				<div class="row selldivider" >
					<div class="label bluelabel" >{{trans("homepage.Form of Ticket")}}</div >
					<div class="fields" >
						<select name="form_of_ticket" required >
							<option value="" > {{trans("homepage.fSelect")}} </option >
							@foreach($node->getFormOfTickets() as $type)
								<option value="{{$type->id}}" > {{trans('homepage.'.$type->title)}} </option >
							@endforeach
						</select >
						<small >{{trans("homepage.sell1long2")}}</small >
					</div >
				</div >
				<div class="row selldivider" >
					<div class="label bluelabel" >{{trans("homepage.Ticket Type")}}</div >
					<div class="fields" >
						<select name="ticket_type" @if(!empty($stadium))onChange="testRunThis()"@endif required >
							<option value="" > {{trans("homepage.fSelect")}} </option >
							@foreach($node->getTicketTypes() as $type)
								<option data-clean="{{urlencode($type->title)}}" value="{{$type->id}}" > {{$type->title}} </option >
							@endforeach
						</select >
					</div >
				</div >
				<div class="row selldivider" >
					<div class="label bluelabel" >{{trans("homepage.sellCanHD")}}</div >
					<div class="fields" >
						<input type="radio" name="hand_delivery" value="1" class="" >
						<label class="inline-label hand_delivery" >{{trans("homepage.Yes")}}</label >
						<input type="radio" name="hand_delivery" value="0" class="" checked >
						<label class="inline-label hand_delivery" >{{trans("homepage.No")}}</label >
					</div >
				</div >
				<div class="row selldivider" >
					<div class="label bluelabel" >{{trans("homepage.Location")}}</div >
					<div class="fields" >
						<label class="inline-label" >{{trans("homepage.Block")}}</label >
						<input class="inline-input input loc_block" type="text" name="loc_block" value="" />
						<!--label class="inline-label" >{{trans("homepage.Row")}}</label -->
						<input class="inline-input input" type="hidden" name="loc_row" value="" />
						<small >{{trans("homepage.sell1long3")}}</small >
						<div class="row nomargin" >
							<label class="inline-label ticketrestrict" >{{trans("homepage.sell1as")}}</label >
							<input type="radio" name="restriction_option restrict-q" value="1" class="restriction-option" >
							<label class="inline-label restrict-answer" >{{trans("homepage.Yes")}}</label >
							<input type="radio" name="restriction_option restrict-q" value="0" class="restriction-option restriction-option-no" ckecked >
							<label class="inline-label restrict-answer" >{{trans("homepage.No")}}</label >
						</div >
						<div class="restrictions-yes" style="display: none" >
							<ul >
								@foreach($node->getSelectedRestrictions() as $type)
									<li >
										<input type="checkbox" value="{{$type->id}}" name="restrictions[{{$type->id}}]" >
										<label >{{$type->title}}</label ></li >
								@endforeach
							</ul >
							<!--label class="inline-label" >{{trans("homepage.Other information")}}</label -->
							<textarea name="restrictions[others]" style="display:none !important;"></textarea >
							<small >{{trans("homepage.sell1long4")}}</small >
						</div >
					</div >
             <span class="notification-green" >
                        <span class="infoicon" ></span >
				 {{trans("homepage.sell1long5")}}
              </span >
				</div >
				<h3 class="sell-divider row" >{{trans("homepage.Sale Definition")}}</h3 >
				<div class="row selldivider" >
					<div class="label bluelabel" >{{trans("homepage.accMEarnings")}}
						({{trans("homepage.currencyInUse")}}){{trans('homepage.currencyAfter')}}
					</div >
					<div class="fields" >
						<input class="inline-input input" placeholder="{{trans('homepage.Enter price')}}" type="text" name="price" required >
					</div >
				</div >
				<div class="row selldivider" >
					<div class="label bluelabel" >{{trans("homepage.sell1howsell")}}</div >
					<div class="fields" >
						<select name="sell_preference" required >
							<option value="" > {{trans("homepage.fSelect")}} </option >
							<option value="0" >{{trans("homepage.trf4")}}</option >
							<option value="1" >{{trans("homepage.All together")}}</option >
							<option value="2" >{{trans("homepage.AvoidLeaving1")}}</option >
							<option value="3" >{{trans("homepage.In pairs")}}</option >
						</select >
					</div >
				</div >
				<h3 class="sell-divider row price-review" style="display: none" >{{trans("homepage.Price Review")}}</h3 >
				<div class="row selldivider" >
					<label class="inline-label" >{{trans("homepage.sell1LF")}}</label >
					<span class="blueprice review-amount" ></span >
				</div >
				<div class="row selldivider" >
					<table class="responsivetable" >
						<thead >
						<th >&nbsp;</th >
						<th >{{trans("homepage.Maximum Price")}}</th >
						<th >{{trans("homepage.Average Price")}}</th >
						<th >{{trans("homepage.Minimum Price")}}</th >
						</thead >
						<tbody >
						@if ($ticketAggregatedPrice)
							<tr >
								<td ><strong >{{trans("homepage.sell1CSF")}}</strong ></td >
								<td >{{trans("homepage.currencyInUse")}}{{number_format ($ticketAggregatedPrice->max_price, 2)}} {{trans('homepage.currencyAfter')}}</td >
								<td >{{trans("homepage.currencyInUse")}}{{number_format ($ticketAggregatedPrice->avg_price, 2)}} {{trans('homepage.currencyAfter')}}</td >
								<td >{{trans("homepage.currencyInUse")}}{{number_format ($ticketAggregatedPrice->min_price, 2)}} {{trans('homepage.currencyAfter')}}</td >
							</tr >
						@else
							<tr >
								<td ><strong >{{trans("homepage.sell1CSF")}}</strong ></td >
								<td >-</td >
								<td >-</td >
								<td >-</td >
							</tr >
						@endif
						</tbody >
					</table >
				</div >
				<input name="submit" type="submit" class="btn blubtn pull-right" value="{{trans('homepage.CONTINUE')}}" >
			</div >
		</div >
	</form >

@stop

{{
    Assets::setScripts(
    [
        'underscore'             => 'js/underscore.min.js',
        'jquery-validator'       => 'js/jquery-validator/jquery.validate.min.js',
        'jquery-form'            => 'js/jquery-form.min.js'
    ], false, true);
}}
{{ Assets::jsStart() }}
<script type="text/javascript" >
	(function ($) {
		$(document).ready(function () {

			$('.restriction-option').click(function (e) {
				if ($(this).val() == 1) {
					$(".restrictions-yes").css({display: "block"});
				} else {
					$(".restrictions-yes").css({display: "none"});
				}
			});

			$('.restriction-option-no').prop('checked', true);

			$('input[name=price]').change(function (e) {
				e.preventDefault();
				if (parseFloat($(this).val()) > 0) {
					var price = parseFloat($(this).val());
					$(this).val(price.toFixed(2));
					price = ((price/85)*100) * {{1 + ($bookingPerc/100)}}
					$('.price-review').css({display: 'block'});
					$('.review-amount').html("&nbsp; {{trans('homepage.currencyInUse')}}" + price.toFixed(2));


					$('input[name=submit]').css({display: 'initial'});
					$('input[name=price]').css({borderColor: '#ccc'});
				} else {
					$('input[name=submit]').css({display: 'none'});
					$('input[name=price]').css({borderColor: '#f00'});
				}
			});
			$('input[name=price]').focusout(function (e) {
				if (parseFloat($(this).val()) <= 0) {
					alert('{{trans("homepage.sell1long6")}}');
				}
			});

			var validator = '';

			$('input[type=submit]').click(function () {
				validator = $('form[name=ticket_sell]').validate();
			});
			$('form[name=ticket_sell]').submit(function () {
				validator = $('form[name=ticket_sell]').validate();
				return validator.form();
			});

		});
	})(jQuery)
</script >
<script >
	@if(!empty($stadium))
	function testRunThis() {

		var currentCat = jQuery('body > form > div > div > div:nth-child(6) > div.fields > select  option:selected').text().toLowerCase().trim().replace("/","%2F").replace("/","%2F").replace(" ", "").replace(" ", "").replace(" ", "").replace(" ", "");
		$.ajax({
			@if(!empty($node->stadium))
			url: "/map/naming/json/{{urlencode($node->stadium)}}/" + encodeURI(currentCat),
			@else
			url: "/map/naming/json/{{urlencode($stadium)}}/" + encodeURI(currentCat),
			@endif
			context: document.body,
			beforeSend: function( xhr ) {

				jQuery('.removeLaters').remove();
				jQuery('body > form > div > div > div:nth-child(6) > div.fields').append('<div class="removeLaters" style="background:#CE2B2B; padding:10px; color:white;">( Loading in Block IDs )</div>');
			}
		}).done(function (data) {
			var thisxx = data;
			var objectlist = "<select class='inline-input input loc_block' name='loc_block'><option value=''>Block Unknown</option>";
			var arr = [];

			for (var x in thisxx) {
				arr.push(thisxx[x]);
			}
			jQuery(arr).each(function (e) {
				objectlist = objectlist + "<option>" + this + "</option>";
			});
			objectlist = objectlist + "</select>";
			console.log(objectlist);
			jQuery('.loc_block').replaceWith(objectlist);
			jQuery('.removeLaters').remove();
			jQuery('body > form > div > div > div:nth-child(6) > div.fields').append('<div class="removeLaters" style="background:#7EC26D; padding:10px; color:white;">( Seat Blocks have loaded )</div>');

		}).fail(function (data) {
			jQuery('body > form > div > div > div:nth-child(6) > div.fields').append('<div class="removeLaters" style="background:#CE2B2B; padding:10px; color:white;">( Unable to load in the blocks,  Send us an email info@footballtickpad.com )</div>');


		});


	}
	@endif
</script >
{{ Assets::jsEnd() }}