<div class="framed overviewMobile" >
	<input name="qty" class="qty" value="2" type="hidden" >
	<div >
		<span class="ftp-line" >
    	<span class="greenline" ></span >
        <span class="yellowline" ></span >
        <span class="pinkline" ></span >
        <span class="blueline" ></span >
    </span >
		<div >
			<h3 style="font-weight: 700; border:none !important;" >
				{{$ticket->title}}
			</h3 >
		</div >
		<div >
			<span class="ticketq" ></span >
			x {{trans('homepage.currencyInUse')}}{{number_format(($ticket->ticket->ticketInformation->price)*1.1, 2)}} {{trans('homepage.currencyAfter')}}
		</div >
		<div clas="checkoutMaringSperarateLine" >
		<div class="currencyBlockCheckout" >	{{trans('homepage.currencyInUse')}}</div><div class="currencyCostInprice">0 : Free standard delivery</div>
		</div >
		<div style="font-weight: bold;font-size: 20px;padding-top: 5px;     clear: both;" >
			Final Price :
			<div class="afterTxt" style="float:right; font-weight: bold; font-size: 20px; padding-top: 5px;" >&nbsp;{{trans('homepage.currencyAfter')}}</div >
			<div style="float: right ; font-weight: bold;font-size: 20px;padding-top: 5px;" class="ticketp" ></div >
			<div class="currex" style="float: right;font-size: 20px;margin: 5px;" >{{trans('homepage.currencyInUse')}}</div >
		</div >
	</div >
</div >