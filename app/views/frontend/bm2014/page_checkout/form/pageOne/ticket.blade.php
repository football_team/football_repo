<div class="" style="
    width: 100% !important;
    float: left;
" >
	<div style="
    width: 100%;
    float: right;
    text-align: center;
    margin-left: auto;
    margin-right: auto;
    /* box-shadow: 1px 3px 0px 1px #F2F2F2; */
    margin-top: 5px;
    border-radius: 4px;
    /* border-left: 10px solid #1BA1E2; */
    color: black;
    padding: 0 !important;
" >
		<div class="ticketBody" >
			@if(!empty($ticket->info['loc_block']) && $ticket->info['loc_block'] != "" && $ticket->info['loc_block'] != " ")
				<p >Block : {{$ticket->info['loc_block']}}</p >@else
				<p ></p >@endif
			@if(!empty($ticket->info['loc_row']) && $ticket->info['loc_row'] != "" && $ticket->info['loc_row'] != " ")
				<p >Row : {{$ticket->info['loc_row']}}</p >@else
				<p ></p >@endif
			<p >{{trans('homepage.currencyInUse')}}{{number_format(($ticket->ticket->ticketInformation->price)*1.1, 2)}}  each</p >
			<p >Form Of Ticket : {{$ticket->formOfTicket['title']}}</p >
			<p ></p >
			<p ></p >
			<p ></p >
		</div >
		<div class="ticketBodyRight" >
			<div class="col-xs-6" >{{$ticket->title}}</div >
			<div class="col-xs-6" >{{$ticket->event_location}}</div >
			<div class="col-xs-6" >{{date('d-m-y, h:ia', strtotime($ticket->datetime))}}</div >
			<div class="col-xs-6" >{{$ticket->ticketType['title']}} Seats</div >
			<div >Number of tickets
				<select style="background: darkgrey;border-radius: 15px;margin-top: 10px;float: right;width: 50%;margin-top: -1px;" name="number_of_ticket" class="number-of-ticket-chk" data-preferance="{{$sell_preference}}" data-qty="{{$ticket->available_qty}}" >
					@foreach($ticketQty as $key=>$val)
						@if(isset($_GET['n']) && $_GET['n'] == $key )
							<option value="{{$key}}" selected >{{$val}}</option >
						@else
							<option value="{{$key}}" >{{$val}}</option >
						@endif

					@endforeach
				</select >
			</div >
			<div class="col-x-12 " STYLE="    font-size: 10PX;
				MARGIN-TOP: 21PX;">@if(!empty($ticket->buyerNot)){{$ticket->buyerNot }}@endif</div>
		</div >
	</div >
	<div class="outerTicketBlock" >
		<input type="text" name="voucherCode" id="discountField" placeholder="Apply discount code">
		<div class="lowerTicketBlock" >Checkout
		</div >
	</div >
</div >