
<div style="display:none;" class="hotelAddress" >
	<div class="col-xs-12 col-md-6 leftSdeInput" >
		<div >Reservation Name</div >
		{{ Form::text('reservation_name', '', ['class'=>' reservation_name', 'placeholder' => trans('homepage.reservation_name'), 'required'=>false ]) }}
	</div >
	<div class="col-xs-12 col-md-6 rightSideInput" >
		<div >Hotel Name</div >
		{{ Form::text('hotel_name', '', ['class'=>' hotel_name', 'placeholder' => trans('homepage.Hotel Name'), 'required'=>false ]) }}
	</div >
	<div class="col-xs-12 col-md-6 leftSdeInput" >
		<div >Checkin Date</div >
		{{ Form::text('checkin_dt', '', ['class'=>' checkin_dt', 'placeholder' => trans('homepage.Checkin Date'), 'required'=>false ]) }}
	</div >
	<div class="col-xs-12 col-md-6 rightSideInput" >
		<div >Checkout Date</div >
		{{ Form::text('checkout_dt', '', ['class'=>' checkout_dt', 'placeholder' => trans('homepage.Checkout Date'), 'required'=>false ]) }}
	</div >
</div >