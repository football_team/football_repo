<h2 class="billingAddressTitle" style="display:none;" >What is your billing address</h2 >
<div style="opacity:0; display:none;" class="billingAll" >
	<div class="col-xs-12 col-md-6 leftSdeInput" >
		<div >Street Address</div >
		{{ Form::text('street_bill', '', ['class'=>' street', 'placeholder' => trans('homepage.Address'), 'required'=>true ]) }}
	</div >
	<div class="col-xs-12 col-md-6 rightSideInput" >
		<div >Postcode</div >
		{{ Form::text('postcode_bill', '', ['class'=>' postcode', 'placeholder' => trans('homepage.Postcode'), 'required'=>true ]) }}
	</div >
	<div class="col-xs-12 col-md-6 leftSdeInput" >
		<div >City</div >
		{{ Form::text('city_bill', '', ['class'=>' city', 'placeholder' => trans('homepage.City'), 'required'=>true ]) }}
	</div >
	<div class="col-xs-12 col-md-6 rightSideInput" >
		<div >Country Code</div >
		{{ Form::select('country_id_bill', $_country, 'GB', ['class'=>' country-code input', 'placeholder' => trans('homepage.Country Code'), 'id'=>'country', 'required'=>true] ) }}
	</div >
</div >