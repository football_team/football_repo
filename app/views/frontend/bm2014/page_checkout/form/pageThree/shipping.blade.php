<div class="deliveryCountryBlock" style="display:none;">
    <h2>Choose your delivery country</h2>
    @if(trans('homepage.countrycodefefault') != 'homepage.countrycodefefault')
    {{ Form::select('shipping_country', $_country, trans('homepage.countrycodefefault'),['class'=>'input col-md-12', 'placeholder' => trans('homepage.Country Code'), 'id'=>'country', 'required'=>true] ) }}
    @else
        {{ Form::select('shipping_country', $_country, 'GB', ['class'=>'input country-code', 'placeholder' => trans('homepage.Country Code'), 'id'=>'country', 'required'=>true] ) }}
    @endif
</div>
<div class="deliveryBlock" style="display:none;">
    <h2 class="delTitle">How should we deliver?</h2>
    <select class="countrieslist col-md-12" name="other_country"
            data-delivery-selected="{{ isset($_GET['d'])? filter_var($_GET['d'], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH): ''}}">
        <option value="" data-price="0.00"> {{trans('homepage.fSelect')}} </option>
    </select>
    @include('frontend.bm2014.page_checkout.form.pageThree.hotel')

    <div class="col-xs-12 col-md-6 leftSdeInput">
        <div>Street Address</div>
        {{ Form::text('street', '', ['class'=>' street', 'placeholder' => trans('homepage.Address'), 'required'=>true ]) }}
    </div>
    <div class="col-xs-12 col-md-6 rightSideInput">
        <div>Postcode</div>
        {{ Form::text('postcode', '', ['class'=>' postcode', 'placeholder' => trans('homepage.Postcode'), 'required'=>true ]) }}
    </div>
    <div class="col-xs-12 col-md-6 leftSdeInput">
        <div>City</div>
        {{ Form::text('city', '', ['class'=>' city', 'placeholder' => trans('homepage.City'), 'required'=>true ]) }}
    </div>
    <div class="col-xs-12 col-md-6 rightSideInput">
        <div>Country Code</div>
        @if(trans('homepage.countrycodefefault') != 'homepage.countrycodefefault')
            {{ Form::select('country_id', $_country, trans('homepage.countrycodefefault'), ['class'=>'input country-code', 'placeholder' => trans('homepage.Country Code'), 'id'=>'country', 'required'=>true] ) }}
        @else
            {{ Form::select('country_id', $_country, 'GB', ['class'=>'input country-code', 'placeholder' => trans('homepage.Country Code'), 'id'=>'country', 'required'=>true] ) }}
        @endif
    </div>
</div>