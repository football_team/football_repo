<div class="col-xs-12 col-md-6 leftSdeInput" >
	<div  class="col-xs-12">First Name</div >
	<input class="col-xs-12 col-md-12 validCheck1" minlength="2" type="text" value="@if(!empty($customer['firstname'])){{ $customer['firstname'] }}@endif" placeholder="First Name" name="first_name" aria-required="true" required="1" >
</div >
<div class="col-xs-12 col-md-6 leftSdeInput" >
	<div  class="col-xs-12">Last name</div >
	<input class="col-xs-12 col-md-12 validCheck2" type="text" value="@if(!empty($customer['lastname'])){{ $customer['lastname'] }}@endif" placeholder="Last Name" name="last_name" aria-required="true" required="1" >
</div >
<div class="col-xs-12 col-md-6 leftSdeInput" >
	<div  class="col-xs-12">Email</div >
	@if(empty($customer['email']))
		{{ Form::email('email', ' ', ['class'=>'col-xs-12 col-md-12 validCheck3 emailCopy', 'placeholder' => trans('homepage.Email') ,'required'=>true]) }}
		@else
		{{ Form::email('email', $customer['email'], ['class'=>'col-xs-12 col-md-12 validCheck3 emailCopy', 'placeholder' => trans('homepage.Email') ,'required'=>true]) }}
	@endif
</div >
<div class="col-xs-12 col-md-6 leftSdeInput" >
	<div  class="col-xs-12">Email confirm</div >
	@if(empty($customer['email']))'
		{{ Form::email('email_confirmation', '', ['class'=>'col-xs-12 col-md-12 validCheck4', 'placeholder' => "Confirm email",'required'=>true,'equalTo'=>'.emailCopy' ]) }}
	@else
		{{ Form::email('email_confirmation', $customer['email'], ['class'=>'col-xs-12 col-md-12 validCheck4', 'placeholder' => 'Confirm email','required'=>true,'equalTo'=>'.emailCopy' ]) }}
	@endif
</div >
<div class="col-xs-12 col-md-6 leftSdeInput" >
	<div  class="col-xs-12">Phone number</div >
	{{ Form::select('country_code', $country_with_phone, '0044', ['class'=>'col-xs-12 col-md-12 input country-code validCheck5', 'placeholder' => trans('homepage.Country Code'), 'required'=>true ] ) }}
</div >
<div class="col-xs-12 col-md-6 leftSdeInput" >
	<div  class="col-xs-12">Numbers Only Please </div >

	@if(empty($telephone))
		{{ Form::text('contact_no', '', ['class'=>'col-xs-12 col-md-12 validCheck6',"digits"=>"true", 'placeholder' => trans('homepage.Mobile Number'), 'required'=>true ]) }}
	@else
		{{ Form::text('contact_no', $telephone, ['class'=>'col-xs-12 col-md-12 validCheck6',"digits"=>"true", 'placeholder' => trans('homepage.Mobile Number'), 'required'=>true ]) }}
	@endif
</div >
<div class="col-md-12 errorBox" >
</div >
<div class="col-xs-6 bckbuttonHolder" >
	<p class="backToPage1 backButton col-xs-12" >Back</p >
</div >
<div class="col-xs-6 nextButtonHolder" >
	<p class="nextToPage3 nextButton col-xs-12" >Next</p >
</div >