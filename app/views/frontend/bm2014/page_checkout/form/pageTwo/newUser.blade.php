<div class="col-xs-12 col-md-6 leftSdeInput" >
	<div  class="col-xs-12">First name</div >
	<input class="col-xs-12 col-md-12 validCheck1" minlength="2" type="text" value="@if(!empty($customer['firstname'])){{ $customer['firstname'] }}@endif" placeholder="First Name" name="first_name" aria-required="true" required="1" >
</div >
<div class="col-xs-12 col-md-6 leftSdeInput" >
	<div  class="col-xs-12">Last name</div >
	<input class="col-xs-12 col-md-12 validCheck2" type="text" value="@if(!empty($customer['lastname'])){{ $customer['lastname'] }}@endif" placeholder="Last Name" name="last_name" aria-required="true" required="1" >
</div >
<div class="col-xs-12 col-md-6 leftSdeInput" >
	<div  class="col-xs-12">Email</div >
	{{ Form::email('email', '', ['class'=>'col-xs-12 col-md-12 validCheck3 emailCopy', 'placeholder' => trans('homepage.Email') ,'required'=>true]) }}
</div >
<div class="col-xs-12 col-md-6 leftSdeInput" >
	<div  class="col-xs-12">Email confirm</div >
	{{ Form::email('email_confirmation', '', ['class'=>'col-xs-12 col-md-12 validCheck4', 'placeholder' => trans('homepage.Confirm Email'),'required'=>true,'equalTo'=>'.emailCopy' ]) }}
</div >
<div class="col-xs-12 col-md-6 leftSdeInput" >
	<div  class="col-xs-12">Phone number</div >
	{{ Form::select('country_code', $country_with_phone, '0044', ['class'=>'col-xs-12 col-md-12 input country-code validCheck5', 'placeholder' => trans('homepage.Country Code'), 'required'=>true ] ) }}
</div >
<div class="col-xs-12 col-md-6 leftSdeInput" >
	<div  class="col-xs-12">Numbers Only Please</div >
	{{ Form::text('contact_no', '', ["minlength"=>"8","digits"=>"true",'class'=>'col-xs-12 col-md-12 validCheck6', 'placeholder' => trans('homepage.Mobile Number'), 'required'=>true ]) }}
</div >
<div class="col-xs-12 col-md-6 leftSdeInput" >
	<div  class="col-xs-12">Password ( 8 characters or longer )</div >
	{{ Form::password('password', ['class'=>'passwordToPass',"minlength"=>"8","required"=>true]) }}</div >
<div class="col-xs-12 col-md-6 leftSdeInput" >
	<div  class="col-xs-12 col-md-6">Password confirm</div >
	{{ Form::password('password_confirmation', ['class'=>'passwordToPassSec',"minlength"=>"8","required"=>true]) }}
</div >
<div class="col-md-12 errorBox" >
</div >
<div class="col-md-6 col-xs-6 bckbuttonHolder" >
	<p class="backToUserList backButton col-xs-12" >Back</p >
</div >
<div class="col-md-6 col-xs-6 nextButtonHolder" >
	<p class="nextToPage3 nextButton col-xs-12" >Next</p >
</div >