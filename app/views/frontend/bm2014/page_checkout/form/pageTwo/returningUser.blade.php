<form action="{{route('customer.account.checkout.login')}}" method="post" >
	<div class="col-md-6 leftSdeInput" >
		<div class="col-xs-12" >Email</div >
		<input class="col-xs-12 col-md-12 validCheck1" minlength="2" type="text" value="" placeholder="username" id="username" name="username" aria-required="true" required="1" >
	</div >
	<div class="col-md-6 leftSdeInput" >
		<div class="col-xs-12" >Password</div >
		<input class="col-xs-12 col-md-12 validCheck2" type="password" value="" placeholder="password" id="password" name="password" aria-required="true" required="1" >
	</div >
	<div class="col-md-6 bckbuttonHolder" >
		<p class="backToUserList backButton col-md-12" >Back</p >
	</div >
	<div class="col-md-6 nextButtonHolder" >
		<input type="button" style="width:100% !important;" class="pillbutton greenPillButton pull-right login-btn" value="{{trans('homepage.login')}}" >
	</div >
</form >