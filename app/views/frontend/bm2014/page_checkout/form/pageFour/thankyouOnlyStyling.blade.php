{{
				Assets::setScripts(
				[
				'serializejson'          => 'js/jquery-form.min.js',
				], false, true);
				}}
<style >
	.checkoutstep.active {
		padding: 15px;
		width: 100%;
		background: #7EC26D;
		color: white;
		font-size: 13px !important;
		font-weight: 500;
		border-radius: 5px;
	}

	.checkoutstep {
		padding: 15px;
		width: 100%;
		background: #FFD05C;
		color: white;
		font-size: 13px !important;
		font-weight: 500;
		border-radius: 5px;
	}

	.checkoutStepOuter {
		width: 25%;
		float: left;
		padding: 5px;
	}

	style {
	}

	#discountField {
		max-width: 70% !important;
		border-radius: 20px 0 0 20px !important;
		-webkit-border-radius: 20px 0 0 20px !important;
		display: inline-block !important;

	}

	#centerthis {
		margin-left: auto;
		margin-right: auto;
		max-width: 622px !important;
		text-align: center;
	}

	#centerthis #discountGo {
		margin-top: -1px;
		height: 42px !important;
	}
</style >