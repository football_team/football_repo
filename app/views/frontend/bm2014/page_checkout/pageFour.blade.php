@extends(Template::name('frontend.%s._layout.layout'))
@section('style')
	@include('frontend.bm2014.page_checkout.style')
@stop
@section('content')
	<form action="{{route('ticket.checkout.order', array('id'=>$productId))}}" method="post" id="ticket-checkout-form" >
		<div class="row col-xs-12" >
			<div class="container" >
				<div class="col-md-7" style="padding-right: 30px;" >
					@if(Options::getOption('payment_gateway') == 'tp')
						<iframe style="overflow-y: hidden;" height="600" scrolling="no" seamless="seamless" width="100%" src="{{Config::get('transactpro.gatewayUrl')}}{{$tid}}" frameborder="0" allowfullscreen ></iframe >
					@elseif(Options::getOption('payment_gateway') == 'g2s' && isset($redirect_link))
						<iframe style="overflow-y: hidden;" height="1450" scrolling="no" seamless="seamless" width="100%" src="{{$redirect_link}}" frameborder="0" allowfullscreen ></iframe >
					@endif;
				</div >
				<div class="col-md-4" >
					@include('frontend.bm2014.page_checkout.form.sidebar.oderTotal')
					@include('frontend.bm2014.page_checkout.form.sidebar.peopleOnPage')
					@include('frontend.bm2014.page_checkout.form.sidebar.timer')
					@include('frontend.bm2014.page_checkout.trust')
				</div >
			</div >
		</div >
		</div>
	</form >
@stop
@section('script')
	@parent()
	@include('frontend.bm2014.page_checkout.js.includes')
	@include('frontend.bm2014.page_checkout.js.clock')
	@include('frontend.bm2014.page_checkout.js.movingpages')
	@include('frontend.bm2014.page_checkout.js.validation')
	@include('frontend.bm2014.page_checkout.js.login')
	@include('frontend.bm2014.page_checkout.js.submitform')
@stop
