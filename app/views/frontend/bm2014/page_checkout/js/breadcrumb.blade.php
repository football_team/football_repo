<div class="checkoutarea hidden-xs">
    <div class="checkoutStepOuter
            ">
        <div class="checkoutstep active step1 hidden-xs">1. Your Ticket
        </div>
    </div>
    <div class="checkoutStepOuter
            ">
        <div class="checkoutstep step2 hidden-xs">2. About You
        </div>
    </div>
    <div class="checkoutStepOuter
            ">
        <div class="checkoutstep step3 hidden-xs">3. Shipping
        </div>
    </div>
    <div class="checkoutStepOuter
            ">
        <div class="checkoutstep step4 hidden-xs">4. Billing
        </div>
    </div>

</div>
<div class="checkoutarea visible-xs">
    <div class="checkoutStepOuter">
        <div class="checkoutstep mobilestep active step1 visible-xs" style="font-size:10px;">Your Ticket
        </div>
    </div>
    <div class="checkoutStepOuter">
        <div class="checkoutstep mobilestep step2 visible-xs" style="font-size:10px;">About You
        </div>
    </div>
    <div class="checkoutStepOuter">
        <div class="checkoutstep mobilestep step3 visible-xs" style="font-size:10px;">Shipping
        </div>
    </div>
    <div class="checkoutStepOuter">
        <div class="checkoutstep mobilestep step4 visible-xs" style="font-size:10px;">Billing
        </div>
    </div>

</div>
<style>
    .checkoutstep.active {
        padding: 15px;
        width: 100%;
        background: #7EC26D;
        color: white;
        font-size: 13px !important;
        font-weight: 500;
        border-radius: 5px;
    }

    .checkoutstep {
        padding: 15px;
        width: 100%;
        background: #FFD05C;
        color: white;
        font-size: 13px !important;
        font-weight: 500;
        border-radius: 5px;
    }

    .checkoutStepOuter {
        width: 25%;
        float: left;
        padding: 5px;
    }

    .checkoutstep.mobilestep{
        padding: 15px;
        width: 100%;
        font-size: 13px !important;
        font-weight: 500;
        border-radius: 5px;
        text-align: center;
        padding: 0;
        padding: 20px;
        vertical-align: middle;
        font-size: 9px !important;
    }
</style>