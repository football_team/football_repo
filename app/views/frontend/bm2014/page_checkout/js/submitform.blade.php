<script >

	var body = $('body');
	var ticketContainer = $('body');
	$(document).ready(function () {

		var isUserFalse = false;
		var form = jQuery('#ticket-checkout-form');
		form.ajaxForm({

			beforeSubmit: function (arr, $form, options) {
				$(".ajax-loading-modal").remove();

				if (form.valid()) {
					ticketContainer.append('<div class="ajax-loading-modal"></div>');
					ticketContainer.addClass("loading");
					return true;
				} else {
					return false;
				}
			},

			success: function (response) {

				@if (empty($customer))
				if (!isUserFalse) {
					var resp = response;
					$(window).unbind('beforeunload');
					$.ajax({
						url: '/customer/account/login',
						data: {
							login: {
								username: $('.emailCopy').val(),
								password: $('.passwordToPass').val()
							}
						},
						type: 'POST',
						dataType: 'json',
						success: function (response) {
							window.onbeforeunload = null;
							ticketContainer.removeClass('loading');
							isUserFalse = true;
							var n = $('.number-of-ticket-chk').val();
							var d = $('.countrieslist').val();
							$(window).unbind('beforeunload');
							window.location.replace(resp.link);
						},
						error: function (response) {
							ticketContainer.removeClass('loading');
							body.removeClass("loading");
							jQuery('.errorZone').css('display','block');
							jQuery('.errorZone').text(response.responseText);
							console.log(response);
						}
					});
				} else {
					window.onbeforeunload = null;
					window.location.href = response.link;
				}

				@else

						window.onbeforeunload = null;
					window.location.href = response.link;
				@endif
			},

			error: function (response) {

				jQuery('.errorZone').css('display','block');
				ticketContainer.removeClass('loading');
				body.removeClass("loading");
				jQuery('.errorZone').css('display','block');
				jQuery('.errorZone').text(response.responseText);
				//alert(response.responseText);

				$('.site-content').find('p.error').remove();

				if (response.responseText && response.responseText.match(/email already exists/g)) {
					$('.site-content').prepend('<p style="color:red" class="error">{{trans("homepage.emExists")}}</p>');
				} else if (response.responseText && response.responseText == "{}") {

					jQuery('.errorZone').text('Unable to process, Please try submitting it again');
				}else {
					$('.site-content').prepend('<p style="color:red" class="error">{{trans("homepage.unableToProcess")}}</p>');
					jQuery('.errorZone').text('Unable to process,  Please try  submitting it again or contact us via the support chat.');
				}

				$('html, body').animate({
					scrollTop: $('.site-content').offset().top + 'px'
				}, 'fast');
			}

		});
	});
</script >