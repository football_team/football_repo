<script >
	function updatePageTitle(str) {
		$('.page-header').html(str);
	}

	function backToCheckout() {
		var url = "{{URL::to('/checkout/'.$product_id)}}";
		if (isiPhone()) {
			window.location.replace(url);
		} else {
			window.location = url;
		}
	}

	function isiPhone() {
		return (
			(navigator.platform.indexOf("iPhone") != -1)
			|| (navigator.platform.indexOf("iPod") != -1)
			|| (navigator.platform.indexOf("iPad") != -1)
		);
	}


	$(document).ready(function () {

		$('#centerthis').ajaxForm({
			beforeSend: function () {
				var ticketContainer = $('body');
				ticketContainer.append('<div class="ajax-loading-modal"></div>');
				ticketContainer.addClass("loading");
				$('.error-message').css({'display': 'none'});
			},

			success: function (response) {
				window.location.href = response.link;

			},

			error: function (response) {
				var ticketContainer = $('body');
				ticketContainer.removeClass('loading');
				$('.ajax-loading-modal').remove();
				alert("Could not apply coupon.");
			}
		})
	})
</script >