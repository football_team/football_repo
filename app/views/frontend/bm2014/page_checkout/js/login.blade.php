<script type="text/javascript" charset="utf-8" >
	$(document).ready(function () {

		var loginURL = "{{route('customer.account.checkout.login')}}";
		var ticketContainer = $('body');

		jQuery('.login-btn').click(function(e){
			e.preventDefault();

			var data = {
				login: {
					username: $('#username').val(),
					password: $('#password').val()
				}
			};
			$.ajax({
				url: loginURL,
				type: 'post',
				dataType: 'json',
				data: data,
				beforeSend: function () {
					$(".ajax-loading-modal").remove();
					jQuery('#ticket-checkout-form > div > div > div.col-md-7 > div:nth-child(3)').css('display','none');
					$('.error-message').css({'display': 'none'});
				},
				success: function (reponse) {

					if (reponse.data) {
						var n = $('.number-of-ticket-chk').val();
						var d = $('.countrieslist').val();

						$.ajax({
							url: "/checkout/logproglogin/{{$productId}}",
							type: 'post',
							success: function (response) {
							},
							error: function (response) {
							}
						});

						window.location.replace("/checkout/{{$productId}}?n=" + n + "&d=" + d);

					} else {
						ticketContainer.removeClass('loading');
						jQuery('#ticket-checkout-form > div > div > div.col-md-7 > div:nth-child(3)').css('display','block');
						$('.error-message').css({'display': 'block'});
					}
				},
				error: function (reponse) {
					ticketContainer.removeClass('loading');
					var respObj = $.parseJSON(reponse.responseText);
					if (respObj.error) {
						var html = template({message: respObj.error});
						alert(html);
					}
				}
			});
		});
	});
</script >
