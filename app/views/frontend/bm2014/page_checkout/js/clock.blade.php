<script>

	function countdown(timeLeft)
	{
		window.setInterval(function()
		{
			var mins = Math.floor(timeLeft / 60000);
			var seconds = (timeLeft % 60000) / 1000;
			if(mins >= 0 && seconds >= 0)
			{
				seconds = Math.floor(seconds);
				var timerstring = mins + " : " + seconds;
				jQuery('#defaultCountdown').text(timerstring);
				timeLeft = timeLeft - 1000;

				if(mins <= 7){
					jQuery('.needhelp').css('display', 'block');
					jQuery('.needhelp').addClass('animated fadeInRight');
				}
			}
			else
			{
				jQuery('#defaultCountdown').text("00 : 00");
			}


			var chance = Math.floor((Math.random() * 60) + 1);
			if(chance==10){
				$('.peopleOnPage').removeClass('animated pulse');
				$('.peopleOnPage').addClass('animated pulse');
				jQuery('.people').text(parseInt(jQuery('.people').text())+1);
			}
			if(chance==20){
				$('.peopleOnPage').removeClass('animated pulse');
				$('.peopleOnPage').addClass('animated pulse');
				jQuery('.people').text(parseInt(jQuery('.people').text())-1);
			}

		}, 1000);
	}

	$(document).ready(function ()
	{
		var ticketsThen = $.now() + ( @if(!empty($timer)){{$timer}}@endif * 60000)
		var ticketsNow =  $.now();
		var timeLeft = ticketsThen - ticketsNow;

		countdown(timeLeft);

		if(timeLeft>1)
		{
			jQuery('.timerClock').css('display', 'block');
			jQuery('.timerClock').addClass('animated fadeInRight');
		}
	});
</script>