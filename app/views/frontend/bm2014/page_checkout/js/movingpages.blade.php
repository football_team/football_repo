<script >



		$(document).ready(function () {

			$zopim(function() {
				$zopim.livechat.appendNotes('----NEW ORDER : START----');

				$zopim.livechat.appendNotes("<?php if (!empty($ticket->title)) {
					echo $ticket->title;
				} ?>");

				$zopim.livechat.appendNotes("<?php if (!empty($ticket->event_location)) {
					echo $ticket->event_location;
				} ?>");

				$zopim.livechat.appendNotes("<?php if (!empty($ticket->ticketType['title'])) {
					echo $ticket->ticketType['title'];
				} ?>");

				$zopim.livechat.appendNotes("<?php if (!empty($ticket->info['loc_block'])) {
					echo $ticket->info['loc_block'];
				} ?>");

				$zopim.livechat.appendNotes("<?php if (!empty($ticket->info['loc_row'])) {
					echo $ticket->info['loc_row'];
				} ?>");

				$zopim.livechat.appendNotes("<?php if (!empty($ticket->formOfTicket['title'])) {
					echo $ticket->formOfTicket['title'];
				} ?> <?php echo number_format(($ticket->ticket->ticketInformation->price)*1.1, 2); ?> <?php echo trans('homepage.currencyAfter'); ?>");

				$zopim.livechat.appendNotes("<?php echo $_SERVER['HTTP_HOST'];?>");


				jQuery('input').change(function(){
					$zopim.livechat.addTags(jQuery('this').attr('name'),jQuery('this').attr('value'));
				});

			});


		/**
		 * JOPPIM
		 */
		jQuery('.billingaddressaction ').click(function(){
			jQuery('.shipsame').prop('checked', false)
		});

		window.onbeforeunload = function() {
			return "You're about to end your session, are you sure?";
		}

		jQuery('.qty').val(jQuery('.number-of-ticket-chk').val());

		jQuery('.number-of-ticket-chk').change(function(){
			jQuery('.qty').val(jQuery('.number-of-ticket-chk').val());
		});

		jQuery('.checkout_dt').datetimepicker({closeOnDateSelect:true,timepicker:false,format:'d/m/Y'});
		jQuery('.checkin_dt').datetimepicker({closeOnDateSelect:true,timepicker:false,format:'d/m/Y'});



		jQuery('.meetAtStad').css('display','none');
		$(".countrieslist option").each(function(){
			if(jQuery(this).text()=="Collect On Match Day"){
				$(".countrieslist").val(jQuery(this).val());
				jQuery('.meetAtStad').css('display','block');
				console.log('Collect On Mach Day');
				jQuery('.delTitle').text('What is your billing address');
			}
		});

		$(".nextToPage3").click(function(){
			$(".countrieslist option").each(function(){
			if(jQuery(this).text()=="Collect On Match Day"){
				$(".countrieslist").val(jQuery(this).val());
				jQuery('.meetAtStad').css('display','block');
				console.log('Collect On Mach Day');
				jQuery('.delTitle').text('What is your billing address');
			}
		});});

		jQuery('.resetDeliverToo').click(function(){
			jQuery('.delTitle').text('What is your billing address ');
		});


		jQuery('.backToPageTicket').click(function(){

			jQuery('.pageNumberTwo').css('display','none');
			jQuery('.checkoutPageOne').css('display','block');
		});

		/**
		 * GEt the price on first load
		 */
		jQuery('.ticketq').text(jQuery('.number-of-ticket-chk').val());
		var ticketmaths = jQuery('.number-of-ticket-chk').val() * <?php echo number_format(($ticket->ticket->ticketInformation->price) * 1.1, 2,'.',''); ?>;
		var deliveryPrice =  jQuery('.countrieslist option:selected').attr('data-price');
		ticketmaths = parseFloat(ticketmaths) + parseFloat(deliveryPrice);
		jQuery('.ticketp').text(ticketmaths);

		/**
		 * Then on change
		 */
		jQuery('.number-of-ticket-chk').change(function () {

			jQuery('.ticketq').text(jQuery('.number-of-ticket-chk').val());
			var ticketmaths = jQuery('.number-of-ticket-chk').val() * <?php echo number_format(($ticket->ticket->ticketInformation->price) * 1.1, 2,'.',''); ?>;
			var deliveryPrice =  jQuery('.countrieslist option:selected').attr('data-price');
			ticketmaths = parseFloat(ticketmaths) + parseFloat(deliveryPrice);
			jQuery('.ticketp').text(ticketmaths);
		});

		jQuery('.countrieslist').change(function () {

			jQuery('.ticketq').text(jQuery('.number-of-ticket-chk').val());
			var ticketmaths = jQuery('.number-of-ticket-chk').val() * <?php echo number_format(($ticket->ticket->ticketInformation->price) * 1.1, 2,'.',''); ?>;
			var deliveryPrice = jQuery('.countrieslist option:selected').attr('data-price') + ' : ' + jQuery('.countrieslist option:selected').text();
			jQuery('.currencyCostInprice').text(deliveryPrice);
			ticketmaths = parseFloat(ticketmaths) + parseFloat(deliveryPrice);
			jQuery('.ticketp').text(ticketmaths);
		});


		/**
		 *  User / New User
		 */

		jQuery('.newUser').click(function () {
			jQuery('.returningUserBlock').css('display', 'none');
			jQuery('.newUserblock').css('display', 'block');
		});


		jQuery('.oldUser').click(function () {
			jQuery('.returningUserBlock').css('display', 'block');
			jQuery('.newUserblock').css('display', 'none');
		});


		/**
		 * delivery options
		 */

		jQuery('.backToUserList').click(function () {


			jQuery('.backToPageTicket').css('display', 'block');

			jQuery('.returningUserBlock').css('display', 'none');
			jQuery('.newUserblock').css('display', 'none');
			jQuery('.newUserButton').css('display', 'block');
			jQuery('.returningUserButton').css('display', 'block');
			jQuery('.termsConditions').css('display', 'none');

		});


		jQuery('.resetDeliverToo').click(function () {
			jQuery('.countrieslist').css('display','block');
			jQuery('.deliveryCountryBlock').css('display', 'none ');
			jQuery('.deliveryBlock').css('display', 'none');
			jQuery('.billingAll').css('display', 'none');
			jQuery('.billingAll').css('opacity', '0');
			jQuery('.locationsOnPageThree').css('display', 'block');
			jQuery('.billingAddressTitle').css('display', 'none');
			jQuery('.billingAddressTitle').text('What is your billing address');
			jQuery('.submitform').css('display', 'none');
			jQuery('.hotelAddress').css('display', 'none');
			jQuery('.billingaddressaction').css('display', 'none');
			jQuery('.resetDeliverToo').css('display', 'none');
			jQuery('.termsConditions').css('display', 'none');
			jQuery('.backToPage2').css('display', 'block');
		});

		jQuery('.meetAtStad').click(function () {

			jQuery('.billingAll').css('display', 'none ');
			jQuery('.billingAddressTitle').css('display', 'none ');
			jQuery('.billingAddressTitle').text('What is your billing address');
			jQuery('.locationsOnPageThree').css('display', 'none');
			jQuery('.submitform').css('display', 'block');
			jQuery('.resetDeliverToo').css('display', 'block');
			jQuery('.termsConditions').css('display', 'block');
			jQuery('.backToPage2').css('display', 'none');
			jQuery('.deliveryBlock').css('display', 'block ');
			jQuery('.countrieslist').css('display','none');

			$(".countrieslist option").each(function(){
				if(jQuery(this).text()=="Collect On Match Day"){
					$(".countrieslist").val(jQuery(this).val())
				}
			})


		});


		jQuery('.deliverToHouse').click(function () {
			jQuery('.countrieslist').val('flatrate');
			jQuery('.deliveryCountryBlock').css('display', 'block ');
			jQuery('.deliveryBlock').css('display', 'block');
			jQuery('.billingaddressaction').css('display', 'block');
			jQuery('.locationsOnPageThree').css('display', 'none');
			jQuery('.submitform').css('display', 'block');
			jQuery('.resetDeliverToo').css('display', 'block');
			jQuery('.backToPage2').css('display', 'none');
			jQuery('.termsConditions').css('display', 'block');


		});


		jQuery('.deliverToHotel').click(function () {
			jQuery('.deliveryCountryBlock').css('display', 'block ');
			jQuery('.deliveryBlock').css('display', 'block');
			jQuery('.billingAll').css('display', 'block');
			jQuery('.billingAll').css('opacity', '1');
			jQuery('.locationsOnPageThree').css('display', 'none');
			jQuery('.billingAddressTitle').css('display', 'block');
			jQuery('.billingAddressTitle').text('What is your billing address');
			jQuery('.submitform').css('display', 'block');
			jQuery('.hotelAddress').css('display', 'block');
			jQuery('.backToPage2').css('display', 'none');
			jQuery('.termsConditions').css('display', 'block');
			jQuery('.resetDeliverToo').css('display', 'block');



			$(".countrieslist option").each(function(){
				if(jQuery(this).text()=="UK - Hotel Delivery Free"){
					$(".countrieslist").val(jQuery(this).val())
				}
			})
		});


		jQuery('.deliverToOffice').click(function () {
			jQuery('.deliveryCountryBlock').css('display', 'block ');
			jQuery('.deliveryBlock').css('display', 'block');
			jQuery('.billingAll').css('display', 'block');
			jQuery('.billingAll').css('opacity', '1');
			jQuery('.locationsOnPageThree').css('display', 'none');
			jQuery('.billingAddressTitle').css('display', 'block');
			jQuery('.billingAddressTitle').text('What is your billing address');
			jQuery('.submitform').css('display', 'block');
			jQuery('.resetDeliverToo').css('display', 'block');
			jQuery('.termsConditions').css('display', 'block');
			jQuery('.billingaddressaction').css('display', 'none');
			jQuery('.backToPage2').css('display', 'none');


		});


		/**
		 * Changing From a New user to an old suer
		 */

		jQuery('.newUserButton').click(function () {
			jQuery('.backToPageTicket').css('display', 'none');
			jQuery('.newUserblock').css('display', 'block');
			jQuery('.newUserButton').css('display', 'none');
			jQuery('.returningUserButton').css('display', 'none');
		});


		jQuery('.returningUserButton').click(function () {
			jQuery('.backToPageTicket').css('display', 'none');
			jQuery('.returningUserBlock').css('display', 'block');
			jQuery('.newUserButton').css('display', 'none');
			jQuery('.returningUserButton').css('display', 'none');
		});


		jQuery('.billingaddressaction').click(function () {
			jQuery('.billingAll').addClass('animated fadeInDown');
			jQuery('.billingAll').css('display', 'block');
			jQuery('.billingAll').css('opacity', '1');
			jQuery('.billingAll').css('display', 'block');
			jQuery('.billingaddressaction').css('display', 'none');
			jQuery('.billingAddressTitle').css('display', 'block');

		});


		jQuery('.lowerTicketBlock').click(function () {
			$('.backToPageTicket').css('display', ' block');
			jQuery('.pageNumberTwo').css('display','block');
			$('.checkoutPageOne').addClass('animated fadeOutDown');
			$('.checkoutPageOne').css('display', 'none');
			$('.checkoutPageTwo').css('opacity', '1');
			$('.checkoutPageOne').removeClass('animated fadeOutDown');


			$('.checkoutstep').removeClass('active');
			$('.checkoutstep.step2').addClass('animated flash active');
			$('.checkoutstep.step2').removeClass('animated flash');

			jQuery('.step1').addClass('stepDone');


			$('.checkoutPageTwo').css('display', 'block');
			$('.checkoutPageTwo').addClass('animated fadeInDown');
			$('.checkoutstep.step3').addClass('animated flash');
			$('.checkoutstep.step3').removeClass('animated flash');


		});
		jQuery('.backToPage1').click(function () {
			$('.checkoutPageTwo').addClass('animated fadeOutDown');
			$('.checkoutPageTwo').css('display', 'none');
			$('.checkoutPageTwo').removeClass('animated fadeOutDown');


			jQuery('.step1').removeClass('stepDone');
			$('.checkoutstep').removeClass('active');
			$('.checkoutstep.step1').addClass('animated flash active');
			$('.checkoutstep.step1').removeClass('animated flash');


			$('.checkoutPageOne').css('display', 'block');
			$('.checkoutPageOne').addClass('animated fadeInDown');


		});
		jQuery('.backToPage2').click(function () {
			$('.checkoutPageThree').addClass('animated fadeOutDown');
			$('.checkoutPageThree').css('display', 'none');
			$('.checkoutPageThree').removeClass('animated fadeOutDown');


			jQuery('.step2').removeClass('stepDone');
			$('.checkoutstep').removeClass('active');
			$('.checkoutstep.step2').addClass('animated flash active');
			$('.checkoutstep.step2').removeClass('animated flash');

			$('.checkoutPageTwo').css('display', 'block');
			$('.checkoutPageTwo').addClass('animated fadeInDown');
		});
		jQuery('.nextToPage3').click(function () {
			validatePageOne();

			jQuery('.step2').addClass('stepDone');
		});
		jQuery('.nextToPage4').click(function () {

			$('.checkoutPageThree').addClass('animated fadeOutDown');
			$('.checkoutPageThree').css('display', 'none');
			$('.checkoutPageThree').removeClass('animated fadeOutDown');
			jQuery('.step3').addClass('stepDone');
			$('.checkoutstep').removeClass('active');
			$('.checkoutstep.step4').addClass('animated flash active');
			$('.checkoutstep.step4').removeClass('animated flash');

			$('.checkoutPageThree').css('display', 'block');
			$('.checkoutPageThree').addClass('animated fadeInDown');
		})
	});
</script >