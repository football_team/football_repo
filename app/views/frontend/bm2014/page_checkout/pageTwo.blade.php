<div class="col-xs-12 pageNumberTwo" >
	<h2 class="col-xs-12">A Little about yourself</h2 >
	<!-- Row One Of Options -->
	<input type="hidden" value='shipping' name="new_shipping_address" >
@if (empty($customer))
		@include('frontend.bm2014.page_checkout.form.pageTwo.loggedout')
	@else
		@include('frontend.bm2014.page_checkout.form.pageTwo.loggedin')
	@endif
	<div class="backToPageTicket backButton col-xs-12" style="border:none;">Back</div>
</div >
