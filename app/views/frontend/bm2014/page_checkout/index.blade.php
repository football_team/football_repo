@extends(Template::name('frontend.%s._layout.layout'))
@section('style')
	@include('frontend.bm2014.page_checkout.style')
@stop
@section('script')
	@parent
	@include('frontend.bm2014.page_checkout.js.includes')
	@include('frontend.bm2014.page_checkout.js.clock')
	@include('frontend.bm2014.page_checkout.js.oldCheckoutScripts')
	@include('frontend.bm2014.page_checkout.js.movingpages')
	@include('frontend.bm2014.page_checkout.js.validation')
	@include('frontend.bm2014.page_checkout.js.login')
	@include('frontend.bm2014.page_checkout.js.submitform')
@stop

@section('content')
	<div style="
    width: 100%;
    text-align: left;
" ><a href="/" ><img src="/footballticketpadlogo-whitebackground.png" style="
    margin: 10px;
" ></a ></div >
	<form action="{{route('ticket.checkout.order', array('id'=>$productId))}}" method="post" id="ticket-checkout-form" >
		<div class="" >
			<div class="container" >
				<div class="col-md-7" style="" >
					@include('frontend.bm2014.page_checkout.js.breadcrumb')
					<div class="checkoutarea" >
						<div class="checkoutPageOne" >@include('frontend.bm2014.page_checkout.pageOne')</div >
						<div class="checkoutPageTwo" style="display:none; opacity: 0;" >@include('frontend.bm2014.page_checkout.pageTwo')</div >
						<div class="checkoutPageThree" style="display:none;  opacity:0;" >@include('frontend.bm2014.page_checkout.pageThree')</div >
					</div >
				</div >
				<div class="col-md-4 col-xs-12 checkoutSideBarMainContainer" style="" >
					@include('frontend.bm2014.page_checkout.form.sidebar.oderTotal')
					@include('frontend.bm2014.page_checkout.form.sidebar.peopleOnPage')
					@include('frontend.bm2014.page_checkout.form.sidebar.helpnote')
					@include('frontend.bm2014.page_checkout.form.sidebar.timer')
					@include('frontend.bm2014.page_checkout.trust')
				</div >
			</div >
		</div >
		</div>
	</form >
@stop
