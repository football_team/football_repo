
<div class="checkoutarea" >
	<!---------buyer details------------>
	<h3 class="buy-divider row" >{{trans('homepage.Buyer Details')}}</h3 >
	@if (!empty($customer))
		<span class="label-seperator" >{{trans('homepage.Personal Details')}}</span >
		<div class="registration-form" >

			<div class="columns six" >
				<label >
				</label >
			</div >
			<div style="clear: both" >
			</div >
			@if((!isset($shipping['entity_id']))||(!isset($billing['entity_id'])))
				<span class="label-seperator" >{{trans('homepage.Shipping Address')}}</span >
				<input type="hidden" value='shipping' name="new_shipping_address" >
				<div id="hd-elements" >
				</div >
				<div class="columns six" >
					<label >
						{{ Form::text('street', '', ['class'=>'input street', 'placeholder' => trans('homepage.Address'), 'required'=>true ]) }}
					</label >
				</div >

				<div class="columns six" >
					<label >
						{{ Form::text('postcode', '', ['class'=>'input postcode', 'placeholder' => trans('homepage.Postcode'), 'required'=>true ]) }}
					</label >
				</div >

				<div class="columns six" >
					<label >
						{{ Form::text('city', '', ['class'=>'input city', 'placeholder' => trans('homepage.City'), 'required'=>true ]) }}
					</label >
				</div >

				<div class="columns six" >
					{{ Form::select('country_id', $_country, 'GB', ['class'=>'input country-code', 'placeholder' => trans('homepage.Country Code'), 'id'=>'country', 'required'=>true] ) }}
				</div >

				<div class="columns six" >
					<label >
					</label >
				</div >
				<div class="columns six" >
					<label >
						{{ Form::checkbox('ship-same', '1') }}
						{{trans('homepage.bsAreSame')}}
					</label >
				</div >

				<div style="clear: both" >
				</div >
				<div class="hideIfSameAsShip" >
					<span class="label-seperator" >{{trans('homepage.Billing Address')}}</span >
					<div id="hd-elements" >
					</div >
					<div class="columns six" >
						<label >
							{{ Form::text('street_bill', '', ['class'=>'input street', 'placeholder' => trans('homepage.Address') ]) }}
						</label >
					</div >
					<div class="columns six" >
						<label >
							{{ Form::text('postcode_bill', '', ['class'=>'input postcode', 'placeholder' => trans('homepage.Postcode') ]) }}
						</label >
					</div >
					<div class="columns six" >
						<label >
							{{ Form::text('city_bill', '', ['class'=>'input city', 'placeholder' => trans('homepage.City') ]) }}
						</label >
					</div >
					<div class="columns six" >
						{{ Form::select('country_id_bill', $_country, 'GB', ['class'=>'input country-code', 'placeholder' => trans('homepage.Country Code'), 'id'=>'country'] ) }}
					</div >
				</div >
				<div style="clear: both" >
				</div >

			@else
				<span class="label-seperator" >{{trans('homepage.Shipping Address')}}</span >
				<input type="hidden" value='shipping' name="new_shipping_address" >
				<div id="hd-elements" >
				</div >
				<div class="columns six" >
					<label >
						{{ Form::text('street', $shipping['street'], ['class'=>'input street', 'placeholder' => trans('homepage.Address'), 'required'=>true ]) }}
					</label >
				</div >

				<div class="columns six" >
					<label >
						{{ Form::text('postcode', $shipping['postcode'], ['class'=>'input postcode', 'placeholder' => trans('homepage.Postcode'), 'required'=>true ]) }}
					</label >
				</div >

				<div class="columns six" >
					<label >
						{{ Form::text('city', $shipping['city'], ['class'=>'input city', 'placeholder' => trans('homepage.City'), 'required'=>true ]) }}
					</label >
				</div >
				<div class="columns six" >
					{{ Form::select('country_id', $_country, $shipping['country_id'], ['class'=>'input country-code', 'placeholder' => trans('homepage.Country Code'), 'id'=>'country', 'required'=>true] ) }}
				</div >
				<div class="columns six" >
					<label >
						{{ Form::select('country_code', $country_with_phone, '0044', ['class'=>'input country-code', 'placeholder' => trans('homepage.Country Code'), 'required'=>true ] ) }}
					</label >
				</div >
				<div class="columns six" >
					<label >
						{{ Form::text('contact_no', '', ['class'=>'input contact-no', 'placeholder' => trans('homepage.Mobile Number'), 'required'=>true ]) }}
					</label >
				</div >
				<div class="columns six" >
					<label >
						{{ Form::checkbox('ship-same', '1') }}
						{{trans('homepage.bsAreSame')}}
					</label >
				</div >
				<div style="clear: both" >
				</div >

				<div class="hideIfSameAsShip" >
					<span class="label-seperator" >{{trans('homepage.Billing Address')}}</span >
					<div id="hd-elements" >
					</div >
					<div class="columns six" >
						<label >
							{{ Form::text('street_bill', $billing['street'], ['class'=>'input street', 'placeholder' => trans('homepage.Address') ]) }}
						</label >
					</div >
					<div class="columns six" >
						<label >
							{{ Form::text('postcode_bill', $billing['postcode'], ['class'=>'input postcode', 'placeholder' => trans('homepage.Postcode') ]) }}
						</label >
					</div >
					<div class="columns six" >
						<label >
							{{ Form::text('city_bill', $billing['city'],['class'=>'input city', 'placeholder' => trans('homepage.City') ]) }}
						</label >
					</div >
					<div class="columns six" >
						{{ Form::select('country_id_bill', $_country, $billing['country_id'], ['class'=>'input country-code', 'placeholder' => trans('homepage.Country Code'), 'id'=>'country'] ) }}
					</div >
				</div >
				<div style="clear: both" >
				</div >
			@endif
		</div >
	@else
		<div class="login-container" >{{trans('homepage.ifRegClickHere')}}
			<a href="#" class="ajax-login" >{{trans('homepage.click here')}}</a > {{trans('homepage.to login')}}
		</div >

		<br />
		<span class="label-seperator" >{{trans('homepage.Personal Details')}}</span >
		<div class="registration-form" >
			<div class="columns six" >
				<label >
					{{ Form::text('first_name', '', ['class'=>'input first-name', 'placeholder' => trans('homepage.fname') ]) }}
				</label >
			</div >
			<div class="columns six" >
				<label >
					{{ Form::text('last_name', '', ['class'=>'input last-name', 'placeholder' => trans('homepage.sname') ]) }}
				</label >
			</div >
			<div class="columns six" >
				<label >
					{{ Form::select('country_code', $country_with_phone, '0044', ['class'=>'input country-code', 'placeholder' => trans('homepage.Country Code') ] ) }}
				</label >
			</div >
			<div class="columns six" >
				<label >
					{{ Form::text('contact_no', '', ['class'=>'input contact-no', 'placeholder' => trans('homepage.Mobile Number') ]) }}
				</label >
			</div >
			<div class="columns six" >
				<label >
					{{ Form::checkbox('newsletters', '1') }}
					{{trans('homepage.subForPromDis')}}
				</label >
			</div >
			<span class="label-seperator" >{{trans('homepage.Security')}}</span >
			<div class="columns six" >
				<label >
										<span class="mbtm" > {{trans('homepage.Password')}}
											<small class="smalltxts-warning" >{{trans('homepage.passWarningLN')}}</small ></span >
					{{ Form::password('password', '') }}
				</label >
			</div >
			<div class="columns six" >
				<label >
										<span class="mbtm" >{{trans('homepage.Confirm Password')}}
											<small class="smalltxts-warning" >{{trans('homepage.passWarningLN')}}</small ></span >
					{{ Form::password('password_confirmation', '') }}
				</label >
			</div >
			<div style="clear: both" >
			</div >
			<span class="label-seperator" >{{trans('homepage.Shipping Address')}}</span >
			<div id="hd-elements" >
			</div >
			<div class="columns six" >
				<label >
					{{ Form::text('street', '', ['class'=>'input street', 'placeholder' => trans("homepage.Address") ]) }}
				</label >
			</div >
			<div class="columns six" >
				<label >
					{{ Form::text('postcode', '', ['class'=>'input postcode', 'placeholder' => trans('homepage.Postcode') ]) }}
				</label >
			</div >
			<div class="columns six" >
				<label >
					{{ Form::text('city', '', ['class'=>'input city', 'placeholder' => trans('homepage.City') ]) }}
				</label >
			</div >
			<div class="columns six" >
				{{ Form::select('country_id', $_country, 'GB', ['class'=>'input country-code', 'placeholder' => trans('homepage.Country Code'), 'id'=>'country'] ) }}
			</div >
			<div class="columns six" >
				<label >
					{{ Form::checkbox('ship-same', '1') }}
					{{trans('homepage.bsAreSame')}}
				</label >
			</div >
			<div style="clear: both" >
			</div >
			<div class="hideIfSameAsShip" >
				<span class="label-seperator" >{{trans('homepage.Billing Address')}}</span >
				<div id="hd-elements" >
				</div >
				<div class="columns six" >
					<label >
						{{ Form::text('street_bill', '', ['class'=>'input street', 'placeholder' => trans('homepage.Address') ]) }}
					</label >
				</div >
				<div class="columns six" >
					<label >
						{{ Form::text('postcode_bill', '', ['class'=>'input postcode', 'placeholder' => trans('homepage.Postcode') ]) }}
					</label >
				</div >
				<div class="columns six" >
					<label >
						{{ Form::text('city_bill', '', ['class'=>'input city', 'placeholder' => trans('homepage.City') ]) }}
					</label >
				</div >
				<div class="columns six" >
					{{ Form::select('country_id_bill', $_country, 'GB', ['class'=>'input country-code', 'placeholder' => trans('homepage.Country Code'), 'id'=>'country'] ) }}
				</div >
			</div >
			<div style="clear: both" >
			</div >
		</div >
	@endif
		<!---------buyer details------------>
</div >
<div class="checkoutarea" >
	<span class="label-seperator" >&nbsp;</span >
	<div class="columns six" >
		<label >
			<input type="checkbox" value="1" name="terms_n_conditions" >
			{{trans('homepage.tncAgree')}}
			<a href="/terms-conditions" target="_blank" >{{trans('homepage.terms and conditions')}} </a >
		</label >
		<br /><br />
		<input type="submit" value="{{trans('homepage.SUBMIT ORDER')}}" class="btn pinkbtn pull-right submit-order" >
	</div >
</div >