<link href="https://cdn2.footballticketpad.com/css/bootstrapgrid.css" rel="stylesheet" >
<link href="https://cdn2.footballticketpad.com/css/animate.css" rel="stylesheet" >
<link href="https://cdn2.footballticketpad.com/frontend/bm2014/js/jquery-datetime/jquery.datetimepicker.css" rel="stylesheet" >

<style >
	header{
		display: none !important;
	}
	body{
		margin: 0 !important;
		padding: 0 !important;
	}
	#ticket-checkout-form{
		margin:0 !important;
	}
	@media (max-width: 768px) {
		.row.col-xs-12 {
			width: 100%;
			padding: 20px;
		}
	}

	.checkoutPageThree input, .checkoutPageTwo input {
		border-radius: 2px !important;
	}

	footer {
		float: left;
		width: 100%;
		padding: 0;
	}
	.ticketBodyRight div{
		color: black !important;
	}

	.ticketBody {
		min-height: 200px;

		width: 30%;
		background: lightgrey;
		height: 100% !important;
		margin: 0 !important;
		padding: 16px;
		/* box-shadow: 1px 3px 0px 1px #F2F2F2; */
		border-left: dashed white 3px;
		border-bottom-left-radius: 5px;
		border-top-left-radius: 5px;
		float: right;
		border-bottom-right-radius: 5px;
		border-top-right-radius: 5px;
		font-size: 20px;
		text-align: left;
		color: white;
		font-weight: bold;
		border-right: dashed white 3px;
	}

	section.section_main {
		display: none;
	}

	.full.lightgrey {
		display: none;
	}

	img.columns.four.accepted-cards {
		display: none;
	}

	.social.columns.four {
		display: none;
	}

	.ticketBodyRight {
		min-height: 200px;

		width: 70%;
		background: lightgrey;
		height: 100% !important;
		margin: 0 !important;
		padding: 16px;
		border-left: 10px solid darkgrey;
		/* box-shadow: 1px 3px 0px 1px #F2F2F2; */
		border-bottom-right-radius: 5px;
		border-top-right-radius: 5px;
		border-top-left-radius: 5px;
		border-bottom-left-radius: 5px;
		font-size: 20px;
		text-align: left;
		color: white;
		font-weight: bold;

	}

	.ticketBody p {
		color: black;
		font-weight: 400;
		font-size: 15px;
		line-height: 1;
		padding: 0px;
		margin: 5px;
	}

	.ticketBodyRight p {
		color: black;
		font-weight: 400;
		font-size: 15px;
		line-height: 1;
		padding: 0px;
		margin: 5px;
	}

	.outerTicketBlock {

		float: left;
		width: 100%;
		margin-top: 15px;
	}

	.lowerTicketBlock {

		width: 100%;
		float: left;
		background: #7EC26D;
		padding: 5px;
		border-radius: 5px;
		font-size: 20px;
		color: white;
		font-weight: bold;
		text-align: center;

	}

	header {
		background: #2E3136;
	}

	.peopleOnPage {
		width: 100%;
		height: 100px;
		border-radius: 4px;
		margin-bottom: 20px;
		box-shadow: 1px 3px 0px 1px #F2F2F2;
		border-left: #F2F2F2 1px solid;
		border-top: #F2F2F2 1px solid;
	}

	.framed.col-xs-12.col-md-6 {
		width: 46% !important;
		float: left;
		margin: 2%;
		text-align: center;

	}

	.searcharea, .loginbtn, .menu-container {
		display: none !important;
	}

	.framed {
		width: 100%;
		height: 100%;
		border-radius: 4px;
		border-top: 1px solid #F2F2F2;
		border-left: 1px solid #F2F2F2;
		box-shadow: 1px 3px 0px 1px #F2F2F2;
		margin-bottom: 20px;
		padding: 12px;
	}

	.backButton {
		float: left;
		background: #CE2B2B;
		padding: 5px;
		border-left: 5px solid #AD2A26;
		border-radius: 5px;
		font-size: 20px;
		color: white;
		font-weight: bold;
		text-align: center;
	}

	.rightSideInput {
		padding-left: 10px;
	}

	.leftSdeInput {
		padding-right: 10px;
	}

	.nextButton {
		width: 100%;
		float: left;

		border-left: 5px solid #158919;
		background: #7EC26D;
		padding: 5px;
		border-radius: 5px;
		font-size: 20px;
		color: white;
		font-weight: bold;
		text-align: center;
	}

	.bckbuttonHolder {
		padding-right: 10px;
	}

	.nextButtonHolder {
		padding-right: 10px;
	}

	label.error, label .error {
		display: none !important;
	}

	.newUserButton.col-md-12 {
		float: left;
		width: 100%;
		background: #7EC26D;
		padding: 5px;
		border-left: 5px solid #158919;
		border-radius: 5px;
		font-size: 20px;
		color: white;
		font-weight: bold;
		text-align: center;
	}

	.returningUserButton.col-md-12 {
		float: left;
		background: #63C9DA;
		width: 100%;
		padding: 5px;
		border-left: 5px solid #1BA1E2;
		border-radius: 5px;
		font-size: 20px;
		color: white;
		font-weight: bold;
		text-align: center;
	}

	.pillbutton {
		border-radius: 5px;
		font-size: 20px;
		color: white;
		font-size: 20px;
		color: white;
		font-weight: bold;
		text-align: center;
		padding: 5px;
		float: left;
		border-top: 0;
		border-right: 0;
		border-bottom: 0;
	}

	.greenPillButton {

		background: #7EC26D;
		border-left: 5px solid #158919;
	}

	.yellowPillButton {

		background: #FFD05C;
		border-left: #FEB82C 5px solid;
	}

	.bluePillButton {

		background: #63C9DA;
		border-left: 5px solid #1BA1E2;
	}

	.redPillButton {
		background: #CE2B2B;
		border-left: 5px solid #AD2A26;
	}

	.stepDone {
		background: #7EC26D !important;
	}

	@media (max-width: 767px) {
		.hidden-xs {
			display: none !important;
		}

		.rightSideInput {
			padding-left: 0 !important;
		}

		footer {
			display: none;
		}

		.sidebar-mobile {
			display: none;
		}

		aside {
			display: none;
		}

		select {
			border-radius: 5px !important;
			width: 100%;
		}

		header {
			height: 100px;
			overflow: hidden;
			margin-bottom: -20px !important;
		}

		form {
			margin-top: -50px !important;
			margin: 0 0 19.41641px;
		}

		.ticketBody {
			min-height: 134px;
			width: 100%;
			background: lightgrey;
			height: 100% !important;
			margin: 0 !important;
			padding: 16px;
			border-left: 10px solid darkgrey;
			border-right: 10px solid darkgrey;
			/* box-shadow: 1px 3px 0px 1px #F2F2F2; */
			border-bottom: dashed vl 3px;
			border-bottom-left-radius: 5px;
			border-top: dashed white 3px;
			border-top-left-radius: 5px;
			float: right;
			border-bottom-right-radius: 5px;
			border-top-right-radius: 5px;
			font-size: 20px;
			text-align: left;
			color: black;
			font-weight: bold;
		}

		.ticketBodyRight {
			width: 100%;
			float: left;
			background: lightgrey;
			height: 100% !important;
			margin: 0 !important;
			padding: 16px;
			border-left: 10px solid darkgrey;
			border-right: 10px solid darkgrey;
			/* box-shadow: 1px 3px 0px 1px #F2F2F2; */
			border-bottom-right-radius: 5px;
			border-top-right-radius: 5px;
			border-bottom: dashed white 3px;
			border-top-left-radius: 5px;
			border-bottom-left-radius: 5px;
			font-size: 20px;
			text-align: left;
			color: black;
			font-weight: bold;
		}


		.backToPage2{
			width: 100%;
		}

	}
</style >