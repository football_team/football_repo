<section class="banner-home" >
    <span class="page-header buy-detail" >
        <h1 class="gamename" >{{ e($node->title) }}</h1 >
    </span >
    <span class="club-effect" >
        @if(isset($feature_image) && $feature_image != '')
			<img class="inner-banner" src="{{ $feature_image }}" alt="{{$node->title}}" />
		@else
			<img class="inner-banner" src="{{ Assets::Path('images/default.jpg') }}" alt="{{$node->title}}" />
		@endif
    </span >
    <span class="ftp-line" >
    	<span class="greenline" ></span >
        <span class="yellowline" ></span >
        <span class="pinkline" ></span >
        <span class="blueline" ></span >
    </span >
</section >