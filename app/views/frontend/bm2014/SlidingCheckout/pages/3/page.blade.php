<br>
<form action="/" id="checkoutForm" class="checkoutForm">
    <! --Billing information -->
    <h2 class="billingAddressTitle" style="" >{{trans('homepage.Billing address')}}</h2 >

    <input type="hidden" id="cThis" name="same_as_ship" value="false">
    @if($cbs==true)
        <h3>{{trans('homepage.bsAreSame')}}: <input type="checkbox" name="unrel" id="is_same" value="true" style="width:initial !important;" autocomplete="off"></h3>
    @endif
    <div id="htis">
        <div class="col-xs-12 col-md-6 leftSdeInput" >
            <div >{{trans('homepage.Address')}}</div >
            {{ Form::text('street_bill', array_key_exists('street',$billing)?$billing['street']:'', ['class'=>' street', 'placeholder' => trans('homepage.Address'), 'required'=>true ]) }}
        </div >
        <div class="col-xs-12 col-md-6 rightSideInput" >
            <div >{{trans('homepage.Postcode')}}</div >
            {{ Form::text('postcode_bill', array_key_exists('postcode',$billing)?$billing['postcode']:'', ['class'=>' postcode', 'placeholder' => trans('homepage.Postcode'), 'required'=>true ]) }}
        </div >
        <div class="col-xs-12 col-md-6 leftSdeInput" >
            <div >{{trans('homepage.City')}}</div >
            {{ Form::text('city_bill', array_key_exists('city',$billing)?$billing['city']:'', ['class'=>' city', 'placeholder' => trans('homepage.City'), 'required'=>true ]) }}
        </div >
        <div class="col-xs-12 col-md-6 rightSideInput" >
            <div>{{trans('homepage.Country')}}</div >
            {{ Form::select('country_id_bill', $_country, array_key_exists('country_id',$billing)?$billing['country_id']:'GB', ['class'=>' country-code input', 'placeholder' => trans('homepage.Country Code'), 'id'=>'country', 'required'=>true] ) }}
        </div >
     </div>
     @if(Options::getOption('payment_gateway') == 'ezpay')

    <style>
        .cc_details input[type=text] {width:200p !important; float: left;}
        .trust-container {margin-bottom: 15px; }
        .trust-container img { margin: 10px; }
        .visa-logo {width: 90px}
        .master-logo {width: 50px}
        .ssl-logo {width: 120px}
        .ui-datepicker-month, .ui-datepicker-year {background-color: transparent; border: none;}
        .ui-datepicker-calendar {display: none;}
    </style>

     <!-- Payment information -->
     <h2 class="billingAddressTitle" style="" >Payment Information</h2 >
     <div id="htis">
        <div class="col-xs-12 col-md-6 leftSideInput" >
            <div>{{trans('homepage.ccName')}}</div >
            <input type="text" name="name" placeholder="{{trans('homepage.ccName')}}"><br/>
        </div >
     	<div class="col-xs-12 col-md-6 rightSideInput" >
            <div >{{trans('homepage.ccNumber')}}</div >
            <input type="text" id="ez_card" data-ezpay-token-type="card" placeholder="{{trans('homepage.ccNumber')}}" /> <br/>
        </div >
        <div class="col-xs-12 col-md-6 leftSdeInput" >
		<div class="cc_details">
		</div>
        <div >{{trans('homepage.ccExpiry')}}</div >
         <input type="text" id="ez_expiry" data-ezpay-token-type="expiry" placeholder="YYMM" /><br/>
        </div >

        <div class="col-xs-12 col-md-6 rightSideInput" >
            <div >{{trans('homepage.ccCvv')}}</div >
            <input type="text" id="ez_cvv" data-ezpay-token-type="cvv" placeholder="{{trans('homepage.ccCvv')}}" /> <br/>
        </div >
        <div class="trust-container">
            <img class="visa-logo" src="https://cdn1.footballticketpad.com/frontend/bm2014/images/visa-logo.png" title="visa-logo">
            <img class="master-logo" src="https://cdn1.footballticketpad.com/frontend/bm2014/images/mastercard-logo.png" title="mastercard-logo">
            <img class="ssl-logo" src="https://cdn1.footballticketpad.com/frontend/bm2014/images/ssl-secure.png" title="ssl-secure">
        </div>

        <input type="hidden" data-ezpay-token-type="amount-decimal" name="amount" id="amount" value=""/>
        <input type="hidden" data-ezpay-token-type="currency" name="currency" id="currency" value="GBP"/>
        <input type="hidden" data-ezpay-token-type="mode" value="MPI"/>
        <input type="hidden" name="token" id="token"/>
        <input type="hidden" name="cc_number" id="cc_number" />
        <input type="hidden" name="cc_expire" id="cc_expire" />
        <input type="hidden" name="cc_cvv" id="cc_cvv"/>
        <script>
            $('#ez_expiry').datepicker({
                changeMonth: true,
                changeYear: true,
                showButtonPanel: true,
                dateFormat:"ymm",
                onClose: function(dateText, inst) {
                    $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
                }
            });
        </script>
    </div>
    @endif
    <div class="col-xs-12" id="errDiv" >
    </div >
    <div class="col-xs-6 bckbuttonHolder" >
        <p class="backToPage1 backButton prevBtn col-xs-12" data-active-page=".step4">{{trans('homepage.BACK')}}</p >
    </div >
    <div class="col-xs-6 nextButtonHolder" >
        <p data-active-page="" class="nextToPage3 nextButton checkout-final col-xs-12" >{{trans('homepage.Checkout')}}</p >
    </div >
</form>
