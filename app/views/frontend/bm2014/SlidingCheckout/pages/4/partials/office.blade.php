<form action="/" id="checkoutForm">
    <input type="hidden" name="shipping_method" value="{{$sm}}" id="ifSM">
    <input type="hidden" name="country" value="" id="cntry">
    <input type="hidden" name="checkin_dt" value="2016-01-01 00:00">
    <input type="hidden" name="checkout_dt" value="2016-01-01 00:00">
    <input type="hidden" name="hotel_name" value="">
    <input type="hidden" name="reservation_name" value="">
    <input type="hidden" name="is_hotel" value="false">
    <input type="hidden" name="is_office" value="true">
    <input type="hidden" name="allow_same_as_ship" value="false">
    <br><br>
    <div style="" class="" >
        <div class="col-xs-12 col-md-6 leftSdeInput">
            <div>{{trans('homepage.Company Name')}} <div class="green">({{trans('homepage.optional')}})</div></div>
            {{ Form::text('office_address', '', ['class'=>' street', 'placeholder' => trans('homepage.Address'), 'required'=>true ]) }}
        </div>
        <div class="col-xs-12 col-md-6 rightSideInput">
            <div>{{trans('homepage.Address')}}</div>
            {{ Form::text('street', array_key_exists('street',$shipping)?$shipping['street']:'', ['class'=>' street', 'placeholder' => trans('homepage.Address'), 'required'=>true ]) }}
        </div>
        <div class="col-xs-12 col-md-6 leftSdeInput">
            <div>{{trans('homepage.City')}}</div>
            {{ Form::text('city', array_key_exists('city',$shipping)?$shipping['city']:'', ['class'=>' city', 'placeholder' => trans('homepage.City'), 'required'=>true ]) }}
        </div>
        <div class="col-xs-12 col-md-6 rightSideInput">
            <div>{{trans('homepage.Postcode')}}</div>
            {{ Form::text('postcode', array_key_exists('postcode',$shipping)?$shipping['postcode']:'', ['class'=>' postcode', 'placeholder' => trans('homepage.Postcode'), 'required'=>true ]) }}
        </div>
        <div class="col-xs-12 col-md-6"></div>
    </div >
</form>
<br>
<div class="col-xs-12" id="errDiv" >
</div >
<div class="row" style="display: inline-block; width: 100%; min-width: initial;">
    <div class="col-xs-6 bckbuttonHolder" >
        <p class="backToPage1 backButton shipShow col-xs-12" data-active-page=".step3">{{trans('homepage.BACK')}}</p >
    </div >
    <div class="col-xs-6 nextButtonHolder" >
        <p data-active-page=".step4" class="nextToPage3 nextButton nextBtn col-xs-12" >{{trans('homepage.ch_next')}}</p >
    </div >
</div>
