<form action="/" id="checkoutForm">
    <input type="hidden" name="shipping_method" value="{{$sm}}" id="ifSM">
    <input type="hidden" name="country" value="" id="cntry">
    <input type="hidden" name="street" value="tbc">
    <input type="hidden" name="postcode" value="tbc">
    <input type="hidden" name="city" value="tbc">
    <input type="hidden" name="checkin_dt" value="2016-01-01 00:00">
    <input type="hidden" name="checkout_dt" value="2016-01-01 00:00">
    <input type="hidden" name="hotel_name" value="">
    <input type="hidden" name="reservation_name" value="">
    <input type="hidden" name="is_hotel" value="false">
    <input type="hidden" name="is_office" value="false">
    <input type="hidden" name="allow_same_as_ship" value="false">
    <input type="hidden" name="office_address" value="">
</form>
<br>
<p>{{trans('homepage.printInfoMsg')}}</p>
<div class="row" style="display: inline-block; width: 100%; min-width: initial;">
    <div class="col-xs-6 bckbuttonHolder" >
        <p class="backToPage1 backButton shipShow col-xs-12" data-active-page=".step3">{{trans('homepage.BACK')}}</p >
    </div >
    <div class="col-xs-6 nextButtonHolder" >
        <p data-active-page=".step4" class="nextToPage3 nextButton nextBtn col-xs-12" >{{trans('homepage.ch_next')}}</p >
    </div >
</div>