<br><br>
<form action="/" id="checkoutForm">
<input type="hidden" name="shipping_method" value="{{$sm}}" id="ifSM">
<input type="hidden" name="country" value="" id="cntry">
<input type="hidden" name="is_hotel" value="true">
<input type="hidden" name="is_office" value="false">
<input type="hidden" name="allow_same_as_ship" value="false">
<input type="hidden" name="office_address" value="">
<div style="" class="" >
    <div class="col-xs-12 col-md-6 leftSdeInput" >
        <div >{{trans('homepage.reservation_name')}} <div class="green" style="display:inline-block;">({{trans('homepage.optional')}})</div></div >
        {{ Form::text('reservation_name', '', ['class'=>'reservation_name', 'placeholder' => trans('homepage.reservation_name'), 'required'=>false ]) }}
    </div >
    <div class="col-xs-12 col-md-6 rightSideInput" >
        <div >{{trans('homepage.Hotel Name')}} <div class="green">({{trans('homepage.optional')}})</div></div >
        {{ Form::text('hotel_name', '', ['class'=>'hotel_name', 'placeholder' => trans('homepage.Hotel Name'), 'required'=>false ]) }}
    </div >
    <div class="col-xs-12 col-md-6 leftSdeInput" >
        <div >{{trans('homepage.Checkin Date')}} <div class="green">({{trans('homepage.optional')}})</div></div >
        {{ Form::text('checkin_dt', '', ['class'=>'checkin_dt', 'placeholder' => trans('homepage.Checkin Date'), 'required'=>false ]) }}
    </div >
    <div class="col-xs-12 col-md-6 rightSideInput" >
        <div >{{trans('homepage.Checkout Date')}} <div class="green">({{trans('homepage.optional')}})</div></div >
        {{ Form::text('checkout_dt', '', ['class'=>'checkout_dt', 'placeholder' => trans('homepage.Checkout Date'), 'required'=>false ]) }}
    </div >
    <div class="col-xs-12 col-md-6 leftSdeInput">
        <div>{{trans('homepage.Address')}}</div>
        {{ Form::text('street', array_key_exists('street',$shipping)?$shipping['street']:'', ['class'=>'street', 'placeholder' => trans('homepage.Address'), 'required'=>true ]) }}
    </div>
    <div class="col-xs-12 col-md-6 rightSideInput">
        <div>{{trans('homepage.City')}}</div>
        {{ Form::text('city', array_key_exists('city',$shipping)?$shipping['city']:'', ['class'=>' city', 'placeholder' => trans('homepage.City'), 'required'=>true ]) }}
    </div>
    <div class="col-xs-12 col-md-6 leftSdeInput">
        <div>{{trans('homepage.Postcode')}}</div>
        {{ Form::text('postcode', array_key_exists('postcode',$shipping)?$shipping['postcode']:'', ['class'=>'postcode', 'placeholder' => trans('homepage.Postcode'), 'required'=>true ]) }}
    </div>
</div >
</form>
<div class="col-xs-12" id="errDiv" >
</div >
<br>
<div class="row" style="display: inline-block; width: 100%; min-width: initial;">
    <div class="col-xs-6 bckbuttonHolder" >
        <p class="backToPage1 backButton shipShow col-xs-12" data-active-page=".step3">{{trans('homepage.BACK')}}</p >
    </div >
    <div class="col-xs-6 nextButtonHolder" >
        <p data-active-page=".step4" class="nextToPage3 nextButton nextBtn col-xs-12" >{{trans('homepage.ch_next')}}</p >
    </div >
</div>

<script>
   $(function(){
       $('.checkin_dt').datetimepicker({format:"Y-m-d H:i"});
       $('.checkout_dt').datetimepicker({format:"Y-m-d H:i"});
   });

</script>