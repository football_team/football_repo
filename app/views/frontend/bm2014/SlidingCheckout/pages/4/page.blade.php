<br>
<div class="deliveryCountryBlock">
    @if(count($_country) > 1)
    <h2>{{trans('homepage.Delivery Option')}}</h2>
    {{ Form::select('shipping_country', $_country, 'GB', ['class'=>'input country-code', 'placeholder' => trans('homepage.Country Code'), 'id'=>'country', 'required'=>true] ) }}
    @else
        <h3 style="font-size:20px; display:inline-block; width:100%;">{{trans('homepage.Delivery Option')}} - {{reset($_country)}}</h3>
        <input type="hidden" readonly="readonly" name="shipping_country" id="country" data-name="{{reset($_country)}}" value="@foreach($_country as $key=>$val){{$key}}@endforeach">
    @endif
</div>
<div class="deliveryBlock">
    <div id="shipping_method">
    </div>
</div>
<div id="shipping-form-block">

</div>

<p class="backToPage1 backButton prevBtn col-xs-12" data-active-page=".step3">{{trans('homepage.BACK')}}</p >
@include('frontend.bm2014.SlidingCheckout.partials.scripts.shipping-scripts')