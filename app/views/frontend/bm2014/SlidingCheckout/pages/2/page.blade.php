@include('frontend.bm2014.SlidingCheckout.partials.scripts.login-register-js')

@if($customer)
    @include('frontend.bm2014.SlidingCheckout.pages.2.partials.logged-in')
@else
    @include('frontend.bm2014.SlidingCheckout.pages.2.partials.logged-out')
@endif