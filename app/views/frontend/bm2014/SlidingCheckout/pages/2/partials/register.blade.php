<form action="" id="regForm">
    <div class="col-xs-12 col-md-6 leftSdeInput" >
        <div  class="col-xs-12">{{trans('homepage.ch_firstname')}}</div >
        <input class="col-xs-12 col-md-12 validCheck1" minlength="2" type="text" value="" placeholder="{{trans('homepage.ch_firstname')}}" id="first_name" name="first_name" aria-required="true" required="1" >
    </div >
    <div class="col-xs-12 col-md-6 leftSdeInput" >
        <div  class="col-xs-12">{{trans('homepage.ch_lastname')}}</div >
        <input class="col-xs-12 col-md-12 validCheck2" type="text" value="" placeholder="{{trans('homepage.ch_lastname')}}" name="last_name" id="last_name" aria-required="true" required="1" >
    </div >
    <div class="col-xs-12 col-md-6 leftSdeInput" >
        <div  class="col-xs-12">{{trans('homepage.affilEmail')}}</div >
        {{ Form::email('email', '', ['class'=>'col-xs-12 col-md-12 validCheck3 emailCopy', 'placeholder' => trans('homepage.Email'), 'id'=>'email' ,'required'=>true]) }}
    </div >
    <div class="col-xs-12 col-md-6 leftSdeInput" >
        <div  class="col-xs-12">{{trans('homepage.Confirm Email')}}</div >
        {{ Form::email('email_confirmation', '', ['class'=>'col-xs-12 col-md-12 validCheck4', 'placeholder' => trans('homepage.Confirm Email'),'required'=>true,'id'=>'email_confirmation','equalTo'=>'.emailCopy' ]) }}
    </div >
    <div class="col-xs-12 col-md-6 leftSdeInput" >
        <div  class="col-xs-12">{{trans('homepage.Telephone')}}</div >
        
        <select class="col-xs-12 col-md-12 input country-code validCheck5" placeholder="Country Code" required="1" id="country_code" name="country_code">
            @foreach($country_with_phone as $c)
                @if($c[0] == '0044')
                    <option value="{{$c[0]}}" selected>{{$c[1]}}</option>
                @else
                    <option value="{{$c[0]}}">{{$c[1]}}</option>
                @endif
            @endforeach
        </select>
    </div >
    <div class="col-xs-12 col-md-6 leftSdeInput" >
        <div  class="col-xs-12">{{trans('homepage.Telephone')}}</div >
        {{ Form::text('contact_no', '', ["minlength"=>"8","digits"=>"true",'class'=>'col-xs-12 col-md-12 validCheck6', 'placeholder' => trans('homepage.Mobile Number'), 'id'=>'contact_no','required'=>true ]) }}
    </div >
    <div class="col-xs-12 col-md-6 leftSdeInput" >
        <div  class="col-xs-12">{{trans('homepage.Password')}}</div >
        {{ Form::password('password', ['class'=>'passwordToPass',"minlength"=>"8","required"=>true, 'id'=>'password']) }}</div >
    <div class="col-xs-12 col-md-6 leftSdeInput" >
        <div  class="col-xs-12 col-md-6">{{trans('homepage.Confirm Password')}}</div >
        {{ Form::password('password_confirmation', ['class'=>'passwordToPassSec',"minlength"=>"8","required"=>true, 'id'=>'password_confirmation']) }}
    </div >
    <div class="col-xs-12 col-md-6 leftSdeInput" >
        {{trans('homepage.tncAgree')}} <a href="/terms-conditions" target="_blank">{{trans('homepage.terms and conditions')}}</a>
        {{ Form::checkbox('terms_and_conditions', 'yes') }}
    </div >
    <div class="col-xs-12" id="errDiv"></div>
    <div class="col-xs-6 bckbuttonHolder" >
        <p class="backToPage1 backButton prevBtn col-xs-12" data-active-page=".step2">{{trans('homepage.BACK')}}</p >
    </div >
    <div class="col-xs-6 nextButtonHolder" >
        <p class="nextToPage3 nextButton col-xs-12 reg-btn" >{{trans('homepage.Register')}}</p >
    </div >
</form>
