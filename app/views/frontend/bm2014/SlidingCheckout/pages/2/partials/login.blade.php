<form action="{{route('customer.account.checkout.login')}}" id="login-form" method="post" >
    <div class="col-md-6 leftSdeInput" >
        <div class="col-xs-12" >{{trans('homepage.affilEmail')}}</div >
        <input class="col-xs-12 col-md-12 validCheck1" minlength="2" type="text" value="" placeholder="{{trans('homepage.username')}}" id="username" name="username" aria-required="true" required="1" >
    </div >
    <div class="col-md-6 leftSdeInput" >
        <div class="col-xs-12" >{{trans('homepage.Password')}}</div >
        <input class="col-xs-12 col-md-12 validCheck2" type="password" value="" placeholder="{{trans('homepage.Password')}}" id="password" name="password" aria-required="true" required="1" >
    </div >
    <div class="col-xs-12" id="errDiv">
        <br>
    </div>
    <div class="col-xs-6 bckbuttonHolder" >
        <p class="backToPage1 backButton prevBtn col-xs-12" data-active-page=".step2">{{trans('homepage.BACK')}}</p >
    </div >
    <div class="col-xs-6 nextButtonHolder" >
        <p class="nextButton pull-right login-btn">{{trans('homepage.login')}}</p>
    </div >
</form >