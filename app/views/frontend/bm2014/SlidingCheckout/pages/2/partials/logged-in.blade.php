<form action="/" id="checkoutForm" class="checkoutForm">
<br>
<h2>{{trans('homepage.Personal Details')}}</h2>
<div class="col-xs-12 col-md-6 leftSdeInput" >
    <div  class="col-xs-12">{{trans('homepage.ch_firstname')}}</div >
    <input class="col-xs-12 col-md-12 validCheck1" minlength="2" type="text" value="@if(!empty($customer['firstname'])){{ $customer['firstname'] }}@endif" placeholder="{{trans('homepage.ch_firstname')}}" name="first_name" aria-required="true" required="1" >
</div >
<div class="col-xs-12 col-md-6 leftSdeInput" >
    <div  class="col-xs-12">{{trans('homepage.ch_lastname')}}</div >
    <input class="col-xs-12 col-md-12 validCheck2" type="text" value="@if(!empty($customer['lastname'])){{ $customer['lastname'] }}@endif" placeholder="{{trans('homepage.ch_lastname')}}" name="last_name" aria-required="true" required="1" >
</div >
<div class="col-xs-12 col-md-6 leftSdeInput" >
    <div  class="col-xs-12">{{trans('homepage.Country Code')}}</div >
    <select class="col-xs-12 col-md-12 input country-code validCheck5" placeholder="Country Code" required="1" name="country_code">
        @foreach($country_with_phone as $c)
            @if($c[0] == '0044')
                <option value="{{$c[0]}}" selected>{{$c[1]}}</option>
            @else
            <option value="{{$c[0]}}">{{$c[1]}}</option>
            @endif
        @endforeach
    </select>
</div >
<div class="col-xs-12 col-md-6 leftSdeInput" >
    <div  class="col-xs-12">{{trans('homepage.Phone Number:')}}</div >
        {{ Form::text('contact_no', $customer['telephone']?$customer['telephone']:'', ['class'=>'col-xs-12 col-md-12 validCheck6',"digits"=>"true", 'placeholder' => trans('homepage.Mobile Number'), 'required'=>true ]) }}
</div >
<div class="col-xs-12" id="errDiv" >
</div >
<div class="col-xs-6 bckbuttonHolder" >
    <p class="backToPage1 backButton prevBtn col-xs-12" data-active-page=".step2">{{trans('homepage.BACK')}}</p >
</div >
<div class="col-xs-6 nextButtonHolder" >
    <p data-active-page=".step3" class="nextToPage3 nextButton nextBtn col-xs-12" >{{trans('homepage.ch_next')}}</p >
</div >
</form>