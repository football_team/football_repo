<form id="checkoutForm" class="checkoutForm" action="/">
<div class="framed col-xs-12 col-md-6 hidden-xs" style="height: 250px;" >
    <div style="width:100%; float: left; clear:both;">
        <svg xmlns="http://www.w3.org/2000/svg" style="width: 30% ; max-height:100px; margin-bottom: 15px;" viewBox="0 0 26 26" >
            <path fill="#7EC26D" d="M23.633,5.028c-0.2-0.227-0.481-0.358-0.777-0.366c-2.295-0.06-5.199-2.514-7.119-3.477 c-1.186-0.593-1.969-0.984-2.557-1.087C13.119,0.09,13.06,0.084,13,0.084s-0.119,0.006-0.18,0.015 c-0.588,0.103-1.371,0.494-2.556,1.087C8.344,2.148,5.44,4.602,3.145,4.662C2.849,4.67,2.567,4.802,2.367,5.028 C2.165,5.254,2.061,5.555,2.076,5.862c0.493,10.023,4.088,16.226,10.396,19.831c0.164,0.093,0.346,0.141,0.527,0.141 s0.363-0.048,0.528-0.141c6.308-3.605,9.902-9.808,10.396-19.831C23.939,5.555,23.835,5.254,23.633,5.028z M18.617,8.97 l-5.323,7.855c-0.191,0.282-0.491,0.469-0.788,0.469c-0.298,0-0.629-0.163-0.838-0.372l-3.752-3.753 c-0.255-0.256-0.255-0.671,0-0.926l0.927-0.929c0.256-0.254,0.672-0.254,0.926,0l2.44,2.44l4.239-6.257 c0.202-0.298,0.611-0.375,0.91-0.173l1.085,0.736C18.741,8.263,18.819,8.671,18.617,8.97z" ></path >
        </svg >
    </div>
    <p style="font-weight: bold;font-size: 17px;" >{{trans('homepage.bwclt1')}}</p>
    <p>{{trans('homepage.bwclt2')}}</p>
</div>
<div class="framed col-xs-12 col-md-6 hidden-xs" style="    height: 250px;" >
    <div style="width:100%; float: left; clear:both;">
        <svg version="1" style="width: 30%; max-height:100px;" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24">
            <path d="M22.8,11.4C22.6,11.1,18,5,12,5S1.4,11.1,1.2,11.4c-0.3,0.4-0.3,0.8,0,1.2C1.4,12.9,6,19,12,19s10.6-6.1,10.8-6.4 C23.1,12.2,23.1,11.8,22.8,11.4z M12,17c-2.8,0-5-2.2-5-5c0-2.8,2.2-5,5-5s5,2.2,5,5C17,14.8,14.8,17,12,17z" ></path >
        </svg >
    </div>
    <p style="font-weight: bold;font-size: 17px;" >{{trans('homepage.no-hidden-fees')}}</p>
    <p>{{trans('homepage.no-hidden-fees-long')}}</p>
</div>

<div style="width: 100% !important; float: left;" >
    <div style="width: 100%; float: right; text-align: center; margin-left: auto; margin-right: auto; margin-top: 5px; border-radius: 4px; color: black; padding: 0 !important;">
        <div class="ticketBody" >
            @if(!empty($ticket->info['loc_block']) && $ticket->info['loc_block'] != "" && $ticket->info['loc_block'] != " ")
                <p>{{trans('homepage.Block')}} : {{$ticket->info['loc_block']}}</p>@else
                <p></p>@endif
            @if(!empty($ticket->info['loc_row']) && $ticket->info['loc_row'] != "" && $ticket->info['loc_row'] != " ")
                <p>{{trans('homepage.Row')}} : {{$ticket->info['loc_row']}}</p>@else
                <p></p>
            @endif

            @if($show_fees)
                <p>{{trans('homepage.currencyInUse')}}{{number_format(($ticket->ticket->ticketInformation->price)*(1+(intval(Options::getOption('booking_fees'))/100)), 2)}} each</p>
            @else
                <p>Price {{trans('homepage.currencyInUse')}}{{number_format($ticket->ticket->ticketInformation->price, 2)}} each</p>
                <p><b>Price + Booking Fees ({{Options::getOption('booking_fees')}}%) : {{trans('homepage.currencyInUse')}}{{number_format(($ticket->ticket->ticketInformation->price)*(1+(intval(Options::getOption('booking_fees'))/100)), 2)}} each</b></p>
            @endif

            <p>{{trans('homepage.Form of Ticket')}} : {{$ticket->formOfTicket['title']}}</p>
            <p></p>
            <p></p>
            <p></p>
        </div>
        <div class="ticketBodyRight" >
            <div class="col-xs-6" >{{$ticket->title}}</div>
            <div class="col-xs-6" >{{$ticket->event_location}}</div>
            <br>
            <div class="col-xs-6" >@if(!$ticket->is_tbc){{date('d-m-y, h:ia', strtotime($ticket->datetime))}}@else TBC @endif</div>
            <div class="col-xs-6" >{{$ticket->ticketType['title']}}</div>
            <br>
            <div >{{trans('homepage.Number of Tickets')}}
                <select style="background: darkgrey;border-radius: 15px;margin-top: 10px;float: right;width: 50%;margin-top: -1px;" name="number_of_ticket" class="number-of-ticket-chk" data-preferance="{{$sell_preference}}" data-qty="{{$ticket->available_qty}}" >
                    @foreach($ticketQty as $key=>$val)
                        @if(isset($_GET['n']) && $_GET['n'] == $key )
                            <option value="{{$key}}" selected >{{$val}}</option >
                        @else
                            <option value="{{$key}}" >{{$val}}</option >
                        @endif

                    @endforeach
                </select >
            </div>
            <div class="col-x-12 " STYLE="    font-size: 10PX;
				MARGIN-TOP: 21PX;">@if(!empty($ticket->buyerNot)){{$ticket->buyerNot }}@endif</div>
        </div>
    </div>
    <div class="outerTicketBlock">
        <input type="text" name="voucherCode" id="discountField" placeholder="{{trans('homepage.applyDiscountCode')}}">
        <div class="lowerTicketBlock nextBtn" data-active-page=".step2">{{trans('homepage.Checkout')}}</div>
    </div>
</div>
</form>

<script>
    $(function(){
        $('.ticketq').html("{{reset($ticketQty)}}");
        $('.ticketp').html("{{number_format(reset($ticketQty)*($ticket->ticket->ticketInformation->price*(1+(intval(Options::getOption('booking_fees'))/100))),2,'.','')}}");
        $('.ticketp').data('price', "{{number_format(reset($ticketQty)*($ticket->ticket->ticketInformation->price*(1+(intval(Options::getOption('booking_fees'))/100))),2,'.','')}}")
	@if(Options::getOption('payment_gateway') == 'ezpay')
		var qty = $('.number-of-ticket-chk').val()
		var total = qty * {{ number_format((($ticket->ticket->ticketInformation->price)*(1+(intval(Options::getOption('booking_fees'))/100)))*$conv_rate,2) }};
		var total = total.toFixed(2);
        	$('.gbp').html(total);
        	$('.original').html($('.ticketp').text());
	@endif
   });

    $('.number-of-ticket-chk').change(function(){
        var val = $(this).val();
        var ttl = val * {{number_format(($ticket->ticket->ticketInformation->price)*(1+(intval(Options::getOption('booking_fees'))/100)), 2,'.','')}};
        var ttl = ttl.toFixed(2);
	@if(Options::getOption('payment_gateway') == 'ezpay')
        	var gbp = val * {{ number_format((($ticket->ticket->ticketInformation->price)*(1+(intval(Options::getOption('booking_fees'))/100)))*$conv_rate,2) }};
        	var gbp = gbp.toFixed(2);
	@endif
        $('.ticketq').html(val);
        $('.ticketp').html(ttl);
	@if(Options::getOption('payment_gateway') == 'ezpay')
    		$('.gbp').html(gbp);
    		$('.original').html($('.ticketp').html());
	@endif
        $('.ticketp').data('price', ttl);
    })
</script>
