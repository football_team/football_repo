@extends(Template::name('frontend.%s._layout.layout'))
@section('style')
    @parent
    @include('frontend.bm2014.page_checkout.style')
    @include('frontend.bm2014.page_checkout.form.pageFour.thankyouOnlyStyling')
@stop
@section('script')

    @parent
    @include('frontend.bm2014.SlidingCheckout.partials.scripts.includes')
    @include('frontend.bm2014.SlidingCheckout.partials.scripts.clock')
    @include('frontend.bm2014.SlidingCheckout.partials.scripts.validation')
    @include('frontend.bm2014.SlidingCheckout.partials.scripts.submitform')
    <script>
        $('.checkoutstep').addClass('active');
    </script>
@stop

@section('content')
    <script>
        !function(){"use strict";var t=document,e=function(){var e,r,o;if(t.getElementsByClassName)e=t.getElementsByClassName("trustpilot-widget");else if(t.querySelectorAll)e=t.querySelectorAll(".trustpilot-widget");else{var s=[],a=new RegExp("(^| )trustpilot-widget( |$)"),l=t.body.getElementsByTagName("*");for(r=0,o=l.length;r<o;r++)a.test(l[r].className)&&s.push(l[r]);e=s}for(r=0;r<e.length;++r)for(var i=e[r];i.firstChild;)i.removeChild(i.firstChild)};e(),t.addEventListener?t.addEventListener("DOMContentLoaded",e):t.attachEvent("onreadystatechange",function(){"complete"==t.readyState&&e()});var r="https:"==t.location.protocol?"https:":"http:",o=r+"//widget.trustpilot.com",s=t.createElement("script");s.src=o+"/bootstrap/v5/tp.widget.bootstrap.min.js",s.async=!0,s.defer=!0,t.querySelector("head").appendChild(s)}();
        //# sourceMappingURL=tp.widget.sync.bootstrap.min.js.map
    </script>
    <div style="width: 100%;text-align: left;" >
        <a href="/" >
            <img src="/footballticketpadlogo-whitebackground.png" style="margin: 10px;" >
        </a >
    </div >
    <div class="" >
        <div class="container" >
            <div class="col-md-7" style="" >
                <div class="checkoutarea hidden-xs" >
                    @include('frontend.bm2014.SlidingCheckout.partials.breadcrumbs')
                </div >
                @if(Options::getOption('payment_gateway') == 'tp')
                    <iframe style="overflow-y: hidden;" height="600" scrolling="no" seamless="seamless" width="100%" src="{{Config::get('transactpro.gatewayUrl')}}{{$tid}}" frameborder="0" allowfullscreen ></iframe >
                @elseif(Options::getOption('payment_gateway') == 'g2s' && isset($redirect_link))
                    <iframe style="overflow-y: hidden;" height="1450" scrolling="no" seamless="seamless" width="100%" src="{{$redirect_link}}" frameborder="0" allowfullscreen ></iframe >
                @endif
            </div >
            <div class="col-md-4 col-xs-12" style="padding-left: 20px;" >
                <div class="framed overviewMobile" >
                    <input name="qty" class="qty" value="2" type="hidden" >
                    <div >
                        <span class="ftp-line" >
                            <span class="greenline" ></span >
                            <span class="yellowline" ></span >
                            <span class="pinkline" ></span >
                            <span class="blueline" ></span >
                        </span >
                        <div >
                            <h3 style="font-weight: 700; border:none !important;" >
                                {{$ticket->title}}
                            </h3 >
                            <h3>{{$event->datetime}}</h3>
                        </div >
                        <div >
                            <span class="ticketq" >{{$uip['qty']}}</span >
                            x {{trans('homepage.currencyInUse')}}{{number_format(($ticket->ticket->ticketInformation->price)*(1+(intval(Options::getOption('booking_fees'))/100)), 2)}} {{trans('homepage.currencyAfter')}}
                        </div >
                        <div clas="checkoutMaringSperarateLine" >
                            <div class="currencyBlockCheckout" >{{trans('homepage.Delivery fee')}}: {{trans('homepage.currencyInUse')}}</div><div class="del-cost" data-price="0">{{$uip['ship_cost']}}</div>
                        </div >
                        <div style="font-weight: bold;font-size: 20px;padding-top: 5px; clear: both;" >
                            Final Price :
                            <div class="afterTxt" style="float:right; font-weight: bold; font-size: 20px; padding-top: 5px;" >&nbsp;{{trans('homepage.currencyAfter')}}</div >
                            <div style="float: right ; font-weight: bold;font-size: 20px;padding-top: 5px;" class="ticketp" data-price="0">{{$uip['ttl']}}</div >
                            <div class="currex" style="float: right;font-size: 20px;margin: 5px;" >{{trans('homepage.currencyInUse')}}</div >
            			    @if(Options::getOption('payment_gateway') == 'ezpay')
            			    	<div class="ticketq">GBP value was converted from <span class="original_price">{{$uip['ttl']}}</span> </div>
            			    @endif
                        </div >
                    </div >
                </div >
                <div class="framed hidden-xs" style="float: left;" >
                    <div class="trustpilot-widget" data-locale="en-GB" data-template-id="539ad60defb9600b94d7df2c" data-businessunit-id="54f5f5ad0000ff00057dce30" data-style-height="500px" data-style-width="100%" data-tags="checkout" data-stars="4,5">
                        <a href="https://uk.trustpilot.com/review/footballticketpad.com" target="_blank">Trustpilot</a>
                    </div>
                </div >
            </div >
        </div >
    </div >
@stop

