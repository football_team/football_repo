<br><br>
<h2 class="billingAddressTitle" style="" >{{trans('homepage.applyDiscountCode')}}</h2 >
<form action="/" id="checkoutForm">
    <input type="text" name="voucherCode" id="discountField" placeholder="{{trans('homepage.applyDiscountCode')}}">
</form>
<div class="row" style="display: inline-block; width: 100%; min-width: initial;">
    <div class="col-xs-6 bckbuttonHolder" >
        <p class="backToPage1 backButton prevBtn col-xs-12" data-active-page=".step5">{{trans('homepage.BACK')}}</p >
    </div >
    <div class="col-xs-6 nextButtonHolder" >
        <p data-active-page="" class="nextToPage3 nextButton checkout-final col-xs-12" >{{trans('homepage.Checkout')}}</p >
    </div >
</div>