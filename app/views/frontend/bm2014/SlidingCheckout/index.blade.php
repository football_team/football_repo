@extends(Template::name('frontend.%s._layout.layout'))

@section('style')
    @parent
    @include('frontend.bm2014.SlidingCheckout.partials.styles')
@stop

@section('script')
    @parent
    @include('frontend.bm2014.SlidingCheckout.partials.scripts.includes')
    @include('frontend.bm2014.SlidingCheckout.partials.scripts.clock')
    @include('frontend.bm2014.SlidingCheckout.partials.scripts.framework')
@stop

@section('content')
    @if(Options::getOption('payment_gateway') == 'ezpay')
        <!-- Ezpay css -->
        <link href="https://psp.stg.transactium.com/hps/Content/css/ezpay.css" rel="stylesheet"/>

        <!-- Ezpay scripts -->
        <script src="https://code.jquery.com/jquery-1.11.3.min.js" type="text/javascript"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.14.0/jquery.validate.min.js" type="text/javascript"></script>
        <script src="https://psp.stg.transactium.com/hps/CardToken/ezpay.js?sessionkey={{Ezpay::GetSessionKey(Config::get('ezpay.private_key'))}}"></script>
    @endif
    <div style="width: 100%; text-align: left;" >
        <a href="/" ><img src="/footballticketpadlogo-whitebackground.png" style="margin: 10px;" ></a >
    </div >
    <div class="container" >
        <div class="col-md-4 col-xs-12 checkoutSideBarMainContainer pull-right" style="" >
             @include('frontend.bm2014.SlidingCheckout.partials.sidebar.oderTotal')
        </div >
        <div class="col-md-7" style="" >
            @include('frontend.bm2014.SlidingCheckout.partials.breadcrumbs')
            <div class="checkoutarea" >
                @include('frontend.bm2014.SlidingCheckout.pages.page-container')
            </div >
        </div >
        <div class="col-md-4 col-xs-12 checkoutSideBarMainContainer pull-right" style="" >

            @include('frontend.bm2014.SlidingCheckout.partials.sidebar.peopleOnPage')
            @include('frontend.bm2014.SlidingCheckout.partials.sidebar.helpnote')
            @include('frontend.bm2014.SlidingCheckout.partials.sidebar.timer')
            @include('frontend.bm2014.SlidingCheckout.partials.sidebar.trust')
        </div >
    </div >
@stop
