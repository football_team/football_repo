<?php
/**
 *
 * Javascript Handles the Shipping options provided to the client
 *
 */
?>

<script >
	$(document).ready(function () {
		var shipping_url = '/checkout/ticket/shipping/{{$ticketInfo->event_id}}';
		var delCountry = $('select[name=shipping_country]').find("option:selected").text();
		var pid = "{{$productId}}";
		var ttype = "{{$ticket->formOfTicket['title']}}";

		$.ajax({
			url: shipping_url,
			type: 'post',
			dataType: 'json',
			data: {del_country: delCountry, rtID: pid},
			beforeSend: function () {
				jQuery('#ticket-checkout-form > div > div > div.col-md-7 > div:nth-child(3)').css('display','none');
			},
			success: function (response) {
				if (response.data) {
					var html = '';
					if (response.data.length > 1) {
						html = '<option value="" data-price="0.00">{{trans("homepage.fSelect")}}</option>';
					}
					if (response.data.length == 0) {
						html = '<option value="" data-price="0.00">{{trans("homepage.noSOAvail")}}</option>';
					}
					for (var i = 0; i < response.data.length; i++) {
						html += "<option value='" + response.data[i].value + "' data-price='" + response.data[i].price + "' >" + response.data[i].label + "</option>";
					}
					$('.countrieslist').html(html);
					if (ttype == "E-tickets") {
						$('#hideifeticket').css('display', 'none');
					}
				}
			}
		}).done(function () {
			update_delivery_option();
			jQuery('#ticket-checkout-form > div > div > div.col-md-7 > div:nth-child(3)').css('display','block');
		});
	});

	var update_delivery_option = function () {
		var deliveryFee = parseFloat($('select[name=other_country] option:selected').data('price'));
		$('.delivery-fee').html(deliveryFee.toFixed(2));
		var totalAmount = parseFloat($('.ticket-line-price').html())+deliveryFee;
		$('.ticket-total-price').html(totalAmount.toFixed(2));
	};

	var updateQty = function (elem) {
		var qty = elem.val();
		var ticketPrice = parseFloat($('.single-ticket-price').html().replace(',', ''));
		var ticketLinePrice = qty * ticketPrice;
		var deliveryFee = parseFloat($('.delivery-fee').html());
		$('.ticket-line-price').html(ticketLinePrice.toFixed(2));
		var totalAmount = ticketLinePrice+deliveryFee;
		$('.ticket-total-price').html(totalAmount.toFixed(2));
		$('.qty').val(qty);var ticketLinePrice = parseFloat($('.ticket-line-price').html().replace(',', ''));
		$('.ticket-qty').html(qty);
	}

	$('#country').change(function(){
		var shipping_url = '/checkout/ticket/shipping/{{$ticketInfo->event_id}}';
		var delCountry = $('select[name=shipping_country]').find("option:selected").text();
		var pid = "{{$productId}}";
		var ttype = "{{$ticket->formOfTicket['title']}}";

		$.ajax({
			url: shipping_url,
			type: 'post',
			dataType: 'json',
			data: {del_country: delCountry, rtID: pid},
			beforeSend: function () {
				jQuery('#ticket-checkout-form > div > div > div.col-md-7 > div:nth-child(3)').css('display','none');
			},
			success: function (response) {
				if (response.data) {
					var html = '';
					if (response.data.length > 1) {
						html = '<option value="" data-price="0.00">{{trans("homepage.fSelect")}}</option>';
					}
					if (response.data.length == 0) {
						html = '<option value="" data-price="0.00">{{trans("homepage.noSOAvail")}}</option>';
					}
					for (var i = 0; i < response.data.length; i++) {
						html += "<option value='" + response.data[i].value + "' data-price='" + response.data[i].price + "' >" + response.data[i].label + "</option>";
					}
					$('.countrieslist').html(html);
					if (ttype == "E-tickets") {
						$('#hideifeticket').css('display', 'none');
					}
				}
			}
		}).done(function () {
			update_delivery_option();
			jQuery('#ticket-checkout-form > div > div > div.col-md-7 > div:nth-child(3)').css('display','block');
		});
	})

</script >