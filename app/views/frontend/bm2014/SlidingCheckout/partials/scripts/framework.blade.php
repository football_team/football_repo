<script>
    $(function() {
        initialize();

        /*$zopim(function() {
            $zopim.livechat.appendNotes('----NEW ORDER : START----');

            $zopim.livechat.appendNotes("<?php if (!empty($ticket->title)) {
                echo $ticket->title;
            } ?>");

            $zopim.livechat.appendNotes("<?php if (!empty($ticket->event_location)) {
                echo $ticket->event_location;
            } ?>");

            $zopim.livechat.appendNotes("<?php if (!empty($ticket->ticketType['title'])) {
                echo $ticket->ticketType['title'];
            } ?>");

            $zopim.livechat.appendNotes("<?php if (!empty($ticket->info['loc_block'])) {
                echo $ticket->info['loc_block'];
            } ?>");

            $zopim.livechat.appendNotes("<?php if (!empty($ticket->info['loc_row'])) {
                echo $ticket->info['loc_row'];
            } ?>");

            $zopim.livechat.appendNotes("<?php if (!empty($ticket->formOfTicket['title'])) {
                echo $ticket->formOfTicket['title'];
            } ?> <?php echo number_format(($ticket->ticket->ticketInformation->price)*(1+(intval(Options::getOption('booking_fees'))/100)), 2); ?> <?php echo trans('homepage.currencyAfter'); ?>");

            $zopim.livechat.appendNotes("<?php echo $_SERVER['HTTP_HOST'];?>");


            jQuery('input').change(function(){
                $zopim.livechat.addTags(jQuery('this').attr('name'),jQuery('this').attr('value'));
            });

        });
        */
    });

    $(window).bind("pageshow", function(event) {
        if (event.originalEvent.persisted) {
            window.location.reload()
        }
    });

    function initialize() {
        //This is run to restart whole process
        var url = "/checkout/init/{{$productId}}";
        $.ajax({
            type: "POST",
            url: url,
            success: function(response) {
                $('#sliding-page-container').html(response);
                $('.checkoutstep').removeClass('active');
                $('.step1').addClass('active');
            },
            error: function(response) {
                console.log(response);
            }
        });
    }

    function getNext(page) {
        $('body').append('<div class="ajax-loading-modal-2" style="display:initial;"></div>');
        var url = "/checkout/getNext/{{$productId}}";
        var data = $('#checkoutForm').serialize();
        $('#errDiv').html('');
        $.ajax({
            type: "POST",
            url: url,
            data: data,

            success: function(response) {
                $('.ajax-loading-modal-2').remove();
                $('#sliding-page-container').html(response);
                if (page) {
                    $(page).addClass('active');
                }
            },
            error: function(response) {
                $('.ajax-loading-modal-2').remove();
                var errs = $.parseJSON(response.responseText);

                if ($.type(errs) === "string") {
                    alert(errs);
                    window.location.href = "/";
                }

                $.each(errs, function(key, val) {
                    console.log(key);
                    console.log(val);
                    $('[name="' + key + '"]').css('borderColor', 'red');
                    $('#errDiv').append(val + "<br>");
                });
            }
        });
        //This is run to get the next page
    }

    function getPrev(page) {
        $('body').append('<div class="ajax-loading-modal-2" style="display:initial;"></div>');
        //This is run to go back one step
        var url = "/checkout/getPrev/{{$productId}}";
        $.ajax({
            type: "POST",
            url: url,
            success: function(response) {
                $('.ajax-loading-modal-2').remove();
                $('#sliding-page-container').html(response);
                if (page) {
                    $(page).removeClass('active');
                }
            },
            error: function(response) {
                $('.ajax-loading-modal-2').remove();
                console.log(response);
            }
        });
    }

    function goTo(pageNum) {
        if (parseInt(pageNum) != 'NaN') {
            $('body').append('<div class="ajax-loading-modal-2" style="display:initial;"></div>');
            var url = "/checkout/goto-page/{{$productId}}";
            var p = parseInt(pageNum);

            $.ajax({
                type: 'POST',
                url: url,
                data: {
                    page: p
                },
                success: function(response) {
                    $('.ajax-loading-modal-2').remove();
                    $('#sliding-page-container').html(response);
                },
                error: function(response) {
                    $('.ajax-loading-modal-2').remove();
                    console.log(response);
                }
            });
        } else {
            console.log("PageNumber is not a valid int");
        }
        //This is run to jump back to a chosen step
    }

    $('body').on('click', '.nextBtn', function() {

        if ($(this).data('active-page')) {
            var ap = $(this).data('active-page');
            getNext(ap);

        } else {
            getNext(false);
        }
    });

    $('body').on('click', '.prevBtn', function() {

        if ($(this).data('active-page')) {
            var ap = $(this).data('active-page');
            getPrev(ap);
        } else {
            getPrev(false);
        }
    });

    $('body').on('click', 'input', function() {
        $(this).css('border-color', '#ccc');
    });
    $('body').on('click', 'select', function() {
        $(this).css('border-color', '#ccc');
    });

    $('body').on('change', '#country', function() {
        getShipping();
    });

    function getShipping() {
        var curCountry = $('#country option:selected').text();
        if (curCountry == '') {
            curCountry = $('#country').data('name');
        }
        var url = '/checkout/getShipping/{{$productId}}';
        console.log(url);
        $.ajax({
            type: 'POST',
            url: url,
            data: {
                del_country: curCountry
            },
            success: function(response) {
                $('.ajax-loading-modal-2').remove();
                var methodString = '';
                $.each(response.data, function() {
                    pStr = this.price;
                    if (pStr == '0.00') {
                        pStr = '';
                    } else {
                        pStr = "<br>Price: {{trans('homepage.currencyInUse')}}" + pStr;
                    }
                    methodString = methodString + '<div class="sMethod ' + this.typeD + '" value="' + this.value + '" data-price="' + this.price + '">' +
                        '<div class="svg-holder">' + this.img + '</div><div class="inner-ship">' + this.label + pStr +
                        '<input type="radio" class="sTypeRad" name="shipping_method" value="' + this.value + '" data-price="' + this.price + '"></div></div>';
                });
                $('#shipping_method').html(methodString);
                //then fire the function to get the form
            },
            error: function(response) {
                $('.ajax-loading-modal-2').remove();
                $('#shipping_method').html('<option value="NA" data-price="0">No Shipping Options Available</option>');
                $('#shipping-form-block').html('<br><br><div class="col-xs-12"><p class="backToPage1 backButton prevBtn col-xs-12" data-active-page=".step3">Back</p> </div>')
                console.log(response);
            }
        });
    }

    $('body').on('click', '.sMethod', function() {
        $(this).find('.sTypeRad').prop("checked", true);
        getShippingForm();
    });

    $('body').on('click', '.shipShow', function() {
        $('#shipping-form-block').html('');
        $('.deliveryBlock').show();
        $('.prevBtn').show();
    });

    function getShippingForm() {
        $('body').append('<div class="ajax-loading-modal-2" style="display:initial;"></div>');
        var method = $('input[name=shipping_method]:checked').val();
        var price = $('input[name=shipping_method]:checked').data('price');

        var url = '/checkout/getShippingForm';

        $.ajax({
            url: url,
            type: "POST",
            data: {
                method: method,
            },
            success: function(response) {
                $('.deliveryBlock').hide();
                $('.prevBtn').hide();
                $('.ajax-loading-modal-2').remove();
                $('#shipping-form-block').html(response);
                var country = $('#country').val();
                $('#cntry').val(country);
                $('.del-cost').html(price);
                var aCost = $('.ticketp').data('price');
                aCost = parseFloat(aCost) + parseFloat(price);
                aCost = aCost.toFixed(2);

                $('.ticketp').html(aCost);
                @if(Options::getOption('payment_gateway') == 'ezpay')
                    var gbpCost = aCost * {{@$conv_rate}};
                    gbpCost = parseFloat(gbpCost).toFixed(2);
                    $('.original').html($('.ticketp').text());
                    $('.gbp').html(gbpCost);
                @endif
            },
            error: function(response) {
                $('.ajax-loading-modal-2').remove();
                console.log(response);
            }
        })
    };

    $('body').on('click', '.checkout-final', function() {

        var pay_method = "{{Options::getOption('payment_gateway') }}";
        if (pay_method == 'ezpay') {
            EZPAY.API.getOrCreateCurtain();
            $('body').append('<div class="ajax-loading-modal-ezpay"><table class="ezpay-processing"><tbody><tr><td><img src="https://cdn1.footballticketpad.com/frontend/bm2014/images/ezpay-processing.png"></td></tr></tbody></table></div>');
	        var form = $('#checkoutForm');
            function onSuccessfulTokenize(data) {
                // handle successful payments here
                // store the token in the form such that it is submitted
                $('#token').val(data.token);
                $('#cc_number').val($('#ez_card').val());
                $('#cc_expire').val($('#ez_expiry').val());
                $('#cc_cvv').val($('#ez_cvv').val());
                $('#errDiv').html('');
                var url = "/checkout/getNext/{{$productId}}";
                var data = $('#checkoutForm').serialize();
                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function(response) {
                        // console.log(response);
                        window.location.href = response;
                    },
                    error: function(response) {
                        EZPAY.API.getOrCreateCurtain().fadeOut();
                        $('.ajax-loading-modal-ezpay').remove();
                        var errs = $.parseJSON(response.responseText);
                        $('#ez_card').removeAttr('disabled');
                        $('#ez_cvv').removeAttr('disabled');
                        $('#ez_expiry').removeAttr('disabled');

                        if (errs && errs.Error && errs.Error.Message) {
                            $('#errDiv').html(errs.Error.Message + "<br>");
                        }else{
                            $.each(errs, function(key, val) {
                                $('[name="' + key + '"]').css('borderColor', 'red');
                                $('#errDiv').append(val + "<br>");
                            });
                        }
                        // reverse step count
                        $.ajax({type:"POST", url:"/checkout/getPrev/{{$productId}}"});
                    }
                });
            }

            function onFailedTokenize(jqxhr, textstatus, errorthrown) {
                var errorMsg = "Payment gateway error";
                debugger;
                if(errorthrown && errorthrown.Message){
                    errorMsg = errorthrown.Message;
                }
                // handle failed payments here
                alert("Error-token:" + errorMsg);
                EZPAY.API.getOrCreateCurtain().fadeOut();
                $('.ajax-loading-modal-ezpay').remove();
                $.ajax({type:"POST", url:"/checkout/getPrev/{{$productId}}"});
            }

            function validateForm() {
                // validation code goes here
                // return true only if validation is successful
                return true;
            }
            if (validateForm() === true) {
                $("[data-ezpay-token-type=amount-decimal]").val($(".gbp").text() || $(".ticketp").text());
                EZPAY.API.tokenizeForm(form)
                    .done(onSuccessfulTokenize)
                    .fail(onFailedTokenize);
            }
        } else {
            $('body').append('<div class="ajax-loading-modal-2" style="display:initial;"></div>');
            var url = "/checkout/getNext/{{$productId}}";
            var data = $('#checkoutForm').serialize();
            $('#errDiv').html('');
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function(response) {
                    $('.ajax-loading-modal-2').remove();
                    window.location.href = response.link;
                },
                error: function(response) {
                    $('.ajax-loading-modal-2').remove();
                    var errs = $.parseJSON(response.responseText);
                    console.log(errs);
                    $.each(errs, function(key, val) {
                        console.log(key);
                        console.log(val);
                        $('[name="' + key + '"]').css('borderColor', 'red');
                        $('#errDiv').append(val + "<br>");
                    });
                }
            });
        }
    });

    //now when we do the shipping select just get the different forms and we're done!


    //Get log in
    //Get register
    //Log in

    //Register
    $(function() {

        $('body').on('click', '.newUserButton', function() {
            $('body').append('<div class="ajax-loading-modal-2" style="display:initial;"></div>');
            //Ajax for the register page
            $.ajax({
                type: 'POST',
                url: '/checkout/getRegister/',
                success: function(response) {
                    $('#lreg-container').html(response);
                    $('.ajax-loading-modal-2').remove();
                },
                error: function(response) {
                    console.log(response);
                    $('.ajax-loading-modal-2').remove();
                }
            });

        });

        $('body').on('click', '.returningUserButton', function() {
            $('body').append('<div class="ajax-loading-modal-2" style="display:initial;"></div>');
            $.ajax({
                type: 'POST',
                url: '/checkout/getLogin/',
                success: function(response) {
                    $('#lreg-container').html(response);
                    $('.ajax-loading-modal-2').remove();
                },
                error: function(response) {
                    console.log(response);
                    $('.ajax-loading-modal-2').remove();
                }
            });
        });

        $('body').on('click', '.login-btn', function() {
            login();
        });

        $('body').on('click', '.reg-btn', function() {
            register();
        });

        $('body').on('change', '#is_same', function() {
            if ($(this).is(':checked')) {
                $('#cThis').val("true");
                $('#htis').css('display', 'none');
            } else {
                $('#cThis').val("false");
                $('#htis').css('display', 'initial');
            }
        });

        function login() {
            var data = $('#login-form').serialize();
            var url = "/customer/account/checkout/login";
            $('body').append('<div class="ajax-loading-modal-2" style="display:initial;"></div>');
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function(response) {
                    $('.ajax-loading-modal-2').remove();
                    goTo(2);
                },
                error: function(response) {
                    $('#errDiv').html(response.responseText);
                    $('.ajax-loading-modal-2').remove();
                }
            });
        }

        function register() {
            var data = $('#regForm').serialize();
            var url = "/checkout/register";
            $('body').append('<div class="ajax-loading-modal-2" style="display:initial;"></div>');
            $('#lreg-container input').css('border-color', '#ccc');

            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function(response) {
                    $('.ajax-loading-modal-2').remove();
                    goTo(2);
                },
                error: function(response) {
                    $('.ajax-loading-modal-2').remove();
                    var resArray = $.parseJSON(response.responseText);
                    console.log(resArray);
                    if ($.type(resArray) != "string") {
                        $('#errDiv').html('');
                        $.each(resArray, function(key, value) {
                            if (value.length > 0) {
                                $('[name="' + key + '"]').css('border-color', "red");
                                $('#errDiv').append(value + "<br>");
                            }
                        });
                    } else {
                        $('#errDiv').html(response.responseText);
                    }
                }
            });
        }
    });
</script>
