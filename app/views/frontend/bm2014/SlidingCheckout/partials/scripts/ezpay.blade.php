<script>
console.log("this is not used");
function onSubmit(event) {
	event.preventDefault();
	var form = $(this);

	function onSuccessfulTokenize(data) {
		// handle successful payments here

		// store the token in the form such that it is submitted
		$('#token').val(data.token);

		// unbind the form to avoid recursion
		form.unbind('submit', onSubmit);
		form.submit();
	}

	function onFailedTokenize(jqXHR, textStatus, errorThrown) {
		// handle failed payments here
		alert("Tokenize Failed : " + textStatus + " - " + errorThrown);
	}

	function validateForm() {
		// validation code goes here
		// return true only if validation is successful
		return true;
	}

	if (validateForm() === true) {

		EZPAY.API.getOrCreateCurtain();

		EZPAY.API.tokenizeForm(form)
			.done(onSuccessfulTokenize)
			.fail(onFailedTokenize);
	}

};

//following assumes form is already defined, otherwise code in Document.Ready
$('#checkoutForm').bind('submit', onSubmit);
</script>



