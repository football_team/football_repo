
<div class="framed overviewMobile" >
	<input name="qty" class="qty" value="2" type="hidden" >
	<div >
		<span class="ftp-line" >
    	<span class="greenline" ></span >
        <span class="yellowline" ></span >
        <span class="pinkline" ></span >
        <span class="blueline" ></span >
    </span >
		<div >
			<h3 style="font-weight: 700; border:none !important;" >
				{{$ticket->title}}
			</h3 >
			<h3>@if(!$event->is_tbc){{$event->datetime}}@else TBC @endif</h3>
		</div >
		<div >
			<span class="ticketq" ></span >
			x {{trans('homepage.currencyInUse')}}{{number_format(($ticket->ticket->ticketInformation->price)*(1+(intval(Options::getOption('booking_fees'))/100)), 2)}} {{trans('homepage.currencyAfter')}} (Including booking fees {{Options::getOption('booking_fees')}}%)
		</div >
		<div clas="checkoutMaringSperarateLine" >
		<div class="currencyBlockCheckout" >{{trans('homepage.Delivery fee')}}: {{trans('homepage.currencyInUse')}}</div><div class="del-cost" data-price="0">0</div>
		</div >
		<div style="font-weight: bold; font-size: 20px; padding-top: 5px; clear: both;" >
			{{trans('homepage.Final price')}} :<br><br>
			<div class="currex" style="display:inline; font-size: 20px;" >{{trans('homepage.currencyInUse')}}</div >
			<div style="display:inline; font-weight: bold;font-size: 20px;padding-top: 5px;" class="ticketp" data-price="0"></div >
			<div class="afterTxt" style="display:inline; font-weight: bold; font-size: 20px; padding-top: 5px;" >&nbsp;{{trans('homepage.currencyAfter')}}</div >
			@if(Options::getOption('payment_gateway') == 'ezpay' && App::getLocale() != 'en_gb')
				<style>
					.gbp-rate{margin-top:15px;}
					.gbp-rate>span{line-height: normal; font-weight: normal;}
				</style>
				<div class="gbp-rate">
					<div><span>{{trans('homepage.ccYouWillBeCharged')}}</span></div>
					<span>
						<b>GBP <span class="gbp"></span></b>
						<span class="hidden">({{trans('homepage.currencyInUse')}} <span class="original"></span> {{trans('homepage.currencyAfter')}})</span>
						@ 1 GBP = {{trans('homepage.currencyInUse')}} {{number_format($gbp_rate,2)}} {{trans('homepage.currencyAfter')}}
					</span>
				</div>
            @endif
		</div >
	</div >
</div >
