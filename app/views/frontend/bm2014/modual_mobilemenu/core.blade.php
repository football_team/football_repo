
<div class="mobileMenu" style="display: none;">

    <div class="col-xs-12 mobileMenuLong mobileMenuClose"><img class="iconRight" src="https://cdn1.footballticketpad.com/svg/back.svg"></div>

    <!-- SEARCH -->
    <div class="col-xs-12 mobileMenuLong" style="
    margin: 0;
    padding: 0;
    height: auto;
">
    <form action="/ticket-search" style="
    margin: 0;
    padding: 15px;
"><input type="text" name="search" placeholder="{{trans('homepage.search')}}" style="
    margin: 0 !important;
    padding: 0 !important;
    color: black !important;
    float: left;
    border-radius: 0px !important;"></form>
    </div>

    <!--LOGINS -->
    @if(!isset($customer['firstname']))
    <div class="col-xs-6 mobileMenuItem mobileMenuLeft"><a style="font-size:10px; padding:0px !important;" href="/login">{{trans('homepage.login')}}</a></div>
    <div class="col-xs-6 mobileMenuItem mobileMenuRight"><a style="font-size:10px; padding:0px !important;" href="/registration">{{trans('homepage.Register')}}</a></div>
    @else
    <div class="col-xs-6 mobileMenuItem mobileMenuLeft"><a href="{{url('account/listing')}}">ACCOUNT</a></div>
    <div class="col-xs-6 mobileMenuItem mobileMenuRight"><a href="{{url('customer/account/logout')}}">{{trans('homepage.logout')}}</a></div>
    @endif

    <div class="col-xs-12 mobileMenuLong mobileMenuTopGames"><p
                class="mobileMenuTextLeft">{{trans('homepage.hot_tickets_header')}}</p><img class="iconRight"
                                                                                            src="https://cdn1.footballticketpad.com/svg/add.svg">
    </div>


    <!-- HOT TICKETS -->
    <div class="mobileMenuHotTickets" style="display:none; margin: 0; padding: 0;">
    @foreach($hotTicketsMobileMenu as $ticket)
        <div class="col-xs-12 mobileMenuLong mobileMenuSelectedItem">
            <a href="{{ FootBallEvent::getUrl($ticket) }}"><p class="mobileMenuTextLeft">
                    {{$ticket->title}}
                </p></a>
        </div>
    @endforeach
    </div>


    <div class="col-xs-12 mobileMenuLong mobileMenuTopTeams"><p
                class="mobileMenuTextLeft">{{trans('homepage.top_teams_header')}}</p><img class="iconRight"
                                                                                          src="https://cdn1.footballticketpad.com/svg/add.svg">
    </div>
    <!-- HOT TEAMS -->
    <div class="mobileMenuHotTeams" style="display:none; margin: 0; padding: 0;">
        @foreach($featureClubMenu as $ticket)
            <div class="col-xs-12 mobileMenuLong mobileMenuSelectedItem">
                <a href="/group/club/{{$ticket->slug}}"><p class="mobileMenuTextLeft" >
                    {{$ticket->title}}
                </p></a>
            </div>
        @endforeach
    </div>


    <div class="col-xs-12 mobileMenuLong mobileMenuTopLeague"><p class="mobileMenuTextLeft">{{trans('homepage.tournaments_header')}}</p><img
                class="iconRight" src="https://cdn1.footballticketpad.com/svg/add.svg"></div>
    <!-- HOT LEAGUES -->
    <div class="mobileMenuLeague" style="display:none; margin: 0; padding: 0;">
        @foreach($featureTournaments as $ticket)
            <div class="col-xs-12 mobileMenuLong mobileMenuSelectedItem">
                <a href="/group/league/{{$ticket->slug}}"><p class="mobileMenuTextLeft" >
                    {{$ticket->title}}
                </p></a>
            </div>
        @endforeach
    </div>


    <div class="col-xs-12 mobileMenuLong"><a href="/faq"><p class="mobileMenuTextLeft">{{trans('homepage.faq')}}</p></a>
    </div>
    <div class="col-xs-12 mobileMenuLong"><a href="/about"><p
                    class="mobileMenuTextLeft">{{trans('homepage.aboutus')}}</p></a></div>
    <div class="col-xs-12 mobileMenuLong"><a href="/contact-us"><p class="mobileMenuTextLeft">{{trans('homepage.contactus')}}</p></a></div>
    <div class="col-xs-12 mobileMenuLong"><a href="/corporate"><p class="mobileMenuTextLeft">{{trans('homepage.Corporate')}}</p></a></div>

    <div class="col-xs-6 mobileMenuItem mobileMenuLeft" style="padding:10px !important;"><a
                href="tel:{{trans('homepage.tnumber')}}" style="font-size:11px; padding:5px 10px !important;">{{trans('homepage.callus')}}</a></div>
    <div class="col-xs-6 mobileMenuItem mobileMenuRight" style="padding:10px !important;"><a
                href="mailto:{{trans('homepage.contact-email')}}" style="font-size:11px; padding:5px 10px !important;">{{trans('homepage.emailus')}}</a></div>
</div>