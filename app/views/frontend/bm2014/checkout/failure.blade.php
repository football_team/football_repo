<!---------main content------------>
<div class="row">
    <div class="site-content">
        <h2>{{trans("homepage.Error")}}</h2>
        {{trans("homepage.checkoutError")}}
        <br />
        {{ $node->content }}
    </div>
</div>
<!---------main content------------>



<script>
    window.parent.updatePageTitle('{{trans("homepage.Error")}}');
</script>
