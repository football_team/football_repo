<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
        <title>Payment Error Page</title>
        <link href="{{URL::to('/assets/frontend/bm2014/css/ezpay-success.css')}}" rel="stylesheet">
    </head>
    <body class="outer-body">

        @include(Template::name('frontend.%s._layout.header'))
        <link media="all" type="text/css" rel="stylesheet" href="https://cdn2.footballticketpad.com/frontend/bm2014/css/foundation.min.css?1.0">
        <link media="all" type="text/css" rel="stylesheet" href="https://cdn2.footballticketpad.com/frontend/bm2014/css/jquery-ui/jquery-ui.min.css?1.0">
        <link media="all" type="text/css" rel="stylesheet" href="https://cdn2.footballticketpad.com/frontend/bm2014/css/main.min.css?1.0">
        <link rel="stylesheet" type="text/css" href="https://cdn2.footballticketpad.com/css/animate.css">
        @include('frontend.bm2014.modual_mobilemenu.core')
        <br style="clear:both;"/>
        <div class="bts-popup-container">
            <div class="popup-valid err">
                <h1><img src="https://cdn2.footballticketpad.com/frontend/bm2014/images/cross.png" alt="error"/></h1>
                <h2>{{trans("homepage.paymentError")}}</h2>
            </div>
            <div class="message-comein">
                <div>
                    {{trans("homepage.checkoutError")}}
                    <br /><br />
                    {{ $node->content }}
                </div>
                <div class="msgCTA">
                    <b>{{trans("homepage.CC: call your bank to resolve the incident or try again")}}</b>
                </div>
                <button onclick="javascript:window.history.back();">{{trans("homepage.tryAgain")}}</button>
                <br />
                <br />
                <a href="{{URL::to('/')}}">{{trans("homepage.cc: homepage button")}}</a>
            </div>

        </div>
        <script>
        var auto_complete_source=null;
        </script>
        <script src="https://cdn3.footballticketpad.com/frontend/bm2014/js/modernizr.foundation.js?1.0"></script>
        <script src="https://cdn3.footballticketpad.com/frontend/bm2014/js/jquery.2.0.3.js?1.0"></script>
        <script src="https://cdn3.footballticketpad.com/frontend/bm2014/js/foundation.min.js?1.0"></script>
        <script src="https://cdn3.footballticketpad.com/frontend/bm2014/js/jquery-ui.min.js?1.0"></script>
        <script src="https://cdn3.footballticketpad.com/frontend/bm2014/js/app.js?1.0"></script>
        <script src="https://cdn3.footballticketpad.com/frontend/bm2014/js/stack/stacktable.js?1.0"></script>
        <script src="https://cdn3.footballticketpad.com/frontend/bm2014/js/foundation-5.4/foundation.min.js?1.0"></script>
        <script src="https://cdn3.footballticketpad.com/frontend/bm2014/js/underscore.min.js?1.0"></script>
        <script src="https://cdn3.footballticketpad.com/frontend/bm2014/js/core.js"></script>
    </body>
</html>