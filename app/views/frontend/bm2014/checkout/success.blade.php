
<html>
<head>
    {{
        Assets::setStyles(
        [
            'foundation'         => 'css/foundation.min.css',
            'main'               => 'css/main.css',
            'jquery-ui'          => 'css/jquery-ui/jquery-ui.min.css',
        ], false, true);
    }}
    <!-- Included CSS Files (Compressed) -->
    {{ Assets::dumpStyles(Config::get('bondcms.assets_debug')) }}
</head>
<body>
<!---------main content------------>
@if(isset($orderInfo))
    <div class="row">
        <div class="site-content" style="min-height:200px;">
            <h2>{{trans("homepage.Purchase confirmation")}}</h2>
            {{trans("homepage.pc1")}}<br><br>
            @if(isset($orderInfo['order_id']))
                {{trans("homepage.pc2")}} {{{$orderInfo['order_id'] or 'Invalid'}}}.
            @endif
            <br /><br>
            {{trans("homepage.pc3")}}
            <br />
            <br>
            <br><br>
            {{trans("homepage.pc4")}}<br><br>
            <div class="addthis_sharing_toolbox"></div>
            <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-4f426a07524dc6b4"></script>
        </div>
    </div>
    <!---------main content------------>
    @if(isset($orderInfo['order_id']))
    <img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/964655791/?value={{$orderInfo['amount']}}&amp;currency_code=GBP&amp;label=NEV5CMuIoVsQr_X9ywM&amp;guid=ON&amp;script=0&amp;oid={{$orderInfo['order_id']}}"/>

    <img src="https://www.clixGalore.com/AdvTransaction.aspx?AdID=15947&SV={{number_format($orderInfo['amount'],2,'.','')}}&OID={{$orderInfo['order_id']}}" height="0" width="0" border="0">
    <br><br><br>
    @endif
    <script>
        window.parent.updatePageTitle('{{trans("homepage.Success")}}');
    </script>

    <script type="text/javascript">
        window.addEventListener('load',function(){

            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function()
                {(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})
            (window,document,'script','//www.google-analytics.com/analytics.js','ga');ga('create',"{{ trans('homepage.ggtrackingcode') }}",'auto');ga('send','pageview');


            ga('require', 'ecommerce');
            ga('ecommerce:addTransaction', {
              'id': "{{$orderInfo['order_id']}}",                     // Transaction ID. Required.
              'affiliation': 'Football Ticket Pad',   // Affiliation or store name.
              'revenue': '{{number_format($orderInfo["amount"],2,'.','')}}',
                'shipping': '{{number_format($orderInfo['amount']-($orderInfo['listed_amount']*$orderInfo['qty']),2,'.','')}}',                  // Shipping.
              'tax': '0'                     // Tax.
            });


            ga('ecommerce:addItem', {
              'id': "{{$orderInfo['order_id']}}",                     // Transaction ID. Required.
              'name': 'Football ticket',    // Product name. Required.
              'sku': "{{$orderInfo['product_id']}}",                 // SKU/code.
              'category': 'NA',         // Category or variation.
              'price': "{{{number_format($orderInfo['listed_amount'],2,'.','') or '0.00'}}}",                 // Unit price.
              'quantity': "{{{$orderInfo['qty'] or '1'}}}"                   // Quantity.
            });
            ga('ecommerce:send');

        });
        </script>
@endif
</body>
</html>