
<html>
<head>
    {{
        Assets::setStyles(
        [
            'foundation'         => 'css/foundation.min.css',
            'main'               => 'css/main.css',
            'jquery-ui'          => 'css/jquery-ui/jquery-ui.min.css',
        ], false, true);
    }}
    <!-- Included CSS Files (Compressed) -->
    {{ Assets::dumpStyles(Config::get('bondcms.assets_debug')) }}
</head>
<body>
<!---------main content------------>
@if(isset($orderInfo))
    <div class="row">
        <div class="site-content" style="min-height:200px;">
            <h2>{{trans("homepage.Purchase confirmation")}}</h2>
            {{trans("homepage.pc1")}}<br><br>
            @if(isset($orderInfo['order_id']))
                {{trans("homepage.pc2")}} {{$orderInfo['order_id']}}.
            @endif
            <br /><br>
            {{trans("homepage.pc3")}}
            <br />
            <br>
            <br><br>
            {{trans("homepage.pc4")}}<br><br>
            <div class="addthis_sharing_toolbox"></div>
                 <!-- Go to www.addthis.com/dashboard to customize your tools -->
            <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-4f426a07524dc6b4"></script>
        </div>
    </div>
    <!---------main content------------>
@endif
</body>
</html>