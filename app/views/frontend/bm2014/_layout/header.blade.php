{{ Assets::jsStart() }}
<script type="text/javascript">
setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>
{{ Assets::jsEnd() }}
<!-- TrustBox script -->
<script type="text/javascript" src="//widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js" async></script>
<!-- End Trustbox script -->
<div id="topheader">
    <div class="tel-no diskonlyTel  hidden-xs othertel"><span class="telpink">t:</span> <a
                href="tel:{{trans('homepage.tnumber')}}">{{trans('homepage.tnumber')}}</a></div>
    <div class="tel-no diskonlyTel maintel hidden-xs"><span class="telpink">e:</span> <a
                href="mailto:info@footballticketpad.com">{{trans('homepage.contact-email')}}</a></div>
    <!-- TrustBox widget - Micro Star -->

    <div class="trustpilot-widget" data-locale="en-GB" data-template-id="5419b732fbfb950b10de65e5" data-businessunit-id="54f5f5ad0000ff00057dce30" data-style-height="20px" data-style-width="300px" data-theme="dark">
        <a href="https://uk.trustpilot.com/review/footballticketpad.com" target="_blank">Trustpilot</a>
    </div>
    <!-- End TrustBox widget -->
</div>
<header>
    <div class="visible-xs menuGo menuGoZone"><img class="menuGo" style="
    width:50px;" src="https://cdn1.footballticketpad.com/svg/menu.svg"></div>
    <a class="logoa" href="/">
        <img class="logo" src="@if(isset($locale_logo))  https://cdn1.footballticketpad.com/frontend/bm2014/images/{{$locale_logo}} @else https://cdn1.footballticketpad.com/frontend/bm2014/images/main_logo.png @endif" alt="Football Ticket Pad" />
    </a>
    <div class="contain hidden-xs">
        <ul class="nav">
            <li class="parent">
                <a href="#">{{trans('homepage.hot_tickets_header')}} &#x2304;</a>
                <ul class="nav child">
                    @foreach($featureEvents as $n=>$event)
                        <li>
                            <a href="{{$event->url}}">{{$event->title}}</a>
                        </li>
                    @endforeach
                </ul>
            </li>

            <li class="parent">
                <a href="#">{{trans('homepage.top_teams_header')}} &#x2304;</a>
                <ul class="nav child">
                    @foreach($featureClub as $n => $club)
                        @if($n%7 == 0) <div class="navrow"> @endif
                            <li>
                                <a href="/group/club/{{$club->slug}}"><img src="{{$club->value}}">{{$club->title}}</a>
                            </li>
                        @if($n%7 == 6) </div> @endif
                    @endforeach
                </ul>
            </li>

            <li class="parent">
                <a href="#">{{trans('homepage.Country')}} &#x2304;</a>
                <ul class="nav child">
                    @foreach($internationalClub as $n=>$club)
                        <li>
                            <a href="/group/club/{{$club->slug}}"><img src="{{$club->value}}">{{$club->title}}</a>
                        </li>
                    @endforeach
                </ul>
            </li>


            <li class="parent">
                <a href="#">{{trans('homepage.tournaments_header')}} &#x2304;</a>
                <ul class="nav child">
                    @foreach($featureTournaments as $n=>$club)
                        <li>
                            <a href="/group/league/{{$club->slug}}"><img src="{{$club->value}}">{{$club->title}}</a>
                        </li>
                    @endforeach
                </ul>
            </li>
            <li><a href="/contact-us">{{trans('homepage.Contact Us')}}</a></li>
            @if($customer === null)
                <li><a href="/login">{{trans('homepage.rLogin')}}</a></li>
            @else
                <li><a href="/account/listing">{{trans('homepage.account_button')}}</a></li>
                <li><a href="/customer/account/logout">{{trans('homepage.logout')}}</a></li>
            @endif
            <li class="parent">
                <a href="#" class="search-expand">
                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 129 129" xmlns:xlink="http://www.w3.org/1999/xlink" enable-background="new 0 0 129 129">
                        <g>
                            <path d="M51.6,96.7c11,0,21-3.9,28.8-10.5l35,35c0.8,0.8,1.8,1.2,2.9,1.2s2.1-0.4,2.9-1.2c1.6-1.6,1.6-4.2,0-5.8l-35-35   c6.5-7.8,10.5-17.9,10.5-28.8c0-24.9-20.2-45.1-45.1-45.1C26.8,6.5,6.5,26.8,6.5,51.6C6.5,76.5,26.8,96.7,51.6,96.7z M51.6,14.7   c20.4,0,36.9,16.6,36.9,36.9C88.5,72,72,88.5,51.6,88.5c-20.4,0-36.9-16.6-36.9-36.9C14.7,31.3,31.3,14.7,51.6,14.7z"/>
                        </g>
                    </svg>
                </a>
                <ul class="nav child searchdrop">
                    <li>
                        <div class="searcharea">
                            <form id="form-home" action="/ticket-search" method="get">
                                <input class="forminput ui-autocomplete-input" name="search" autocomplete="off">
                                <input class="bigbox-search-button" type="submit">
                            </form>
                        </div>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</header>

<style>
    #topheader{
        display: none;
        position: fixed;
        width: 100%;
        top: 0px;
        left: 0;
        right: 0;
        height: 30px;
        background-color: rgba(0,0,0,0.4);
        z-index: 100;
    }

    .contain > ul.nav, .contain > ul.nav > li.parent > ul.nav.child{
        list-style:none;
    }

    .contain > ul.nav > li{
        display:inline-block;
        margin-top:-10px;
    }

    .contain > ul.nav > li > a{
        padding:8px;
        color:#fff!important;
        font-size:14px;
        display:block;
        height:65px;
        padding-top:23px;
    }

    .contain > ul.nav > li.parent > ul.nav.child{
        display:none;
    }

    .contain > ul.nav > li.parent:hover >ul.nav.child{
        display:block;
        position:absolute;
        margin-left:0px;
        list-style:none;
        padding:0px 15px 5px 15px;
        background-color:rgba(0,0,0,0.9)
    }

    .nav.child img{
        height:15px;
        margin-right:10px;
    }

    .contain > ul.nav > li.parent:hover >ul.nav.child > li, .contain > ul.nav > li.parent:hover >ul.nav.child > .navrow > li{
        margin-top:10px;
        text-align:left;
    }

    .contain > ul.nav > li.parent.focused >ul.nav.child > li{
        margin-top:10px;
        text-align:left;
    }

    .contain > ul.nav > li.parent:hover > ul.nav.child > li > a, .contain > ul.nav > li.parent:hover > ul.nav.child > .navrow > li > a{
        color:#fff !important;
    }

    .contain > ul.nav > li.parent:hover > ul.nav.child > .navrow{
        float:left;
        margin-right:20px;
        min-width:150px;
    }

    .navrow-title {
        color: #fff;
        font-size: 15px;
        border-bottom: 1px solid #fff;
        padding: 10px 0 8px 0;
    }

    .contain > ul.nav > li.parent:hover > ul.nav.child > .navrow:last-child{
        margin-right:0px;
    }

    .contain > ul.nav > li.parent:hover >ul.nav.child.searchdrop{
        right:0px;
        padding-bottom:5px;
        background:none;
        padding:0px 0px 5px 15px;
    }

    .contain > ul.nav > li.parent.focused >ul.nav.child.searchdrop{
        right:0px;
        padding-bottom:5px;
        background:none;
        padding:0px 0px 5px 15px;
        display:block;
        position:absolute;
        margin-left:0px;
        list-style:none;
    }

    .search-expand{
        padding:none;
        padding-left:5px;
    }

    .search-expand svg{
        fill:#fff;
        display:inline-block;
        height:15px;
        width:15px;
    }

    .logo{
        max-width:180px !important;
    }

    header{
        height:65px !important;
        position:fixed !important;
        top:0px !important;
        left:0 !important;
        right:0 !important;
    }

    .flexslider{
        width:100vw;
    }

    .flexslider #controls{
        display:none;
        position: absolute;
        right: 0;
        top: 160px;
        direction:rtl;
        z-index:11;
    }

    .flexslider #controls .control{
        display: block;
        background-color: rgba(0,0,0,0.5);
        height:80px;
        padding: 5px;
        margin-bottom:10px;
        min-width:65px;
        max-width:65px;

        -webkit-transition: background-color 0.5s ease;
        -moz-transition: background-color 0.5s ease;
        -o-transition: background-color 0.5s ease;
        transition: background-color 0.5s ease;
    }

    .flexslider #controls .control img{
        display:inline-block;
        margin-top:10px;
        max-width:50px;
        max-height:50px;
        margin-left:10px;
    }

    .flexslider #controls .control .slide-title{
        position:relative;
        top:-18px;
        margin-left:20px;
        margin-right:20px;
        display:none;
        color:#fff !important;
    }

    .flexslider #controls .control.active{
        /*max-width:none;*/
        background-color:rgba(0, 255, 45, 0.4);
    }

    .flexslider #controls .control.active .slide-title{
        /*display:inline-block;*/
    }

    .flexslider #controls .control:hover{
        max-width:none;
    }

    .flexslider #controls .control:hover .slide-title{
        display:inline-block;
    }

    .flexslider .slider-image .slide{
        position:absolute;
        left:-100vw;
        top:0px;

        -webkit-transition: left 0.5s ease 0.5s;
        -moz-transition: left 0.5s ease 0.5s;
        -o-transition: left 0.5s ease 0.5s;
        transition: left 0.5s ease 0.5s;
    }

    .flexslider .slider-image .slide.active{
        left:0px;
        z-index:10;

        -webkit-transition: left 0.5s ease 0s;
        -moz-transition: left 0.5s ease 0s;
        -o-transition: left 0.5s ease 0s;
        transition: left 0.5s ease 0s;
    }

    @media only screen and (min-width: 1239px) {
        .flexslider #controls{
            display:block;
        }
    }

    @media only screen and (min-width: 768px) {
        header{
            height:65px !important;
            position:fixed !important;
            top:30px !important;
            left:0 !important;
            right:0 !important;
        }

        header.scrolled{
            height:65px !important;
            position:fixed !important;
            top:0 !important;
            left:0 !important;
            right:0 !important;
        }

        .home header{
            background:none !important;
        }

        .home header.scrolled{
            background-color:rgba(0,0,0,0.9)!important;
        }

        #topheader{
            display:block;
        }

        #topheader.scrolled{
            display:none;
        }

        .diskonlyTel{
            display:inline-block !important;
        }

        #topheader .trustpilot-widget{
            display:inline-block;
            float:right;
            padding-top:5px;
        }


    }

    @media only screen and (max-width:768px){
        .flexslider .slider-image .slide.active{
            right:0px;
            background-position:center center;
        }
    }

</style>