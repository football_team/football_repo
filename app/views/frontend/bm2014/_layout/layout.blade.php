<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8" />
    <!-- Set the viewport width to device width for mobile -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
    <meta name="af_network" content="3f961ce1360f38e68ed780938b2a1cc7">
    <title>{{ isset($meta_title) ? $meta_title : (isset($settings['site_title'])? $settings['site_title'] : 'Bond CMS')  }}</title>
    <meta name="description" content="{{ isset($meta_description) ? $meta_description : $settings['meta_description'] }}">
    <meta name="keywords" content="{{ isset($meta_keywords) ? $meta_keywords : ($settings['meta_keywords']) }}">
    <meta name="author" content="{{ isset($author) ? $author : '' }}">
    <link rel="alternate" href="https://www.footballticketpad.com/{{(Request::path() != '/') ? Request::path() : ''}}" hreflang="x-default" />

    <link rel="alternate" hreflang="no-NO" href="https://www.ticketpad.no/{{(Request::path() != '/') ? Request::path() : ''}}" />
    <link rel="alternate" hreflang="es-ES" href="https://www.ticketpad.es/{{(Request::path() != '/') ? Request::path() : ''}}" />
    <link rel="alternate" hreflang="en-CA" href="https://www.ticketpad.ca/{{(Request::path() != '/') ? Request::path() : ''}}" />
    <link rel="alternate" hreflang="en-NZ" href="https://www.footballticketpad.co.nz/{{(Request::path() != '/') ? Request::path() : ''}}" />
    <link rel="alternate" hreflang="en-ZA" href="https://www.ticketpad.co.za/{{(Request::path() != '/') ? Request::path() : ''}}" />
    <link rel="alternate" hreflang="en-GB" href="https://www.footballticketpad.com/{{(Request::path() != '/') ? Request::path() : ''}}" />
    <link rel="alternate" hreflang="ru-RU" href="https://www.ticketpad.ru/{{(Request::path() != '/') ? Request::path() : ''}}" />
    <link rel="alternate" hreflang="en-US" href="https://www.ticketpad.net/{{(Request::path() != '/') ? Request::path() : ''}}" />
    <link rel="alternate" hreflang="fr-FR" href="https://www.ticketpad.fr/{{(Request::path() != '/') ? Request::path() : ''}}" />
    <link rel="alternate" hreflang="en-HK" href="https://www.ticketpad.com.hk/{{(Request::path() != '/') ? Request::path() : ''}}" />
    <link rel="alternate" hreflang="en-IE" href="https://www.ticketpad.ie/{{(Request::path() != '/') ? Request::path() : ''}}" />
    <link rel="alternate" hreflang="fr-BE" href="https://www.ticketpad.be/{{(Request::path() != '/') ? Request::path() : ''}}" />
    <link rel="alternate" hreflang="fr-CH" href="https://www.ticketpad.ch/{{(Request::path() != '/') ? Request::path() : ''}}" />
    <link rel="alternate" hreflang="fr-LU" href="https://www.ticketpad.lu/{{(Request::path() != '/') ? Request::path() : ''}}" />
    <link rel="alternate" hreflang="ru-BY" href="https://www.ticketpad.by/{{(Request::path() != '/') ? Request::path() : ''}}" />
    <link rel="alternate" hreflang="ru-UA" href="https://www.ticketpad.com.ua/{{(Request::path() != '/') ? Request::path() : ''}}" />
    <link rel="alternate" hreflang="es-AR" href="https://www.ticketpad.com.ar/{{(Request::path() != '/') ? Request::path() : ''}}" />
    <link rel="alternate" hreflang="es-CL" href="https://www.ticketpad.cl/{{(Request::path() != '/') ? Request::path() : ''}}" />
    <link rel="alternate" hreflang="es-CO" href="https://www.ticketpad.co/{{(Request::path() != '/') ? Request::path() : ''}}" />
    <link rel="alternate" hreflang="es-EC" href="https://www.ticketpad.ec/{{Request::path()}}" />
    <link rel="alternate" hreflang="es-MX" href="https://www.ticketpad.mx/{{(Request::path() != '/') ? Request::path() : ''}}" />
    <link rel="alternate" hreflang="es-PE" href="https://www.ticketpad.pe/{{(Request::path() != '/') ? Request::path() : ''}}" />
    <link rel="alternate" hreflang="es-VE" href="https://www.ticketpad.co.ve/{{(Request::path() != '/') ? Request::path() : ''}}" />
    {{
        Assets::setStyles(
        [
            'main' => 'css/main.min.css',
        ], false, true);
    }}
    <!-- Included CSS Files (Compressed) -->
    <!--link media="all" type="text/css" rel="stylesheet" href="https://cdn2.footballticketpad.com/frontend/bm2014/css/main.min.css?1.0"-->


    @yield('style')

    @if(!App::environment('local', 'stage'))
    <meta name="google-site-verification" content="{{trans('homepage.gglSiteVer')}}" />
    @endif


    <!-- IE Fix for HTML5 Tags -->
    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <script>
        var auto_complete_source="{{route('ticket.events.autocomplete')}}";
    </script>

</head>
<body class="{{ $body_class }}">

@include(Template::name('frontend.%s._layout.header'))
@yield('content')
<link rel="stylesheet" type="text/css" href="https://cdn2.footballticketpad.com/css/animate.css">

<link media="all" type="text/css" rel="stylesheet" href="https://cdn2.footballticketpad.com/frontend/bm2014/css/foundation.min.css?1.0">
<link media="all" type="text/css" rel="stylesheet" href="https://cdn2.footballticketpad.com/frontend/bm2014/css/jquery-ui/jquery-ui.min.css?1.0">
<link media="all" type="text/css" rel="stylesheet" href="https://cdn2.footballticketpad.com/frontend/bm2014/css/main.min.css?1.0">
@include(Template::name('frontend.%s._layout.footer'))
@include('frontend.bm2014.modual_mobilemenu.core')
@if (Request::is('checkout/*'))
    <script src="https://cdn3.footballticketpad.com/frontend/bm2014/js/jquery.2.0.3.js?1.0"></script>
    <script src="https://cdn3.footballticketpad.com/frontend/bm2014/js/jquery-ui.min.js?1.0"></script>
    <script src="https://cdn3.footballticketpad.com/frontend/bm2014/js/app.js?1.0"></script>
    <script src="https://cdn3.footballticketpad.com/frontend/bm2014/js/stack/stacktable.js?1.0"></script>
@else
    <script src="https://cdn3.footballticketpad.com/frontend/bm2014/js/modernizr.foundation.js?1.0"></script>
    <script src="https://cdn3.footballticketpad.com/frontend/bm2014/js/jquery.2.0.3.js?1.0"></script>
    <script src="https://cdn3.footballticketpad.com/frontend/bm2014/js/foundation.min.js?1.0"></script>
    <script src="https://cdn3.footballticketpad.com/frontend/bm2014/js/jquery-ui.min.js?1.0"></script>
    <script src="https://cdn3.footballticketpad.com/frontend/bm2014/js/app.js?1.0"></script>
    <script src="https://cdn3.footballticketpad.com/frontend/bm2014/js/stack/stacktable.js?1.0"></script>
    <script src="https://cdn3.footballticketpad.com/frontend/bm2014/js/foundation-5.4/foundation.min.js?1.0"></script>
    <script src="https://cdn3.footballticketpad.com/frontend/bm2014/js/underscore.min.js?1.0"></script>
@endif


{{ Assets::dumpScripts(Config::get('bondcms.assets_debug')) }}
@yield('script')
<script type="text/javascript">
    $('.ticket-listing .responsivetable').stacktable();
    $('.collapsible_content .responsivetable').stacktable();
    Foundation.global.namespace = '';
</script>

{{ Assets::dumpOnPageScripts() }}
        <!--Start of Zopim Live Chat Script-->
<!-- Start of ticketpad Zendesk Widget script -->
<script>/*<![CDATA[*/window.zEmbed||function(e,t){var n,o,d,i,s,a=[],r=document.createElement("iframe");window.zEmbed=function(){a.push(arguments)},window.zE=window.zE||window.zEmbed,r.src="javascript:false",r.title="",r.role="presentation",(r.frameElement||r).style.cssText="display: none",d=document.getElementsByTagName("script"),d=d[d.length-1],d.parentNode.insertBefore(r,d),i=r.contentWindow,s=i.document;try{o=s}catch(e){n=document.domain,r.src='javascript:var d=document.open();d.domain="'+n+'";void(0);',o=s}o.open()._l=function(){var o=this.createElement("script");n&&(this.domain=n),o.id="js-iframe-async",o.src=e,this.t=+new Date,this.zendeskHost=t,this.zEQueue=a,this.body.appendChild(o)},o.write('<body onload="document._l();">'),o.close()}("//assets.zendesk.com/embeddable_framework/main.js","ticketpad.zendesk.com");
    /*]]>*/</script>
<!-- End of ticketpad Zendesk Widget script -->

<script src="https://cdn3.footballticketpad.com/frontend/bm2014/js/core.js"></script>
<!--End of Zopim Live Chat Script-->
<script>
    $.ajaxSetup({ headers: { 'csrftoken' : '{{ csrf_token() }}' } });
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/38479495" style="position:absolute; left:-9999px;" alt="" /></div></noscript>

    @if(
    trans('homepage.ggtrackingcode') == 'UA-57128660-1' ||
    trans('homepage.ggtrackingcode') == 'UA-84288121-1' ||
    trans('homepage.ggtrackingcode') == 'UA-69890502-1' ||
    trans('homepage.ggtrackingcode') == 'UA-70831169-1' ||
    trans('homepage.ggtrackingcode') == 'UA-70049536-1' ||
    trans('homepage.ggtrackingcode') == 'UA-72179636-1'
    )
    <!-- Google Tag Manager -->
    <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-KWDJ87"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                    '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
            })(window,document,'script','dataLayer','GTM-KWDJ87');
        </script>
    <!-- End Google Tag Manager -->
    @elseif( trans('homepage.ggtrackingcode') == 'UA-111280416-1') )
    <!-- Google Tag Manager -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-111280416-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
      gtag('config', 'UA-111280416-1');
    </script>
    <!-- End Google Tag Manager -->
    @else
        <script>
        $(document).ready(function ()
        {
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                            (i[r].q = i[r].q || []).push(arguments)
                        }, i[r].l = 1 * new Date();
                a = s.createElement(o), m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })
            (window, document, 'script', 'https://cdn3.footballticketpad.com/frontend/bm2014/js/ga-local.js', 'ga');
            ga('create', "{{ trans('homepage.ggtrackingcode') }}", 'auto');
            ga('send', 'pageview');
        });
        </script>
    @endif
</body>
</html>
