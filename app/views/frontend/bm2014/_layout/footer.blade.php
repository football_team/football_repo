<div class="mobile row sidebar-mobile" >
	@include(Template::name('frontend.%s.sidebars.side1'))
</div >
<footer >
<span class="ftp-line" >
    	<span class="greenline" ></span >
        <span class="yellowline" ></span >
        <span class="pinkline" ></span >
        <span class="blueline" ></span >
    </span >
	<div class="full lightgrey" >
		<div class="row " >
			<div class="contain" >
				<a class="telephone-footwr" href="tel:{{trans('homepage.tnumber')}}" >
					<span class="tely" ></span > {{trans('homepage.tnumber')}}</a >
				<span class="footerblock newsletterplane" ></span >
				<span class="footerblock  support-text" >{{trans('homepage.never miss out')}}</span >
                <span class="footerblock" >
                    <form id="form-subscribe" action="{{route('email.subscribe')}}" method="post" >
						<input class="formfooter" type="email" placeholder="{{trans('homepage.enter email address')}}" name="subscriber_email" >
						<input class="gobtn" type="button" value="&#9654;" />
					</form >
                </span >
			</div >
		</div >
	</div >
	<section class="section_main" style="background:url('https://cdn3.footballticketpad.com/frontend/bm2014/images/newfootercompressed.png') no-repeat center top #171616; background-size:cover;" >
		<div class="row" >
			<article class="three columns" >
				<div class="panel" >
                    <span class="smallfooter-text" >
                        Ticket Pad
                    </span >
					<ul>
						<li><a href="/news">{{trans('homepage.News')}}</a></li>
						<li><a href="/faq">{{trans('homepage.FAQ')}}</a></li>
						<li><a href="/about">{{trans('homepage.About Us')}}</a></li>
						<li><a href="/terms-conditions">{{trans('homepage.termsCondFoot')}}</a></li>
						<li><a href="/privacy-policy">{{trans('homepage.Privacy policy')}}</a></li>
						<li><a href="/affiliates">{{trans('homepage.affilBecomeAPartner')}}</a></li>
						<li><a href="/affiliates/login">{{trans('homepage.affilAffiliateDashboard')}}</a></li>
						<li><a href="/sitemap">Sitemap</a></li>
					</ul>
				</div>
			</article >
			<article class="three columns" >
				<div class="panel">
					<span class="smallfooter-text" >
						{{trans('homepage.top_teams_header')}}
                    </span >
					<ul>
						@foreach($featureClub as $n=>$club)
							@if($n<6)
								<li>
									<a href="/group/club/{{$club->slug}}">{{$club->title}}</a>
								</li>
							@endif
						@endforeach
					</ul>
				</div>
			</article >
			<article class="three columns" >
				<div class="panel" >
					<span class="smallfooter-text" >
						{{trans('homepage.tournaments_header')}}
					</span>
					<ul>
						@foreach($featureTournaments as $n=>$club)
							@if($n<6)
								<li>
									<a href="/group/league/{{$club->slug}}">{{$club->title}}</a>
								</li>
							@endif
						@endforeach
					</ul>
				</div >
			</article >

			<article class="three columns" >
				<div class="panel" >
					<span class="smallfooter-text" >
						{{trans('homepage.hot_tickets_header')}}
					</span>
					<ul>
						@foreach($hotTickets as $n=>$ticket)
							@if($n < 6)
								<li>
									<a href="{{ FootBallEvent::getUrl($ticket) }}">{{$ticket->title}}</a>
								</li>
							@endif
						@endforeach
					</ul>
				</div >
			</article >
		</div >
	</section >
	<div class="banner-modal" id="banner-mod" ></div >
</footer >{{ Assets::jsStart() }}
<script >
	(function ($) {
		/*jQuery('input').change(function () {

			smartlook('tag', jQuery(this).attr('name'), jQuery(this).attr('value'));

		});*/

		$('.gobtn').click(function () {
			var email = $('input[name=subscriber_email]').val();
			if (email != '') {
				$.ajax({
					url: $('#form-subscribe').attr('action'),
					type: 'post',
					data: {email: email},
					success: function (response) {
						alert('Thank you very much for subscribing to us.');
						$('input[name=subscriber_email]').val('');
					},
					error: function (response) {
						console.log(response.responseText);
					}
				})
			}
		});
	})(jQuery)
</script >{{ Assets::jsEnd() }}