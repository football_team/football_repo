
<div name="" id="" class="twelve columns 7" style="background: #232323;margin-top: 10px;border: 2px solid #171616;" >
	<div class="four columns popupOpen" >
		<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 50 50" style="max-height:80px; width: 100%;fill: white; margin-top:10px; margin-botom:10px;" >
			<path d="M 25 2 C 12.318 2 2 11.42 2 23 C 2 27.754 3.77825 32.414 7.03125 36.125 C 8.33925 38.503 5.24525 42.67675 3.90625 44.46875 C 3.22025 45.38675 3 45.65475 3 46.09375 C 3 47.01075 3.6785 48 5.1875 48 C 8.7595 48 12.219 45.82725 15.25 43.90625 C 15.903 43.49225 16.521 43.07775 17.125 42.71875 C 19.654 43.56275 22.299 44 25 44 C 37.683 44 48 34.579 48 23 C 48 11.42 37.683 2 25 2 z M 16.5 12 C 16.926 12 17.2985 12.2845 17.4375 12.6875 L 21.9375 25.6875 C 22.1175 26.2085 21.8345 26.7575 21.3125 26.9375 C 21.2045 26.9745 21.108 27 21 27 C 20.585 27 20.2055 26.7265 20.0625 26.3125 L 18.90625 23 L 14.09375 23 L 12.9375 26.3125 C 12.7565 26.8335 12.2095 27.1155 11.6875 26.9375 C 11.1655 26.7565 10.8825 26.2085 11.0625 25.6875 L 15.5625 12.6875 C 15.7025 12.2845 16.074 12 16.5 12 z M 16.5 16.0625 L 14.78125 21 L 18.21875 21 L 16.5 16.0625 z M 33 19 C 33.553 19 34 19.448 34 20 L 34 22 L 39 22 C 39.553 22 40 22.448 40 23 C 40 23.552 39.553 24 39 24 L 37.8125 24 C 37.4495 25.395 36.45025 28.266 33.90625 30.875 C 34.88825 31.659 36.0515 32.42975 37.4375 33.09375 C 37.9355 33.33275 38.14425 33.9395 37.90625 34.4375 C 37.73425 34.7965 37.373 35 37 35 C 36.855 35 36.7015 34.97325 36.5625 34.90625 C 34.8905 34.10525 33.517 33.15875 32.375 32.21875 C 31.028 33.28175 29.3645 34.2345 27.3125 34.9375 C 27.2055 34.9735 27.107 35 27 35 C 26.584 35 26.2045 34.7275 26.0625 34.3125 C 25.8835 33.7905 26.1655 33.2425 26.6875 33.0625 C 28.3635 32.4885 29.70675 31.70175 30.84375 30.84375 C 29.05975 29.04675 28.2455 27.5455 28.1875 27.4375 C 27.9255 26.9505 28.10775 26.3225 28.59375 26.0625 C 29.08375 25.8005 29.6755 25.98375 29.9375 26.46875 C 29.9615 26.51175 30.735 27.90125 32.375 29.53125 C 34.366 27.50825 35.30675 25.303 35.71875 24 L 27 24 C 26.447 24 26 23.552 26 23 C 26 22.448 26.447 22 27 22 L 32 22 L 32 20 C 32 19.448 32.447 19 33 19 z" ></path >
		</svg >
	</div >
	<div class="eight columns popupOpen" style="padding : 0 !important;" >
		<div style="fill: white; margin-top:10px; margin-botom:10px;" >
			<div >
				<div class="six columns" >
					<b >{{trans('homepage.country')}}</b >
				</div >
				<div class="six columns" >{{trans('homepage.currentCountry')}}</div >
			</div >
			<div >
				<div class="six columns" >
					<b >{{trans('homepage.currency')}}</b >
				</div >
				<div class="six columns" >{{trans('homepage.currencyInUse')}}{{trans('homepage.currencyAfter')}}</div >
			</div >
			<div >
				<div class="six columns" >
					<b >{{trans('homepage.language')}}</b >
				</div >
				<div class="six columns" >
					{{trans('homepage.langiosTwoThree')}}
				</div >
			</div >
		</div >
	</div >
	<div class="twelve columns changeLang popupOpen" style="margin:5px; text-align: center;" href="#modal" onclick="" >{{trans('homepage.notCorrect')}}</div >
</div >


@if(!empty($locale_url))
	<div class="mask" style="@if(isset($set_counter_visit)) display: {{$set_counter_visit}};   @else display: none;  @endif " >
		<div class="local woops col-xs-12 animated fadeIn " style="position: fixed;top: 5%;background: #232323;width: 80%;left: 0;height: auto;padding-bottom: 20px;margin-left: 10%;opacity:1;display:@if(!empty($set_counter_visit)){{$set_counter_visit}}@endif;z-index: 100000000;margin-right: 10%;" >
			<h2 style="color: white;padding: 20px;" >
				<div class="popupClose" style="float: right;border: 1px solid white;padding: 10px;border-radius: -3px;" >
					{{trans('homepage.close')}}
				</div >
				<div >
					<A href="http://{{$locale_url}}{{$_SERVER['REQUEST_URI']}}" >
						<img style="padding: 0 13px;margin: 20px;" class="columns two" src="https://cdn1.footballticketpad.com/frontend/bm2014/images/flags/{{$locale_flag}}" >
					</a >
					<div class="columns eight"
						 style="font-size: 20px;margin-top: 15px;">@if(!empty($locale_woops)){{$locale_woops}}@endif</div>
					<div class="columns eight" style="font-size: 15px;margin-top: 15px; color: lightgrey;" > {{trans('homepage.woops')}}</div >


					<div style="
    float: left;
    width: 100%;
" >
						<div style="float: left;border: 1px solid white;padding: 10px;width: 50%;margin-left: 25%;text-align: center;" class="columns six" >
							<A href="http://{{$locale_url}}{{$_SERVER['REQUEST_URI']}}">@if(!empty($locale_view)){{$locale_view}}@endif</a>
						</div >
						<div ></div >
					</div >


					<div style="
    float: left;
    width: 100%;
" >
						<div style="float: left;border: 1px solid white;padding: 10px;width: 50%;margin-left: 25%;text-align: center;" class="columns six" >
							<A style="" href="/" > {{trans('homepage.viewThisPage')}} </a >
						</div >
						<div ></div >
					</div >
				</div >
			</h2 >
		</div >
	</div >
@endif


<div class="mask" style="display: none;" >
	<div class="local popup col-xs-12 " style=" position: fixed;top: 5%;background: #232323;width: 80%;left: 0;height: auto; padding-bottom:20px; z-index: 1000;margin-left: 10%;opacity:0;margin-right: 10%;" >
		<div class="twelve columns" >
<p style="padding: 20px; color: white !important;
		font-size: 24px;">{{trans('homepage.pleaseSelectYourLocale')}}</p>
		</div >
		<!-- Section For Collumns -->
		<div class="twelve columns" >
			<div class="" >
				<div class="four columns flagimg mobile-one" >
					<a href="https://www.footballticketpad.com" >
						<img class="homeflag sprite sprite-uk " src="https://cdn1.footballticketpad.com/trans.png" alt="United Kingdom" >
						<div >United Kingdom</div >
					</a >
				</div >
				<div class="four columns flagimg mobile-one" >
					<a href="https://www.ticketpad.net" >
						<img class="homeflag sprite sprite-United_States" src="https://cdn1.footballticketpad.com/trans.png" alt="United States" >
						<div >United States</div >
					</a >
				</div >
				<div class="four columns flagimg mobile-one" >
					<a href="https://www.ticketpad.fr" >
						<img class="homeflag  sprite sprite-France" src="https://cdn1.footballticketpad.com/trans.png" alt="France" >
						<div >France</div >
					</a >
				</div >
				<div class="four columns flagimg mobile-one" >
					<a href="https://www.ticketpad.es" >
						<img class="homeflag  sprite sprite-Spain" src="https://cdn1.footballticketpad.com/trans.png" alt="Spain" >
						<div >Spain</div >
					</a >
				</div >
				<div class="four columns flagimg mobile-one" >
					<a href="https://www.ticketpad.ru" >
						<img class="homeflag  sprite sprite-Russia  " src="https://cdn1.footballticketpad.com/trans.png" alt="Russia" >
						<div >Russia</div >
					</a >
				</div >
				<div class="four columns flagimg mobile-one" >
					<a href="https://www.ticketpad.com.hk" >
						<img class="homeflag  sprite sprite-Hong_Kong " src="https://cdn1.footballticketpad.com/trans.png" alt="Hong Kong" >
						<div >Hong Kong</div >
					</a >
				</div >
				<div class="four columns flagimg mobile-one" >
					<a href="https://www.ticketpad.co.za" >
						<img class="homeflag  sprite  sprite-South_Africa  " src="https://cdn1.footballticketpad.com/trans.png" alt="South Africa" >
						<div >South Africa</div >
					</a >
				</div >
				<div class="four columns flagimg mobile-one" >
					<a href="https://www.footballticketpad.co.nz" >
						<img class="homeflag  sprite  sprite-New_Zealand " src="https://cdn1.footballticketpad.com/trans.png" alt="New Zealand" >
						<div >New Zealand</div >
					</a >
				</div >
				<div class="four columns flagimg mobile-one" >
					<a href="https://www.ticketpad.be" >
						<img class="homeflag sprite sprite-Belgium" src="https://cdn1.footballticketpad.com/trans.png" alt="Belgium" >
						<div >Belgium</div >
					</a >
				</div >
				<div class="four columns flagimg mobile-one" >
					<a href="https://www.ticketpad.by" >
						<img class="homeflag  sprite  sprite-Belgium " src="https://cdn1.footballticketpad.com/trans.png" alt="Belarus" >
						<div >Belarus</div >
					</a >
				</div >
				<div class="four columns flagimg mobile-one" >
					<a href="https://www.ticketpad.com.ua" >
						<img class="homeflag sprite sprite-Ukraine" src="https://cdn1.footballticketpad.com/trans.png" alt="Ukraine" >
						<div >Ukraine</div >
					</a >
				</div >
				<div class="four columns flagimg mobile-one" >
					<a href="https://www.ticketpad.ec" >
						<img class="homeflag  sprite  sprite-Ukraine" src="https://cdn1.footballticketpad.com/trans.png" alt="Ecuador" >
						<div > Ecuador</div >
					</a >
				</div >
				<div class="four columns flagimg mobile-one" >
					<a href="https://www.ticketpad.cl" >
						<img class="homeflag  sprite  sprite-Chile" src="https://cdn1.footballticketpad.com/trans.png" alt="Chile" >
						<div >Chile</div >
					</a >
				</div >
				<div class="four columns flagimg mobile-one" >
					<a href="https://www.ticketpad.com.ar" >
						<img class="homeflag  sprite  sprite-Argentina" src="https://cdn1.footballticketpad.com/trans.png" alt="Argentina" >
						<div >Argentina</div >
					</a >
				</div >
				<div class="four columns flagimg mobile-one" >
					<a href="https://www.ticketpad.co.ve" >
						<img class="homeflag sprite sprite-Venezuela" src="https://cdn1.footballticketpad.com/trans.png" alt="Venuzuela" >
						<div >Venuzuela</div >
					</a >
				</div >
				<div class="four columns flagimg mobile-one" >
					<a href="https://www.ticketpad.pe" >
						<img class="homeflag sprite sprite-Peru" src="https://cdn1.footballticketpad.com/trans.png" alt="Peru" >
						<div >Peru</div >
					</a >
				</div >
				<div class="four columns flagimg mobile-one" >
					<a href="https://www.ticketpad.mx" >
						<img class="homeflag sprite sprite-Mexico" src="https://cdn1.footballticketpad.com/trans.png" alt="Mexico" >
						<div >Mexico</div >
					</a >
				</div >
				<div class="four columns flagimg mobile-one" >
					<a href="https://www.ticketpad.co" >
						<img class="homeflag sprite sprite-Colombia" src="https://cdn1.footballticketpad.com/trans.png" alt="Columbia" >
						<div >Columbia</div >
					</a >
				</div >
				<div class="four columns flagimg mobile-one" >
					<a href="https://www.ticketpad.ca" >
						<img class="homeflag sprite sprite-Canada" src="https://cdn1.footballticketpad.com/trans.png" alt="Canada" >
						<div >Canada</div >
					</a >
				</div >
				<div class="four columns flagimg mobile-one" >
					<a href="https://www.ticketpad.lu" >
						<img class="homeflag sprite sprite-Luxembourg" src="https://cdn1.footballticketpad.com/trans.png" alt="Luxembourg" >
						<div >Luxembourg</div >
					</a >
				</div >
				<div class="four columns flagimg mobile-one" >
					<a href="https://www.ticketpad.ch" >
						<img class="homeflag sprite sprite-Switzerland" src="https://cdn1.footballticketpad.com/trans.png" alt="Switzerland" >
						<div >Switzerland</div >
					</a >
				</div >
			</div >
		</div >
	</div >
</div >
<style >
	.flagimg a img {
		float: left;
		width: 50px;
		height: 50px;
	}

	.flagimg a div {
		float: left;
		color: white;
		font-size: 18px;
		width: 50%;
		margin-top: 15px;
	}

	.mask {
		width: 100vw;
		height: 100vh;
		background: rgba(1, 1, 1, 0.6);
		position: fixed;
		z-index: 10000000;
		top: 0;
		left: 0;
	}

	@media (max-width: 767px) {
		.flagimg a div {
			display: none;
			width: 100%;
			padding: 0 !important;
			margin: 0 !important;
		}

		.flagimg a img {
			float: left;
			width: 100%;
			padding: 0 !important;
			margin: 5px !important;
		}

	}
</style >