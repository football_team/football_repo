@extends(Template::name('frontend.%s._layout.layout'))
@section('style')
{{ HTML::style('ckeditor/contents.css') }}
    <style>
        @media (max-width: 768px) {
        .row.col-xs-12 {
            width: 100%;
            padding: 20px;
        }
        }

    </style>
@stop
@section('content')
<form action="{{route('ticket.checkout.order', array('id'=>$productId))}}" method="post" id="ticket-checkout-form">
<section class="banner-home">
    <span class="page-header buy-detail">
        <h1 class="gamename">{{ e($node->title) }}</h1>
    </span>
    <span class="club-effect">
        @if(isset($feature_image) && $feature_image != '')
        <img class="inner-banner" src="{{ $feature_image }}" alt="{{$node->title}}" />
        @else
        <img class="inner-banner" src="{{ Assets::Path('images/default.jpg') }}" alt="{{$node->title}}" />
        @endif
    </span>
    <span class="ftp-line">
    	<span class="greenline"></span>
        <span class="yellowline"></span>
        <span class="pinkline"></span>
        <span class="blueline"></span>
    </span>
</section>
<!---------main content------------>
<div class="row col-xs-12">
    <div class="site-content">

        <div class="columns seven">
            <div class="checkoutarea">
                <!---------description------------>

                <h3 class="buy-divider row">{{trans('homepage.DescTickets')}}</h3>
                <div class="row buydivider">
                    <div class="label pinklabel">{{trans('homepage.Event')}}</div>
                    <div class="fields columns seven pull-right">
                        <strong>{{$ticket->title}}</strong> <br><br>
                        {{date('d-m-y, h:ia', strtotime($ticket->datetime))}}, {{$ticket->event_location}}
                        <input name="ticket_description" value="{{$ticket->title}}, {{date('d-m-y h:ia', strtotime($ticket->datetime)) }}, {{$ticket->event_location}}" type="hidden">
                    </div>
                </div>

                <div class="row buydivider">
                    <div class="label pinklabel">{{trans('homepage.Ticket Type')}}</div>
                    <div class="fields columns seven pull-right">
                        <strong>{{$ticket->ticketType['title']}}</strong>
                    </div>
                </div>
                <div class="row buydivider">
                    <div class="label pinklabel">{{trans('homepage.Block')}}</div>
                    <div class="fields columns seven pull-right">
                        <strong>{{$ticket->info['loc_block']}}</strong>
                    </div>
                </div>
                <div class="row buydivider">
                    <div class="label pinklabel">{{trans('homepage.Row')}}</div>
                    <div class="fields columns seven pull-right">
                        <strong>{{$ticket->info['loc_row']}}</strong>
                    </div>
                </div>
                @if(isset($ticket->formOfTicket['title']))
                <div class="row buydivider">

                    <div class="label pinklabel">{{trans('homepage.Form of Ticket')}}</div>
                    <div class="fields columns seven pull-right">

                        <strong>{{$ticket->formOfTicket['title']}}</strong>

                    </div>

                </div>
                @endif
                @if($ticket->info['restrictions'] || $ticket->buyerNot != '')
                <div class="row buydivider">
                    <div class="label pinklabel">{{trans('homepage.Restriction')}}</div>
                    <div class="fields columns seven pull-right">

                        <ul class="restriction">
                            @foreach($defaultRestriction as $r)
                            @if(in_array($r['id'],$ticket->info['restrictions']))
                            <li><strong>{{$r['title']}}</strong></li>
                            @endif
                            @endforeach

                            @if($ticket->buyerNot != '' )
                            <li class="buyer-note"><strong><span>{{trans('homepage.Other Note')}}:</span></strong>
                                <div>{{$ticket->buyerNot }}</div>
                            </li>
                            @endif
                        </ul>

                    </div>
                </div>
                @endif

                <div class="row buydivider">
                    <div class="label pinklabel">{{trans('homepage.Number of Tickets')}}</div>
                    <div class="fields columns seven pull-right">
                        <select name="number_of_ticket" class="number-of-ticket-chk" data-preferance="{{$sell_preference}}" data-qty="{{$ticket->available_qty}}" >
                            @foreach($ticketQty as $key=>$val)
                                @if(isset($_GET['n']) && $_GET['n'] == $key )
                                    <option value="{{$key}}" selected>{{$val}}</option>
                                @else
                                    <option value="{{$key}}">{{$val}}</option>
                                @endif

                            @endforeach
                        </select>
                        @if($sell_preference == 2)
                        <br />
                        <span class="sell-note">{{trans('homepage.cantLeaveOne')}}</span>
                        @endif
                    </div>
                </div>

                <div class="row buydivider">
                    <div class="label pinklabel">{{trans('homepage.Shipping')}}</div>
                    <div class="fields columns seven pull-right">
                        <strong>{{trans('homepage.delGuarantee')}} <br><br>{{trans('homepage.internationalDelInfo')}}</strong>
                    </div>
                </div>

                <div class="row buydivider" id="hideifeticket">
                    <div class="label pinklabel">{{trans('homepage.Delivery Country')}}</div>
                    <div class="fields columns seven pull-right">
                        {{ Form::select('shipping_country', $_country, 'GB',['class'=>'input country-code', 'placeholder' => trans('homepage.Country Code'), 'id'=>'country', 'required'=>true] ) }}
                    </div>
                </div>
                <div class="row buydivider">
                    <div class="label pinklabel">{{trans('homepage.Delivery Option')}}</div>
                    <div class="fields columns seven pull-right">
                        <select class="countrieslist" name="other_country" data-delivery-selected="{{ isset($_GET['d'])? filter_var($_GET['d'], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH): ''}}">
                            <option value="" data-price="0.00"> {{trans('homepage.fSelect')}} </option>
                        </select>
                    </div>
                </div>

                <!---------descriptoin------------>
            </div>
            <div class="checkoutarea">
                <!---------buyer details------------>
                <h3 class="buy-divider row">{{trans('homepage.Buyer Details')}}</h3>

                @if (!empty($customer))
                <span class="label-seperator">{{trans('homepage.Personal Details')}}</span>
                <div class="registration-form">
                    <div class="columns six">
                        <label>
                            <input type="text" value="{{ $customer['firstname'] }}" name="first_name" >
                            <input type="text" value="{{ $customer['lastname'] }}" name="last_name" >
                        </label>
                    </div>

                    <div class="columns six">
                        <label>

                        </label>
                    </div>
                    <div style="clear: both">
                    </div>

                    @if((!isset($shipping['entity_id']))||(!isset($billing['entity_id'])))
                    <span class="label-seperator">{{trans('homepage.Shipping Address')}}</span>
                        <input type="hidden" value='shipping' name="new_shipping_address" >
                        <div id="hd-elements">
                        </div>
                        <div class="columns six">
                            <label>
                                {{ Form::text('street', '', ['class'=>'input street', 'placeholder' => trans('homepage.Address'), 'required'=>true ]) }}
                            </label>
                        </div>

                        <div class="columns six">
                            <label>
                                {{ Form::text('postcode', '', ['class'=>'input postcode', 'placeholder' => trans('homepage.Postcode'), 'required'=>true ]) }}
                            </label>
                        </div>

                        <div class="columns six">
                            <label>
                                {{ Form::text('city', '', ['class'=>'input city', 'placeholder' => trans('homepage.City'), 'required'=>true ]) }}
                            </label>
                        </div>

                        <div class="columns six">
                            {{ Form::select('country_id', $_country, 'GB', ['class'=>'input country-code', 'placeholder' => trans('homepage.Country Code'), 'id'=>'country', 'required'=>true] ) }}
                        </div>

                        <div class="columns six">
                            <label>
                                {{ Form::select('country_code', $country_with_phone, '0044', ['class'=>'input country-code', 'placeholder' => trans('homepage.Country Code'), 'required'=>true ] ) }}
                            </label>

                        </div>

                        <div class="columns six">
                            <label>
                                {{ Form::text('contact_no', '', ['class'=>'input contact-no', 'placeholder' => trans('homepage.Mobile Number'), 'required'=>true ]) }}
                            </label>
                        </div>
                        <div class="columns six">
                            <label>

                            </label>
                        </div>
                        <div class="columns six">
                            <label>
                                {{ Form::checkbox('ship-same', '1') }}
                                {{trans('homepage.bsAreSame')}}
                            </label>
                        </div>

                        <div style="clear: both">
                        </div>
                        <div class="hideIfSameAsShip">
                            <span class="label-seperator">{{trans('homepage.Billing Address')}}</span>
                            <div id="hd-elements">
                            </div>
                            <div class="columns six">
                                <label>
                                    {{ Form::text('street_bill', '', ['class'=>'input street', 'placeholder' => trans('homepage.Address') ]) }}
                                </label>
                            </div>

                            <div class="columns six">
                                <label>
                                    {{ Form::text('postcode_bill', '', ['class'=>'input postcode', 'placeholder' => trans('homepage.Postcode') ]) }}
                                </label>
                            </div>

                            <div class="columns six">
                                <label>
                                    {{ Form::text('city_bill', '', ['class'=>'input city', 'placeholder' => trans('homepage.City') ]) }}
                                </label>
                            </div>

                            <div class="columns six">
                                {{ Form::select('country_id_bill', $_country, 'GB', ['class'=>'input country-code', 'placeholder' => trans('homepage.Country Code'), 'id'=>'country'] ) }}
                            </div>
                        </div>
                        <div style="clear: both">
                        </div>

                    @else
                        <span class="label-seperator">{{trans('homepage.Shipping Address')}}</span>
                        <input type="hidden" value='shipping' name="new_shipping_address" >
                        <div id="hd-elements">
                        </div>
                        <div class="columns six">
                            <label>
                                {{ Form::text('street', $shipping['street'], ['class'=>'input street', 'placeholder' => trans('homepage.Address'), 'required'=>true ]) }}
                            </label>
                        </div>

                        <div class="columns six">
                            <label>
                                {{ Form::text('postcode', $shipping['postcode'], ['class'=>'input postcode', 'placeholder' => trans('homepage.Postcode'), 'required'=>true ]) }}
                            </label>
                        </div>

                        <div class="columns six">
                            <label>
                                {{ Form::text('city', $shipping['city'], ['class'=>'input city', 'placeholder' => trans('homepage.City'), 'required'=>true ]) }}
                            </label>
                        </div>
                        <div class="columns six">
                            {{ Form::select('country_id', $_country, $shipping['country_id'], ['class'=>'input country-code', 'placeholder' => trans('homepage.Country Code'), 'id'=>'country', 'required'=>true] ) }}
                        </div>
                        <div class="columns six">
                            <label>
                                {{ Form::select('country_code', $country_with_phone, '0044', ['class'=>'input country-code', 'placeholder' => trans('homepage.Country Code'), 'required'=>true ] ) }}
                            </label>

                        </div>
                        <div class="columns six">
                            <label>
                                {{ Form::text('contact_no', '', ['class'=>'input contact-no', 'placeholder' => trans('homepage.Mobile Number'), 'required'=>true ]) }}
                            </label>
                        </div>
                        <div class="columns six">
                            <label>
                                {{ Form::checkbox('ship-same', '1') }}
                                {{trans('homepage.bsAreSame')}}
                            </label>
                        </div>
                        <div style="clear: both">
                        </div>

                        <div class="hideIfSameAsShip">
                            <span class="label-seperator">{{trans('homepage.Billing Address')}}</span>
                            <div id="hd-elements">
                            </div>
                            <div class="columns six">
                                <label>
                                    {{ Form::text('street_bill', $billing['street'], ['class'=>'input street', 'placeholder' => trans('homepage.Address') ]) }}
                                </label>
                            </div>

                            <div class="columns six">
                                <label>
                                    {{ Form::text('postcode_bill', $billing['postcode'], ['class'=>'input postcode', 'placeholder' => trans('homepage.Postcode') ]) }}
                                </label>
                            </div>

                            <div class="columns six">
                                <label>
                                    {{ Form::text('city_bill', $billing['city'],['class'=>'input city', 'placeholder' => trans('homepage.City') ]) }}
                                </label>
                            </div>

                            <div class="columns six">
                                {{ Form::select('country_id_bill', $_country, $billing['country_id'], ['class'=>'input country-code', 'placeholder' => trans('homepage.Country Code'), 'id'=>'country'] ) }}
                            </div>
                        </div>
                        <div style="clear: both">
                        </div>
                    @endif
                </div>
                @else
                <div class="login-container">{{trans('homepage.ifRegClickHere')}} <a href="#" class="ajax-login">{{trans('homepage.click here')}}</a> {{trans('homepage.to login')}}</div>

                <br />
                <span class="label-seperator">{{trans('homepage.Personal Details')}}</span>
                <div class="registration-form">

                    <div class="columns six">
                        <label>
                            {{ Form::text('first_name', '', ['class'=>'input first-name', 'placeholder' => trans('homepage.fname') ]) }}
                        </label>
                    </div>

                    <div class="columns six">
                        <label>
                            {{ Form::text('last_name', '', ['class'=>'input last-name', 'placeholder' => trans('homepage.sname') ]) }}
                        </label>
                    </div>

                    <span class="label-seperator">{{trans('homepage.emailAndContact')}}</span>
                    <div class="columns six">
                        <label>
                            {{ Form::email('email', '', ['class'=>'input email', 'placeholder' => trans('homepage.Email') ]) }}
                        </label>
                    </div>

                    <div class="columns six">
                        <label>
                            {{ Form::email('email_confirmation', '', ['class'=>'input confirmEmail', 'placeholder' => trans('homepage.Confirm Email') ]) }}
                        </label>
                    </div>

                    <div class="columns six">
                        <label>
                            {{ Form::select('country_code', $country_with_phone, '0044', ['class'=>'input country-code', 'placeholder' => trans('homepage.Country Code') ] ) }}
                        </label>

                    </div>

                    <div class="columns six">
                        <label>
                            {{ Form::text('contact_no', '', ['class'=>'input contact-no', 'placeholder' => trans('homepage.Mobile Number') ]) }}
                        </label>
                    </div>

                    <div class="columns six">
                        <label>
                            {{ Form::checkbox('newsletters', '1') }}
                            {{trans('homepage.subForPromDis')}}
                        </label>

                    </div>


                    <span class="label-seperator">{{trans('homepage.Security')}}</span>

                    <div class="columns six">
                        <label>
                            <span class="mbtm"> {{trans('homepage.Password')}} <small class="smalltxts-warning">{{trans('homepage.passWarningLN')}}</small></span>
                            {{ Form::password('password', '') }}
                        </label>

                    </div>

                    <div class="columns six">
                        <label>
                            <span class="mbtm">{{trans('homepage.Confirm Password')}} <small class="smalltxts-warning">{{trans('homepage.passWarningLN')}}</small></span>
                            {{ Form::password('password_confirmation', '') }}
                        </label>
                    </div>
                    <div style="clear: both">
                    </div>
                    <span class="label-seperator">{{trans('homepage.Shipping Address')}}</span>
                    <div id="hd-elements">
                    </div>
                    <div class="columns six">
                        <label>
                            {{ Form::text('street', '', ['class'=>'input street', 'placeholder' => trans("homepage.Address") ]) }}
                        </label>
                    </div>

                    <div class="columns six">
                        <label>
                            {{ Form::text('postcode', '', ['class'=>'input postcode', 'placeholder' => trans('homepage.Postcode') ]) }}
                        </label>
                    </div>

                    <div class="columns six">
                        <label>
                            {{ Form::text('city', '', ['class'=>'input city', 'placeholder' => trans('homepage.City') ]) }}
                        </label>
                    </div>

                    <div class="columns six">
                        {{ Form::select('country_id', $_country, 'GB', ['class'=>'input country-code', 'placeholder' => trans('homepage.Country Code'), 'id'=>'country'] ) }}
                    </div>
                    <div class="columns six">
                            <label>
                                {{ Form::checkbox('ship-same', '1') }}
                                {{trans('homepage.bsAreSame')}}
                            </label>
                        </div>
                    <div style="clear: both">
                    </div>

                    <div class="hideIfSameAsShip">
                        <span class="label-seperator">{{trans('homepage.Billing Address')}}</span>
                        <div id="hd-elements">
                        </div>
                        <div class="columns six">
                            <label>
                                {{ Form::text('street_bill', '', ['class'=>'input street', 'placeholder' => trans('homepage.Address') ]) }}
                            </label>
                        </div>

                        <div class="columns six">
                            <label>
                                {{ Form::text('postcode_bill', '', ['class'=>'input postcode', 'placeholder' => trans('homepage.Postcode') ]) }}
                            </label>
                        </div>

                        <div class="columns six">
                            <label>
                                {{ Form::text('city_bill', '', ['class'=>'input city', 'placeholder' => trans('homepage.City') ]) }}
                            </label>
                        </div>

                        <div class="columns six">
                            {{ Form::select('country_id_bill', $_country, 'GB', ['class'=>'input country-code', 'placeholder' => trans('homepage.Country Code'), 'id'=>'country'] ) }}
                        </div>
                    </div>
                    <div style="clear: both">
                    </div>
                </div>
                @endif

                <!---------buyer details------------>
            </div>
            <div class="checkoutarea">
                <span class="label-seperator">&nbsp;</span>
                <div class="columns six">
                    <label>
                        <input type="checkbox" value="1" name="terms_n_conditions">
                        <a href="/terms-conditions" target="_blank">{{trans('homepage.tncAgree')}} {{trans('homepage.terms and conditions')}} </a>
                    </label>
                    <br/><br/>
                    <input type="submit" value="{{trans('homepage.SUBMIT ORDER')}}" class="btn pinkbtn pull-right submit-order">
                </div>
            </div>
        </div>

                <!---------main cart------------>
        <div class="columns four cart-summary cart-right">

            <h2>{{trans('homepage.Order Summary')}}</h2>

                         <span class="ftp-line">
                            <span class="greenline"></span>
                            <span class="yellowline"></span>
                            <span class="pinkline"></span>
                            <span class="blueline"></span>
                        </span>
            <input name="qty" class="qty" value="1" type="hidden" />
            <table>
                <tr>
                    <td><span class="ticket-qty">1</span> {{trans('homepage.ticketMulti')}} x {{trans('homepage.currencyInUse')}}<span class="single-ticket-price">{{number_format(($ticket->ticket->ticketInformation->price)*1.1, 2)}} {{trans('homepage.currencyAfter')}}</span></td>

                    <td class="rightAlign"> {{trans('homepage.currencyInUse')}}<span class="ticket-line-price">{{str_replace(',','',number_format(($ticket->ticket->ticketInformation->price)*1.1, 2))}} {{trans('homepage.currencyAfter')}}</span></td>
                </tr>
                <tr>
                    <td>{{trans('homepage.Delivery fee')}}</td>
                    <td class="rightAlign"> {{trans('homepage.currencyInUse')}}<span class="delivery-fee">0.00</span> {{trans('homepage.currencyAfter')}}</td>
                </tr>

                <tr>
                    <td class="cc-charge" data-cc-charge="{{Options::getOption('credit_card_charge') }}">{{trans('homepage.CCCPerc')}} ({{Options::getOption('credit_card_charge') }}%)</td>
                    <td class="rightAlign"> {{trans('homepage.currencyInUse')}}<span class="cc-charge-amount">0.00</span> {{trans('homepage.currencyAfter')}}</td>
                </tr>
            </table>

            <hr>

            <span class="finalprice">{{trans('homepage.Final price')}}</span>
            <span class="pull-right bigprice"> {{trans('homepage.currencyInUse')}}<span class="ticket-total-price">{{number_format(($ticket->ticket->ticketInformation->price)*1.1, 2)}}{{trans('homepage.currencyAfter')}}</span></span>
            <span class="priceHelp">{{trans('homepage.fpf')}}</span><br><br>
            <hr>
            <ul class="guarenteetxt-container">
                <li class="guarenteetxt">{{trans('homepage.gttc1')}}</li>
                <li class="guarenteetxt">{{trans('homepage.gttc2')}}</li>
                <li class="guarenteetxt">{{trans('homepage.gttc3')}}</li>
                <li class="guarenteetxt">{{trans('homepage.gttc4')}}</li>
            </ul>



            <!--trustpilot--->
            <div class="trustpilot-widget" data-locale="en-GB" data-template-id="539ad60defb9600b94d7df2c" data-businessunit-id="54f5f5ad0000ff00057dce30" data-style-height="500px" data-style-width="100%" data-tags="checkout" data-stars="4,5">
                <a href="https://uk.trustpilot.com/review/footballticketpad.com" target="_blank">Trustpilot</a>
            </div>
            <!--trustpilot---->

            <div id="why-buy">
                <div class="wb-h">{{trans('homepage.wbfu0')}}</div>
                <ul>
                    <li class="wb1 wb">
                        {{trans('homepage.wbfu1')}}
                    </li>
                    <li class="wb2 wb">
                        {{trans('homepage.wbfu2')}}
                    </li>
                    <li class="wb3 wb">
                        {{trans('homepage.wbfu3')}}
                    </li>
                    <li class="wb4 wb">
                        {{trans('homepage.wbfu4')}}
                    </li>
                    <li class="wb5 wb">
                        {{trans('homepage.wbfu5')}}
                    </li>
                </ul>
            </div>
        </div>
        <!---------main cart------------>

  <!--

            @if(Config::get('transactpro.COD_Enable') ==  false)
            <div class="checkoutarea">

                <span class="label-seperator">Card Details</span>
                <div class="columns six">
                    <label>Card Type</label>
                    <label>
                        <select name="card_type">
                            @foreach($credit_card_types as $key=>$type)
                            <option value="{{$key}}">{{$type}}</option>
                            @endforeach
                        </select>
                    </label>
                </div>

                <div class="columns six">

                    <label>Card Number</label>
                    <label>
                        {{ Form::text('card_number', '', ['class'=>'input card_number', 'placeholder' => 'Card Number', 'required'=>'']) }}

                    </label>
                    <div id="card_number_error" class="error" for="card_number" style="display: none">This field is required.</div>
                </div>
                <br />


                <div class="columns six">
                    <label>CVV</label>
                    <label>
                        {{ Form::text('card_cvv', '', ['class'=>'input card_cvv', 'placeholder' => '123', 'required'=>'' ]) }}
                    </label>
                </div>

                <br />

                <div class="columns six">
                    <label>Expire</label>
                    <label>
                        {{ Form::text('card_expire', '', ['class'=>'input card_expire', 'placeholder' => 'MM/YY', 'required'=>'' ]) }}
                    </label>
                </div>
            </div>
            @endif
            -->
        </div>

    </div>
</div>
<!---------main content------------>
</form>

<div id="payment-modal" class="reveal-modal" data-options="close_on_background_click:false;close_on_esc:false;">
    <h2>This is a modal.</h2>
    <p>Reveal makes these very easy to summon and dismiss. The close button is simply an anchor with a unicode character icon and a class of <code>close-reveal-modal</code>. Clicking anywhere outside the modal will also dismiss it.</p>
    <p>Finally, if your modal summons another Reveal modal, the plugin will handle that for you gracefully.</p>
    <p><a href="#" data-reveal-id="secondModal" class="secondary button">Second Modal...</a></p>
    <a class="close-reveal-modal">&#215;</a>
</div>
@stop

{{
Assets::setStyles(
[
'datetimecss'     => 'js/jquery-datetime/jquery.datetimepicker.css'
], false, true);
}}

{{
    Assets::setScripts(
    [
        'jquery-form'            => 'js/jquery-form.min.js',
        'underscore'             => 'js/underscore.min.js'
    ], false, true);
}}
{{
Assets::setScripts(
[
'serializejson'          => 'js/jquery.serializejson.min.js',

'jquery-validator'       => 'js/jquery-validator/jquery.validate.min.js',
'validator-add-method'   => 'js/jquery-validator/additional-methods.min.js',
'password-strongify'     => 'js/password-strongify.js',
'datetimepicker'         => 'js/jquery-datetime/jquery.datetimepicker.js',

], false, true);
}}

{{ Assets::jsStart() }}
<script type="text/javascript" charset="utf-8">
    (function ($) {
        var loginURL = "{{route('customer.account.checkout.login')}}";
        var ticketContainer =  $('body');
        $(document).ready(function () {
            $('body').on('click', '.login-btn', function (e) {
                var ticketContainer =  $('body');
                e.preventDefault();

                var data = {
                    login: {
                        username: $('#username').val(),
                        password: $('#password').val()
                    }
                };
                $.ajax({
                    url: loginURL,
                    type: 'post',
                    dataType: 'json',
                    data: data,
                    beforeSend: function () {
                        $(".ajax-loading-modal").remove();
                        ticketContainer.append('<div class="ajax-loading-modal"></div>');
                        ticketContainer.addClass("loading");
                        $('.error-message').css({'display':'none'});
                    },
                    success: function (reponse) {

                        if(reponse.data) {
                            var n = $('.number-of-ticket-chk').val();
                            var d = $('.countrieslist').val();

                            $.ajax({
                               url:"/checkout/logproglogin/{{$productId}}",
                                type:'post',
                                success:function(response){
                                    console.log('..eyeeeeeees');

                                },
                                error:function(response){
                                    console.log('.nooooo');
                                }
                            });

                            window.location.replace("/checkout/{{$productId}}?n="+n+"&d="+d);

                        } else {
                            ticketContainer.removeClass('loading');
                            $('.ajax-loading-modal').remove();
                            $('.error-message').css({'display':'block'});
                        }
                    },
                    error: function (reponse) {
                        ticketContainer.removeClass('loading');
                        var respObj = $.parseJSON(reponse.responseText);
                        if(respObj.error) {
                            var html = template({message: respObj.error});
                            alert(html);
                        }
                    }
                });
            });
        });

    })(jQuery);
</script>

<script type="text/javascript" charset="utf-8">
    (function ($) {
        var body = $('body');

        /* close modal */
        body.on('click','.close-reveal-modal', function (e) {
            e.preventDefault();
            var ElemId = $(this).closest('div.reveal-modal').attr('id');
            $('#'+ElemId).foundation('reveal', 'close');
            $('#'+ElemId).fadeOut('slow', function () {
                $('#'+ElemId).remove();
                $('.reveal-modal-bg').remove();
            });
        });

        body.on('click','.reveal-modal-bg', function (e) {
            e.preventDefault();
            var ElemId = body.find('div.reveal-modal').attr('id');
            $('#'+ElemId).foundation('reveal', 'close');
            $('#'+ElemId).fadeOut('slow', function () {
                $('#'+ElemId).remove();
                $('.reveal-modal-bg').remove();
            });
        })

    })(jQuery);
</script>

<script type="text/javascript" charset="utf-8">
    (function ($) {
        var body = $('body');
        $.validator.setDefaults({
            success: "valid"
        });

        $(document).ready(function () {

            updateQty($('select[name=number_of_ticket]'));

            $('select[name=number_of_ticket]').change(function (e) {

                var prefrence = parseInt($(this).data('preferance'));
                var qty = parseInt($(this).data('qty'));
                var selectedQty = parseInt($(this).val());
                var left = qty - selectedQty;

                if(prefrence  == 2  && left == 1 && qty > 1) {
                    alert('{{trans("homepage.cantLeaveOne")}}');
                    $(this).val(1);
                }

                updateQty($(this));
                apply_booking_fees();
                apply_cc_charge();
            });

            $('select[name=other_country]').change(function (e) {
                //todo: set ups price from magento right now added static price
                var deliveryFee = parseFloat($('select[name=other_country] option:selected').data('price'));
                if($(this).val() != '-1') {
                    $('.delivery-fee').html(deliveryFee.toFixed(2));
                    var totalAmount = parseFloat($('.ticket-line-price').html())+deliveryFee;
                    $('.ticket-total-price').html(totalAmount.toFixed(2));

                    apply_booking_fees();
                    apply_cc_charge();

                    var del_option = $('select[name=other_country] option:selected').val();
                    if((del_option == "flatrate3")||(del_option == "flatrate6")||(del_option == "flatrate8")||(del_option == "flatrate10")||(del_option == "flatrate12"))
                    {
                        var ws = '';
                        ws +='<div class="columns six"><label>';
                        ws += '<input type="text" name="reservation_name" value="" placeholder="{{trans("homepage.Reservation Name")}}"/>';
                        ws += '</label></div>';
                        ws +='<div class="columns six"><label>';
                        ws += '<input type="text" name="hotel_name" value="" placeholder="{{trans("homepage.Hotel Name")}}"/>';
                        ws += '</label></div>';
                        ws +='<div class="columns six"><label>';
                        ws += '<input type="text" name="checkin_dt" value="" placeholder="{{trans("homepage.Checkin Date")}}" id="datetimepicker-checkin"/>';
                        ws += '</label></div>';
                        ws +='<div class="columns six"><label>';
                        ws += '<input type="text" name="checkout_dt" value="" placeholder="{{trans("homepage.Checkout Date")}}" id="datetimepicker-checkout"/>';
                        ws += '</label></div>';
                        $('#hd-elements').html(ws);
                        $('#datetimepicker-checkin').datetimepicker({});
                        $('#datetimepicker-checkout').datetimepicker({});
                    }
                    else
                    {
                        $('#hd-elements').html("");
                    }
                }
            });

            $('select[name=shipping_country]').change(function (e) {
                var pid = "{{$productId}}";
                var shipping_url = '/checkout/ticket/shipping/{{$ticketInfo->event_id}}';
                var delCountry = $(this).find("option:selected").text();
                $.ajax({
                    url: shipping_url,
                    type: 'post',
                    dataType: 'json',
                    data: {del_country:delCountry, rtID: pid},
                    beforeSend: function () {
                        $('body').append('<div class="ajax-loading-modal"></div>');
                        $('body').addClass("loading");
                    },
                    success: function (response) {
                        if(response.data) {
                            var html = '';

                            if(response.data.length>1)
                            {
                                html = '<option value="" data-price="0.00">{{trans("homepage.fSelect")}}</option>';
                            }
                            if(response.data.length==0)
                            {
                                html = '<option value="" data-price="0.00">{{trans("homepage.noSOAvail")}}</option>';
                            }
                            for(var i=0; i< response.data.length; i++ ) {
                                html += "<option value='"+response.data[i].value+"' data-price='"+response.data[i].price+"' >"+response.data[i].label+"</option>";
                            }
                            $('.countrieslist').html(html);

                        }
                    }
                }).done(function () {
                        update_delivery_option();
                        apply_booking_fees();
                        apply_cc_charge();
                        $('.ajax-loading-modal').remove();
                        $('body').removeClass("loading");
                });
            });


            $('input[name=delivery_country]').click(function (e) {
                var deliveryFee = 0.00;
                if($(this).val() == 'uk') {
                    $('.delivery-fee').html(deliveryFee.toFixed(2));
                    var totalAmount = parseFloat($('.ticket-line-price').html())+deliveryFee;
                    $('.ticket-total-price').html(totalAmount.toFixed(2));

                    $('select[name=other_country]').val('-1');
                }
            });

            //loading shipping informations
            var shipping_url = '/checkout/ticket/shipping/{{$ticketInfo->event_id}}';
            var delCountry = $('select[name=shipping_country]').find("option:selected").text();
            var pid = "{{$productId}}";
            var ttype="{{$ticket->formOfTicket['title']}}";
            console.log("ttype = "+ttype);
            console.log("About to ajax");

            $.ajax({
                url: shipping_url,
                type: 'post',
                dataType: 'json',
                data: {del_country:delCountry, rtID:pid},
                beforeSend: function () {
                    $('body').append('<div class="ajax-loading-modal"></div>');
                    $('body').addClass("loading");
                },
                success: function (response) {
                    console.log("Inside Success");
                    console.log(response);
                    if(response.data)
                    {
                        console.log("response data");
                        var html = '';
                        if(response.data.length>1)
                        {
                            html = '<option value="" data-price="0.00">{{trans("homepage.fSelect")}}</option>';
                        }
                        if(response.data.length==0)
                        {
                            html = '<option value="" data-price="0.00">{{trans("homepage.noSOAvail")}}</option>';
                        }
                        for(var i=0; i< response.data.length; i++ ) {
                            html += "<option value='"+response.data[i].value+"' data-price='"+response.data[i].price+"' >"+response.data[i].label+"</option>";
                        }
                        $('.countrieslist').html(html);
                        if(ttype == "E-tickets")
                        {
                            $('#hideifeticket').css('display', 'none');
                        }
                    }
                    else
                    {
                        console.log("No response data");
                    }
                },
                error:function(response){
                    console.log(response);
                }
            }).done(function () {
                    update_delivery_option();
                    apply_booking_fees();
                    apply_cc_charge();
                    $('.ajax-loading-modal').remove();
                    $('body').removeClass("loading");
            });
        });


        $('.ajax-login').click(function (e) {
            e.preventDefault();
            var template = _.template($('#login-form-template').html());
            $('.login-container').html(template());
        });

        //ajax submit

        // wait for the DOM to be loaded
        var form = $('#ticket-checkout-form');
        var isUserFalse = false;
        $(document).ready(function() {
            var ticketContainer =  $('body');
            form.ajaxForm({

                beforeSubmit: function(arr, $form, options) {
                    $(".ajax-loading-modal").remove();

                    if(form.valid()) {
                        ticketContainer.append('<div class="ajax-loading-modal"></div>');
                        ticketContainer.addClass("loading");
                        return true;
                    } else {
                        return false;
                    }
                },

                success: function(response) {

                    @if (empty($customer))
                    if(!isUserFalse) {
                        var resp = response;
                        $.ajax({
                            url: '/customer/account/login',
                            data:{
                                login : {
                                    username: $('input[name=email]').val(),
                                    password: $('input[name=password]').val()
                                }},
                            type: 'POST',
                            dataType: 'json',
                            success: function (response) {
                                ticketContainer.removeClass('loading');
                                body.removeClass("loading");
                                isUserFalse = true;
                                var n = $('.number-of-ticket-chk').val();
                                var d = $('.countrieslist').val();
                                window.location.replace(resp.link);
                            },
                            error: function (response) {
                                body.removeClass("loading");
                                ticketContainer.removeClass('loading');
                                console.log(response);
                            }
                        });
                    } else {
                        window.location.href = response.link;
                    }

                    @else

                        window.location.href = response.link;
                    @endif
                },

                error: function (response) {
                    ticketContainer.removeClass('loading');
                    body.removeClass("loading");
                    //alert(response.responseText);

                    $('.site-content').find('p.error').remove();

                    if(response.responseText && response.responseText.match(/email already exists/g)){
                        $('.site-content').prepend('<p style="color:red" class="error">{{trans("homepage.emExists")}}</p>');
                    } else {
                        $('.site-content').prepend('<p style="color:red" class="error">{{trans("homepage.unableToProcess")}}</p>');
                    }

                    $('html, body').animate({
                        scrollTop: $('.site-content').offset().top + 'px'
                    }, 'fast');
                }

            });

            $('input[name="password"]').passStrengthify({
                minimum: 8,
                labels: {
                    tooShort: '',
                    passwordStrength: ''
                }
            });

            $('input[name="ship-same"]').click(function(){
                console.log($(this).is(':checked'));
                if($(this).is(':checked'))
                {
                    $('.hideIfSameAsShip').css({"display":'none'});
                }
                else
                {
                    $('.hideIfSameAsShip').css({"display":'inline-block'});
                }
            });
            form.validate({
                rules: {
                    first_name: {
                        required: true,
                        minlength: 2,
                        maxlength:60
                    },
                    last_name: {
                        required: true,
                        minlength: 2,
                        maxlength:60
                    },
                    email: {
                        required: true,
                        email: true
                    },
                    email_confirmation: {
                        equalTo: '[name="email"]'
                    },
                    password: {
                        required: true,
                        minlength: 8,
                        maxlength: 16
                    },
                    password_confirmation: {
                        equalTo: '[name="password"]'
                    },
                    terms_n_conditions: {
                        required: true
                    },
                    contact_no: {
                        digits: true,
                        minlength: 8,
                        maxlength: 20
                    },
                    other_country: {
                        required: true
                    },
                    street: {
                        required: true
                    },
                    city: {
                        required: true
                    },
                    postcode: {
                        required: true
                    },
                    country_id: {
                        required: true
                    },
                    reservation_name: {
                       required:function(element){
                            return($('select[name=other_country] option:selected').val() == "flatrate3");
                       }
                    },
                    checkin_dt: {
                       required:function(element){
                            return($('select[name=other_country] option:selected').val() == "flatrate3");
                       }
                    },
                    checkout_dt: {
                       required:function(element){
                            return($('select[name=other_country] option:selected').val() == "flatrate3");
                       }
                    }
                },
                messages: {
                    first_name: {
                        required: "{{trans('homepage.This field is required')}}",
                        minlength: $.validator.format("{{trans('homepage.min2chars')}}")
                    },
                    last_name: {
                        required: "{{trans('homepage.This field is required')}}",
                        minlength: $.validator.format("{{trans('homepage.min2chars')}}")
                    },
                    email: {
                        required: "{{trans('homepage.This field is required')}}"
                    },
                    email_confirmation: {
                        equalTo: "{{trans('homepage.emailMismatch')}}"
                    },
                    password: {
                        required: "{{trans('homepage.This field is required')}}",
                        minlength: $.validator.format("{{trans('homepage.passMinLen')}}"),
                        maxlength: $.validator.format("{{trans('homepage.passMaxLen')}}")
                    },
                    password_confirmation: {
                        equalTo: "{{trans('homepage.passMismatch')}}"
                    },
                    terms_n_conditions: {
                        required: "{{trans('homepage.mustAcceptTerms')}}"
                    },
                    contact_no: {
                        digits: "{{trans('homepage.onlyNumeric')}}"
                    },
                    other_country: {
                        required: "{{trans('homepage.mustSellDelivery')}}"
                    },
                    street: {
                        required: "{{trans('homepage.This field is required')}}"
                    },
                    city: {
                        required: "{{trans('homepage.This field is required')}}"
                    },
                    postcode: {
                        required: "{{trans('homepage.This field is required')}}"
                    },
                    country_id: {
                        required: "{{trans('homepage.This field is required')}}"
                    },
                    reservation_name: {
                       required: "{{trans('homepage.This field is required')}}"
                    },
                    checkin_dt: {
                       required: "{{trans('homepage.This field is required')}}"
                    },
                    checkout_dt: {
                       required: "{{trans('homepage.This field is required')}}"
                    }
                }
            });

        });

        var apply_booking_fees = function () {
            var ticketLinePrice = parseFloat($('.ticket-line-price').html().replace(',', ''));
            var totalAmount = parseFloat($('.ticket-total-price').html().replace(',', ''));
            var booking_fees = (parseFloat($('.booking-fee').data('booking-fee')) * ticketLinePrice ) / 100;
            totalAmount = totalAmount;
            $('.ticket-total-price').html(totalAmount.toFixed(2));
        };

        var apply_cc_charge =  function () {
            var ticketLinePrice = parseFloat($('.ticket-line-price').html().replace(',', ''));
            var totalAmount = parseFloat($('.ticket-total-price').html().replace(',', ''));
            var cc_charge = (parseFloat($('.cc-charge').data('cc-charge')) * ticketLinePrice ) / 100;
            totalAmount = totalAmount + cc_charge;
            $('.cc-charge-amount').html(cc_charge.toFixed(2));
            $('.ticket-total-price').html(totalAmount.toFixed(2));
        };

        var update_delivery_option = function () {
            var deliveryFee = parseFloat($('select[name=other_country] option:selected').data('price'));
            $('.delivery-fee').html(deliveryFee.toFixed(2));
            var totalAmount = parseFloat($('.ticket-line-price').html())+deliveryFee;
            $('.ticket-total-price').html(totalAmount.toFixed(2));
        };

        var updateQty = function (elem) {
            var qty = elem.val();
            var ticketPrice = parseFloat($('.single-ticket-price').html().replace(',', ''));
            var ticketLinePrice = qty * ticketPrice;
            var deliveryFee = parseFloat($('.delivery-fee').html());
            $('.ticket-line-price').html(ticketLinePrice.toFixed(2));
            var totalAmount = ticketLinePrice+deliveryFee;
            $('.ticket-total-price').html(totalAmount.toFixed(2));
            $('.qty').val(qty);
            $('.ticket-qty').html(qty);
        };

        $('#discountGo').on('click tap', function(){
            var code = $('#discountField').val();
            $.ajax({
                url: '/checkout/apply-code/{{$productId}}',
                data:{
                    coupon:code
                },
                type: 'POST',
                dataType: 'json',
                success: function (response) {
                    console.log(response);
                },
                error: function (response) {
                    console.log(response);
                }
            });
        });

    })(jQuery);

    function postCollectedData(fieldname, value)
    {

        $.ajax({
            url:"/checkout/logprog",
            type:"POST",
            data:{
                field:fieldname,
                val:value,
                pid:"{{$productId}}"
            },
            success:function(response){
                console.log(response);
            },
            error:function(response){
                console.log(response);
            }
        });
    }

    $('.input.email').change(function(){
        var val = $(this).val();
       postCollectedData('email', val);
    });

    $('.input.contact-no').change(function(){
        var val = $(this).val();
        postCollectedData('telephone', val);
    });

    $('.input.first-name').change(function(){
        var val = $(this).val();
        postCollectedData('firstname', val);
    });

    $('.input.last-name').change(function(){
        var val = $(this).val();
        postCollectedData('lastname', val);
    });

</script>
<!-- template -->
<script type="text/x-template" id="login-form-template" charset="utf-8">
    <div id="ajax-login-container">
            <h2 class="login-icon">Login</h2>
            <p class="error-message" style="display: none">Invalid username/ password<p>
            <form  action="{{route('customer.account.checkout.login')}}" method="post">
                <div class="row">
                    <input type="text" name="username" id="username" placeholder="{{trans('homepage.EMAIL')}}"/>
                </div>
                <div class="row">
                    <input type="password" name="password" id="password" placeholder="{{trans('homepage.PASSWORD')}}"/>
                </div>
                <div class="row">
                    <a href="/forgot-password" class="pull-left forgottenpassword">{{trans('homepage.ifmp')}}</a>
                    <input type="button" class="btn pinkbtn pull-right login-btn" value="{{trans('homepage.login')}}">
                </div>
            </form>
    </div>
</script>


{{ Assets::jsEnd() }}

