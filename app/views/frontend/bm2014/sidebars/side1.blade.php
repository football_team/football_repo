<aside class="sidebar">

 <!-- ######################## hotticket start ######################## -->  
  
   <div class="hottickets block">
   
          <span class="block-heading">
                <span class="blockicon"></span>	
                <H3>{{trans('homepage.hot_tickets_header')}}</H3>
          </span>
          
          <span class="block-content">

            @foreach($hotTickets as $ticket)
                <!-- repeater -->
                <a href="{{ FootBallEvent::getUrl($ticket) }}">
                <span class="blockrow">
                	
                    <span class="date">
                    	<span class="day">{{date('d', strtotime($ticket->datetime))}}</span>
                        <span class="month">{{date('m', strtotime($ticket->datetime))}}</span>
                    </span>
                    
                    <span class="game">
                    	{{trans('homepage.frticbefore')}} {{$ticket->title}} {{trans("homepage.Tickets")}}
                    </span>
                    
                    
                </span>
                </a>
                <!-- repeater -->

            @endforeach
          </span>
      	
   </div>
 <!-- ########################  hotticket end ######################## -->  

  <!-- ######################## top teams start ######################## -->  
  @if(count($featureClub) > 0)
   <div class="topteams block">
   
          <span class="block-heading">
                <span class="blockicon"></span>	
                <H3>{{trans('homepage.top_teams_header')}}</H3>
          </span>
          
          <span class="block-content">
                @foreach($featureClub as $club)
                <!-- repeater -->
                <a href="/group/club/{{$club->slug}}">
                <span class="blockrow">
                    <span class="date">
                    	<img src="{{ $club->value }}" alt="Buy {{$club->title}} tickets" />
                    </span>
                    
                    <span class="game">
                    	 {{trans('homepage.frticbefore')}} {{$club->title}} {{trans("homepage.Tickets")}}
                    </span>
                </span>
                </a>
                @endforeach
                <!-- repeater -->

          </span>
      	
   </div>
  @endif
  <!-- ######################## top teams end ######################## -->  
 
 
 
 
 <!-- ######################## tournaments  ######################## -->
@if(count($featureTournaments) > 0)

 <div class="tourneaments block" >
   
          <span class="block-heading">
                <span class="blockicon"></span>	
                <h3>{{trans('homepage.tournaments_header')}}</h3>
          </span>
          
          <span class="block-content">
               <!-- repeater -->
              @foreach($featureTournaments as $club)
                <a href="/group/league/{{$club->slug}}">
                <span class="blockrow even">
                    <span class="date">
                    	 <img src="{{ $club->value }}" alt="Buy {{$club->title}} tickets" />
                    </span>
                    
                    <span class="game">
                    	{{trans('homepage.frticbefore')}} {{$club->title}} {{trans("homepage.Tickets")}}
                    </span>
                </span>
                </a>

               @endforeach

          </span>
      	
   </div>
@endif
 <!-- ######################## tournaments  ######################## -->  
   
</aside>