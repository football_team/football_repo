@extends(Template::name('frontend.%s._layout.layout'))
@section('style')
{{ HTML::style('ckeditor/contents.css') }}
@stop
@section('content')



<section class="banner-home">


    <h1 class="page-header">
        {{trans('homepage.Confirmation')}}
    </h1>

    @if(isset($feature_image) && $feature_image != '')
    <img class="inner-banner" src="{{ $feature_image }}" alt="{{$node->title}}" />
    @else
    <img class="inner-banner" src="{{ Assets::Path('images/default.jpg') }}" alt="{{$node->title}}" />
    @endif
    
    <span class="ftp-line">
    	<span class="greenline"></span>
        <span class="yellowline"></span>
        <span class="pinkline"></span>
        <span class="blueline"></span>
    </span>	
    
    
</section>




<!---------main content------------>
<div class="row">
     <div class="site-content">
           <h2>{{trans('homepage.Purchase confirmation')}}</h2>
           {{trans('homepage.pc1')}} {{trans('homepage.pc2')}} {{$orderNumber}}.
           <br />
           {{trans('homepage.pc3')}}
           <br />
     </div>
</div>
<!---------main content------------>


@stop
