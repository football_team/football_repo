@extends(Template::name('frontend.%s._layout.layout'))
@section('style')
{{ HTML::style('ckeditor/contents.css') }}
@stop
@section('content')

@include(Template::name('frontend.%s._layout.account-tabs'))
<div class="accountarea">
    <div class="site-content hide-pp">
        <div class="row">
           <h2>{{trans('homepage.Sold Tickets')}}</h2>
        </div>
        <div class="row toolbar">
            <span class="filter-label pull-left offset-right">{{trans('homepage.Filter By')}}</span>
            <input type="text" placeholder="{{trans('homepage.Filter by Event')}}" class="quickfilter  pull-left offset-right">
            <div class="myFilters">
                <div class="aFilter">
                    <input id="filter-future" type="checkbox" name="filter-future" value="true" checked>
                    <label style="display:inline;" for="filter-future">{{trans('homepage.accSF1')}}</label>
                </div>
                <div class="aFilter">
                    <input id="filter-paid" type="checkbox" name="filter-future" value="true" checked>
                    <label style="display:inline;" for="filter-paid">{{trans('homepage.accSF2')}}</label>
                </div>
            </div>
            
            <span class="ftp-line">
                <span class="greenline"></span>
                <span class="yellowline"></span>
                <span class="pinkline"></span>
                <span class="blueline"></span>
            </span>
        </div>
        @foreach($soldTickets as $event)
            <div class="row listing {{$event->isFuture}}" data-slug="{{$event->slug}}" data-loaded="no" data-event-id="{{$event->event_id}}" data-label="{{$event->title}}" data-type-ids="" data-form-of-ticket="" @if($event->isFuture == "past") {{'style="display:none !important;"'}} @endif >
                <div class="collapsible_header">
                    <div class="thumb-crop pull-left">
                        @if($event->feature_image != '')
                        <img src="{{$event->feature_image}}" alt="{{$event->title}}"/>
                        @else
                        <img src="{{ Assets::Path('images/default.jpg') }}" alt="{{$event->title}}"/>
                        @endif
                    </div>

                    <div class="pull-left gamedetail">
                        <span class="game">
                            {{$event->title}} 
                        </span>         
                        <span class="game-info pull-left  clearboth"> 
                            {{$event->datetime}}. {{$event->location}} 
                        </span>
                    </div>
                    <br>
                    <div style="float:right;">Total Orders: {{$event->total}}</div><br><br>
                    <div style="float:right; @if($event->undelivered){{'color:#f00; '}} @else {{'color:#0eb603; '}} @endif">Action Required: <div class="actions" style="display:inline;">{{$event->undelivered}}</div></div>
                </div>
                <div class="collapsible_content">
                </div>
            </div>
        @endforeach
    </div>
</div>
<!---------main content------------>
<!--div class="accountarea">


     <div class="site-content">
         <div class="row">
            <h2>{{trans('homepage.Sold Tickets')}}</h2>
         </div>
            <div class="row toolbar">

                <span class="filter-label pull-left offset-right">{{trans('homepage.Filter By')}}</span>
                <input type="text" placeholder="{{trans('homepage.Filter by Event')}}" class="quickfilter  pull-left offset-right">
                <div class="myFilters">
                    <div class="aFilter">
                        <input id="filter-future" type="checkbox" name="filter-future" value="true" checked>
                        <label style="display:inline;" for="filter-future">{{trans('homepage.accSF1')}}</label>
                    </div>
                    <div class="aFilter">
                        <input id="filter-paid" type="checkbox" name="filter-future" value="true" checked>
                        <label style="display:inline;" for="filter-paid">{{trans('homepage.accSF2')}}</label>
                    </div>
                </div>
                
                <span class="ftp-line">
                    <span class="greenline"></span>
                    <span class="yellowline"></span>
                    <span class="pinkline"></span>
                    <span class="blueline"></span>
                </span>

            </div>

        <div class="row overflow-mobile">
            <table class="responsivetable">   

                <thead>
                    <tr>
                    <th>{{trans('homepage.Event')}}</th>
                    <th>{{trans('homepage.Ticket Type')}}</th>
                    <th>{{trans('homepage.accBlocknRow')}}</th>
                    <th>{{trans('homepage.Qty')}}</th>
                    <th>{{trans('homepage.accTTBP')}}</th>
                    <th>{{trans('homepage.accTransID')}}</th>
                    <th>{{trans('homepage.Sale Date')}}</th>
                    <th>{{trans('homepage.accPaySt')}}</th>
                    <th>{{trans('homepage.accDelSt')}}</th>
                    <th>{{trans('homepage.See Tracking')}}</th>
                    </tr>
                </thead>
                <tbody>

                @if(isset($soldTickets) && count($soldTickets) > 0)
                @foreach($soldTickets as $ticket)

                <tr class="{{$ticket->event->slug}} listing {{$ticket->isFuture}} {{'paymentStatus'.$ticket->seller_payment_status}} {{'deliveryStatus'.$ticket->delivery_status}}" data-slug="{{$ticket->event->slug}}" data-label="{{$ticket->event->title}}" data-order-id="{{$ticket->order_id}}" data-event-id="{{$ticket->event->id}}">
                    <td>
                        <div class="pull-left gamedetail nopadding">

                            <span class="game">{{ $ticket->event->title}}</span>
                            <span class="game-info pull-left  clearboth">{{date('d/m/Y, G:i', strtotime($ticket->event->datetime) ) }}</span>
                        </div>

                    </td>

                    <td>{{ $ticket->ticketType }}</td>

                    <td class="venue-location">{{trans('homepage.Block')}}: {{ $ticket->ticket['ticketInformation']['loc_block'] }}
                        <br/>
                        {{trans('homepage.Row')}}: {{ $ticket->ticket['ticketInformation']['loc_row'] }}
                    </td>

                    <td>{{ $ticket->qty }}</td>

                    <td>{{trans('homepage.currencyInUse')}}{{ number_format($ticket->priceAfterCommission, 2) }} {{trans('homepage.currencyAfter')}}</td>

                    <td class="product-id">{{$ticket->order_id}}</td>

                    <td>{{ date('d/m/Y', strtotime($ticket->orderDate)) }}</td>

                    <td>
                        {{ $ticket->seller_payment_status}}
                    </td>
                    <td class="seller-status-{{$ticket->order_id}}">
                       {{$ticket->delivery_status}}
                    </td>
                    <td>
                        <a href="/account/sale/customer-address/orderId" class="view-customer-address">{{trans('homepage.View Address')}}</a>
                        <br />
                        <a href="#" class="ship-ticket" data-tracking-code="{{$ticket->tracking_code}}">{{trans('homepage.Ship Ticket')}}</a> 
                        <br/>
                        <a href="#" class="contact-ticketpad">{{trans('homepage.cftp')}}</a>
                    </td>
                </tr>
                @endforeach
                @else
                <tr>
                    <td colspan="8">{{trans('homepage.saleTickNotFound')}}</td>
                </tr>
                @endif

                </tbody>


            </table>
        </div>                            
    </div>
</div>
-->
<style>
    .collapsible_content{
        padding:0px !important;
        display:none;
    }
    .listing .collapsible_content{
        display:none !important;
    }
    .listing.open .collapsible_content{
        display:block !important;
    }

    .responsivetable{
        margin:0px !important;
    }
    input.trackingChange{
        padding: 3px !important;
        border-radius: 0px !important;
        height: 25px !important;
        margin-bottom: 0px !important;
    }
    .row{
        width:100%;
    }
    h2{
        margin-left:20px;
    }
    #customer-address-template h2{
        margin-left:0px;
    }
    .hide-pp .payment-paid{
        display:none;
    }

    .stacktable.small-only{
        border-bottom: 10px solid #e13d7f !important;
        margin-bottom: 0px;
    }

    .stacktable.small-only .st-key{
        text-align:center;
    }

    .stacktable.small-only .st-val{
        text-align:center;
    }

    .stacktable.small-only .st-val .has-tip{
        margin-left: auto;
        margin-right: auto;
        float:initial;
        margin: 10px !important;
    }

    .stacktable.small-only .st-val select{
        width:100% !important;
        background:none;
    }
</style>


@stop
{{  Assets::setStyles([
    'jquery-datetime'           => 'js/jquery-datetime/jquery.datetimepicker.css',
     'stacktable'           => 'js/stacktable/stacktable.css'
], false, true) }}


{{
Assets::setScripts(
    [
    'underscore'             => 'js/underscore.min.js',
    'jquery-datetime'           => 'js/jquery-datetime/jquery.datetimepicker.js',
    'stacktable'           => 'js/stacktable/stacktable.min.js'
    ], false, true);
}}
 
{{ Assets::jsStart() }}
<script>
    (function ($) {
        $(document).ready(function () {
            $('body').on('click', '.view-customer-address', function(e){
                e.preventDefault();
                $('body').append('<div id="customer-address-template" style="display: none" class="reveal-modal" data-reveal></div>');
                e.preventDefault();
                var self = $(this);
                var row = $(this).closest('tr.listing');
                var event_html = row.find('.gamedetail').html()+row.find('.venue-location').html();
                // url with custom callbacks
                $('#customer-address-template').foundation('reveal', 'open', {
                    url: self.attr('href'),
                    success: function(data) {
                       $('.ticket-information').append( event_html);
                    },
                    error: function() {

                    }
                });
            });
            Foundation.global.namespace = '';
            $('.ship-ticket').click(function (e) {
                e.preventDefault();
                var template = _.template($('#add-tracking-information').html());
                var row = $(this).closest('tr.listing');
                $('body').append(template({
                    product_id: row.find('.product-id').text(),
                    event_id: row.data('event-id'),
                    order_id: row.data('order-id'),
                    tracking_code: $(this).data('tracking-code')
                }));
                $('#customer-tracking-details').foundation('reveal', 'open');
            });

            $('body').on('click', '.contact-ticketpad', function(e){
                e.preventDefault();
                var template = _.template($('#contact-football-ticketpads-template').html());
                var row = $(this).closest('tr');

                var rowId = row.data('row-id');

                if(typeof rowId == 'undefined')
                {
                    var classes = $(row).attr("class").split(' ');
                    var splitclass = classes[1].split('-');
                    rowId = splitclass[splitclass.length - 1];

                    var content = $(this).closest('.collapsible_content');
                    var row = $(content).closest('.large-only .ticket-row-'+rowId);
                }

                console.log(row);

                $('body').append(template({
                    event_id: row.data('event-id'),
                    order_id: rowId,
                    game_description: row.find('.gamedetail').html()
                }));
                $('#contact-football-ticketpads').foundation('reveal', 'open');
            });
            $('body').on('click', '.save-tracking-details', function () {
                var trackingCodeElm = $('input[name=tracking_code]');

                if($.trim(trackingCodeElm.val()) == '') {
                    return;
                }


                var data = {
                    tracking_code: trackingCodeElm.val(),
                    product_id:    trackingCodeElm.data('product-id'),
                    order_id:      trackingCodeElm.data('order-id'),
                    event_id:      trackingCodeElm.data('event-id')
                };

                $.ajax({
                    url: '/account/sale/tracking-code/update',
                    data: data,
                    type: 'POST',
                    success: function (response) {
                        $('seller-status-'+data.order_id).find('.delivery-st-select').val('in-transit');
                        $('.close-reveal-modal').trigger('click');
                    },
                    error: function () {
                        $('.close-reveal-modal').trigger('click');
                    }
                })
            });

            $('.delivery-st-select').change(function (e) {
                var data = {
                    'order_id': $(this).data('order-id'),
                    'status': $(this).val()
                };
                var self = $(this);
                $.ajax({
                    url: '/ticket/delivery/status',
                    data: data,
                    type: 'POST',
                    success: function (response) {
                        console.log(response);
                    },
                    error: function () {

                    }
                })
            });

            $('body').on('click', '.seller-contact', function () {
                var message = $('textarea[name=message]');
                var game    = $('p.modal-gamedetail');

                if($.trim(message.val()) == '') {
                    return '';
                }


                var data = {
                    message   : message.val(),
                    game      : game.html(),
                    event_id  : game.data('event-id'),
                    order_id  : $('input[name=order_id]').val()
                };

                $.ajax({
                    url: '/account/sale/contact-by-seller',
                    data: data,
                    type: 'POST',
                    success: function (response) {
                        $('.close-reveal-modal').trigger('click');
                    },
                    error: function () {
                        $('.close-reveal-modal').trigger('click');
                    }
                })
            });

            $('.collapsible_header').click(function (e){
                var listing = $(this).parent();
                
                if($(listing).hasClass("open"))
                {
                    $(listing).toggleClass("open");
                }
                else if($(listing).data('loaded')=="no")
                {
                    var id = $(listing).data('event-id');
                    var url = "/account/sales/get-sales-for-event/"+id;
                    var content = $(listing).children(".collapsible_content");
                    content.html("");
                    

                    $.ajax({
                        url: url,
                        type: 'POST',
                        success: function (response) {
                            var template = _.template($('#template-ticket-listing').html());
                            content.append(template({tickets: response}));
                            $(listing).data('loaded', 'yes');
                            content.css("display", "block");
                            $(listing).toggleClass("open");
                            var inputs = $(content).find("input[type='text']");
                            for(var i = 0; i<inputs.length;i++)
                            {
                                if($(inputs[i]).val()!= "")
                                {
                                    $(inputs[i]).prop("disabled", true);
                                }
                            }

                            $(content).find('.responsivetable').cardtable({myClass:'mob-tables'});
                        },
                        error: function () {
                        }
                    })
                }
                else
                {
                    $(listing).toggleClass("open");
                }
            });
        });


    })(jQuery)
</script>

<script type="text/javascript" charset="utf-8">
    (function ($) {
        var body = $('body');

        /* close modal */
        body.on('click','.close-reveal-modal', function (e) {
            e.preventDefault();
            var ElemId = $(this).closest('div.reveal-modal').attr('id');
            $('#'+ElemId).foundation('reveal', 'close');
            $('#'+ElemId).fadeOut('slow', function () {
                $('#'+ElemId).remove();
                $('.reveal-modal-bg').remove();
            });
        });
        body.on('click','.reveal-modal-bg', function (e) {
            e.preventDefault();
            var ElemId = body.find('div.reveal-modal').attr('id');
            $('#'+ElemId).foundation('reveal', 'close');
            $('#'+ElemId).fadeOut('slow', function () {
                $('#'+ElemId).remove();
                $('.reveal-modal-bg').remove();
            });
        });

        body.on('click', '.getDispatchModal', function (e){
            e.preventDefault();
            var oId = $(this).data('oid');

            var url = "/account/sales/get-dispatch-view-for-order/"+oId;

            $.ajax({
                url: url,
                type: 'POST',
                success: function (response) {
                    var template = _.template($('#tracking-modal').html());
                    $('body').append(template({
                        content: response
                    }));
                    $('#dispatch-modal').foundation('reveal', 'open');
                },
                error: function () {
                    alert("Something went wrong. Could not update tracking code.");
                }
            })
        });

        body.on('focusout', '.trackingChange', function(e){
            var val = $(this).val();
            
            var oId = $(this).data('order-id');
            var pId = $(this).data('product-id');
            var eId = $(this).data('event-id');
            var row = $('.large-only .ticket-row-'+oId);
            var status = $(row).children('.dStatus');
            var theInput = this;
            var actions = $(row).closest('.listing').find('.actions');

            var data = {
                tracking_code: val,
                product_id:    pId,
                order_id:      oId,
                event_id:      eId
            };
            if(confirm("Would you like to set order number: "+ oId +" tracking code to: "+val))
            {
                $.ajax({
                    url: '/account/sale/tracking-code/update',
                    data: data,
                    type: 'POST',
                    success: function (response) {
                        $(status).html("in-transit");
                        $(row).addClass("delivery-status-in-transit");
                        $(theInput).prop('disabled',true);

                        $('.small-only .ticket-row-'+oId+' .dstatus').html('in-transit');
                        $('.small-only .ticket-row-'+oId).addClass("delivery-status-in-transit");


                        var acNum = parseInt($(actions).html());
                        acNum = acNum - 1;
                        $(actions).html(acNum);
                        if(acNum==0)
                        {
                           $(actions).parent().css('color', 'color:#0eb603;'); 
                        }
                    },
                    error: function () {
                        alert("Something went wrong. Could not update tracking code.");
                    }
                })
            }
        });
        
        body.on('click', '.deleteUpload', function(e)
        {
            var id = $(this).data('id');
            var url = "/account/delete-ticket/"+id;

            var row = $(this).closest('tr');
            $.ajax({
                url: url,
                type: 'POST',
                success: function (response) {
                    $(row).remove();
                },
                error: function () {
                    alert("Could not delete ticket.");
                }
            })
        });

        body.on('submit', '#updCollectionForm', function(e){
            e.preventDefault();
            var data = $(this).serialize();
            var id = $(this).data('id');
            var url="/account/sale/update-collection/"+id;

            var row = $('.large-only .ticket-row-'+id);
            var status = $(row).children('.dStatus');
            var actions = $(row).closest('.listing').find('.actions');

            $.ajax({
                url: url,
                data:data,
                type: 'POST',
                success: function (response) {
                    //So, close modal, update status, update colour, actions - 1;
                    $(status).html("in-transit");
                    $(row).addClass("delivery-status-in-transit");

                    var acNum = parseInt($(actions).html());
                    acNum = acNum - 1;
                    if(acNum < 0){acNum = 0;}
                    $(actions).html(acNum);
                    if(acNum==0)
                    {
                       $(actions).parent().css('color', 'color:#0eb603;'); 
                    }
                    $('.close-reveal-modal').trigger('click');
                },
                error: function (response) {
                    alert("could not set collection point");
                }
            })

        });

        $('#filter-future').change(function(){
            var chk = $('#filter-future').is(':checked');
            var chkOther = $('#filter-paid').is(':checked');

            $('.listing').show();
            if(chk && chkOther)
            {
                $('.listing.past').hide();
            }
            else if (chk && !chkOther)
            {
                $('.listing.past').hide();
            }
            else if (!chk && chkOther)
            {
                $('.sale.paymentStatuspaid').hide();
            }
        });

        $('#filter-paid').change(function(){
            $('.site-content').toggleClass('hide-pp');
        });
    })(jQuery);
</script>

<script type="text/x-template" id="template-ticket-listing" charset="utf-8">

<table class="responsivetable">
    <thead>
        <th>{{trans('homepage.Form of Ticket')}}</th>
        <th>{{trans('homepage.tttc')}}</th>
        <th>{{trans('homepage.Block')}}</th>
        <th>{{trans('homepage.Row')}}</th>
        <th>{{trans('homepage.QTY')}}</th>
        <th>{{trans('homepage.TsOID')}}</th>
        <th>{{trans('homepage.accMEarnings')}} ({{trans('homepage.currencyInUse')}}{{trans('homepage.currencyAfter')}})</th>
        <th>{{trans('homepage.accPaySt')}}</th>   
        <th>{{trans('homepage.fname')}}</th>
        <th>{{trans('homepage.sname')}}</th>
        <th>{{trans('homepage.Telephone-mobile')}}</th>
        <th>{{trans('homepage.Tracking Code')}}</th>
        <th>{{trans('homepage.DelStatus')}}</th>
        <th>{{trans('homepage.Contact Us')}}</th>
    </thead>
    <tbody>
        <% _.each(tickets, function (ticket) {  %>
            <tr class="ticket-row ticket-row-<%= ticket.order_id %> payment-<%= ticket.seller_payment_status %> delivery-status-<%= ticket.delivery_status %>" data-row-id="<%= ticket.order_id %>" data-event-id="<%= ticket.event_id %>">
                <td><%= ticket.form_of_ticket %></td>
                <td><%= ticket.ticket_type %></td>
                <td><%= ticket.block %></td>
                <td><%= ticket.row %></td>
                <td><%= ticket.qty %></td>
                <td><a href="/account/sale/customer-address/<%= ticket.order_id %>" class="view-customer-address"><%= ticket.order_id %></a></td>
                <td><%= ticket.listed_amount %></td>
                <td><%= ticket.seller_payment_status %></td>
                <td><%= ticket.buyer_fname %> </td>
                <td><%= ticket.buyer_lname %></td>
                <td><%= ticket.buyer_cnum %></td>
                <td><a href="#" class="getDispatchModal" data-oId="<%= ticket.order_id %>"><%= ticket.tcode %></a></td>
                <td class="dStatus"><%= ticket.delivery_status %></td>
                <td>
                    <a href="#" class="contact-ticketpad"><span class="compare-ticket"></span></a>
                </td>
            </tr>
        <% }); %>
    </tbody>
</table>
</script>

<script type="text/x-template" charset="UTF-8" id="add-tracking-information" >
    <div id="customer-tracking-details" style="display: none" class="reveal-modal" data-reveal>
        <h2>{{trans('homepage.accRMTC')}}</h2>
        <p>
          <input name="tracking_code" value="<%= tracking_code %>" type="text" data-product-id="<%= product_id %>" data-event-id="<%= event_id %>" data-order-id="<%= order_id %>"  data-orig="<%= tracking_code %>"/>
        </p>
        <button name="btn_save" class="btn btn-info save-tracking-details">{{trans('homepage.Save')}}</button>
        <a class="close-reveal-modal">&#215;</a>
    </div>
</script>

<script type="text/x-template" charset="UTF-8" id="tracking-modal" >
    <div id="dispatch-modal" style="display: none" class="reveal-modal" data-reveal>
        <div id="dispatch-content">
            <%= content %>
        </div>
        <a class="close-reveal-modal">&#215;</a>
    </div>
</script>

<script type="text/x-template" charset="UTF-8" id="contact-football-ticketpads-template" >
    <div id="contact-football-ticketpads" style="display: none" class="reveal-modal" data-reveal>
        <h2>{{trans('homepage.cftp')}}</h2>
        <p data-event-id="<%= event_id %>" class="modal-gamedetail">
            <strong>{{trans('homepage.Game')}}: </strong> <br />
            <%=game_description%>
        </p>
        <div style="clear: both">
        <p>
            {{trans('homepage.TsOID')}} <br />
            <input name="order_id" value="<%=order_id%>" type="text" disabled />
        </p>
        <div style="clear: both">
        <p>
            <strong>{{trans('homepage.Message')}}: </strong>
            <textarea name="message" rows="4"></textarea>
        </p>

        <button name="btn_save" class="btn btn-info seller-contact">{{trans('homepage.Contact Us')}}</button>
        <a class="close-reveal-modal">&#215;</a>
    </div>
</script>
<script>
    function applyFilters($)
    {
    }
</script>
{{ Assets::jsEnd() }}