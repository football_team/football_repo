@extends(Template::name('frontend.%s._layout.layout'))
@section('style')
{{ HTML::style('ckeditor/contents.css') }}
{{ HTML::style('assets/css/style.css') }}
{{ HTML::style('code_prettify/css/prettify.css') }}

@if($article->feature_image != '')
    <meta property="og:image" content="https://www.footballticketpad.com{{ $article->feature_image  }}" />
@else
    <meta property="og:image" content="https://cdn1.footballticketpad.com/frontend/bm2014/images/main_logo.png" />
@endif

@stop

@section('content')


<!---------sidebar------------>
<section class="main">

    <div class="row">
        @include(Template::name('frontend.%s.sidebars.side1'))
    </div>
</section>
<!---------sidebar------------>


<section class="banner-home">

    <h1 class="page-header">
        {{ $lText['title'] }}
    </h1>
    @if($article->feature_image != '')
    <img class="inner-banner" src="{{ $article->feature_image  }}" alt="{{ $article->title }}" /></a>
    @else
    <img class="inner-banner" src="{{ Assets::Path('images/default.jpg') }}" alt="" /></a>
    @endif
    <span class="ftp-line">
    	<span class="greenline"></span>
        <span class="yellowline"></span>
        <span class="pinkline"></span>
        <span class="blueline"></span>
    </span>
</section>

<div class="row">

    <div class="container site-content">


            @foreach($article->tags as $tag)
            <a href="{{ URL::route('dashboard.tag', array('tag'=>$tag->slug)) }}"><span class="label label-warning">{{ $tag->name }}</span></a>
            @endforeach

            <p><strong>{{trans("homepage.Posted on")}} {{ date('dS M Y', strtotime($article->created_at)) }}</strong>
            </p>

            {{ $lText['content'] }}

        <!--<h4>Categories</h4>
        <div class="tagcloud tabbed_tag">
            @foreach($categories as $category)
            <a href="{{ URL::route('dashboard.category', array('category'=>$category->title)) }}">{{ $category->title }}</a>
            @endforeach
        </div>-->

        <!--<h4>Tags</h4>
        <div class="tagcloud tabbed_tag">
            @foreach($tags as $tag)
            <a href="{{ URL::route('dashboard.tag', array('tag'=>$tag->slug)) }}">{{ $tag->name }}</a>
            @endforeach
        </div>-->
            <br/>    <br/>
            <div class="sharebox">

                <!-- Go to www.addthis.com/dashboard to customize your tools -->
                <div class="addthis_sharing_toolbox"></div>
                <!-- Go to www.addthis.com/dashboard to customize your tools -->
                <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-4f426a07524dc6b4"></script>


            </div>
            <br><br><br>

            @if(!empty($taggedLeagues))
                <div class="columns four">
                    <p style="font-size:23px;">Tagged Leagues</p>
                    <ul>
                        @foreach($taggedLeagues as $league)
                            <li>
                                <a href="/group/league/{{$league->slug}}">{{$league->title}}</a>
                            </li>
                        @endforeach
                    </ul>
                </div>

            @endif
            @if(!empty($taggedClubs))
                <div class="columns four">
                    <p style="font-size:23px;">Tagged Clubs</p>
                    <ul>
                        @foreach($taggedClubs as $club)
                            <li>
                                <a href="/group/club/{{$club->slug}}">{{$club->title}}</a>
                            </li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if(!empty($taggedEvents))
                <div class="columns four">
                    <p style="font-size:23px;">Tagged Events</p>
                    <ul>
                        @foreach($taggedEvents as $event)
                            <li>
                                <a href="{{$event->url}}">{{$event->title}}</a>
                            </li>
                        @endforeach
                    </ul>
                </div>
            @endif
    </div>


</div>




@stop

@section('script')
{{ HTML::script('code_prettify/js/prettify.js') }}
<script type="text/javascript">

    !function ($) {
        $(function () {
            window.prettyPrint && prettyPrint()
        })
    }(window.jQuery)
</script>
@stop