<?php

/*
|--------------------------------------------------------------------------
| Frontend Routes
|--------------------------------------------------------------------------
*/
Route::any('/customer/account/logout', function () {
    $api_path = Config::get('api.mage_soap_api_path');
    require_once("{$api_path}app/Mage.php");
    umask(0);
    Mage::app('default');
    Mage::getSingleton('core/session', array('name' => 'frontend'));
    $session = Mage::getSingleton('customer/session', array('name' => 'frontend'));
    $session->logout();
    Session::put('customer', null);
    return Redirect::to('/login');
});

Route::any('/map/style.css', 'MapMakerControllerPublic@style');

Route::get('/sitemap.xml', function () {
    header("Content-type: text/xml");
    $locale = \App::getLocale();
    $contents = File::get(public_path() . '/sitemaps/' . $locale . '/sitemap.xml');
    $response = Response::make($contents, 200);
    $response->header('Content-Type', "text/xml");
    return $response;
});

Route::get('/robots.txt', function () {
    header('Content-Type: text/plain');
    $locale = \App::getLocale();

    $contents = "User-agent: *
Disallow: /checkout/*
Disallow: /ticket/sell/*
Disallow: /ticket-search?search=*
Disallow: /group/club/champions-league-finalist-a
Disallow: /group/club/champions-league-finalist-b
";


    $contents .= "Sitemap: " . Request::root() . '/sitemap.xml';

    $response = Response::make($contents, 200);
    $response->header('Content-Type', "text/plain");
    return $response;
});

Route::any('/feed-gen-rrppssd', 'IndexController@generateXMLFeed');

Route::any('/iftt-articles', 'IndexController@ifttArticles');


Route::group(array('before' => 'customer.account'), function () {

    Route::get('/account/listing', array('as' => 'customer.account.ticket.listing', 'uses' => 'AccountController@ticketListing'));
    Route::get('/account/purchases', 'AccountController@ticketPurchase');
    Route::get('/account/sales', 'AccountController@ticketSales');
    Route::get('/account/account-information', 'AccountController@accountInformation');
    Route::get('/account/addresses', 'AccountController@address');

    Route::get('/account/account-information/bank', 'AccountController@getCustomerBankInfo'); //get customer bank information
    Route::post('/account/account-information/bank', array('before' => 'csrf', 'uses' => 'AccountController@setCustomerBankInfo')); //set customer bank information

    Route::get('/account/account-information/card', 'AccountController@getCustomerCardInfo'); //get customer bank card information
    Route::post('/account/account-information/card', array('before' => 'csrf', 'uses' => 'AccountController@setCustomerCardInfo')); //set customer bank card information
    Route::get('/account/account-information/card/all', 'AccountController@getCustomerAllCardInfo'); //get customer bank card information

    Route::get('/account/account-information/card/all', 'AccountController@getCustomerAllCardInfo');

    Route::get('/account/account-information/personal', 'AccountController@getCustomerInfo'); //get customer bank card information
    Route::post('/account/account-information/personal', array('before' => 'csrf', 'uses' => 'AccountController@setCustomerInfo')); //set customer bank card information

    Route::post('account/account-information/password', array('before' => 'csrf', function () {
        $api_path = Config::get('api.mage_soap_api_path');
        require_once("{$api_path}app/Mage.php");
        umask(0);
        Mage::app('default');

        $password = Input::get('_password');
        $confirm_password = Input::get('_confirm_password');
        if ($password != $confirm_password) {
            header('content-type: application/json');
            header($_SERVER["SERVER_PROTOCOL"] . " 400");
            echo json_encode(array('error' => 'password does not match'));
        }

        $SesCustomer = Session::get('customer');
        $customer = Mage::getModel("customer/customer");
        $customer = $customer->load($SesCustomer['entity_id']);

        $customer->setPassword($password);
        $customer->save();

        header('content-type: application/json');
        header($_SERVER["SERVER_PROTOCOL"] . " 200");
        echo json_encode(array('data' => array('message' => 'success')));
    }));

    Route::get('/account/account-information/billing', 'AccountController@getCustomerBillingAddress'); //get customer billing address information
    Route::post('/account/account-information/billing', array('before' => 'csrf', 'uses' => 'AccountController@setCustomerBillingAddress')); //set customer billing address  information
    Route::get('/account/account-information/shipping', 'AccountController@getCustomerShippingAddress'); //get customer address information
    Route::post('/account/account-information/shipping', array('before' => 'csrf', 'uses' => 'AccountController@setCustomerShippingAddress')); //set customer address information
});

/*
|--------------------------------------------------------------------------
| Football tickets customer account
|--------------------------------------------------------------------------
*/


Route::any('/map/data/json/{id}/{cat}', 'MapMakerControllerPublic@compileBlocks');
Route::any('/map/naming/json/{id}/{cat}', 'MapMakerControllerPublic@fetchCompare');


Route::post('/customer/account/login', array('before' => 'csrf', 'uses' => function () {
    header('Content-type: application/json');
    $api_path = Config::get('api.mage_soap_api_path');
    $data = Input::all();
    $email = @$data['login']['username'];
    $password = @$data['login']['password'];
    require_once("{$api_path}app/Mage.php");
    umask(0);
    Mage::app('default');

    Mage::getSingleton('core/session', array('name' => 'frontend'));
    $session = Mage::getSingleton('customer/session', array('name' => 'frontend'));
    if ($session->isLoggedIn()) {
        echo json_encode(array(
            'data' => array(
                'message' => 'success',
                'reason' => 'already logged in'
            )
        ));
        return;
    }

    try {
        if (empty($email) || empty($password)) {
            throw new Exception ('Login and password are required.', 400);
        }
        try {
            $session->login($email, $password);
            echo json_encode(array(
                'data' => array(
                    'message' => 'success'
                )
            ));

            $customer = Mage::getSingleton('customer/session')->getCustomer()->getData();
            Session::put('customer', $customer);
        } catch (Mage_Core_Exception $e) {
            switch ($e->getCode()) {
                case Mage_Customer_Model_Customer::EXCEPTION_EMAIL_NOT_CONFIRMED:
                    throw new Exception ('Email is not confirmed. <a href="#">Resend confirmation email.</a>', 400);
                    break;
                default:
                    throw new Exception ($e->getMessage(), 400);
            }
        }
    } catch (Exception $e) {
        header('Content-type: application/json');
        echo json_encode(array('error' => $e->getMessage()));
    }
}));


Route::post('/customer/account/checkout/login', array('before' => 'csrf', 'as' => 'customer.account.checkout.login', 'uses' => function () {
    header('Content-type: application/json');
    $api_path = Config::get('api.mage_soap_api_path');
    $data = Input::all();
    $email = $data['username'];
    $password = $data['password'];
    require_once("{$api_path}app/Mage.php");
    umask(0);
    Mage::app('default');

    Mage::getSingleton('core/session', array('name' => 'frontend'));
    $session = Mage::getSingleton('customer/session', array('name' => 'frontend'));

    if ($session->isLoggedIn()) {
        return Response::json('Already logged in.', 200);
    }

    try {
        if (empty($email) || empty($password)) {
            return Response::json('Login and password are required.', 404);
        }

        try {
            $session->login($email, $password);

            $customer = Mage::getSingleton('customer/session')->getCustomer()->getData();
            Session::put('customer', $customer);

            return Response::json("success",200);

        } catch (Mage_Core_Exception $e) {
            switch ($e->getCode()) {
                case Mage_Customer_Model_Customer::EXCEPTION_EMAIL_NOT_CONFIRMED:
                    throw new Exception ('Email is not confirmed. <a href="#">Resend confirmation email.</a>', 404);
                    break;
                default:
                    throw new Exception ($e->getMessage(), 404);
            }
        }
    } catch (Exception $e) {
        return Response::json($e->getMessage(), 404);
    }
}));

//Custom url rewrites because fuck messing with nginx right now

Route::get('/premier-league/2015-16/liverpool-v-bournemouth', function () {
    return Redirect::to('/capital-one-cup/2015-16/liverpool-v-bournemouth');
});
Route::get('/ligue-1/2015-16/psg-v-troyes', function () {
    return Redirect::to('/ligue-1/2015-16/paris-saint-germain-v-troyes', 301);
});
Route::get('/premier-league/2015-16/leicester-city-v-tottenham-hotspur', function () {
    return Redirect::to('/premier-league/2015-16/leicester-city-v-tottenham', 301);
});

Route::get('/champions-league-tickets/2015-16/champions-league-final-berlin-2015', function () {
    return Redirect::to('/champions-league-tickets/2015-16/champions-league-final-milan-2016', 301);
});

Route::get('/capital-one-cup/2015-16/capital-one-cup-final-tickets', function () {
    return Redirect::to('/capital-one-cup/2015-16/capital-one-cup-final-2016', 301);
});

/*

    Euro 2016 301 redirects from  before the announcement
    All old urls redirected to new
    6 per group ordered by group

*/

//a
Route::get('/euro-2016/2015-16/france-v-a3-group-stage-euro-2016-tickets', function () {
    return Redirect::to('/euro-2016/2015-16/france-v-albania-euro-2016-tickets', 301);
});

Route::get('/euro-2016/2015-16/france-v-a2-opening-match-euro-2016-tickets', function () {
    return Redirect::to('/euro-2016/2015-16/france-v-romania-opening-match-euro-2016-tickets', 301);
});

Route::get('/euro-2016/2015-16/a4-v-france-euro-2016-group-stage-tickets', function () {
    return Redirect::to('/euro-2016/2015-16/switzerland-v-france-euro-2016-tickets', 301);
});

Route::get('/euro-2016/2015-16/a2-v-a3-group-stage-euro-2016-tickets', function () {
    return Redirect::to('/euro-2016/2015-16/romania-v-albania-euro-2016-tickets', 301);
});

Route::get('/euro-2016/2015-16/a2-v-a4-euro-2016-group-stage-tickets', function () {
    return Redirect::to('/euro-2016/2015-16/romania-v-switzerland-euro-2016-tickets', 301);
});

Route::get('/euro-2016/2015-16/a3-v-a4-group-stage-euro-2016-tickets', function () {
    return Redirect::to('/euro-2016/2015-16/albania-v-switzerland-euro-2016-tickets', 301);
});

//b

Route::get('/euro-2016/2015-16/england-v-b2-euro-2016-group-stage-tickets', function () {
    return Redirect::to('/euro-2016/2015-16/england-v-russia-euro-2016-tickets', 301);
});

Route::get('/euro-2016/2015-16/b2-v-b3-euro-2016-group-stage-tickets', function () {
    return Redirect::to('/euro-2016/2015-16/russia-v-wales-euro-2016-tickets', 301);
});

Route::get('/euro-2016/2015-16/b2-v-b4-euro-2016-group-stage-tickets', function () {
    return Redirect::to('/euro-2016/2015-16/russia-v-slovakia-euro-2016-tickets', 301);
});

Route::get('/euro-2016/2015-16/b3-v-b4-group-stage-euro-2016-tickets', function () {
    return Redirect::to('/euro-2016/2015-16/wales-v-slovakia-euro-2016-tickets', 301);
});

Route::get('/euro-2016/2015-16/b4-v-b1-group-stage-euro-2016-tickets', function () {
    return Redirect::to('/euro-2016/2015-16/slovakia-v-england-euro-2016-tickets', 301);
});

Route::get('/euro-2016/2015-16/b1-v-b3-group-stage-euro-2016-tickets', function () {
    return Redirect::to('/euro-2016/2015-16/england-v-wales-euro-2016-tickets', 301);
});
//c
Route::get('/euro-2016/2015-16/c1-v-c2-euro-2016-group-stage-tickets', function () {
    return Redirect::to('/euro-2016/2015-16/germany-v-ukraine-euro-2016-tickets', 301);
});

Route::get('/euro-2016/2015-16/c4-v-c1-group-stage-euro-2016-tickets', function () {
    return Redirect::to('/euro-2016/2015-16/northern-ireland-v-germany-euro-2016-tickets', 301);
});

Route::get('/euro-2016/2015-16/c1-v-c3-group-stage-euro-2016-tickets', function () {
    return Redirect::to('/euro-2016/2015-16/germany-v-poland-euro-2016-tickets', 301);
});

Route::get('/euro-2016/2015-16/c2-v-c3-euro-2016-group-stage-tickets', function () {
    return Redirect::to('/euro-2016/2015-16/ukraine-v-poland-euro-2016-tickets', 301);
});

Route::get('/euro-2016/2015-16/c2-v-c4-euro-2016-group-stage-tickets', function () {
    return Redirect::to('/euro-2016/2015-16/ukraine-v-northern-ireland-euro-2016-tickets', 301);
});

Route::get('/euro-2016/2015-16/c3-v-c4-euro-2016-group-stage-tickets', function () {
    return Redirect::to('/euro-2016/2015-16/poland-v-northern-ireland-group-c-euro-2016-tickets', 301);
});
//d
Route::get('/euro-2016/2015-16/d1-v-d3-group-stage-euro-2016-tickets', function () {
    return Redirect::to('/euro-2016/2015-16/spain-v-turkey-euro-2016-tickets', 301);
});

Route::get('/euro-2016/2015-16/d4-v-d1-group-stage-euro-2016-tickets', function () {
    return Redirect::to('/euro-2016/2015-16/croatia-v-spain-group-d-euro-2016', 301);
});

Route::get('/euro-2016/2015-16/d1-v-d2-group-stage-euro-2016-tickets', function () {
    return Redirect::to('/euro-2016/2015-16/spain-v-czech-republic-euro-2016-tickets', 301);
});

Route::get('/euro-2016/2015-16/d2-v-d3-group-stage-euro-2016-tickets', function () {
    return Redirect::to('/euro-2016/2015-16/czech-republic-v-turkey-euro-2016-tickets', 301);
});

Route::get('/euro-2016/2015-16/d3-v-d4-group-stage-euro-2016-tickets', function () {
    return Redirect::to('/euro-2016/2015-16/turkey-v-croatia-euro-2016-tickets', 301);
});

Route::get('/euro-2016/2015-16/d2-v-d4-group-stage-euro-2016-tickets', function () {
    return Redirect::to('/euro-2016/2015-16/czech-republic-v-d-croatia-euro-2016-tickets', 301);
});
//e
Route::get('/euro-2016/2015-16/e1-v-e3-group-stage-euro-2016-tickets', function () {
    return Redirect::to('/euro-2016/2015-16/belgium-v-republic-of-ireland-euro-2016-tickets', 301);
});

Route::get('/euro-2016/2015-16/e4-v-e1-euro-2016-group-stage-tickets', function () {
    return Redirect::to('/euro-2016/2015-16/sweden-v-belgium-euro-2016-tickets', 301);
});

Route::get('/euro-2016/2015-16/e2-v-e3-group-stage-euro-2016-tickets', function () {
    return Redirect::to('/euro-2016/2015-16/italy-v-republic-of-ireland-euro-2016-tickets', 301);
});

Route::get('/euro-2016/2015-16/e3-v-e4-euro-2016-group-stage-tickets', function () {
    return Redirect::to('/euro-2016/2015-16/republic-of-ireland-v-sweden-euro-2016-tickets', 301);
});

Route::get('/euro-2016/2015-16/e2-v-e4-euro-2016-group-stage-tickets', function () {
    return Redirect::to('/euro-2016/2015-16/italy-v-sweden-euro-2016-tickets', 301);
});

Route::get('/euro-2016/2015-16/e1-v-e2-euro-2016-group-stage-tickets', function () {
    return Redirect::to('/euro-2016/2015-16/belgium-v-italy-euro-2016-tickets', 301);
});
//f
Route::get('/euro-2016/2015-16/f1-v-f2-group-stage-euro-2016-tickets', function () {
    return Redirect::to('/euro-2016/2015-16/portugal-v-iceland-euro-2016-tickets', 301);
});

Route::get('/euro-2016/2015-16/f1-v-f3-group-stage-euro-2016-tickets', function () {
    return Redirect::to('/euro-2016/2015-16/portugal-v-austria-euro-2016-tickets', 301);
});

Route::get('/euro-2016/2015-16/f4-v-f1-euro-2016-group-stage-tickets', function () {
    return Redirect::to('/euro-2016/2015-16/hungary-v-portugal-euro-2016-tickets', 301)->withInput();
});

Route::get('/euro-2016/2015-16/f2-v-f3-group-stage-euro-2016-tickets', function () {
    return Redirect::to('/euro-2016/2015-16/iceland-v-austria-euro-2016-tickets', 301)->withInput();
});

Route::get('/euro-2016/2015-16/f2-v-f4-group-stage-euro-2016-tickets', function () {
    return Redirect::to('/euro-2016/2015-16/iceland-v-hungary-euro-2016-tickets', 301)->withInput();
});

Route::get('/euro-2016/2015-16/f3-v-f4-euro-2016-group-stage-tickets', function () {
    return Redirect::to('/euro-2016/2015-16/austria-v-hungary-euro-2016-tickets', 301)->withInput();
});

Route::get('/fa-cup/2015-16/fa-cup-final-2016', function () {
    return Redirect::to('/fa-cup/2015-16/manchester-united-v-crystal-palace-fa-cup-final-2016', 301)->withInput();
});

Route::get('/copa-del-rey/2015-16/copa-del-rey-final-2016', function () {
    return Redirect::to('/copa-del-rey/2015-16/copa-del-rey-final-2016-sevilla-v-barcelona', 301)->withInput();
});

Route::get('/community-shield/2015-16/community-shield-2015', function () {
    return Redirect::to('/community-shield/2016-17/community-shield-2016', 301)->withInput();
});

Route::get('/euro-2016/w47-v-w48-semi-finals-euro-2016-tickets', function () {
    if (Input::has('refCode')) {
        return Redirect::to('/euro-2016/germany-v-france-semi-final-euro-2016?refCode=' . Input::get('refCode'), 301);
    }

    return Redirect::to('/euro-2016/germany-v-france-semi-final-euro-2016', 301)->withInput();
});

Route::get('/euro-2016/w45-v-w46-semi-finals-euro-2016', function () {
    if (Input::has('refCode')) {
        return Redirect::to('/euro-2016/portugal-v-wales-tickets-euro-2016-semi-final-tickets?refCode=' . Input::get('refCode'), 301);
    }
    return Redirect::to('/euro-2016/portugal-v-wales-tickets-euro-2016-semi-final-tickets', 301)->withInput();
});
/*
    End of euro redirects

*/
Route::group((Config::get('bondcms')['cache']) ? array('before' => 'cache.fetch', 'after' => 'cache.put') : array(), function () {

    // frontend dashboard
    Route::get('/', ['as' => 'dashboard', 'uses' => 'HomeController@index']);

    // article
    //Route::get('/article', array('as' => 'dashboard.article', 'uses' => 'ArticleController@index'));
    Route::get('/article/{id}/{slug?}', array('as' => 'dashboard.article.show', 'uses' => 'ArticleController@show'));

    // news
    Route::get('/news', array('as' => 'dashboard.news', 'uses' => 'ArticleController@index'));
    Route::get('/news/{slug?}', array('as' => 'dashboard.news.show', 'uses' => 'ArticleController@showNews'));

    // tags
    Route::get('/tag/{tag}', array('as' => 'dashboard.tag', 'uses' => 'TagController@index'));

    // categories
    Route::get('/category/{category}', array('as' => 'dashboard.category', 'uses' => 'CategoryController@index'));

    // page
    Route::get('/page', array('as' => 'dashboard.page', 'uses' => 'PageController@index'));
    Route::get('/page/{id}', array('as' => 'dashboard.page.show', 'uses' => 'PageController@show'));

    // photo gallery
    Route::get('/photo_gallery/{id}', array('as' => 'dashboard.photo_gallery.show', 'uses' => 'PhotoGalleryController@show'));

    // contact
    Route::get('/contact', array('as' => 'dashboard.contact', 'uses' => 'FormPostController@getContact'));

    // rss
    Route::get('/rss', array('as' => 'rss', 'uses' => 'RssController@index'));

    // search
    Route::get('/search', ['as' => 'admin.search', 'uses' => 'SearchController@index']);

    // ticket-search
    Route::get('/ticket-search', ['as' => 'ticket.search', 'uses' => 'SearchController@ticketSearch']);

    //mail chimp subscribe
    Route::post('/subscribe', ['as' => 'email.subscribe', 'uses' => 'MailChimpController@subscribe']);

});

Route::post('/contact', array('as' => 'dashboard.contact.post', 'uses' => 'FormPostController@postContact'), array('before' => 'csrf'));
Route::post('/corporate-contact', array('as' => 'corporate.contact.post', 'uses' => 'FormPostController@postCorporateForm'), array('before' => 'csrf'));
Route::any('/tests', 'IndexController@test');
Route::any('/request-tickets', 'TicketRequestController@post');
Route::any('/gateway/response', array('as' => 'payment.gateway.response', 'uses' => 'PaymentGatewayController@gatewayReturn'));
Route::any('/gateway/success', array('as' => 'payment.gateway.success', 'uses' => 'PaymentGatewayController@success'));
Route::any('/gateway/error', array('as' => 'payment.gateway.error', 'uses' => 'PaymentGatewayController@error'));
Route::any('/gateway/ezerror', array('as' => 'payment.gateway.ezerror', 'uses' => 'PaymentGatewayController@ezPayError'));
Route::any('/gateway/pending', array('as' => 'payment.gateway.pending', 'uses' => 'PaymentGatewayController@pending'));

Route::any('/gateway/cod', array('as' => 'payment.gateway.response.cod', 'uses' => 'PaymentGatewayController@codPayment'));

//Route::post('/translate/from-csv/', array('uses'=>'App\Controllers\Admin\TranslateController@importCSV'));


/*
|--------------------------------------------------------------------------
| Backend Routes
|--------------------------------------------------------------------------
*/

Route::group(array('prefix' => Config::get('bondcms.admin_prefix'), 'namespace' => 'App\Controllers\Admin', 'before' => array('auth.admin', 'assets_admin')), function () {


    /**
     * Hidden Tools
     */
    Route::any('/hiddentools/checkgames/{home}', 'EventsController@checkGamesListTeam');
    Route::any('/hiddentools/checkgames', 'EventsController@checkGamesList');
    Route::any('/hiddentools/export/{id}', 'EventsController@export');



    Route::any('/emptyCache', 'EventsController@emptyCache');


    /**
     * Migratoin Rules
     */
    Route::any('/migration/postfix', 'EventsController@postFix');
    Route::any('/migration/dofix/{new}/{lang}/{old}', 'EventsController@doFix');
    Route::any('/migration/translate/{id}', 'EventsController@MigrateTranslate');
    Route::any('/migrate/2017/{id}/', 'EventsController@MigrateTo');
    Route::any('/migrate/fast/2017/{id}/', 'EventsController@MigrateToFast');

    /**
     * Seller ShortView
     */
    Route::any('/lists/list/sellers', 'UserController@getSellers');
    Route::any('/lists/list/brokers', 'UserController@getBrokers');
    Route::any('/lists/list/cust', 'UserController@getCustomers');
    Route::any('/disable/ticket/{id}', 'UserController@removeByID');

    // admin dashboard
    Route::get('/', array('as' => 'admin.dashboard', function () {
        return View::make('backend/_layout/dashboard')
            ->with('active', 'home')
            ->with('menu', 'dashboard');
    }));

    Route::get('/translate/{lang}', 'TranslateController@translate');
    Route::get('/searchtranslate/{search}/{lang}', 'TranslateController@translatesearch');
    Route::any('/save/translate/{lang}', 'TranslateController@transSave');
    Route::get('/fs/ruImport/', 'TranslateController@ruImport');
    Route::get('/fs/frImport/', 'TranslateController@frImport');
    Route::any('/from-csv/', 'TranslateController@importCSV');


    //Log view
    Route::any('/log/translate', 'LogEventController@translatorLog');
    Route::any('/log/events', 'LogEventController@eventlog');
    Route::any('/log/migration', 'LogEventController@migrateLog');


    Route::any('/log/migration', 'LogEventController@migrateLog');



    Route::any('/map/select', 'MapMakerController@selectMap');
    Route::any('/map/new/create', 'MapMakerController@createMap');
    Route::any('/map/new/save', 'MapMakerController@saveMap');
    Route::any('/map/enable/{id}', 'MapMakerController@enableMap');
    Route::any('/map/remove/{id}', 'MapMakerController@removeThis');
    Route::any('/map/remove', 'MapMakerController@removeList');
    Route::any('/map/disable/{id}', 'MapMakerController@disableMap');
    Route::any('/map/builder/{id}', 'MapMakerController@mapBuilder');
    Route::any('/map/savepitch/{w}/{h}/{x}/{y}/{id}', 'MapMakerController@savepitch');
    Route::any('/map/savetext/{t}/{x}/{y}/{s}/{r}/{id}', 'MapMakerController@savetext');
    Route::any('/map/source/{id}', 'MapMakerController@source');
    Route::any('/map/saveblock/{id}', 'MapMakerController@saveblock');
    Route::any('/map/compile/{id}', 'MapMakerController@mapCompile');
    Route::any('/map/compare/new/{left}/{right}', 'MapMakerController@mapCompare');
    Route::any('/map/compare/new/', 'MapMakerController@mapCompareNew');
    Route::any('/map/compare/new/save/', 'MapMakerController@mapCompareSaveNew');
    Route::any('/map/compare/{id}', 'MapMakerController@mapCompareEdit');
    Route::any('/map/compare/save/{id}', 'MapMakerController@mapCompareSaveThis');
    Route::any('/map/compare/', 'MapMakerController@mapCompare');


    Route::any('/map/kill/{id}', 'MapMakerController@killblock');


    //media
    Route::get('media', 'MediaController@showMedia');
    Route::post('media/upload', 'MediaController@uploadAction');
    Route::get('media/list', 'MediaController@showListAction');
    Route::post('media/list_json', 'MediaController@listJsonAction');
    Route::any('media/filemanager', 'MediaController@filemanagerAction');
    Route::get('media/edit/{id}', 'MediaController@editAction')->where('id', '[0-9]+');
    Route::any('media/destroy/{id}', 'MediaController@destroy')->where('id', '[0-9]+');
    Route::get('media/delete/{id}', 'MediaController@deleteAction')->where('id', '[0-9]+');

    //page
    Route::get('pages', array('as' => 'admin.page.index', 'uses' => 'PageAdminController@showPages'));
    Route::post('pages/list', 'PageAdminController@listAction');
    Route::get('pages/edit/{id}', 'PageAdminController@editAction')->where('id', '[0-9]+');
    Route::get('pages/new', 'PageAdminController@newAction');
    // user
    Route::resource('user', 'UserController');
    Route::get('user/{id}/delete', array('as' => 'admin.user.delete', 'uses' => 'UserController@confirmDestroy'))
        ->where('id', '[0-9]+');

    Route::post('user/addToCS/{id}', array('uses' => 'UserController@addUserToCSGroup', 'before' => 'csrf'))->where('id', '[0-9]+');
    Route::post('user/removeFromCS/{id}', array('uses' => 'UserController@removeUserFromCSGroup', 'before' => 'csrf'))->where('id', '[0-9]+');

    // blog
    Route::resource('article', 'ArticleController');
    Route::get('article/{id}/delete', array('as' => 'admin.article.delete', 'uses' => 'ArticleController@confirmDestroy'))
        ->where('id', '\d+');

    // news
    Route::resource('news', 'NewsController');
    Route::get('news/{id}/delete', array('as' => 'admin.news.delete', 'uses' => 'NewsController@confirmDestroy'))
        ->where('id', '[0-9]+');


    /**
     * EVENTS AND TRANSLATION TOOLS
     */
    Route::any('/prem/2016/events/{dataType}/', 'EventsController@eventsMetaPrem2016');
    Route::any('/prem/2016/events/{dataType}/{search}', 'EventsController@eventsMetaPrem2016Search');
    Route::any('/prem/2016/filter/{lang}/{order}/{dir}/{year}/{season}/{team}/{dataType}/', 'EventsController@teamFilterEents');
    Route::any('/prem/2016/filter/{lang}/{order}/{dir}/{year}/{season}/{team}/{dataType}/{search}', 'EventsController@teamFilterEentsSearch');
    Route::any('/datetime/save/{dataType}', 'EventsController@saveNewDate');


    /**
     * OLD EVENTS AND MIGRATION TOOLS
     */
    Route::any('events/migrate/{id}', 'EventsController@migrateList');
    Route::resource('events', 'EventsController');


    /**
     * AUTOMATION TOOLKIT
     */
    Route::any('/automate/view/checkout', 'AutomatedController@viewTest');
    Route::any('/automate/testlist', 'AutomatedController@testlist');
    Route::any('/automate/run/checkout/{os}/{browser}', 'AutomatedController@buyTest');
    Route::any('/automate/run/home/{os}/{browser}', 'AutomatedController@checkAllHomepages');
    Route::any('/automate/run/fullCheckout', 'AutomatedController@AutomatedController');


    Route::get('events/{id}/delete', array('as' => 'admin.events.delete', 'uses' => 'EventsController@confirmDestroy'))
        ->where('id', '[0-9]+');

    Route::any('events/edit/{id}', array('as' => 'admin.events.edit', 'uses' => 'EventsController@edit'))
        ->where('id', '[0-9]+');
    Route::any('events/destroy/{id}', array('as' => 'admin.events.destroy', 'uses' => 'EventsController@destroy'))
        ->where('id', '[0-9]+');


    /**
     * CATEGORY MANAGER
     */
    Route::any('/catmanager', 'CatTicketManager@listAll');
    Route::any('/catmanagersave/{id}/{tickettype}/', 'CatTicketManager@saveItem');



    //club tabs
    Route::get('club/tabs', array('as' => 'admin.club.tabs', 'uses' => 'TabController@index'));
    Route::post('club/tabs/list.json', array('as' => 'admin.club.list', 'uses' => 'TabController@listjson'));
    Route::get('club/tabs/edit/{id}', array('as' => 'admin.club.tabs.club', 'uses' => 'TabController@team'))->where('id', '[0-9]+');
    Route::post('club/tabs/club/{id}/list.json', array('as' => 'admin.club.tabs.club.list', 'uses' => 'TabController@getTeamTabs'))->where('id', '[0-9]+');
    Route::get('club/tabs/club/{id}/add', 'TabController@addTeamTab')->where('id', '[0-9]+');
    Route::get('club/tabs/edittab/{id}', 'TabController@editTab')->where('id', '[0-9]+');
    Route::post('club/tabs/save/{id}', 'TabController@saveTab')->where('id', '[0-9]+');
    Route::post('club/tabs/add/{id}', 'TabController@addTab')->where('id', '[0-9]+');
    Route::get('club/tabs/delete/{id}', 'TabController@deleteTab')->where('id', '[0-9]+');

    //hot ticket
    Route::get('widget/events', array('as' => 'admin.events.widget', 'uses' => 'EventsController@widget'));
    Route::post('widget/events/update', array('as' => 'admin.events.widget.update', 'uses' => 'EventsController@widgetUpdate'));
    Route::post('/events/list.json', array('as' => 'admin.event.list.json', 'uses' => 'EventsController@jsonList'));
    Route::post('/events/{season}/list.json', 'EventsController@jsonMigrateList');


    //top team
    Route::get('widget/topteam', array('as' => 'admin.topteam.widget', 'uses' => 'EventsController@topTeamWidget'));
    Route::get('widget/toptournament', array('as' => 'admin.toptournament.widget', 'uses' => 'EventsController@topTournaments'));
    Route::post('widget/toptournament/update', array('as' => 'admin.toptournament.widget.update', 'uses' => 'EventsController@topTournamentsUpdate'));
    Route::post('widget/topteam/update', array('as' => 'admin.topteam.widget.update', 'uses' => 'EventsController@topTeamWidgetUpdate'));

    //upcoming
    Route::get('widget/upcoming', array('as' => 'admin.upcoming.widget', 'uses' => 'EventsController@upComing'));
    Route::post('widget/upcoming/update', array('as' => 'admin.upcoming.widget.update', 'uses' => 'EventsController@upComingWidgetUpdate'));

    // faq
    Route::resource('faq', 'FaqController');
    Route::get('faq/{id}/delete', array('as' => 'admin.faq.delete', 'uses' => 'FaqController@confirmDestroy'))
        ->where('id', '[0-9]+');
    // category
    Route::resource('category', 'CategoryController');
    Route::get('category/{id}/delete', array('as' => 'admin.category.delete', 'uses' => 'CategoryController@confirmDestroy'))
        ->where('id', '[0-9]+');

    // page
    Route::resource('page', 'PageController');
    Route::get('page/{id}/delete', array('as' => 'admin.page.delete', 'uses' => 'PageController@confirmDestroy'))
        ->where('id', '[0-9]+');

    // photo gallery
    Route::resource('photo_gallery', 'PhotoGalleryController');
    Route::get('photo_gallery/{id}/delete', array('as' => 'admin.photo_gallery.delete', 'uses' => 'PhotoGalleryController@confirmDestroy'))
        ->where('id', '[0-9]+');

    // ajax - blog
    Route::post('article/{id}/toggle-publish', array('as' => 'admin.article.toggle-publish', 'uses' => 'ArticleController@togglePublish'))
        ->where('id', '[0-9]+');

    // ajax - news
    Route::post('news/{id}/toggle-publish', array('as' => 'admin.news.toggle-publish', 'uses' => 'NewsController@togglePublish'))
        ->where('id', '[0-9]+');

    // ajax - events
    Route::post('events/{id}/toggle-publish', array('as' => 'admin.events.toggle-publish', 'uses' => 'EventsController@togglePublish'))
        ->where('id', '[0-9]+');

    // ajax - faq
    Route::post('faq/{id}/toggle-publish', array('as' => 'admin.faq.toggle-publish', 'uses' => 'FaqController@togglePublish'))
        ->where('id', '[0-9]+');
    // ajax - photo gallery
    Route::post('photo_gallery/{id}/toggle-publish', array('as' => 'admin.photo_gallery.toggle-publish', 'uses' => 'PhotoGalleryController@togglePublish'))
        ->where('id', '[0-9]+');
    Route::post('photo_gallery/{id}/toggle-menu', array('as' => 'admin.photo_gallery.toggle-menu', 'uses' => 'PhotoGalleryController@toggleMenu'))
        ->where('id', '[0-9]+');

    // ajax - page
    Route::post('page/{id}/toggle-publish', array('as' => 'admin.page.toggle-publish', 'uses' => 'PageController@togglePublish'))
        ->where('id', '[0-9]+');
    Route::post('page/{id}/toggle-menu', array('as' => 'admin.page.toggle-menu', 'uses' => 'PageController@toggleMenu'))
        ->where('id', '[0-9]+');


    // ajax - form post
    Route::post('form-post/{id}/toggle-answer', array('as' => 'admin.form-post.toggle-answer', 'uses' => 'FormPostController@toggleAnswer'))
        ->where('id', '[0-9]+');

    // file upload photo gallery
    Route::post('/photo-gallery/upload/{id}', array('as' => 'admin.photo.gallery.upload.image', 'uses' => 'PhotoGalleryController@upload'))
        ->where('id', '[0-9]+');
    Route::post('/photo-gallery-delete-image', array('as' => 'admin.photo.gallery.delete.image', 'uses' => 'PhotoGalleryController@deleteImage'));

    // settings
    Route::get('/settings/website', array('as' => 'admin.settings', 'uses' => 'SettingController@index'));
    Route::get('/settings/general', array('as' => 'admin.settings.general', 'uses' => 'SettingController@generalSettings'));
    Route::post('/settings', array('as' => 'admin.settings.save', 'uses' => 'SettingController@save'), array('before' => 'csrf'));
    Route::post('/settings/options', array('as' => 'admin.settings.save.options', 'uses' => 'SettingController@saveOptions'), array('before' => 'csrf'));

    // form post
    Route::resource('form-post', 'FormPostController', array('only' => array('index', 'show', 'destroy')));
    Route::get('form-post/{id}/delete', array('as' => 'admin.form-post.delete', 'uses' => 'FormPostController@confirmDestroy'))
        ->where('id', '[0-9]+');

    // slider
    Route::get('/slider', array('as' => 'admin.slider', function () {
        return View::make('backend/slider/index');
    }));

    // slider
    Route::resource('slider', 'SliderController', array('only' => array('index', 'create', 'edit', 'update', 'destroy')));
    Route::get('slider/{id}/delete', array('as' => 'admin.slider.delete', 'uses' => 'SliderController@confirmDestroy'))
        ->where('id', '[0-9]+');

    // file upload slider
    Route::post('/slider/upload/{id}', array('as' => 'admin.slider.upload.image', 'uses' => 'SliderController@upload'))
        ->where('id', '[0-9]+');
    Route::post('/slider-delete-image', array('as' => 'admin.slider.delete.image', 'uses' => 'SliderController@deleteImage'));

    //slider images meta save
    Route::post('/slider/save/image-meta', array('as' => 'admin.slider.save.imagemeta', 'uses' => 'SliderController@saveImageMeta'));
    Route::post('/slider/delete/image', array('as' => 'admin.slider.delete.image.by.id', 'uses' => 'SliderController@deleteImageById'));

    // menu-managment
    Route::resource('menu', 'MenuController');
    Route::post('menu/save', array('as' => 'admin.menu.save', 'uses' => 'MenuController@save'));
    Route::get('menu/{id}/delete', array('as' => 'admin.menu.delete', 'uses' => 'MenuController@confirmDestroy'))
        ->where('id', '[0-9]+');
    Route::post('menu/{id}/toggle-publish', array('as' => 'admin.menu.toggle-publish', 'uses' => 'MenuController@togglePublish'))
        ->where('id', '[0-9]+');

    // log
    Route::any('log', ['as' => 'admin.log', 'uses' => 'LogController@index']);

    //filemanager
    Route::get('filemanager/show-admin', 'FilemanagerController@showInAdmin');
    Route::get('filemanager/show', 'FilemanagerController@index');
    Route::get('filemanager/getinfo', 'FilemanagerController@getinfo');
    Route::post('filemanager/getinfo', 'FilemanagerController@getinfo');


});

Route::group(array('namespace' => 'App\Controllers\Admin', 'before' => array('auth.admin', 'assets_admin')), function () {
    // filemanager

});

//// login
//Route::get('/'.Config::get('bondcms.admin_prefix').'/login', array('as' => 'admin.login', 'before'=> 'assets_admin', function () {
//
//    return View::make('backend/auth/login');
//}));

Route::group(array('namespace' => 'App\Controllers\Admin', 'before' => 'assets_admin'), function () {
    // admin auth
    Route::get('/' . Config::get('bondcms.admin_prefix') . '/logout', array('as' => 'admin.logout', 'uses' => 'AuthController@getLogout'));
    Route::get('/' . Config::get('bondcms.admin_prefix') . '/login', array('as' => 'admin.login', 'uses' => 'AuthController@getLogin'));
    Route::post('/' . Config::get('bondcms.admin_prefix') . '/login', array('as' => 'admin.login.post', 'uses' => 'AuthController@postLogin'));

    // admin password reminder
    Route::get('/' . Config::get('bondcms.admin_prefix') . '/forgot-password', array('as' => 'admin.forgot.password', 'uses' => 'AuthController@getForgotPassword'));
    Route::post('/' . Config::get('bondcms.admin_prefix') . '/forgot-password', array('as' => 'admin.forgot.password.post', 'uses' => 'AuthController@postForgotPassword'));

    Route::get('/' . Config::get('bondcms.admin_prefix') . '/{id}/reset/{code}', array('as' => 'admin.reset.password', 'uses' => 'AuthController@getResetPassword'))
        ->where('id', '[0-9]+');
    Route::post('/' . Config::get('bondcms.admin_prefix') . '/reset-password', array('as' => 'admin.reset.password.post', 'uses' => 'AuthController@postResetPassword'));


});

Route::group(array('prefix' => Config::get('bondcms.admin_prefix'), 'before' => array('auth.admin', 'assets_admin')), function () {
    //football ticket
    Route::resource('footballticket/', 'FootballTicketController');

    Route::get('/footballticket?action_type={action_type}', array('as' => 'admin.footballticket.index', 'uses' => 'FootballTicketController@index'));

    Route::get('footballticket/edit/{id}/', array('as' => 'admin.footballticket.edit', 'uses' => 'FootballTicketController@edit'))
        ->where('id', '[0-9]+');

    Route::post('footballticket/update/{id}/', array('as' => 'admin.footballticket.update', 'uses' => 'FootballTicketController@update'))
        ->where('id', '[0-9]+');

    Route::get('footballticket/{id}/delete/', array('as' => 'admin.footballticket.delete', 'uses' => 'FootballTicketController@confirmDestroy'))
        ->where('id', '[0-9]+');
    Route::post('footballticket/{id}/destroy', array('as' => 'admin.footballticket.destroy', 'uses' => 'FootballTicketController@destroy'))
        ->where('id', '[0-9]+');
    Route::get('/footballticket?action_type={action_type}', array('as' => 'dashboard.footballticket', 'uses' => 'FootballTicketController@index'));

    Route::post('/footballticket/{id}/toggle-publish', array('as' => 'admin.footballticket.toggle-publish', 'uses' => 'FootballTicketController@togglePublish'))
        ->where('id', '[0-9]+');

});

Route::get('/group/{type}/{slug}', array('as' => 'dashboard.footballticket.show', 'uses' => 'FootballTicketController@show', 'before' => 'assoc.affil'));
Route::get('/testgroup/{type}/{slug}', array('as' => 'dashboard.footballticket.show', 'uses' => 'FootballTicketController@show', 'before' => 'assoc.affil'));

Route::get('/sitemap.xml', 'FootballTicketController@sitemap');
Route::get('/sitemap-auto', 'FootballTicketController@sitemap');
Route::get('/sitemap.html', 'FootballTicketController@sitemaphtml');
Route::get('/sitemap', 'FootballTicketController@sitemaphtml');

/*
|--------------------------------------------------------------------------
| Football tickets Admin Controller
|--------------------------------------------------------------------------
*/

Route::group(array('prefix' => Config::get('bondcms.admin_prefix'), 'before' => array('auth.admin', 'assets_admin')), function () {
    Route::post('/ticket/events/ticket-types/add', array('as' => 'ticket.events.ticket-types.add', 'uses' => 'TicketTypeController@create'));
    Route::post('/ticket/events/ticket-types/remove', array('as' => 'ticket.events.ticket-types.delete', 'uses' => 'TicketTypeController@remove'));

    Route::post('/ticket/events/form-of-ticket/add', array('as' => 'ticket.events.formOfTicket.add', 'uses' => 'FormOfTicketController@create'));
    Route::post('/ticket/events/form-of-ticket/remove', array('as' => 'ticket.events.formOfTicket.delete', 'uses' => 'FormOfTicketController@remove'));

    Route::post('/ticket/events/ticket-restriction/add', array('as' => 'ticket.events.restriction.add', 'uses' => 'TicketRestrictionController@create'));
    Route::post('/ticket/events/ticket-restriction/remove', array('as' => 'ticket.events.restriction.delete', 'uses' => 'TicketRestrictionController@remove'));

    //save mfootball ticket meta
    Route::post('/footballticket/meta-data/save', array('as' => 'footballticket.meta.data.save', 'uses' => 'FootballTicketController@saveMeta'));

    Route::post('/footballticket/meta-data/delete', array('as' => 'footballticket.meta.data.delete', 'uses' => 'FootballTicketController@deleteMeta'));

});


Route::post('/ticket/registrationInternal', array('as' => 'ticket.registrationsInternal', 'uses' => 'CustomerController@registrationAction'));

Route::get('/ticket/events/ticket-types', array('as' => 'ticket.events.ticket-types', 'uses' => 'TicketTypeController@index'));
Route::get('/ticket/events/form-of-ticket-types', array('as' => 'ticket.events.formOfTicketTypes', 'uses' => 'FormOfTicketController@index'));
//TicketRestrictionController
Route::get('/ticket/events/ticket-restriction', array('as' => 'ticket.events.ticketRestriction', 'uses' => 'TicketRestrictionController@index'));
Route::get('/email/test', 'PaymentGatewayController@testMail');

//Ticket Sell part 1
Route::get('/ticket/sell/{id}', array('as' => 'ticket.sell.1', 'uses' => 'SellController@ticketInformation'))
    ->where('id', '[0-9]+');
Route::post('/ticket/sell/{id}', array('as' => 'ticket.sell.1', 'uses' => 'SellController@ticketInformation'))
    ->where('id', '[0-9]+');

//Ticket Sell part 2
Route::any('/ticket/sell/paymentmethod/{id}', array('as' => 'ticket.sell.2', 'uses' => 'SellController@ticketSellerInfo'))
    ->where('id', '[0-9]+');

Route::any('/ticket/sell/agreement/{id}', array('as' => 'ticket.sell.3', 'uses' => 'SellController@ticketSellerAgreement'))
    ->where('id', '[0-9]+');

Route::any('/ticket/sell/published/{id}', array('as' => 'ticket.sell.4', 'uses' => 'SellController@publishTicket'))
    ->where('id', '[0-9]+');

//Route::get('/uplTrans', 'IndexController@importLangFile');
//Route::get('/uplTransV', 'IndexController@getLangUploadRoute');


Route::any('/checkout/{id}','CheckoutController@index')->where('id', '[0-9]+');
Route::any('/checkout2/order/{id}', array('as' => 'ticket.checkout.order', 'uses' => 'CheckoutController@order2'))->where('id', '[0-9]+');
Route::post('/checkout/init/{id}', array('uses'=>'CheckoutController@initializeCheckout'))->where('id', '[0-9]+');
Route::post('/checkout/getNext/{id}', array('uses'=>'CheckoutController@getNext'))->where('id', '[0-9]+');
Route::post('/checkout/getPrev/{id}', array('uses'=>'CheckoutController@getPrev'))->where('id', '[0-9]+');
Route::post('/checkout/goto-page/{id}', array('uses'=>'CheckoutController@gotoPage'))->where('id', '[0-9]+');
Route::post('/checkout/getLogin/', array('uses'=>'CheckoutController@getLoginForm'));
Route::post('/checkout/getRegister/', array('uses'=>'CheckoutController@getRegisterForm'));
Route::post('/checkout/register/', array('uses'=>'CheckoutController@register'));
Route::post('/checkout/getShipping/{id}', array('uses'=>'CheckoutController@sGetShipping'))->where('id', '[0-9]+');
Route::post('/checkout/getShippingForm', array('uses'=>'CheckoutController@sGetShippingForm'));

//ticket checkout
Route::get('/checkout2/{id}', array('as' => 'ticket.checkout', 'uses' => 'CheckoutController@indextwo'))->where('id', '[0-9]+');
Route::any('/checkout/order/{id}', array('as' => 'ticket.checkout.order', 'uses' => 'CheckoutController@order'))->where('id', '[0-9]+');
Route::any('/checkout/thank-you', array('as' => 'ticket.checkout.thanks', 'uses' => 'PaymentGatewayController@thanks'));
Route::any('/search/ticket', array('as' => 'ticket.events.search', 'uses' => 'FootballTicketController@searchEvent'));
Route::any('/search/ticket/category', array('as' => 'ticket.events.search.category', 'uses' => 'FootballTicketController@searchEventCategory'));
//Route::get('/events/{slug}', array('as'=>'ticket.events.display', 'uses'=>'FootballTicketController@displayEvents'));
Route::get('/{league}/{slug}', array('as' => 'ticket.events.display.noseason', 'uses' => 'FootballTicketController@displayEventsNoSeason', 'before' => 'assoc.affil'));
Route::get('/{league}/{season}/{slug}', array('as' => 'ticket.events.display', 'uses' => 'FootballTicketController@displayEvents', 'before' => 'assoc.affil'));
Route::any('/checkout/ticket/shipping/{id}', array('as' => 'ticket.checkout.shipping.methods.id', 'uses' => 'CheckoutController@getShippingMethods'))->where('id', '[0-9]+');
Route::post('/checkout/apply-code/{id}', array('as' => 'ticket.checkout.addCode', 'uses' => 'CheckoutController@applyCoupon'));

/*
|--------------------------------------------------------------------------
| General Routes
|--------------------------------------------------------------------------
*/

Route::group((Config::get('bondcms')['cache']) ? array('before' => 'cache.fetch', 'after' => 'cache.put') : array(), function () {
    //should be an end
    Route::any('{all}', 'IndexController@showPage')->where('all', '.*');
});


/*
$rules = array(
    'name'             => 'required|min:3',                       // just a normal required validation
    'email'            => 'required|email',    // required and must be unique in the ducks table
    'numTickets'       => 'required|integer',
    'reqTogether'      => 'required|boolean',
    'eventId'          => 'required|integer|exists:events,id'
);

$validator = Validator::make(Input::all(), $rules);

if ($validator->fails())
{
    // get the error messages from the validator
    $messages = $validator->messages();
    // redirect our user back to the form with the errors from the validator
    return Redirect::to('/');
}
else
{
    $result = DB::select('SELECT * FROM events WHERE events.id = ?', Input::get('eventId'));
    $htmlForEmail = '';
    $htmlForEmail.='A customer would like to request a ticket for the following game:<br>';
    $htmlForEmail.=$result->title.'<br>';
    $htmlForEmail.='<br><br>';
    $htmlForEmail.='Customer would like: '.Input::get('numTickets').' tickets<br>';
    if(Input::get('reqTogether')==0)
    {
        $htmlForEmail.='Customer requires these tickets seated together<br>';
    }
    else{
        $htmlForEmail.='Customer does not require these tickets seated together<br>';
    }
    if(Input::get('additionalInfo')!=null)
    {
        $htmlForEmail.='Customers additional notes: <br><br>';
        $htmlForEmail.=strip_tags(Input::get('additionalInfo'));
    }

    Mail::raw($htmlForEmail, function($message){
        $message->from(Input::get('email'), Input::get('name'));
        $message->to('info@footballticketpad.com')->subject('Ticket Request for '.$result->title);
    });
}*/

// error
App::error(function (Exception $exception) {
    Log::error($exception);
    $error = $exception->getMessage();
    View::share('body_class', 'two-column');
    return Response::view('errors.error', compact('error'), 404);
});

// 404 page not found
App::missing(function () {
    View::share('body_class', 'two-column');
    return Response::view('errors.missing', array(), 404);
});


