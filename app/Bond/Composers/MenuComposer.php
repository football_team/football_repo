<?php namespace Bond\Composers;

use Menu;
use Page;
use PhotoGallery;
use FormPost;
use Session;

class MenuComposer {

    public function compose($view) {
        $locale = \App::getLocale();
        $view->with('customer', Session::get('customer'));

        $cacheName = 'menu_events_'.$locale;

        if(!\Cache::has($cacheName))
        {

            $hotTickets = \FootBallEvent::where('widget_display', '=', '1')
                ->orderBy('widget_order', 'ASC')
                ->where('is_tbc', false)
                ->whereRaw('datetime >= NOW()')
                ->get();

            foreach($hotTickets as $tick)
            {
                $trans = \DB::table('locale_text')
                    ->where('id', 'event_title_'.$tick->id)
                    ->where('locale', $locale)
                    ->first();
                if($trans)
                {
                    if(trim($trans->lstring) != '')
                    {
                        $tick->title = $trans->lstring;
                    }
                }
                $url = \FootBallEvent::getUrl($tick);
                $tick->url = $url;
            }

            //TOP CLUBS
            $featureClub = \FootballTickets::where('type', '=', 'club')
                ->leftJoin('football_ticket_meta', function ($join) {
                    $join->on('football_ticket.id', '=', 'football_ticket_meta.football_ticket_id')
                        ->where('football_ticket_meta.key','=', 'club_logo');
                })->where('widget', '=', '1')
                ->orderBy('widget_order', 'ASC')
                ->groupBy('football_ticket.id')
                ->take(15)
                ->get();

            foreach($featureClub as $club)
            {
                $trans = \DB::table('locale_text')
                    ->where('id', 'fbt_title_'.$club->football_ticket_id)
                    ->where('locale', $locale)
                    ->first();
                if($trans)
                {
                    $club->title = $trans->lstring;
                }
                $aLogo = \ImageController::getSideBarImage('club', $club->football_ticket_id, $club->value);
                if($aLogo)
                {
                    $club->value = $aLogo;
                }

            }

            $internationalClub = \FootballTickets::where('type', '=', 'club')
                ->leftJoin('football_ticket_meta', function ($join) {
                    $join->on('football_ticket.id', '=', 'football_ticket_meta.football_ticket_id')
                        ->where('football_ticket_meta.key','=', 'club_logo');
                })
                ->where('is_international', true)
                ->groupBy('football_ticket.id')
                ->take(15)
                ->get();

            foreach($internationalClub as $club)
            {
                $trans = \DB::table('locale_text')
                    ->where('id', 'fbt_title_'.$club->football_ticket_id)
                    ->where('locale', $locale)
                    ->first();
                if($trans)
                {
                    $club->title = $trans->lstring;
                }
                $aLogo = \ImageController::getSideBarImage('club', $club->football_ticket_id, $club->value);
                if($aLogo)
                {
                    $club->value = $aLogo;
                }

            }

            //FEATURED TOURNAMENTS

            $featureTournaments = \FootballTickets::where('type', '=', 'league')
                ->leftJoin('football_ticket_meta', function ($join) {
                    $join->on('football_ticket.id', '=', 'football_ticket_meta.football_ticket_id')
                        ->where('football_ticket_meta.key','=', 'club_logo');
                })->where('widget', '=', '1')
                ->orderBy('widget_order', 'ASC')
                ->groupBy('football_ticket.id')
                ->get();

            foreach($featureTournaments as $tour)
            {

                $trans = \DB::table('locale_text')
                    ->where('id', 'fbt_title_'.$tour->football_ticket_id)
                    ->where('locale', $locale)
                    ->first();
                if($trans)
                {
                    $tour->title = $trans->lstring;
                }
                $aLogo = \ImageController::getSideBarImage('league', $tour->football_ticket_id, $tour->value);
                if($aLogo)
                {
                    $tour->value = $aLogo;
                }
            }

            //CACHE AND RETURN VIEW

            \Cache::add('menu_tournaments_'.$locale, $featureTournaments, 3600);
            \Cache::add('menu_clubs_'.$locale, $featureClub, 3600);
            \Cache::add('menu_international_clubs_'.$locale, $internationalClub, 3600);
            \Cache::add('menu_events_'.$locale, $hotTickets, 3600);

            $view->with('featureTournaments', $featureTournaments);
            $view->with('featureClub', $featureClub);
            $view->with('featureEvents', $hotTickets);
            $view->with('internationalClub', $internationalClub);
        }
        else
        {
            $featureTournaments = \Cache::get('menu_tournaments_'.$locale);
            $featureClub = \Cache::get('menu_clubs_'.$locale);
            $featureEvents = \Cache::get('menu_events_'.$locale);
            $internationalClub = \Cache::get('menu_international_clubs_'.$locale);

            $view->with('featureTournaments', $featureTournaments);
            $view->with('featureClub', $featureClub);
            $view->with('featureEvents', $featureEvents);
            $view->with('internationalClub', $internationalClub);
        }

    }
}

