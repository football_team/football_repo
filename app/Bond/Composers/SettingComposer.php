<?php namespace Bond\Composers;

use Setting;
use Config;
use Session;
use Route;

class SettingComposer {

    public function compose($view) {
        $settings = (Setting::get()->first()) ? Setting::get()->first()->toArray() : array();
        $view->with('settings', $settings);
        $view->with('ticketApi', Config::get('api.mage_soap_api_url'));
        $view->with('customer', Session::get('customer'));

        /*
         $path = Route::current()->getPath();
        if(preg_match('/^(news|news\/)$/', $path)) {
            $view->with('meta_title', 'Latest Football News | Football Ticket Pad');
            $view->with('meta_description', "Get the latest football news and see what top European matches are coming up. Fantastic articles in Football Ticket Pad's news section.");
            $view->with('meta_keywords', "" );
        }
        */

    }
}