<?php namespace Bond\Composers;

use Setting;
use Config;
use Session;
use FootballTickets;
use FootBallEvent;
use Cache;
use ImageController;


class FooterComposer {
    public function compose($view) {
        $locale = \App::getLocale();
        $cachName = 'sidebar_hotTickets_'.$locale;
        if (!Cache::has($cachName)) {

            $featureClub = FootballTickets::where('type', '=', 'club')
                ->leftJoin('football_ticket_meta', function ($join) {
                    $join->on('football_ticket.id', '=', 'football_ticket_meta.football_ticket_id')
                        ->where('football_ticket_meta.key','=', 'club_logo');
                })->where('widget', '=', '1')
                ->orderBy('widget_order', 'ASC')
                ->groupBy('football_ticket.id')
                ->take(10)
                ->get();

            foreach($featureClub as $club)
            {
                $trans = \DB::table('locale_text')
                    ->where('id', 'fbt_title_'.$club->football_ticket_id)
                    ->where('locale', $locale)
                    ->first();
                if($trans)
                {
                    $club->title = $trans->lstring;
                }
                $aLogo = ImageController::getSideBarImage('club', $club->football_ticket_id, $club->value);
                if($aLogo)
                {
                    $club->value = $aLogo;
                }

            }
            $view->with('featureClub', $featureClub);


            $hotTickets = FootBallEvent::where('widget_display', '=', '1')
                ->orderBy('widget_order', 'ASC')
                ->whereRaw('datetime >= NOW()')
                ->take(10)
                ->get();

            foreach($hotTickets as $tick)
            {
                $trans = \DB::table('locale_text')
                    ->where('id', 'event_title_'.$tick->id)
                    ->where('locale', $locale)
                    ->first();
                if($trans)
                {
                    if(trim($trans->lstring) != '')
                    {
                        $tick->title = $trans->lstring;
                    }
                }
            }
            $view->with('hotTickets', $hotTickets);


            $featureTournaments = FootballTickets::where('type', '=', 'league')
                ->leftJoin('football_ticket_meta', function ($join) {
                    $join->on('football_ticket.id', '=', 'football_ticket_meta.football_ticket_id')
                        ->where('football_ticket_meta.key','=', 'club_logo');
                })->where('widget', '=', '1')
                ->orderBy('widget_order', 'ASC')
                ->groupBy('football_ticket.id')
                ->take(10)
                ->get();

            foreach($featureTournaments as $tour)
            {

                $trans = \DB::table('locale_text')
                    ->where('id', 'fbt_title_'.$tour->football_ticket_id)
                    ->where('locale', $locale)
                    ->first();
                if($trans)
                {
                    $tour->title = $trans->lstring;
                }
                $aLogo = ImageController::getSideBarImage('league', $tour->football_ticket_id, $tour->value);
                if($aLogo)
                {
                    $tour->value = $aLogo;
                }
            }



            Cache::add('sidebar_hotTickets_'.$locale, $hotTickets, 300);
            Cache::add('sidebar_featureTournaments_'.$locale, $featureTournaments, 300);
            Cache::add('sidebar_featureClub_'.$locale, $featureClub, 300);
            $view->with('hotTickets', $hotTickets);
            $view->with('featureTournaments', $featureTournaments);
            $view->with('featureClub', $featureClub);
        }
        else{

            $hotTickets = Cache::get('sidebar_hotTickets_'.$locale);
            $featureTournaments = Cache::get('sidebar_featureTournaments_'.$locale);
            $featureClub = Cache::get('sidebar_featureClub_'.$locale);
            $view->with('hotTickets', $hotTickets);
            $view->with('featureTournaments', $featureTournaments);
            $view->with('featureClub', $featureClub);
        }


    }
}