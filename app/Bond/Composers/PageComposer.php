<?php namespace Bond\Composers;

use Setting;
use Pages;
use Bond\Repositories\Page\PageRepository as PageRepo;

class PageComposer {

    public function compose($view) {
        $locale = \App::getLocale();
        $uriArr = explode('?', $_SERVER['REQUEST_URI']);
        $uri = ltrim(rtrim($uriArr[0], '/'), '/');

        if($uri == '') { //homepage
            $node = null;
        } else {
            $node = Pages::parseUri($uri);
        }


        if ( $node ) {
            $pr = new PageRepo($node);
            if($pr->checkTrans($locale, $node->id))
            {
                $lText = $pr->getLText($node->id, $locale);
                $node->title = $lText['pt']->lstring;
                $node->content = $lText['pc']->lstring;
                $node->meta_keywords = $lText['pmc']->lstring;
                $node->meta_content = $lText['pmc']->lstring;
                $node->meta_title = $lText['pmt']->lstring;
                $node->meta_description = $lText['pmd']->lstring;
            }

            if(isset($node->meta_title)) {
                $view->with('meta_title', $node->meta_title);
            }
            $view->with('meta_description', $node->meta_description);
            $view->with('meta_keywords', $node->meta_content);
        }
    }
}