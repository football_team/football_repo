<?php
namespace Bond\Traits;

use Input;
use DB;
use Log;

trait GridPagination {
    private $filter = array();
    /**
     * @param       $tableName
     * @param array $columns
     *
     * @return array
     */
    public function getGridData ($tableName, array $columns, array $filtersIn = null, $id = null, $format = null)
    {
        if (!empty($tableName) || !empty($columns)) {

            $search = Input::get ('search');
            $order  = Input::get ('order');

            $orderColumn = isset($order[0]['column']) ? $columns[$order[0]['column']] : 'id';
            $orderDir    = isset($order[0]['dir']) ? $order[0]['dir'] : 'DESC';

            $search = isset($search['value']) ? $search['value'] : '';

            if(!empty($format) && $format=="eventsMigrate" && !empty($id)){

                $qb = DB::table ($tableName)
                    ->where('season_id','=',$id)
                    ->where('tournament_id','=','7')
                    ->where('migrate','=',0)
                    ->orderBy ($orderColumn, $orderDir);
            }else{
                $qb = DB::table ($tableName)
                    ->orderBy ($orderColumn, $orderDir);
            }

            if($filtersIn != null)
            {
                if(!empty($filtersIn))
                {
                    foreach($filtersIn as $key=>$val) {
                        $qb->where($key, $val[0], $val[1]);
                    }
                }
            }

            //build search conditions
            foreach ($columns as $key => $column) {
                if (!empty($search)) {
                    if ($key == 0) {
                        $qb->where ($column, 'Like', "%$search%");
                    } else {
                        $qb->orWhere ($column, 'Like', "%$search%");
                    }
                }
            }

            if(!empty($this->filter)) {
                foreach($this->filter as $key=>$val) {
                    $qb->where($key, $val);
                }
            }

            $totalItem = $qb->count ();

            $itemCollection = $qb->skip (Input::get ('start'))
                ->take (Input::get ('length'))
                ->get ();

            $data = [];

            foreach ($itemCollection as $item) {
                foreach ($columns as $column) {
                    if (property_exists ($item, $column)) {
                        $temp[] = $item->$column;
                    }
                }

                if (!empty($item->tournament_id)) {
                    $actionString = "<a href='".$item->id."' class=\"edit-node\">Edit</a> | <a href='".$item->id."' class=\"delete-node\">Delete</a>";
                    if($item->season_id==50 && $item->tournament_id==7 && $item->migrate==0){$actionString = "<a style=\"
    background: #7EC26D;
                    padding: 5px 15px;
    color: white;
    -webkit-border-radius: 5px;
    -moz-border-radius: 5px;
    border-radius: 5px;
\" href='/admin/migrate/2016/".$item->id."' class=''>Migrate</a>";}
                }else{
                    $stadium = Input::get('');
                    $actionString = "<a href='".$item->id."/".$id."/' class=\"edit-node\">Edit</a> | <a href='".$item->id."' class=\"delete-node\">Delete</a>";
                }

                $temp[] = $actionString;
                $data[] = $temp;
                unset($temp);
            }

            return array(
                'draw'            => Input::get ('draw'),
                'recordsTotal'    => $totalItem,
                'recordsFiltered' => $totalItem,
                'data'            => $data
            );
        } else {
            throw new \Exception ("Table name or columns is not provided");
        }
    }

    /**
     * @param array $filter
     */
    public function setFilter($filter = array()) {
        $this->filter = $filter;
    }
}