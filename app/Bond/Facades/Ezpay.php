<?php namespace Bond\Facades;

use Illuminate\Support\Facades\Facade;

class Ezpay extends Facade {

    protected static function getFacadeAccessor() {

        return 'ezpay';
    }
}
