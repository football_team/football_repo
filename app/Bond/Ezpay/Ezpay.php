<?php namespace Bond\Ezpay;

class Ezpay {

    /*Payment is Core*/
    //Gets session key for form tokenization*/
    public static function GetSessionKey($privateMerchantKey){
        $url = EZPAY_SERVER . "/hps/webservice/ezpay/CreateSessionKey";
        $params = array("privateMerchantKey" => $privateMerchantKey);
        $request = \Rest::CreateRequest($url, $params, "GET", 'Content-Type: application/json');
        $response = \Rest::GetResponse($request, "json");
        return $response->sessionkey;
    }

    //Pay with token
    public static function PayWithToken($object){
        $json_string = json_encode($object);
        $url = EZPAY_SERVER . "/hps/webservice/ezpay/PayWithToken";
        $request = \Rest::CreateRequest($url, $json_string, "POST", 'Content-Type: application/json');
        $response = \Rest::GetResponse($request, "json");
        return $response;
    }

    /*Payment is Secondary*/
    //Complete Transaction
    public static function AdjustTransaction($object){

        $json_string = json_encode($object);

        $url = EZPAY_SERVER . "/hps/webservice/ezpay/AdjustTransaction";
        $request = \Rest::CreateRequest($url, $json_string, "POST", 'Content-Type: application/json');
        $response = \Rest::GetResponse($request, "json");
        return $response;
    }

    public static function PayWithReference($object){

        $json_string = json_encode($object);

        $url = EZPAY_SERVER . "/hps/webservice/ezpay/PayWithReference";
        $request = \Rest::CreateRequest($url, $json_string, "POST", 'Content-Type: application/json');
        $response = \Rest::GetResponse($request, "json");
        return $response;
    }
}


