<?php namespace Bond\Rest;

class Rest {

    public static function CreateRequest($url, $params = null, $method = "GET", $header = null)
    {
        $httpOptions = array(
            'method' => $method,
            'ignore_errors' => true,
            'header' => $header
        );

        if ($params !== null) {
            if(is_object($params) || is_array($params))
                $params = http_build_query($params);

            if ($method == 'POST' || $method == 'PUT') {
                $httpOptions['content'] = $params;
            } else {
                $url .= '?' . $params;
            }
        }

        return array(
            "url" => $url,
            "httpOptions" => $httpOptions
        );
    }

    /**
    Note:
    For fopen to work over https 'php_openssl' extension must be enabled, and 'allow_url_fopen' must be set  to 'on'.
    Add the following two lines in php.ini to enable:
    extension=php_openssl.dll
    allow_url_fopen = On
    Else, consider using other libraries like cURL that lets you make HTTP requests in PHP
    **/
    public static function GetResponse($request, $format)
    {
        // Download cacert.pem from https://curl.haxx.se/ca/cacert.pem
        // curl --remote-name --time-cond cacert.pem https://curl.haxx.se/ca/cacert.pem
        // https://curl.haxx.se/docs/caextract.html
        // This has all the cert verification company public keys
        $cparams = array(
            "ssl"=>array(
                "verify_peer"=>true,
                "verify_peer_name"=>true,
                "cafile" => "/etc/letsencrypt/cacert.pem",
            ),
            'http' => $request['httpOptions']
        );
        $context = stream_context_create($cparams);

        $fp = fopen($request['url'], 'rb', false, $context);
        if (!$fp) {
            $res = false;
        } else {
            // If you're trying to troubleshoot problems, try uncommenting the
            // next two lines; it will show you the HTTP response headers across
            // all the redirects:
            // $meta = stream_get_meta_data($fp);
            // var_dump($meta['wrapper_data']);
            $res = stream_get_contents($fp);
        }

        if ($res === false) {
            $method = $request['httpOptions']['method'];
            $url = $request['url'];
            throw new Exception("$method $url failed: $php_errormsg");
        }
        switch ($format) {
            case 'json':
              $r = json_decode($res);
              if ($r === null) {

                throw new Exception("failed to decode $res as json");
              }
              return $r;

            case 'xml':
              $r = simplexml_load_string($res);
              if ($r === null) {
                throw new Exception("failed to decode $res as xml");
              }
              return $r;
        }
        return $res;
    }
}

