<?php namespace Bond\Repositories\Page;

use DB;
use Pages;
use PagesMeta;
use Config;
use Response;
use Bond\Traits\GridPagination;
use Bond\Repositories\BaseRepositoryInterface as BaseRepositoryInterface;
use Bond\Exceptions\Validation\ValidationException;
use Bond\Repositories\AbstractValidator as Validator;

class PageRepository extends Validator implements BaseRepositoryInterface
{

    use GridPagination;

    protected $perPage;
    protected $page;

    protected $pageMeta = ['feature_image', 'template'];
    /**
     * Rules
     *
     * @var array
     */
    protected static $rules = [
        'title'   => 'required|min:3',
        'content' => 'required|min:5'
    ];

    public function __construct(Pages $page = null)
    {

        if (empty($page))
            $page = new Pages();

        $config        = Config::get('bondcms');
        $this->perPage = $config['modules']['per_page'];
        $this->page    = $page;
    }

    public function all()
    {

        return $this->page->orderBy('created_at', 'DESC')->get();
    }

    public function lists()
    {

        return $this->page->get()->lists('title', 'slug');
    }

    public function paginate($perPage = null)
    {

        if ($this->page instanceof Page) {
            return $this->page->paginate(($perPage) ? $perPage : $this->perPage);
        }
        return $perPage;
    }

    public function find($id)
    {
        $locale = \App::getLocale();
        $page = $this->page->findOrFail($id);
        if($this->checkTrans($locale, $id))
        {
            $lText = $this->getLText($id, $locale);
            $page->title = $lText['pt']->lstring;
            $page->content = $lText['pc']->lstring;
            $page->meta_content = $lText['pmc']->lstring;
            $page->meta_title = $lText['pmt']->lstring;
            $page->meta_description = $lText['pmd']->lstring;
        }
        return $page;
    }

    public function create($attributes)
    {
        $locale = 'en_gb';
        $attributes['is_published'] = isset($attributes['is_published']) ? true : false;

        if ($this->isValid($attributes)) {
            $this->page->fill($attributes)->save();
            $id = $this->page->id;
            $this->createLText($id, $locale, $attributes);
            $this->savePageMeta($this->page->id, $attributes);
            return;
        }

        throw new ValidationException('Page validation failed', $this->getErrors());
    }

    public function update($id, $attributes)
    {
        $locale = \App::getLocale();
        $attributes['is_published'] = isset($attributes['is_published']) ? true : false;

        $this->page = $this->find($id);

        if ($this->isValid($attributes)) {
            if($locale == 'en_gb')
            {
               $this->page->fill($attributes)->save(); 
            }
            if($this->checkTrans($locale, $id))
            {
                $this->updateLText($id, $attributes, $locale);
            }
            else
            {
                $this->createLText($id, $locale, $attributes);
            }

            $this->savePageMeta($this->page->id, $attributes);
            return true;
        }

        throw new ValidationException('Category validation failed', $this->getErrors());
    }

    public function destroy($id)
    {
        $this->page->findOrFail($id)->delete();
        $this->flushLText($id);
    }

    public function togglePublish($id)
    {

        $page               = $this->page->find($id);
        $page->is_published = ($page->is_published) ? false : true;
        $page->save();
        return Response::json(array('result' => 'success', 'changed' => ($page->is_published) ? 1 : 0));
    }

    public function savePageMeta($id, $attributes)
    {
        foreach ($this->pageMeta as $meta) {
            if (isset($attributes[$meta])) {
                $pagesMeta   = new PagesMeta();
                $metaOptions = [
                    'page_id'      => $id,
                    'meta_keyword' => $meta,
                    'meta_content' => $attributes[$meta]
                ];

                $pagesMeta->setMetaKey($metaOptions);
            }
        }
    }

    public function updateLText($id, $attributes, $locale)
    {
        \DB::table('locale_text')
            ->where('id', "page_title_".$id)
            ->where('locale', $locale)
            ->update(array('lstring' => $attributes['title']));

        \DB::table('locale_text')
            ->where('id', "page_content_".$id)
            ->where('locale', $locale)
            ->update(array('lstring' => $attributes['content']));

        \DB::table('locale_text')
            ->where('id', "page_meta_title_".$id)
            ->where('locale', $locale)
            ->update(array('lstring' => $attributes['meta_title']));

        \DB::table('locale_text')
            ->where('id', "page_meta_content_".$id)
            ->where('locale', $locale)
            ->update(array('lstring' => $attributes['meta_content']));

        \DB::table('locale_text')
            ->where('id', "page_meta_desc_".$id)
            ->where('locale', $locale)
            ->update(array('lstring' => $attributes['meta_description']));

    }

    public function createLText($id, $locale, $attributes)
    {
        $fbt_m_t = new \LocaleText;
        $fbt_m_t->id = "page_meta_title_".$id;
        $fbt_m_t->locale = $locale;
        $fbt_m_t->lstring = $attributes['meta_title'];
        $fbt_m_t->save();

        $fbt_m_d = new \LocaleText;
        $fbt_m_d->id = "page_meta_desc_".$id;
        $fbt_m_d->locale = $locale;
        $fbt_m_d->lstring = $attributes['meta_description'];
        $fbt_m_d->save();

        $fbt_m_kw = new \LocaleText;
        $fbt_m_kw->id = "page_meta_content_".$id;
        $fbt_m_kw->locale = $locale;
        $fbt_m_kw->lstring = $attributes['meta_content'];
        $fbt_m_kw->save();

        $fbt_c = new \LocaleText;
        $fbt_c->id = "page_content_".$id;
        $fbt_c->locale = $locale;
        $fbt_c->lstring = $attributes['content'];
        $fbt_c->save();

        $fbt_t = new \LocaleText;
        $fbt_t->id = "page_title_".$id;
        $fbt_t->locale = $locale;
        $fbt_t->lstring = $attributes['title'];
        $fbt_t->save();

        return true;
    }

    public function getLText($id, $locale)
    {
        $rArray = array();

        $rArray['pmt'] = \DB::table('locale_text')
                            ->where('id', "page_meta_title_".$id)
                            ->where('locale', $locale)
                            ->first();

        $rArray['pmc'] = \DB::table('locale_text')
                            ->where('id', "page_meta_content_".$id)
                            ->where('locale', $locale)
                            ->first();

        $rArray['pmd'] = \DB::table('locale_text')
                            ->where('id', "page_meta_desc_".$id)
                            ->where('locale', $locale)
                            ->first();

        $rArray['pc'] = \DB::table('locale_text')
                           ->where('id', "page_content_".$id)
                           ->where('locale', $locale)
                           ->first();

        $rArray['pt'] = \DB::table('locale_text')
                            ->where('id', "page_title_".$id)
                            ->where('locale', $locale)
                            ->first();

        return $rArray;
    } 


    public function flushLText($id)
    {
         \DB::table('locale_text')
            ->where('id', "page_meta_title_".$id)
            ->delete();

         \DB::table('locale_text')
            ->where('id', "page_meta_content_".$id)
            ->delete();

         \DB::table('locale_text')
            ->where('id', "page_meta_desc_".$id)
            ->delete();

        \DB::table('locale_text')
           ->where('id', "page_content_".$id)
           ->delete();

        \DB::table('locale_text')
            ->where('id', "page_title_".$id)
            ->delete();
    }

    public function checkTrans($locale, $id)
    {
        $trans = \DB::table('locale_text')
                    ->where('id', 'page_title_'.$id)
                    ->where('locale', $locale)
                    ->first();

        if($trans)
        {
            return true;
        }
        return false;
    }
}
