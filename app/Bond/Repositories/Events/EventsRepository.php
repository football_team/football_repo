<?php namespace Bond\Repositories\Events;

use Config;
use FootBallEvent;
use Response;
use Bond\Repositories\BaseRepositoryInterface as BaseRepositoryInterface;
use Bond\Exceptions\Validation\ValidationException;
use Bond\Repositories\AbstractValidator as Validator;

class EventsRepository extends Validator implements BaseRepositoryInterface {

    /**
     * Rules
     *
     * @var array
     */
    protected static $rules = [
        'title'    => 'required',
        'content'  => 'required',
        'datetime' => 'required|date',
    ];
    protected $perPage;
    protected $events;

    public function __construct(FootBallEvent $events) {

        $config = Config::get('bondcms');
        $this->perPage = $config['modules']['per_page'];
        $this->events = $events;
    }

    public function all() {

        return $this->news->orderBy('created_at', 'DESC')
            ->where('is_published', 1)
            ->get();
    }

    public function lists() {

        return $this->events->get()->lists('title', 'id');
    }

    public function paginate($perPage = null, $all=false) {
        if($all)
            return $this->events->orderBy('created_at', 'DESC')
            ->paginate(($perPage) ? $perPage : $this->perPage);

        return $this->events->orderBy('created_at', 'DESC')
            ->where('is_published', 1)
            ->paginate(($perPage) ? $perPage : $this->perPage);
    }

    public function create($attributes) {
        $locale = 'en_gb';
        $attributes['is_published']     = isset($attributes['is_published']) ? true : false;
        $attributes['event_in_home']    = isset($attributes['event_in_home']) ? true : false;

        if (isset($attributes['datetime']) && !empty($attributes['datetime'])) {
            $dateArr = explode(' ', $attributes['datetime']);
            $d1 = explode('/', $dateArr[0]);
            $attributes['datetime'] = "{$d1[2]}-{$d1[1]}-{$d1[0]} {$dateArr[1]}:00";
        }
        if ($this->isValid($attributes)) {
            if(array_key_exists('ttypes', $attributes))
            {

                $ttypes = $attributes['ttypes'];
                $ttypeStr = '';
                foreach($ttypes as $ttype)
                {
                    $ttypeStr.=$ttype.',';
                }

                $this->events->ticket_type_ids = $ttypeStr;

                unset($attributes['ttypes']);

            }
            else
            {
                $this->events->ticket_type_ids = '';
            }
            $this->events->fill($attributes)->save();
            $id = $this->events->id;
            $this->createLText($id, $locale, $attributes);
            return $id;
        }

        throw new ValidationException('Events validation failed', $this->getErrors());
    }

    public function createLText($id, $locale, $attributes)
    {
        $fbt_m_t = new \LocaleText;
        $fbt_m_t->id = "event_meta_title_" . $id;
        $fbt_m_t->locale = $locale;
        $fbt_m_t->lstring = $attributes['meta_title'];
        $fbt_m_t->save();

        $fbt_m_d = new \LocaleText;
        $fbt_m_d->id = "event_meta_desc_" . $id;
        $fbt_m_d->locale = $locale;
        $fbt_m_d->lstring = $attributes['meta_description'];
        $fbt_m_d->save();

        $fbt_m_kw = new \LocaleText;
        $fbt_m_kw->id = "event_meta_kw_" . $id;
        $fbt_m_kw->locale = $locale;
        $fbt_m_kw->lstring = $attributes['meta_keyword'];
        $fbt_m_kw->save();

        $fbt_c = new \LocaleText;
        $fbt_c->id = "event_content_" . $id;
        $fbt_c->locale = $locale;
        $fbt_c->lstring = $attributes['content'];
        $fbt_c->save();

        $fbt_t = new \LocaleText;
        $fbt_t->id = "event_title_" . $id;
        $fbt_t->locale = $locale;
        $fbt_t->lstring = $attributes['title'];
        $fbt_t->save();

        return true;
    }

    public function update($id, $attributes) {
        $locale = \App::getLocale();
        $attributes['is_published']     = isset($attributes['is_published']) ? true : false;
        $attributes['event_in_home']    = isset($attributes['event_in_home']) ? true : false;

        if (isset($attributes['datetime']) && !empty($attributes['datetime'])) {
            $dateArr = explode(' ', $attributes['datetime']);
            $d1 = explode('/', $dateArr[0]);
            $attributes['datetime'] = "{$d1[2]}-{$d1[1]}-{$d1[0]} {$dateArr[1]}:00";
        }

        $this->events = $this->find($id);

        if ($this->isValid($attributes)) {
            if($locale == 'en_gb')
            {
                if(array_key_exists('ttypes', $attributes))
                {

                    $ttypes = $attributes['ttypes'];
                    $ttypeStr = '';
                    foreach($ttypes as $ttype)
                    {
                        $ttypeStr.=$ttype.',';
                    }

                    $this->events->ticket_type_ids = $ttypeStr;

                    unset($attributes['ttypes']);

                }
                else
                {
                    $this->events->ticket_type_ids = '';
                }
               $this->events->fill($attributes)->save(); 
            }
            if($this->checkTrans($locale, $id))
            {
                $this->updateLText($id, $attributes, $locale);
            }
            else
            {
                $this->createLText($id, $locale, $attributes);
            }
            return true;
        }

        throw new ValidationException('Events validation failed', $this->getErrors());
    }

    public function find($id)
    {

        $locale = \App::getLocale();
        $event = $this->events->findOrFail($id);

        if ($this->checkTrans($locale, $id)) {
            $lText = $this->getLText($id, $locale);
            if (!empty($lText['emt']->lstring)) {
                $event->meta_title = $lText['emt']->lstring;
            }
            if (!empty($lText['emd']->lstring)) {
                $event->meta_description = $lText['emd']->lstring;
            }
            if (!empty($lText['emk']->lstring)) {
                $event->meta_keyword = $lText['emk']->lstring;
            }
            if (!empty($lText['ec']->lstring)) {
                $event->content = $lText['ec']->lstring;
            }
            if (!empty($lText['et']->lstring)) {
                $event->title = $lText['et']->lstring;
            }
        }

        return $event;
    }

    public function checkTrans($locale, $id)
    {
        $trans = \DB::table('locale_text')
            ->where('id', 'event_title_' . $id)
            ->where('locale', $locale)
            ->first();

        if ($trans) {
            return true;
        }
        return false;
    }

    public function getLText($id, $locale)
    {
        $rArray = array();

        $rArray['emt'] = \DB::table('locale_text')
            ->where('id', "event_meta_title_" . $id)
            ->where('locale', $locale)
            ->first();

        $rArray['emk'] = \DB::table('locale_text')
            ->where('id', "event_meta_kw_" . $id)
            ->where('locale', $locale)
            ->first();

        $rArray['emd'] = \DB::table('locale_text')
            ->where('id', "event_meta_desc_" . $id)
            ->where('locale', $locale)
            ->first();

        $rArray['ec'] = \DB::table('locale_text')
            ->where('id', "event_content_" . $id)
            ->where('locale', $locale)
            ->first();

        $rArray['et'] = \DB::table('locale_text')
            ->where('id', "event_title_" . $id)
            ->where('locale', $locale)
            ->first();

        return $rArray;
    }

    public function updateLText($id, $attributes, $locale)
    {
        $res = \DB::table('locale_text')
            ->where('id', "event_title_".$id)
            ->where('locale', $locale)
            ->first();
        if(!$res)
        {
            \DB::table('locale_text')
                ->insert([
                    'id' => "event_title_".$id,
                    'locale' => $locale,
                    'lstring' => $attributes['title']
                ]);
        }
        else
        {
            \DB::table('locale_text')
                ->where('id', "event_title_".$id)
                ->where('locale', $locale)
                ->update(array('lstring' => $attributes['title']));
        }

        $res = \DB::table('locale_text')
            ->where('id', "event_content_".$id)
            ->where('locale', $locale)
            ->first();

        if(!$res)
        {
            \DB::table('locale_text')
                ->insert([
                    'id' => "event_content_".$id,
                    'locale' => $locale,
                    'lstring' => $attributes['content']
                ]);
        }
        else
        {
            \DB::table('locale_text')
                ->where('id', "event_content_".$id)
                ->where('locale', $locale)
                ->update(array('lstring' => $attributes['content']));
        }

        $res = \DB::table('locale_text')
            ->where('id', "event_meta_title_".$id)
            ->where('locale', $locale)
            ->first();

        if(!$res)
        {
            \DB::table('locale_text')
                ->insert([
                    'id' => "event_meta_title_".$id,
                    'locale' => $locale,
                    'lstring' => $attributes['meta_title']
                ]);
        }
        else
        {
            \DB::table('locale_text')
                ->where('id', "event_meta_title_".$id)
                ->where('locale', $locale)
                ->update(array('lstring' => $attributes['meta_title']));
        }

        $res = \DB::table('locale_text')
            ->where('id', "event_meta_kw_".$id)
            ->where('locale', $locale)
            ->first();

        if(!$res)
        {
            \DB::table('locale_text')
                ->insert([
                    'id' => "event_meta_kw_".$id,
                    'locale' => $locale,
                    'lstring' => $attributes['meta_keyword']
                ]);
        }
        else
        {
            \DB::table('locale_text')
                ->where('id', "event_meta_kw_".$id)
                ->where('locale', $locale)
                ->update(array('lstring' => $attributes['meta_keyword']));
        }

        $res = \DB::table('locale_text')
            ->where('id', "event_meta_desc_".$id)
            ->where('locale', $locale)
            ->first();

        if(!$res)
        {
            \DB::table('locale_text')
                ->insert([
                    'id' => "event_meta_desc_".$id,
                    'locale' => $locale,
                    'lstring' => $attributes['meta_description']
                ]);
        }
        else
        {
            \DB::table('locale_text')
                ->where('id', "event_meta_desc_".$id)
                ->where('locale', $locale)
                ->update(array('lstring' => $attributes['meta_description']));
        }
    }

    public function destroy($id)
    {

        $events = $this->events->find($id)->delete();
        $this->flushLText($id);
    }

    public function flushLText($id)
    {
         \DB::table('locale_text')
            ->where('id', "event_meta_title_".$id)
            ->delete();

         \DB::table('locale_text')
            ->where('id', "event_meta_kw_".$id)
            ->delete();

         \DB::table('locale_text')
            ->where('id', "event_meta_desc_".$id)
            ->delete();

        \DB::table('locale_text')
           ->where('id', "event_content_".$id)
           ->delete();

        \DB::table('locale_text')
            ->where('id', "event_title_".$id)
            ->delete();
    }

    public function togglePublish($id)
    {

        $events = $this->events->find($id);
        $events->is_published = ($events->is_published) ? false : true;
        $events->save();

        return Response::json(array('result' => 'success', 'changed' => ($events->is_published) ? 1 : 0));
    }
}
