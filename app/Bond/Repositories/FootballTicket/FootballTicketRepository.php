<?php namespace Bond\Repositories\FootballTicket;

use Input;
use Config;
use FootballTickets;
use FootballTicketMeta;
use Response;
use Bond\Repositories\BaseRepositoryInterface as BaseRepositoryInterface;
use Bond\Exceptions\Validation\ValidationException;
use Bond\Repositories\AbstractValidator as Validator;
use \App\LocaleText as LocaleText;

class FootballTicketRepository extends Validator implements BaseRepositoryInterface {

    protected $perPage;
    protected $footballTicket;
    protected $slug_prefix = 'club';
    protected $meta_data = array('club_logo', 'nickname', 'founded', 'rivals','recorded_goal_scorer', 'record_signing', 'country', 'season');

    /**
     * Rules
     *
     * @var array
     */
    protected static $rules = [
        'title'    => 'required',
        'content'  => 'required'
    ];


    public function __construct(FootballTickets $footballTicket) {

        $config = Config::get('bondcms');
        $this->perPage = $config['modules']['per_page'];
        $this->footballTicket= $footballTicket;

        $type = Input::get('action_type');
        if(!empty($type)) {
            $this->slug_prefix =  $type;
        }
    }

    /**
     * @param string $slug_prefix
     */
    public function setSlugPrefix($slug_prefix)
    {
        $this->slug_prefix = $slug_prefix;
    }

    public function all() {

        return $this->footballTicket->orderBy('created_at', 'DESC')
            ->where('is_published', 1)
            ->get();
    }

    public function lists() {

        return $this->footballTicket->get()->lists('title', 'id');
    }

    public function paginate($perPage = null, $all=false) {

        if($all)
            return $this->footballTicket
                ->where('type', '=',$this->slug_prefix)
                ->orderBy('created_at', 'DESC')
                ->paginate(($perPage) ? $perPage : $this->perPage);

        return $this->footballTicket
            ->where('type', '=',$this->slug_prefix)
            ->orderBy('created_at', 'DESC')
            ->where('is_published', 1)
            ->paginate(($perPage) ? $perPage : $this->perPage);
    }

    public function find($id) {
        $locale = \App::getLocale();
        $lText = $this->getLText($id, $locale);
        $fbt = $this->footballTicket->findOrFail($id);

        if($lText['fbtt'])
        {
            $fbt->meta_title = $lText['fbtmt']->lstring;
            $fbt->meta_description = $lText['fbtmd']->lstring;
            $fbt->meta_content = $lText['fbtmc']->lstring;
            $fbt->content = $lText['fbtc']->lstring;
            $fbt->title = $lText['fbtt']->lstring;
        }

        return $fbt;
    }

    public function findByUri($type, $slug) {
        return $this->footballTicket->where('type', '=', $type)
                                    ->where('slug', '=', $slug)
                                    ->first();
    }

    public function findByType($type = '') {
        return $this->footballTicket->where('type', '=', $type)
            ->orderBy('created_at', 'ASC')
            ->groupBy('slug')
            ->get();
    }

    public function create($attributes) {
        $locale = 'en_gb';

        $attributes['is_published'] = isset($attributes['is_published']) ? true : false;
        if ($this->isValid($attributes)) {
            $this->footballTicket->fill($attributes)->save();
            $id = $this->footballTicket->id;
            $this->createLText($id, $locale, $attributes);

            if(in_array($this->footballTicket->type, array('club', 'league'))) {
                $footballTicketMeta = new FootballTicketMeta();
                if(isset($attributes['country']) && $attributes['country'] != '') {
                    $footballTicketMeta->fill(
                        array(
                            'football_ticket_id' => $this->footballTicket->id,
                            'key'                => 'country',
                            'value'              => $attributes['country']
                        )
                    )->save();
                }

                foreach($this->meta_data as $_val) {
                    if(isset($attributes[$_val])) {
                        $footballTicketMeta = new FootballTicketMeta();
                        $footballTicketMeta->fill(
                            array(
                                'football_ticket_id' => $this->footballTicket->id,
                                'key'                => $_val,
                                'value'              => $attributes[$_val]
                            )
                        )->save();
                    }
                }

            }
            return $id;
        }

        throw new ValidationException('Football Ticket validation failed', $this->getErrors());
    }

    public function update($id, $attributes) {
        $locale = \App::getLocale();
        
        $attributes['is_published'] = isset($attributes['is_published']) ? true : false;

        $this->footballTicket = $this->find($id);
        $type = $this->slug_prefix;

        if ($this->isValid($attributes)) {
            if($locale == 'en_gb')
            {
                $this->footballTicket->fill($attributes)->save();
            }

            $hasDat = \DB::table('locale_text')
                        ->where('id', "fbt_title_".$id)
                        ->where('locale', $locale)
                        ->first();

            if($hasDat)
            {
                $this->updateLText($id, $attributes, $locale);
            }
            else
            {
                $this->createLText($id, $locale, $attributes);
            }
            
            $footballTicketMeta = new FootballTicketMeta();
            if(in_array($this->footballTicket->type, array('club', 'league'))) {

                foreach($this->meta_data as $_val) {
                    if(isset($attributes[$_val])) {
                        $footballTicketMeta = $footballTicketMeta->where('football_ticket_id', '=', $this->footballTicket->id)
                            ->where('key', '=', $_val)->delete();

                        $footballTicketMeta = new FootballTicketMeta();
                        $footballTicketMeta->fill(
                            array(
                                'football_ticket_id' => $this->footballTicket->id,
                                'key'                => $_val,
                                'value'              => $attributes[$_val]
                            )
                        )->save();
                    }
                }

            }

            return true;
        }

        throw new ValidationException('Football Ticket validation failed', $this->getErrors());
    }

    public function destroy($id) {

        $footballTicket = $this->footballTicket->find($id)->delete();
        $this->flushLText($id);
    }

    public function togglePublish($id) {

        $footballTicket = $this->footballTicket->find($id);
        $footballTicket->is_published = ($footballTicket->is_published) ? false : true;
        $footballTicket->save();

        return Response::json(array('result' => 'success', 'changed' => ($footballTicket->is_published) ? 1 : 0));
    }

    public function updateLText($id, $attributes, $locale)
    {
        \DB::table('locale_text')
            ->where('id', "fbt_title_".$id)
            ->where('locale', $locale)
            ->update(array('lstring' => $attributes['title']));

        \DB::table('locale_text')
            ->where('id', "fbt_content_".$id)
            ->where('locale', $locale)
            ->update(array('lstring' => $attributes['content']));

        \DB::table('locale_text')
            ->where('id', "fbt_meta_title_".$id)
            ->where('locale', $locale)
            ->update(array('lstring' => $attributes['meta_title']));

        \DB::table('locale_text')
            ->where('id', "fbt_meta_content_".$id)
            ->where('locale', $locale)
            ->update(array('lstring' => $attributes['meta_content']));

        \DB::table('locale_text')
            ->where('id', "fbt_meta_description_".$id)
            ->where('locale', $locale)
            ->update(array('lstring' => $attributes['meta_description']));

    }

    public function createLText($id, $locale, $attributes)
    {
        $fbt_m_t = new \LocaleText;
        $fbt_m_t->id = "fbt_meta_title_".$id;
        $fbt_m_t->locale = $locale;
        $fbt_m_t->lstring = $attributes['meta_title'];
        $fbt_m_t->save();

        $fbt_m_d = new \LocaleText;
        $fbt_m_d->id = "fbt_meta_description_".$id;
        $fbt_m_d->locale = $locale;
        $fbt_m_d->lstring = $attributes['meta_description'];
        $fbt_m_d->save();

        $fbt_m_kw = new \LocaleText;
        $fbt_m_kw->id = "fbt_meta_content_".$id;
        $fbt_m_kw->locale = $locale;
        $fbt_m_kw->lstring = $attributes['meta_content'];
        $fbt_m_kw->save();

        $fbt_c = new \LocaleText;
        $fbt_c->id = "fbt_content_".$id;
        $fbt_c->locale = $locale;
        $fbt_c->lstring = $attributes['content'];
        $fbt_c->save();

        $fbt_t = new \LocaleText;
        $fbt_t->id = "fbt_title_".$id;
        $fbt_t->locale = $locale;
        $fbt_t->lstring = $attributes['title'];
        $fbt_t->save();

        return true;
    }

    public function getLText($id, $locale)
    {
        $rArray = array();

        $rArray['fbtmt'] = \DB::table('locale_text')
                            ->where('id', "fbt_meta_title_".$id)
                            ->where('locale', $locale)
                            ->first();

        $rArray['fbtmd'] = \DB::table('locale_text')
                            ->where('id', "fbt_meta_description_".$id)
                            ->where('locale', $locale)
                            ->first();

        $rArray['fbtmc'] = \DB::table('locale_text')
                            ->where('id', "fbt_meta_content_".$id)
                            ->where('locale', $locale)
                            ->first();

        $rArray['fbtc'] = \DB::table('locale_text')
                           ->where('id', "fbt_content_".$id)
                           ->where('locale', $locale)
                           ->first();

        $rArray['fbtt'] = \DB::table('locale_text')
                            ->where('id', "fbt_title_".$id)
                            ->where('locale', $locale)
                            ->first();

        return $rArray;
    } 


    public function flushLText($id)
    {
         \DB::table('locale_text')
            ->where('id', "fbt_meta_title_".$id)
            ->delete();

         \DB::table('locale_text')
            ->where('id', "fbt_meta_description_".$id)
            ->delete();

         \DB::table('locale_text')
            ->where('id', "fbt_meta_content_".$id)
            ->delete();

        \DB::table('locale_text')
           ->where('id', "fbt_content_".$id)
           ->delete();

        \DB::table('locale_text')
            ->where('id', "fbt_title_".$id)
            ->delete();
    }

    public function checkTrans($locale, $id)
    {
        $trans = \DB::table('locale_text')
                    ->where('id', 'fbt_title_'.$id)
                    ->where('locale', $locale)
                    ->first();

        if($trans)
        {
            return true;
        }
        return false;
    }
}

