<?php namespace Bond\Repositories\Article;

use Article;
use Config;
use Response;
use Tag;
use Category;
use Str;
use Event;
use Bond\Repositories\BaseRepositoryInterface as BaseRepositoryInterface;
use Bond\Exceptions\Validation\ValidationException;
use Bond\Repositories\AbstractValidator as Validator;
use \App\LocaleText as LocaleText;

class ArticleRepository extends Validator implements BaseRepositoryInterface {

    protected $perPage;
    protected $article;

    /**
     * Rules
     *
     * @var array
     */
    protected static $rules = [
        'title'   => 'required',
        'content' => 'required'
    ];

    public function __construct(Article $article) {

        $config = Config::get('bondcms');
        $this->perPage = $config['modules']['per_page'];
        $this->article = $article;
    }

    public function all() {

        return $this->article->with('tags')->orderBy('created_at', 'DESC')
            ->where('is_published', 1)
            ->get();
    }

    public function lists() {

        return $this->article->get()->lists('title', 'id');
    }

    public function paginate($perPage = null, $all=false) {
        $locale = \App::getLocale();

        $in = \DB::table('locale_text')
                    ->where('id', 'like', 'art_title_%')
                    ->select(\DB::raw('SUBSTRING_INDEX(id, "_", -1) as ids'))
                    ->where('locale', $locale)
                    ->get();

        $share = array();

        foreach($in as $i)
        {
            $share[] = $i->ids;
        }

        if($locale != 'en_gb')
        {
            if($all)
                return $this->article->with('tags')->whereIn('id', $share)->orderBy('created_at', 'DESC')
                    ->paginate(($perPage) ? $perPage : $this->perPage);

            return $this->article->with('tags')->whereIn('id', $share)->orderBy('created_at', 'DESC')
                ->where('is_published', 1)
                ->paginate(($perPage) ? $perPage : $this->perPage);
        }

        if($all)
            return $this->article->with('tags')->orderBy('created_at', 'DESC')
                ->paginate(($perPage) ? $perPage : $this->perPage);

        return $this->article->with('tags')->orderBy('created_at', 'DESC')
            ->where('is_published', 1)
            ->paginate(($perPage) ? $perPage : $this->perPage);
    }

    public function find($id) {
        //So I need to replace the data in the artcle with the locale text associated with it.
        $locale = \App::getLocale();
        $lText = $this->getLText($id, $locale);
        $art = $this->article->with(['tags', 'category'])->findOrFail($id);
        if($lText['ac'])
        {
            $art->excerpt = $lText['ame']->lstring;
            $art->meta_title = $lText['amt']->lstring;
            $art->meta_description = $lText['amd']->lstring;
            $art->meta_keywords = $lText['amk']->lstring;
            $art->content = $lText['ac']->lstring;
            $art->title = $lText['at']->lstring;
        }
        
        return $art;
    }

    public function findBySlug($slug) {
        return $this->article->with(['tags', 'category'])->where('slug', '=', $slug)->first();
    }

    public function create($attributes) {
        $locale = 'en_gb';
        $attributes['is_published'] = isset($attributes['is_published']) ? true : false;

        if ($this->isValid($attributes)) {

            if ($this->article->fill($attributes)->save()) {

                $category = Category::find($attributes['category']);
                $category->articles()->save($this->article);
            }
            $id = $this->article->id;
            $this->createLText($id, $attributes, $locale);
            if(isset($attributes['tag']))
            {
              $articleTags = explode(',', $attributes['tag']);

                foreach ($articleTags as $articleTag) {

                    if (!$articleTag) continue;

                    $tag = Tag::where('name', '=', $articleTag)->first();

                    if (!$tag) $tag = new Tag; 

                    $tag->name = $articleTag;
                    $tag->slug = Str::slug($articleTag);
                    $this->article->tags()->save($tag);
                }  
            }
            

            //Event::fire('article.created', $this->article);
            Event::fire('article.creating', $this->article);
            return $this->article;
        }

        throw new ValidationException('Article validation failed', $this->getErrors());
    }

    public function update($id, $attributes) {
        $locale = \App::getLocale();
        $this->article = $this->find($id);
        $attributes['is_published'] = isset($attributes['is_published']) ? true : false;

        if ($this->isValid($attributes)) {
            $flag = 0;
            if($locale == 'en_gb')
            {
                $flag = $this->article->fill($attributes)->save();
            }
            if ($flag) {

                $category = Category::find($attributes['category']);
                $category->articles()->save($this->article);
            }

            $a_m_e = \DB::table('locale_text')
                        ->where('id', "art_meta_ex_".$id)
                        ->where('locale', $locale)
                        ->first();

            if($a_m_e)
            {
                $this->editLText($id, $attributes, $locale);
            }
            else
            {
                $this->createLText($id, $attributes, $locale);
            }
            if(isset($attributes['tag']))
            {
               $articleTags = explode(',', $attributes['tag']);
                foreach ($articleTags as $articleTag) {

                    if (!$articleTag) continue;

                    $tag = Tag::where('name', '=', $articleTag)->first();

                    if (!$tag) $tag = new Tag;

                    $tag->name = $articleTag;
                    $tag->slug = Str::slug($articleTag);
                    $this->article->tags()->save($tag);
                } 
            }
            
            return $this->article;
        }

        throw new ValidationException('Article validation failed', $this->getErrors());
    }

    public function editLText($id, $attributes, $locale)
    {
        \DB::table('locale_text')
            ->where('id', "art_meta_ex_".$id)
            ->where('locale', $locale)
            ->update(array('lstring' => $attributes['excerpt']));

        \DB::table('locale_text')
            ->where('id', "art_meta_title_".$id)
            ->where('locale', $locale)
            ->update(array('lstring' => $attributes['meta_title']));

        \DB::table('locale_text')
            ->where('id', "art_meta_desc_".$id)
            ->where('locale', $locale)
            ->update(array('lstring' => $attributes['meta_description']));

        \DB::table('locale_text')
            ->where('id', "art_meta_kw_".$id)
            ->where('locale', $locale)
            ->update(array('lstring' => $attributes['meta_keywords']));

        \DB::table('locale_text')
            ->where('id', "art_content_".$id)
            ->where('locale', $locale)
            ->update(array('lstring' => $attributes['content']));

        \DB::table('locale_text')
            ->where('id', "art_title_".$id)
            ->where('locale', $locale)
            ->update(array('lstring' => $attributes['title']));
    }

    public function createLText($id, $attributes, $locale)
    {
        $id = $this->article->id;

        $a_m_e = new \LocaleText;
        $a_m_e->id = "art_meta_ex_".$id;
        $a_m_e->locale = $locale;
        $a_m_e->lstring = $attributes['excerpt'];
        $a_m_e->save();

        $a_m_t = new \LocaleText;
        $a_m_t->id = "art_meta_title_".$id;
        $a_m_t->locale = $locale;
        $a_m_t->lstring = $attributes['title'];
        $a_m_t->save();

        $a_m_d = new \LocaleText;
        $a_m_d->id = "art_meta_desc_".$id;
        $a_m_d->locale = $locale;
        $a_m_d->lstring = $attributes['meta_description'];
        $a_m_d->save();

        $a_m_kw = new \LocaleText;
        $a_m_kw->id = "art_meta_kw_".$id;
        $a_m_kw->locale = $locale;
        $a_m_kw->lstring = $attributes['meta_keywords'];
        $a_m_kw->save();

        $a_c = new \LocaleText;
        $a_c->id = "art_content_".$id;
        $a_c->locale = $locale;
        $a_c->lstring = $attributes['content'];
        $a_c->save();

        $a_t = new \LocaleText;
        $a_t->id = "art_title_".$id;
        $a_t->locale = $locale;
        $a_t->lstring = $attributes['title'];
        $a_t->save();

        return true;
    }

    public function getLText($id, $locale)
    {
        $rArray = array();
        $rArray['ame'] = \DB::table('locale_text')
                            ->where('id', "art_meta_ex_".$id)
                            ->where('locale', $locale)
                            ->first();

        $rArray['amt'] = \DB::table('locale_text')
                            ->where('id', "art_meta_title_".$id)
                            ->where('locale', $locale)
                            ->first();

        $rArray['amd'] = \DB::table('locale_text')
                            ->where('id', "art_meta_desc_".$id)
                            ->where('locale', $locale)
                            ->first();

        $rArray['amk'] = \DB::table('locale_text')
                            ->where('id', "art_meta_kw_".$id)
                            ->where('locale', $locale)
                            ->first();

        $rArray['ac'] = \DB::table('locale_text')
                           ->where('id', "art_content_".$id)
                           ->where('locale', $locale)
                           ->first();

        $rArray['at'] = \DB::table('locale_text')
                            ->where('id', "art_title_".$id)
                            ->where('locale', $locale)
                            ->first();

        return $rArray;
    }

    public function flushLText($id)
    {
        \DB::table('locale_text')
            ->where('id', "art_meta_ex_".$id)
            ->delete();

         \DB::table('locale_text')
            ->where('id', "art_meta_title_".$id)
            ->delete();

         \DB::table('locale_text')
            ->where('id', "art_meta_desc_".$id)
            ->delete();

         \DB::table('locale_text')
            ->where('id', "art_meta_kw_".$id)
            ->delete();

        \DB::table('locale_text')
           ->where('id', "art_content_".$id)
           ->delete();

        \DB::table('locale_text')
            ->where('id', "art_title_".$id)
            ->delete();
    }

    public function destroy($id) {

        $article = $this->article->findOrFail($id);
        $article->tags()->detach();
        $article->delete();

        $this->flushLText($id);
    }

    public function togglePublish($id) {

        $page = $this->article->find($id);

        $page->is_published = ($page->is_published) ? false : true;
        $page->save();

        return Response::json(array('result' => 'success', 'changed' => ($page->is_published) ? 1 : 0));
    }

    function getUrl($id) {

        $article = $this->article->findOrFail($id);
        return url('article/' . $id . '/' . $article->slug, $parameters = array(), $secure = null);
    }
}
