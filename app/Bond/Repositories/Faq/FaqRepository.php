<?php namespace Bond\Repositories\Faq;

use Config;
use Faqs;
use DB;
use Response;
use Bond\Repositories\BaseRepositoryInterface as BaseRepositoryInterface;
use Bond\Exceptions\Validation\ValidationException;
use Bond\Repositories\AbstractValidator as Validator;

class FaqRepository extends Validator implements BaseRepositoryInterface {

    protected $perPage;
    protected $news;

    /**
     * Rules
     *
     * @var array
     */
    protected static $rules = [
        'title'    => 'required',
        'content'  => 'required',
        'datetime' => 'required|date',
    ];

    public function __construct(Faqs $news) {

        $config = Config::get('bondcms');
        $this->perPage = $config['modules']['per_page'];
        if ($news) {
            $this->news = $news;
        } else {
            $this->news = new Faqs();
        }

    }

    public function byCategory($cat) {
        $locale = \App::getLocale();
        $result =DB::table('faq')->orderBy('order', 'ASC')
            ->join('fbf_faq_category_fbf_laravel_simple_faqs', 'fbf_faq_category_fbf_laravel_simple_faqs.fbf_laravel_simple_faqs_id', '=', 'faq.id')
            ->where('is_published', 1)
            ->where('fbf_faq_category_fbf_laravel_simple_faqs.fbf_faq_category_id', '=', $cat)
            ->get();
        $ret = array();
        foreach($result as $r)
        {
            if($locale!="en_gb")
            {
                if($this->checkTrans($locale, $r->fbf_laravel_simple_faqs_id))
                {
                    $ltext = $this->getLText($r->fbf_laravel_simple_faqs_id, $locale);
                    $r->title = $ltext['ft']->lstring;
                    $r->content = $ltext['fc']->lstring;
                    $ret[] = $r;
                }
            }
            else
            {
                $ret[]=$r;
            }

        }
        return $ret;
    }


    public function all() {

        return $this->news->orderBy('order', 'ASC')
            ->where('is_published', 1)
            ->get();
    }

    public function lists() {

        return $this->news->get()->lists('title', 'id');
    }

    public function paginate($perPage = null, $all=false) {

        if($all)
            return $this->news->orderBy('order', 'ASC')
            ->paginate(($perPage) ? $perPage : $this->perPage);

        return $this->news->orderBy('order', 'ASC')
            ->where('is_published', 1)
            ->paginate(($perPage) ? $perPage : $this->perPage);
    }

    public function find($id) {
        $locale = \App::getLocale();
        $news = $this->news->findOrFail($id);
        if($this->checkTrans($locale, $id))
        {
            $lText = $this->getLText($id, $locale);
            $news->title = $lText['ft']->lstring;
            $news->content = $lText['fc']->lstring;
        }
        return $news;
    }

    public function create($attributes) {
        $locale = 'en_gb';
        $attributes['is_published'] = isset($attributes['is_published']) ? true : false;

        if ($this->isValid($attributes)) {

            $this->news->fill($attributes)->save();
            $id = $this->news->id;
            $this->createLText($id, $locale, $attributes);
            //save category
            if(isset($attributes['category']) && is_array($attributes['category'])) {
                $categories = $attributes['category'];
                $data = array();
                foreach($categories as $cat) {
                    $data[] = array(
                        'fbf_faq_category_id' => $cat,
                        'fbf_laravel_simple_faqs_id' => $this->news->id
                    );
                }
                DB::table('fbf_faq_category_fbf_laravel_simple_faqs')->insert($data);
            }

            return true;
        }

        throw new ValidationException('News validation failed', $this->getErrors());
    }

    public function update($id, $attributes) {
        $locale = \App::getLocale();
        $attributes['is_published'] = isset($attributes['is_published']) ? true : false;

        $this->news = $this->find($id);

        if ($this->isValid($attributes)) {
            if($locale == 'en_gb')
            {
                $this->news->fill($attributes)->save();
            }
            if($this->checkTrans($locale, $id))
            {
                $this->updateLText($id, $attributes, $locale);
            }
            else
            {
                $this->createLText($id, $locale, $attributes);
            }
            //update category
            if(isset($attributes['category']) && is_array($attributes['category'])) {
                //before update delete already exists
                DB::table('fbf_faq_category_fbf_laravel_simple_faqs')
                    ->where('fbf_laravel_simple_faqs_id', '=', $this->news->id)->delete();
                $categories = $attributes['category'];
                $data = array();
                foreach($categories as $cat) {
                    $data[] = array(
                        'fbf_faq_category_id' => $cat,
                        'fbf_laravel_simple_faqs_id' => $this->news->id
                    );
                }
                DB::table('fbf_faq_category_fbf_laravel_simple_faqs')->insert($data);
            }
            return true;
        }

        throw new ValidationException('News validation failed', $this->getErrors());
    }

    public function destroy($id) {
        $news = $this->news->find($id)->delete();
        $this->flushLText($id);
    }

    public function togglePublish($id) {

        $news = $this->news->find($id);
        $news->is_published = ($news->is_published) ? false : true;
        $news->save();

        return Response::json(array('result' => 'success', 'changed' => ($news->is_published) ? 1 : 0));
    }

    public function updateLText($id, $attributes, $locale)
    {
        \DB::table('locale_text')
            ->where('id', "faq_title_".$id)
            ->where('locale', $locale)
            ->update(array('lstring' => $attributes['title']));

        \DB::table('locale_text')
            ->where('id', "faq_content_".$id)
            ->where('locale', $locale)
            ->update(array('lstring' => $attributes['content']));
    }

    public function createLText($id, $locale, $attributes)
    {
        $fbt_m_t = new \LocaleText;
        $fbt_m_t->id = "faq_title_".$id;
        $fbt_m_t->locale = $locale;
        $fbt_m_t->lstring = $attributes['title'];
        $fbt_m_t->save();

        $fbt_m_d = new \LocaleText;
        $fbt_m_d->id = "faq_content_".$id;
        $fbt_m_d->locale = $locale;
        $fbt_m_d->lstring = $attributes['content'];
        $fbt_m_d->save();

        return true;
    }

    public function getLText($id, $locale)
    {
        $rArray = array();

        $rArray['ft'] = \DB::table('locale_text')
                            ->where('id', "faq_title_".$id)
                            ->where('locale', $locale)
                            ->first();

        $rArray['fc'] = \DB::table('locale_text')
                            ->where('id', "faq_content_".$id)
                            ->where('locale', $locale)
                            ->first();
        return $rArray;
    } 


    public function flushLText($id)
    {
         \DB::table('locale_text')
            ->where('id', "faq_title_".$id)
            ->delete();

         \DB::table('locale_text')
            ->where('id', "faq_content_".$id)
            ->delete();
    }

    public function checkTrans($locale, $id)
    {
        $trans = \DB::table('locale_text')
                    ->where('id', 'faq_title_'.$id)
                    ->where('locale', $locale)
                    ->first();

        if($trans)
        {
            return true;
        }
        return false;
    }
}
