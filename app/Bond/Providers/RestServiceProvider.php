<?php namespace Bond\Providers;

use Illuminate\Support\ServiceProvider;

class RestServiceProvider extends ServiceProvider {
        public function register() {
                $this->app->bind('rest', 'Bond\Rest\Rest');
        }
}
