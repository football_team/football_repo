<?php namespace Bond\Providers;

use Illuminate\Support\ServiceProvider;

class EzpayServiceProvider extends ServiceProvider {
	public function register() {
		$this->app->bind('ezpay', 'Bond\Ezpay\Ezpay');
	}
}

