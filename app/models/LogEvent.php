<?php

class LogEvent extends Eloquent {

    public $table = 'log';
    public $fillable = ['modual', 'user', 'action', 'note'];
}
