<?php

class Map extends Eloquent {
    public $table = 'maps_list';
    public $fillable = ['name', 'src', 'status'];
    public $timestamps = false;

}