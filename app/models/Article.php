<?php

use Bond\Interfaces\BaseModelInterface as BaseModelInterface;

class Article extends BaseModel implements BaseModelInterface {

    public $table = 'articles';
    protected $fillable = ['title', 'slug', 'content', 'meta_title', 'meta_keywords', 'meta_description', 'is_published', 'feature_image', 'excerpt'];
    protected $appends = ['url'];

    public function tags() {

        return $this->belongsToMany('Tag', 'articles_tags');
    }

    public function category() {

        return $this->hasMany('Category', 'id', 'category_id');
    }

    public function setUrlAttribute($value) {

        $this->attributes['url'] = $value;
    }

    public function getUrlAttribute() {

        return "article/" . $this->attributes['id'] . "/" . $this->attributes['slug'];
    }

    public function getLocaleText()
    {
        $locale = App::getLocale();
        $id = $this->attributes['id'];

        if(!$locale)
        {
            $locale='en_gb';
        }

        $meta_title = $this->meta_title;
        $meta_keywords = $this->meta_keywords;
        $meta_description = $this->meta_description;
        $title = $this->title;
        $content = $this->content;
        $excerpt = $this->excerpt;

        if($this->checkLText($locale))
        {
            $meta_title = LocaleText::where('id', '=', 'art_meta_title_'.$id)
                ->where('locale', '=', $locale)
                ->first()
                ->getString();

            if(!$meta_title)
            {
                $meta_title = '';
            }

            $meta_keywords = LocaleText::where('id', '=', 'art_meta_kw_'.$id)
                ->where('locale', '=', $locale)
                ->first();
            $meta_keywords = $meta_keywords->getString();

            if(!$meta_keywords)
            {
                $meta_keywords = '';
            }

            $meta_description = LocaleText::where('id', '=', 'art_meta_desc_'.$id)
                ->where('locale', '=', $locale)
                ->first()
                ->getString();

            if(!$meta_description)
            {
                $meta_description = '';
            }

            $title = LocaleText::where('id', '=', 'art_title_'.$id)
                ->where('locale', '=', $locale)
                ->first()
                ->getString();

            if(!$title)
            {
                $title = '';
            }

            $content = LocaleText::where('id', '=', 'art_content_'.$id)
                ->where('locale', '=', $locale)
                ->first()
                ->getString();

            if(!$content)
            {
                $content = '';
            }

            $excerpt = LocaleText::where('id', '=', 'art_meta_ex_'.$id)
                ->where('locale', '=', $locale)
                ->first()
                ->getString();

            if(!$excerpt)
            {
                $excerpt = '';
            }
        }

        $return = array(
            'meta_title' => $meta_title,
            'meta_keywords' => $meta_keywords,
            'meta_description' => $meta_description,
            'title' => $title,
            'content' => $content,
            'excerpt' => $excerpt,
        );

        return $return;
    }

    public function checkLText($locale)
    {
        $id = $this->id;
        $title = LocaleText::where('id', '=', 'art_title_'.$id)
            ->where('locale', '=', $locale)
            ->first();

        if($title)
        {
            return true;
        }
        return false;
    }
}
