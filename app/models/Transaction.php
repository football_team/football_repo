<?php

class Transaction extends Eloquent {
    public $table = 'payment_redirect_request';
    public $fillable = ['order_id', 'tid', 'status','order'];
    public $timestamps = false;

    public static function getTransaction ($orderId, $status = '0') {
        return Transaction::where('order_id', '=',$orderId )->where('status', '=', $status)->first();
    }


}