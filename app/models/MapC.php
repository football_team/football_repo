<?php

/**
 * @property  frontend
 * @property  right
 * @property  left
 */
class MapC extends BaseModel
{
    public $table = 'map_compare';
    public $timestamps = false;
    protected $fillable = ['left', 'right','frontend','color'];


}