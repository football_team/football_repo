<?php

class MapMeta extends Eloquent {
    public $table = 'map_meta';
    public $fillable = ['map_object_id', 'key','value'];
    public $timestamps = false;

}