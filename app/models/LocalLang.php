<?php

class LocalLang extends BaseModel
{
    public $table = 'locale_text';
    public $timestamps = false;
    protected $fillable = ['id','locale', 'lstring'];


}