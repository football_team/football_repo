<?php

class LocaleText extends BaseModel {
	public $table = 'locale_text';
	public $timestamps = false;
	protected $fillable = ['id', 'locale', 'lstring'];

	public function getString()
	{	
		return $this->attributes['lstring'];
	}
}