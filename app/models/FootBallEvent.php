<?php

use Bond\Interfaces\BaseModelInterface as BaseModelInterface;

class FootBallEvent extends BaseModel implements BaseModelInterface {

    public $table = 'events';
    public $fillable=['title', 'slug', 'content', 'datetime', 'is_published', 'team_type','svgmap',
        'home_team_id', 'away_team_id', 'season_id', 'tournament_id', 'event_in_home',
        'venue_image', 'feature_image', 'ticket_type_ids', 'form_of_ticket_ids', 'ticket_restriction_ids', 'event_location', 'feature_event',
        'meta_title', 'meta_description', 'meta_keyword', 'home_widget', 'home_widget_order', 'tag_feat', 'tag_sellingfast', 'is_tbc'];

    protected $appends = ['url'];

    public static function getUrl($events) {
        $locale = \App::getLocale();
        if(isset($events->season_id) && $events->tournament_id) {
            $output = "";
            if(Cache::has($locale.'_event_url_'.$events->id))
            {
                $output.= Cache::get($locale.'_event_url_'.$events->id);
            }
            else
            {
                $season = FootballTickets::find($events->season_id);
                $league = FootballTickets::find($events->tournament_id);
                if($season && $league ) {
                    $str = route('ticket.events.display.noseason', array('league'=>$league->slug, 'slug'=>$events->slug));
                    Cache::put($locale.'_event_url_'.$events->id, $str, 3600);
                    $output .= $str;
                } else {
                    return '';
                }
            }

            return $output;
        }

    }

    public static function getClubRelatedTickets($clubId = '') {
        $results = DB::table('events')
            ->select('events.id','events.season_id', 'events.tournament_id','events.slug', 'events.title', 'events.datetime', DB::raw('MIN(events_related_tickets.price) AS price'), 'events.is_tbc')
            ->join('events_related_tickets','events_related_tickets.event_id','=','events.id')
            ->whereRaw('events.datetime >= NOW() AND ( events.away_team_id = '.$clubId.' OR events.home_team_id='.$clubId.'  ) AND events_related_tickets.available_qty > 0 AND events_related_tickets.ticket_status = "active"' )
            ->where('events.is_tbc', false)
            ->groupBy('events.id')
            ->orderBy('price', 'ASC')
            ->get();



        return $results;
    }

    public static function getClubRelatedTicketsOrderByDateTime($clubId = '', $show_fees = true) {
        $results = DB::table('events')
            ->leftJoin('football_ticket', 'events.tournament_id', '=', 'football_ticket.id')
            ->select('events.id', 'events.season_id', 'events.tournament_id', 'events.slug', 'events.tag_feat', 'events.tag_sellingfast', 'events.tag_topgame', 'events.title', 'events.datetime', 'events.event_location', 'football_ticket.title as tourney_title', 'events.is_tbc')
            ->whereRaw('events.datetime >= NOW() AND ( events.away_team_id = '.$clubId.' OR events.home_team_id='.$clubId.'  )' )
            ->groupBy('events.id')
            ->orderBy('events.is_tbc', 'ASC')
            ->orderBy('events.datetime', 'ASC')
            ->get();
        $locale = App::getLocale();
        $cRate = DB::table('locales')
                    ->where('lang', $locale)
                    ->first();

        $bf = 1 + (intval(Options::getOption('booking_fees'))/100);

        $cRate = $cRate->conversion;

        foreach($results as $result)
        {
            $id = $result->id;
            $rs = DB::table('events_related_tickets')
                ->select(DB::Raw('MIN(price) as price'))
                ->where('ticket_status', '=', "active")
                ->where('event_id','=', $id)
                ->where('available_qty', '>', 0)
                ->first();

            if($rs->price)
            {
                if($show_fees)
                {
                    $result->price = ($rs->price * $bf) * $cRate;
                }
                else
                {
                    $result->price = $rs->price * $cRate;
                }
            }

            $trans=DB::table('locale_text')
                        ->where('locale', $locale)
                        ->where('id', 'event_title_'.$result->id)
                        ->first();

            if($trans && $trans->lstring!='')
            {
                $result->title = $trans->lstring;
            }
        }

        return $results;
    }

    public static function getClubRelatedTicketsOrderByDateTimeHA($clubId = '', $home = false, $show_fees = true) {
        $bf = 1 + (intval(Options::getOption('booking_fees'))/100);
        if($home)
        {
            $results = DB::table('events')
                ->leftJoin('football_ticket', 'events.tournament_id', '=', 'football_ticket.id')
                ->select('events.id', 'events.season_id', 'events.tournament_id', 'events.slug', 'events.tag_feat', 'events.tag_sellingfast', 'events.tag_topgame', 'events.title', 'events.datetime', 'events.event_location', 'football_ticket.title as tourney_title', 'events.is_tbc')
                ->whereRaw('events.datetime >= NOW() AND ((events.away_team_id = '.$clubId.' AND events.event_in_home = 0) OR (events.home_team_id='.$clubId.' AND events.event_in_home = 1 ))' )
                ->groupBy('events.id')
                ->orderBy('events.is_tbc', 'ASC')
                ->orderBy('events.datetime', 'ASC')
                ->get();
        }
        else
        {
            $results = DB::table('events')
                ->leftJoin('football_ticket', 'events.tournament_id', '=', 'football_ticket.id')
                ->select('events.id', 'events.season_id', 'events.tournament_id', 'events.slug', 'events.tag_feat', 'events.tag_sellingfast', 'events.tag_topgame', 'events.title', 'events.datetime', 'events.event_location', 'football_ticket.title as tourney_title', 'events.is_tbc')
                ->whereRaw('events.datetime >= NOW() AND ((events.away_team_id = '.$clubId.' AND events.event_in_home = 1) OR (events.home_team_id='.$clubId.' AND events.event_in_home = 0 ))' )
                ->groupBy('events.id')
                ->orderBy('events.is_tbc', 'ASC')
                ->orderBy('events.datetime', 'ASC')
                ->get();
        }

        $locale = App::getLocale();
        $cRate = DB::table('locales')
            ->where('lang', $locale)
            ->first();

        $cRate = $cRate->conversion;

        foreach($results as $result)
        {
            $id = $result->id;
            $rs = DB::table('events_related_tickets')
                ->select(DB::Raw('MIN(price) as price'))
                ->where('ticket_status', '=', "active")
                ->where('event_id','=', $id)
                ->where('available_qty', '>', 0)
                ->first();

            if($rs->price)
            {
                if($show_fees)
                {
                    $result->price = ($rs->price * $bf) * $cRate;
                }
                else
                {
                    $result->price = $rs->price * $cRate;
                }
            }

            $trans=DB::table('locale_text')
                ->where('locale', $locale)
                ->where('id', 'event_title_'.$result->id)
                ->first();

            if($trans && $trans->lstring!='')
            {
                $result->title = $trans->lstring;
            }
        }

        return $results;
    }

    public static function getLeagueRelatedTickets($tid = '') {
        $locale = App::getLocale();
        $output = array();

        $results = DB::table('events')
            ->select('events.*','football_ticket.venue_splash as feature_imagex','football_ticket.id as fooit')
            ->leftJoin('football_ticket','football_ticket.id','=','events.home_team_id')
            ->where('events.tournament_id', '=',$tid)
            ->where('events.feature_event', '=', '1')
            ->whereRaw('events.datetime >= NOW() ' )
            ->groupBy('events.id')
            ->orderBy('events.datetime', 'DESC')
            ->where('events.is_tbc', false)
            ->take(6)
            ->get();

        foreach($results as $result) {
            $trans = DB::table('locale_text')
                        ->where('locale',$locale)
                        ->where('id', 'event_title_'.$result->id)
                        ->first();

            if($trans && $trans->lstring != "")
            {
                $result->title = $trans->lstring;
            }
            $result->homeTeamClubLog = FootballTicketMeta::getTicketMeta($result->home_team_id, 'club_logo');
            $result->awayTeamClubLog = FootballTicketMeta::getTicketMeta($result->away_team_id, 'club_logo');
            $output[] = $result;
        }
        return $output;
    }

    public static function upcoming($tid = 0) {
        $locale = App::getLocale();

        $output = array();

        $output = DB::table('events')
            ->select('events.*','football_ticket.*')
            ->leftJoin('football_ticket','football_ticket.id','=','events.home_team_id')
            ->where('events.tournament_id', '=',$tid)
            ->groupBy('events.id')
            ->orderBy('events.datetime', 'ASC')
            ->where('events.is_tbc', false)
            ->whereRaw('events.datetime >= NOW() ' )
            ->take(10)
            ->get();

        foreach($output as $event)
        {
            $trans = DB::table('locale_text')
                        ->where('locale', $locale)
                        ->where('id', 'event_title_'.$event->id)
                        ->first();

            if($trans && $trans->lstring!='')
            {
                $event->title = $trans->lstring;
            }
        }
        return $output;
    }


    public static function upcomingWithoutJoin($tid = 0) {
        $locale = App::getLocale();

        $output = array();

        $output = DB::table('events')
            ->select('events.*')
            ->where('events.tournament_id', '=',$tid)
            ->groupBy('events.id')
            ->orderBy('events.datetime', 'ASC')
            ->where('events.is_tbc', false)
            ->whereRaw('events.datetime >= NOW() ' )
            ->take(10)
            ->get();

        foreach($output as $event)
        {
            $trans = DB::table('locale_text')
                ->where('locale', $locale)
                ->where('id', 'event_title_'.$event->id)
                ->first();

            if($trans && $trans->lstring!='')
            {
                $event->title = $trans->lstring;
            }
        }
        return $output;
    }

    public static function updateMagentoDescription($productId) {
        $ticket = RelatedTicket::getTicketByTicketId($productId);
        $event = FootBallEvent::find($ticket->event_id);

        $data['game']       = $event->title;
        $data['event_date'] = $event->datetime;
        $data['event_location'] = $event->event_location;
        $data['formOfTicket'] = trans('homepage.'.@$ticket->formOfTicket['title']);
        $data['location'] = @$ticket->ticketType['title'];
        //Log::info("Data: " . var_export($data, true));
        $temp = array();
        if(isset($ticket->info['loc_block']) && !empty($ticket->info['loc_block'])) {
            $temp[] =     'Block: '.$ticket->info['loc_block'];
        }

        if(isset($ticket->info['loc_row']) && !empty($ticket->info['loc_row'])) {
            $temp[] =     'Row: '.$ticket->info['loc_row'];
        }

        $data['location'] = $data['location'].', '.implode(', ', $temp);

        $temp = array();

        $defaultRestriction = [];
        foreach($event->getSelectedRestrictions()  as $val) {

            $defaultRestriction[] = array(
                'id'    => $val->id,
                'title' => $val->title
            );
        }

        foreach($defaultRestriction as $r):
            if(in_array($r['id'],$ticket->info['restrictions'])):
                $temp[] = $r['title'];
            endif;
        endforeach;

        if($ticket->buyerNot != '' ):
            $temp[] = '<br />Note: '.$ticket->buyerNot;
        endif;

        $data['restrictions'] = implode(', ', $temp);

        $description = self::ticketInfo($data);
        //Log::info("Description: " . $description);
        $response = TicketSoap::process('catalog_product.update', array($productId, array(
            'description' => $description,
            'short_description' => $description
        )));
    }

    public static function ticketInfo($option) {
        ob_start();
        ?>
        <table cellspacing="0" cellpadding="0" border="0" width="650">
            <thead>
            <tr>
                <th align="left" width="325" bgcolor="#EAEAEA" style="font-size:13px;padding:5px 9px 6px 9px;line-height:1em">Game</th>
                <th width="10"></th>
                <th align="left" width="325" bgcolor="#EAEAEA" style="font-size:13px;padding:5px 9px 6px 9px;line-height:1em">Ticket Information</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td valign="top" style="font-size:12px;padding:7px 9px 9px 9px;border-left:1px solid #eaeaea;border-bottom:1px solid #eaeaea;border-right:1px solid #eaeaea">
                    <?php echo @$option['game'];?>, <?php echo @$option['event_location']; ?> <br/>

                    Date: <?php echo date('l, dS F Y, h:ia', strtotime(@$option['event_date']));?>
                </td>
                <td>&nbsp;</td>
                <td valign="top" style="font-size:12px;padding:7px 9px 9px 9px;border-left:1px solid #eaeaea;border-bottom:1px solid #eaeaea;border-right:1px solid #eaeaea">
                    Location: <?php echo @$option['location'];?> <br />
                    Ticket Type: <?php echo @$option['formOfTicket']; ?> <br />
                    Restrictions: <?php echo !empty($option['restrictions'])?$option['restrictions']: 'No restrictions'; ?>
                </td>
            </tr>
            </tbody>
        </table>
        <?php
        $output = ob_get_contents();
        ob_end_clean();
        return $output;
    }

    public function setUrlAttribute($value)
    {
        $this->attributes['url'] = $value;
    }

    public function getUrlAttribute()
    {
        //return "events/" . $this->attributes['id'] . "/" . $this->attributes['slug'];
        $season = FootballTickets::find($this->attributes['season_id']);
        $league = FootballTickets::find($this->attributes['tournament_id']);
        $output = "";
        if ($season && $league) {
            $output .= route('ticket.events.display.noseason', array('league' => $league->slug, 'slug' => $this->attributes['slug']));
        } else {
            return '';
        }
        return $output;
    }

    public function getTicketTypes($id = '')
    {
        if ($id != '' && $id != $this->id) {
            $this->find($id);
        }
        if (!empty($this->ticket_type_ids) && $this->ticket_type_ids != '') {
            $ArrTypes = explode(',', $this->ticket_type_ids);
            $types = new TicketType();
            return $types->ticketByIds($ArrTypes);
        } else {
            $stads = DB::table('event_stadium')
                ->where('event_id', $this->id)
                ->first();

            if (!empty($stads)) {
                $stads = Stadium::where('id', $stads->stadium_id)->first();
                if ((!empty($stads->ticket_type_ids)) && ($stads->ticket_type_ids != ''))
                {
                    $ArrTypes = explode(',', $stads->ticket_type_ids);
                    $types = new TicketType();
                    return $types->ticketByIds($ArrTypes);
                }
            } else {
                $team_id = null;
                if ($this->event_in_home) {
                    $team_id = $this->home_team_id;
                } else {
                    $team_id = $this->away_team_id;
                }
                $team = DB::table('football_ticket')->where('id', $team_id)->first();

                $teamStad = DB::table('team_stadium')
                    ->where('team_id', $team_id)
                    ->first();
                if (!empty($teamStad)) {
                    $stad = Stadium::where('id', $teamStad->stadium_id)->first();
                    if ($stad) {
                        if (!empty($stad->ticket_type_ids) && ($stad->ticket_type_ids != '')) {
                            $ArrTypes = explode(',', $stad->ticket_type_ids);
                            $types = new TicketType();
                            return $types->ticketByIds($ArrTypes);
                        }
                    }

                }
            }
        }
        return array();
    }

    public function getFormOfTickets($id = '')
    {
        if ($id != '' && $id != $this->id) {
            $this->find($id);
        }
        if (!empty($this->form_of_ticket_ids)) {
            $ArrTypes = explode(',', $this->form_of_ticket_ids);
            $types = new FormOfTicket();
            return $types->ticketByIds($ArrTypes);
        }
        return array();
    }

    public function getSelectedRestrictions()
    {
        if (!empty($this->ticket_restriction_ids)) {
            $ArrTypes = explode(',', $this->ticket_restriction_ids);
            $types = new TicketRestriction();
            return $types->ticketByIds($ArrTypes);
        }
        return array();
    }
}
