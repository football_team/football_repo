<?php

class Translate extends BaseModel
{
    public $table = 'trans';
    public $timestamps = false;
    protected $fillable = ['id', 'text'];


}