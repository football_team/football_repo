<?php

class EventsrelatedTickets extends BaseModel {
    public $table = 'events_related_tickets';
    public $timestamps = false;
    protected $fillable = ['ticket','reviewed'];

}