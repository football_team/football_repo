<?php

class MapModel extends Eloquent {
    public $table = 'map_object';
    public $fillable = ['map_id', 'node','status'];
    public $timestamps = false;

}