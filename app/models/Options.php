<?php

class Options extends BaseModel {
    public $table = 'options';
    public $fillable = ['option', 'value'];
    public $timestamps = false;

    public static function getOption($key='') {
        $opt = Options::where('option', '=', $key)->first();

        if(!$opt) {
            return '';
        }

        return $opt->value;
    }
}
